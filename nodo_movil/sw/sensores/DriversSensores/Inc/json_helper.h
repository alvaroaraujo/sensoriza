/*
 * json_helper.h
 *
 *  Created on: 28 nov. 2018
 *      Author: albarc
 */

#ifndef INC_JSON_HELPER_H_
#define INC_JSON_HELPER_H_

/* Formato JSON mensajes Sensoriza
 *
 * {
 * 		"name":"Nodo 1",
 * 		"latitude":40.45232589,
 * 		"longitude":-3.72633016,
 * 		"air_temperature":22,
 * 		"ground_temperature":18,
 * 		"humidity":59,
 * 		"pressure":800,
 * 		"luminosity":403,
 * 		"rain":4,
 * 		"salt":16
 * }
 *
 */

/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "string.h"

/* JSON full struct*/
typedef struct {
	uint8_t node_id;
	double latitude;
	double longitude;
	int16_t air_temp;
	int16_t ground_temp;
	uint8_t hum;
	uint16_t press;
	uint32_t lum;
	uint16_t rain;
	uint16_t salt;
} json_all_fields_t;

/** JSON Fields */
#define NAME_KEY					"\"name\""
#define LATITUDE_KEY				"\"latitude\""
#define LONGITUDE_KEY				"\"longitude\""
#define AIR_TEMP_KEY				"\"air_temperature\""
#define GROUND_TEMP_KEY				"\"ground_temperature\""
#define HUM_KEY						"\"humidity\""
#define PRESS_KEY					"\"pressure\""
#define LUM_KEY						"\"luminosity\""
#define RAIN_KEY					"\"rain\""
#define SALT_KEY					"\"salt\""

/** JSON Fields Masks */
#define LATITUDE_MASK				0x0100
#define LONGITUDE_MASK				0x0080
#define AIR_TEMP_MASK				0x0040
#define GROUND_TEMP_MASK			0x0020
#define HUM_MASK					0x0010
#define PRESS_MASK					0x0008
#define LUM_MASK					0x0004
#define RAIN_MASK					0x0002
#define SALT_MASK					0x0001

/* Functions ------------------------------------------------------------------*/
void full_json_struct_writer (char* json_message, json_all_fields_t fields);
void requested_fields_json_writer (char* json_message, uint16_t requested_fields, json_all_fields_t fields);
void dtostrf (double val, uint8_t decimal_width, char *sout);

#endif /* INC_JSON_HELPER_H_ */
