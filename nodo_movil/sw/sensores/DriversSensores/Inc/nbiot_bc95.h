/*
 * nbiot_bc95.h
 *
 *  Created on: 23 oct. 2018
 *      Author: albarc
 */

#ifndef INC_NBIOT_BC95_H_
#define INC_NBIOT_BC95_H_

/* Includes ------------------------------------------------------------------*/
#include "usart.h"
#include "string.h"
#include "stdlib.h"

/** Boolean Constants */
#define TRUE	1
#define FALSE	0

/** BC95 Constants */
#define BC95_BAUDRATE							9600
#define VODAFONE_NETWORK_CODE					"\"21401\""			// Vodafone ID (escape quotes)
#define WAIT_AFTER_REBOOT_MS					5000
#define WAIT_AFTER_CONNECT_MS					20000
#define WAIT_AFTER_SOCKET_MS					2000

/** BC95 AT COMMANDS */
/** COMMAND CODES */
#define AT_PREFIX								"AT+"
#define EQUAL_SIGN								"="
#define COMMA_SIGN								","
#define SEMICOLON_SIGN							";"
#define PLUS_SIGN								"+"
#define QUESTION_MARK							"?"
#define END_OF_COMMAND							"\r"
#define	REBOOT									"NRB"
#define NWK_REGISTER							"CEREG"
#define CONNECTION_STATUS						"CSCON"
#define SET_FUNCTIONALITY						"CFUN"
#define PDP_CONTEXT								"CGDCONT"
#define OPERATOR_SELECTION						"COPS"
#define GET_ASSIGNED_IP_ADDRESS					"CGPADDR"
#define CREATE_SOCKET							"NSOCR"
#define SEND_UDP_DATAGRAM						"NSOST"
#define RECEIVE_UDP_DATAGRAM					"NSORF"
#define CLOSE_SOCKET							"NSOCL"

/** COMMAND PARAMETERS */
#define NWK_REGISTER_PARAM						"2"				// Enable registration and location info in response
#define CONNECTION_STATUS_PARAM					"1"				// Result code only mode
#define SET_FUNCTIONALITY_PARAM					"1"				// Full functionality
#define SET_PDP_CONTEXT_PARAM_1					"0"				// Context ID
#define SET_PDP_CONTEXT_PARAM_2					"\"IP\""		// Type of IP connection (escape quotes)
#define SET_PDP_CONTEXT_PARAM_3					"\"\""			// Name of network, leave empty (escape quotes)
#define OPERATOR_SELECTION_PARAM_1				"1"				// Manual
#define OPERATOR_SELECTION_PARAM_2				"2"				// Numeric format
#define CREATE_SOCKET_PARAM_TYPE				"DGRAM"			// Type (datagram)
#define CREATE_SOCKET_PARAM_PROTOCOL			"17"			// UDP code
#define CREATE_SOCKET_PARAM_LIST_PORT			"16666"			// Listening port
#define CREATE_SOCKET_PARAM_REF					"1"				// Reference to the created socket (to use in further sendings)
#define SEND_UDP_DATAGRAM_PARAM_REF				0				// Socket number returned by NSOCR (maybe not a define)
#define SEND_UDP_DATAGRAM_PARAM_IP				"35.237.66.97"	// Remote server IP
#define SEND_UDP_DATAGRAM_PARAM_PORT			"8888"			// Remote server port
#define RECEIVE_UDP_DATAGRAM_PARAM_REF			0				// Socket number returned by NSOCR (maybe not a define)
#define RECEIVE_UDP_DATAGRAM_PARAM_MAX_LENGTH	80				// Received packet max length (in bytes)
#define CLOSE_SOCKET_PARAM_REF					0				// Socket number returned by NSOCR (maybe not a define)


/* Functions  ****************************************************/
void reboot (void);
void network_register (void);
void enable_connection_status_messages (void);
void set_full_functionality (void);
void set_pdp_context (void);
void get_pdp_context (void);
void connect_to_network (void);
void get_ip_address (void);
void open_udp_socket (char** socket_reference);
void send_udp_message (char* socket_reference, char* ip_address, char* port_number, char* message);
void close_udp_socket (char* socket_reference);
void all_connection_commands (void);

uint16_t string_to_hex (char* input, char** output);
uint8_t create_get_AT_command (char* command_code, char** at_command);
uint8_t create_non_param_AT_command (char* command_code, char** at_command);
uint8_t create_one_param_AT_command (char* command_code, char* command_param, char** at_command);
uint8_t create_three_param_AT_command (char* command_code, char* command_param_1, char* command_param_2,
		char* command_param_3, char** at_command);
uint8_t create_four_param_AT_command (char* command_code, char* command_param_1,
		char* command_param_2, char* command_param_3, char* command_param_4, char** at_command);

// Only functions needed by the main app
void bc95_setup_nbiot_module (char** socket_reference);
void bc95_send_nbiot_message (char* socket_reference, char* message);


#endif /* INC_NBIOT_BC95_H_ */
