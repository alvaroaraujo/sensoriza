/*
 * nbiot_bg96.h
 *
 *  Created on: 12 dic. 2018
 *      Author: albarc
 */

#ifndef INC_NBIOT_BG96_H_
#define INC_NBIOT_BG96_H_

/* Includes ------------------------------------------------------------------*/
#include "usart.h"
#include "string.h"
#include "stdlib.h"

/** Boolean Constants */
#define TRUE	1
#define FALSE	0

/** BG96 Constants */
#define BG96_BAUDRATE							115200
#define VODAFONE_NETWORK_CODE					"\"21401\""			// Vodafone ID (escape quotes)
#define WAIT_AFTER_POWERUP_MS					2000
#define WAIT_AFTER_CONNECT_MS					10000
#define WAIT_AFTER_SOCKET_MS					2000

#define POWERUP_PULSE_WIDTH_MS					580
#define RESET_PULSE_WIDTH_MS					300

/** BG96 AT COMMANDS */
/** COMMAND CODES */
#define AT_PREFIX								"AT+"
#define EQUAL_SIGN								"="
#define COMMA_SIGN								","
#define SEMICOLON_SIGN							";"
#define PLUS_SIGN								"+"
#define QUESTION_MARK							"?"
#define END_OF_COMMAND							"\r"
#define NWK_REGISTRATION_STATUS					"CREG"
#define NWK_REGISTRATION_STATUS_2				"CGREG"
#define EPS_NWK_REGISTRATION_STATUS				"CEREG"
#define SET_FUNCTIONALITY						"CFUN"
#define OPERATOR_SELECTION						"COPS"
#define GET_ASSIGNED_IP_ADDRESS					"CGPADDR"
#define OPEN_SOCKET								"QIOPEN"
#define SEND_DATA								"QISEND"
#define CLOSE_SOCKET							"QICLOSE"
#define ENABLE_GPS								"QGPS"
#define REQUEST_GPS_LOCATION					"QGPSLOC"
#define SLEEP_MODE								"QSCLK"

/** COMMAND PARAMETERS */
#define NWK_REGISTRATION_PARAM					"2"					// Enable registration and location info in response
#define SET_FULL_FUNCTIONALITY_PARAM			"1"					// Full functionality
#define SET_MIN_FUNCTIONALITY_PARAM				"0"					// Minimum functionality
#define OPERATOR_SELECTION_PARAM_1				"1"					// Manual
#define OPERATOR_SELECTION_PARAM_2				"2"					// Numeric format
#define OPEN_SOCKET_CONTEXT_ID					"1"					// Between 1 and 16
#define OPEN_SOCKET_CONNECT_ID					"0"					// Between 0 and 11
#define OPEN_SOCKET_TYPE						"\"UDP SERVICE\""	// UDP (escape quotes)
#define OPEN_SOCKET_REMOTE_IP					"\"35.237.66.97\""	// Remote server IP
#define OPEN_SOCKET_REMOTE_PORT					"8888"				// Remote server port
#define OPEN_SOCKET_OWN_LISTEN_PORT				"16666"				// Own listen port
#define ENABLE_GPS_PARAM						"1"					// Enabled
#define REQUEST_GPS_LOCATION_PARAM				"2"					// GPS location format
#define ENTER_SLEEP_MODE_PARAM					"1"					// Enter sleep mode
#define EXIT_SLEEP_MODE_PARAM					"0"					// Exit sleep mode


/* Functions  ****************************************************/
void bg96_network_registration_status (void);
void bg96_network_registration_status_2 (void);
void bg96_eps_network_registration_status (void);
void bg96_set_full_functionality (void);
void bg96_set_minimum_functionality (void);
void bg96_connect_to_network (void);
void bg96_get_ip_address (void);
void bg96_open_udp_socket (void);
void bg96_send_udp_message (char* message);
void bg96_close_udp_socket (void);
void bg96_all_connection_commands (void);
void bg96_enableGPS (void);
void bg96_requestGPSlocation (double *lat, double *lon);
void bg96_enter_sleep_mode (void);
void bg96_exit_sleep_mode (void);

uint8_t bg96_create_non_param_AT_command (char* command_code, char** at_command);
uint8_t bg96_create_one_param_AT_command (char* command_code, char* command_param, char** at_command);
uint8_t bg96_create_three_param_AT_command (char* command_code, char* command_param_1, char* command_param_2,
		char* command_param_3, char** at_command);
uint8_t bg96_create_six_param_AT_command (char* command_code, char* command_param_1,
		char* command_param_2, char* command_param_3, char* command_param_4, char* command_param_5,
		char* command_param_6, char** at_command);

// Only functions needed by the main app
void bg96_powerup (void);
void bg96_reset (void);
uint8_t bg96_setup_nbiot_module (void);
void bg96_send_nbiot_message (char* message);


#endif /* INC_NBIOT_BC95_H_ */
