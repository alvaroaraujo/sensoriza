/*
 * rain_sensor.h
 *
 *  Created on: 28 may. 2019
 *      Author: albarc
 */

#ifndef INC_RAIN_SENSOR_H_
#define INC_RAIN_SENSOR_H_

/* Includes ------------------------------------------------------------------*/
#include "adc.h"

/** Boolean Constants */
#define TRUE	1
#define FALSE	0

/* Constants  ****************************************************/
// Los números están buscados para que den divisiones enteras
#define NUM_LEVELS 8
#define MAX_LEVEL 3990	// Cuando el sensor está seco se lee entre 3990 y los 4096 de fondo de escala
#define MIN_LEVEL 1098	// Cuando el sensor está totalmente cubierto de agua se lee como 1040-1060
#define STEP_SIZE 482 	// (MAX_LEVEL-MIN_LEVEL)/(NUM_LEVELS-2)

/* Functions  ****************************************************/
void rainSensor_init (void);
uint16_t readRainLevel (void);

#endif /* INC_RAIN_SENSOR_H_ */
