/*
 * luminosidad_tsl2561.c
 *
 *  Created on: 16 mar. 2018
 *      Author: albarc
 */

/* Includes ------------------------------------------------------------------*/
#include "../Inc/luminosidad_tsl2561.h"

#define USE_DISCO_LEDS 0

#if USE_DISCO_LEDS
#define toggle_green()	HAL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin)
#define toggle_blue()	HAL_GPIO_TogglePin(LD4_GPIO_Port, LD4_Pin)
#else
#define toggle_green()
#define toggle_blue()
#endif

/* Variables ------------------------------------------------------------------*/
uint8_t tx_data[4];
uint8_t rx_data[4];

uint8_t tsl2561Initialized = FALSE;
uint8_t tsl2561AutoGain = FALSE;
tsl2561IntegrationTime_t tsl2561IntegrationTime = TSL2561_INTEGRATIONTIME_DEFAULT;
tsl2561Gain_t tsl2561Gain = TSL2561_GAIN_DEFAULT;

/**************************************************************************/
/*!
    Enables the device
*/
/**************************************************************************/
void enable (void)
{
	/* Enable the device by setting the control bits to 0x03 */
	tx_data[0] = TSL2561_CONTROL_POWERON;
	uint8_t command_code = TSL2561_COMMAND_BIT | TSL2561_REGISTER_CONTROL;

	HAL_StatusTypeDef status = HAL_I2C_Mem_Write(&hi2c1, TSL2561_ADDRESS, command_code, I2C_MEMADD_SIZE_8BIT, tx_data, 1, 100);

	if (status == HAL_OK){
		toggle_green();
	} else {
		toggle_blue();
	}
}

/**************************************************************************/
/*!
    Disables the device
*/
/**************************************************************************/
void disable (void)
{
	/* Disable the device by setting the control bits to 0x00 */
	tx_data[0] = TSL2561_CONTROL_POWEROFF;
	uint8_t command_code = TSL2561_COMMAND_BIT | TSL2561_REGISTER_CONTROL;

	HAL_StatusTypeDef status = HAL_I2C_Mem_Write(&hi2c1, TSL2561_ADDRESS, command_code, I2C_MEMADD_SIZE_8BIT, tx_data, 1, 100);

	if (status == HAL_OK){
		toggle_green();
	} else {
		toggle_blue();
	}
}

/**************************************************************************/
/*!
    Reads the device ID
    TSL2561T/FN/CL should return 0x50
*/
/**************************************************************************/
uint8_t readID (void)
{
	uint8_t command_code = TSL2561_COMMAND_BIT | TSL2561_REGISTER_ID;

	HAL_StatusTypeDef statusRx = HAL_I2C_Mem_Read(&hi2c1, TSL2561_ADDRESS, command_code, I2C_MEMADD_SIZE_8BIT, rx_data, 1, 100);

	if (statusRx == HAL_OK){
		toggle_green();
		return rx_data[0];
	} else {
		toggle_blue();
		return FALSE;
	}
}

/**************************************************************************/
/*!
    Initializes the device checking if it's properly connected
    Returns TRUE if the sensor is found and responsive, FALSE otherwise
*/
/**************************************************************************/
uint8_t tsl2561_init (void)
{
	/* Read the REGISTER_ID to check the connection */
	uint8_t id = readID();

	if (id != TSL2561_ID_VALUE) {
		return FALSE;
	}

	tsl2561Initialized = TRUE;

	/* Set the default sensor values */
	tsl2561AutoGain = FALSE;
	tsl2561IntegrationTime = TSL2561_INTEGRATIONTIME_DEFAULT;
	tsl2561Gain = TSL2561_GAIN_DEFAULT;
	setIntegrationTime(tsl2561IntegrationTime);
	setGain(tsl2561Gain);

	/* Reduce consumption */
	disable();

	return TRUE;
}

/**************************************************************************/
/*!
    Enables or disables the auto-gain settings when reading
    data from the sensor
    Set to true to enable, false to disable
*/
/**************************************************************************/
void tsl2561_enableAutoRange(uint8_t enable)
{
	tsl2561AutoGain = enable;
}

/**************************************************************************/
/*!
    Sets the integration time to one of its three possible values
*/
/**************************************************************************/
void setIntegrationTime (tsl2561IntegrationTime_t time)
{
	/* Init the device if it's not initialized yet */
	if (tsl2561Initialized == FALSE) {
		tsl2561_init();
	}

	/* Wake up */
	enable();

	/* When setting the integration time, the gain must also be written */
	tx_data[0] = time | tsl2561Gain;	// Their values are in different nibbles and don't overlap
	uint8_t command_code = TSL2561_COMMAND_BIT | TSL2561_REGISTER_TIMING;

	HAL_I2C_Mem_Write(&hi2c1, TSL2561_ADDRESS, command_code, I2C_MEMADD_SIZE_8BIT, tx_data, 1, 100);

	/* Update variable */
	tsl2561IntegrationTime = time;

	/* Reduce consumption */
	disable();
}

/**************************************************************************/
/*!
    Sets the gain to one of its two possible values
*/
/**************************************************************************/
void setGain (tsl2561Gain_t gain)
{
	/* Init the device if it's not initialized yet */
	if (tsl2561Initialized == FALSE) {
		tsl2561_init();
	}

	/* Wake up */
	enable();

	/* When setting the gain, the integration time must also be written */
	tx_data[0] = tsl2561IntegrationTime | gain;	// Their values are in different nibbles and don't overlap
	uint8_t command_code = TSL2561_COMMAND_BIT | TSL2561_REGISTER_TIMING;

	HAL_I2C_Mem_Write(&hi2c1, TSL2561_ADDRESS, command_code, I2C_MEMADD_SIZE_8BIT, tx_data, 1, 100);

	/* Update variable */
	tsl2561Gain = gain;

	/* Reduce consumption */
	disable();
}

/**************************************************************************/
/*!
    @brief  Gets the broadband (mixed lighting IR+visible) and IR only
            values from the TSL2561, adjusting gain if auto-gain is enabled
    @param  broadband: Pointer to a uint16_t we will fill with a sensor
                      reading from the IR+visible light diode.
    @param  ir: Pointer to a uint16_t we will fill with a sensor the
               IR-only light diode.
*/
/**************************************************************************/
void getLuminosity (uint16_t *broadband, uint16_t *ir)
{
	uint8_t valid = FALSE;

	/* Init the device if it's not initialized yet */
	if (tsl2561Initialized == FALSE) {
		tsl2561_init();
	}

	/* If AutoGain is disabled get a single reading and continue */
	if(tsl2561AutoGain == FALSE) {
		getData (broadband, ir);
		return;
	}

	/* Read data until we find a valid range */
	uint8_t autogainCheck = FALSE;

	do {
		uint16_t broad, infra;
		uint16_t hi, lo;
		tsl2561IntegrationTime_t it = tsl2561IntegrationTime;

		/* Get the hi/low threshold for the current integration time */
		switch(it) {
			case TSL2561_INTEGRATIONTIME_13MS:
				hi = TSL2561_AGC_THI_13MS;
				lo = TSL2561_AGC_TLO_13MS;
				break;

			case TSL2561_INTEGRATIONTIME_101MS:
				hi = TSL2561_AGC_THI_101MS;
				lo = TSL2561_AGC_TLO_101MS;
				break;

			case TSL2561_INTEGRATIONTIME_402MS:
			default:
				hi = TSL2561_AGC_THI_402MS;
				lo = TSL2561_AGC_TLO_402MS;
				break;
		}

		getData(&broad, &infra);

		/* Run an auto-gain check if we haven't already done so ... */
		if (autogainCheck == FALSE) {

			if ((broad < lo) && (tsl2561Gain == TSL2561_GAIN_1X)) {
				/* Increase the gain to 16x and try again */
				setGain(TSL2561_GAIN_16X);

				/* Drop the previous conversion results */
				getData(&broad, &infra);

				/* Set a flag to indicate we've adjusted the gain */
				autogainCheck = TRUE;
			}

			else if ((broad > hi) && (tsl2561Gain == TSL2561_GAIN_16X)) {
				/* Drop gain to 1x and try again */
				setGain(TSL2561_GAIN_1X);

				/* Drop the previous conversion results */
				getData(&broad, &infra);

				/* Set a flag to indicate we've adjusted the gain */
				autogainCheck = TRUE;
			}

			else {
				/* Nothing to look at here, keep moving ....
				Reading is either valid, or we're already at the chips limits */
				*broadband = broad;
				*ir = infra;
				valid = TRUE;
			}

		}

		else {
			/* If we've already adjusted the gain once, just return the new results.
			This avoids endless loops where a value is at one extreme pre-gain,
			and at the other extreme post-gain */
			*broadband = broad;
			*ir = infra;
			valid = TRUE;
		}

	} while (valid == FALSE);

}

/**************************************************************************/
/*!
    Function to read luminosity on both channels
*/
/**************************************************************************/
void getData (uint16_t *broadband, uint16_t *ir)
{
  /* Wake up */
  enable();

  /* Wait x ms for the ADC to complete its operation */
  switch (tsl2561IntegrationTime)
  {
    case TSL2561_INTEGRATIONTIME_13MS:
      HAL_Delay(TSL2561_DELAY_INTTIME_13MS);  // KTOWN: Was 14ms
      break;

    case TSL2561_INTEGRATIONTIME_101MS:
      HAL_Delay(TSL2561_DELAY_INTTIME_101MS); // KTOWN: Was 102ms
      break;

    case TSL2561_INTEGRATIONTIME_402MS:
    default:
      HAL_Delay(TSL2561_DELAY_INTTIME_402MS); // KTOWN: Was 403ms
      break;
  }

  /* Reads a two byte value from channel 0 (visible + infrared) */
  rx_data[0] = 0xAA;
  rx_data[1] = 0xBB;
  uint8_t command_code = TSL2561_COMMAND_BIT | TSL2561_WORD_BIT | TSL2561_REGISTER_CHAN0_LOW;
  HAL_StatusTypeDef statusRx = HAL_I2C_Mem_Read(&hi2c1, TSL2561_ADDRESS, command_code, I2C_MEMADD_SIZE_8BIT, rx_data, 2, 100);
  // rx_data[0] has the low byte and rx_data[1] has the high byte
  broadband[0] = (rx_data[1] << 8) | rx_data[0];

  /* Reads a two byte value from channel 1 (infrared) */
  command_code = TSL2561_COMMAND_BIT | TSL2561_WORD_BIT | TSL2561_REGISTER_CHAN1_LOW;
  statusRx = HAL_I2C_Mem_Read(&hi2c1, TSL2561_ADDRESS, command_code, I2C_MEMADD_SIZE_8BIT, rx_data, 2, 100);
  ir[0] = (rx_data[1] << 8) | rx_data[0];

  if (statusRx == HAL_OK){
	  toggle_green();
  } else {
	  toggle_blue();
  }

  /* Turn the device off to save power */
  disable();
}

/**************************************************************************/
/*!
    @brief  Converts the raw sensor values to the standard SI lux equivalent.
    @param  broadband: The 16-bit sensor reading from the IR+visible light diode.
    @param  ir: The 16-bit sensor reading from the IR-only light diode.
    @returns The integer Lux value we calculate.
             Returns 0 if the sensor is saturated and the values are
             unreliable, or 65536 if the sensor is saturated.
*/
/**************************************************************************/
uint32_t calculateLux(uint16_t broadband, uint16_t ir)
{
	unsigned long chScale;
	unsigned long channel1;
	unsigned long channel0;

	/* Make sure the sensor isn't saturated! */
	uint16_t clipThreshold;
	switch (tsl2561IntegrationTime) {
		case TSL2561_INTEGRATIONTIME_13MS:
			clipThreshold = TSL2561_CLIPPING_13MS;
			break;

		case TSL2561_INTEGRATIONTIME_101MS:
			clipThreshold = TSL2561_CLIPPING_101MS;
			break;

		case TSL2561_INTEGRATIONTIME_402MS:
		default:
			clipThreshold = TSL2561_CLIPPING_402MS;
			break;
	}

	/* Return 65536 lux if the sensor is saturated */
	if ((broadband > clipThreshold) || (ir > clipThreshold)) {
		return 65536;
	}

	/* Get the correct scale depending on the integration time */
	switch (tsl2561IntegrationTime) {
		case TSL2561_INTEGRATIONTIME_13MS:
			chScale = TSL2561_LUX_CHSCALE_TINT0;
			break;

		case TSL2561_INTEGRATIONTIME_101MS:
			chScale = TSL2561_LUX_CHSCALE_TINT1;
			break;

		case TSL2561_INTEGRATIONTIME_402MS:	 /* No scaling ... integration time = 402ms */
		default:
			chScale = (1 << TSL2561_LUX_CHSCALE);
			break;
	}

	/* Scale if gain is not 16x */
	if (tsl2561Gain == TSL2561_GAIN_1X) {
		chScale = chScale << 4;	// Scale 1X to 16X
	}

	/* Scale the channel values */
	channel0 = (broadband * chScale) >> TSL2561_LUX_CHSCALE;
	channel1 = (ir * chScale) >> TSL2561_LUX_CHSCALE;

	/* Find the ratio of the channel values (Channel1/Channel0) */
	unsigned long ratio1 = 0;
	if (channel0 != 0) {	// Protect against divide by 0
		ratio1 = (channel1 << (TSL2561_LUX_RATIOSCALE+1)) / channel0;
	}

	/* Round the ratio value */
	unsigned long ratio = (ratio1 + 1) >> 1;

	unsigned int b, m;

	#ifdef TSL2561_PACKAGE_CS
		if ((ratio >= 0) && (ratio <= TSL2561_LUX_K1C)) {
			b = TSL2561_LUX_B1C;
			m = TSL2561_LUX_M1C;
		}
		else if (ratio <= TSL2561_LUX_K2C) {
			b = TSL2561_LUX_B2C;
			m = TSL2561_LUX_M2C;
		}
		else if (ratio <= TSL2561_LUX_K3C) {
			b = TSL2561_LUX_B3C;
			m = TSL2561_LUX_M3C;
		}
		else if (ratio <= TSL2561_LUX_K4C) {
			b = TSL2561_LUX_B4C;
			m = TSL2561_LUX_M4C;
		}
		else if (ratio <= TSL2561_LUX_K5C) {
			b = TSL2561_LUX_B5C;
			m = TSL2561_LUX_M5C;
		}
		else if (ratio <= TSL2561_LUX_K6C) {
			b = TSL2561_LUX_B6C;
			m = TSL2561_LUX_M6C;
		}
		else if (ratio <= TSL2561_LUX_K7C) {
			b = TSL2561_LUX_B7C;
			m = TSL2561_LUX_M7C;
		}
		else if (ratio > TSL2561_LUX_K8C) {
			b = TSL2561_LUX_B8C;
			m = TSL2561_LUX_M8C;
		}
	#else	// T, FN or CL package
		if ((ratio >= 0) && (ratio <= TSL2561_LUX_K1T)) {
			b = TSL2561_LUX_B1T;
			m = TSL2561_LUX_M1T;
		}
		else if (ratio <= TSL2561_LUX_K2T) {
			b = TSL2561_LUX_B2T;
			m = TSL2561_LUX_M2T;
		}
		else if (ratio <= TSL2561_LUX_K3T) {
			b = TSL2561_LUX_B3T;
			m = TSL2561_LUX_M3T;
		}
		else if (ratio <= TSL2561_LUX_K4T) {
			b = TSL2561_LUX_B4T;
			m = TSL2561_LUX_M4T;
		}
		else if (ratio <= TSL2561_LUX_K5T) {
			b = TSL2561_LUX_B5T;
			m = TSL2561_LUX_M5T;
		}
		else if (ratio <= TSL2561_LUX_K6T) {
			b = TSL2561_LUX_B6T;
			m = TSL2561_LUX_M6T;
		}
		else if (ratio <= TSL2561_LUX_K7T) {
			b = TSL2561_LUX_B7T;
			m = TSL2561_LUX_M7T;
		}
		else if (ratio > TSL2561_LUX_K8T) {
			b = TSL2561_LUX_B8T;
			m = TSL2561_LUX_M8T;
		}
	#endif

	unsigned long temp;
	temp = ((channel0 * b) - (channel1 * m));

	/* Do not allow a negative lux value */
	if (temp < 0) {
		temp = 0;
	}

	/* Round lsb (2^(LUX_SCALE-1)) */
	temp += (1 << (TSL2561_LUX_LUXSCALE-1));

	/* Strip off fractional portion */
	uint32_t lux = temp >> TSL2561_LUX_LUXSCALE;

	/* Signal I2C had no errors */
	return lux;
}
