/*
 * rain_sensor.c
 *
 *  Created on: 28 may. 2019
 *      Author: albarc
 */

/* Includes ------------------------------------------------------------------*/
#include "../Inc/rain_sensor.h"

/* Variables ------------------------------------------------------------------*/

/**************************************************************************/
/*!
    Inits rain sensor

    Starts ADC module
*/
/**************************************************************************/
void rainSensor_init (void)
{
	HAL_ADC_Start(&hadc);
}

/**************************************************************************/
/*!
    Reads rain level

    Returns an integer value from 0 (no rain) to 7 (maximum rain)
    as read from the analog pin of SN-RAIN-MODULE
*/
/**************************************************************************/
uint16_t readRainLevel (void)
{
	/* Read analog value */
	uint32_t adc_val = HAL_ADC_GetValue(&hadc);
	uint16_t rain_level = 0;

	if (adc_val <= MIN_LEVEL) {
		rain_level = 7;
	} else if ((MIN_LEVEL < adc_val) && (adc_val <= (MIN_LEVEL + STEP_SIZE))) {
		rain_level = 6;
	} else if (((MIN_LEVEL + STEP_SIZE) > adc_val) && (adc_val <= (MIN_LEVEL + 2*STEP_SIZE))) {
		rain_level = 5;
	} else if (((MIN_LEVEL + 2*STEP_SIZE) > adc_val) && (adc_val <= (MIN_LEVEL + 3*STEP_SIZE))) {
		rain_level = 4;
	} else if (((MIN_LEVEL + 3*STEP_SIZE) > adc_val) && (adc_val <= (MIN_LEVEL + 4*STEP_SIZE))) {
		rain_level = 3;
	} else if (((MIN_LEVEL + 4*STEP_SIZE) > adc_val) && (adc_val <= (MIN_LEVEL + 5*STEP_SIZE))) {
		rain_level = 2;
	} else if (((MIN_LEVEL + 5*STEP_SIZE) > adc_val) && (adc_val <= MAX_LEVEL)) {
		rain_level = 1;
	} else if (adc_val > MAX_LEVEL) {
		rain_level = 0;
	}

	return rain_level;
}
