/*
 * nbiot_bg96.c
 *
 *  Created on: 12 dic. 2018
 *      Author: albarc
 */

/* Includes ------------------------------------------------------------------*/
#include "../Inc/nbiot_bg96.h"

/* Variables ------------------------------------------------------------------*/


/**************************************************************************/
/*!
    Enables registration status updates

    AT+CREG=2
*/
/**************************************************************************/
void bg96_network_registration_status (void)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = bg96_create_one_param_AT_command(NWK_REGISTRATION_STATUS, NWK_REGISTRATION_PARAM, &at_command);

	/* Send it via UART*/
	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	/* Free allocated space */
	free(at_command);
}

/**************************************************************************/
/*!
    Enables registration status updates

    AT+CGREG=2
*/
/**************************************************************************/
void bg96_network_registration_status_2 (void)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = bg96_create_one_param_AT_command(NWK_REGISTRATION_STATUS_2, NWK_REGISTRATION_PARAM, &at_command);

	/* Send it via UART*/
	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	/* Free allocated space */
	free(at_command);
}

/**************************************************************************/
/*!
    Enables EPS registration status updates

    AT+CEREG=2
*/
/**************************************************************************/
void bg96_eps_network_registration_status (void)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = bg96_create_one_param_AT_command(EPS_NWK_REGISTRATION_STATUS, NWK_REGISTRATION_PARAM, &at_command);

	/* Send it via UART*/
	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	/* Free allocated space */
	free(at_command);
}


/**************************************************************************/
/*!
    Puts the device in full functionality mode

    AT+CFUN=1
*/
/**************************************************************************/
void bg96_set_full_functionality (void)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = bg96_create_one_param_AT_command(SET_FUNCTIONALITY, SET_FULL_FUNCTIONALITY_PARAM, &at_command);

	/* Send it via UART*/
	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	/* Free allocated space */
	free(at_command);
}

/**************************************************************************/
/*!
    Puts the device in minimum functionality mode

    AT+CFUN=0
*/
/**************************************************************************/
void bg96_set_minimum_functionality (void)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = bg96_create_one_param_AT_command(SET_FUNCTIONALITY, SET_MIN_FUNCTIONALITY_PARAM, &at_command);

	/* Send it via UART*/
	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	/* Free allocated space */
	free(at_command);
}


/**************************************************************************/
/*!
    Connects to the specified network

    AT+COPS=1,2,"21401"
*/
/**************************************************************************/
void bg96_connect_to_network (void)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = bg96_create_three_param_AT_command(OPERATOR_SELECTION, OPERATOR_SELECTION_PARAM_1,
			OPERATOR_SELECTION_PARAM_2, VODAFONE_NETWORK_CODE, &at_command);

	/* Send it via UART*/
	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	/* Free allocated space */
	free(at_command);
}

/**************************************************************************/
/*!
    Get the assigned IP address

    AT+CGPADDR
*/
/**************************************************************************/
void bg96_get_ip_address (void)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = bg96_create_non_param_AT_command(GET_ASSIGNED_IP_ADDRESS, &at_command);

	/* Send it via UART*/
	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	/* Free allocated space */
	free(at_command);
}

/**************************************************************************/
/*!
    Open a UDP socket

    AT+QIOPEN=1,0,"UDP SERVICE","35.237.66.97",8888,16666
*/
/**************************************************************************/
void bg96_open_udp_socket (void)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = bg96_create_six_param_AT_command(OPEN_SOCKET, OPEN_SOCKET_CONTEXT_ID,
			OPEN_SOCKET_CONNECT_ID, OPEN_SOCKET_TYPE, OPEN_SOCKET_REMOTE_IP, OPEN_SOCKET_REMOTE_PORT,
			OPEN_SOCKET_OWN_LISTEN_PORT, &at_command);

	/* Send it via UART*/
	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	/* Free allocated space */
	free(at_command);
}

/**************************************************************************/
/*!
    Sends a UDP message (in plain text, second argument is the message length)

    AT+QISEND=0,39,"35.237.66.97",8888
*/
/**************************************************************************/
void bg96_send_udp_message (char* message)
{
	/* Calculate message length */
	uint16_t message_length = strlen(message);
	char* message_length_field = malloc(4);	// Como máximo 1460 bytes (4 cifras)
	sprintf(message_length_field, "%d", message_length);

	/* Calculate command length */
	uint16_t command_length = strlen(AT_PREFIX) + strlen(SEND_DATA) + strlen(EQUAL_SIGN)
			+ strlen(OPEN_SOCKET_CONNECT_ID) + strlen(COMMA_SIGN) + strlen(message_length_field)
			+ strlen(COMMA_SIGN) + strlen(OPEN_SOCKET_REMOTE_IP) + strlen(COMMA_SIGN)
			+ strlen(OPEN_SOCKET_REMOTE_PORT) + strlen(END_OF_COMMAND);

	/* Build command */
	char *at_command = malloc(command_length + 1);	// +1 for '\0' string termination
	strcpy(at_command, AT_PREFIX);
	strcat(at_command, SEND_DATA);
	strcat(at_command, EQUAL_SIGN);
	strcat(at_command, OPEN_SOCKET_CONNECT_ID);
	strcat(at_command, COMMA_SIGN);
	strcat(at_command, message_length_field);
	strcat(at_command, COMMA_SIGN);
	strcat(at_command, OPEN_SOCKET_REMOTE_IP);
	strcat(at_command, COMMA_SIGN);
	strcat(at_command, OPEN_SOCKET_REMOTE_PORT);
	strcat(at_command, END_OF_COMMAND);

	/* Send it via UART*/
	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	/* Wait for a second */
	HAL_Delay(100);

	/* Send message */
	HAL_UART_Transmit(&huart1, (uint8_t*) message, message_length, 0xFFFF);

	/* Send CR+LF so next command is not appended to this */
	printf("\r\n");

	/* Free allocated space */
	free(message_length_field);
	free(at_command);
}

/**************************************************************************/
/*!
    Close UDP socket

    AT+QICLOSE=0
*/
/**************************************************************************/
void bg96_close_udp_socket (void)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = bg96_create_one_param_AT_command (CLOSE_SOCKET, OPEN_SOCKET_CONNECT_ID, &at_command);

	/* Send it via UART*/
	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	/* Free allocated space */
	free(at_command);
}

/**************************************************************************/
/*!
    Sends all connection commands at once

    AT+CFUN=1;+COPS=1,2,"21401";
*/
/**************************************************************************/
void bg96_all_connection_commands (void)
{
	/* Create AT command */
	uint8_t command_length = strlen(AT_PREFIX)
					+ strlen(SET_FUNCTIONALITY)	+ strlen(EQUAL_SIGN) + strlen(SET_FULL_FUNCTIONALITY_PARAM)
					+ strlen(SEMICOLON_SIGN) + strlen(PLUS_SIGN) + strlen(OPERATOR_SELECTION) + strlen(EQUAL_SIGN)
					+ strlen(OPERATOR_SELECTION_PARAM_1) + strlen(COMMA_SIGN) + strlen(OPERATOR_SELECTION_PARAM_2)
					+ strlen(COMMA_SIGN) + strlen(VODAFONE_NETWORK_CODE) + strlen(END_OF_COMMAND);

	/* Build command */
	char *at_command = malloc(command_length + 1);	// +1 for '\0' string termination
	strcpy(at_command, AT_PREFIX);
	strcat(at_command, SET_FUNCTIONALITY);
	strcat(at_command, EQUAL_SIGN);
	strcat(at_command, SET_FULL_FUNCTIONALITY_PARAM);
	strcat(at_command, SEMICOLON_SIGN);

	strcat(at_command, PLUS_SIGN);
	strcat(at_command, OPERATOR_SELECTION);
	strcat(at_command, EQUAL_SIGN);
	strcat(at_command, OPERATOR_SELECTION_PARAM_1);
	strcat(at_command, COMMA_SIGN);
	strcat(at_command, OPERATOR_SELECTION_PARAM_2);
	strcat(at_command, COMMA_SIGN);
	strcat(at_command, VODAFONE_NETWORK_CODE);

	strcat(at_command, END_OF_COMMAND);

	/* Send it via UART*/
	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	// Free allocated space
	free(at_command);
}

/**************************************************************************/
/*!
    Enables GPS

    AT+QGPS=1
*/
/**************************************************************************/
void bg96_enableGPS (void)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = bg96_create_one_param_AT_command (ENABLE_GPS, ENABLE_GPS_PARAM, &at_command);

	/* Send it via UART*/
	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	/* Free allocated space */
	free(at_command);
}

/**************************************************************************/
/*!
    Requests GPS value and reads the response

    AT+QGPSLOC=2
*/
/**************************************************************************/
void bg96_requestGPSlocation (double *lat, double *lon)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = bg96_create_one_param_AT_command (REQUEST_GPS_LOCATION, REQUEST_GPS_LOCATION_PARAM, &at_command);

	/* Send it via UART*/
	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	/* Read response which contains ':' */
	char rx_buf[200];
	uint8_t read_byte = 0x00;
	int bytes_read = 0;
	uint8_t command_started = 0;
	uint8_t timeout_counter = 0;
	HAL_StatusTypeDef stat;
	while ((command_started == 0) || ((read_byte != '\r') && (read_byte != '\n'))) {
		stat = HAL_UART_Receive(&huart1, &read_byte, 1, 500);
		if(stat == HAL_TIMEOUT) {	// Avoid block if bg96 does not respond
			timeout_counter++;
			if(timeout_counter > 6) {
				break;
			}
		}
		rx_buf[bytes_read] = read_byte;
		bytes_read++;
		if(read_byte == ':') {
			command_started = 1;
		}
	}
	/* Extract latitude and longitude from the received string */
	//char prueba[] = "+QGPSLOC: 165336.0,40.45273,-3.72555,0.0";
	char *pch;
	char *latitude = malloc(10);
	char *longitude = malloc(10);
	int i = 0;

	//char *error = strstr(prueba, "ERROR");
	char *error = strstr(rx_buf, "ERROR");

	if(error == NULL) {	// If there was no error
		//pch = strtok (prueba," ,");
		pch = strtok (rx_buf," ,");
		while (pch != NULL)
		{
			if(i == 2) {	// latitude is located at the second position
				strcpy(latitude, pch);
			} else if (i == 3) {	// longitude is located at the third position
				strcpy(longitude, pch);
			}
			pch = strtok (NULL, " ,");
			i++;
		}

		/* Convert strings to double */
		if(latitude != NULL) {
			*lat = atof(latitude);
		} else {
			*lat = 0.0;
		}
		if(longitude != NULL) {
			*lon = atof(longitude);
		} else {
			*lon = 0.0;
		}
	} else {
		*lat = 0.0;
		*lon = 0.0;
	}

	/* Free allocated space */
	free(at_command);
	free(pch);
	free(latitude);
	free(longitude);
	free(error);
}

/**************************************************************************/
/*!
    Set up nb-iot module before sending messages
    Returns TRUE if a socket has been succesfully opened, FALSE otherwise
*/
/**************************************************************************/
uint8_t bg96_setup_nbiot_module (void)
{
	// Send dummy data to make sure nb-iot module receives next commands correctly
	printf("\r\n");

	// Send connection parameters
	bg96_all_connection_commands();
	HAL_Delay(WAIT_AFTER_CONNECT_MS);

	// Open socket
	bg96_open_udp_socket();
	//HAL_Delay(WAIT_AFTER_SOCKET_MS);

	// Return whether the socket was correctly opened
	/* Read response which contains '+' */
	char rx_buf[200];
	uint8_t read_byte = 0x00;
	int bytes_read = 0;
	uint8_t command_started = 0;
	uint8_t timeout_counter = 0;
	HAL_StatusTypeDef stat;
	while ((command_started == 0) || ((read_byte != '\r') && (read_byte != '\n'))) {
		stat = HAL_UART_Receive(&huart1, &read_byte, 1, 500);
		if(stat == HAL_TIMEOUT) {	// Avoid block if bg96 does not respond
			timeout_counter++;
			if(timeout_counter > 6) {
				break;
			}
		}
		rx_buf[bytes_read] = read_byte;
		bytes_read++;
		if(read_byte == '+') {
			command_started = 1;
		}
	}
	/* Response to AT+QIOPEN has the form +QIOPEN:<connectID>,<err>. <err> is 0 if success */
	//char prueba[] = "+QIOPEN: 0,0";
	char *pch = malloc(16);
	char *err = malloc(1);
	uint8_t socket_open = FALSE;
	int i = 0;

	//pch = strtok (prueba," ,");
	pch = strtok (rx_buf," ,");
	while (pch != NULL)
	{
		if(i == 2) {	// <err> is located at the second position
			strcpy(err, pch);
		}
		pch = strtok (NULL, " ,");
		i++;
	}

	/* Convert strings to int */
	if(err != NULL) {
		if (err[0] == '0') {
			socket_open = TRUE;
		} else {
			socket_open = FALSE;
		}
	} else {
		socket_open = FALSE;
	}

	/* Free allocated space */
	free(pch);
	free(err);

	/* Return socket open */
	return socket_open;
}

/**************************************************************************/
/*!
    Send nbiot message, hiding details to the main app
*/
/**************************************************************************/
void bg96_send_nbiot_message (char* message)
{
	bg96_send_udp_message(message);
}

/**************************************************************************/
/*!
    Enter sleep mode

    AT+QSCLK=1
*/
/**************************************************************************/
void bg96_enter_sleep_mode (void)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = bg96_create_one_param_AT_command (SLEEP_MODE, ENTER_SLEEP_MODE_PARAM,
			&at_command);

	/* Send it via UART*/
	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	/* Free allocated space */
	free(at_command);
}

/**************************************************************************/
/*!
    Exit sleep mode

    AT+QSCLK=0, WHILE DTR pin is driven LOW!!!!
*/
/**************************************************************************/
void bg96_exit_sleep_mode (void)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = bg96_create_one_param_AT_command (SLEEP_MODE, EXIT_SLEEP_MODE_PARAM,
			&at_command);

	/* Configure DTR pin as output */
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = BG96_DTR_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(BG96_DTR_GPIO_Port, &GPIO_InitStruct);

	/* Drive DTR pin low */
	HAL_GPIO_WritePin(BG96_DTR_GPIO_Port, BG96_DTR_Pin, GPIO_PIN_RESET);
	HAL_Delay(100);

	/* Send command via UART*/
	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	/* Put DTR pin back as open input */
	GPIO_InitStruct.Pin = BG96_DTR_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(BG96_DTR_GPIO_Port, &GPIO_InitStruct);

	/* Free allocated space */
	free(at_command);
}

/**************************************************************************/
/*!
    Power up the device by driving PWR_KEY pin high during 500 ms at least

*/
/**************************************************************************/
void bg96_powerup (void) {

	/* Drive PWR_KEY pin high */
	HAL_GPIO_WritePin(BG96_PWRKEY_GPIO_Port, BG96_PWRKEY_Pin, GPIO_PIN_SET);
	HAL_Delay(POWERUP_PULSE_WIDTH_MS);
	HAL_GPIO_WritePin(BG96_PWRKEY_GPIO_Port, BG96_PWRKEY_Pin, GPIO_PIN_RESET);

	/* Wait before further commands */
	HAL_Delay(WAIT_AFTER_POWERUP_MS);
}

/**************************************************************************/
/*!
    Reset the device by driving RESET pin high for 150 to 460 ms

*/
/**************************************************************************/
void bg96_reset (void) {

	/* Drive RESET pin high */
	HAL_GPIO_WritePin(BG96_PWRKEY_GPIO_Port, BG96_PWRKEY_Pin, GPIO_PIN_SET);
	HAL_Delay(RESET_PULSE_WIDTH_MS);
	HAL_GPIO_WritePin(BG96_PWRKEY_GPIO_Port, BG96_PWRKEY_Pin, GPIO_PIN_RESET);

	/* Wait before further commands */
	HAL_Delay(WAIT_AFTER_POWERUP_MS);
}

/**************************************************************************/
/*!
    Create AT command with no parameters: AT+CODE

    Returns resulting command in pointer 'at_command' passed as argument
*/
/**************************************************************************/
uint8_t bg96_create_non_param_AT_command (char* command_code, char** at_command)
{
	/* Create AT command */
	uint8_t length = strlen(AT_PREFIX) + strlen(command_code) + strlen(END_OF_COMMAND);
	*at_command = malloc(length + 1);	// +1 for '\0' string termination
	strcpy(*at_command, AT_PREFIX);
	strcat(*at_command, command_code);
	strcat(*at_command, END_OF_COMMAND);

	return length;
}

/**************************************************************************/
/*!
    Create AT command with one parameter: AT+CODE=PARAM

    Returns resulting command in pointer 'at_command' passed as argument
*/
/**************************************************************************/
uint8_t bg96_create_one_param_AT_command (char* command_code, char* command_param, char** at_command)
{
	/* Create AT command */
	uint8_t length = strlen(AT_PREFIX) + strlen(command_code) + strlen(EQUAL_SIGN)
			+ strlen(command_param) + strlen(END_OF_COMMAND);
	*at_command = malloc(length + 1);	// +1 for '\0' string termination
	strcpy(*at_command, AT_PREFIX);
	strcat(*at_command, command_code);
	strcat(*at_command, EQUAL_SIGN);
	strcat(*at_command, command_param);
	strcat(*at_command, END_OF_COMMAND);

	return length;
}

/**************************************************************************/
/*!
    Create AT command with three parameters: AT+CODE=PARAM,PARAM,PARAM

    Returns resulting command in pointer 'at_command' passed as argument
*/
/**************************************************************************/
uint8_t bg96_create_three_param_AT_command (char* command_code, char* command_param_1,
		char* command_param_2, char* command_param_3, char** at_command)
{
	/* Create AT command */
	uint8_t length = strlen(AT_PREFIX) + strlen(command_code) + strlen(EQUAL_SIGN)
			+ strlen(command_param_1) + strlen(COMMA_SIGN) + strlen(command_param_2) +strlen(COMMA_SIGN)
			+ strlen (command_param_3) + strlen(END_OF_COMMAND);
	*at_command = malloc(length + 1);	// +1 for '\0' string termination
	strcpy(*at_command, AT_PREFIX);
	strcat(*at_command, command_code);
	strcat(*at_command, EQUAL_SIGN);
	strcat(*at_command, command_param_1);
	strcat(*at_command, COMMA_SIGN);
	strcat(*at_command, command_param_2);
	strcat(*at_command, COMMA_SIGN);
	strcat(*at_command, command_param_3);
	strcat(*at_command, END_OF_COMMAND);

	return length;
}

/**************************************************************************/
/*!
    Create AT command with six parameters: AT+CODE=PARAM,PARAM,PARAM,PARAM,PARAM,PARAM

    Returns resulting command in pointer 'at_command' passed as argument
*/
/**************************************************************************/
uint8_t bg96_create_six_param_AT_command (char* command_code, char* command_param_1,
		char* command_param_2, char* command_param_3, char* command_param_4, char* command_param_5,
		char* command_param_6, char** at_command)
{
	/* Create AT command */
	uint8_t length = strlen(AT_PREFIX) + strlen(command_code) + strlen(EQUAL_SIGN)
			+ strlen(command_param_1) + strlen(COMMA_SIGN) + strlen(command_param_2) + strlen(COMMA_SIGN)
			+ strlen(command_param_3) + strlen(COMMA_SIGN) + strlen(command_param_4) + strlen(COMMA_SIGN)
			+ strlen(command_param_5) + strlen(COMMA_SIGN) + strlen(command_param_6) + strlen(END_OF_COMMAND);
	*at_command = malloc(length + 1);	// +1 for '\0' string termination
	strcpy(*at_command, AT_PREFIX);
	strcat(*at_command, command_code);
	strcat(*at_command, EQUAL_SIGN);
	strcat(*at_command, command_param_1);
	strcat(*at_command, COMMA_SIGN);
	strcat(*at_command, command_param_2);
	strcat(*at_command, COMMA_SIGN);
	strcat(*at_command, command_param_3);
	strcat(*at_command, COMMA_SIGN);
	strcat(*at_command, command_param_4);
	strcat(*at_command, COMMA_SIGN);
	strcat(*at_command, command_param_5);
	strcat(*at_command, COMMA_SIGN);
	strcat(*at_command, command_param_6);
	strcat(*at_command, END_OF_COMMAND);

	return length;
}
