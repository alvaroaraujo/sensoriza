/*
 * nbiot_bc95.c
 *
 *  Created on: 23 oct. 2018
 *      Author: albarc
 */

/* Includes ------------------------------------------------------------------*/
#include "../Inc/nbiot_bc95.h"

/* Variables ------------------------------------------------------------------*/


/**************************************************************************/
/*!
    Reboots the device

    AT+NRB
*/
/**************************************************************************/
void reboot (void)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = create_non_param_AT_command(REBOOT, &at_command);

	/* Send it via UART*/
	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	/* Free allocated space */
	free(at_command);
}

/**************************************************************************/
/*!
    Registers the device in the network

    AT+CEREG=2
*/
/**************************************************************************/
void network_register (void)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = create_one_param_AT_command(NWK_REGISTER, NWK_REGISTER_PARAM, &at_command);

	/* Send it via UART*/
	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	/* Free allocated space */
	free(at_command);
}

/**************************************************************************/
/*!
    Receive notifications if the device status changes

    AT+CSCON=1
*/
/**************************************************************************/
void enable_connection_status_messages (void)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = create_one_param_AT_command(CONNECTION_STATUS, CONNECTION_STATUS_PARAM, &at_command);

	/* Send it via UART*/
	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	/* Free allocated space */
	free(at_command);
}

/**************************************************************************/
/*!
    Puts the device in full functionality mode

    AT+CFUN=1
*/
/**************************************************************************/
void set_full_functionality (void)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = create_one_param_AT_command(SET_FUNCTIONALITY, SET_FUNCTIONALITY_PARAM, &at_command);

	/* Send it via UART*/
	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	/* Free allocated space */
	free(at_command);
}

/**************************************************************************/
/*!
    Sets the Packet Data Protocol (PDP) context

    AT+CGDCONT=0,"IP",""
*/
/**************************************************************************/
void set_pdp_context (void)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = create_three_param_AT_command(PDP_CONTEXT, SET_PDP_CONTEXT_PARAM_1,
			SET_PDP_CONTEXT_PARAM_2, SET_PDP_CONTEXT_PARAM_3, &at_command);

	/* Send it via UART*/
	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	/* Free allocated space */
	free(at_command);
}

/**************************************************************************/
/*!
    Gets the Packet Data Protocol (PDP) context

    AT+CGDCONT?
*/
/**************************************************************************/
void get_pdp_context (void)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = create_get_AT_command(PDP_CONTEXT, &at_command);

	/* Send it via UART*/
	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	/* Free allocated space */
	free(at_command);
}

/**************************************************************************/
/*!
    Connects to the specified network

    AT+COPS=1,2,"21401"
*/
/**************************************************************************/
void connect_to_network (void)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = create_three_param_AT_command(OPERATOR_SELECTION, OPERATOR_SELECTION_PARAM_1,
			OPERATOR_SELECTION_PARAM_2, VODAFONE_NETWORK_CODE, &at_command);

	/* Send it via UART*/
	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	/* Free allocated space */
	free(at_command);
}

/**************************************************************************/
/*!
    Get the assigned IP address

    AT+CGPADDR
*/
/**************************************************************************/
void get_ip_address (void)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = create_non_param_AT_command(GET_ASSIGNED_IP_ADDRESS, &at_command);

	/* Send it via UART*/
	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	/* Free allocated space */
	free(at_command);
}

/**************************************************************************/
/*!
    Open a UDP socket and return given reference in argument

    AT+NSOCR=DGRAM,17,16666,1
*/
/**************************************************************************/
void open_udp_socket (char** socket_reference)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = create_four_param_AT_command(CREATE_SOCKET, CREATE_SOCKET_PARAM_TYPE,
			CREATE_SOCKET_PARAM_PROTOCOL, CREATE_SOCKET_PARAM_LIST_PORT, CREATE_SOCKET_PARAM_REF, &at_command);

	/* Send it via UART*/
	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	/* Free allocated space */
	free(at_command);

	/* Wait for response */
	// HALUARTRECEIVEWITHTIMEOUT!!

	/* Return socket reference number in argument */
	*socket_reference = malloc(2);
	strcpy(*socket_reference, "0");
}

/**************************************************************************/
/*!
    Sends a UDP message (message should be in hex form)

    AT+NSOST=0,35.237.66.97,8888,2,3333
*/
/**************************************************************************/
void send_udp_message (char* socket_reference, char* ip_address, char* port_number, char* message)
{
	/* Convert message to hex */
	char* hex_message = malloc(1024); // Como máx 512 bytes, y cada uno ocupa 2 caracteres hex
	uint16_t hex_length = string_to_hex(message, &hex_message);

	/* Calculate message length */
	uint16_t message_length = hex_length/2;	// La mitad de bytes que de caracteres hex
	char* message_length_field = malloc(3);	// Como máximo 512 bytes (3 cifras)
	sprintf(message_length_field, "%d", message_length);

	/* Calculate command length */
	uint16_t command_length = strlen(AT_PREFIX) + strlen(SEND_UDP_DATAGRAM) + strlen(EQUAL_SIGN)
			+ strlen(socket_reference) + strlen(COMMA_SIGN) + strlen(ip_address) + strlen(COMMA_SIGN)
			+ strlen(port_number) + strlen(COMMA_SIGN) + strlen(message_length_field) + strlen(COMMA_SIGN)
			+ hex_length + strlen(END_OF_COMMAND);

	/* Build command */
	char *at_command = malloc(command_length + 1);	// +1 for '\0' string termination
	strcpy(at_command, AT_PREFIX);
	strcat(at_command, SEND_UDP_DATAGRAM);
	strcat(at_command, EQUAL_SIGN);
	strcat(at_command, socket_reference);
	strcat(at_command, COMMA_SIGN);
	strcat(at_command, ip_address);
	strcat(at_command, COMMA_SIGN);
	strcat(at_command, port_number);
	strcat(at_command, COMMA_SIGN);
	strcat(at_command, message_length_field);
	strcat(at_command, COMMA_SIGN);
	strcat(at_command, hex_message);
	strcat(at_command, END_OF_COMMAND);

	/* Send it via UART*/
	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	/* Free allocated space */
	free(hex_message);
	free(message_length_field);
	free(at_command);
}

/**************************************************************************/
/*!
    Close the UDP socket passed in argument

    AT+NSOCL=0
*/
/**************************************************************************/
void close_udp_socket (char* socket_reference)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = create_one_param_AT_command (CLOSE_SOCKET, socket_reference, &at_command);

	/* Send it via UART*/
	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	/* Free allocated space */
	free(at_command);
	free(socket_reference);
}

/**************************************************************************/
/*!
    Sends all connection commands at once

    AT+CEREG=2;+CSCON=1;+CFUN=1;+CGDCONT=0,"IP","";+COPS=1,2,"21401";
*/
/**************************************************************************/
void all_connection_commands (void)
{
	/* Create AT command */
	uint8_t command_length = strlen(AT_PREFIX)
					+ strlen(NWK_REGISTER) + strlen(EQUAL_SIGN)	+ strlen(NWK_REGISTER_PARAM) + strlen(SEMICOLON_SIGN)
					+ strlen(PLUS_SIGN) + strlen(CONNECTION_STATUS)	+ strlen(EQUAL_SIGN)
					+ strlen(CONNECTION_STATUS_PARAM) + strlen(SEMICOLON_SIGN)
					+ strlen(PLUS_SIGN) + strlen(SET_FUNCTIONALITY)	+ strlen(EQUAL_SIGN)
					+ strlen(SET_FUNCTIONALITY_PARAM) + strlen(SEMICOLON_SIGN)
					+ strlen(PLUS_SIGN) + strlen(PDP_CONTEXT) + strlen(EQUAL_SIGN)
					+ strlen(SET_PDP_CONTEXT_PARAM_1) + strlen(COMMA_SIGN) + strlen(SET_PDP_CONTEXT_PARAM_2)
					+ strlen(COMMA_SIGN) + strlen(SET_PDP_CONTEXT_PARAM_3) + strlen(SEMICOLON_SIGN)
					+ strlen(PLUS_SIGN) + strlen(OPERATOR_SELECTION) + strlen(EQUAL_SIGN)
					+ strlen(OPERATOR_SELECTION_PARAM_1) + strlen(COMMA_SIGN) + strlen(OPERATOR_SELECTION_PARAM_2)
					+ strlen(COMMA_SIGN) + strlen(VODAFONE_NETWORK_CODE) + strlen(SEMICOLON_SIGN)
					+ strlen(END_OF_COMMAND);

	/* Build command */
	char *at_command = malloc(command_length + 1);	// +1 for '\0' string termination
	strcpy(at_command, AT_PREFIX);
	strcat(at_command, NWK_REGISTER);
	strcat(at_command, EQUAL_SIGN);
	strcat(at_command, NWK_REGISTER_PARAM);
	strcat(at_command, SEMICOLON_SIGN);

	strcat(at_command, PLUS_SIGN);
	strcat(at_command, CONNECTION_STATUS);
	strcat(at_command, EQUAL_SIGN);
	strcat(at_command, CONNECTION_STATUS_PARAM);
	strcat(at_command, SEMICOLON_SIGN);

	strcat(at_command, PLUS_SIGN);
	strcat(at_command, SET_FUNCTIONALITY);
	strcat(at_command, EQUAL_SIGN);
	strcat(at_command, SET_FUNCTIONALITY_PARAM);
	strcat(at_command, SEMICOLON_SIGN);

	strcat(at_command, PLUS_SIGN);
	strcat(at_command, PDP_CONTEXT);
	strcat(at_command, EQUAL_SIGN);
	strcat(at_command, SET_PDP_CONTEXT_PARAM_1);
	strcat(at_command, COMMA_SIGN);
	strcat(at_command, SET_PDP_CONTEXT_PARAM_2);
	strcat(at_command, COMMA_SIGN);
	strcat(at_command, SET_PDP_CONTEXT_PARAM_3);
	strcat(at_command, SEMICOLON_SIGN);

	strcat(at_command, PLUS_SIGN);
	strcat(at_command, OPERATOR_SELECTION);
	strcat(at_command, EQUAL_SIGN);
	strcat(at_command, OPERATOR_SELECTION_PARAM_1);
	strcat(at_command, COMMA_SIGN);
	strcat(at_command, OPERATOR_SELECTION_PARAM_2);
	strcat(at_command, COMMA_SIGN);
	strcat(at_command, VODAFONE_NETWORK_CODE);
	strcat(at_command, SEMICOLON_SIGN);

	strcat(at_command, END_OF_COMMAND);

	/* Send it via UART*/
	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	// Free allocated space
	free(at_command);
}

/**************************************************************************/
/*!
    Set up nb-iot module before sending messages
*/
/**************************************************************************/
void bc95_setup_nbiot_module (char** socket_reference)
{
	// Send dummy data to make sure nb-iot module receives next commands correctly
	printf("\r\n");

	// Reboot module
	reboot();
	HAL_Delay(WAIT_AFTER_REBOOT_MS);

	// Send connection parameters
	all_connection_commands();
	HAL_Delay(WAIT_AFTER_CONNECT_MS);

	// Open socket
	open_udp_socket(socket_reference);
	HAL_Delay(WAIT_AFTER_SOCKET_MS);
}

/**************************************************************************/
/*!
    Send nbiot message, hiding details to the main app
*/
/**************************************************************************/
void bc95_send_nbiot_message (char* socket_reference, char* message)
{
	send_udp_message(socket_reference, SEND_UDP_DATAGRAM_PARAM_IP, SEND_UDP_DATAGRAM_PARAM_PORT, message);
}

/**************************************************************************/
/*!
    Convert string to hex
*/
/**************************************************************************/
uint16_t string_to_hex (char* input, char** output)
{
	char* aux_in = malloc(512);
	char* aux_out = malloc(512);
	int i, j;

	strcpy(aux_in, input);
	memset(aux_out, 0, 512);	// Fill aux_out with nulls

	for(i=0, j=0; i < strlen(aux_in); i++,j+=2) {
		sprintf((char*)aux_out+j,"%02X",aux_in[i]);
	}
	aux_out[j] = '\0';
	strcpy(*output, aux_out);

	// Free allocated space
	free(aux_in);
	free(aux_out);

	return strlen(*output);
}

/**************************************************************************/
/*!
    Create get AT command: AT+CODE?

    Returns resulting command in pointer 'at_command' passed as argument
*/
/**************************************************************************/
uint8_t create_get_AT_command (char* command_code, char** at_command)
{
	/* Create AT command */
	uint8_t length = strlen(AT_PREFIX) + strlen(command_code) + strlen(QUESTION_MARK) + strlen(END_OF_COMMAND);
	*at_command = malloc(length + 1);	// +1 for '\0' string termination
	strcpy(*at_command, AT_PREFIX);
	strcat(*at_command, command_code);
	strcat(*at_command, QUESTION_MARK);
	strcat(*at_command, END_OF_COMMAND);

	return length;
}

/**************************************************************************/
/*!
    Create AT command with no parameters: AT+CODE

    Returns resulting command in pointer 'at_command' passed as argument
*/
/**************************************************************************/
uint8_t create_non_param_AT_command (char* command_code, char** at_command)
{
	/* Create AT command */
	uint8_t length = strlen(AT_PREFIX) + strlen(command_code) + strlen(END_OF_COMMAND);
	*at_command = malloc(length + 1);	// +1 for '\0' string termination
	strcpy(*at_command, AT_PREFIX);
	strcat(*at_command, command_code);
	strcat(*at_command, END_OF_COMMAND);

	return length;
}

/**************************************************************************/
/*!
    Create AT command with one parameter: AT+CODE=PARAM

    Returns resulting command in pointer 'at_command' passed as argument
*/
/**************************************************************************/
uint8_t create_one_param_AT_command (char* command_code, char* command_param, char** at_command)
{
	/* Create AT command */
	uint8_t length = strlen(AT_PREFIX) + strlen(command_code) + strlen(EQUAL_SIGN)
			+ strlen(command_param) + strlen(END_OF_COMMAND);
	*at_command = malloc(length + 1);	// +1 for '\0' string termination
	strcpy(*at_command, AT_PREFIX);
	strcat(*at_command, command_code);
	strcat(*at_command, EQUAL_SIGN);
	strcat(*at_command, command_param);
	strcat(*at_command, END_OF_COMMAND);

	return length;
}

/**************************************************************************/
/*!
    Create AT command with three parameters: AT+CODE=PARAM,PARAM,PARAM

    Returns resulting command in pointer 'at_command' passed as argument
*/
/**************************************************************************/
uint8_t create_three_param_AT_command (char* command_code, char* command_param_1,
		char* command_param_2, char* command_param_3, char** at_command)
{
	/* Create AT command */
	uint8_t length = strlen(AT_PREFIX) + strlen(command_code) + strlen(EQUAL_SIGN)
			+ strlen(command_param_1) + strlen(COMMA_SIGN) + strlen(command_param_2) +strlen(COMMA_SIGN)
			+ strlen (command_param_3) + strlen(END_OF_COMMAND);
	*at_command = malloc(length + 1);	// +1 for '\0' string termination
	strcpy(*at_command, AT_PREFIX);
	strcat(*at_command, command_code);
	strcat(*at_command, EQUAL_SIGN);
	strcat(*at_command, command_param_1);
	strcat(*at_command, COMMA_SIGN);
	strcat(*at_command, command_param_2);
	strcat(*at_command, COMMA_SIGN);
	strcat(*at_command, command_param_3);
	strcat(*at_command, END_OF_COMMAND);

	return length;
}


/**************************************************************************/
/*!
    Create AT command with four parameters: AT+CODE=PARAM,PARAM,PARAM,PARAM

    Returns resulting command in pointer 'at_command' passed as argument
*/
/**************************************************************************/
uint8_t create_four_param_AT_command (char* command_code, char* command_param_1,
		char* command_param_2, char* command_param_3, char* command_param_4, char** at_command)
{
	/* Create AT command */
	uint8_t length = strlen(AT_PREFIX) + strlen(command_code) + strlen(EQUAL_SIGN)
			+ strlen(command_param_1) + strlen(COMMA_SIGN) + strlen(command_param_2) +strlen(COMMA_SIGN)
			+ strlen (command_param_3) +strlen(COMMA_SIGN) + strlen (command_param_4) + strlen(END_OF_COMMAND);
	*at_command = malloc(length + 1);	// +1 for '\0' string termination
	strcpy(*at_command, AT_PREFIX);
	strcat(*at_command, command_code);
	strcat(*at_command, EQUAL_SIGN);
	strcat(*at_command, command_param_1);
	strcat(*at_command, COMMA_SIGN);
	strcat(*at_command, command_param_2);
	strcat(*at_command, COMMA_SIGN);
	strcat(*at_command, command_param_3);
	strcat(*at_command, COMMA_SIGN);
	strcat(*at_command, command_param_4);
	strcat(*at_command, END_OF_COMMAND);

	return length;
}
