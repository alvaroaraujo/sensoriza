/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32l1xx_hal.h"
#include "adc.h"
#include "i2c.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "luminosidad_tsl2561.h"
#include "temphumpres_bme280.h"
#include "rain_sensor.h"
#include "json_helper.h"

#define BME280_PRESENT		1
#define TSL2561_PRESENT		1
#define RAINSENSOR_PRESENT	0

#define MAX_TRIES 3

//#define USE_BC95
#define USE_BG96

#ifdef USE_BC95
#include "nbiot_bc95.h"
#elif defined(USE_BG96)
#include "nbiot_bg96.h"
#endif

#define USE_DISCO_LEDS 0

#if USE_DISCO_LEDS
#define toggle_green()	HAL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin)
#define toggle_blue()	HAL_GPIO_TogglePin(LD4_GPIO_Port, LD4_Pin)
#define turn_on_green()	HAL_GPIO_WritePin(LD3_GPIO_Port, LD3_Pin, GPIO_PIN_SET)
#define turn_on_blue()	HAL_GPIO_WritePin(LD4_GPIO_Port, LD4_Pin, GPIO_PIN_SET)
#else
#define toggle_green()
#define toggle_blue()
#define turn_on_green()
#define turn_on_blue()
#endif

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
volatile float temp, press, hum, alt;
int16_t tempVal;
uint8_t humVal;
uint16_t pressVal;
/* Buffer used for reception */
uint8_t aRxBuffer[20];

json_all_fields_t example_fields;

char* socket_ref;
char* json_message;

uint8_t flag_wakeup = 0;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  MX_I2C1_Init();
  MX_TIM2_Init();
  MX_ADC_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  uint8_t num_tries = 0;

#if BME280_PRESENT
  // Init TEMP-HUM-PRESS sensor (several tries in case it does not respond)
  uint8_t bme280_enabled = FALSE;
  while (num_tries < MAX_TRIES) {
	  bme280_enabled = bme280_init();
	  if (bme280_enabled == TRUE) {
		  num_tries = 0;
		  toggle_green();
		  break;
	  } else {
		  num_tries++;
		  HAL_Delay(500);
		  toggle_blue();
  	  }
  }
#endif

#if TSL2561_PRESENT
  // Init luminosity sensor (several tries in case it does not respond)
  uint8_t tsl2561_enabled = FALSE;
  while (num_tries < MAX_TRIES) {
	  tsl2561_enabled = tsl2561_init();
	  if (tsl2561_enabled == TRUE) {
		  num_tries = 0;
		  tsl2561_enableAutoRange(TRUE);
		  toggle_green();
		  break;
	  } else {
		  num_tries++;
		  HAL_Delay(500);
		  toggle_blue();
	  }
  }
#endif

#if RAINSENSOR_PRESENT
  // Init rain_sensor
  rainSensor_init();
#endif

  // Fill JSON struct
  example_fields.node_id = 2;
  example_fields.latitude = 40.45232589;
  example_fields.longitude = -3.72633016;
  example_fields.air_temp = 17;
  example_fields.ground_temp = 15;
  example_fields.hum = 45;
  example_fields.press = 960;
  example_fields.lum = 200;
  example_fields.rain = 0;
  example_fields.salt = 16;

  // Setup nbiot module
#ifdef USE_BC95
  bc95_setup_nbiot_module(&socket_ref);
#elif defined(USE_BG96)
  bg96_powerup();
  uint8_t bg96_socket_open = FALSE;
  while (num_tries < MAX_TRIES) {
	  bg96_socket_open = bg96_setup_nbiot_module();
	  if (bg96_socket_open == TRUE) {
		  num_tries = 0;
		  HAL_Delay(WAIT_AFTER_SOCKET_MS);
		  toggle_green();
		  break;
	  } else {
		  num_tries++;
		  HAL_Delay(1000);
		  toggle_blue();
	  }
  }
  if (bg96_socket_open == FALSE) {
	  turn_on_green();
	  turn_on_blue();
  }
#endif

#ifdef USE_BG96
  // Enable GPS
  bg96_enableGPS();
#endif

  // Send first message
  json_message = malloc(1024);
  full_json_struct_writer(json_message, example_fields);
#ifdef USE_BC95
  bc95_send_nbiot_message(socket_ref, json_message);
#elif defined(USE_BG96)
  if (bg96_socket_open) {
	  bg96_send_nbiot_message(json_message);
  }
#endif
  free(json_message);

  // Init timer
  HAL_TIM_Base_Start_IT(&htim2);

  while (1)
  {

  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

	flag_wakeup = 0;

	uint16_t active_fields = 0x0000; // When a sensor is read, the corresponding flag activates

#if BME280_PRESENT
	// TEMP-HUM-PRESS sensor
	if (bme280_enabled) {

		int16_t tempVal;
		uint8_t humVal;
		uint16_t pressVal;

		readTempHumPressValuesAndRoundToInteger(&tempVal, &humVal, &pressVal);
		example_fields.air_temp = tempVal;
		example_fields.hum = humVal;
		example_fields.press = pressVal;

		active_fields |= AIR_TEMP_MASK | HUM_MASK | PRESS_MASK;
	}
#endif

#if TSL2561_PRESENT
	// Luminosity sensor
	if (tsl2561_enabled) {
		uint16_t broad = 0xabcd;
		uint16_t infra = 0x8796;
		getLuminosity(&broad, &infra);
		uint32_t luxValue = calculateLux(broad, infra);

		example_fields.lum = luxValue;

		active_fields |= LUM_MASK;
	}
#endif

#if RAINSENSOR_PRESENT
	// Rain sensor
	example_fields.rain = readRainLevel();

	active_fields |= RAIN_MASK;
#endif

#ifdef USE_BG96
	// Request GPS location
	bg96_requestGPSlocation(&example_fields.latitude, &example_fields.longitude);
#endif

	// Create JSON message
	json_message = malloc(256);
	active_fields |= LATITUDE_MASK | LONGITUDE_MASK;	// LAT and LONG are always sent
	requested_fields_json_writer(json_message, active_fields, example_fields);

	// Send JSON message
#ifdef USE_BC95
	bc95_send_nbiot_message(socket_ref, json_message);
#elif defined(USE_BG96)
	if (bg96_socket_open) {
		bg96_send_nbiot_message(json_message);
	}
#endif
	free(json_message);

	toggle_green();

	// Enter SLEEP MODE in both BG96 and discovery
#ifdef USE_BG96
	bg96_enter_sleep_mode();
#endif
	HAL_SuspendTick();
	HAL_PWR_EnterSLEEPMode(PWR_LOWPOWERREGULATOR_ON, PWR_SLEEPENTRY_WFI);
	while(flag_wakeup != 1);
	HAL_ResumeTick();
#ifdef USE_BG96
	bg96_exit_sleep_mode();
#endif
	//HAL_Delay(60000);

	// Pruebas bajo consumo
//	HAL_Delay(2000);
//	bg96_get_ip_address();
//
//	HAL_Delay(2000);
//	bg96_set_minimum_functionality();
//	HAL_Delay(2000);
//	bg96_enter_sleep_mode();
//
//	HAL_Delay(2000);
//	bg96_get_ip_address();
//
//	HAL_Delay(10000);
//	bg96_exit_sleep_mode();
//	HAL_Delay(2000);
//	bg96_setup_nbiot_module();
//	HAL_Delay(2000);
//
//	bg96_get_ip_address();
	// Fin pruebas bajo consumo
  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
  RCC_OscInitStruct.PLL.PLLDIV = RCC_PLL_DIV3;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */
/**
  * @brief Rx Transfer completed callbacks
  * @param huart: uart handle
  * @retval None
  */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(huart);

  HAL_UART_Transmit(&huart1, (uint8_t *)aRxBuffer, 10, 0xFFFF);
  HAL_UART_Transmit(&huart1, (uint8_t *)"\r\n", 2, 0xFFFF);
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @param  htim: TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    if (htim->Instance == htim2.Instance)
    {
        toggle_blue();
        flag_wakeup = 1;
    }
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
