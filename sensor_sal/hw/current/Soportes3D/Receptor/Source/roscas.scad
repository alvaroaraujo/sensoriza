module roscaHembraISO(radio, paso, vueltas, precision=10, redondo=false, helices=1, orientacion=1, holgura=0, rExterno=0)
{
    precision=(precision==10 || precision<=0)?10:precision;
    holgura=(holgura<=0)?0:holgura;
    helices=(helices<=1)?1:helices;
    orientacion=(orientacion!=1 && orientacion!=2)?1:(orientacion==1)?1:-1;
    vueltas=vueltas+1;
    radioInterno=paso*0.14434;
    incrementoTornillo=sqrt(3)*paso/8-(paso/(tan(30)*8)+sqrt(pow(radioInterno,2)-pow(paso/8,2))-radioInterno);
    interior=sqrt(3)*paso/16-(paso/(tan(30)*16)+sqrt(pow(radioInterno,2)-pow(paso/16,2))-radioInterno);
    medio=sqrt(3)*5*paso/16;
    radioExterior=incrementoTornillo+5*sqrt(3)*paso/16-sqrt(pow(radioInterno/2,2)-pow(paso/16,2));
    rExterno=(rExterno<=(radio+incrementoTornillo+interior+medio))?(radio+incrementoTornillo+interior+medio+.001):rExterno;
    mitad=paso/2;
    difference()
    {
        translate([0,0,-(vueltas-1)*paso]) cylinder(r=rExterno,h=(vueltas-1)*paso,$fn=360);
        difference()
        {
            union()
            {
                translate([0,0,-vueltas*paso]) rotate([0,0,3*precision/16]) cylinder(r=radio+.01,h=vueltas*paso+.01,center=false,$fn=360);
                
                // Hacer en negativo
                for(j=[360:-(360/helices):1])
                {
                    for(i=[1:precision:round(360*vueltas/helices)+precision])
                    {
                        x=radio;
                        y=0;
                        zPre=paso-(i-precision)*(helices*(paso/4)/90);
                        zPost=paso-i*(helices*(paso/4)/90);
                        incrementoTri=paso/16;
                        union()
                        {
                            hull()
                            {
                                rotate([0,0,orientacion*(i-precision+j)]) polyhedron(
                                    [[x,y,zPre],[x+incrementoTornillo,y,zPre],[x+incrementoTornillo,y,zPre-paso],[x,y,zPre-paso]],
                                    [[0,1,2],[2,3,0]]);
                                rotate([0,0,orientacion*(i+j)]) polyhedron(
                                    [[x,y,zPost],[x+incrementoTornillo,y,zPost],[x+incrementoTornillo,y,zPost-paso],[x,y,zPost-paso]],
                                    [[0,1,2],[2,3,0]]);
                            }
                            hull()
                            {
                                rotate([0,0,orientacion*(i-precision+j)]) polyhedron(
                                    [[x+incrementoTornillo,y,zPre-2*incrementoTri+holgura],[x+incrementoTornillo+medio,y,zPre-mitad+incrementoTri+holgura],[x+incrementoTornillo+medio,y                      ,zPre-paso+mitad-incrementoTri-holgura],[x+incrementoTornillo,y,zPre-paso+2*incrementoTri-holgura]],
                                    [[0,1,2],[2,3,0]]);
                                rotate([0,0,orientacion*(i+j)]) polyhedron(
                                    [[x+incrementoTornillo,y,zPost-2*incrementoTri+holgura],[x+incrementoTornillo+medio,y,zPost-mitad+incrementoTri+holgura],[x+incrementoTornillo+medio,y,                       zPost-paso+mitad-incrementoTri-holgura],[x+incrementoTornillo,y,zPost-paso+2*incrementoTri-holgura]],
                                    [[0,1,2],[2,3,0]]);
                            }
                           
                            if(redondo){
                                hull()
                                {
                                    rotate([90,0,i-precision]) translate([x+radioExterior,zPre-mitad,y])                    
                                    difference()
                                    {
                                        cylinder(r=radioInterno/2,h=.001,$fn=25);
                                        translate([incrementoTornillo-radioExterior,-mitad,0]) cube([medio,paso,.02]);
                                    }
                                    rotate([90,0,i]) translate([x+radioExterior,zPost-mitad,y]) 
                                    difference()
                                    {
                                        cylinder(r=radioInterno/2,h=.001,$fn=25);
                                        translate([incrementoTornillo-radioExterior,-mitad,0]) cube([medio,paso,.02]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            *cylinder(r=radio+radioExterior+radioInterno,h=2*paso,$fn=360);
            *translate([0,0,-(vueltas+1)*paso]) cylinder(r=radio+radioExterior+radioInterno,h=2*paso,$fn=360);
        }
        
        rotate_extrude(convexity=4,$fn=100)
polygon(points=[[radio+incrementoTornillo+medio,.0001],[radio+incrementoTornillo+medio-paso,.0001],[.0001,-radio*tan(40)],[paso,-radio*tan(40)]]);
    }
}

module roscaMachoISO(radio, paso, vueltas, precision=10, redondo=false, helices=1, orientacion=1, holgura=0.05)
{
    precision=(precision==10 || precision<=0)?10:precision;
    holgura=(holgura<=0)?0:holgura;
    helices=(helices<=1)?1:helices;
    orientacion=(orientacion!=1 && orientacion!=2)?-1:(orientacion==1)?-1:1;
    vueltas=vueltas+1;
    radioInterno=paso*0.14434;
    abajo=sqrt(3)*paso/8-(paso/(tan(30)*8)+sqrt(pow(radioInterno,2)-pow(paso/8,2))-radioInterno);
    medio=sqrt(3)*5*paso/16;
    arriba=sqrt(3)*paso/16;
    mitad=7*paso/16;
    difference()
    {
        for(i=[1:precision:round(360*vueltas/helices)+precision])
        {
            x=radio;
            y=0;
            zPre=paso-(i-precision)*(helices*(paso/4)/90);
            zPost=paso-i*(helices*(paso/4)/90);
            incrementoTri=paso/8;
            union()
            {
                for(j=[360:-(360/helices):1])
                {
                    hull()
                    {
                        rotate([0,0,orientacion*(i-precision+j)]) polyhedron(
                            [[x,y,zPre],[x+abajo,y,zPre],[x+abajo,y,zPre-paso],[x,y,zPre-paso]],
                            [[0,1,2],[2,3,0]]);
                        rotate([0,0,orientacion*(i+j)]) polyhedron(
                            [[x,y,zPost],[x+abajo,y,zPost],[x+abajo,y,zPost-paso],[x,y,zPost-paso]],
                            [[0,1,2],[2,3,0]]);
                    }
                    hull()
                    {
                        rotate([0,0,orientacion*(i-precision+j)]) polyhedron(
                            [[x+abajo,y,zPre-incrementoTri-holgura],[x+abajo+medio,y,zPre-mitad-holgura],[x+abajo+medio,y                      ,zPre-paso+mitad+holgura],[x+abajo,y,zPre-paso+incrementoTri+holgura]],
                            [[0,1,2],[2,0,3]]);
                        rotate([0,0,orientacion*(i+j)]) polyhedron(
                            [[x+abajo,y,zPost-incrementoTri-holgura],[x+abajo+medio,y,zPost-mitad-holgura],[x+abajo+medio,y,                       zPost-paso+mitad+holgura],[x+abajo,y,zPost-paso+incrementoTri+holgura]],
                            [[0,1,2],[2,0,3]]);
                    }
                }
            }
        }
        if(redondo){
            for(i=[1:precision:round(360*vueltas/helices)+precision])
            {
                x=radio;
                y=0;
                zPre=paso-(i-precision)*((paso/4)/90);
                zPost=paso-i*((paso/4)/90);
                hull()
                {
                    rotate([90,0,i-precision]) translate([x+radioInterno,zPre,y]) cylinder(r=radioInterno,h=.01,$fn=25);
                    rotate([90,0,i]) translate([x+radioInterno,zPost,y]) cylinder(r=radioInterno,h=.01,$fn=25);
                }
            }
                
            for(i=[(round(360*vueltas/helices)+precision):precision:(round(360*vueltas/helices)+360)+precision])
            {
                x=radio;
                y=0;
                zPre=paso-(i-precision)*((paso/4)/90);
                zPost=paso-i*((paso/4)/90);
                hull()
                {
                    rotate([90,0,i-precision]) translate([x+radioInterno,zPre,y]) cylinder(r=radioInterno,h=.01,$fn=25);
                    rotate([90,0,i]) translate([x+radioInterno,zPost,y]) cylinder(r=radioInterno,h=.01,$fn=25);
                }
            }
        }
        
        cylinder(r=radio+abajo+medio+arriba,h=2*paso,center=false,$fn=360);
        translate([0,0,-vueltas*paso-.02]) cylinder(r=radio+abajo+medio+arriba,h=paso,center=false,$fn=360);
        rotate_extrude(convexity=4,$fn=100)
polygon(points=[[.0001,radio*tan(40)],[paso,radio*tan(40)],[radio+sqrt(3)*paso/2+paso-.0001,-sqrt(3)*paso/2/tan(50)],[radio+sqrt(3)*paso/2,-sqrt(3)*paso/2/tan(50)]]);
    }
}
