include <roscas.scad>
include <params.scad>

module estructura(altura, radio, grosor, extraGrosor, precision=25)
{
    redondo=(precision==25)?25:precision;

    difference(){
        hull(){
            union(){
                for(i=[0:3]){
                    x=[16.7,0,-16.7,0];
                    y=[0,16.7,0,-16.7];;
                    translate([x[i], y[i], .5]) cylinder(r=4,h=1, center=true, $fn=redondo);
                }
            }
            cylinder(r=radio, h=altura, $fn=redondo);
        }
        /*cylinder(r=radio-paso, h=altura, $fn=redondo);
        for(i=[0:3]){
            x=[16.7,0,-16.7,0];
    y=[0,16.7,0,-16.7];
            translate([x[i], y[i], altura/2+1]) cylinder(r=3+toleranciaRadio,h=altura, center=true, $fn=redondo);
            translate([x[i], y[i], -altura/2]) cylinder(r=1.5+toleranciaRadio,h=2*altura, center=true, $fn=redondo);
        }*/
    }
    
}

altura=distanciaFiltro-(grosorFiltro+grosor/2);
toleranciaRadio=.1;
grosorTuerca=2.5;

difference(){
union(){
    estructura(altura-vueltas*paso,radioFiltro+toleranciaRadio+paso/2+toleranciaRadio,grosor+toleranciaRadio,paso,360);
    translate([0,0,altura]) roscaMachoISO(radioFiltro+toleranciaRadio,paso,vueltas,5,helices=3, holgura=.1);
    
    /*difference()
    {
        cylinder(r=radioFiltro+toleranciaRadio,h=altura,center=false,$fn=360);
        cylinder(r=radioFiltro-grosor,h=altura,center=false,$fn=360);
        translate([0,0,altura]) rotate_extrude(convexity=4,$fn=100)
polygon(points=[[.0001,(radioFiltro+toleranciaRadio)*tan(40)],[paso,(radioFiltro+toleranciaRadio)*tan(40)],[(radioFiltro+toleranciaRadio)+sqrt(3)*paso/2+paso,-sqrt(3)*paso/2/tan(50)],[(radioFiltro+toleranciaRadio)+sqrt(3)*paso/2,-sqrt(3)*paso/2/tan(50)]]);
    }*/
}
    cylinder(r=radioFiltro+toleranciaRadio, h=altura-vueltas*paso, $fn=360);
       k = altura-vueltas*paso-paso/2;
        for(i=[0:3]){
            x=[16.7,0,-16.7,0];
            y=[0,16.7,0,-16.7];
            if (i==3 || i==2) {
                translate([x[i], y[i], 1]) cylinder(r=3+toleranciaRadio,h=altura-vueltas*paso-paso/2, center=false, $fn=360);
            } else {
                translate([x[i], y[i], 1]) cylinder(r=3+toleranciaRadio,h=altura-vueltas*paso, center=false, $fn=360);
            }
            //translate([x[i], y[i], (altura-vueltas*paso)/2+1]) cylinder(r=3+toleranciaRadio,h=k, center=true, $fn=360);
            translate([x[i], y[i], -(altura-vueltas*paso)/2]) cylinder(r=1.5+toleranciaRadio,h=2*(altura-vueltas*paso), center=true, $fn=360);
        }
/*translate([0,0,0]) rotate_extrude(convexity=4,$fn=100)
polygon(points=[[.0001,16*tan(90-atan((16-radioFiltro)/(altura-vueltas*paso)))],[16,-.0001],[.0001,-.0001]]);*/
    
    *rotate([0,0,-90]) cube([30, 30 ,30]);
}

//translate([16.7,0,0]) cylinder(r=1.5, h=0.5);
/*translate([0,0,0]) rotate_extrude(convexity=4,$fn=100)
polygon(points=[[.0001,18*tan(90-atan((18-radioFiltro)/(altura-vueltas*paso)))],[paso,18*tan(90-atan((18-radioFiltro)/(altura-vueltas*paso)))],[18+paso,0],[18,0]]);*/
