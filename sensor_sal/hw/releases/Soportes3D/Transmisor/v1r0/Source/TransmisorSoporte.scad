include <roscas.scad>

module estructura(altura, radio, grosor, extraGrosor, precision=25)
{
    redondo=(precision==25)?25:precision;
    union(){
        difference()
        {
            cylinder(r=radio+grosor+extraGrosor,h=altura, center=false, $fn=redondo);
            cylinder(r=radio,h=altura, center=false, $fn=redondo);
            difference()
            {
                union()
                {
                    translate([-4,-2*(radio+grosor+extraGrosor)/2,-.10]) cube([8,2*(radio+grosor+extraGrosor),2*altura],center=false);
                    rotate([0,0,90]) translate([-4,-2*(radio+grosor+extraGrosor)/2,-.10]) cube([8,2*(radio+grosor+extraGrosor),2*altura],center=false);
                }
                cylinder(r=radio+grosor+sqrt(3)*paso/4+toleranciaRadio+.25,h=4*grosor,$fn=redondo);
            }
        }
        for(i=[0:3]){
            x=[radio+grosor-toleranciaRadio+4,0,-radio-grosor+toleranciaRadio-4,0];
            y=[0,radio+grosor-toleranciaRadio+4,0,-radio-grosor+toleranciaRadio-4];
            t_cubo=[8,8,8,8,8,8,8,8];
            x_cubo=[radio+grosor-toleranciaRadio-4,-4,-radio-grosor+toleranciaRadio-4,-4];
            y_cubo=[-4,radio+grosor-toleranciaRadio-4,-4,-radio-grosor+toleranciaRadio-4];
            difference(){
                union(){
                    translate([x[i],y[i],.5]) cylinder(r=4,h=1, center=true, $fn=redondo);
                    translate([x_cubo[i],y_cubo[i],0]) cube([t_cubo[2*i],t_cubo[2*i+1], 1], center=false);
                }
                translate([x[i],y[i],.5]) cylinder(r=1.5,h=grosor, center=true, $fn=redondo);
                cylinder(r=radio,h=altura, center=false, $fn=redondo);
            }
        }
    }
}

vueltas=2;
paso=2;
grosor=sqrt(3)*paso/2;
radioFiltro=12.5;
grosorFiltro=2;

distanciaFiltro=10;
altura=distanciaFiltro-(grosorFiltro+grosor/2);
toleranciaRadio=.1;
grosorTuerca=2.5;
difference(){
union(){
    estructura(altura-vueltas*paso,radioFiltro-grosor,grosor+toleranciaRadio,paso,360);
    translate([0,0,altura]) roscaMachoISO(radioFiltro+toleranciaRadio,paso,vueltas,5,helices=3, holgura=.05);
    difference()
    {
        cylinder(r=radioFiltro+toleranciaRadio,h=altura,center=false,$fn=360);
        cylinder(r=radioFiltro-grosor,h=altura,center=false,$fn=360);
        translate([0,0,altura]) rotate_extrude(convexity=4,$fn=100)
polygon(points=[[.0001,(radioFiltro+toleranciaRadio)*tan(40)],[paso,(radioFiltro+toleranciaRadio)*tan(40)],[(radioFiltro+toleranciaRadio)+sqrt(3)*paso/2+paso,-sqrt(3)*paso/2/tan(50)],[(radioFiltro+toleranciaRadio)+sqrt(3)*paso/2,-sqrt(3)*paso/2/tan(50)]]);
    }
}
*rotate([0,0,-90]) cube([30, 30 ,30]);
}