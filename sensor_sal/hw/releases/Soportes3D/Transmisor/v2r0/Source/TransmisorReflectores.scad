$fn = 150;
grosorBase = 1;
grosorSujecion = 2;
alturaReflector = 9.6;
grosorTop = 0.5;
radioBase = 21.5;
grosorBorde = 0.5;
tornillo = 1.5;
cabezaTornillo = 3;
tolerancia = 0.1;

union(){
    difference(){
        hull(){
            union(){
                translate([24.75, 0, grosorBase + alturaReflector - grosorTop]) cylinder(grosorTop, r=cabezaTornillo+2*tolerancia*cabezaTornillo);
                translate([0, -24.75, grosorBase + alturaReflector - grosorTop]) cylinder(grosorTop, r=cabezaTornillo+2*tolerancia*cabezaTornillo);
                translate([-24.75, 0, grosorBase + alturaReflector - grosorTop]) cylinder(grosorTop, r=cabezaTornillo+2*tolerancia*cabezaTornillo);
                translate([0, 24.75, grosorBase + alturaReflector - grosorTop]) cylinder(grosorTop, r=cabezaTornillo+2*tolerancia*cabezaTornillo);
            }
            cylinder(grosorBase + alturaReflector,r=radioBase);
        }
        translate([0, 0, grosorSujecion]) cylinder(alturaReflector + 0.1,r=radioBase - grosorBorde);
        translate([24.75, 0, 0]) cylinder(grosorBase + alturaReflector + 0.1, r=tornillo+tolerancia*tornillo);
        translate([0, -24.75, 0]) cylinder(grosorBase + alturaReflector + 0.1, r=tornillo+tolerancia*tornillo);
        translate([-24.75, 0, 0]) cylinder(grosorBase + alturaReflector + 0.1, r=tornillo+tolerancia*tornillo);
        translate([0, 24.75, 0]) cylinder(grosorBase + alturaReflector + 0.1, r=tornillo+tolerancia*tornillo);
        translate([24.75, 0, 0]) cylinder(grosorBase + alturaReflector - grosorTop, r=cabezaTornillo+tolerancia*cabezaTornillo);
        translate([0, -24.75, 0]) cylinder(grosorBase + alturaReflector - grosorTop, r=cabezaTornillo+tolerancia*cabezaTornillo);
        translate([-24.75, 0, 0]) cylinder(grosorBase + alturaReflector - grosorTop, r=cabezaTornillo+tolerancia*cabezaTornillo);
        translate([0, 24.75, 0]) cylinder(grosorBase + alturaReflector - grosorTop, r=cabezaTornillo+tolerancia*cabezaTornillo);
        translate([8.6, 8.6, grosorBase]) cylinder(1, r=8.5);
        translate([-8.6, 8.6, grosorBase]) cylinder(1, r=8.5);
        translate([8.6, -8.6, grosorBase]) cylinder(1, r=8.5);
        translate([-8.6, -8.6, grosorBase]) cylinder(1, r=8.5);
        translate([8.6, 8.6, 0]) cylinder(1, r=7.8);
        translate([-8.6, 8.6, 0]) cylinder(1, r=7.8);
        translate([8.6, -8.6, 0]) cylinder(1, r=7.8);
        translate([-8.6, -8.6, 0]) cylinder(1, r=7.8);
    }
}