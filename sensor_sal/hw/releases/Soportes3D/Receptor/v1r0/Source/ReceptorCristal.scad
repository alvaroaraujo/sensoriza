include <roscas.scad>

module estructura(altura, radio, grosor, precision=25)
{
    redondo=(precision==25)?25:precision;
    union(){
        difference()
        {
            cylinder(r=radio+grosor+1,h=altura, center=false, $fn=redondo);
            translate([0,0,.5]) cylinder(r=radio,h=altura, center=false, $fn=redondo);
            cylinder(r=radio-2,h=altura, center=false, $fn=redondo);
            *translate([0,0,.5]) cylinder(r=12.7,h=2, center=false, $fn=redondo);
        }
    }
}

vueltas=1.5;
paso=2;
grosor=sqrt(3)*paso/2;
radioFiltro=12.7;
grosorFiltro=6.3;
toleranciaRadio=.1;
toleranciaAltura=.25;
altura=vueltas*paso+.5+grosorFiltro+toleranciaAltura;

difference(){
union(){
    estructura(altura-paso*vueltas,radioFiltro+toleranciaRadio,grosor,360);
    translate([0,0,altura]) roscaHembraISO(radioFiltro+toleranciaRadio,paso,vueltas,5,helices=3,orientacion=2,holgura=.1,rExterno=radioFiltro+toleranciaRadio+grosor+1);
}

    *rotate([0,0,-90]) cube([20, 20 ,20]);
}
//rExterno=radioFiltro+toleranciaRadio+grosor