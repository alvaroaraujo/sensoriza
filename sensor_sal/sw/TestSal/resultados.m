%% Fetch data periodico
clear, clc
Con = 1;
Sin = 1;

nTest = zeros(1, Con+Sin);
numRepet = zeros(1, Con+Sin);
durSec = zeros(1, Con+Sin);
numVeces = zeros(1, Con+Sin);
Telapsed = zeros(1, Con+Sin);

if Con
    pruebaCon = csvread('pruebaConSal2.csv');
    tmp = pruebaCon(1,:);
    nTest(1) = tmp(1);
    config = zeros(Con+Sin,nTest(1)+5);
    config(1,:) = tmp(1:nTest(1)+5);
    pruebaCon = pruebaCon(2:end,1:(end-1));
    frecuencias = config(1,2:(2+nTest-1));
    numRepet(1) = config(1,nTest(1)+2);
    durSec(1) = config(1,nTest(1)+3)/1000;
    numVeces(1) = config(1,nTest(1)+4);
    Telapsed(1) = config(1,nTest(1)+5);
end
if Sin
    pruebaSin = csvread('pruebaSinSal2.csv');
    tmp = pruebaSin(1,:);
    nTest(end) = tmp(1);
    if (Con == 0)
        config = zeros(1,nTest(end)+5);
    end
    config(end,:) = tmp(1,1:nTest(end)+5);
    pruebaSin = pruebaSin(2:end,1:(end-1));
    frecuencias = config(1,2:(2+nTest-1));
    numRepet(end) = config(1,nTest(end)+2);
    durSec(end) = config(1,nTest(end)+3)/1000;
    numVeces(end) = config(1,nTest(end)+4);
    Telapsed(end) = config(1,nTest(end)+5);
end

colores = cell(3,7);
colores{1,1} = [0 1 0.9]; colores{1,2} = [1/7 1 0.9]; colores{1,3} = [2/7 1 0.9];
colores{1,4} = [3/7 1 0.9]; colores{1,5} = [4/7 1 0.9]; colores{1,6} = [5/7 1 0.9]; colores{1,7} = [6/7 1 0.9];

colores{2,1} = [0.55 1 0.5]; colores{2,2} = [0.55 1 0.5]; colores{2,3} = [0.55 1 0.5];
colores{2,4} = [0.55 1 0.5]; colores{2,5} = [0.55 1 0.5]; colores{2,6} = [0.55 1 0.5]; colores{2,7} = [0.55 1 0.5];

colores{3,1} = [0 1 0.5]; colores{3,2} = [0 1 0.5]; colores{3,3} = [0 1 0.5];
colores{3,4} = [0 1 0.5]; colores{3,5} = [0 1 0.5]; colores{3,6} = [0 1 0.5]; colores{3,7} = [0 1 0.5];

clear tmp config

%% An�lisis temporal y en frecuencia

etapas = 1;     % N�mero de veces que se aplica el primer filtrado
fcorte1 = 25;   % Frecuencia de corte del primer filtro
fcorte2 = 7;    % Frecuencia de corte del segundo filtro

if Con
    x = linspace(0,durSec(end),length(pruebaCon(1,:)));
else
    x = linspace(0,durSec(end),length(pruebaSin(1,:)));
end

[b, a] = butter(6, fcorte1*durSec(end)*2/length(x), 'low'); % 0.45, 'low'); % [0.015 0.15], 'bandpass');
[c, d] = butter(6, fcorte2*durSec(end)*2/length(x), 'low');

for k = 0:nTest(end)-1
    
    % Generaci�n de leyendas para las gr�ficas
    if (Con + Sin) == 2
        leg = zeros(2,1);
        leg2 = zeros(3,1);
    else
        leg = zeros(numVeces(end),1);
    end
    
    % h: n�mero de bloque      p: n�mero de repetici�n
    for h = 0:numVeces(end)-1
        for p = 0:numRepet(end)-1
            if Con
                figure(frecuencias(k+1))
                nFilt = 0;
                filtradoA = pruebaCon(k*numRepet(1)+h*numRepet(1)*nTest(1)+p+1,:);
                while (nFilt < etapas)
                    filtradoA = filter(b,a,filtradoA);
                    nFilt = nFilt+1;
                end
                % Temporal
                subplot(211)
                plot(x,filtradoA, 'Color', hsv2rgb(colores{1+Con*Sin,h+1}-(1-Con*Sin)*[0 0 p/numRepet(end)*0.5]));
                hold on
                % Frecuencial
                subplot(212)
                fftP = fft(filtradoA);
                P2 = abs(fftP/length(fftP));
                P1 = P2(1:fix(length(fftP)/2)+1);
                P1(2:end-1) = 2*P1(2:end-1);
                if ( p == 0 && (Con + Sin == 2))
                    leg(1) = plot(linspace(0,(length(P1)-1)/durSec(end),length(P1)),P1, 'Color', hsv2rgb(colores{1+Con*Sin,h+1}-(1-Con*Sin)*[0 0 p/numRepet(end)*0.5]));
                elseif (p == 0)
                    leg(h+1) = plot(linspace(0,(length(P1)-1)/durSec(end),length(P1)),P1, 'Color', hsv2rgb(colores{1+Con*Sin,h+1}-(1-Con*Sin)*[0 0 p/numRepet(end)*0.5]));
                else
                    plot(linspace(0,(length(P1)-1)/durSec(end),length(P1)),P1, 'Color', hsv2rgb(colores{1+Con*Sin,h+1}-(1-Con*Sin)*[0 0 p/numRepet(end)*0.5]));
                end
                axis([0 frecuencias(k+1)*10 0 max(P1(3:end))]);
                hold on
            end
            
            if Sin
                figure(frecuencias(k+1))
                filtradoB = pruebaSin(k*numRepet(end)+h*numRepet(end)*nTest(end)+p+1,:);
                nFilt = 0;
                while (nFilt < etapas)
                    filtradoB = filter(b,a,filtradoB);
                    nFilt = nFilt+1;
                end
                % Temporal
                subplot(211)
                plot(x,filtradoB, 'Color', hsv2rgb(colores{1+2*Con*Sin,h+1}-(1-Con*Sin)*[0 0 p/numRepet(end)*0.5]));
                hold on
                % Frecuencial
                subplot(212)
                fftP = fft(filtradoB);
                P2 = abs(fftP/length(fftP));
                P1 = P2(1:fix(length(fftP)/2)+1);
                P1(2:end-1) = 2*P1(2:end-1);
                if ( p == 0 && (Con + Sin == 2))
                    leg(end) = plot(linspace(0,(length(P1)-1)/durSec(end),length(P1)),P1, 'Color', hsv2rgb(colores{1+2*Con*Sin,h+1}-(1-Con*Sin)*[0 0 p/numRepet(end)*0.5]));
                elseif (p == 0)
                    leg(h+1) = plot(linspace(0,(length(P1)-1)/durSec(end),length(P1)),P1, 'Color', hsv2rgb(colores{1+2*Con*Sin,h+1}-(1-Con*Sin)*[0 0 p/numRepet(end)*0.5]));
                else
                    plot(linspace(0,(length(P1)-1)/durSec(end),length(P1)),P1, 'Color', hsv2rgb(colores{1+2*Con*Sin,h+1}-(1-Con*Sin)*[0 0 p/numRepet(end)*0.5]));
                end
                if Con == 0
                    axis([0 frecuencias(k+1)*10 0 max(P1(3:end))]);
                end
                hold on
            end
            
            if Con+Sin == 2
                figure(1000+frecuencias(k+1))
                tmp = pruebaCon(k*numRepet(1)+h*numRepet(1)*nTest(1)+p+1,:)-pruebaSin(k*numRepet(end)+h*numRepet(end)*nTest(end)+p+1,:);
                filtradoA = filter(c,d,tmp);
                % Temporal
                subplot(211)
                plot(x,filtradoA, 'Color', hsv2rgb(colores{1+2*Con*Sin,h+1}-(1-Con*Sin)*[0 0 p/numRepet(end)*0.5]));
                hold on
                % Frecuencial
                subplot(212)
                fftP = fft(filtradoA);
                P2 = abs(fftP/length(fftP));
                P1 = P2(1:fix(length(fftP)/2)+1);
                P1(2:end-1) = 2*P1(2:end-1);
                if (h == 0)
                    leg2(1) = plot(linspace(0,(length(P1)-1)/durSec(end),length(P1)),P1, 'Color', hsv2rgb(colores{1+2*Con*Sin,h+1}-(1-Con*Sin)*[0 0 p/numRepet(end)*0.5]));
                else
                    plot(linspace(0,(length(P1)-1)/durSec(end),length(P1)),P1, 'Color', hsv2rgb(colores{1+2*Con*Sin,h+1}-(1-Con*Sin)*[0 0 p/numRepet(end)*0.5]));
                end
                axis([0 frecuencias(k+1)*10 0 max(P1(3:end))]);
                hold on
            end
        end
    end    
    
    % Etiquetas
    figure(frecuencias(k+1))
    subplot(211)
    ylabel('Valor lectura ADC')
    xlabel('Tiempo (seg)');
    title(['Evoluci�n en el tiempo con frecuencia ' num2str(frecuencias(k+1)) ' Hz'])
    hold off
    subplot(212)
    xlabel('Frecuencia (Hz)');
    title('Espectro de las muestras tomadas')
    if (Con + Sin == 2)
        legend(leg, ['Con Sal'; 'Sin Sal'], 'Location', 'best');
        % Comparaci�n con la sinusoidal de emisi�n y desfasada seg�n el
        % paper
        original = ((max(filtradoA(length(filtradoA)/2:end))-min(filtradoA(length(filtradoA)/2:end)))/2) * cos(2*pi*frecuencias(k+1)*linspace(0,durSec(end),length(P2))+pi) + mean(filtradoA(length(filtradoA)/2:end));
        original = filter(c,d,original);
        desfasado = ((max(filtradoA(length(filtradoA)/2:end))-min(filtradoA(length(filtradoA)/2:end)))/2) * cos(2*pi*frecuencias(k+1)*linspace(0,durSec(end),length(P2))+pi-30*log10(frecuencias(k+1))*pi/180) + mean(filtradoA(length(filtradoA)/2:end));
        desfasado = filter(c,d,desfasado);
        figure(1000+frecuencias(k+1))
        subplot(211)
        leg2(2) = plot(linspace(0,durSec(end),length(original)), original, 'b');
        leg2(3) = plot(linspace(0,durSec(end),length(desfasado)), desfasado, 'g');
        ylabel('Valor lectura ADC')
        xlabel('Tiempo (seg)');
        title(['Comparaci�n senoEmisi�n y resta(RecibidoSal-RecibidoSinSal) a ' num2str(frecuencias(k+1)) ' Hz'])
        legend(leg2, 'Muestras', 'Emisi�n', 'Te�rico desfase', 'Location', 'best');
        hold off
        subplot(212)
        xlabel('Frecuencia (Hz)');
        title('Espectro de las muestras tomadas')
        hold off
    else
        minutes = 0;
        leyenda{1} = sprintf('Referencia');
        for s = 2:numVeces
            minutes = minutes + Telapsed;
            leyenda{s} = sprintf('+%d minutos', minutes);
        end
        legend(leg, leyenda, 'Location', 'best');
    end
    hold off
end

clear x s p minutes leg leg2 k h P1 P2 fftP etapas original desfasado leyenda tmp