%% Fetch data periodico
clear, clc
Con = 1;
Sin = 1;

nTest = zeros(1, Con+Sin);
numRepet = zeros(1, Con+Sin);
durSec = zeros(1, Con+Sin);
numVeces = zeros(1, Con+Sin);
Telapsed = zeros(1, Con+Sin);

if Con
    pruebaCon = csvread('pruebaConSalDeepUV.csv');
    tmp = pruebaCon(1,:);
    nTest(1) = tmp(1);
    config = zeros(Con+Sin,nTest(1)+5);
    config(1,:) = tmp(1:nTest(1)+5);
    pruebaCon = pruebaCon(2:end,1:(end-1));
    frecuencias = config(1,2:(2+nTest-1));
    numRepet(1) = config(1,nTest(1)+2);
    durSec(1) = config(1,nTest(1)+3)/1000;
    numVeces(1) = config(1,nTest(1)+4);
    Telapsed(1) = config(1,nTest(1)+5);
end
if Sin
    pruebaSin = csvread('pruebaSinSalDeepUV.csv');
    tmp = pruebaSin(1,:);
    nTest(end) = tmp(1);
    if (Con == 0)
        config = zeros(1,nTest(end)+5);
    end
    config(end,:) = tmp(1,1:nTest(end)+5);
    pruebaSin = pruebaSin(2:end,1:(end-1));
    frecuencias = config(1,2:(2+nTest-1));
    numRepet(end) = config(1,nTest(end)+2);
    durSec(end) = config(1,nTest(end)+3)/1000;
    numVeces(end) = config(1,nTest(end)+4);
    Telapsed(end) = config(1,nTest(end)+5);
end

colores = cell(3,7);
colores{1,1} = [0 1 0.9]; colores{1,2} = [1/7 1 0.9]; colores{1,3} = [2/7 1 0.9];
colores{1,4} = [3/7 1 0.9]; colores{1,5} = [4/7 1 0.9]; colores{1,6} = [5/7 1 0.9]; colores{1,7} = [6/7 1 0.9];

colores{2,1} = [0.55 1 0.5]; colores{2,2} = [0.55 1 0.5]; colores{2,3} = [0.55 1 0.5];
colores{2,4} = [0.55 1 0.5]; colores{2,5} = [0.55 1 0.5]; colores{2,6} = [0.55 1 0.5]; colores{2,7} = [0.55 1 0.5];

colores{3,1} = [0 1 0.5]; colores{3,2} = [0 1 0.5]; colores{3,3} = [0 1 0.5];
colores{3,4} = [0 1 0.5]; colores{3,5} = [0 1 0.5]; colores{3,6} = [0 1 0.5]; colores{3,7} = [0 1 0.5];

clear tmp config

%% Quitar frecuencias espurias debido a la fuente y su rectificaci�n

% Por ahora con IIR
Fs = length(pruebaCon(1,:))/durSec(1);
d = designfilt('bandstopiir', 'FilterOrder', 6, 'HalfPowerFrequency1', 95, 'HalfPowerFrequency2', 105, 'DesignMethod', 'butter', 'SampleRate', Fs);
fvtool(d, 'Fs', Fs)
buttLoop = filtfilt(d, pruebaCon(1,:));
x = linspace(0, durSec(1), length(pruebaCon(1,:)));
figure
plot(x,pruebaCon(1,:))
hold on
plot(x,buttLoop, 'r')
figure
[popen, fopen] = periodogram(pruebaCon(1,:), [], [], Fs);
[pbutt, fbutt] = periodogram(buttLoop, [], [], Fs);
plot(fopen, 20*log10(abs(popen)), fbutt, 20*log10(abs(pbutt)), '--')

pruebaCon(1,:) = filtfilt(d, pruebaCon(1,:));
pruebaCon(2,:) = filtfilt(d, pruebaCon(2,:));
pruebaCon(3,:) = filtfilt(d, pruebaCon(3,:));
pruebaSin(1,:) = filtfilt(d, pruebaSin(1,:));
pruebaSin(2,:) = filtfilt(d, pruebaSin(2,:));
pruebaSin(3,:) = filtfilt(d, pruebaSin(3,:));

clear Fs d buttLoop x popen fopen pbutt fbutt

%% Aislar frecuencia de inter�s por IIR
Fs = length(pruebaCon(5,:))/durSec(1);
d = designfilt('bandstopiir', 'FilterOrder', 6, 'HalfPowerFrequency1', 8, 'HalfPowerFrequency2', 12, 'DesignMethod', 'butter', 'SampleRate', Fs);
fvtool(d, 'Fs', Fs)
for k = 4:9
    buttLoop = filtfilt(d, pruebaCon(k,:));
    x = linspace(0, durSec(1), length(pruebaCon(k,:)));
    figure(50)
    [popen, fopen] = periodogram(pruebaCon(k,:), [], [], Fs);
    [pbutt, fbutt] = periodogram(buttLoop, [], [], Fs);
    [presta, fresta] = periodogram(pruebaCon(k,:)-buttLoop, [], [], Fs);
    plot(fresta, 20*log10(abs(presta)), 'b'); % fopen, 20*log10(abs(popen)), fbutt, 20*log10(abs(pbutt)), '--', fresta, 20*log10(abs(presta)), '--')
    hold on

    ConSalSeno(k,:) = pruebaCon(k,:)-buttLoop;
    figure(51)
    f(1) = plot(x,ConSalSeno(k,:), 'b');
    hold on
end

for k = 4:9
    buttLoop = filtfilt(d, pruebaSin(k,:));
    x = linspace(0, durSec(1), length(pruebaSin(k,:)));
    figure(50)
    [popen, fopen] = periodogram(pruebaSin(k,:), [], [], Fs);
    [pbutt, fbutt] = periodogram(buttLoop, [], [], Fs);
    [presta, fresta] = periodogram(pruebaSin(k,:)-buttLoop, [], [], Fs);
    if k >= 7
        plot(fresta, 20*log10(abs(presta)), 'k-'); % fopen, 20*log10(abs(popen)), fbutt, 20*log10(abs(pbutt)), '--', fresta, 20*log10(abs(presta)), '--')
    else
        plot(fresta, 20*log10(abs(presta)), 'r'); 
    end
    hold on

    SinSalSeno(k,:) = pruebaSin(k,:)-buttLoop;
    figure(51)
    f(2) = plot(x,SinSalSeno(k,:), 'r');
    hold on
end

title('Se�ales recibidas tras filtrado')
ylabel('Amplitud (u. a.)')
xlabel('Tiempo (seg)')
legend(f, 'Con sal', 'Sin sal')

xmuest = ConSalSeno(k,round(3.8*length(ConSalSeno(k,:))/durSec(1)):round(4.2*length(ConSalSeno(k,:))/durSec(1))-1);
xref1 = SinSalSeno(k,round(3.8*length(SinSalSeno(k,:))/durSec(1)):round(4.2*length(SinSalSeno(k,:))/durSec(1))-1);
xref2 = SinSalSeno(k,(round(3.8*length(SinSalSeno(k,:))/durSec(1))+round(length(SinSalSeno(k,:))/durSec(1)/frecuencias/4)):(round(4.2*length(SinSalSeno(k,:))/durSec(1))+round(length(SinSalSeno(k,:))/durSec(1)/frecuencias/4))-1);

figure
plot(linspace(0,.4,length(xref1)),xref1)
hold on
plot(linspace(0,.4,length(xref2)),xref2)

% pruebaCon(1,:) = pruebaCon(1,:) - filtfilt(d, pruebaCon(1,:));
% pruebaCon(2,:) = filtfilt(d, pruebaCon(2,:));
% pruebaCon(3,:) = filtfilt(d, pruebaCon(3,:));
% pruebaSin(1,:) = filtfilt(d, pruebaSin(1,:));
% pruebaSin(2,:) = filtfilt(d, pruebaSin(2,:));
% pruebaSin(3,:) = filtfilt(d, pruebaSin(3,:));

%% Aislar frecuencia de inter�s por IIR (continuacion)
buttLoop = filtfilt(d, pruebaSin(1,:));
figure
[popen, fopen] = periodogram(pruebaSin(1,:), [], [], Fs);
[pbutt, fbutt] = periodogram(buttLoop, [], [], Fs);
[presta, fresta] = periodogram(pruebaSin(1,:)-buttLoop, [], [], Fs);
plot(fopen, 20*log10(abs(popen)), fbutt, 20*log10(abs(pbutt)), '--', fresta, 20*log10(abs(presta)), '-')

figure
plot(x,pruebaSin(1,:))
hold on
plot(x,buttLoop, 'r')
plot(x,pruebaSin(1,:)-buttLoop, 'k')

SinSalSeno = pruebaSin(1,:)-buttLoop;
figure
plot(x, ConSalSeno, x, SinSalSeno)