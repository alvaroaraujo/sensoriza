/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * spi_arch.h
 *
 *  Created on: 20 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file spi_arch.h
 */

#ifndef APPLICATION_BOARD_PLATFORM_INCLUDE_SPI_ARCH_H_
#define APPLICATION_BOARD_PLATFORM_INCLUDE_SPI_ARCH_H_

#include "spi.h"
#include "sys_gpio.h"
#include "platform-conf.h"


/* Spi file private data */
typedef struct spi_data_{
	uint16_t spi_hw_num;
	uint8_t enabled_cs;
	uint8_t enabled_byte_cs;
	uint32_t speed;
	gpio_pin_t cs_gpio_pin;
	uint8_t polarity_high;
	uint8_t spi_edge_1;
	uint8_t spi_mode_master;
}spi_data_t;



retval_t spi_arch_init(uint16_t spi_hw_number);
retval_t spi_arch_deinit(uint16_t spi_hw_number);
retval_t spi_arch_write(spi_data_t* spi_data, uint8_t* txData, uint16_t size);
retval_t spi_arch_read(spi_data_t* spi_data, uint8_t* rxData, uint16_t size);
retval_t spi_arch_read_write(spi_data_t* spi_data, uint8_t* txData, uint8_t* rxData, uint16_t size);
retval_t spi_arch_set_input_hw_cs(spi_data_t* spi_data);
retval_t spi_arch_set_output_hw_cs(spi_data_t* spi_data);
retval_t spi_arch_set_sw_cs(spi_data_t* spi_data);
retval_t spi_arch_set_mode(spi_data_t* spi_data);
retval_t spi_arch_set_polarity(spi_data_t* spi_data);
retval_t spi_arch_set_edge(spi_data_t* spi_data);
retval_t spi_arch_set_speed(spi_data_t* spi_data);


#endif /* APPLICATION_BOARD_PLATFORM_INCLUDE_SPI_ARCH_H_ */
