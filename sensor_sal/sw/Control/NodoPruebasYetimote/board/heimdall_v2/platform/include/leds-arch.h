/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * leds-arch.h
 *
 *  Created on: 28 de ago. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file leds-arch.h
 */
#ifndef APPLICATION_BOARD_HEIMDALL_V2_LEDS_LEDS_ARCH_H_
#define APPLICATION_BOARD_HEIMDALL_V2_LEDS_LEDS_ARCH_H_


/* LED ports */
#define LEDS_PORT_B	GPIOB
#define LEDS_BLUE_PIN	GPIO_PIN_5
#define LEDS_GREEN_PIN	GPIO_PIN_4
#define LEDS_PORT_A	GPIOA
#define LEDS_RED1_PIN	GPIO_PIN_8
#define LEDS_RED2_PIN	GPIO_PIN_15


/* LEDS_GREEN */
#ifndef LEDS_GREEN
#define LEDS_GREEN  1
#endif
/* LEDS_BLUE */
#ifndef LEDS_BLUE
#define LEDS_BLUE  2
#endif /* LEDS_BLUE */
/* LEDS_RED */
#ifndef LEDS_RED1
#define LEDS_RED1  4
#endif /* LEDS_RED*/
/* LEDS_RED */
#ifndef LEDS_RED2
#define LEDS_RED2  8
#endif /* LEDS_RED*/
#ifdef LEDS_CONF_ALL
#define LEDS_ALL    LEDS_CONF_ALL
#else /* LEDS_CONF_ALL */
#define LEDS_ALL    15
#endif /* LEDS_CONF_ALL */

/*
 * Leds implementation
 */
void leds_arch_init(void);
unsigned char leds_arch_get(void);
void leds_arch_set(unsigned char leds);

#endif /* APPLICATION_BOARD_HEIMDALL_V2_LEDS_LEDS_ARCH_H_ */
