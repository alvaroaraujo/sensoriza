/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * spi_arch.c
 *
 *  Created on: 20 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file spi_arch.c
 */


#include "uart_arch.h"
#include "usart.h"
#include "system_api.h"

#define DEFAULT_TIMEOUT	500

extern DMA_HandleTypeDef hdma_usart2_rx;

static uint32_t uart_tx_semaphore_id;

static uint32_t uart_rx_semaphore_id;

/**
 *
 * @return
 */
retval_t uart_arch_init(){


	if((uart_tx_semaphore_id = ytSemaphoreCreate(1)) == 0){
		return RET_ERROR;
	}
	if((uart_rx_semaphore_id = ytSemaphoreCreate(1)) == 0){
		return RET_ERROR;
	}
	ytSemaphoreWait(uart_tx_semaphore_id, YT_WAIT_FOREVER);
	ytSemaphoreWait(uart_rx_semaphore_id, YT_WAIT_FOREVER);
	MX_USART2_UART_Init();

	/* Asegurarse que la DMA est� en modo normal (no circular), ya que cuando se usa como stdio est� en modo circular por defecto */
    hdma_usart2_rx.Instance = DMA1_Channel6;
    hdma_usart2_rx.Init.Mode = DMA_NORMAL;

    if (HAL_DMA_Init(&hdma_usart2_rx) != HAL_OK)
    {
      _Error_Handler(__FILE__, __LINE__);
    }

	return RET_OK;
}

/**
 *
 * @return
 */
retval_t uart_arch_deinit(){

	ytSemaphoreDelete(uart_tx_semaphore_id);
	ytSemaphoreDelete(uart_rx_semaphore_id);
	HAL_UART_DeInit(&huart2);

	return RET_OK;
}


/**
 *
 * @param txData
 * @param size
 * @return
 */
retval_t uart_arch_write(uint8_t* txData, uint16_t size){

	HAL_UART_Transmit_DMA(&huart2, txData, size);
	if(ytSemaphoreWait(uart_tx_semaphore_id, DEFAULT_TIMEOUT) != RET_OK){
		return RET_ERROR;
	}

	return RET_OK;
}

/**
 *
 * @param rxData
 * @param size
 * @return
 */
retval_t uart_arch_read(uint8_t* rxData, uint16_t size){

	HAL_UART_Receive_DMA(&huart2, rxData, size);
	if(ytSemaphoreWait(uart_rx_semaphore_id, DEFAULT_TIMEOUT) != RET_OK){
		return RET_ERROR;
	}


	return RET_OK;
}

/**
 *
 * @param speed
 * @return
 */
retval_t uart_arch_set_speed(uint32_t speed){
	huart2.Instance = USART2;
	huart2.Init.BaudRate = speed;

	if (HAL_UART_Init(&huart2) != HAL_OK)
	{
	_Error_Handler(__FILE__, __LINE__);
	}
	return RET_OK;
}

/**
 *
 * @param parity
 * @return
 */
retval_t uart_arch_set_parity(uint16_t parity){
	huart2.Instance = USART2;

	switch(parity){
	case 0:
		huart2.Init.Parity = UART_PARITY_NONE;
		break;
	case 1:
		huart2.Init.Parity = UART_PARITY_EVEN;
		break;
	case 2:
		huart2.Init.Parity = UART_PARITY_ODD;
		break;
	default:
		return RET_ERROR;
		break;
	}

	if (HAL_UART_Init(&huart2) != HAL_OK)
	{
	_Error_Handler(__FILE__, __LINE__);
	}
	return RET_OK;
}


void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart){
	if(huart->Instance == USART2){
		ytSemaphoreRelease(uart_tx_semaphore_id);
	}
}
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){
	if(huart->Instance == USART2){
		ytSemaphoreRelease(uart_rx_semaphore_id);
	}
}
