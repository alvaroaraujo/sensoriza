/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * main_process.c
 *
 *  Created on: 28 de ago. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file main_process.c
 */

#include "platform-conf.h"
#include "bsp_heimdall_v2.h"
#include "iwdg.h"
#include "leds.h"
#include "fatfs.h"
#include "rtc_time.h"
#include "time_meas.h"
#include <stdio.h>
#include "usbd_cdc_if.h"
#include <string.h>
#include "platform_stdio.h"
#include "yetimote_stdio.h"
#include "usart.h"
#include "process.h"
#include "mainWatch.h"
#include "device.h"
#include "sys_gpio.h"
#include "arm_math.h"
#include "adc_arch.h"
#include "system_api.h"

#define WATCH_MSG_QUEUE_SIZE	2

#define TOGGLE_LED_PERIOD		2000
#define TOGGLE_LED_DURATION		50

osMessageQDef(watchMsgBox, WATCH_MSG_QUEUE_SIZE, uint32_t);
osMessageQId  watchMsgBoxId;

static void mainDelayWatchMsg(uint32_t time);


void main_process(){

	watchMsgBoxId = osMessageCreate(osMessageQ(watchMsgBox), NULL);

	init_platform();

	while(1){
#if USE_HW_WATCHDOG
//		HAL_IWDG_Refresh(&hiwdg);		//The watchdog resets the systems if no activity is detected in 5 seconds
#endif
		ytLedsOn(LEDS_GREEN);
		mainDelayWatchMsg(TOGGLE_LED_DURATION);
		ytLedsOff(LEDS_GREEN);

		mainDelayWatchMsg(TOGGLE_LED_PERIOD-TOGGLE_LED_DURATION);

	}
}

/**
 * @brief		This function listens to messages during delay periods for the main process. For example it calls the osExitProcess function when a message to do it is received
 * @param time	Time to delay
 */
static void mainDelayWatchMsg(uint32_t time){
	uint32_t t1, t2;
	mainWatchMsg_t* mainWatchMsg;
	osEvent evt;

	t1 = osKernelSysTick();
	t2 = osKernelSysTick();

	while(t2-t1 < (time)){
		evt = osMessageGet(watchMsgBoxId, (time) - (t2-t1));  // wait for message
		if (evt.status == osEventMessage) {
			mainWatchMsg = evt.value.p;
			switch(mainWatchMsg->MsgType){
			case EXIT_TASK_MSG:
				osExitProcess((uint32_t)(mainWatchMsg->data.uintData));
				break;
			case ERROR_MSG:
#ifdef ERROR_LED
				ytLedsOn(ERROR_LED);
#endif
				osThreadSuspendAll();
				while(1);
				break;
			case NO_MSG:
				break;
			default:
				break;
			}
		}
		t2 = osKernelSysTick();
	}
}

mainWatchMsg_t* createMainWatchMsg(watchMsgType_t watchMsgType, void* data){
	mainWatchMsg_t* mainWatchMsg = (mainWatchMsg_t*) ytMalloc(sizeof(mainWatchMsg_t));
	if(mainWatchMsg == NULL){
		return NULL;
	}
	mainWatchMsg->MsgType = watchMsgType;
	mainWatchMsg->data.ptrData = data;
	return mainWatchMsg;
}
