/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * spi_arch.c
 *
 *  Created on: 20 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file spi_arch.c
 */


#include "spi_arch.h"
#include "system_api.h"

#define DEFAULT_TIMEOUT	500


static uint32_t spi1_semaphore_id;

static uint32_t spi2_semaphore_id;

/**
 *
 * @param spi_hw_number
 * @return
 */
retval_t spi_arch_init(uint16_t spi_hw_number){

	if(spi_hw_number == 1){
		if((spi1_semaphore_id = ytSemaphoreCreate(1)) == 0){
			return RET_ERROR;
		}
		ytSemaphoreWait(spi1_semaphore_id, YT_WAIT_FOREVER);
		MX_SPI1_Init();
	}
	else if(spi_hw_number == 2){
		if((spi2_semaphore_id = ytSemaphoreCreate(1)) == 0){
			return RET_ERROR;
		}
		ytSemaphoreWait(spi2_semaphore_id, YT_WAIT_FOREVER);
		MX_SPI2_Init();
	}
	else{
		return RET_ERROR;
	}
	return RET_OK;
}

/**
 *
 * @param spi_hw_number
 * @return
 */
retval_t spi_arch_deinit(uint16_t spi_hw_number){

	if(spi_hw_number == 1){
		ytSemaphoreDelete(spi1_semaphore_id);
		HAL_SPI_DeInit(&hspi1);
	}
	else if(spi_hw_number == 2){
		ytSemaphoreDelete(spi2_semaphore_id);
		HAL_SPI_DeInit(&hspi2);
	}
	else{
		return RET_ERROR;
	}
	return RET_OK;
}

/**
 *
 * @param spi_data
 * @param txData
 * @param size
 * @return
 */
retval_t spi_arch_write(spi_data_t* spi_data, uint8_t* txData, uint16_t size){

	if(spi_data->spi_hw_num == 1){

		HAL_SPI_Transmit_DMA(&hspi1, txData, size);
		if(ytSemaphoreWait(spi1_semaphore_id, DEFAULT_TIMEOUT) != RET_OK){
			return RET_ERROR;
		}
	}
	else if(spi_data->spi_hw_num == 2){
		HAL_SPI_Transmit_DMA(&hspi2, txData, size);
		if(ytSemaphoreWait(spi2_semaphore_id, DEFAULT_TIMEOUT) != RET_OK){
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}


	return RET_OK;
}

/**
 *
 * @param spi_data
 * @param rxData
 * @param size
 * @return
 */
retval_t spi_arch_read(spi_data_t* spi_data, uint8_t* rxData, uint16_t size){

	if(spi_data->spi_hw_num == 1){
		HAL_SPI_Receive_DMA(&hspi1, rxData, size);
		if(ytSemaphoreWait(spi1_semaphore_id, DEFAULT_TIMEOUT) != RET_OK){
			return RET_ERROR;
		}
	}
	else if(spi_data->spi_hw_num == 2){
		HAL_SPI_Receive_DMA(&hspi2, rxData, size);
		if(ytSemaphoreWait(spi2_semaphore_id, DEFAULT_TIMEOUT) != RET_OK){
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}


	return RET_OK;
}

/**
 *
 * @param spi_data
 * @param txData
 * @param rxData
 * @param size
 * @return
 */
retval_t spi_arch_read_write(spi_data_t* spi_data, uint8_t* txData, uint8_t* rxData, uint16_t size){

	if(spi_data->spi_hw_num == 1){
		HAL_SPI_TransmitReceive_DMA(&hspi1, txData, rxData, size);
		if(ytSemaphoreWait(spi1_semaphore_id, DEFAULT_TIMEOUT) != RET_OK){
			return RET_ERROR;
		}
	}
	else if(spi_data->spi_hw_num == 2){
		HAL_SPI_TransmitReceive_DMA(&hspi2, txData, rxData, size);
		if(ytSemaphoreWait(spi2_semaphore_id, DEFAULT_TIMEOUT) != RET_OK){
			return RET_ERROR;
		}
	}
	else{
		return RET_ERROR;
	}


	return RET_OK;
}

/**
 *
 * @param spi_data
 * @return
 */
retval_t spi_arch_set_input_hw_cs(spi_data_t* spi_data){
	GPIO_InitTypeDef GPIO_InitStruct;

	switch(spi_data->spi_hw_num){
	case 1:
		hspi1.Instance = SPI1;
		hspi1.Init.NSS = SPI_NSS_HARD_INPUT;

		GPIO_InitStruct.Pin = GPIO_PIN_4;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_PULLUP;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
		HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

		if (HAL_SPI_Init(&hspi1) != HAL_OK)
		{
			return RET_ERROR;
		}
		break;
	case 2:
		hspi2.Instance = SPI2;
		hspi2.Init.NSS = SPI_NSS_HARD_INPUT;

		GPIO_InitStruct.Pin = GPIO_PIN_12;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_PULLUP;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF5_SPI2;
		HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

		if (HAL_SPI_Init(&hspi2) != HAL_OK)
		{
			return RET_ERROR;
		}
		break;

	default:
		return RET_ERROR;	//Wrong hardware spi number
		break;
	}
	return RET_OK;
}

/**
 *
 * @param spi_data
 * @return
 */
retval_t spi_arch_set_output_hw_cs(spi_data_t* spi_data){
	GPIO_InitTypeDef GPIO_InitStruct;

	switch(spi_data->spi_hw_num){
	case 1:
		hspi1.Instance = SPI1;
		hspi1.Init.NSS = SPI_NSS_HARD_OUTPUT;

		GPIO_InitStruct.Pin = GPIO_PIN_4;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_PULLUP;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
		HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

		if (HAL_SPI_Init(&hspi1) != HAL_OK)
		{
			return RET_ERROR;
		}
		break;
	case 2:
		hspi2.Instance = SPI2;
		hspi2.Init.NSS = SPI_NSS_HARD_OUTPUT;

		GPIO_InitStruct.Pin = GPIO_PIN_12;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_PULLUP;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF5_SPI2;
		HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

		if (HAL_SPI_Init(&hspi2) != HAL_OK)
		{
			return RET_ERROR;
		}
		break;

	default:
		return RET_ERROR;	//Wrong hardware spi number
		break;
	}
	return RET_OK;
}

/**
 *
 * @param spi_data
 * @return
 */
retval_t spi_arch_set_sw_cs(spi_data_t* spi_data){
	switch(spi_data->spi_hw_num){
	case 1:
		hspi1.Instance = SPI1;
		hspi1.Init.NSS = SPI_NSS_SOFT;

		if (HAL_SPI_Init(&hspi1) != HAL_OK)
		{
			return RET_ERROR;
		}
		break;
	case 2:
		hspi2.Instance = SPI2;
		hspi2.Init.NSS = SPI_NSS_SOFT;

		if (HAL_SPI_Init(&hspi2) != HAL_OK)
		{
			return RET_ERROR;
		}
		break;

	default:
		return RET_ERROR;	//Wrong hardware spi number
		break;
	}
	return RET_OK;
}

/**
 *
 * @param spi_data
 * @return
 */
retval_t spi_arch_set_mode(spi_data_t* spi_data){

	switch(spi_data->spi_hw_num){
	case 1:
		hspi1.Instance = SPI1;
		if(spi_data->spi_mode_master == 1){
			hspi1.Init.Mode = SPI_MODE_MASTER;
		}
		else{
			hspi1.Init.Mode = SPI_MODE_SLAVE;
		}
		if (HAL_SPI_Init(&hspi1) != HAL_OK)
		{
			return RET_ERROR;
		}
		break;
	case 2:
		hspi2.Instance = SPI2;
		if(spi_data->spi_mode_master == 1){
			hspi2.Init.Mode = SPI_MODE_MASTER;
		}
		else{
			hspi2.Init.Mode = SPI_MODE_SLAVE;
		}
		if (HAL_SPI_Init(&hspi2) != HAL_OK)
		{
			return RET_ERROR;
		}
		break;

	default:
		return RET_ERROR;	//Wrong hardware spi number
		break;
	}
	return RET_OK;
}


/**
 *
 * @param spi_data
 * @return
 */
retval_t spi_arch_set_polarity(spi_data_t* spi_data){

	switch(spi_data->spi_hw_num){
	case 1:
		hspi1.Instance = SPI1;
		if(spi_data->polarity_high == 1){
			hspi1.Init.CLKPolarity = SPI_POLARITY_HIGH;
		}
		else{
			hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
		}
		if (HAL_SPI_Init(&hspi1) != HAL_OK)
		{
			return RET_ERROR;
		}
		break;
	case 2:
		hspi2.Instance = SPI2;
		if(spi_data->polarity_high == 1){
			hspi2.Init.CLKPolarity = SPI_POLARITY_HIGH;
		}
		else{
			hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
		}
		if (HAL_SPI_Init(&hspi2) != HAL_OK)
		{
			return RET_ERROR;
		}
		break;

	default:
		return RET_ERROR;	//Wrong hardware spi number
		break;
	}
	return RET_OK;
}


/**
 *
 * @param spi_data
 * @return
 */
retval_t spi_arch_set_edge(spi_data_t* spi_data){

	switch(spi_data->spi_hw_num){
	case 1:
		hspi1.Instance = SPI1;
		if(spi_data->spi_edge_1 == 1){
			hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
		}
		else{
			hspi1.Init.CLKPhase = SPI_PHASE_2EDGE;
		}
		if (HAL_SPI_Init(&hspi1) != HAL_OK)
		{
			return RET_ERROR;
		}
		break;
	case 2:
		hspi2.Instance = SPI2;
		if(spi_data->spi_edge_1 == 1){
			hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
		}
		else{
			hspi2.Init.CLKPhase = SPI_PHASE_2EDGE;
		}
		if (HAL_SPI_Init(&hspi2) != HAL_OK)
		{
			return RET_ERROR;
		}
		break;

	default:
		return RET_ERROR;	//Wrong hardware spi number
		break;
	}
	return RET_OK;
}


/**
 *
 * @param spi_data
 * @return
 */
retval_t spi_arch_set_speed(spi_data_t* spi_data){

	uint32_t prescaler = (uint32_t) HAL_RCC_GetHCLKFreq()/spi_data->speed;	//SET SPI SPEED. IT IS DONE WITH THE BAUDRATE PRESCALER

	if(prescaler > 128){												//SPI BAUDRATE IS CALCULATED TO SET SPEED TO THE NEAREST LOWER VALUE
		spi_data->speed = (uint32_t) HAL_RCC_GetHCLKFreq()/256;
		prescaler = SPI_BAUDRATEPRESCALER_256;
	}
	else if(prescaler > 64){
		spi_data->speed = (uint32_t) HAL_RCC_GetHCLKFreq()/128;
		prescaler = SPI_BAUDRATEPRESCALER_128;
	}
	else if(prescaler > 32){
		spi_data->speed = (uint32_t) HAL_RCC_GetHCLKFreq()/64;
		prescaler = SPI_BAUDRATEPRESCALER_64;
	}
	else if(prescaler > 16){
		spi_data->speed = (uint32_t) HAL_RCC_GetHCLKFreq()/32;
		prescaler = SPI_BAUDRATEPRESCALER_32;
	}
	else if(prescaler > 8){
		spi_data->speed = (uint32_t) HAL_RCC_GetHCLKFreq()/16;
		prescaler = SPI_BAUDRATEPRESCALER_16;
	}
	else if(prescaler > 4){
		spi_data->speed = (uint32_t) HAL_RCC_GetHCLKFreq()/8;
		prescaler = SPI_BAUDRATEPRESCALER_8;
	}
	else if(prescaler > 2){
		spi_data->speed = (uint32_t) HAL_RCC_GetHCLKFreq()/4;
		prescaler = SPI_BAUDRATEPRESCALER_4;
	}
	else if(prescaler >= 0){
		spi_data->speed = (uint32_t) HAL_RCC_GetHCLKFreq()/2;
		prescaler = SPI_BAUDRATEPRESCALER_2;
	}
	else{
		return RET_ERROR;
	}

	switch(spi_data->spi_hw_num){
	case 1:
		hspi1.Instance = SPI1;
		hspi1.Init.BaudRatePrescaler = prescaler;

		if (HAL_SPI_Init(&hspi1) != HAL_OK)
		{
			return RET_ERROR;
		}
		break;
	case 2:
		hspi2.Instance = SPI2;
		hspi2.Init.BaudRatePrescaler = prescaler;
		if (HAL_SPI_Init(&hspi2) != HAL_OK)
		{
			return RET_ERROR;
		}
		break;

	default:
		return RET_ERROR;	//Wrong hardware spi number
		break;
	}
	return RET_OK;
}

/**
 *
 * @param spi_data
 * @return
 */
retval_t spi_arch_set_config(spi_data_t* spi_data){


	SPI_InitTypeDef tempSpiInit;


	if(spi_data->spi_mode_master == 1){
		tempSpiInit.Mode = SPI_MODE_MASTER;
	}
	else{
		tempSpiInit.Mode = SPI_MODE_SLAVE;
	}

	tempSpiInit.Direction = SPI_DIRECTION_2LINES;
	tempSpiInit.DataSize = SPI_DATASIZE_8BIT;

	if(spi_data->polarity_high == 1){
		tempSpiInit.CLKPolarity = SPI_POLARITY_HIGH;
	}
	else{
		tempSpiInit.CLKPolarity = SPI_POLARITY_LOW;
	}

	if(spi_data->spi_edge_1 == 1){
		tempSpiInit.CLKPhase = SPI_PHASE_1EDGE;
	}
	else{
		tempSpiInit.CLKPhase = SPI_PHASE_2EDGE;
	}

	tempSpiInit.NSS = SPI_NSS_SOFT;


	uint32_t prescaler = (uint32_t) HAL_RCC_GetHCLKFreq()/spi_data->speed;	//SET SPI SPEED. IT IS DONE WITH THE BAUDRATE PRESCALER

	if(prescaler > 128){												//SPI BAUDRATE IS CALCULATED TO SET SPEED TO THE NEAREST LOWER VALUE
		spi_data->speed = (uint32_t) HAL_RCC_GetHCLKFreq()/256;
		tempSpiInit.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_256;
	}
	else if(prescaler > 64){
		spi_data->speed = (uint32_t) HAL_RCC_GetHCLKFreq()/128;
		tempSpiInit.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_128;
	}
	else if(prescaler > 32){
		spi_data->speed = (uint32_t) HAL_RCC_GetHCLKFreq()/64;
		tempSpiInit.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_64;
	}
	else if(prescaler > 16){
		spi_data->speed = (uint32_t) HAL_RCC_GetHCLKFreq()/32;
		tempSpiInit.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_32;
	}
	else if(prescaler > 8){
		spi_data->speed = (uint32_t) HAL_RCC_GetHCLKFreq()/16;
		tempSpiInit.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_16;
	}
	else if(prescaler > 4){
		spi_data->speed = (uint32_t) HAL_RCC_GetHCLKFreq()/8;
		tempSpiInit.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_8;
	}
	else if(prescaler > 2){
		spi_data->speed = (uint32_t) HAL_RCC_GetHCLKFreq()/4;
		tempSpiInit.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_4;
	}
	else if(prescaler >= 0){
		spi_data->speed = (uint32_t) HAL_RCC_GetHCLKFreq()/2;
		tempSpiInit.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
	}
	else{
		return RET_ERROR;
	}


	switch(spi_data->spi_hw_num){	//The hardware spi device used. For the yetimote STM32L4 SPI2 is used for transceiver control. If another SPI is used, just add a case with the number.
	case 1:
		hspi1.Instance = SPI1;
		hspi1.Init.Mode = tempSpiInit.Mode;
		hspi1.Init.Direction = tempSpiInit.Direction;
		hspi1.Init.DataSize = tempSpiInit.DataSize;
		hspi1.Init.CLKPolarity = tempSpiInit.CLKPolarity;
		hspi1.Init.CLKPhase = tempSpiInit.CLKPhase;
		hspi1.Init.NSS = tempSpiInit.NSS;
		hspi1.Init.BaudRatePrescaler = tempSpiInit.BaudRatePrescaler;
		hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
		hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
		hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
		hspi1.Init.CRCPolynomial = 7;
		hspi1.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
		hspi1.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
		if (HAL_SPI_Init(&hspi1) != HAL_OK)
		{
			return RET_ERROR;
		}
		break;
	case 2:
		hspi2.Instance = SPI2;
		hspi2.Init.Mode = tempSpiInit.Mode;
		hspi2.Init.Direction = tempSpiInit.Direction;
		hspi2.Init.DataSize = tempSpiInit.DataSize;
		hspi2.Init.CLKPolarity = tempSpiInit.CLKPolarity;
		hspi2.Init.CLKPhase = tempSpiInit.CLKPhase;
		hspi2.Init.NSS = tempSpiInit.NSS;
		hspi2.Init.BaudRatePrescaler = tempSpiInit.BaudRatePrescaler;
		hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
		hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
		hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
		hspi2.Init.CRCPolynomial = 7;
		hspi2.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
		hspi2.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
		if (HAL_SPI_Init(&hspi2) != HAL_OK)
		{
			return RET_ERROR;
		}

		break;

	default:
		return RET_ERROR;	//Wrong hardware spi number
		break;
	}

	return RET_OK;
}


void HAL_SPI_TxCpltCallback(SPI_HandleTypeDef *hspi){
	if(hspi->Instance == SPI1){
		ytSemaphoreRelease(spi1_semaphore_id);
	}
	else if(hspi->Instance == SPI2){
		ytSemaphoreRelease(spi2_semaphore_id);
	}
}


void HAL_SPI_RxCpltCallback(SPI_HandleTypeDef *hspi){
	if(hspi->Instance == SPI1){
		ytSemaphoreRelease(spi1_semaphore_id);
	}
	else if(hspi->Instance == SPI2){
		ytSemaphoreRelease(spi2_semaphore_id);
	}
}

void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef *hspi){
	if(hspi->Instance == SPI1){
		ytSemaphoreRelease(spi1_semaphore_id);
	}
	else if(hspi->Instance == SPI2){
		ytSemaphoreRelease(spi2_semaphore_id);
	}
}
