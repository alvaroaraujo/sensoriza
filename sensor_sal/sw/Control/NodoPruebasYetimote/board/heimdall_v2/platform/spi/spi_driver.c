/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * spi_driver.c
 *
 *  Created on: 12 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file spi_driver.c
 */

#include "spi_arch.h"
#include "device.h"
#include "platform-conf.h"
#include "yetimote_stdio.h"
#include "sys_gpio.h"
#include "low_power.h"
#include "spi_driver.h"
#include "process.h"
#include "system_api.h"

#define DEBUG 1
#if DEBUG
#include "yetimote_stdio.h"
#define PRINTF(...) _printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

#if ENABLE_SPI_DRIVER

/* Device name and Id */
#define SPI1_DEVICE_NAME SPI_DEV_1

#define SPI1_HW_NUMBER	1

/* Device name and Id */
#define SPI2_DEVICE_NAME SPI_DEV_2

#define SPI2_HW_NUMBER	2

static uint32_t spi1_mutex_id;

static uint32_t spi2_mutex_id;

/* Device Init functions */
static retval_t spi_init(void);
static retval_t spi_exit(void);


/* Device driver operation functions declaration */
static retval_t spi_open(device_t* device, dev_file_t* filep);
static retval_t spi_close(dev_file_t* filep);
static size_t spi_read(dev_file_t* filep, uint8_t* prx, size_t size);
static size_t spi_write(dev_file_t* filep, uint8_t* ptx, size_t size);
static retval_t spi_ioctl(dev_file_t* filep, uint16_t request, void* args);

static size_t spi_read_write(dev_file_t* filep, uint8_t* ptx, uint8_t* prx, size_t size);

/* Define driver operations */
static driver_ops_t spi_driver_ops = {
	.open = spi_open,
	.close = spi_close,
	.read = spi_read,
	.write = spi_write,
	.ioctl = spi_ioctl,
};


/**
 *
 * @return
 */
static retval_t spi_init(void){
	if(spi_arch_init(SPI1_HW_NUMBER) != RET_OK){				//Inicialización del HW del spi 1
		return RET_ERROR;
	}
	if(registerDevice(SPI1_DEVICE_ID, &spi_driver_ops, SPI1_DEVICE_NAME) != RET_OK){
		return RET_ERROR;
	}
	spi1_mutex_id = ytMutexCreate();	//Initialize blocking mutex
	PRINTF(">SPI 1 Init Done\r\n");

	if(spi_arch_init(SPI2_HW_NUMBER) != RET_OK){				//Inicialización del HW del spi 2
		return RET_ERROR;
	}
	if(registerDevice(SPI2_DEVICE_ID, &spi_driver_ops, SPI2_DEVICE_NAME) != RET_OK){
		return RET_ERROR;
	}
	spi2_mutex_id = ytMutexCreate();	//Initialize blocking mutex
	PRINTF(">SPI 2 Init Done\r\n");

	return RET_OK;
}

/**
 *
 * @return
 */
static retval_t spi_exit(void){
	if(spi_arch_deinit(SPI1_HW_NUMBER) != RET_OK){				//De-Inicialización del HW del spi 1
		return RET_ERROR;
	}
	unregisterDevice(SPI1_DEVICE_ID, SPI1_DEVICE_NAME);
	ytMutexDelete(spi1_mutex_id);
	PRINTF(">SPI 1 Exit Done\r\n");

	if(spi_arch_deinit(SPI2_HW_NUMBER) != RET_OK){				//De-Inicialización del HW del spi 2
		return RET_ERROR;
	}
	unregisterDevice(SPI2_DEVICE_ID, SPI2_DEVICE_NAME);
	ytMutexDelete(spi2_mutex_id);
	PRINTF(">SPI 2 Exit Done\r\n");
	return RET_OK;
}

/**
 *
 * @param device
 * @param filep
 * @return
 */
static retval_t spi_open(device_t* device, dev_file_t* filep){
	spi_data_t* spi_data;
	if(device->device_state != DEV_STATE_INIT){
		return RET_ERROR;
	}
	if((strcmp(device->device_name, SPI1_DEVICE_NAME)) == 0){

		spi_data = (spi_data_t*) ytMalloc(sizeof(spi_data_t));
		if(spi_data == NULL){
			return RET_ERROR;
		}
		ytMutexWait(spi1_mutex_id, YT_WAIT_FOREVER);

		spi_data->spi_hw_num = SPI1_HW_NUMBER;
		spi_data->enabled_cs = 0;
		spi_data->enabled_byte_cs = 0;
		spi_data->speed = 6000000;
		spi_data->polarity_high = 0;
		spi_data->spi_edge_1 = 0;
		spi_data->spi_mode_master = 1;
		spi_data->cs_gpio_pin = 0;
		filep->private_data = (void*) spi_data;
		ytMutexRelease(spi1_mutex_id);
	}
	else if((strcmp(device->device_name, SPI2_DEVICE_NAME)) == 0){
		spi_data = (spi_data_t*) ytMalloc(sizeof(spi_data_t));
		if(spi_data == NULL){
			return RET_ERROR;
		}
		ytMutexWait(spi2_mutex_id, YT_WAIT_FOREVER);

		spi_data->spi_hw_num = SPI2_HW_NUMBER;
		spi_data->enabled_cs = 0;
		spi_data->enabled_byte_cs = 0;
		spi_data->speed = 6000000;
		spi_data->polarity_high = 0;
		spi_data->spi_edge_1 = 0;
		spi_data->spi_mode_master = 1;
		spi_data->cs_gpio_pin = 0;
		filep->private_data = (void*) spi_data;
		ytMutexRelease(spi2_mutex_id);
	}
	else{
		return RET_ERROR;
	}
	return RET_OK;
}

/**
 *
 * @param filep
 * @return
 */
static retval_t spi_close(dev_file_t* filep){
	if(filep->private_data != NULL){
		spi_data_t* spi_data = filep->private_data;
		if(spi_data->spi_hw_num == 1){
			ytMutexWait(spi1_mutex_id, YT_WAIT_FOREVER);
		}
		else if(spi_data->spi_hw_num == 2){
			ytMutexWait(spi2_mutex_id, YT_WAIT_FOREVER);
		}

		if(spi_data->enabled_cs){
			ytGpioDeInitPin(spi_data->cs_gpio_pin);
		}
		ytFree(spi_data);

		if(spi_data->spi_hw_num == 1){
			ytMutexRelease(spi1_mutex_id);
		}
		else if(spi_data->spi_hw_num == 2){
			ytMutexRelease(spi2_mutex_id);
		}
		return RET_OK;
	}
	return RET_ERROR;
}

/**
 *
 * @param filep
 * @param ptx
 * @param size
 * @return
 */
static size_t spi_write(dev_file_t* filep, uint8_t* ptx, size_t size){
	spi_data_t* spi_data =  (spi_data_t*) filep->private_data;
	uint16_t i;
	retval_t ret;
	if(spi_data->spi_hw_num == 1){
		ytMutexWait(spi1_mutex_id, YT_WAIT_FOREVER);
	}
	else if(spi_data->spi_hw_num == 2){
		ytMutexWait(spi2_mutex_id, YT_WAIT_FOREVER);
	}
	disable_low_power_mode();

	if(spi_data->enabled_cs){

		if(spi_data->enabled_byte_cs){

			for(i=0; i<size; i++){
				ytGpioPinReset(spi_data->cs_gpio_pin);
				ret = spi_arch_write(spi_data, ptx+i, 1);
				ytGpioPinSet(spi_data->cs_gpio_pin);
			}
		}
		else{
			ytGpioPinReset(spi_data->cs_gpio_pin);
			ret = spi_arch_write(spi_data, ptx, (uint16_t) size);
			ytGpioPinSet(spi_data->cs_gpio_pin);
		}

	}
	else{
		 ret = spi_arch_write(spi_data, ptx, (uint16_t) size);
	}

	enable_low_power_mode();

	if(spi_data->spi_hw_num == 1){
		ytMutexRelease(spi1_mutex_id);
	}
	else if(spi_data->spi_hw_num == 2){
		ytMutexRelease(spi2_mutex_id);
	}


	if (ret != RET_OK){
		return 0;
	}
	else{
		return size;
	}
}


/**
 *
 * @param filep
 * @param prx
 * @param size
 * @return
 */
static size_t spi_read(dev_file_t* filep, uint8_t* prx, size_t size){

	spi_data_t* spi_data =  (spi_data_t*) filep->private_data;
	uint16_t i;
	retval_t ret;
	if(spi_data->spi_hw_num == 1){
		ytMutexWait(spi1_mutex_id, YT_WAIT_FOREVER);
	}
	else if(spi_data->spi_hw_num == 2){
		ytMutexWait(spi2_mutex_id, YT_WAIT_FOREVER);
	}

	disable_low_power_mode();

	if(spi_data->enabled_cs){

		if(spi_data->enabled_byte_cs){

			for(i=0; i<size; i++){
				ytGpioPinReset(spi_data->cs_gpio_pin);
				ret = spi_arch_read(spi_data, prx+i, 1);
				ytGpioPinSet(spi_data->cs_gpio_pin);
			}
		}
		else{
			ytGpioPinReset(spi_data->cs_gpio_pin);
			ret = spi_arch_read(spi_data, prx, (uint16_t) size);
			ytGpioPinSet(spi_data->cs_gpio_pin);
		}

	}
	else{
		 ret = spi_arch_read(spi_data, prx, (uint16_t) size);
	}

	enable_low_power_mode();

	if(spi_data->spi_hw_num == 1){
		ytMutexRelease(spi1_mutex_id);
	}
	else if(spi_data->spi_hw_num == 2){
		ytMutexRelease(spi2_mutex_id);
	}


	if (ret != RET_OK){
		return 0;
	}
	else{
		return size;
	}
}

/**
 *
 * @param filep
 * @param request
 * @param args
 * @return
 */
static retval_t spi_ioctl(dev_file_t* filep, uint16_t request, void* args){
	spi_data_t* spi_data;
	gpio_pin_t gpio_pin;
	spi_driver_ioctl_cmd_t cmd;
	spi_read_write_buffs_t* read_write_buffs;
	retval_t ret = RET_OK;
	if(filep == NULL){
		return RET_ERROR;
	}
	spi_data = (spi_data_t*) filep->private_data;
	cmd = (spi_driver_ioctl_cmd_t) request;

	if(spi_data->spi_hw_num == 1){
		ytMutexWait(spi1_mutex_id, YT_WAIT_FOREVER);
	}
	else if(spi_data->spi_hw_num == 2){
		ytMutexWait(spi2_mutex_id, YT_WAIT_FOREVER);
	}

	switch(cmd){
	case ENABLE_SW_CS:
		spi_arch_set_sw_cs(spi_data);
		gpio_pin = (gpio_pin_t) args;
		if(ytGpioInitPin(gpio_pin, GPIO_PIN_OUTPUT_OPEN_DRAIN, GPIO_PIN_PULLUP) != RET_OK){
			ret = RET_ERROR;
			break;
		}
		ytGpioPinSet(gpio_pin);
		spi_data->cs_gpio_pin = gpio_pin;
		spi_data->enabled_cs = 1;
		break;

	case DISABLE_SW_CS:
		gpio_pin = (gpio_pin_t) args;
		if(ytGpioDeInitPin(gpio_pin) != RET_OK){
			ret = RET_ERROR;
			break;
		}
		spi_data->enabled_cs = 0;
		break;

	case ENABLE_SW_CS_EACH_BYTE:
		spi_data->enabled_byte_cs = 1;
		break;

	case DISABLE_SW_CS_EACH_BYTE:
		spi_data->enabled_byte_cs = 0;
		break;

	case SET_SPI_SPEED:
		spi_data->speed = (uint32_t) args;
		if(spi_arch_set_speed(spi_data) != RET_OK){
			ret = RET_ERROR;
			break;
		}
		break;

	case SET_POLARITY_HIGH:
		spi_data->polarity_high = 1;
		if(spi_arch_set_polarity(spi_data) != RET_OK){
			ret = RET_ERROR;
			break;
		}
		break;

	case SET_POLARITY_LOW:
		spi_data->polarity_high = 0;
		if(spi_arch_set_polarity(spi_data) != RET_OK){
			ret = RET_ERROR;
			break;
		}
		break;

	case SET_EDGE_1:
		spi_data->spi_edge_1 = 1;
		if(spi_arch_set_edge(spi_data) != RET_OK){
			ret = RET_ERROR;
			break;
		}
		break;

	case SET_EDGE_2:
		spi_data->spi_edge_1 = 0;
		if(spi_arch_set_edge(spi_data) != RET_OK){
			ret = RET_ERROR;
			break;
		}
		break;

	case SET_SPI_MODE_MASTER:
		spi_data->spi_mode_master = 1;
		if(spi_arch_set_mode(spi_data) != RET_OK){
			ret = RET_ERROR;
			break;
		}
		break;

	case SET_SPI_MODE_SLAVE:
		spi_data->spi_mode_master = 0;
		if(spi_arch_set_mode(spi_data) != RET_OK){
			ret = RET_ERROR;
			break;
		}
		break;
	case SPI_READ_WRITE_OP:
		read_write_buffs = (spi_read_write_buffs_t*) args;
		if(!spi_read_write(filep, read_write_buffs->ptx, read_write_buffs->prx, read_write_buffs->size)){
			ret = RET_ERROR;
			break;
		}
		break;
	case ENABLE_INPUT_HW_CS:
		if(!spi_data->spi_mode_master){
			if(spi_arch_set_input_hw_cs(spi_data) != RET_OK){
				return RET_ERROR;
			}
		}
		break;
	case ENABLE_OUTPUT_HW_CS:
		if(spi_arch_set_output_hw_cs(spi_data) != RET_OK){
			return RET_ERROR;
		}
		break;
	default:
		ret = RET_ERROR;
		break;
	}

	if(spi_data->spi_hw_num == 1){
		ytMutexRelease(spi1_mutex_id);
	}
	else if(spi_data->spi_hw_num == 2){
		ytMutexRelease(spi2_mutex_id);
	}
	return ret;
}


/**
 *
 * @param filep
 * @param ptx
 * @param prx
 * @param size
 * @return
 */
static size_t spi_read_write(dev_file_t* filep, uint8_t* ptx, uint8_t* prx, size_t size){
	spi_data_t* spi_data =  (spi_data_t*) filep->private_data;
	uint16_t i;
	retval_t ret;

	disable_low_power_mode();

	if(spi_data->enabled_cs){

		if(spi_data->enabled_byte_cs){

			for(i=0; i<size; i++){
				ytGpioPinReset(spi_data->cs_gpio_pin);
				ret = spi_arch_read_write(spi_data, ptx+1, prx+i, 1);
				ytGpioPinSet(spi_data->cs_gpio_pin);
			}
		}
		else{
			ytGpioPinReset(spi_data->cs_gpio_pin);
			ret = spi_arch_read_write(spi_data, ptx, prx, (uint16_t) size);
			ytGpioPinSet(spi_data->cs_gpio_pin);
		}

	}
	else{
		 ret = spi_arch_read_write(spi_data, ptx, prx, (uint16_t) size);
	}

	enable_low_power_mode();

	if (ret != RET_OK){
		return 0;
	}
	else{
		return size;
	}
}

/* Register the init functions in the kernel Init system */
init_device(spi_init);
exit_device(spi_exit);

#endif
