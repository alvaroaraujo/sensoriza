/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * spi_arch.c
 *
 *  Created on: 20 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file spi_arch.c
 */


#include "i2c_arch.h"
#include "i2c.h"
#include "system_api.h"

#define DEFAULT_TIMEOUT	500

static uint32_t i2c_semaphore_id;


/**
 *
 * @return
 */
retval_t i2c_arch_init(){


	if((i2c_semaphore_id = ytSemaphoreCreate(1)) == 0){
		return RET_ERROR;
	}
	ytSemaphoreWait(i2c_semaphore_id, YT_WAIT_FOREVER);
	MX_I2C1_Init();
	return RET_OK;
}

/**
 *
 * @return
 */
retval_t i2c_arch_deinit(){


	ytSemaphoreDelete(i2c_semaphore_id);
	HAL_I2C_DeInit(&hi2c1);

	return RET_OK;
}

/**
 *
 * @param i2c_data
 * @param txData
 * @param size
 * @return
 */
retval_t i2c_arch_write(i2c_data_t i2c_data, uint8_t* txData, uint16_t size){

	HAL_I2C_Master_Transmit_DMA(&hi2c1, (uint16_t) i2c_data, txData, size);
	if(ytSemaphoreWait(i2c_semaphore_id, DEFAULT_TIMEOUT) != RET_OK){
		return RET_ERROR;
	}

	return RET_OK;
}

/**
 *
 * @param i2c_data
 * @param rxData
 * @param size
 * @return
 */
retval_t i2c_arch_read(i2c_data_t i2c_data, uint8_t* rxData, uint16_t size){

	HAL_I2C_Master_Receive_DMA(&hi2c1, (uint16_t) i2c_data, rxData, size);
	if(ytSemaphoreWait(i2c_semaphore_id, DEFAULT_TIMEOUT) != RET_OK){
		return RET_ERROR;
	}


	return RET_OK;
}

/**
 *
 * @param i2c_data
 * @param i2c_reg_args
 * @return
 */
retval_t i2c_arch_read_reg(i2c_data_t i2c_data, i2c_reg_ops_args_t* i2c_reg_args){

	HAL_I2C_Mem_Read_DMA(&hi2c1, (uint16_t) i2c_data, i2c_reg_args->memAddr, i2c_reg_args->memAddrSize, i2c_reg_args->data, i2c_reg_args->size);
	if(ytSemaphoreWait(i2c_semaphore_id, DEFAULT_TIMEOUT) != RET_OK){
		return RET_ERROR;
	}

	return RET_OK;
}


/**
 *
 * @param i2c_data
 * @param i2c_reg_args
 * @return
 */
retval_t i2c_arch_write_reg(i2c_data_t i2c_data, i2c_reg_ops_args_t* i2c_reg_args){
	HAL_I2C_Mem_Write_DMA(&hi2c1, (uint16_t) i2c_data, i2c_reg_args->memAddr, i2c_reg_args->memAddrSize, i2c_reg_args->data, i2c_reg_args->size);
	if(ytSemaphoreWait(i2c_semaphore_id, DEFAULT_TIMEOUT) != RET_OK){
		return RET_ERROR;
	}

	return RET_OK;
}


void HAL_I2C_MasterTxCpltCallback(I2C_HandleTypeDef *hi2c){
	if(hi2c->Instance == I2C1){
		ytSemaphoreRelease(i2c_semaphore_id);
	}
}
void HAL_I2C_MasterRxCpltCallback(I2C_HandleTypeDef *hi2c){
	if(hi2c->Instance == I2C1){
		ytSemaphoreRelease(i2c_semaphore_id);
	}
}
void HAL_I2C_MemTxCpltCallback(I2C_HandleTypeDef *hi2c){
	if(hi2c->Instance == I2C1){
		ytSemaphoreRelease(i2c_semaphore_id);
	}
}
void HAL_I2C_MemRxCpltCallback(I2C_HandleTypeDef *hi2c){
	if(hi2c->Instance == I2C1){
		ytSemaphoreRelease(i2c_semaphore_id);
	}
}
