/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * platform-conf.h
 *
 *  Created on: 28 de ago. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file platform-conf.h
 */
#ifndef APPLICATION_BOARD_PLATFORM_PLATFORM_CONF_H_
#define APPLICATION_BOARD_PLATFORM_PLATFORM_CONF_H_

#include "types.h"
#include "stm32l4xx_hal.h"
#include "cmsis_os.h"
#include "FreeRTOSConfig.h"
#include "sys_def.h"
#include "arm_math.h"
/* Button ports */
#define BUTTON_PORT	GPIOB
#define BUTTON_PIN	GPIO_PIN_B1

/* Error led */
#define ERROR_LED	LEDS_RED2


/* SD Card Filesystem */
#define INIT_MOUNT_SD_FILESYSTEM	0	//Select to mount the sd card filesystem on init.


/* LOW POWER FEATURES */
#define LOW_POWER_IDLE_RTOS			configUSE_TICKLESS_IDLE		//Select using or not a tickless iddle in FreeRTOS. Change the configUSE_TICKLESS_IDLE define to the desired value: 1 enable low power, 0 disable low power
#if LOW_POWER_IDLE_RTOS
	#define USE_STOP_MODE			1							//Use uC stop mode in low power mode. It achieves the lowest consumption. Default:1
	#define USE_SLEEP_MODE			!USE_STOP_MODE				//Use uC sleep mode instad of stop. The power consumption is much higher as peripherals remains activated
#endif
#define ENABLE_STOP_MODE_DEBUG		0							//Selects to enable debug when the uC is in stop mode. This option must be disabled to reduce power consumption as much as possible
#define HAL_SYNC_WITH_RTOS_SYSTICK	0							//Select to synchronize the rtos systick with HAL tick when the uC is in sleep node. Otherwise, HAL_Delay() will wait actively the selected time plus the sleep time. Default: 0


/* TRACE AND STATS OPTIONS */
#define ENABLE_TRACE_RTOS			configUSE_TRACE_FACILITY		//Enables or disables taking rtos traces.
#if ENABLE_TRACE_RTOS
	#define USE_TRACEALYZER_SW		1								//Enables using Tracealyzer Percepio software. User software that uses USB will not work, as the trace recorder facility makes use of the USB_CDC device
																	//If this option is used the system will not start until the Percepio Tracealyzer software connects to it
	#define ENABLE_RUNTIME_STATS	configGENERATE_RUN_TIME_STATS	//Enables or disables taking runtime stats of the executing tasks. These stats can be collected by vTaskGetRunTimeStats(char*) function
#endif


/* STANDARD INPUT OUTPUT */
#define ENABLE_STDIO_FUNCS			1					//Enable or disables using the STDIO funcs
#if ENABLE_STDIO_FUNCS
	#define STDIO_INTERFACE				USB_STDIO			//Selects the STDIO peripheral interface. It could be UART or USB

	#if USE_TRACEALYZER_SW
		#undef STDIO_INTERFACE
		#define STDIO_INTERFACE		UART_STDIO			//Selects the STDIO peripheral interface. It could be UART or USB
	#endif
	#define USE_YETISHELL			1					//Enable or disables using the yetishell
	#define	OUTPUT_BUFFER_SIZE		96					//Output buffer size in bytes
	#define INPUT_BUFFER_SIZE		96					//Input buffer size in bytes. If using the YETISHELL, the actual memory allocated  is 3xINPUT_BUFFER_SIZE, as more buffers are needed

#endif


/* TIME MEASUREMENT FUNCTIONS */
#define ENABLE_TIME_MEASURE_FUNCS	1					//Enable compiling and using functions for time measurements
#if ENABLE_TIME_MEASURE_FUNCS
	#define USE_HIGH_RES_TIMER		1							//Use high resolution timer with microseconds resolution. It is important to note that high res timers may be off during uC sleep, so measurements could be unreliable in these cases
	#define USE_LOW_RES_TIMER		!USE_HIGH_RES_TIMER			//Use low resolution timer (usually RTC) with milliseconds resolution. The RTC is usually always active, even in sleep modes
#endif

/* HIGH SPEED TIMER (TIM5) */
#define HIGH_SPEED_TIMER_FREQ		1000000							//Frequency of the timer. Default 1 MHz

/* HARDWARE WATCHDOG ENABLE */
#define USE_HW_WATCHDOG			0

/* ENABLED DRIVERS */
#define ENABLE_SPI_DRIVER		1
#define ENABLE_I2C_DRIVER		1
#define ENABLE_ADC_DRIVER		1
#define ENABLE_PWM_TIM_DRIVER	1

#if (ENABLE_STDIO_FUNCS) && (STDIO_INTERFACE != UART_STDIO)
	#define ENABLE_UART_DRIVER		1
#endif

#if ENABLE_I2C_DRIVER
	#define ENABLE_AT30TS75_DRIVER		1
#endif

/* PERIPHERAL DRIVERS */
#if ENABLE_SPI_DRIVER
	#define SPI1_DEVICE_ID		1
	#define	SPI_DEV_1			"DEV_SPI1"
	#define SPI2_DEVICE_ID		2
	#define SPI_DEV_2			"DEV_SPI2"
#endif
#if ENABLE_I2C_DRIVER
	#define I2C_DEVICE_ID		3
	#define I2C_DEV				"DEV_I2C"
#endif
#if ENABLE_UART_DRIVER
	#define UART_DEVICE_ID		4
	#define UART_DEV			"DEV_UART"
#endif
#if ENABLE_ADC_DRIVER
	#define ADC_DEVICE_ID		5
	#define ADC_DEV				"DEV_ADC"
#endif
#if ENABLE_AT30TS75_DRIVER
	#define AT30TS75_DEVICE_ID	7
	#define AT30TS75_DEV		"DEV_AT30TS75"
#endif
#if ENABLE_PWM_TIM_DRIVER
	 #define PWM_TIM_DEVICE_ID	8
	#define PWM_TIM_DEV			"DEV_PWM_TIM"
#endif

/* DSP LIB*/
#define USE_ARM_DSP_LIB		1			//Enable using or not the ARM DSP Lib

/* RADAR PROCESSING*/
#if USE_ARM_DSP_LIB
	#define USE_RADAR_PROCESSING 1
#endif

/* RTC TIME */
#define INIT_TIMESTAMP 1475582400000	//4th October 2016 12:00 //TIMESTAMP IN MS

/* OPERATING SYSTEM PROCESSES */
#define MAX_NUM_PROCESSES					24			//Numero m�ximo de procesos simult�neos que el sistema operativo soporta
#define CHILD_PROCESSES_LINKED_WITH_PARENTS	1			//Selecciona si se desea que los procesos hijos dependan de los padres o no

/* NETSTACK CONFIG */
#define ENABLE_NETSTACK_ARCH	1

#endif /* PLATFORM_CONF_H_ */
