/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * uart_stdio.c
 *
 *  Created on: 7 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file uart_stdio.c
 */

#include "yetimote_stdio.h"
#include "system_api.h"

#if STDIO_INTERFACE == UART_STDIO

#include "usart.h"

#define UART_OUT_TIMEOUT	250

uint32_t	uart_stdio_mutex_id;

uint32_t uart_stdio_semaphore_id;

extern stdio_input_t* stdio_input;

/**
 *
 * @return
 */
retval_t uart_stdio_init(void){
#if ((USE_STOP_MODE == 0))
	MX_USART2_UART_Init();
	uart_stdio_semaphore_id = ytSemaphoreCreate(1);
	ytSemaphoreWait (uart_stdio_semaphore_id, YT_WAIT_FOREVER);
	uart_stdio_mutex_id = ytMutexCreate();
	HAL_UART_Receive_DMA(&huart2, stdio_input->circular_buffer, INPUT_BUFFER_SIZE);
#endif
	return RET_OK;;
}

/**
 *
 * @param format
 */
void uart_printf(char* format, ...){
#if ((USE_STOP_MODE == 0))

	ytMutexWait(uart_stdio_mutex_id, YT_WAIT_FOREVER);					//By using the mutex we prevent waste large amount of memory if uart_printf function is called simultaneously by parallel threads
	uint8_t* buff = (uint8_t* ) ytMalloc(OUTPUT_BUFFER_SIZE);
	va_list argptr;
	va_start(argptr, format);
	vsnprintf((char*)buff, OUTPUT_BUFFER_SIZE-1, format, argptr);
	va_end(argptr);
	HAL_UART_Transmit_DMA(&huart2, buff, strlen((char*)buff));

	ytSemaphoreWait (uart_stdio_semaphore_id, YT_WAIT_FOREVER);

	ytFree(buff);
	ytMutexRelease(uart_stdio_mutex_id);

#endif

}

/**
 *
 * @param data
 * @param size
 * @return
 */
retval_t uart_stdout_send(uint8_t* data, uint16_t size){
#if ((USE_STOP_MODE == 0))

	ytMutexWait(uart_stdio_mutex_id, YT_WAIT_FOREVER);					//By using the mutex we prevent waste large amount of memory if uart_printf function is called simultaneously by parallel threads

	if(size < OUTPUT_BUFFER_SIZE){

		HAL_UART_Transmit_DMA(&huart2, data, size);

		ytSemaphoreWait (uart_stdio_semaphore_id, UART_OUT_TIMEOUT);

	}

	ytMutexRelease(uart_stdio_mutex_id);

#endif
	return RET_OK;
}

/**
 *
 * @return
 */
retval_t uart_stdio_update_rcv_counter(void){

#if ((USE_STOP_MODE == 0))
	stdio_input->rcv_buffer_counter = __HAL_DMA_GET_COUNTER(huart2.hdmarx);
#endif
	return RET_OK;
}


void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart){
	ytSemaphoreRelease (uart_stdio_semaphore_id);
}

#endif
