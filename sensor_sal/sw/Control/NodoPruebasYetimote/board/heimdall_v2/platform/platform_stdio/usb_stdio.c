/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * usb_stdio.c
 *
 *  Created on: 7 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file usb_stdio.c
 */

#include "system_api.h"
#include "yetimote_stdio.h"

#if STDIO_INTERFACE == USB_STDIO


uint32_t	usb_stdio_mutex_id;

extern USBD_HandleTypeDef hUsbDeviceFS;

extern stdio_input_t* stdio_input;
uint16_t usb_failed = 0;

void stdio_usb_vcom_rcv_callback(uint8_t *Buf, uint32_t Len);

/**
 *
 * @return
 */
retval_t usb_stdio_init(void){
#if ((USE_STOP_MODE == 0) && (USE_TRACEALYZER_SW == 0))
	usb_stdio_mutex_id = ytMutexCreate();
#endif
	return RET_OK;;
}

/**
 *
 * @param format
 */
void usb_printf(char* format, ...){
#if ((USE_STOP_MODE == 0) && (USE_TRACEALYZER_SW == 0))
	if(!usb_failed){
		ytMutexWait(usb_stdio_mutex_id, YT_WAIT_FOREVER);					//By using the mutex we prevent waste large amount of memory if usb_printf function is called simultaneously by parallel threads
		uint8_t* buff = (uint8_t* ) ytMalloc(OUTPUT_BUFFER_SIZE);
		va_list argptr;
		va_start(argptr, format);
		vsnprintf((char*)buff, OUTPUT_BUFFER_SIZE-1, format, argptr);
		va_end(argptr);


		if(CDC_Transmit_FS(buff, strlen((char*)buff)) == USBD_FAIL){
			usb_failed = 1;
		}

		ytFree(buff);
		ytMutexRelease(usb_stdio_mutex_id);
	}
#endif

}

/**
 *
 * @param data
 * @param size
 * @return
 */
retval_t usb_stdout_send(uint8_t* data, uint16_t size){
#if ((USE_STOP_MODE == 0) && (USE_TRACEALYZER_SW == 0))
	if(!usb_failed){
		ytMutexWait(usb_stdio_mutex_id, YT_WAIT_FOREVER);					//By using the mutex we prevent waste large amount of memory if usb_printf function is called simultaneously by parallel threads

		if(size < OUTPUT_BUFFER_SIZE){
			if(CDC_Transmit_FS(data, strlen((char*)data)) == USBD_FAIL){
				usb_failed = 1;
			}
		}

		ytMutexRelease(usb_stdio_mutex_id);
	}
#endif
	return RET_OK;
}

/**
 *
 * @return
 */
retval_t usb_stdio_update_rcv_counter(void){	//Esta funcion lo unico que hace es poner el Usb a recibir cada cierto tiempo, ya que a veces se fastidia
												//No se hace update del counter del buffer de entrada del usb, ya que esto se hace por interrupcion en Usb_Vcom_Rcv_Callback()
#if ((USE_STOP_MODE == 0) && (USE_TRACEALYZER_SW == 0))
	static uint32_t n;
	n++;
	if(n > 300){
		USBD_CDC_ReceivePacket(&hUsbDeviceFS);
		n = 0;
	}
#endif
	return RET_OK;
}

/**
 *
 * @param Buf
 * @param Len
 */
void stdio_usb_vcom_rcv_callback(uint8_t *Buf, uint32_t Len){		//No bloquear esta funcion. Se llama desde una interrupcion
															//Simplemente guardar el buffer recibido para usarlo como convenga
															//Tampoco llamar a printf ni nada similar, ya que usa interrupciones y se bloquea.
	uint16_t i = 0;

	if(stdio_input != NULL){
		for(i=0; i<Len; i++){
			stdio_input->circular_buffer[INPUT_BUFFER_SIZE-stdio_input->rcv_buffer_counter] = Buf[i];
			stdio_input->rcv_buffer_counter--;
			if(stdio_input->rcv_buffer_counter <= 0){
				stdio_input->rcv_buffer_counter = INPUT_BUFFER_SIZE;
			}
		}
		USBD_CDC_ReceivePacket(&hUsbDeviceFS);		//Esta instrucci�n indica al driver USB que ya esta listo para recibir m�s paquetes, ya que el dato anterior ya se ha procesado
	}
}


#endif
