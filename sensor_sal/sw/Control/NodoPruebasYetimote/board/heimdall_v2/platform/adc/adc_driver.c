/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * adc_driver.c
 *
 *  Created on: 12 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file adc_driver.c
 */

#include "device.h"
#include "platform-conf.h"
#include "yetimote_stdio.h"
#include "adc_arch.h"
#include "low_power.h"
#include "adc_driver.h"
#include "process.h"
#include "system_api.h"


#define DEBUG 1
#if DEBUG
#include "yetimote_stdio.h"
#define PRINTF(...) _printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif


#if ENABLE_ADC_DRIVER

/* Device name and Id */
#define ADC_DEVICE_NAME ADC_DEV

static uint32_t adc_mutex_id;

/* Device Init functions */
static retval_t adc_init(void);
static retval_t adc_exit(void);


/* Device driver operation functions declaration */
static retval_t adc_open(device_t* device, dev_file_t* filep);
static retval_t adc_close(dev_file_t* filep);
static size_t adc_read(dev_file_t* filep, uint8_t* prx, size_t size);
static retval_t adc_ioctl(dev_file_t* filep, uint16_t request, void* args);


/* Define driver operations */
static driver_ops_t adc_driver_ops = {
	.open = adc_open,
	.close = adc_close,
	.read = adc_read,
	.write = NULL,
	.ioctl = adc_ioctl,
};


/**
 *
 * @return
 */
static retval_t adc_init(void){
	if(adc_arch_init() != RET_OK){				//Inicialización del HW del ADC
		return RET_ERROR;
	}
	if(registerDevice(ADC_DEVICE_ID, &adc_driver_ops, ADC_DEVICE_NAME) != RET_OK){
		return RET_ERROR;
	}
	adc_mutex_id = ytMutexCreate();	//Initialize blocking mutex
	PRINTF(">ADC Init Done\r\n");

	return RET_OK;
}

/**
 *
 * @return
 */
static retval_t adc_exit(void){
	if(adc_arch_deinit() != RET_OK){				//De-Inicialización del HW del i2c
		return RET_ERROR;
	}
	unregisterDevice(ADC_DEVICE_ID, ADC_DEVICE_NAME);
	ytMutexDelete(adc_mutex_id);
	PRINTF(">ADC Exit Done\r\n");

	return RET_OK;
}

/**
 *
 * @param device
 * @param filep
 * @return
 */
static retval_t adc_open(device_t* device, dev_file_t* filep){
	adc_config_t* adc_config;
	if(device->device_state != DEV_STATE_INIT){
		return RET_ERROR;
	}
	if((adc_config = (adc_config_t*) ytMalloc(sizeof(adc_config_t))) == NULL){
			return RET_ERROR;
	}
	ytMutexWait(adc_mutex_id, YT_WAIT_FOREVER);
	adc_config->adc_pin_diff1 = 0;
	adc_config->adc_pin_diff2 = 0;
	adc_config->adc_pin_se1 = 0;
	adc_config->adc_pin_se2 = 0;
	adc_config->diff1_mode_enabled = 0;
	adc_config->diff2_mode_enabled = 0;
	adc_config->dual_mode_enabled = 0;

	filep->private_data = (void*) adc_config;
	ytMutexRelease(adc_mutex_id);
	return RET_OK;
}

/**
 *
 * @param filep
 * @return
 */
static retval_t adc_close(dev_file_t* filep){
	adc_config_t* adc_config;
	if(filep->private_data != NULL){
		ytMutexWait(adc_mutex_id, YT_WAIT_FOREVER);
		adc_config = (adc_config_t*) filep->private_data;
		if(adc_config->adc_pin_se1){
			ytGpioDeInitPin(adc_config->adc_pin_se1);
		}
		if(adc_config->adc_pin_se2){
			ytGpioDeInitPin(adc_config->adc_pin_se2);
		}
		if(adc_config->adc_pin_diff1){
			ytGpioDeInitPin(adc_config->adc_pin_diff1);
		}
		if(adc_config->adc_pin_diff2){
			ytGpioDeInitPin(adc_config->adc_pin_diff2);
		}
		ytFree(filep->private_data);
		ytMutexRelease(adc_mutex_id);
	}
	return RET_OK;
}

/**
 *
 * @param filep
 * @param prx
 * @param size
 * @return
 */
static size_t adc_read(dev_file_t* filep, uint8_t* prx, size_t size){

	retval_t ret;

	ytMutexWait(adc_mutex_id, YT_WAIT_FOREVER);
	disable_low_power_mode();
	ret = adc_arch_read_channel((adc_config_t*) filep->private_data, prx, size);
	enable_low_power_mode();
	ytMutexRelease(adc_mutex_id);


	if (ret != RET_OK){
		return 0;
	}
	else{
		return size;
	}
}


/**
 *
 * @param filep
 * @param request
 * @param args
 * @return
 */
static retval_t adc_ioctl(dev_file_t* filep, uint16_t request, void* args){
	adc_driver_ioctl_cmd_t cmd;
	adc_config_t* new_adc_config;
	adc_config_t* adc_config;
	retval_t ret = RET_OK;
	if(filep == NULL){
		return RET_ERROR;
	}
	cmd = (adc_driver_ioctl_cmd_t) request;
	ytMutexWait(adc_mutex_id, YT_WAIT_FOREVER);

	switch(cmd){
	case CONFIG_ADC:

		new_adc_config = (adc_config_t*) args;
		adc_config = (adc_config_t*) filep->private_data;

		adc_config->adc_pin_diff1 = new_adc_config->adc_pin_diff1;
		adc_config->adc_pin_diff2 = new_adc_config->adc_pin_diff2;
		adc_config->adc_pin_se1 = new_adc_config->adc_pin_se1;
		adc_config->adc_pin_se2 = new_adc_config->adc_pin_se2;
		adc_config->channel_speed = new_adc_config->channel_speed;
		adc_config->diff1_mode_enabled = new_adc_config->diff1_mode_enabled;
		adc_config->diff2_mode_enabled = new_adc_config->diff2_mode_enabled;
		adc_config->dual_mode_enabled = new_adc_config->dual_mode_enabled;
		adc_arch_config_channel(adc_config);

		break;


	default:
		ret = RET_ERROR;
		break;
	}
	ytMutexRelease(adc_mutex_id);
	return ret;
}

/* Register the init functions in the kernel Init system */
init_device(adc_init);
exit_device(adc_exit);

#endif
