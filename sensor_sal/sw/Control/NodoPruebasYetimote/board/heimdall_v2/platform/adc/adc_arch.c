/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * adc_arch.c
 *
 *  Created on: 20 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file adc_arch.c
 */


#include "adc_arch.h"
#include "adc.h"
#include "leds.h"
#include "process.h"
#include "system_api.h"

#define DEFAULT_TIMEOUT	500

extern DMA_HandleTypeDef hdma_adc1;
extern DMA_HandleTypeDef hdma_adc2;

static uint32_t adc_semaphore_id;

static uint32_t adc_arch_get_channel_from_gpio(gpio_pin_t gpio);
static retval_t adc_arch_set_oversampling(uint32_t* speed, uint32_t* oversampling, uint32_t* bitShift);
static retval_t adc_arch_set_listening_channels(adc_config_t* adc_config);

/**
 *
 * @return
 */
retval_t adc_arch_init(void){
	MX_ADC1_Init();
	MX_ADC2_Init();
	adc_semaphore_id = ytSemaphoreCreate(1);

	return RET_OK;
}

/**
 *
 * @return
 */
retval_t adc_arch_deinit(void){
	HAL_ADC_DeInit(&hadc1);
	HAL_ADC_DeInit(&hadc2);

	ytSemaphoreDelete(adc_semaphore_id);
	return RET_OK;
}

/**
 *
 * @param adc_config
 * @param data
 * @param numSamples
 * @return
 */
retval_t adc_arch_read_channel(adc_config_t* adc_config, uint8_t* data, uint16_t numSamples){
	if(adc_arch_set_listening_channels(adc_config) != RET_OK){
		return RET_ERROR;
	}
	if(adc_config->dual_mode_enabled){
		HAL_ADCEx_MultiModeStart_DMA(&hadc1, (uint32_t*) data, numSamples);		//Leo con el ADC en modo simultaneo y espero hasta que se termina
		if(ytSemaphoreWait(adc_semaphore_id, DEFAULT_TIMEOUT) != RET_OK){
			return RET_ERROR;
		}
		HAL_ADCEx_MultiModeStop_DMA(&hadc1);
	}
	else{
		HAL_ADC_Start_DMA(&hadc1, (uint32_t*) data, numSamples);			//Leo con el ADC y espero hasta que se termina
		if(ytSemaphoreWait(adc_semaphore_id, DEFAULT_TIMEOUT) != RET_OK){
			return RET_ERROR;
		}
		HAL_ADC_Stop_DMA(&hadc1);
	}
	return RET_OK;
}

/**
 *
 * @param adc_config
 * @return
 */
retval_t adc_arch_config_channel(adc_config_t* adc_config){
	uint32_t channel_adc_1;
	uint32_t channel_adc_2;
	uint32_t oversampling, bitShift;

	ADC_MultiModeTypeDef multimode;
	ADC_ChannelConfTypeDef sConfig1, sConfig2;

	if( adc_arch_set_oversampling(&(adc_config->channel_speed), &oversampling, &bitShift) != RET_OK){
		return RET_ERROR;
	}

	/* Simultaneous Dual ADC configuration */
	if(adc_config->dual_mode_enabled){
		if((channel_adc_1 = adc_arch_get_channel_from_gpio(adc_config->adc_pin_se1)) == 0xFFFFFFFF){
			return RET_ERROR;
		}
		if((channel_adc_2 = adc_arch_get_channel_from_gpio(adc_config->adc_pin_se2)) == 0xFFFFFFFF){
			return RET_ERROR;
		}

		/* First Channel configuration */
		if(adc_config->diff1_mode_enabled){
			/* Config GPIOs */
			if(ytGpioInitPin(adc_config->adc_pin_se1, GPIO_PIN_ANALOG_ADC_CONTROL, GPIO_PIN_NO_PULL) != RET_OK){
				return RET_ERROR;
			}
			if(ytGpioInitPin(adc_config->adc_pin_diff1, GPIO_PIN_ANALOG_ADC_CONTROL, GPIO_PIN_NO_PULL) != RET_OK){
				ytGpioDeInitPin(adc_config->adc_pin_se1);
				return RET_ERROR;
			}
			sConfig1.SingleDiff = ADC_DIFFERENTIAL_ENDED;
		}
		else{

			/* Config GPIO */
			if(ytGpioInitPin(adc_config->adc_pin_se1, GPIO_PIN_ANALOG_ADC_CONTROL, GPIO_PIN_NO_PULL) != RET_OK){
				return RET_ERROR;
			}

			/* Config channel */
			sConfig1.SingleDiff = ADC_SINGLE_ENDED;
		}

		/* Second channel configuration */
		if(adc_config->diff2_mode_enabled){
			/* Config GPIOs */
			if(ytGpioInitPin(adc_config->adc_pin_se2, GPIO_PIN_ANALOG_ADC_CONTROL, GPIO_PIN_NO_PULL) != RET_OK){
				return RET_ERROR;
			}
			if(ytGpioInitPin(adc_config->adc_pin_diff2, GPIO_PIN_ANALOG_ADC_CONTROL, GPIO_PIN_NO_PULL) != RET_OK){
				ytGpioDeInitPin(adc_config->adc_pin_se2);
				return RET_ERROR;
			}
			sConfig2.SingleDiff = ADC_DIFFERENTIAL_ENDED;
		}
		else{

			/* Config GPIO */
			if(ytGpioInitPin(adc_config->adc_pin_se2, GPIO_PIN_ANALOG_ADC_CONTROL, GPIO_PIN_NO_PULL) != RET_OK){
				return RET_ERROR;
			}

			/* Config channel */
			sConfig2.SingleDiff = ADC_SINGLE_ENDED;
		}
		/* Config speed */
		hadc1.Init.Oversampling.Ratio = oversampling;
		hadc1.Init.Oversampling.RightBitShift = bitShift;
		if (HAL_ADC_Init(&hadc1) != HAL_OK)
		{
		_Error_Handler(__FILE__, __LINE__);
		}
		hadc2.Init.Oversampling.Ratio = oversampling;
		hadc2.Init.Oversampling.RightBitShift = bitShift;
		if (HAL_ADC_Init(&hadc2) != HAL_OK)
		{
		_Error_Handler(__FILE__, __LINE__);
		}

		/* COnfig DMA */
	    hdma_adc1.Init.PeriphDataAlignment = DMA_PDATAALIGN_WORD;
	    hdma_adc1.Init.MemDataAlignment = DMA_MDATAALIGN_WORD;
	    if (HAL_DMA_Init(&hdma_adc1) != HAL_OK)
	    {
	      _Error_Handler(__FILE__, __LINE__);
	    }
	    hdma_adc2.Init.PeriphDataAlignment = DMA_PDATAALIGN_WORD;
	    hdma_adc2.Init.MemDataAlignment = DMA_MDATAALIGN_WORD;
	    if (HAL_DMA_Init(&hdma_adc2) != HAL_OK)
	    {
	      _Error_Handler(__FILE__, __LINE__);
	    }


		/* Config multi mode */
		multimode.Mode = ADC_DUALMODE_REGSIMULT;
		multimode.DMAAccessMode = ADC_DMAACCESSMODE_12_10_BITS;
		multimode.TwoSamplingDelay = ADC_TWOSAMPLINGDELAY_1CYCLE;
		if (HAL_ADCEx_MultiModeConfigChannel(&hadc1, &multimode) != HAL_OK)
		{
		_Error_Handler(__FILE__, __LINE__);
		}

		/* Config channel */
		sConfig1.Channel = channel_adc_1;
		sConfig1.Rank = 1;
		sConfig1.SamplingTime = ADC_SAMPLETIME_2CYCLES_5;
		sConfig1.OffsetNumber = ADC_OFFSET_NONE;
		sConfig1.Offset = 0;
		if (HAL_ADC_ConfigChannel(&hadc1, &sConfig1) != HAL_OK)
		{
		_Error_Handler(__FILE__, __LINE__);
		}

		sConfig2.Channel = channel_adc_2;
		sConfig2.Rank = 1;
		sConfig2.SamplingTime = ADC_SAMPLETIME_2CYCLES_5;
		sConfig2.OffsetNumber = ADC_OFFSET_NONE;
		sConfig2.Offset = 0;
		if (HAL_ADC_ConfigChannel(&hadc2, &sConfig2) != HAL_OK)
		{
		_Error_Handler(__FILE__, __LINE__);
		}

	}


	/* Single ADC configuration */
	else{

		if((channel_adc_1 = adc_arch_get_channel_from_gpio(adc_config->adc_pin_se1)) == 0xFFFFFFFF){
			return RET_ERROR;
		}

		/* Channel configuration */
		if(adc_config->diff1_mode_enabled){
			/* Config GPIOs */
			if(ytGpioInitPin(adc_config->adc_pin_se1, GPIO_PIN_ANALOG_ADC_CONTROL, GPIO_PIN_NO_PULL) != RET_OK){
				return RET_ERROR;
			}
			if(ytGpioInitPin(adc_config->adc_pin_diff1, GPIO_PIN_ANALOG_ADC_CONTROL, GPIO_PIN_NO_PULL) != RET_OK){
				ytGpioDeInitPin(adc_config->adc_pin_se1);
				return RET_ERROR;
			}
			sConfig1.SingleDiff = ADC_DIFFERENTIAL_ENDED;
		}
		else{
			/* Config GPIO */
			if(ytGpioInitPin(adc_config->adc_pin_se1, GPIO_PIN_ANALOG_ADC_CONTROL, GPIO_PIN_NO_PULL) != RET_OK){
				return RET_ERROR;
			}

			/* Config channel */
			sConfig1.SingleDiff = ADC_SINGLE_ENDED;

		}

		/* Config speed */
		hadc1.Init.Oversampling.Ratio = oversampling;
		hadc1.Init.Oversampling.RightBitShift = bitShift;
		if (HAL_ADC_Init(&hadc1) != HAL_OK)
		{
		_Error_Handler(__FILE__, __LINE__);
		}

		/* COnfig DMA */
	    hdma_adc1.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;
	    hdma_adc1.Init.MemDataAlignment = DMA_MDATAALIGN_HALFWORD;
	    if (HAL_DMA_Init(&hdma_adc1) != HAL_OK)
	    {
	      _Error_Handler(__FILE__, __LINE__);
	    }

		/* Config multi mode */
		multimode.Mode = ADC_MODE_INDEPENDENT;
		if (HAL_ADCEx_MultiModeConfigChannel(&hadc1, &multimode) != HAL_OK)
		{
		_Error_Handler(__FILE__, __LINE__);
		}

		/* Config channel */
		sConfig1.Channel = channel_adc_1;
		sConfig1.Rank = 1;
		sConfig1.SamplingTime = ADC_SAMPLETIME_2CYCLES_5;
		sConfig1.OffsetNumber = ADC_OFFSET_NONE;
		sConfig1.Offset = 0;
		if (HAL_ADC_ConfigChannel(&hadc1, &sConfig1) != HAL_OK)
		{
		_Error_Handler(__FILE__, __LINE__);
		}

	}

	return RET_OK;
}


/**
 *
 * @param speed
 * @param oversampling
 * @param bitShift
 * @return
 */
static retval_t adc_arch_set_oversampling(uint32_t* speed, uint32_t* oversampling, uint32_t* bitShift){

	(*oversampling) = (uint32_t) HAL_RCC_GetHCLKFreq()/((*speed)*15);

	if((*oversampling) > 128){
		(*speed) = (uint32_t) HAL_RCC_GetHCLKFreq()/(256*15);
		(*oversampling) = ADC_OVERSAMPLING_RATIO_256;
		(*bitShift) = ADC_RIGHTBITSHIFT_4;
	}
	else if((*oversampling) > 64){
		(*speed) = (uint32_t) HAL_RCC_GetHCLKFreq()/(128*15);
		(*oversampling) = ADC_OVERSAMPLING_RATIO_128;
		(*bitShift) = ADC_RIGHTBITSHIFT_3;
	}
	else if((*oversampling) > 32){
		(*speed) = (uint32_t) HAL_RCC_GetHCLKFreq()/(64*15);
		(*oversampling) = ADC_OVERSAMPLING_RATIO_64;
		(*bitShift) = ADC_RIGHTBITSHIFT_2;
	}
	else if((*oversampling) > 16){
		(*speed) = (uint32_t) HAL_RCC_GetHCLKFreq()/(32*15);
		(*oversampling) = ADC_OVERSAMPLING_RATIO_32;
		(*bitShift) = ADC_RIGHTBITSHIFT_1;
	}
	else if((*oversampling) > 8){
		(*speed) = (uint32_t) HAL_RCC_GetHCLKFreq()/(16*15);
		(*oversampling) = ADC_OVERSAMPLING_RATIO_16;
		(*bitShift) = ADC_RIGHTBITSHIFT_NONE;
	}
	else if((*oversampling) > 4){
		(*speed) = (uint32_t) HAL_RCC_GetHCLKFreq()/(8*15);
		(*oversampling) = ADC_OVERSAMPLING_RATIO_8;
		(*bitShift) = ADC_RIGHTBITSHIFT_NONE;
	}
	else if((*oversampling) > 2){
		(*speed) = (uint32_t) HAL_RCC_GetHCLKFreq()/(4*15);
		(*oversampling) = ADC_OVERSAMPLING_RATIO_4;
		(*bitShift) = ADC_RIGHTBITSHIFT_NONE;
	}
	else if((*oversampling) >= 0){
		(*speed) = (uint32_t) HAL_RCC_GetHCLKFreq()/(2*15);
		(*oversampling) = ADC_OVERSAMPLING_RATIO_2;
		(*bitShift) = ADC_RIGHTBITSHIFT_NONE;
	}
	else{
		return RET_ERROR;
	}
	return RET_OK;
}

/**
 *
 * @param gpio
 * @return
 */
static uint32_t adc_arch_get_channel_from_gpio(gpio_pin_t gpio){
	switch(gpio){
	case GPIO_PIN_A0:
		return ADC_CHANNEL_5;
		break;
	case GPIO_PIN_A1:
		return ADC_CHANNEL_6;
		break;
	case  GPIO_PIN_A4:
		return ADC_CHANNEL_9;
		break;
	case GPIO_PIN_C1:
		return ADC_CHANNEL_2;
		break;
	case GPIO_PIN_C3:
		return ADC_CHANNEL_4;
		break;
	case GPIO_PIN_C5:
		return ADC_CHANNEL_14;
		break;
	default:
		return 0xFFFFFFFF;
		break;
	}
}

/**
 *
 * @param adc_config
 * @return
 */
static retval_t adc_arch_set_listening_channels(adc_config_t* adc_config){
	uint32_t channel_adc_1, channel_adc_2;
	ADC_ChannelConfTypeDef sConfig1, sConfig2;

	/* Dual ADC mode */
	if(adc_config->dual_mode_enabled){

		if((channel_adc_1 = adc_arch_get_channel_from_gpio(adc_config->adc_pin_se1)) == 0xFFFFFFFF){
			return RET_ERROR;
		}
		if((channel_adc_2 = adc_arch_get_channel_from_gpio(adc_config->adc_pin_se2)) == 0xFFFFFFFF){
			return RET_ERROR;
		}

		if(adc_config->diff1_mode_enabled){
			sConfig1.SingleDiff = ADC_DIFFERENTIAL_ENDED;
		}
		else{
			sConfig1.SingleDiff = ADC_SINGLE_ENDED;
		}

		if(adc_config->diff2_mode_enabled){
			sConfig2.SingleDiff = ADC_DIFFERENTIAL_ENDED;
		}
		else{
			sConfig2.SingleDiff = ADC_SINGLE_ENDED;
		}

		/* Config channel */
		sConfig1.Channel = channel_adc_1;
		sConfig1.Rank = 1;
		sConfig1.SamplingTime = ADC_SAMPLETIME_2CYCLES_5;
		sConfig1.OffsetNumber = ADC_OFFSET_NONE;
		sConfig1.Offset = 0;
		if (HAL_ADC_ConfigChannel(&hadc1, &sConfig1) != HAL_OK)
		{
		_Error_Handler(__FILE__, __LINE__);
		}

		sConfig2.Channel = channel_adc_2;
		sConfig2.Rank = 1;
		sConfig2.SamplingTime = ADC_SAMPLETIME_2CYCLES_5;
		sConfig2.OffsetNumber = ADC_OFFSET_NONE;
		sConfig2.Offset = 0;
		if (HAL_ADC_ConfigChannel(&hadc2, &sConfig2) != HAL_OK)
		{
		_Error_Handler(__FILE__, __LINE__);
		}

	}

	/* Single ADC mode */
	else{
		if((channel_adc_1 = adc_arch_get_channel_from_gpio(adc_config->adc_pin_se1)) == 0xFFFFFFFF){
			return RET_ERROR;
		}

		/* Channel configuration */
		if(adc_config->diff1_mode_enabled){
			sConfig1.SingleDiff = ADC_DIFFERENTIAL_ENDED;
		}
		else{
			sConfig1.SingleDiff = ADC_SINGLE_ENDED;
		}
		/* Config channel */
		sConfig1.Channel = channel_adc_1;
		sConfig1.Rank = 1;
		sConfig1.SamplingTime = ADC_SAMPLETIME_2CYCLES_5;
		sConfig1.OffsetNumber = ADC_OFFSET_NONE;
		sConfig1.Offset = 0;
		if (HAL_ADC_ConfigChannel(&hadc1, &sConfig1) != HAL_OK)
		{
		_Error_Handler(__FILE__, __LINE__);
		}
	}
	return RET_OK;

}

// Comentado para poder usar el programa de ADC continuo
/*
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc){
	ytSemaphoreRelease(adc_semaphore_id);
}*/

void HAL_ADC_ErrorCallback(ADC_HandleTypeDef *hadc){
#ifdef ERROR_LED
	ytLedsOn(ERROR_LED);
#endif
	while(1);
}
