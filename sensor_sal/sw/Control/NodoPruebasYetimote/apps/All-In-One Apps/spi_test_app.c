/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * spi_test_app.c
 *
 *  Created on: 22 de ene. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file spi_test_app.c
 */

#include "system_api.h"
#include "net_api.h"

#define USE_ALLINONE_SPI_TEST	0

#if USE_ALLINONE_SPI_TEST

#define MODE_SLAVE		1
//#define MODE_MASTER		2

#if MODE_SLAVE

#define MAX_AVAILABLE_SPEEDS	10

static uint32_t test_spi_output	[MAX_AVAILABLE_SPEEDS+1];

static uint32_t test_storing_bufer[MAX_AVAILABLE_SPEEDS+1];
static float32_t* test_stored_speeds = (float32_t*) &test_storing_bufer[1];
static uint32_t* test_num_stored_speeds = &test_storing_bufer[0];

uint16_t allinoneSpiTestId;
uint16_t bufferWriterId;

static retval_t config_spi_device(uint32_t* spi_id, char* spi_dev);

YT_PROCESS void allinone_spi_test_func(void const * argument);
YT_PROCESS void buffer_writter_func(void const * argument);

ytInitProcess(allinone_spi, allinone_spi_test_func, DEFAULT_PROCESS, 256, &allinoneSpiTestId, NULL);

YT_PROCESS void allinone_spi_test_func(void const * argument){
	uint32_t spi_id;
	test_stored_speeds[0] = 3.4f;
	test_stored_speeds[1] = 53;
	test_stored_speeds[2] = -67.2f;
	test_stored_speeds[3] = 22;
	test_stored_speeds[4] = 104.5f;
	test_stored_speeds[5] = 55.8f;
	test_stored_speeds[6] = -122;
	test_stored_speeds[7] = -67.2f;
	test_stored_speeds[8] = 134.5f;
	test_stored_speeds[9] = 12;
	(*test_num_stored_speeds) = 5;
	config_spi_device(&spi_id, SPI_DEV_2);
	ytStartDefaultProcess("buff_write_proc", buffer_writter_func, &bufferWriterId);
	while(1){
		memcpy(test_spi_output, test_storing_bufer, (MAX_AVAILABLE_SPEEDS+1)*4);
		(*test_num_stored_speeds) = 0;
		ytWrite(spi_id, (uint8_t*) test_spi_output, (MAX_AVAILABLE_SPEEDS+1)*4);
		leds_toggle(LEDS_RED1);
	}
}



YT_PROCESS void buffer_writter_func(void const * argument){

	while(1){
		osDelay(250);
		leds_toggle(LEDS_BLUE);
		if((*test_num_stored_speeds) >= (MAX_AVAILABLE_SPEEDS-1)){
			(*test_num_stored_speeds) = 0;
		}
		else{
			(*test_num_stored_speeds)++;
		}

	}

}

static retval_t config_spi_device(uint32_t* spi_id, char* spi_dev){

	ytSpiDriverIoctlCmd_t spi_ioctl_cmd;

	(*spi_id) = ytOpen(spi_dev, 0);
	if(!(*spi_id)){
		return RET_ERROR;
	}
//	spi_ioctl_cmd = DISABLE_SW_CS;
//	if(ytIoctl((*spi_id), spi_ioctl_cmd, NULL) != RET_OK){
//		return RET_ERROR;
//	}
	spi_ioctl_cmd = DISABLE_SW_CS_EACH_BYTE;
	if(ytIoctl((*spi_id), spi_ioctl_cmd, NULL) != RET_OK){
		return RET_ERROR;
	}
	spi_ioctl_cmd = SET_POLARITY_LOW;
	if(ytIoctl((*spi_id), spi_ioctl_cmd, NULL) != RET_OK){
		return RET_ERROR;
	}
	spi_ioctl_cmd = SET_EDGE_2;
	if(ytIoctl((*spi_id), spi_ioctl_cmd, NULL) != RET_OK){
		return RET_ERROR;
	}
	spi_ioctl_cmd = SET_SPI_MODE_SLAVE;
	if(ytIoctl((*spi_id), spi_ioctl_cmd, NULL) != RET_OK){
		return RET_ERROR;
	}
	spi_ioctl_cmd = ENABLE_INPUT_HW_CS;
	if(ytIoctl((*spi_id), spi_ioctl_cmd, NULL) != RET_OK){
		return RET_ERROR;
	}
	return RET_OK;
}
#endif

#if MODE_MASTER
#define SPI_SPEED 1500000

#define DATA_SIZE	10

uint32_t read_data[DATA_SIZE+1];
uint32_t* num_count = &read_data[0];
float32_t* vehicles_speeds = (float32_t*) &read_data[1];

uint16_t allinoneSpiTestId;

static retval_t config_spi_device(uint32_t* spi_id, char* spi_dev);

YT_PROCESS void allinone_spi_test_func(void const * argument);

ytInitProcess(allinone_spi, allinone_spi_test_func, DEFAULT_PROCESS, 256, &allinoneSpiTestId, NULL);

YT_PROCESS void allinone_spi_test_func(void const * argument){

	uint16_t i;
	uint32_t spi_fd;

	config_spi_device(&spi_fd, SPI_DEV_2);
	(*num_count) = 0;

	ytDelay(1000);
	while(1){

		ytRead(spi_fd, read_data, (DATA_SIZE+1)*4);

		if((*num_count) < 10){
			ytPrintf("Count vehicles: %d\r\n", (*num_count));
			for(i=0; i<(*num_count); i++){
				ytPrintf("Speed val %d: %.2f\r\n", i, vehicles_speeds[i]);
			}
		}
		ytDelay(1000);

	}
}

static retval_t config_spi_device(uint32_t* spi_id, char* spi_dev){

	ytSpiDriverIoctlCmd_t spi_ioctl_cmd;

	(*spi_id) = ytOpen(spi_dev, 0);
	if(!(*spi_id)){
		return RET_ERROR;
	}
	spi_ioctl_cmd = ENABLE_SW_CS;
	if(ytIoctl((*spi_id), spi_ioctl_cmd, (void*) GPIO_PIN_B12) != RET_OK){
		return RET_ERROR;
	}
	spi_ioctl_cmd = DISABLE_SW_CS_EACH_BYTE;
	if(ytIoctl((*spi_id), spi_ioctl_cmd, NULL) != RET_OK){
		return RET_ERROR;
	}
	spi_ioctl_cmd = SET_POLARITY_LOW;
	if(ytIoctl((*spi_id), spi_ioctl_cmd, NULL) != RET_OK){
		return RET_ERROR;
	}
	spi_ioctl_cmd = SET_EDGE_2;
	if(ytIoctl((*spi_id), spi_ioctl_cmd, NULL) != RET_OK){
		return RET_ERROR;
	}
	spi_ioctl_cmd = SET_SPI_MODE_MASTER;
	if(ytIoctl((*spi_id), spi_ioctl_cmd, NULL) != RET_OK){
		return RET_ERROR;
	}
	spi_ioctl_cmd = SET_SPI_SPEED;
	if(ytIoctl((*spi_id), spi_ioctl_cmd, (void*) SPI_SPEED) != RET_OK){
		return RET_ERROR;
	}
	return RET_OK;
}

#endif

#endif
