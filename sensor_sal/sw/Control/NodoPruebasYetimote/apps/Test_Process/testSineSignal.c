/*
 * testPeriodico.c
 *
 *  Created on: 27 jun. 2018
 *      Author: miguelvp
 */

#include "system_api.h"
#include "platform-conf.h"
#include "arm_math.h"

#define USE_PERIODIC_SINE_TEST	1

#if USE_PERIODIC_SINE_TEST

#if ENABLE_PWM_TIM_DRIVER
#if ENABLE_ADC_DRIVER
#include "pwm_tim_driver.h"
#include "pwm_tim_arch.h"
#include "time_meas.h"
#include "fsm.h"
#include "task.h"
#include "projdefs.h"
#include "queue.h"
#include "sineGenerator.h"
#include "stm32l4xx_hal_adc.h"
#include "stm32l4xx_hal_adc_ex.h"
#include "stm32l4xx_hal_dma.h"
#include "stm32l4xx_hal_tim.h"
#include "stm32l4xx_hal_dac.h"

#define USE_FILTER			0
#if USE_FILTER
#define FILTER_15			0
#define FILTER_31			!FILTER_15
#endif

#define PROC_STACK_SIZE 	512
#define PERIODO_FSM			10
#define N_FREC				1			// Numero de frecuencias a probar
#define MARGEN_ANTIRREBOTES 500
#define REPETITION_TEST		1
#define N_PRUEBAS			1

#define T_PRUEBAS			10			// minutos
#define ESPERA				15000		// milisegundos
#define TOTAL 				6000000		// useg

#define DATA_TO_READ	16
#define MAX_BUFFER		4
//#define SUBMUESTREO		10

#define BUTTON_PRES		0x01U
#define WAIT_FIN		0x02U
#define TIMER_FIN		0x04U
#define ENDLIST			0x08U
#define REPEAT			0x10U
#define FIN				0x20U
#define PRINT			0x40U

static uint32_t maxSizeQueue;
static uint16_t flags;
static uint8_t sendData;
static uint8_t nTimes, globalCounter, nMuestra, nQueue;
static uint8_t frecuencias[N_FREC] = {10};

uint16_t periodicSineTestProcId;

uint32_t imprime;

uint16_t counter_ADC, counter_PWM;

uint32_t tiempoRepet, tiempoSine;
static QueueHandle_t voltajes;

// Coeficientes {b0, 0, b1, b2, a1, a2} = {0.0057673071509567, 0, -0.0104324966268927, 0.0057673071509567, -1.952631994299386, 0.9537341119743593}
#if USE_FILTER
static int8_t shift = 12;
#if FILTER_15
static q15_t coeff15[6] = {0x005E, 0, 0x80AA, 0x005E, 0xFCF7, 0x3D09};
#endif
#if FILTER_31
static q31_t coeff31[5] = {0x00000BCF, 0xFFFFEAA3, 0x00000BCF, 0xFFF06103, 0x0007A13F}; //{0x005E7DD6, 0x80AAED0F, 0x005E7DD6, 0xFCF7EC2F, 0x3D09FACD};
#endif
#endif

extern ADC_HandleTypeDef hadc1;
extern DMA_HandleTypeDef hdma_adc1;
//extern TIM_HandleTypeDef htim3;

typedef struct test {
	fix_point_t frecuencia;
	fix_point_t duty;
	uint32_t tTotal;
} test_t;

typedef struct data_fsm {
	test_t* testTable;
	uint32_t driverADC_fd;
	uint32_t driverPWM_fd;
	QueueHandle_t voltajes;
	uint32_t tGlob;
	uint32_t tRep;
	uint8_t nTest;
	uint8_t maxTest;
} data_fsm_t;

// Test states machine
static enum testStates {
	IDLE,
	LOAD,
	READ,
	STOP,
	CONTINUE,
};

//
static retval_t adc_init(adc_config_t* adc_config);

// Test functions
static int button_pressed(fsm_t* this);
static int wait_finished(fsm_t* this);
static int next_list(fsm_t* this);
static int end_list(fsm_t* this);
static int new(fsm_t* this);
static void loadValues(fsm_t* this);
static void next(fsm_t* this);
static void startTest(fsm_t* this);
static void stopTest(fsm_t* this);
static void fin(fsm_t* this);
static void reset(fsm_t* this);

// Test transition table
static fsm_trans_t test_tt[] = {
	{IDLE, 			button_pressed, 	LOAD, 			loadValues	},
	{LOAD,			wait_finished,		READ,			startTest	},
	{READ,			wait_finished,		STOP,			stopTest	},
	{STOP,			end_list,			CONTINUE,		fin			},
	{STOP,			next_list,			LOAD,			next		},
	{CONTINUE,		wait_finished,		LOAD,			next		},
	{CONTINUE,		new,				IDLE,			reset		},
	{-1, NULL, -1, NULL },
};

// Mantenemos en este caso el PWM siempre encendido
static test_t tablaTest[] = {
		{{0,100}, 	{95,0}, 	TOTAL},
		{{0,0}, {0,0}, 0},
};

/* TBASIC EST PROCESS ************************************/
YT_PROCESS void periodicSineTest_func(void const * argument);

YT_PROCESS void testTask(void const * argument);
YT_PROCESS void input(void const * argument);
YT_PROCESS void output(void const * argument);

static void timer(void const * args);
static void wait(void const * args);

static void switchEnable(void const * args);
static void updateDAC(void const * args);

static void timer(void const * args) {
	flags |= TIMER_FIN;
}

static void wait(void const * args) {
	flags |= WAIT_FIN;
}

static void switchEnable(void const * args) {
	ytTimeMeas_t* antirrebotes = (ytTimeMeas_t*) args;

	ytStopTimeMeasure(antirrebotes);
	if (antirrebotes->elapsed_time < MARGEN_ANTIRREBOTES*1000){
		ytStartTimeMeasure(antirrebotes);
		return;
	}

	flags |= BUTTON_PRES;
	ytStartTimeMeasure(antirrebotes);
}

static int button_pressed(fsm_t* this){
	return flags & BUTTON_PRES;
}

static int wait_finished(fsm_t* this){
	return flags & WAIT_FIN;
}

static int end_list(fsm_t* this){
	return flags & ENDLIST;
}

static int next_list(fsm_t* this){
	return !(flags & ENDLIST);
}

static int new(fsm_t* this){
	return (flags & FIN) && (flags & PRINT);
}

static void loadValues(fsm_t* this){
	uint8_t i;
	data_fsm_t* data = ((data_fsm_t*)(this->mutex_sem_data));
	flags = 0;
	globalCounter = 0;

	ytPrintf("%d,", data->maxTest);
	for(i = 0; i < N_FREC; i++){
		ytPrintf("%d,", frecuencias[i]);
	}
	ytPrintf("%d,%d,%d,%d\r\n", REPETITION_TEST, TOTAL/1000, N_PRUEBAS, T_PRUEBAS);
	ytTimerStart(data->tGlob, ESPERA);
}

static void next(fsm_t* this){
	data_fsm_t* data = ((data_fsm_t*)(this->mutex_sem_data));
	flags = 0;
	ytTimerStart(data->tGlob, ESPERA);
}

static void startTest(fsm_t* this){
	data_fsm_t* data = ((data_fsm_t*)(this->mutex_sem_data));
	uint8_t numTest = data->nTest;
	flags &= ~(WAIT_FIN | TIMER_FIN);
	xQueueReset(data->voltajes);
	test_t test = (data->testTable)[0];

	ytTimerStart(data->tGlob, test.tTotal/1000);
	ytTimerStart(tiempoRepet, 1);
	ytTimerStart(tiempoSine, 20/frecuencias[numTest%N_FREC]);

	ytIoctl(data->driverPWM_fd, (uint16_t) PWM_TIM_SEL_PWM, (void*) PWM_TIM4_B9);
	ytIoctl(data->driverPWM_fd, (uint16_t) PWM_TIM_SET_FREQ, (void*) makeFixPoint(((float)(test.frecuencia.h_fix*1000+test.frecuencia.l_fix))/1000));
	ytIoctl(data->driverPWM_fd, (uint16_t) PWM_TIM_SET_DUTY, (void*) makeFixPoint((float)test.duty.h_fix));
	ytIoctl(data->driverPWM_fd, (uint16_t) PWM_TIM_START, (void*) PWM_TIM4_B9);

	nMuestra = 0;
	nQueue = 0;
	HAL_ADC_Start_IT(&hadc1);
	//HAL_TIM_Base_Start_IT(&htim3);
}

static void stopTest(fsm_t* this){
	data_fsm_t* data = ((data_fsm_t*)(this->mutex_sem_data));
	uint16_t tmp;
	flags &= ~(WAIT_FIN | TIMER_FIN);
	HAL_ADC_Stop_IT(&hadc1);
	//HAL_TIM_Base_Stop(&htim3);
	ytTimerStop(tiempoSine);
	ytTimerStop(tiempoRepet);
	sineReset();

	ytIoctl(data->driverPWM_fd, (uint16_t) PWM_TIM_SEL_PWM, (void*) PWM_TIM4_B9);
	ytIoctl(data->driverPWM_fd, (uint16_t) PWM_TIM_STOP_DOWN, (void*) PWM_TIM4_B9);

	nTimes++;
	if(nTimes == REPETITION_TEST){
		nTimes = 0;
		data->nTest++;
		if(data->nTest >= data->maxTest){
			flags |= ENDLIST;
		}
	}
	ytStartProcess("Output", output, LOW_PRIORITY_PROCESS, PROC_STACK_SIZE, &tmp, (void*) data->voltajes);
	ytTimerStop(data->tGlob);
}

static void fin(fsm_t* this){
	data_fsm_t* data = ((data_fsm_t*)(this->mutex_sem_data));
	data->nTest = 0;
	flags = 0;
	globalCounter++;
	if(globalCounter >= N_PRUEBAS)
		flags |= FIN;
	else
		ytTimerStart(data->tGlob, T_PRUEBAS*60*1000);
}

static void reset(fsm_t* this){
	flags = 0;
	ytPrintf("\r\n******************************");
	ytPrintf("\r\n******** TEST ACABADO ********");
	ytPrintf("\r\n******************************\r\n\r\n");
	ytPrintf("Pulsa el bot�n o la letra s para comenzar con el test: \r\n");
}

ytInitProcess(periodicSineTest_func_proc, periodicSineTest_func, DEFAULT_PROCESS, 2048, &periodicSineTestProcId, NULL);

YT_PROCESS void periodicSineTest_func(void const * argument){

	uint16_t control;
	data_fsm_t *dataTest;
	time_meas_t* antirrebotes;
	fsm_t *test_fsm;
	adc_config_t adc_config;
	antirrebotes = new_time_meas();
	init_time_meas();
	dataTest = (data_fsm_t*) ytMalloc(sizeof(data_fsm_t));
	test_fsm = fsm_new(test_tt, (void*) dataTest);
	tiempoSine = ytTimerCreate(ytTimerPeriodic, updateDAC, NULL);
	sendData = 0;
	nTimes = 0;
	sineInit();
	sineGenerate();
	//HAL_TIM_Base_Stop_IT(&htim3);

	adc_config.dual_mode_enabled = 0;
	adc_config.channel_speed = 12500;
	adc_config.diff2_mode_enabled = 0;
	adc_config.diff1_mode_enabled = 0;
	adc_config.adc_pin_se1 = GPIO_PIN_A1;

	adc_init(&adc_config);

	maxSizeQueue = (adc_config.channel_speed*(TOTAL/1000000))/2*6/10;			// /4 por la division del reloj en la inicializacion de la HAL y entre la relacion de pasar de usar 2.5 ciclos a 24.5
	voltajes = xQueueCreate(maxSizeQueue, sizeof(uint16_t));

	dataTest->driverPWM_fd = ytOpen(PWM_TIM_DEV, 0);
	dataTest->testTable = tablaTest;
	dataTest->nTest = 0;
	dataTest->maxTest = N_FREC;
	dataTest->voltajes = voltajes;
	dataTest->tGlob = ytTimerCreate(ytTimerOnce, wait, NULL);
	dataTest->tRep = ytTimerCreate(ytTimerPeriodic, timer, NULL);

	ytIoctl(dataTest->driverPWM_fd, (uint16_t) PWM_TIM_NEW, (void*) PWM_TIM4_B9);
	ytIoctl(dataTest->driverPWM_fd, (uint16_t) PWM_TIM_SET_DUTY, (void*) makeFixPoint(50));
	ytIoctl(dataTest->driverPWM_fd, (uint16_t) PWM_TIM_START, (void*) PWM_TIM4_B9);
	ytIoctl(dataTest->driverPWM_fd, (uint16_t) PWM_TIM_STOP_DOWN, (void*) PWM_TIM4_B9);

	ytStartProcess("Test", testTask, DEFAULT_PROCESS, PROC_STACK_SIZE, &control, (void *) test_fsm);
	ytStartProcess("Input", input, HIGH_PRIORITY_PROCESS, PROC_STACK_SIZE, &control, NULL);

	ytGpioInitPin(GPIO_PIN_B1, GPIO_PIN_INTERRUPT_RISING, GPIO_PIN_PULLDOWN);
	ytGpioPinSetCallbackInterrupt(GPIO_PIN_B1, switchEnable, (void *) &antirrebotes);

	ytPrintf("Pulsa el bot�n o la letra s para comenzar con el test: \r\n");


	while(1){
		ytDelay(1000);
	}
}
/* *************************************************/

YT_PROCESS void testTask(void const * argument){
	fsm_t* test_fsm = (fsm_t*) argument;
	TickType_t lastTime;

	while(1){
		lastTime = xTaskGetTickCount();
		fsm_fire(test_fsm);
		vTaskDelay(pdMS_TO_TICKS(PERIODO_FSM) - (xTaskGetTickCount()-lastTime) % pdMS_TO_TICKS(PERIODO_FSM));
	}
}

YT_PROCESS void input(void const * argument){
	uint8_t *datos;
	int i;
	datos = (uint8_t*) ytMalloc(MAX_BUFFER*sizeof(uint8_t));
	while(1){
		ytStdinRead(datos, (uint8_t) MAX_BUFFER, 250);
		for (i = 0; i < MAX_BUFFER; i++){
			if(datos[i] == '\0' || datos[i] == '\r'){
				datos[i] = '\0';
				break;
			}
			else if (datos[i] == 's'){
				flags |= BUTTON_PRES;
				ytPrintf("\r\n");
				datos[i] = '\0';
				break;
			}
			else if (datos[i] == 'n'){
				flags |= FIN;
				datos[i] = '\0';
				break;
			}
			else{
				break;
			}
		}
		ytDelay(50);
	}
}

YT_PROCESS void output(void const * argument){
	QueueHandle_t volt = (QueueHandle_t) argument;
#if USE_FILTER
	uint16_t i, j;
	uint8_t tamBuffer = 25;
#if FILTER_15
	int16_t dest[tamBuffer], sour[tamBuffer];
	arm_biquad_casd_df1_inst_q15 S;
	q15_t estado[4] = {0, 0, 0, 0};
	for (i = 0; i < maxSizeQueue/tamBuffer; i++) {
		arm_biquad_cascade_df1_init_q15(&S,1,coeff15,estado, shift);
		for(j = 0; j < 25; j++)
			xQueueReceive(volt, &(sour[j]), pdMS_TO_TICKS(1000));
		arm_biquad_cascade_df1_q15(&S, sour, dest, tamBuffer);
		for (j = 0; j < tamBuffer; j++)
			ytPrintf("%d,", dest[j]);
		estado[0] = sour[tamBuffer-1];
		estado[1] = sour[tamBuffer-2];
		estado[2] = dest[tamBuffer-1];
		estado[3] = dest[tamBuffer-2];
	}
#endif
#if FILTER_31
	int32_t dest[tamBuffer], sour[tamBuffer];
	arm_biquad_casd_df1_inst_q31 S;
	q31_t *estado = ytMalloc(4*sizeof(q31_t));
	for(j = 0; j < tamBuffer; j++){
		sour[j] = 0;
		dest[j] = 0;
	}
	arm_biquad_cascade_df1_init_q31(&S,1,coeff31,estado, shift);
	for (i = 0; i < maxSizeQueue/tamBuffer; i++) {
		for(j = 0; j < tamBuffer; j++){
			xQueueReceive(volt, &(sour[j]), pdMS_TO_TICKS(1000));
			//ytPrintf("%u,", sour[j]);
		}
		//ytPrintf("\r\n");
		arm_biquad_cascade_df1_q31(&S, sour, dest, tamBuffer);
		for (j = 0; j < tamBuffer; j++){
			ytPrintf("%d,", dest[j]);
		}
		//ytPrintf("\r\n");

		estado[0] = *(sour+tamBuffer-1);
		estado[1] = *(sour+tamBuffer-2);
		estado[2] = *(dest+tamBuffer-1);
		estado[3] = *(dest+tamBuffer-2);
	}
#endif
#else
	uint16_t value = 0;
	while(xQueueReceive(volt, &value, pdMS_TO_TICKS(2000)) != errQUEUE_EMPTY) {
		ytPrintf("%u,", value);
	}
#endif
	ytPrintf("\r\n");
	flags |= PRINT;
}

static retval_t adc_init(adc_config_t* adc_config)
{
	uint32_t oversampling, bitShift, speed, channel;

	ADC_MultiModeTypeDef multimode;
	ADC_ChannelConfTypeDef sConfig1;

	speed = adc_config->channel_speed;

	oversampling = (uint32_t) HAL_RCC_GetHCLKFreq()/(speed*15);
	if(oversampling > 128){
		speed = (uint32_t) HAL_RCC_GetHCLKFreq()/(256*15);
		oversampling = ADC_OVERSAMPLING_RATIO_256;
		bitShift = ADC_RIGHTBITSHIFT_4;
	}
	else if(oversampling > 64){
		speed = (uint32_t) HAL_RCC_GetHCLKFreq()/(128*15);
		oversampling = ADC_OVERSAMPLING_RATIO_128;
		bitShift = ADC_RIGHTBITSHIFT_3;
	}
	else if(oversampling > 32){
		speed = (uint32_t) HAL_RCC_GetHCLKFreq()/(64*15);
		oversampling = ADC_OVERSAMPLING_RATIO_64;
		bitShift = ADC_RIGHTBITSHIFT_2;
	}
	else if(oversampling > 16){
		speed = (uint32_t) HAL_RCC_GetHCLKFreq()/(32*15);
		oversampling = ADC_OVERSAMPLING_RATIO_32;
		bitShift = ADC_RIGHTBITSHIFT_1;
	}
	else if(oversampling > 8){
		speed = (uint32_t) HAL_RCC_GetHCLKFreq()/(16*15);
		oversampling = ADC_OVERSAMPLING_RATIO_16;
		bitShift = ADC_RIGHTBITSHIFT_NONE;
	}
	else if(oversampling > 4){
		speed = (uint32_t) HAL_RCC_GetHCLKFreq()/(8*15);
		oversampling = ADC_OVERSAMPLING_RATIO_8;
		bitShift = ADC_RIGHTBITSHIFT_NONE;
	}
	else if(oversampling > 2){
		speed = (uint32_t) HAL_RCC_GetHCLKFreq()/(4*15);
		oversampling = ADC_OVERSAMPLING_RATIO_4;
		bitShift = ADC_RIGHTBITSHIFT_NONE;
	}
	else if(oversampling >= 0){
		speed = (uint32_t) HAL_RCC_GetHCLKFreq()/(2*15);
		oversampling = ADC_OVERSAMPLING_RATIO_2;
		bitShift = ADC_RIGHTBITSHIFT_NONE;
	}
	else{
		return RET_ERROR;
	}

	switch(adc_config->adc_pin_se1){
	case GPIO_PIN_A0:
		channel = ADC_CHANNEL_5;
		break;
	case GPIO_PIN_A1:
		channel = ADC_CHANNEL_6;
		break;
	case  GPIO_PIN_A4:
		channel = ADC_CHANNEL_9;
		break;
	case GPIO_PIN_C1:
		channel = ADC_CHANNEL_2;
		break;
	case GPIO_PIN_C3:
		channel = ADC_CHANNEL_4;
		break;
	case GPIO_PIN_C5:
		channel = ADC_CHANNEL_14;
		break;
	default:
		return RET_ERROR;
		break;
	}

	/* Config GPIO */
	if(ytGpioInitPin(adc_config->adc_pin_se1, GPIO_PIN_ANALOG_ADC_CONTROL, GPIO_PIN_NO_PULL) != RET_OK){
		return RET_ERROR;
	}

	/* Config channel */
	sConfig1.SingleDiff = ADC_SINGLE_ENDED;

	/* Config speed */
	hadc1.Init.Oversampling.Ratio = oversampling;
	hadc1.Init.Oversampling.RightBitShift = bitShift;
	if (HAL_ADC_Init(&hadc1) != HAL_OK)
	{
		return RET_ERROR;
	}

	/* COnfig DMA */
	hdma_adc1.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;
	hdma_adc1.Init.MemDataAlignment = DMA_MDATAALIGN_HALFWORD;
	if (HAL_DMA_Init(&hdma_adc1) != HAL_OK)
	{
		return RET_ERROR;
	}

	/* Config multi mode */
	multimode.Mode = ADC_MODE_INDEPENDENT;
	if (HAL_ADCEx_MultiModeConfigChannel(&hadc1, &multimode) != HAL_OK)
	{
		return RET_ERROR;
	}

	/* Config channel */
	sConfig1.Channel = channel;
	sConfig1.Rank = 1;
	sConfig1.SamplingTime = ADC_SAMPLETIME_12CYCLES_5;
	sConfig1.OffsetNumber = ADC_OFFSET_NONE;
	sConfig1.Offset = 0;
	if (HAL_ADC_ConfigChannel(&hadc1, &sConfig1) != HAL_OK)
	{
		return RET_ERROR;
	}

	return RET_OK;
}

// Si esta esto aqui quiere decir que se ha comentado la del driver de Rober
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc){
	uint16_t tmp = (uint16_t) HAL_ADC_GetValue(hadc);
	xQueueSendToBackFromISR(voltajes, &tmp, pdFALSE);
}

static void updateDAC(void const * args)
{
	sineNext();
}

#endif
#endif
#endif
