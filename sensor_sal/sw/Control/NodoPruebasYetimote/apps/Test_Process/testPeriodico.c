/*
 * testPeriodico.c
 *
 *  Created on: 27 jun. 2018
 *      Author: miguelvp
 */

#include "system_api.h"
#include "platform-conf.h"

#define USE_PERIODIC_TEST	0

#if USE_PERIODIC_TEST

#if ENABLE_PWM_TIM_DRIVER
#if ENABLE_ADC_DRIVER
#include "pwm_tim_driver.h"
#include "pwm_tim_arch.h"
#include "time_meas.h"
#include "fsm.h"
#include "task.h"
#include "projdefs.h"
#include "queue.h"
#include "stm32l4xx_hal_adc.h"

#define DUTY_LED 			50
#define PROC_STACK_SIZE 	512
#define PERIODO				3			// milisegundos
#define MARGEN_ANTIRREBOTES 500
#define TOTAL 				6000			// mseg
#define MUESTREO			6			// milisegundos
#define T_PRUEBAS			10			// minutos
#define N_PRUEBAS			6

#define REPETITION_TEST		3
#define ESPERA				45000

#define DATA_TO_READ	16
#define MAX_BUFFER		4

#define BUTTON_PRES		0x01U
#define WAIT_FIN		0x02U
#define TIMER_FIN		0x04U
#define ENDLIST			0x08U
#define REPEAT			0x10U
#define FIN				0x20U


static uint32_t maxSizeQueue;
static uint16_t flags;
static uint8_t sendData;
static uint8_t nTimes, globalCounter;

uint16_t periodicTestProcId;

uint32_t imprime;

uint16_t counter_ADC, counter_PWM;
//static QueueHandle_t voltajes;

typedef struct test {
	fix_point_t frecuencia;
	fix_point_t duty;
	uint16_t tSample;
	uint32_t tTotal;
} test_t;

typedef struct data_fsm {
	test_t* testTable;
	uint32_t driverADC_fd;
	uint32_t driverPWM_fd;
	QueueHandle_t voltajes;
	uint32_t tGlob;
	uint32_t tRep;
	uint8_t nTest;
	uint8_t maxTest;
	uint8_t booleanSal;
} data_fsm_t;

// Test states machine
static enum testStates {
	IDLE,
	LOAD,
	READ,
	STOP,
	CONTINUE,
};

// Test functions
static int button_pressed(fsm_t* this);
static int wait_finished(fsm_t* this);
static int timer_finished(fsm_t* this);
static int next_list(fsm_t* this);
static int end_list(fsm_t* this);
static int new(fsm_t* this);
static void loadValues(fsm_t* this);
static void next(fsm_t* this);
static void startTest(fsm_t* this);
static void read(fsm_t* this);
static void stopTest(fsm_t* this);
static void fin(fsm_t* this);
static void reset(fsm_t* this);

// Test transition table
static fsm_trans_t test_tt[] = {
	{IDLE, 			button_pressed, 	LOAD, 			loadValues	},
	{LOAD,			wait_finished,		READ,			startTest	},
	{READ, 			timer_finished,		READ, 			read		},
	{READ,			wait_finished,		STOP,			stopTest	},
	{STOP,			end_list,			CONTINUE,		fin			},
	{STOP,			next_list,			LOAD,			next		},
	{CONTINUE,		wait_finished,		LOAD,			next		},
	{CONTINUE,		new,				IDLE,			reset		},
	{-1, NULL, -1, NULL },
};

static test_t tablaTest[] = {
		// Completar con los datos, pero esta vez que solo haya unos valores de test
		/*{{2,0}, 	{10,0}, 	MUESTREO, 	TOTAL},*/
		{{1,500}, 	{10,0}, 	MUESTREO, 	TOTAL},
		{{0,0}, {0,0}, 0, 0},
};

/* TBASIC EST PROCESS ************************************/
YT_PROCESS void periodicTest_func(void const * argument);

YT_PROCESS void testTask(void const * argument);
YT_PROCESS void input(void const * argument);

static void timer(void const * args);
static void wait(void const * args);

static void switchEnable(void const * args);

static void timer(void const * args) {
	flags |= TIMER_FIN;
}

static void wait(void const * args) {
	flags |= WAIT_FIN;
}

static void switchEnable(void const * args) {
	ytTimeMeas_t* antirrebotes = (ytTimeMeas_t*) args;

	ytStopTimeMeasure(antirrebotes);
	if (antirrebotes->elapsed_time < MARGEN_ANTIRREBOTES*1000){
		ytStartTimeMeasure(antirrebotes);
		return;
	}

	flags |= BUTTON_PRES;
	ytStartTimeMeasure(antirrebotes);
}

static int button_pressed(fsm_t* this){
	return flags & BUTTON_PRES;
}

static int wait_finished(fsm_t* this){
	return flags & WAIT_FIN;
}

static int timer_finished(fsm_t* this){
	return flags & TIMER_FIN;
}

static int end_list(fsm_t* this){
	return flags & ENDLIST;
}

static int next_list(fsm_t* this){
	return !(flags & ENDLIST);
}

static int new(fsm_t* this){
	return flags & FIN;
}

static void loadValues(fsm_t* this){
	uint8_t i;
	data_fsm_t* data = ((data_fsm_t*)(this->mutex_sem_data));
	test_t test;
	xQueueReset(data->voltajes);
	flags = 0;
	globalCounter = 0;

	ytPrintf("%d,", data->maxTest);
	for(i = 0; i < data->maxTest; i++){
		test = (data->testTable)[i];
		ytPrintf("%d.%d%d%d,", test.frecuencia.h_fix, test.frecuencia.l_fix/100%10, test.frecuencia.l_fix/10%10, test.frecuencia.l_fix%10);
	}
	ytPrintf("%d,%d,%d,%d,%d\r\n", REPETITION_TEST, TOTAL, MUESTREO, N_PRUEBAS, T_PRUEBAS);
	ytTimerStart(data->tGlob, ESPERA);
}

static void next(fsm_t* this){
	data_fsm_t* data = ((data_fsm_t*)(this->mutex_sem_data));
	xQueueReset(data->voltajes);
	flags = 0;

	ytPrintf("\r\n");
	ytTimerStart(data->tGlob, ESPERA);
}

static void startTest(fsm_t* this){
	data_fsm_t* data = ((data_fsm_t*)(this->mutex_sem_data));
	uint8_t numTest = data->nTest;
	flags &= ~(WAIT_FIN | TIMER_FIN);
	test_t test = (data->testTable)[numTest];

	ytTimerStart(data->tRep, test.tSample);
	ytTimerStart(data->tGlob, test.tTotal);

	ytIoctl(data->driverPWM_fd, (uint16_t) PWM_TIM_SEL_PWM, (void*) PWM_TIM4_B9);
	ytIoctl(data->driverPWM_fd, (uint16_t) PWM_TIM_SET_FREQ, (void*) makeFixPoint(((float)(test.frecuencia.h_fix*1000+test.frecuencia.l_fix))/1000));
	ytIoctl(data->driverPWM_fd, (uint16_t) PWM_TIM_SET_DUTY, (void*) makeFixPoint((float)test.duty.h_fix));
	ytIoctl(data->driverPWM_fd, (uint16_t) PWM_TIM_START, (void*) PWM_TIM4_B9);
}

static void stopTest(fsm_t* this){
	data_fsm_t* data = ((data_fsm_t*)(this->mutex_sem_data));
	uint16_t value = 0;
	flags &= ~(WAIT_FIN | TIMER_FIN);

	ytIoctl(data->driverPWM_fd, (uint16_t) PWM_TIM_SEL_PWM, (void*) PWM_TIM4_B9);
	ytIoctl(data->driverPWM_fd, (uint16_t) PWM_TIM_STOP_DOWN, (void*) PWM_TIM4_B9);

	nTimes++;
	if(nTimes == REPETITION_TEST){
		nTimes = 0;
		data->nTest++;
		if(data->nTest >= data->maxTest){
			flags |= ENDLIST;
		}
	}
	while(xQueueReceive(data->voltajes, &value, pdMS_TO_TICKS(1000)) != errQUEUE_EMPTY) {
		ytPrintf("%u,", value);
	}
	ytTimerStop(data->tRep);
	ytTimerStop(data->tGlob);
}

static void read(fsm_t* this){
	data_fsm_t* data = ((data_fsm_t*)(this->mutex_sem_data));
	uint16_t* volt = (uint16_t*) ytMalloc(DATA_TO_READ*sizeof(uint16_t));
	flags &= ~TIMER_FIN;
	ytTimerStart(data->tRep, ((data->testTable)[(data->nTest)]).tSample);

	ytRead(data->driverADC_fd, (void*) volt, DATA_TO_READ);
	xQueueSend(data->voltajes, (void*) volt, 1000);
	ytFree(volt);

}

static void fin(fsm_t* this){
	data_fsm_t* data = ((data_fsm_t*)(this->mutex_sem_data));
	data->nTest = 0;
	flags = 0;
	globalCounter++;
	if(globalCounter >= N_PRUEBAS)
		flags |= FIN;
	else
		ytTimerStart(data->tGlob, T_PRUEBAS*60*1000);
}

static void reset(fsm_t* this){
	flags = 0;
	ytPrintf("\r\n******************************");
	ytPrintf("\r\n******** TEST ACABADO ********");
	ytPrintf("\r\n******************************\r\n\r\n");
}

ytInitProcess(periodicTest_func_proc, periodicTest_func, DEFAULT_PROCESS, 2048, &periodicTestProcId, NULL);

YT_PROCESS void periodicTest_func(void const * argument){

	uint8_t i;
	uint16_t control, *tmp;
	//uint32_t adc_fd, pwm_fd, tiempoGlobal, tiempoRepet;
	data_fsm_t *dataTest;
	time_meas_t* antirrebotes;
	retval_t intento;
	fsm_t *test_fsm;
	adc_config_t adc_config;
	//QueueHandle_t voltajes;
	//pwm_fd = ytOpen(PWM_TIM_DEV, 0);
	//adc_fd = ytOpen(ADC_DEV, 0);
	antirrebotes = new_time_meas();
	init_time_meas();
	dataTest = (data_fsm_t*) ytMalloc(sizeof(data_fsm_t));
	test_fsm = fsm_new(test_tt, (void*) dataTest);
	tmp = (uint16_t*) ytMalloc(sizeof(uint16_t));
	//voltajes = xQueueCreate(TOTAL/MUESTREO, sizeof(uint16_t));
	//tiempoRepet = ytTimerCreate(ytTimerPeriodic, timer, NULL);
	//tiempoGlobal = ytTimerCreate(ytTimerOnce, wait, NULL);
	sendData = 0;
	nTimes = 0;

	// Numero de valores de test
	for(i = 0; tablaTest[i].tTotal != 0 && tablaTest[i].tSample != 0; i++){
	}

	adc_config.dual_mode_enabled = 0;
	adc_config.channel_speed = 12500;
	adc_config.diff2_mode_enabled = 0;
	adc_config.diff1_mode_enabled = 0;
	adc_config.adc_pin_se1 = GPIO_PIN_A1;

	dataTest->driverADC_fd = ytOpen(ADC_DEV, 0);
	dataTest->driverPWM_fd = ytOpen(PWM_TIM_DEV, 0);
	dataTest->testTable = tablaTest;
	dataTest->nTest = 0;
	dataTest->maxTest = i;
	dataTest->voltajes = xQueueCreate(TOTAL/MUESTREO, sizeof(uint16_t));
	dataTest->tGlob = ytTimerCreate(ytTimerOnce, wait, NULL);
	dataTest->tRep = ytTimerCreate(ytTimerPeriodic, timer, NULL);
	dataTest->booleanSal = 1;

	ytIoctl(dataTest->driverPWM_fd, (uint16_t) PWM_TIM_NEW, (void*) PWM_TIM4_B9);
	//ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SET_FREQ, (void*) makeFixPoint(10));
	ytIoctl(dataTest->driverPWM_fd, (uint16_t) PWM_TIM_SET_DUTY, (void*) makeFixPoint(DUTY_LED));
	ytIoctl(dataTest->driverPWM_fd, (uint16_t) PWM_TIM_START, (void*) PWM_TIM4_B9);
	ytIoctl(dataTest->driverPWM_fd, (uint16_t) PWM_TIM_STOP_DOWN, (void*) PWM_TIM4_B9);

	ytIoctl(dataTest->driverADC_fd, (uint16_t) CONFIG_ADC, (void*) &adc_config);
	ytRead(dataTest->driverADC_fd, (void*) tmp, DATA_TO_READ);

	intento = ytStartProcess("Test", testTask, HIGH_PRIORITY_PROCESS, PROC_STACK_SIZE, &control, (void *) test_fsm);
	ytStartProcess("Input", input, LOW_PRIORITY_PROCESS, PROC_STACK_SIZE, &control, NULL);

	ytGpioInitPin(GPIO_PIN_B1, GPIO_PIN_INTERRUPT_RISING, GPIO_PIN_PULLDOWN);
	ytGpioPinSetCallbackInterrupt(GPIO_PIN_B1, switchEnable, (void *) &antirrebotes);

	ytPrintf("Pulsa el boton para comenzar con el test: \r\n");

	while(1){

//		ytPrintf("Enable PWM: %d\r\n", enable);

		ytDelay(1000);
	}
}
/* *************************************************/

YT_PROCESS void testTask(void const * argument){
	fsm_t* test_fsm = (fsm_t*) argument;
	TickType_t lastTime;

	while(1){
		lastTime = xTaskGetTickCount();
		fsm_fire(test_fsm);
		vTaskDelay(pdMS_TO_TICKS(PERIODO) - (xTaskGetTickCount()-lastTime) % pdMS_TO_TICKS(PERIODO));
	}
}

YT_PROCESS void input(void const * argument){
	uint8_t *datos;
	int i;
	datos = (uint8_t*) ytMalloc(MAX_BUFFER*sizeof(uint8_t));
	while(1){
		ytStdinRead(datos, (uint8_t) MAX_BUFFER, 250);
		for (i = 0; i < MAX_BUFFER; i++){
			if(datos[i] == '\0' || datos[i] == '\r'){
				datos[i] = '\0';
				break;
			}
			else if (datos[i] == 's'){
				flags |= REPEAT;
				datos[i] = '\0';
				break;
			}
			else if (datos[i] == 'n'){
				flags |= FIN;
				datos[i] = '\0';
				break;
			}
			else{
				break;
			}
		}
		ytDelay(50);
	}
}

#endif
#endif
#endif
