/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * basic_test.c
 *
 *  Created on: 12 de ene. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file basic_test.c
 */

#include "system_api.h"
#include "platform-conf.h"

#define USE_BASIC_TEST	0

#if USE_BASIC_TEST

#if ENABLE_PWM_TIM_DRIVER
#include "pwm_tim_driver.h"

uint16_t basicTestProcId;

/* TBASIC EST PROCESS ************************************/
YT_PROCESS void basic_test_func(void const * argument);

ytInitProcess(basic_test_proc, basic_test_func, DEFAULT_PROCESS, 2048, &basicTestProcId, NULL);


YT_PROCESS void basic_test_func(void const * argument){
//	uint32_t sensor_fd;
//	float32_t temp_val = 0;
//	uint16_t proc_id;

	uint32_t pwm_fd;
	void* timerA0 = (void*) PWM_TIM5_A0;
	void* timerA1 = (void*) PWM_TIM2_A1;
	void* timerA4 = (void*) PWM_LPTIM2_A4;
	void* timerB1 = (void*) PWM_TIM1_B1;
	void* timerB8 = (void*) PWM_TIM4_B8;
	void* timerB9 = (void*) PWM_TIM17_B9;
	void* timerB11 = (void*) PWM_TIM2_B11;
	void* timerC1 = (void*) PWM_LPTIM1_C1;
	pwm_fd = 0;
	pwm_fd = ytOpen(PWM_TIM_DEV, 0);

	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_NEW, timerB11);
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_NEW, timerB1);
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_NEW, timerB8);
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_NEW, timerA0);
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_NEW, timerA1);
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_NEW, timerB9);
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_NEW, timerC1);
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_NEW, timerA4);

	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SEL_PWM, timerB9);
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SET_FREQ, (void *) makeFixPoint(200));
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SET_DUTY, (void *) makeFixPoint(25));
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_START, timerB9);

	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_STOP, timerB9); // Mirar en qu� estado se queda en reposo
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_START, timerB9);
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_STOP_DOWN, timerB9); // Mirar que se queda arriba
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_START, timerB9);
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_STOP_UP, timerB9); // Mirar que se queda arribaytIoctl(pwm_fd, (uint16_t) PWM_TIM_START, timerB9);
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_START, timerB9);
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_STOP_DOWN, timerB9); // Mirar que se queda arriba


	// No arranca
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SEL_PWM, timerA1);
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SET_FREQ, (void*) makeFixPoint(1));
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SET_DUTY, (void*) makeFixPoint(22.5));
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_START, timerA1);

	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SEL_PWM, timerA4);
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SET_FREQ, (void*) makeFixPoint(1.965));
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SET_DUTY, (void*) makeFixPoint(50));
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_START, timerA4);

	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SEL_PWM, timerA0);
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SET_FREQ, (void*) makeFixPoint(0.5));
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SET_DUTY, (void*) makeFixPoint(12.5));
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_START, timerA0);

	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SEL_PWM, timerB1);
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SET_FREQ, (void*) makeFixPoint(2.43));
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SET_DUTY, (void*) makeFixPoint(33.33));
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_START, timerB1);

	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_STOP, timerA0);

	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SEL_PWM, timerC1);
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SET_FREQ, (void*) makeFixPoint(50));
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SET_DUTY, (void*) makeFixPoint(25));
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_START, timerC1);

	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SEL_PWM, timerB8);
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SET_FREQ, (void*) makeFixPoint(250));
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SET_DUTY, (void*) makeFixPoint(75));
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_START, timerB8);

	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SEL_PWM, timerB11);
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SET_FREQ, (void*) makeFixPoint(433000));
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SET_DUTY, (void*) makeFixPoint(50));
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_START, timerB11);

	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SEL_PWM, timerB9);
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SET_FREQ, (void*) makeFixPoint(868000));
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SET_DUTY, (void*) makeFixPoint(50));
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_START, timerB9);

	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SEL_PWM, timerB8);
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SET_DUTY, (void*) makeFixPoint(25));

	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SEL_PWM, timerC1);
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SET_DUTY, (void*) makeFixPoint(50));

	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_DELETE, timerB1);
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_START, timerA0);
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_START, timerC1);
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_START, timerB8);

	timerB1 = (void*) PWM_TIM3_B1;

	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_NEW, timerB1);
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SET_FREQ, (void*) makeFixPoint(25));
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SET_DUTY, (void*) makeFixPoint(5.5));
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_START, timerB1);



	while(1){



//		sensor_fd = ytOpen("DEV_AT30TS75", 0);
//		ytRead(sensor_fd, &temp_val, 1);
//		ytClose(sensor_fd);
//
//		ytPrintf("Read temp: %.4f\r\n", temp_val);

		ytDelay(1000);
 		uint32_t size = xPortGetFreeHeapSize();

	}
}
/* *************************************************/

#endif

#endif
