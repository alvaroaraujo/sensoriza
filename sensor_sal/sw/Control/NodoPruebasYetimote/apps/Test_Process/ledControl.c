/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * basic_test.c
 *
 *  Created on: 12 de ene. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file ledControl.c
 */

#include "system_api.h"
#include "platform-conf.h"

#define USE_SPECIFIC_TEST	0

#if USE_SPECIFIC_TEST

#if ENABLE_PWM_TIM_DRIVER
#if ENABLE_ADC_DRIVER
#include "pwm_tim_driver.h"
#include "time_meas.h"
#include "fsm.h"
#include "task.h"
#include "projdefs.h"
#include "queue.h"

#define PWM_OPTION 			1
#define DUTY_LED 			50
#define PROC_STACK_SIZE 	512
#define TIMER_DELAY			20			// milisegundos
#define MARGEN_ANTIRREBOTES 500
#define PERIODO				10			// milisegundos
#define QUEUE_SIZE			6000/TIMER_DELAY

#define DATA_TO_READ	16
#define MAX_BUFFER		4

#define BUTTON_PRES_1	0x01U
#define BUTTON_PRES_2	0x02U
#define TIMER_FIN		0x04U
#define OUTPUT_DONE		0x08U
#define NEW_DATA		0x10U

static uint16_t flags;
static uint8_t sendData;

QueueHandle_t voltajes;

uint16_t ledControlTestProcId;

uint32_t imprime;

uint16_t counter_ADC, counter_PWM;

typedef struct data_test{
	uint16_t new;
	uint16_t freqH;
	uint16_t freqL;
	uint16_t duty;
	uint16_t samp;
	uint16_t totalSec;
} data_test_t;

typedef struct data_fsm {
	uint32_t* mutex;
	void* data;
	uint32_t* driver_fd;
	data_test_t* test;
} data_fsm_t;

typedef struct data_tiempo {
	uint32_t* mutex;
	time_meas_t* reloj;
} data_tiempo_t;

// PWM manage states machine
static enum pwmStates {
	IDLE,
	RUN,
};

// ADC reader states machine
static enum adcStates {
	SLEEP,
	READ,
	OUT,
};

// Test Definer states machine
static enum testStates {
	WAIT,
	H_FREQ,
	L_FREQ,
	DUTY,
	SAMPLING,
	SEC,
	SALT,
};

// PWM manage functions
static void start_pwm(fsm_t* this);
static void stop_pwm(fsm_t* this);

// ADC reader functions
static int timer_finished(fsm_t* this);
static int always(fsm_t* this);
static int fin_prueba_PWM(fsm_t* this);
static int fin_prueba_ADC(fsm_t* this);
static void init_reader(fsm_t* this);
static void read_adc(fsm_t* this);
static void stop_reader(fsm_t* this);
static void output(fsm_t* this);

static int outputDone(fsm_t* this);
static int newData(fsm_t* this);
static void resetValues(fsm_t* this);
static void setH(fsm_t* this);
static void setL(fsm_t* this);
static void setDuty(fsm_t* this);
static void setSamp(fsm_t* this);
static void setSec(fsm_t* this);
static void startTest(fsm_t* this);


// Common machine functions
static int button_pressed_PWM(fsm_t* this);
static int button_pressed_ADC(fsm_t* this);

// PWM manage transitions table
static fsm_trans_t pwm_tt[] = {
	{IDLE, 	button_pressed_PWM, RUN, 	start_pwm	},
	{RUN, 	button_pressed_PWM, IDLE, 	stop_pwm 	},
	{RUN,	fin_prueba_PWM,		IDLE,	stop_pwm	},
	{-1, NULL, -1, NULL },
};

// ADC reader transitions table
static fsm_trans_t adc_tt[] = {
	{SLEEP, button_pressed_ADC, READ, 	init_reader},
	{READ,	button_pressed_ADC,	OUT,	output},
	{READ,	fin_prueba_ADC,		OUT,	output},
	{READ,	timer_finished,		READ, 	read_adc},
	{OUT,	always,				SLEEP,	stop_reader},
	{-1, NULL, -1, NULL},
};

// Test Definer transitions table
static fsm_trans_t test_tt[] = {
	{H_FREQ,	newData,	L_FREQ,		setH},
	{L_FREQ,	newData,	DUTY,		setL},
	{DUTY,		newData,	SAMPLING, 	setDuty},
	{SAMPLING,	newData,	SEC,		setSamp},
	{SEC,		newData,	SALT,		setSec},
	{SALT,		newData,	WAIT,		startTest},
	{WAIT, 		outputDone, H_FREQ,		resetValues},
	{-1, NULL, -1, NULL},
};



/* TBASIC EST PROCESS ************************************/
YT_PROCESS void ledControl_test_func(void const * argument);

YT_PROCESS void pwmTask(void const * argument);
YT_PROCESS void adcTask(void const * argument);
YT_PROCESS void testTask(void const * argument);
YT_PROCESS void input(void const * argument);
//YT_PROCESS void printVolt(void const * argument);

static void timer(void const * args);

static void switchEnable(void const * args);

static void timer(void const * args) {
	flags |= TIMER_FIN;
}

static void switchEnable(void const * args) {
	ytTimeMeas_t* antirrebotes = (ytTimeMeas_t*) (((data_tiempo_t*) args)->reloj);
	uint32_t* semaforo = (uint32_t*) ((data_tiempo_t*) args)->mutex;

	ytStopTimeMeasure(antirrebotes);
	if (antirrebotes->elapsed_time < MARGEN_ANTIRREBOTES*1000){
		ytStartTimeMeasure(antirrebotes);
		return;
	}

	ytSemaphoreWait(*semaforo, 1000);
	flags |= (BUTTON_PRES_1 | BUTTON_PRES_2);
	ytStartTimeMeasure(antirrebotes);
	ytSemaphoreRelease(*semaforo);
}

static int timer_finished(fsm_t* this){
	return flags & TIMER_FIN;
}

static int always(fsm_t* this){
	return 1;
}

static int fin_prueba_PWM(fsm_t* this){
	data_test_t* test = (data_test_t*) ((data_fsm_t*)(this->mutex_sem_data))->test;
	return (counter_PWM >= (test->totalSec/test->samp));
}

static int fin_prueba_ADC(fsm_t* this){
	data_test_t* test = (data_test_t*) ((data_fsm_t*)(this->mutex_sem_data))->test;
	return (counter_ADC >= (test->totalSec/test->samp));
}

static int button_pressed_PWM(fsm_t* this){
	return flags & BUTTON_PRES_1;
}

static int button_pressed_ADC(fsm_t* this){
	return flags & BUTTON_PRES_2;
}

static int outputDone(fsm_t* this){
	return flags & OUTPUT_DONE;
}

static int newData(fsm_t* this){
	return flags & NEW_DATA;
}

static void resetValues(fsm_t* this){

	flags &= ~OUTPUT_DONE;
	ytPrintf("\r\n\r\n----------------------------------------------------------------------\r\n\r\n");
	ytPrintf("NUEVO TEST\r\n\r\nIntroduce la frecuencia (1o entero, 2o decimal): ");
}

static void setH(fsm_t* this){
	uint32_t* semaforo = (uint32_t*) ((data_fsm_t*) this->mutex_sem_data)->mutex;
	data_test_t* data = (data_test_t*) ((data_fsm_t*) this->mutex_sem_data)->data;

	data->freqH = data->new;
	data->new = 0;

	ytSemaphoreWait(*semaforo, 10000);
	flags &= ~NEW_DATA;
	ytPrintf(".",data->freqH);
	ytSemaphoreRelease(*semaforo);
}

static void setL(fsm_t* this){
	uint32_t* semaforo = (uint32_t*) ((data_fsm_t*) this->mutex_sem_data)->mutex;
	data_test_t* data = (data_test_t*) ((data_fsm_t*) this->mutex_sem_data)->data;

	data->freqL = data->new;
	data->new = 0;

	ytSemaphoreWait(*semaforo, 10000);
	flags &= ~NEW_DATA;
	ytPrintf("\r\nEstablecido en %d.%d (decimal sobre 1000)\r\n\r\nIntroduce ciclo de trabajo (%%): ", data->freqH, data->freqL);
	ytSemaphoreRelease(*semaforo);
}

static void setDuty(fsm_t* this){
	uint32_t* semaforo = (uint32_t*) ((data_fsm_t*) this->mutex_sem_data)->mutex;
	data_test_t* data = (data_test_t*) ((data_fsm_t*) this->mutex_sem_data)->data;

	data->duty = data->new;
	data->new = 0;

	ytSemaphoreWait(*semaforo, 10000);
	flags &= ~NEW_DATA;
	ytPrintf("\r\nEstablecido en %d\r\n\r\nIntroduce tiempo de muestreo (ms): ",data->duty);
	ytSemaphoreRelease(*semaforo);
}

static void setSamp(fsm_t* this){
	uint32_t* semaforo = (uint32_t*) ((data_fsm_t*) this->mutex_sem_data)->mutex;
	data_test_t* data = (data_test_t*) ((data_fsm_t*) this->mutex_sem_data)->data;

	data->samp = data->new;
	data->new = 0;

	ytSemaphoreWait(*semaforo, 10000);
	flags &= ~NEW_DATA;
	ytPrintf("\r\nEstablecido en %d\r\n\r\nIntroduce tiempo de duracion (ms): ", data->samp);
	ytSemaphoreRelease(*semaforo);
}

static void setSec(fsm_t* this){
	uint32_t* semaforo = (uint32_t*) ((data_fsm_t*) this->mutex_sem_data)->mutex;
	data_test_t* data = (data_test_t*) ((data_fsm_t*) this->mutex_sem_data)->data;

	data->totalSec = data->new;
	data->new = 0;

	ytSemaphoreWait(*semaforo, 10000);
	flags &= ~NEW_DATA;
	ytPrintf("\r\nEstablecido en %d\r\n\r\nTest con sal (1/0): ",data->totalSec);
	ytSemaphoreRelease(*semaforo);
}

static void startTest(fsm_t* this){
	uint32_t* semaforo = (uint32_t*) ((data_fsm_t*) this->mutex_sem_data)->mutex;
	data_test_t* data = (data_test_t*) ((data_fsm_t*) this->mutex_sem_data)->data;

	ytSemaphoreWait(*semaforo, 10000);
	flags &= ~NEW_DATA;
	flags |= (BUTTON_PRES_1 | BUTTON_PRES_2);
	ytSemaphoreRelease(*semaforo);

	voltajes = xQueueCreate(data->totalSec/data->samp, sizeof(uint16_t));
}

static void start_pwm(fsm_t* this){
	uint32_t* semaforo = (uint32_t*) ((data_fsm_t*) this->mutex_sem_data)->mutex;
	uint16_t* frecuencia = (uint16_t*) ((data_fsm_t*) this->mutex_sem_data)->data;
	uint32_t* pwm_fd = (uint32_t*) ((data_fsm_t*) this->mutex_sem_data)->driver_fd;
	data_test_t* testVal = (data_test_t*) (((data_fsm_t*) this->mutex_sem_data)->test);

	ytSemaphoreWait(*semaforo, 10000);
	flags &= ~BUTTON_PRES_1;

	//ytPrintf("\r\nEncendemos el PWM\r\n");
	ytSemaphoreRelease(*semaforo);

	ytIoctl(*pwm_fd, (uint16_t) PWM_TIM_SEL_PWM, (void*) PWM_TIM4_B9);
	ytIoctl(*pwm_fd, (uint16_t) PWM_TIM_SET_FREQ, (void*) makeFixPoint(((float) testVal->freqH*1000+testVal->freqL)/1000));
	ytIoctl(*pwm_fd, (uint16_t) PWM_TIM_SET_DUTY, (void*) makeFixPoint((float) testVal->duty));
	ytIoctl(*pwm_fd, (uint16_t) PWM_TIM_START, (void*) PWM_TIM4_B9);
}

static void stop_pwm(fsm_t* this){
	uint32_t* semaforo = (uint32_t*) (((data_fsm_t*) this->mutex_sem_data)->mutex);
	uint32_t* pwm_fd = (uint32_t*) (((data_fsm_t*) this->mutex_sem_data)->driver_fd);

	ytSemaphoreWait(*semaforo, 10000);
	flags &= ~BUTTON_PRES_1;

	//ytPrintf("\r\nApagamos el PWM\r\n");
	ytSemaphoreRelease(*semaforo);
	counter_PWM = 0;

	ytIoctl(*pwm_fd, (uint16_t) PWM_TIM_SEL_PWM, (void*) PWM_TIM4_B9);
	ytIoctl(*pwm_fd, (uint16_t) PWM_TIM_STOP_DOWN, (void*) PWM_TIM4_B9);
}

static void init_reader(fsm_t* this){
	uint32_t* semaforo = (uint32_t*) (((data_fsm_t*) this->mutex_sem_data)->mutex);
	uint32_t* tiempo = (uint32_t*) (((data_fsm_t*) this->mutex_sem_data)->data);
	uint32_t* adc_fd = (uint32_t*) (((data_fsm_t*) this->mutex_sem_data)->driver_fd);
	data_test_t* testVal = (data_test_t*) (((data_fsm_t*) this->mutex_sem_data)->test);
	adc_config_t adc_config;
	adc_config.dual_mode_enabled = 0;
	adc_config.channel_speed = 12500;
	adc_config.diff2_mode_enabled = 0;
	adc_config.diff1_mode_enabled = 0;
	adc_config.adc_pin_se1 = GPIO_PIN_A1;

	xQueueReset(voltajes);

	*adc_fd = ytOpen(ADC_DEV, 0);
	ytIoctl(*adc_fd, (uint16_t) CONFIG_ADC, (void*) &adc_config);
	counter_ADC = 0;
	counter_PWM = 0;

	ytSemaphoreWait(*semaforo, 1000);
	flags &= ~(BUTTON_PRES_2 | TIMER_FIN);
	//ytPrintf("\r\nIniciamos ADC\r\n");
	ytSemaphoreRelease(*semaforo);

	ytTimerStart(*tiempo, testVal->samp);
}

static void read_adc(fsm_t* this){
	uint32_t* semaforo = (uint32_t*) (((data_fsm_t*) this->mutex_sem_data)->mutex);
	uint32_t* adc_fd = (uint32_t*) (((data_fsm_t*) this->mutex_sem_data)->driver_fd);
	uint16_t* volt = (uint16_t*) ytMalloc(DATA_TO_READ*sizeof(uint16_t));
	/*adc_config_t adc_config;

	adc_config.dual_mode_enabled = 0;
	adc_config.channel_speed = 12500;
	adc_config.diff2_mode_enabled = 0;
	adc_config.diff1_mode_enabled = 0;
	adc_config.adc_pin_se1 = GPIO_PIN_A1;*/

	ytSemaphoreWait(*semaforo, 1000);
	flags &= ~TIMER_FIN;

	//ytIoctl(*adc_fd, (uint16_t) CONFIG_ADC, (void*) &adc_config);
	ytRead(*adc_fd, (void*) volt, DATA_TO_READ);
	xQueueSend(voltajes, (void*) volt, 1000);
	counter_ADC++;
	counter_PWM++;
	ytSemaphoreRelease(*semaforo);
	ytFree(volt);

	/*sendData++;
	if(sendData > 100){
		ytPrintf("\r\n");
		sendData = 0;
		ytClose(*adc_fd);
	}*/
}

static void stop_reader(fsm_t* this){
	uint32_t* semaforo = (uint32_t*) (((data_fsm_t*) this->mutex_sem_data)->mutex);
	uint32_t* tiempo = (uint32_t*) (((data_fsm_t*) this->mutex_sem_data)->data);
	uint32_t* adc_fd = (uint32_t*) (((data_fsm_t*) this->mutex_sem_data)->driver_fd);

	ytClose(*adc_fd);
	counter_ADC = 0;

	ytSemaphoreWait(*semaforo, 1000);
	flags &= ~(BUTTON_PRES_2 | TIMER_FIN);
	//ytPrintf("\r\nQuitamos ADC\r\n");
	ytSemaphoreRelease(*semaforo);
	ytSemaphoreRelease(imprime);

	ytTimerStop(*tiempo);
}

static void output(fsm_t* this){
	uint16_t value = 0;
	while(xQueueReceive(voltajes, &value, pdMS_TO_TICKS(1000)) != errQUEUE_EMPTY) {
		ytPrintf("%u;", value);

		sendData++;
		/*if(sendData > 30){
			ytPrintf("\r\n");
			sendData = 0;
		}*/
	}
	sendData = 0;
	flags |= OUTPUT_DONE;
}

ytInitProcess(ledControl_test_proc, ledControl_test_func, DEFAULT_PROCESS, 2048, &ledControlTestProcId, NULL); // Realmente es una hebra? (preguntar a Rober)

YT_PROCESS void ledControl_test_func(void const * argument){

	uint16_t control;
	uint32_t adc_fd, pwm_fd, semaforo, tiempo;
	uint16_t* frecuencia;
	data_tiempo_t *dataBoton;
	data_fsm_t *dataPWM, *dataADC, *dataTest;
	data_test_t* testVal;
	time_meas_t* antirrebotes;
	retval_t intento;
	fsm_t *pwm_fsm, *adc_fsm, *test_fsm;
	pwm_fd = ytOpen(PWM_TIM_DEV, 0);
	//adc_fd = ytOpen(ADC_DEV, 0);
	semaforo = ytSemaphoreCreate(1);
	imprime = ytSemaphoreCreate(1);
	antirrebotes = new_time_meas();
	init_time_meas();
	dataBoton = (data_tiempo_t*) ytMalloc(sizeof(data_tiempo_t));
	dataPWM = (data_fsm_t*) ytMalloc(sizeof(data_fsm_t));
	dataADC = (data_fsm_t*) ytMalloc(sizeof(data_fsm_t));
	dataTest = (data_fsm_t*) ytMalloc(sizeof(data_fsm_t));
	testVal = (data_test_t*) ytMalloc(sizeof(data_test_t));
	frecuencia = (uint16_t*) ytMalloc(sizeof(uint16_t));
	pwm_fsm = fsm_new(pwm_tt, (void*) dataPWM);
	adc_fsm = fsm_new(adc_tt, (void*) dataADC);
	test_fsm = fsm_new(test_tt, (void*) dataTest);
	tiempo = ytTimerCreate(ytTimerPeriodic, timer, (void*) semaforo);
	*frecuencia = 10;
	sendData = 0;

	dataBoton->mutex = &semaforo;
	dataBoton->reloj = antirrebotes;
	dataPWM->mutex = &semaforo;
	dataPWM->data = frecuencia;
	dataPWM->driver_fd = &pwm_fd;
	dataPWM->test = testVal;
	dataADC->mutex = &semaforo;
	dataADC->data = &tiempo;
	dataADC->driver_fd = &adc_fd;
	dataADC->test = testVal;
	dataTest->mutex = &semaforo;
	dataTest->data = testVal;

	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_NEW, (void*) PWM_TIM4_B9);
	//ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SET_FREQ, (void*) makeFixPoint(10));
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_SET_DUTY, (void*) makeFixPoint(DUTY_LED));
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_START, (void*) PWM_TIM4_B9);
	ytIoctl(pwm_fd, (uint16_t) PWM_TIM_STOP_DOWN, (void*) PWM_TIM4_B9);

	intento = ytStartProcess("PWM", pwmTask, DEFAULT_PROCESS, PROC_STACK_SIZE, &control, (void *) pwm_fsm);
	ytStartProcess("ADC", adcTask, HIGH_PRIORITY_PROCESS, PROC_STACK_SIZE, &control, (void *) adc_fsm);
	ytStartProcess("Test", testTask, LOW_PRIORITY_PROCESS, PROC_STACK_SIZE, &control, (void *) test_fsm);
	ytStartProcess("Input", input, LOW_PRIORITY_PROCESS, PROC_STACK_SIZE, &control, (void *) testVal);
	//ytStartProcess("Output", printVolt, LOW_PRIORITY_PROCESS, sizeof(uint16_t)*QUEUE_SIZE, &control, (void *) frecuencia);


	ytGpioInitPin(GPIO_PIN_B1, GPIO_PIN_INTERRUPT_RISING, GPIO_PIN_PULLDOWN);
	//ytGpioInitPin(GPIO_PIN_B9, GPIO_PIN_OUTPUT_PUSH_PULL, GPIO_PIN_NO_PULL);
	ytGpioPinSetCallbackInterrupt(GPIO_PIN_B1, switchEnable, (void *) dataBoton);

	ytPrintf("Introduce la frecuencia (1o entero, 2o decimal): ");

	while(1){

//		ytPrintf("Enable PWM: %d\r\n", enable);

		ytDelay(1000);
	}
}
/* *************************************************/

YT_PROCESS void pwmTask(void const * argument){
	fsm_t* pwm_fsm = (fsm_t*) argument;
	TickType_t lastTime;

	while(1){
		lastTime = xTaskGetTickCount();
		fsm_fire(pwm_fsm);
		vTaskDelay(pdMS_TO_TICKS(PERIODO) - (xTaskGetTickCount()-lastTime) % pdMS_TO_TICKS(PERIODO));
	}
}

YT_PROCESS void adcTask(void const * argument){
	fsm_t* adc_fsm = (fsm_t*) argument;
	TickType_t lastTime;

	while(1){
		lastTime = xTaskGetTickCount();
		fsm_fire(adc_fsm);
		vTaskDelay(pdMS_TO_TICKS(PERIODO) - (xTaskGetTickCount()-lastTime) % pdMS_TO_TICKS(PERIODO));
	}
}

YT_PROCESS void testTask(void const * argument){
	fsm_t* test_fsm = (fsm_t*) argument;
	TickType_t lastTime;

	while(1){
		lastTime = xTaskGetTickCount();
		fsm_fire(test_fsm);
		vTaskDelay(pdMS_TO_TICKS(PERIODO) - (xTaskGetTickCount()-lastTime) % pdMS_TO_TICKS(PERIODO));
	}
}

YT_PROCESS void input(void const * argument){
	uint8_t *datos, nNum;
	uint16_t multiplicador;
	int i, j;
	data_test_t* testData = (data_test_t*) argument;
	testData->new = 0;
	multiplicador = 1;
	nNum = 0;
	datos = (uint8_t*) ytMalloc(MAX_BUFFER*sizeof(uint8_t));
	while(1){
		ytStdinRead(datos, (uint8_t) MAX_BUFFER, 250);
		for (i = 0; i < MAX_BUFFER; i++){
			if(datos[i] == '\0' || (datos[i] == '\r' && nNum == 0)){
				datos[i] = '\0';
				break;
			}
			else if (datos[i] == '\r' && nNum != 0){
				for(j = nNum; j < MAX_BUFFER; j++)
					testData->new = testData->new/10;
				flags |= NEW_DATA;
				datos[i] = '\0';
				nNum = 0;
				break;
			}
			else if (((uint8_t) datos[i]-48) >= 10){
				datos[i] = '\0';
				break;
			}
			for(j = nNum; j < MAX_BUFFER-1; j++)
				multiplicador = multiplicador*10;
			testData->new = testData->new + ((uint16_t)((uint8_t) datos[i]-48))*multiplicador;
			datos[i] = '\0';
			nNum++;
			multiplicador = 1;
			if (nNum == (MAX_BUFFER)) {
				/*ytPrintf("\r\nFrecuencia actualizada a %d.%d\r\n", *frecuencia/100, *frecuencia%100);
				ytPrintf("Duty Cycle = %u\r\n\r\n", DUTY_LED);*/
				flags |= NEW_DATA;
				nNum = 0;
			}
		}
		ytDelay(5);
	}
}

/*YT_PROCESS void printVolt(void const* argument){
	uint16_t value = 0;
	while (1) {
		ytSemaphoreWait(imprime, YT_WAIT_FOREVER);
		while(xQueueReceive(voltajes, &value, pdMS_TO_TICKS(1000)) != errQUEUE_EMPTY) {
			ytPrintf("%u;", value);

			sendData++;
			if(sendData > 30){
				ytPrintf("\r\n");
				sendData = 0;
			}
		}
		sendData = 0;
	}
}*/

#endif
#endif
#endif
