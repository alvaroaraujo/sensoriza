/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * tcp_test_process.c
 *
 *  Created on: 15 de ene. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file tcp_test_process.c
 */

#include "system_api.h"
#include "system_net_api.h"

#define USE_TCP_TEST_PROCESS	0

#if USE_TCP_TEST_PROCESS

#define NODE_1	1
//#define NODE_2	2
//#define NODE_3	3
//#define NODE_4	4

#define NET_PORT			100
#define BC_DEST_ADDR		"65535"

uint16_t tcpTestProcId;

static void set_addr_and_routes(void);
/* TEST PROCESS ************************************/
YT_PROCESS void tcp_test_func(void const * argument);


ytInitProcess(tcp_test_proc, tcp_test_func, DEFAULT_PROCESS, 256, &tcpTestProcId, NULL);

#define BUF_SIZE	24
char tx_data[BUF_SIZE];
char rx_data[BUF_SIZE];

uint8_t addr_str_buf[8];
YT_PROCESS void tcp_test_func(void const * argument){
	ytTimeMeas_t time_meas;
	ytNetAddr_t net_addr;
	uint32_t connection_fd;
	set_addr_and_routes();
	sprintf(tx_data, "TEST_PACKET\r\n");

#if NODE_1
	net_addr = ytNewNetAddr("4", 'D');	//Envio al  nodo 4
#endif
#if NODE_4
	uint32_t net_server =  ytRlCreateServer(NET_PORT);
	net_addr = ytNewEmptyNetAddr();	//Donde voy a guardar la direccion origen
#endif
	while(1){

#if NODE_1
		if(!ytRlCheckConnection(connection_fd)){	//Si no estoy conectado me intento conectar

			ytLedsToggle(LEDS_RED1);
			connection_fd = ytRlConnectToServer(net_addr, NET_PORT);

		}

		ytStartTimeMeasure(&time_meas);
		if(ytRlSend(connection_fd,(uint8_t*) tx_data, strlen(tx_data) + 1) == (strlen(tx_data)+1)){
			ytStopTimeMeasure(&time_meas);
			ytPrintf("SEND OK\r\n");
		}
		else{
			ytPrintf("SEND ERROR\r\n");
		}
//		ytDelay(2);
//		ytLedsToggle(LEDS_BLUE);
		ytDelay(40);
		ytLedsToggle(LEDS_BLUE);
#endif

#if NODE_2
		ytDelay(400);
		ytLedsToggle(LEDS_BLUE);
		ytDelay(100);
		ytLedsToggle(LEDS_BLUE);
#endif

#if NODE_3
		ytDelay(400);
		ytLedsToggle(LEDS_BLUE);
		ytDelay(100);
		ytLedsToggle(LEDS_BLUE);
#endif

#if NODE_4
		if(!ytRlCheckConnection(connection_fd)){	//Si no estoy conectado me intento conectar

			ytLedsToggle(LEDS_RED1);
			connection_fd = ytRlServerAcceptConnection(net_server, net_addr);

		}
		if(ytRlRcv(connection_fd, (uint8_t*) rx_data, BUF_SIZE)){
			ytNetAddrToString(net_addr, addr_str_buf, 'D');

			ytPrintf("RCV: %s from %s with level: %.2f\r\n",(uint8_t*) rx_data, addr_str_buf, ytRlGetSignalLevel(connection_fd));
			ytLedsToggle(LEDS_BLUE);
		}


#endif


	}
}

/* *************************************************/


static void set_addr_and_routes(void){
	ytNetAddr_t net_addr;
	ytNetAddr_t next_addr;
#if NODE_1
	/* Set node Addr */
	net_addr = ytNewNetAddr("1", 'D');		//Node adress
	ytNetAddNodeAddr(net_addr);
	ytDeleteNetAddr(net_addr);

	/* Set node Routes */
	net_addr = ytNewNetAddr("4", 'D');		//Dest adress
	next_addr = ytNewNetAddr("2", 'D');		//Next adress
	ytNetAddRoute(net_addr, next_addr, 2);
	ytDeleteNetAddr(net_addr);
	ytDeleteNetAddr(next_addr);

	net_addr = ytNewNetAddr("3", 'D');		//Dest adress
	next_addr = ytNewNetAddr("2", 'D');		//Next adress
	ytNetAddRoute(net_addr, next_addr, 1);
	ytDeleteNetAddr(net_addr);
	ytDeleteNetAddr(next_addr);

	net_addr = new_net_addr("2", 'D');		//Dest adress
	next_addr = new_net_addr("2", 'D');		//Next adress
	ytNetAddRoute(net_addr, next_addr, 0);
	ytDeleteNetAddr(net_addr);
	ytDeleteNetAddr(next_addr);
#endif

#if NODE_2
	/* Set node Addr */
	net_addr = ytNewNetAddr("2", 'D');		//Node adress
	ytNetAddNodeAddr(net_addr);
	ytDeleteNetAddr(net_addr);

	/* Set node Routes */
	net_addr = ytNewNetAddr("4", 'D');		//Dest adress
	next_addr = ytNewNetAddr("3", 'D');		//Next adress
	ytNetAddRoute(net_addr, next_addr, 1);
	ytDeleteNetAddr(net_addr);
	ytDeleteNetAddr(next_addr);

	net_addr = ytNewNetAddr("3", 'D');		//Dest adress
	next_addr = ytNewNetAddr("3", 'D');		//Next adress
	ytNetAddRoute(net_addr, next_addr, 0);
	ytDeleteNetAddr(net_addr);
	ytDeleteNetAddr(next_addr);

	net_addr = ytNewNetAddr("1", 'D');		//Dest adress
	next_addr = ytNewNetAddr("1", 'D');		//Next adress
	ytNetAddRoute(net_addr, next_addr, 0);
	ytDeleteNetAddr(net_addr);
	ytDeleteNetAddr(next_addr);
#endif

#if NODE_3
	/* Set node Addr */
	net_addr = ytNewNetAddr("3", 'D');		//Node adress
	ytNetAddNodeAddr(net_addr);
	ytDeleteNetAddr(net_addr);

	/* Set node Routes */
	net_addr = ytNewNetAddr("4", 'D');		//Dest adress
	next_addr = ytNewNetAddr("4", 'D');		//Next adress
	ytNetAddRoute(net_addr, next_addr, 0);
	ytDeleteNetAddr(net_addr);
	ytDeleteNetAddr(next_addr);

	net_addr = ytNewNetAddr("2", 'D');		//Dest adress
	next_addr = ytNewNetAddr("2", 'D');		//Next adress
	ytNetAddRoute(net_addr, next_addr, 0);
	ytDeleteNetAddr(net_addr);
	ytDeleteNetAddr(next_addr);

	net_addr = ytNewNetAddr("1", 'D');		//Dest adress
	next_addr = ytNewNetAddr("2", 'D');		//Next adress
	ytNetAddRoute(net_addr, next_addr, 1);
	ytDeleteNetAddr(net_addr);
	ytDeleteNetAddr(next_addr);
#endif

#if NODE_4
	/* Set node Addr */
	net_addr = ytNewNetAddr("4", 'D');		//Node adress
	ytNetAddNodeAddr(net_addr);
	ytDeleteNetAddr(net_addr);

	/* Set node Routes */
	net_addr = ytNewNetAddr("1", 'D');		//Dest adress
	next_addr = ytNewNetAddr("3", 'D');		//Next adress
	ytNetAddRoute(net_addr, next_addr, 2);
	ytDeleteNetAddr(net_addr);
	ytDeleteNetAddr(next_addr);

	net_addr = ytNewNetAddr("2", 'D');		//Dest adress
	next_addr = ytNewNetAddr("3", 'D');		//Next adress
	ytNetAddRoute(net_addr, next_addr, 1);
	ytDeleteNetAddr(net_addr);
	ytDeleteNetAddr(next_addr);

	net_addr = ytNewNetAddr("3", 'D');		//Dest adress
	next_addr = ytNewNetAddr("3", 'D');		//Next adress
	ytNetAddRoute(net_addr, next_addr, 0);
	ytDeleteNetAddr(net_addr);
	ytDeleteNetAddr(next_addr);
#endif
}
#endif
