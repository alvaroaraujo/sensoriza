/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * radar_app.c
 *
 *  Created on: 9 de ene. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file radar_app.c
 */

#define USE_RADAR_APP	0

#if USE_RADAR_APP

#include "system_api.h"
#include "radar_proccessing.h"
#include "preprocessing.h"
#include "net_api.h"

#define OUTPUT_S4BIM_INTERFACE	1

#define ENABLE_ALLINONE_OUTPUT	1
#define READABLE_OUTPUT			0
#define RADIO_OUTPUT			1

#define FFT_SAMPLES		128
#define READ_SAMPLES	128

typedef enum send_data_mode_{
	SEND_RAW_DATA,
	SEND_COUNT_DATA,

	NO_SEND,
}send_data_mode_t;

static send_data_mode_t send_data_mode = NO_SEND;

#define DELAY_PASSING_CAR_CONSTANT	42000
static float32_t avg_speed = 40;	//Esperar un tiempo DELAY_PASSING_CAR_CONSTANT/avg_speed cada vez que haya una deteccion para volver a detectar con la misma velocidad.

#define LARGE_VEHICLE_LEVEL	0.4f

#define MAX_NUM_STORED_SPEEDS	128
static float32_t last_speeds[MAX_NUM_STORED_SPEEDS];
static uint16_t stored_num_speeds = 0;

#if SPEED_MODE
#define SPEED_RESOLUTION_FOR_DETECTIONS	20	//km/h

#define MAX_CURRENT_DETECTING_SPEEDS_NUM	8
static float32_t current_detecting_speeds[MAX_CURRENT_DETECTING_SPEEDS_NUM];
static uint64_t start_times[MAX_CURRENT_DETECTING_SPEEDS_NUM];
static uint64_t timeout_times[MAX_CURRENT_DETECTING_SPEEDS_NUM];
static uint8_t long_vehicle_flag[MAX_CURRENT_DETECTING_SPEEDS_NUM];
static uint16_t num_current_detecting_speeds = 0;


//#define SPEED_AVG_CALC_NUM_SAMPLES	8
//static float32_t last_speeds_avg_calc[SPEED_AVG_CALC_NUM_SAMPLES];
//static uint16_t avg_calc_count = 0;

static void speed_detection(preprocessing_block_t* preprocessing_block);
#endif

#if DISTANCE_MODE
#define LANES_NUMBER	2
#define MAX_DISTANCE	8

#define DISTANCE_RESOLUTION_FOR_DETECTIONS	1.5f	//m

#define MAX_CURRENT_DETECTING_DISTANCES_NUM	LANES_NUMBER
static float32_t current_detecting_distances[MAX_CURRENT_DETECTING_DISTANCES_NUM];
static uint64_t start_times[MAX_CURRENT_DETECTING_DISTANCES_NUM];
static uint64_t timeout_times[MAX_CURRENT_DETECTING_DISTANCES_NUM];
static uint16_t num_current_detecting_distances = 0;

static void distance_detection(preprocessing_block_t* preprocessing_block);
#endif

static uint32_t output_data_semaphore_id = 0;
static uint32_t output_done_semaphore_id = 0;
static uint32_t read_adc_samples_semaphore_id = 0;
static uint32_t wait_read_adc_semaphore_id = 0;

static uint16_t radarProcId;
static uint16_t radarAdcProcId;
static uint16_t radarOutProcId;

extern uint8_t just_sent;

static void out_data_s4bim_interface(preprocessing_block_t* preprocessing_block, uint64_t elapsed_time);

/* RADAR PROCESSES ************************************/
YT_PROCESS static void radar_test_process(void const * argument);
YT_PROCESS static void radar_adc_process(void const * argument);
YT_PROCESS static void radar_out_process(void const * argument);

/* This line must be uncommented to launch this process */
ytInitProcess(radar_test, radar_test_process, DEFAULT_PROCESS, 392, &radarProcId, NULL);

YT_PROCESS static void radar_test_process(void const * argument){

	uint64_t t1, t2;

	preprocessing_block_t* preprocessing_block =  new_preprocessing_block(READ_SAMPLES, FFT_SAMPLES, FFT_SAMPLES, 50);

	output_data_semaphore_id = ytSemaphoreCreate(1);
	ytSemaphoreWait(output_data_semaphore_id, YT_WAIT_FOREVER);

	read_adc_samples_semaphore_id = ytSemaphoreCreate(1);
	ytSemaphoreWait(read_adc_samples_semaphore_id, YT_WAIT_FOREVER);

	wait_read_adc_semaphore_id = ytSemaphoreCreate(1);
	ytSemaphoreWait(wait_read_adc_semaphore_id, YT_WAIT_FOREVER);

	output_done_semaphore_id = ytSemaphoreCreate(1);

	ytStartProcess("radar_adc_proc", radar_adc_process, DEFAULT_PROCESS, 392, &radarAdcProcId, (void*) preprocessing_block);

#if ENABLE_ALLINONE_OUTPUT
	ytStartProcess("radar_out_proc", radar_out_process, DEFAULT_PROCESS, 392, &radarOutProcId, (void*) preprocessing_block);

#endif

//	osSignalSet(read_adc_radar_thread_id, READY_TO_ACQUIRE_SIGNAL);

	ytSemaphoreRelease(read_adc_samples_semaphore_id);


	while(1){

		//Espero a que el ADC haya leido datos
		ytSemaphoreWait(wait_read_adc_semaphore_id, YT_WAIT_FOREVER);

		//Proceso la se�al. Me dara un conjunto de velocidades a la salida
		if(!just_sent){
			radar_processData(preprocessing_block);
		}
		just_sent = 0;

#if ENABLE_ALLINONE_OUTPUT
		//Espero a que los anteriores datos hayan sido enviados
		ytSemaphoreWait(output_done_semaphore_id, YT_WAIT_FOREVER);
#endif
		//Miro las velocidades que me han salido y marco la detecci�n
#if SPEED_MODE
		speed_detection(preprocessing_block);
#endif
#if DISTANCE_MODE
		distance_detection(preprocessing_block);
#endif

#if OUTPUT_S4BIM_INTERFACE
		out_data_s4bim_interface(preprocessing_block, (t2)*1000);
#endif

		// Envio los datos procesados
#if ENABLE_ALLINONE_OUTPUT
		send_data_mode = SEND_COUNT_DATA;
		ytSemaphoreRelease(output_data_semaphore_id);
#endif

		t2 = ytTimeGetTimestamp();	//Calculo el tiempo de ON
		t2 = t2 -t1;
		t1 = ytTimeGetTimestamp();

		//Digo que estoy listo para recibir adquirir nuevos datos
		ytSemaphoreRelease(read_adc_samples_semaphore_id);

	}

}



YT_PROCESS static void radar_adc_process(void const * argument){
	static uint32_t read_data[READ_SAMPLES];
	adc_config_t* adc_config;
	uint32_t adc_fd;
	preprocessing_block_t* preprocessing_block = (preprocessing_block_t*) argument;
//	osEvent ret;
//
//	HAL_TIM_Base_Start_IT(&htim6);		//START DAC TIMER6
//
//	radar_adc_semaphore_id = osSemaphoreCreate(osSemaphore(radar_adc_semaphore), 1);
//	osSemaphoreWait(radar_adc_semaphore_id, osWaitForever);
//
//	dac_semaphore_id = osSemaphoreCreate(osSemaphore(dac_semaphore), 1);

//	prepare_dac_signal();

	ytGpioInitPin(GPIO_PIN_B8, GPIO_PIN_OUTPUT_PUSH_PULL, GPIO_PIN_PULLUP);
	ytGpioPinSet(GPIO_PIN_B8);			//Activo el pin que enciende los reguladores del radar

	osDelay(1000);						//y espero un tiempo para que este todo encendido

	/*Config ADC*/
	adc_config = (adc_config_t*) ytMalloc(sizeof(adc_config_t));
	adc_config->channel_speed = 12500;
	adc_config->dual_mode_enabled = 1;
	adc_config->diff2_mode_enabled = 1;
	adc_config->diff1_mode_enabled = 1;
	adc_config->adc_pin_se1 = GPIO_PIN_A0;	//Channel 1
	adc_config->adc_pin_diff1 = GPIO_PIN_A1;

	adc_config->adc_pin_se2 = GPIO_PIN_A6;  //Channel 2
	adc_config->adc_pin_diff2 = GPIO_PIN_A7;

	adc_fd = ytOpen(ADC_DEV, 0);
	ytIoctl(adc_fd, (uint16_t) CONFIG_ADC, (void*) adc_config);

	ytFree(adc_config);

	while(1){

//		osSemaphoreWait(dac_semaphore_id, 20);
//		os_periph_dma_active = 1;

//		__HAL_TIM_SetCounter(&htim6,0);								//Por algun motivo tengo que poner el counter a 0 para que funcione bien
//		HAL_DAC_Start_DMA(&hdac1, DAC_CHANNEL_2, (uint32_t *) &dac_signal[0], TOTAL_DAC_SAMPLES, DAC_ALIGN_12B_R);	//Genero la se�al como tal
//
//		osDelay(2);

		ytDelay(5);
		ytRead(adc_fd, read_data, READ_SAMPLES);
//		HAL_ADCEx_MultiModeStart_DMA(&hadc1, read_data, READ_SAMPLES);		//Leo con el ADC en modo simultaneo y espero hasta que se termina
//		osSemaphoreWait(radar_adc_semaphore_id, ADC_SEMPH_TIMEOUT);
//		os_periph_dma_active = 0;

//		if(adc_ok){		//Se ha leido correctamente del adc
//			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET);		//Desactivo los reguladores del radar para ahorrar energia
//		adc_ok = 0;

		//Espero a que la anterior se�al haya sido procesada
		ytSemaphoreWait(read_adc_samples_semaphore_id, YT_WAIT_FOREVER);

#if ENABLE_ALLINONE_OUTPUT
		ytSemaphoreWait(output_done_semaphore_id, YT_WAIT_FOREVER);
#endif
		//Preparo la se�al en el orden adecuado para el campo iq signal
		prepare_signal(preprocessing_block->adc_iq_signal, read_data, READ_SAMPLES);
		ytSemaphoreRelease(wait_read_adc_semaphore_id);

#if ENABLE_ALLINONE_OUTPUT
		//Puedo enviar los datos RAW.
		send_data_mode = SEND_RAW_DATA;
		ytSemaphoreRelease(output_data_semaphore_id);
#endif
//		}


	}
}


YT_PROCESS static void radar_out_process(void const * argument){

	preprocessing_block_t* preprocessing_block = (preprocessing_block_t*) argument;
	uint16_t i;

	while(1){
		ytSemaphoreWait(output_data_semaphore_id, YT_WAIT_FOREVER);

		if(send_data_mode == SEND_RAW_DATA){
#if READABLE_OUTPUT
#elif RADIO_OUTPUT
#else
			send_data_mode = NO_SEND;
			uint16_t frame_init = 0xA5BF;				//En principio algo acabado en F no es un valor posible para el ADC
			ytStdoutSend((uint8_t*)&frame_init, 2);

			uint16_t sample_num = READ_SAMPLES;
			ytStdoutSend((uint8_t*)&sample_num, 2);

			ytStdoutSend((uint8_t*)preprocessing_block->adc_iq_signal->i_signal, sample_num*2*sizeof(uint16_t));
//			UsbWriteString("RAW_DATA\r\n");
#endif

		}
		else if(send_data_mode == SEND_COUNT_DATA){
			send_data_mode = NO_SEND;

			if(stored_num_speeds > 0){

#if READABLE_OUTPUT
#if SPEED_MODE
				ytPrintf("NUM DET: %d\r\n", stored_num_speeds);
				for(i=0; i< stored_num_speeds; i++){
					ytPrintf("SPEED %d: %.2f\r\n", stored_num_speeds, last_speeds[i]);
				}
#endif
#if DISTANCE_MODE
				uint16_t lane;
				ytPrintf("NUM DET: %d\r\n", stored_num_speeds);
				for(i=0; i< stored_num_speeds; i++){
					if(last_speeds[i]<4){
						lane = 1;
					}
					else{
						lane = 2;
					}
					ytPrintf("Distance %d: %.2f m  Lane: %d  \r\n", stored_num_speeds, last_speeds[i], lane);
				}
#endif
#elif RADIO_OUTPUT

#define NET_PORT	105

				if(stored_num_speeds <= 8){

					uint8_t* out_msg = ytMalloc((stored_num_speeds+2)*sizeof(float32_t));
					uint32_t* out_code = &out_msg[0];
					uint32_t* out_stored_num = &out_msg[4];
					float32_t* out_speeds = &out_msg[8];

					net_uc_open(NET_PORT);	//Abro el puerto

					(*out_code) = 0x00000001;
					(*out_stored_num) = stored_num_speeds;
					net_addr_t dest_addr = new_net_addr("4", 'D');	//Envio al  nodo 4

					for(i=0; i<stored_num_speeds; i++){
						out_speeds[i] = last_speeds[i];
					}
					net_uc_send(NET_PORT, dest_addr, out_msg, (stored_num_speeds+2)*sizeof(float32_t));

					ytFree(out_msg);
					net_uc_close(NET_PORT);
					delete_net_addr(dest_addr);

				}

#else
				uint16_t frame_init = 0xC3DF;				//En principio algo acabado en F no es un valor posible para el ADC
				ytStdoutSend((uint8_t*)&frame_init, 2);

				ytStdoutSend((uint8_t*)&stored_num_speeds, 2);

				ytStdoutSend((uint8_t*)last_speeds, sizeof(float32_t)*stored_num_speeds);
#endif
				stored_num_speeds = 0;
			}


		}
//		leds_toggle(LEDS_RED1);
		ytSemaphoreRelease(output_done_semaphore_id);

	}

}


#if SPEED_MODE
static void speed_detection(preprocessing_block_t* preprocessing_block){
	uint16_t i, j;

	static uint64_t t2;


	t2 = ytTimeGetTimestamp();

	if(preprocessing_block->speed_selection->current_num_speeds > 0){
		if(num_current_detecting_speeds == 0){			//No se est� detectando ningun coche

			for(i=0; i<preprocessing_block->speed_selection->current_num_speeds; i++){

				if(num_current_detecting_speeds >= MAX_CURRENT_DETECTING_SPEEDS_NUM){	//No pueden detectarse simultaneamente muchas velocidades
					break;
				}
				current_detecting_speeds[num_current_detecting_speeds] = preprocessing_block->speed_selection->speeds[i];
				start_times[num_current_detecting_speeds] = ytTimeGetTimestamp();
				timeout_times[num_current_detecting_speeds] = (uint32_t)(DELAY_PASSING_CAR_CONSTANT/(uint32_t)(preprocessing_block->speed_selection->speeds[i]));
				long_vehicle_flag[num_current_detecting_speeds] = 0;
				if(preprocessing_block->speed_selection->speeds_levels[i] > LARGE_VEHICLE_LEVEL){	//Is a truck or bus
					long_vehicle_flag[num_current_detecting_speeds] = 1;
					timeout_times[num_current_detecting_speeds] *= 2;
				}

				last_speeds[stored_num_speeds] = current_detecting_speeds[num_current_detecting_speeds];
				if(stored_num_speeds >= MAX_NUM_STORED_SPEEDS){	//Overflow de las velocidades detectadas. Alguien deberia leerlas antes
					stored_num_speeds = 0;
				}
				stored_num_speeds++;

				num_current_detecting_speeds++;
			}
		}
		else{		//Compruebo si la velocidad detectada esta cerca de alguna existente, y si es asi no la detecto
			uint8_t detect_this = 1;
			for(i=0; i<preprocessing_block->speed_selection->current_num_speeds; i++){

				if(num_current_detecting_speeds >= MAX_CURRENT_DETECTING_SPEEDS_NUM){	//No pueden detectarse simultaneamente muchas velocidades
					break;
				}
				for(j=0; j<num_current_detecting_speeds; j++){
					if((current_detecting_speeds[j] > (preprocessing_block->speed_selection->speeds[i]-SPEED_RESOLUTION_FOR_DETECTIONS))
							&& (current_detecting_speeds[j] < (preprocessing_block->speed_selection->speeds[i]+SPEED_RESOLUTION_FOR_DETECTIONS))){	//Ya existe esta velocidad, no la a�ado

						if(preprocessing_block->speed_selection->speeds_levels[i] > LARGE_VEHICLE_LEVEL){	//Is a truck or bus
							if(long_vehicle_flag[j] == 0){
								long_vehicle_flag[j] = 1;
								timeout_times[j] *= 2;
							}
						}
						detect_this = 0;
					}
				}
				if(detect_this){
					current_detecting_speeds[num_current_detecting_speeds] = preprocessing_block->speed_selection->speeds[i];
					timeout_times[num_current_detecting_speeds] = (uint32_t)(DELAY_PASSING_CAR_CONSTANT/(uint32_t)(preprocessing_block->speed_selection->speeds[i]));
					start_times[num_current_detecting_speeds] = ytTimeGetTimestamp();
					long_vehicle_flag[num_current_detecting_speeds] = 0;
					if(preprocessing_block->speed_selection->speeds_levels[i] > LARGE_VEHICLE_LEVEL){	//Is a truck or bus
						long_vehicle_flag[num_current_detecting_speeds] = 1;
						timeout_times[num_current_detecting_speeds] *= 2;
					}

					last_speeds[stored_num_speeds] = current_detecting_speeds[num_current_detecting_speeds];
					if(stored_num_speeds >= MAX_NUM_STORED_SPEEDS){	//Overflow de las velocidades detectadas. Alguien deberia leerlas antes
						stored_num_speeds = 0;
					}
					stored_num_speeds++;

					num_current_detecting_speeds++;
				}
			}

		}
	}

	//Evaluo si ha pasado tiempo suficiente para dejar de detectar algun coche
	if(num_current_detecting_speeds != 0){
		for(i=0; i<num_current_detecting_speeds; i++){
			if((t2-start_times[i]) >= timeout_times[i]){	//Hay que dejar de detectar, ya que pas� el timeout. En este momento el dato estara disponible para ser enviado y para calculos de avg


				for(j=i; j<num_current_detecting_speeds-1; j++){	//Desplazo los elementos del array para eliminar una deteccion
					current_detecting_speeds[j] = current_detecting_speeds[j+1];
					start_times[j] = start_times[j+1];
					timeout_times[j] = timeout_times[j+1];
					long_vehicle_flag[j] = long_vehicle_flag [j+1];
				}
				num_current_detecting_speeds--;		//Resto la deteccion

				i--; //Vuelvo a evaluar el ultimo miembro ya que puede haber habido un desplazamiento
			}

		}
	}

}
#endif

#if DISTANCE_MODE
void distance_detection(preprocessing_block_t* preprocessing_block){
	uint16_t i, j;
//	static uint16_t detecting_vehicle = 0;
	static uint64_t t2;
	uint32_t timeout = (uint32_t)(DELAY_PASSING_CAR_CONSTANT/(uint32_t)(avg_speed));

	t2 = ytTimeGetTimestamp();

	if(preprocessing_block->speed_selection->current_num_speeds > 0){
		if(num_current_detecting_distances == 0){			//No se est� detectando ningun coche

			for(i=0; i<preprocessing_block->speed_selection->current_num_speeds; i++){

				if(num_current_detecting_distances >= LANES_NUMBER){	//No pueden detectarse simultaneamente m�s de un objeto por v�a
					break;
				}
				if(preprocessing_block->speed_selection->speeds[i] < MAX_DISTANCE){
					current_detecting_distances[num_current_detecting_distances] = preprocessing_block->speed_selection->speeds[i];
					start_times[num_current_detecting_distances] = ytTimeGetTimestamp();
					timeout_times[num_current_detecting_distances] = timeout;

					last_speeds[stored_num_speeds] = current_detecting_distances[num_current_detecting_distances];
					if(stored_num_speeds >= MAX_NUM_STORED_SPEEDS){	//Overflow de las velocidades detectadas. Alguien deberia leerlas antes
						stored_num_speeds = 0;
					}
					stored_num_speeds++;

					num_current_detecting_distances++;

				}

			}
		}
		else{		//Compruebo si la distancia detectada esta cerca de alguna existente, y si es asi no la detecto
			uint8_t detect_this = 1;
			for(i=0; i<preprocessing_block->speed_selection->current_num_speeds; i++){

				if(num_current_detecting_distances >= MAX_CURRENT_DETECTING_DISTANCES_NUM){	//No pueden detectarse simultaneamente muchas velocidades
					break;
				}
				for(j=0; j<num_current_detecting_distances; j++){
					if((current_detecting_distances[j] > (preprocessing_block->speed_selection->speeds[i]-DISTANCE_RESOLUTION_FOR_DETECTIONS))
							&& (current_detecting_distances[j] < (preprocessing_block->speed_selection->speeds[i]+DISTANCE_RESOLUTION_FOR_DETECTIONS))){	//Ya existe esta distancia, no la a�ado
						detect_this = 0;
					}
				}
				if(detect_this){
					if(preprocessing_block->speed_selection->speeds[i] < MAX_DISTANCE){
						current_detecting_distances[num_current_detecting_distances] = preprocessing_block->speed_selection->speeds[i];
						timeout_times[num_current_detecting_distances] = timeout;
						start_times[num_current_detecting_distances] = ytTimeGetTimestamp();

						last_speeds[stored_num_speeds] = current_detecting_distances[num_current_detecting_distances];
						if(stored_num_speeds >= MAX_NUM_STORED_SPEEDS){	//Overflow de las velocidades detectadas. Alguien deberia leerlas antes
							stored_num_speeds = 0;
						}
						stored_num_speeds++;

						num_current_detecting_distances++;
					}
				}
			}

		}
	}

	//Evaluo si ha pasado tiempo suficiente para dejar de detectar algun coche
	if(num_current_detecting_distances != 0){
		for(i=0; i<num_current_detecting_distances; i++){
			if((t2-start_times[i]) >= timeout_times[i]){	//Hay que dejar de detectar, ya que pas� el timeout. En este momento el dato estara disponible para ser enviado y para calculos de avg

//				last_speeds[stored_num_speeds] = current_detecting_distances[i];
//				if(stored_num_speeds >= MAX_NUM_STORED_SPEEDS){	//Overflow de las velocidades detectadas. Alguien deberia leerlas antes
//					stored_num_speeds = 0;
//				}
//				stored_num_speeds++;

				for(j=i; j<num_current_detecting_distances-1; j++){	//Desplazo los elementos del array para eliminar una deteccion
					current_detecting_distances[j] = current_detecting_distances[j+1];
					start_times[j] = start_times[j+1];
					timeout_times[j] = timeout_times[j+1];
				}
				num_current_detecting_distances--;		//Resto la deteccion

				i--; //Vuelvo a evaluar el ultimo miembro ya que puede haber habido un desplazamiento
			}

		}
	}
}
#endif


static void out_data_s4bim_interface(preprocessing_block_t* preprocessing_block, uint64_t elapsed_time){
	uint16_t i;
//	osDelay(150);
		ytPrintf("$\r\n");

		ytPrintf("%d\r\n",(int) preprocessing_block->peaks_selection->current_num_peaks);

		for(i=0; i<preprocessing_block->peaks_selection->current_num_peaks;i++){									//SEND PEAK POSITIONS
			ytPrintf("%d\r\n",(int) preprocessing_block->peaks_selection->peak_positions[i]);

			ytPrintf("%.2f\r\n", (preprocessing_block->peaks_selection->peak_levels[i]*100));
		}
//		osDelay(150);

		//SEND EFFECTIVE SAMPLE NUMBER
		ytPrintf("%d\r\n", preprocessing_block->freq_valid_samples);

		//SEND NOISE
		ytPrintf("%.2f\r\n", (preprocessing_block->snr->noise_lvl*100));

		for(i=0; i<preprocessing_block->snr->effective_sample_number;i++){				//SEND SNR
			ytPrintf("%.2f\r\n", preprocessing_block->snr->snr_signal[i]);
		}

//		osDelay(150);
		for(i=0; i<preprocessing_block->fft_block->effective_sample_number;i++){				//SEND SIGNAL
			ytPrintf("%.2f\r\n", (preprocessing_block->fft_block->fft_mag_output_ordered_signal[i]*100));
		}

//		osDelay(150);
		for(i=0; i<preprocessing_block->os_cfar->effective_sample_number;i++){				//SEND THREESHOLD
			ytPrintf("%.2f\r\n", (preprocessing_block->os_cfar->signal_threshold_output[i]*100));
		}
//		osDelay(150);

		ytPrintf("%d\r\n", (int) (elapsed_time));

		leds_toggle(LEDS_RED1);
}

//void prepare_dac_signal(void){
//	uint16_t i;
//	for(i=0; i<UPRAMP_SAMPLES; i++){
//		dac_signal[i] = VMIN_0V_NUMBER + (i*UPRAMP_SLOPE);
//	}
//	for(i=UPRAMP_SAMPLES; i<TOTAL_DAC_SAMPLES; i++){
//		dac_signal[i] = VMAX_10V_NUMBER - ((i-UPRAMP_SAMPLES+1)*DOWNRAMP_SLOPE);
//	}
//
//}

#endif
