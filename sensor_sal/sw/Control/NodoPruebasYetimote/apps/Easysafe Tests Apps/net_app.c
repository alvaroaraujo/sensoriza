/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * net_app.c
 *
 *  Created on: 11 de ene. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file net_app.c
 */

#include "system_api.h"
#include "system_net_api.h"

#define USE_NET_APP_PROCESS	0

#if USE_NET_APP_PROCESS

#define NODE_1	1
//#define NODE_2	2
//#define NODE_3	3
//#define NODE_4	4

#define NET_PORT			105
#define BC_DEST_ADDR		"65535"

#define TEST_MODE_INTERSECTION	1
#define TEST_MODE_DIRECTION		2
#define TEST_MODE_ANIMAL		3

#define NODE_1_DET_MASK				0x01
#define NODE_2_DET_MASK				0x02
#define NODE_1_ADXL_DETECTING_MASK	0x04

#define INTERSECTION_DETECTION_TIMEOUT	4000
#define DIRECTION_DETECTION_TIMEOUT		500
#define ANIMAL_DETECTION_TIMEOUT		4000

uint16_t netAppProcId;

void node1_timeout_func_cb(void const * argument);
void node2_timeout_func_cb(void const * argument);

void button_func_cb(void const * argument);

static void set_addr_and_routes(void);
/* TEST PROCESS ************************************/
YT_PROCESS void net_app_func(void const * argument);


ytInitProcess(net_app_process, net_app_func, DEFAULT_PROCESS, 256, &netAppProcId, NULL);

#define BUF_SIZE	44
uint8_t tx_data[BUF_SIZE];
uint8_t rx_data[BUF_SIZE];

uint8_t addr_str_buf[8];

YT_PROCESS void net_app_func(void const * argument){

	uint16_t node_detecting = 0;
	uint16_t test_mode = TEST_MODE_INTERSECTION;
	uint32_t node1_timeout_timer = ytTimerCreate(ytTimerOnce, node1_timeout_func_cb, &node_detecting);
	uint32_t node2_timeout_timer = ytTimerCreate(ytTimerOnce, node2_timeout_func_cb, &node_detecting);
	uint16_t detect_send = 0x01AA;
	ytGpioInitPin(BUTTON_PIN, GPIO_PIN_INTERRUPT_FALLING, GPIO_PIN_PULLDOWN);
	ytGpioPinSetCallbackInterrupt(BUTTON_PIN, button_func_cb, &test_mode);
	ytNetAddr_t net_addr;
	set_addr_and_routes();
//	sprintf(tx_data, "TEST_PACKET\r\n");

	while(1){

#if NODE_1
		ytDelay(450);
		ytLedsToggle(LEDS_BLUE);
		ytDelay(100);
		ytLedsToggle(LEDS_BLUE);

//		net_uc_open(NET_PORT);	//Abro el puerto
//
//		net_addr_t dest_addr = new_net_addr("4", 'D');	//Envio al  nodo 4
//		net_uc_send(NET_PORT, dest_addr, "HOLA!", strlen("HOLA!"));
//
//		net_uc_close(NET_PORT);
//		delete_net_addr(dest_addr);

#endif

#if NODE_2
		ytDelay(400);
		ytLedsToggle(LEDS_BLUE);
		ytDelay(100);
		ytLedsToggle(LEDS_BLUE);

//		net_uc_open(NET_PORT);	//Abro el puerto
//
//		net_addr_t dest_addr = new_net_addr("4", 'D');	//Envio al  nodo 4
//		net_uc_send(NET_PORT, dest_addr, "HOLA!", strlen("HOLA!"));
//
//		net_uc_close(NET_PORT);
//		delete_net_addr(dest_addr);
#endif

#if NODE_3
		ytDelay(400);
		ytLedsToggle(LEDS_BLUE);
		ytDelay(100);
		ytLedsToggle(LEDS_BLUE);
#endif

#if NODE_4
		net_uc_open(NET_PORT);	//Abro el puerto
		net_addr = new_net_addr("22222", 'D');	//Creo una direcci�n con cualquier valor donde se guardara la direccion recibida
		net_uc_rcv(NET_PORT, net_addr, rx_data, BUF_SIZE);
		ytNetAddrToString(net_addr, addr_str_buf, 'D');
		uint16_t i;
		uint32_t* rx_code = (uint32_t*) &rx_data[0];
//		ytPrintf("RCV: %s from %s\r\n", rx_data, addr_str_buf);
		ytLedsToggle(LEDS_BLUE);

		net_uc_close(NET_PORT);
		delete_net_addr(net_addr);

		switch(test_mode){
		case TEST_MODE_INTERSECTION:

			if((*rx_code) == 0x00000002){
				if(strcmp(addr_str_buf, "1") == 0){
					if((node_detecting & NODE_1_DET_MASK) != NODE_1_DET_MASK){
						ytTimerStart(node1_timeout_timer, INTERSECTION_DETECTION_TIMEOUT);
						node_detecting |= NODE_1_DET_MASK;
//						float64_t* acel_val = (float64_t*)&rx_data[4];
//						ytPrintf("ACEL_1: %.4f\r\n", (*acel_val));
						ytStdoutSend(&detect_send, 2);
						ytPrintf("\r\n");
					}
				}
				else if(strcmp(addr_str_buf, "2") == 0){
					if((node_detecting & NODE_2_DET_MASK) != NODE_2_DET_MASK){
						ytTimerStart(node2_timeout_timer, INTERSECTION_DETECTION_TIMEOUT);
						node_detecting |= NODE_2_DET_MASK;
//						float64_t* acel_val = (float64_t*) &rx_data[4];
//						ytPrintf("ACEL_2: %.4f\r\n", (*acel_val));
						ytStdoutSend(&detect_send, 2);
						ytPrintf("\r\n");
					}
				}
			}
			else if((*rx_code) == 0x00000001){
				if(strcmp(addr_str_buf, "1") == 0){
					if((node_detecting & NODE_1_DET_MASK) != NODE_1_DET_MASK){
						ytTimerStart(node1_timeout_timer, INTERSECTION_DETECTION_TIMEOUT);
						node_detecting |= NODE_1_DET_MASK;
//						uint32_t* num_speeds = (uint32_t*) &rx_data[4];
//						float32_t* speed_vals = (float32_t*) &rx_data[8];
//						for(i=0; i<(*num_speeds); i++){
//							ytPrintf("SPEED: %.2f\r\n", speed_vals[i]);
//						}
						ytStdoutSend(&detect_send, 2);
						ytPrintf("\r\n");
					}
				}
			}
			break;

		case TEST_MODE_DIRECTION:
			if(strcmp(rx_data, "ADXL355") == 0){
				if(strcmp(addr_str_buf, "1") == 0){
					if((node_detecting & NODE_1_ADXL_DETECTING_MASK) != NODE_1_ADXL_DETECTING_MASK){
						ytTimerStart(node1_timeout_timer, DIRECTION_DETECTION_TIMEOUT);
						node_detecting |= NODE_1_ADXL_DETECTING_MASK;
					}
				}
				else if(strcmp(addr_str_buf, "2") == 0){
					if((node_detecting & NODE_2_DET_MASK) != NODE_2_DET_MASK){
						if(node_detecting & NODE_1_ADXL_DETECTING_MASK){
							ytTimerStart(node2_timeout_timer, DIRECTION_DETECTION_TIMEOUT);
							node_detecting |= NODE_2_DET_MASK;
							node_detecting |= NODE_1_ADXL_DETECTING_MASK;
							ytStdoutSend(&detect_send, 2);
						}
					}
				}
			}
			else if(strcmp(rx_data, "RADAR") == 0){
				if(strcmp(addr_str_buf, "1") == 0){
					if((node_detecting & NODE_1_DET_MASK) != NODE_1_DET_MASK){
						ytTimerStart(node1_timeout_timer, DIRECTION_DETECTION_TIMEOUT);
						node_detecting |= NODE_1_DET_MASK;
						ytStdoutSend(&detect_send, 2);
					}
				}
			}
			break;

		case TEST_MODE_ANIMAL:
			if(strcmp(rx_data, "ADXL355") == 0){
				if(strcmp(addr_str_buf, "1") == 0){
					if((node_detecting & NODE_1_DET_MASK) != NODE_1_DET_MASK){
						ytTimerStart(node1_timeout_timer, ANIMAL_DETECTION_TIMEOUT);
						node_detecting |= NODE_1_DET_MASK;
						ytStdoutSend(&detect_send, 2);
					}
				}
				else if(strcmp(addr_str_buf, "2") == 0){
					if((node_detecting & NODE_2_DET_MASK) != NODE_2_DET_MASK){
						ytTimerStart(node2_timeout_timer, ANIMAL_DETECTION_TIMEOUT);
						node_detecting |= NODE_2_DET_MASK;
						ytStdoutSend(&detect_send, 2);
					}
				}
			}
			else if(strcmp(rx_data, "RADAR") == 0){
				if(strcmp(addr_str_buf, "1") == 0){
					if((node_detecting & NODE_1_DET_MASK) != NODE_1_DET_MASK){
						ytTimerStart(node1_timeout_timer, ANIMAL_DETECTION_TIMEOUT);
						node_detecting |= NODE_1_DET_MASK;
						ytStdoutSend(&detect_send, 2);
					}
				}
			}
			break;

		default:
			break;
		}

		if((node_detecting & NODE_1_DET_MASK)){

		}

#endif


	}
}
/* *************************************************/


static void set_addr_and_routes(void){
	ytNetAddr_t net_addr;
	ytNetAddr_t next_addr;
#if NODE_1
	/* Set node Addr */
	net_addr = new_net_addr("1", 'D');		//Node adress
	net_add_node_addr(net_addr);
	delete_net_addr(net_addr);

	/* Set node Routes */
	net_addr = new_net_addr("4", 'D');		//Dest adress
	next_addr = new_net_addr("2", 'D');		//Next adress
	net_add_route(net_addr, next_addr, 2);
	delete_net_addr(net_addr);
	delete_net_addr(next_addr);

	net_addr = new_net_addr("3", 'D');		//Dest adress
	next_addr = new_net_addr("2", 'D');		//Next adress
	net_add_route(net_addr, next_addr, 1);
	delete_net_addr(net_addr);
	delete_net_addr(next_addr);

	net_addr = new_net_addr("2", 'D');		//Dest adress
	next_addr = new_net_addr("2", 'D');		//Next adress
	net_add_route(net_addr, next_addr, 0);
	delete_net_addr(net_addr);
	delete_net_addr(next_addr);
#endif

#if NODE_2
	/* Set node Addr */
	net_addr = new_net_addr("2", 'D');		//Node adress
	net_add_node_addr(net_addr);
	delete_net_addr(net_addr);

	/* Set node Routes */
	net_addr = new_net_addr("4", 'D');		//Dest adress
	next_addr = new_net_addr("3", 'D');		//Next adress
	net_add_route(net_addr, next_addr, 1);
	delete_net_addr(net_addr);
	delete_net_addr(next_addr);

	net_addr = new_net_addr("3", 'D');		//Dest adress
	next_addr = new_net_addr("3", 'D');		//Next adress
	net_add_route(net_addr, next_addr, 0);
	delete_net_addr(net_addr);
	delete_net_addr(next_addr);

	net_addr = new_net_addr("1", 'D');		//Dest adress
	next_addr = new_net_addr("1", 'D');		//Next adress
	net_add_route(net_addr, next_addr, 0);
	delete_net_addr(net_addr);
	delete_net_addr(next_addr);
#endif

#if NODE_3
	/* Set node Addr */
	net_addr = new_net_addr("3", 'D');		//Node adress
	net_add_node_addr(net_addr);
	delete_net_addr(net_addr);

	/* Set node Routes */
	net_addr = new_net_addr("4", 'D');		//Dest adress
	next_addr = new_net_addr("4", 'D');		//Next adress
	net_add_route(net_addr, next_addr, 0);
	delete_net_addr(net_addr);
	delete_net_addr(next_addr);

	net_addr = new_net_addr("2", 'D');		//Dest adress
	next_addr = new_net_addr("2", 'D');		//Next adress
	net_add_route(net_addr, next_addr, 0);
	delete_net_addr(net_addr);
	delete_net_addr(next_addr);

	net_addr = new_net_addr("1", 'D');		//Dest adress
	next_addr = new_net_addr("2", 'D');		//Next adress
	net_add_route(net_addr, next_addr, 1);
	delete_net_addr(net_addr);
	delete_net_addr(next_addr);
#endif

#if NODE_4
	/* Set node Addr */
	net_addr = new_net_addr("4", 'D');		//Node adress
	net_add_node_addr(net_addr);
	delete_net_addr(net_addr);

	/* Set node Routes */
	net_addr = new_net_addr("1", 'D');		//Dest adress
	next_addr = new_net_addr("3", 'D');		//Next adress
	net_add_route(net_addr, next_addr, 2);
	delete_net_addr(net_addr);
	delete_net_addr(next_addr);

	net_addr = new_net_addr("2", 'D');		//Dest adress
	next_addr = new_net_addr("3", 'D');		//Next adress
	net_add_route(net_addr, next_addr, 1);
	delete_net_addr(net_addr);
	delete_net_addr(next_addr);

	net_addr = new_net_addr("3", 'D');		//Dest adress
	next_addr = new_net_addr("3", 'D');		//Next adress
	net_add_route(net_addr, next_addr, 0);
	delete_net_addr(net_addr);
	delete_net_addr(next_addr);
#endif
}

void node1_timeout_func_cb(void const * argument){
	uint16_t* node_detecting = (uint16_t*) argument;
	if((*node_detecting) & NODE_1_ADXL_DETECTING_MASK){
		(*node_detecting) &= NODE_1_ADXL_DETECTING_MASK;
	}
	(*node_detecting) &= ~NODE_1_DET_MASK;
}
void node2_timeout_func_cb(void const * argument){
	uint16_t* node_detecting = (uint16_t*) argument;
	(*node_detecting) &= ~NODE_2_DET_MASK;

}

void button_func_cb(void const * argument){
	uint16_t* test_mode = (uint16_t*) argument;
//
//	(*test_mode)++;
//	if((*test_mode) > TEST_MODE_ANIMAL){
//		(*test_mode) = TEST_MODE_INTERSECTION;
//		ytLedsOff(LEDS_RED1);
//		ytLedsOff(LEDS_BLUE);
//	}
//	else if((*test_mode) == TEST_MODE_ANIMAL){
//		ytLedsOn(LEDS_RED1);
//		ytLedsOff(LEDS_BLUE);
//	}
//	else if((*test_mode) == TEST_MODE_DIRECTION){
//		ytLedsOff(LEDS_RED1);
//		ytLedsOn(LEDS_BLUE);
//	}
}
#endif
