/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * test_comm.c
 *
 *  Created on: 10 de ene. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file test_comm.c
 */

#define USE_TEST_COMM	0

#include "system_api.h"
#include "net_api.h"
#include "time_meas.h"

#if USE_TEST_COMM

#define NODE_1	1
//#define NODE_2	2

#define NET_PORT			101
#define BC_DEST_ADDR		"65535"

uint16_t testCommId;

static void set_addr_and_routes(void);
/* TEST PROCESS ************************************/
YT_PROCESS void test_comm(void const * argument);


ytInitProcess(test_comm_process, test_comm, DEFAULT_PROCESS, 256, &testCommId, NULL);

#define BUF_SIZE	24
uint8_t tx_data[BUF_SIZE];
uint8_t rx_data[BUF_SIZE];

uint8_t addr_str_buf[8];

YT_PROCESS void test_comm(void const * argument){
	net_addr_t net_addr;
	set_addr_and_routes();
	sprintf(tx_data, "TEST_PACKET\r\n");

//	time_meas_t* time_meas = new_time_meas();

	while(1){

#if NODE_1
		net_uc_open(NET_PORT);	//Abro el puerto

		net_addr = new_net_addr("2", 'D');	//Envio al  nodo 2
//		start_time_measure(time_meas);
		if(net_uc_send(NET_PORT, net_addr, tx_data, strlen(tx_data) + 1) == (strlen(tx_data)+1)){
			ytPrintf("SEND OK\r\n");
		}
//		stop_time_measure(time_meas);
//		ytPrintf("TIME: %d us\r\n ", time_meas->elapsed_time);
		net_uc_close(NET_PORT);
		delete_net_addr(net_addr);
//		ytDelay(150);
		ytLedsToggle(LEDS_BLUE);
//		ytDelay(30);
//		ytLedsToggle(LEDS_BLUE);
#endif


#if NODE_2
		net_uc_open(NET_PORT);	//Abro el puerto
		net_addr = new_net_addr("22222", 'D');	//Creo una direcci�n con cualquier valor donde se guardara la direccion recibida
		net_uc_rcv(NET_PORT, net_addr, rx_data, BUF_SIZE);
		net_addr_to_string(net_addr, addr_str_buf, 'D');
		ytPrintf("RCV: %s from %s\r\n", rx_data, addr_str_buf);

		net_uc_close(NET_PORT);
		delete_net_addr(net_addr);
		ytLedsToggle(LEDS_BLUE);
//		ytDelay(30);
//		ytLedsToggle(LEDS_BLUE);
#endif


	}
}
/* *************************************************/


static void set_addr_and_routes(void){
	net_addr_t net_addr;
	net_addr_t next_addr;
#if NODE_1
	/* Set node Addr */
	net_addr = new_net_addr("1", 'D');		//Node adress
	net_add_node_addr(net_addr);
	delete_net_addr(net_addr);

	/* Set node Routes */
	net_addr = new_net_addr("2", 'D');		//Dest adress
	next_addr = new_net_addr("2", 'D');		//Next adress
	net_add_route(net_addr, next_addr, 0);
	delete_net_addr(net_addr);
	delete_net_addr(next_addr);
#endif

#if NODE_2
	/* Set node Addr */
	net_addr = new_net_addr("2", 'D');		//Node adress
	net_add_node_addr(net_addr);
	delete_net_addr(net_addr);

	/* Set node Routes */
	net_addr = new_net_addr("1", 'D');		//Dest adress
	next_addr = new_net_addr("1", 'D');		//Next adress
	net_add_route(net_addr, next_addr, 0);
	delete_net_addr(net_addr);
	delete_net_addr(next_addr);
#endif


}

#endif
