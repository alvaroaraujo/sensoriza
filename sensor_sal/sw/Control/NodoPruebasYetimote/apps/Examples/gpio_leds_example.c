/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * gpio_leds_example.c
 *
 *  Created on: 1 de dic. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file gpio_leds_example.c
 */

#define USE_GPIO_EXAMPLE	0

#if USE_GPIO_EXAMPLE

#include "platform-conf.h"
#include "system_api.h"

static uint16_t gpioProcId;

/* TEST PROCESS ************************************/
YT_PROCESS static void gpio_driver_example(void const * argument);

static void gpio_interrupt_callback(void const* argument);

/* This line must be uncommented to launch this process */
ytInitProcess(gpio_example_process, gpio_driver_example, DEFAULT_PROCESS, 192, &gpioProcId, NULL);

YT_PROCESS static void gpio_driver_example(void const * argument){

	ytGpioInitPin(GPIO_PIN_B1, GPIO_PIN_OUTPUT_PUSH_PULL, GPIO_PIN_NO_PULL);

	ytGpioInitPin(GPIO_PIN_B9, GPIO_PIN_INTERRUPT_FALLING, GPIO_PIN_PULLUP);
	ytGpioPinSetCallbackInterrupt(GPIO_PIN_B9, gpio_interrupt_callback, NULL);

	while(1){

		ytGpioPinToggle(GPIO_PIN_B1);

		ytDelay(1000);
	}

	ytGpioDeInitPin(GPIO_PIN_B1);
	ytGpioDeInitPin(GPIO_PIN_B9);
}


static void gpio_interrupt_callback(void const* argument){
	ytLedsToggle(LEDS_BLUE);
}
#endif
