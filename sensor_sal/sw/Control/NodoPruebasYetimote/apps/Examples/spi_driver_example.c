/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * spi_driver_example.c
 *
 *  Created on: 1 de dic. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file spi_driver_example.c
 */


#define USE_SPI_EXAMPLE	0

#if USE_SPI_EXAMPLE

#include "platform-conf.h"
#include "system_api.h"

#define BYTES_TO_WRITE	16
#define SW_CS_GPIO_PIN	GPIO_PIN_A0
#define SPI_SPEED		6000000

#define BUFFER_TEST_VAL	0x2b

static uint16_t spiProcId;

/* TEST PROCESS ************************************/
YT_PROCESS static void spi_driver_example(void const * argument);

/* This line must be uncommented to launch this process */
ytInitProcess(spi_example_process, spi_driver_example, DEFAULT_PROCESS, 192, &spiProcId, NULL);

YT_PROCESS static void spi_driver_example(void const * argument){
	uint8_t* write_buff;
	uint32_t spi_fd;
	ytSpiDriverIoctlCmd_t spi_cmd;

	while(1){
		write_buff = (uint8_t*) ytMalloc(BYTES_TO_WRITE);
		memset(write_buff, BUFFER_TEST_VAL, BYTES_TO_WRITE);

		spi_fd = ytOpen(SPI_DEV_1, 0);

		spi_cmd = ENABLE_SW_CS;
		ytIoctl(spi_fd, (uint16_t) spi_cmd, (void*) SW_CS_GPIO_PIN);

		spi_cmd = SET_SPI_SPEED;
		ytIoctl(spi_fd, (uint16_t) spi_cmd, (void*) SPI_SPEED);

		spi_cmd = SET_SPI_MODE_MASTER;
		ytIoctl(spi_fd, (uint16_t) spi_cmd, NULL);

		ytWrite(spi_fd, write_buff, BYTES_TO_WRITE);

		ytClose(spi_fd);

		ytFree(write_buff);

		ytDelay(1000);
	}

}

#endif
