/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * adxl355_example.c
 *
 *  Created on: 9 de ene. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file adxl355_example.c
 */

#define USE_ADXL355_EXAMPLE	0

#if USE_ADXL355_EXAMPLE

#include "platform-conf.h"
#include "system_api.h"
#if ENABLE_ADXL355_DRIVER

#include "adxl355_driver.h"
#include "arm_math.h"

#define DATA_TO_READ	16
#define ADXL355_THR		0.07f

static uint16_t adxl355ProcId;

/* TEST PROCESS ************************************/
YT_PROCESS static void adxl355_driver_example(void const * argument);

/* This line must be uncommented to launch this process */
ytInitProcess(adxl355_proc, adxl355_driver_example, DEFAULT_PROCESS, 256, &adxl355ProcId, NULL);

YT_PROCESS static void adxl355_driver_example(void const * argument){
	float32_t* read_buff;
	float32_t* x_axis;
	float32_t* y_axis;
	float32_t* z_axis;
	uint32_t adxl355_fd;
	float32_t max_val_x, min_val_x, max_val_y, min_val_y, max_val_z, min_val_z;
//	float32_t x_val, y_val, z_val;
	float32_t sum_pp;
	uint16_t i;

	/*Config Device. Check adxl355_driver.h for config options*/
	adxl355_fd = ytOpen(ADXL355_DEV, 0);
	ytIoctl(adxl355_fd, (uint16_t) ADXL355_SET_SCALE, (void*) ADXL355_FULLSCALE_2);
	ytIoctl(adxl355_fd, (uint16_t) ADXL355_SET_ODR, (void*) ADXL355_ODR_4000Hz);
	ytIoctl(adxl355_fd, (uint16_t) ADXL355_SET_AXIS, (void*) (AXIS_X_ENABLE | AXIS_Y_ENABLE | AXIS_Z_ENABLE));
	ytIoctl(adxl355_fd, (uint16_t) ADXL355_SET_MODE, (void*) ADXL355_NORMAL);
	ytClose(adxl355_fd);

	ytDelay(50);

	while(1){
		read_buff = (float32_t*) ytMalloc(DATA_TO_READ*sizeof(float32_t)*3);
		x_axis = read_buff;
		y_axis = read_buff + DATA_TO_READ;
		z_axis = read_buff+ (DATA_TO_READ*2);

		adxl355_fd = ytOpen(ADXL355_DEV, 0);
		ytRead(adxl355_fd, read_buff, DATA_TO_READ);
		ytClose(adxl355_fd);


		max_val_x = -5000;
		min_val_x = 5000;
		max_val_y = -5000;
		min_val_y = 5000;
		max_val_z = -5000;
		min_val_z = 5000;

		//Calculo los valores pico a pico de cada eje
		for(i=0; i<DATA_TO_READ; i ++){

			if(x_axis[i]< min_val_x){
				min_val_x = x_axis[i];
			}
			if(x_axis[i] > max_val_x){
				max_val_x = x_axis[i];
			}
			if(y_axis[i]< min_val_y){
				min_val_y = y_axis[i];
			}
			if(y_axis[i] > max_val_y){
				max_val_y = y_axis[i];
			}
			if(z_axis[i]< min_val_z){
				min_val_z = z_axis[i];
			}
			if(z_axis[i] > max_val_z){
				max_val_z = z_axis[i];
			}
		}

		//Calculo la suma de los valores pico a pico
		sum_pp = (max_val_x - min_val_x) + (max_val_y - min_val_y) + (max_val_z - min_val_z);

		if(sum_pp > ADXL355_THR){
			ytPrintf("DETECTION\r\n");
		}

		ytFree(read_buff);

		ytDelay(100);
	}

}
#endif
#endif
