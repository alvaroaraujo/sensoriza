/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * adc_driver_example.c
 *
 *  Created on: 1 de dic. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file adc_driver_example.c
 */

#define USE_ADC_EXAMPLE		0

#if USE_ADC_EXAMPLE

#include "platform-conf.h"
#include "system_api.h"

#define DATA_TO_READ	16

static uint16_t adcProcId;

/* TEST PROCESS ************************************/
YT_PROCESS static void adc_driver_example(void const * argument);

/* This line must be uncommented to launch this process */
ytInitProcess(adc_ex_proc, adc_driver_example, DEFAULT_PROCESS, 192, &adcProcId, NULL);

YT_PROCESS static void adc_driver_example(void const * argument){
	uint16_t* read_buff;
	uint32_t adc_fd;
	ytAdcDriverIoctlCmd_t adc_cmd = CONFIG_ADC;
	adc_config_t adc_config;
	adc_config.dual_mode_enabled = 0;	//Dual mode disabled
	adc_config.channel_speed = 12500;	//12500 Samples per second
	adc_config.diff2_mode_enabled = 0;
	adc_config.diff1_mode_enabled = 0;	//Single ended mode por channel 1 (second channel not used as dual mode is disabled)
	adc_config.adc_pin_se1 = GPIO_PIN_A1;	//Select Pin A1 as ADC input

	while(1){
		read_buff = (uint16_t*) ytMalloc(DATA_TO_READ*sizeof(uint16_t));	//Each adc data has 16 bits length

		adc_fd = ytOpen(ADC_DEV, 0);
		ytIoctl(adc_fd, (uint16_t) adc_cmd, (void*) &adc_config);
		ytRead(adc_fd, read_buff, DATA_TO_READ);
		ytClose(adc_fd);

		ytFree(read_buff);

		ytDelay(1000);
	}

}
#endif
