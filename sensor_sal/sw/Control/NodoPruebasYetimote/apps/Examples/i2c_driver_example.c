/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * i2c_driver_example.c
 *
 *  Created on: 1 de dic. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file i2c_driver_example.c
 */

#define USE_I2C_EXAMPLE	0

#if USE_I2C_EXAMPLE

#include "platform-conf.h"
#include "system_api.h"

#define DATA_TO_READ	16
#define I2C_SLAVE_ADDR	0x55

static uint16_t i2cProcId;

/* TEST PROCESS ************************************/
YT_PROCESS static void i2c_driver_example(void const * argument);

/* This line must be uncommented to launch this process */
ytInitProcess(i2c_example_process, i2c_driver_example, DEFAULT_PROCESS, 192, &i2cProcId, NULL);

YT_PROCESS static void i2c_driver_example(void const * argument){
	uint8_t* read_buff;
	uint32_t i2c_fd;
	ytI2cDriverIoctlCmd_t i2c_cmd = SET_SLAVE_ADDR;

	while(1){
		read_buff = (uint8_t*) ytMalloc(DATA_TO_READ);

		i2c_fd = ytOpen(I2C_DEV, 0);
		ytIoctl(i2c_fd, (uint16_t) i2c_cmd, (void*) 0x55);
		ytRead(i2c_fd, read_buff, DATA_TO_READ);
		ytClose(i2c_fd);

		ytFree(read_buff);

		ytDelay(1000);
	}

}

#endif
