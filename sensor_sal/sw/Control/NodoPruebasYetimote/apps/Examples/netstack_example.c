/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * netstack_example.c
 *
 *  Created on: 1 de dic. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file netstack_example.c
 */

#define USE_NETSTACK_EXAMPLE	0

#if USE_NETSTACK_EXAMPLE

#include "platform-conf.h"
#include "system_api.h"
#include "net_api.h"

#define DATA_BYTES			16

#define NET_PORT			100
#define DEST_ADDR			"22222"

#define TEST_SEND_BYTE		0xBB
#define NET_ADDR_STR_SIZE	8

static uint16_t netstackProcId;

/* TEST PROCESS ************************************/
YT_PROCESS static void netstack_example(void const * argument);

/* This line must be uncommented to launch this process */
ytInitProcess(netstack_example_process, netstack_example, DEFAULT_PROCESS, 192, &netstackProcId, NULL);

YT_PROCESS static void netstack_example(void const * argument){

	uint8_t* read_write_buf;
	char* net_addr_str;
	ytNetAddr_t net_addr;

	while(1){

		read_write_buf = (uint8_t*) ytMalloc(DATA_BYTES);
		memset(read_write_buf, TEST_SEND_BYTE, DATA_BYTES);

		net_addr = ytNewNetAddr(DEST_ADDR, 'D');

		ytUrOpen(NET_PORT);

		if(ytUrSend(NET_PORT, net_addr, read_write_buf, DATA_BYTES) == DATA_BYTES){
			ytPrintf("SEND OK\r\n");
		}

		if(ytUrRcv(NET_PORT, net_addr, read_write_buf, DATA_BYTES) == DATA_BYTES){
			net_addr_str = (char*) ytMalloc(NET_ADDR_STR_SIZE);
			ytNetAddrToString(net_addr, net_addr_str, 'D');
			ytPrintf("Data Received from %s: ", net_addr_str);
			ytStdoutSend(read_write_buf, DATA_BYTES);
			ytPrintf("\r\n");
			ytFree(net_addr_str);
		}

		ytUrClose(NET_PORT);

		ytDeleteNetAddr(net_addr);

		ytFree(read_write_buf);

		ytDelay(1000);
	}

}
#endif
