/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * processes_example.c
 *
 *  Created on: 1 de dic. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file processes_example.c
 */

#define USE_PROCESSES_EXAMPLE	0

#if USE_PROCESSES_EXAMPLE

#include "platform-conf.h"
#include "system_api.h"


#define PROC_STACK_SIZE	192

static uint16_t processesProcId;

/* TEST PROCESS ************************************/
YT_PROCESS static void processes_example(void const * argument);

YT_PROCESS static void proc1_example(void const * argument);
YT_PROCESS static void proc2_example(void const * argument);

/* This line must be uncommented to launch this process */
ytInitProcess(processes_example_process, processes_example, DEFAULT_PROCESS, 192, &processesProcId, NULL);

/*Ejemplo tonto en el que se inician dos procesos que est�n sincronizados con un semaforo y a los 15 segundos se borran*/
YT_PROCESS static void processes_example(void const * argument){

	uint16_t proc1_id, proc2_id;
	uint32_t semph_id = ytSemaphoreCreate(1);

	ytStartProcess("example_proc_1", proc1_example, DEFAULT_PROCESS, PROC_STACK_SIZE, &proc1_id, (void*) semph_id);
	ytStartProcess("example_proc_2", proc2_example, DEFAULT_PROCESS, PROC_STACK_SIZE, &proc2_id, (void*) semph_id);

	ytDelay(15000);

	ytExitProcess(proc1_id);
	ytExitProcess(proc2_id);

	ytSemaphoreDelete(semph_id);

	while(1){
		ytLedsToggle(LEDS_BLUE);
		ytDelay(1000);
	}

}

YT_PROCESS static void proc1_example(void const * argument){
	uint32_t semph_id = (uint32_t) argument;
	while(1){
		ytSemaphoreWait(semph_id, YT_WAIT_FOREVER);
		ytLedsToggle(LEDS_BLUE);
	}
}

YT_PROCESS static void proc2_example(void const * argument){
	uint32_t semph_id = (uint32_t) argument;
	while(1){
		ytDelay(500);
		ytSemaphoreRelease(semph_id);
		ytLedsToggle(LEDS_GREEN);

	}
}
#endif
