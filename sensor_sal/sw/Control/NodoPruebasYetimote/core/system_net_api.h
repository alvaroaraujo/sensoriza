/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * system_net_api.h
 *
 *  Created on: 24 de ene. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file system_net_api.h
 */
#ifndef APPLICATION_CORE_SYSTEM_NET_API_H_
#define APPLICATION_CORE_SYSTEM_NET_API_H_

#include "net_api.h"

#define API_FUNC	inline __attribute__((always_inline))

/**
 * @brief Callback function type called by async read operations
 */
typedef net_read_cb_func_t ytAsyncReadCbFunc_t;

/**
 * @brief Net addreses object type
 */
typedef net_addr_t ytNetAddr_t;

/* *********************************************************/
/* ****************UDP Like Functions***********************/
/* *********************************************************/
API_FUNC retval_t ytUrOpen(uint16_t port_number);
API_FUNC retval_t ytUrClose(uint16_t port_number);
API_FUNC uint32_t ytUrSend(uint16_t port_number, ytNetAddr_t dest_addr_fd, uint8_t* data, uint32_t size);
API_FUNC uint32_t ytUrRcv(uint16_t port_number, ytNetAddr_t from_addr_fd, uint8_t* data, uint32_t size);
API_FUNC retval_t ytUrRcvAsync(uint16_t port_number, ytNetAddr_t from_addr_fd, uint8_t* data, uint32_t size, ytAsyncReadCbFunc_t callback_func, void* args);
API_FUNC float32_t ytUrGetSignalLevel(uint16_t port_number);
/* *********************************************************/


/* *********************************************************/
/* ****************TCP Like Functions***********************/
/* *********************************************************/
API_FUNC uint32_t ytRlCreateServer(uint16_t port_number);
API_FUNC retval_t ytRlDeleteServer(uint32_t server_fd);

API_FUNC uint32_t ytRlServerAcceptConnection(uint32_t server_fd, ytNetAddr_t from_addr_fd);
API_FUNC uint32_t ytRlConnectToServer(ytNetAddr_t dest_addr_fd, uint16_t port_number);
API_FUNC retval_t ytRlCloseConnection(uint32_t connection_fd);

API_FUNC uint32_t ytRlSend(uint32_t connection_fd, uint8_t* data, uint32_t size);
API_FUNC uint32_t ytRlRcv(uint32_t connection_fd, uint8_t* data, uint32_t size);
API_FUNC retval_t ytRlRcvAsync(uint32_t connection_fd, uint8_t* data, uint32_t size, ytAsyncReadCbFunc_t callback_func, void* args);

API_FUNC uint16_t ytRlCheckConnection(uint32_t connection_fd);
API_FUNC float32_t ytRlGetSignalLevel(uint32_t connection_fd);
API_FUNC retval_t ytRlSetConnectionTimeout(uint32_t connection_fd, uint32_t timeout);
/* *********************************************************/


/* *********************************************************/
/* ************Network Addresses Functions******************/
/* *********************************************************/
API_FUNC ytNetAddr_t ytNewNetAddr(char* net_str_val, char format);
API_FUNC ytNetAddr_t ytNewEmptyNetAddr(void);
API_FUNC retval_t ytDeleteNetAddr(ytNetAddr_t net_addr);

API_FUNC retval_t ytNetAddrToString(ytNetAddr_t net_addr, char* net_addr_str_val, char format);
API_FUNC retval_t ytNetAddrCpy(ytNetAddr_t dest_addr, ytNetAddr_t from_addr);
API_FUNC uint16_t ytNetAddrCmp(ytNetAddr_t a_addr, ytNetAddr_t b_addr);

API_FUNC ytNetAddr_t ytGetNodeAddr(uint16_t index);
API_FUNC retval_t ytNetAddNodeAddr(ytNetAddr_t node_addr);
API_FUNC retval_t ytNetRemoveNodeAddr(ytNetAddr_t node_addr);

API_FUNC retval_t ytNetAddRoute(ytNetAddr_t dest_addr, net_addr_t next_addr, uint16_t hops);
API_FUNC retval_t ytNetRemoveRoute(ytNetAddr_t dest_addr, net_addr_t next_addr, uint16_t hops);
/* *********************************************************/

#include "system_net_api_func_def.h"

#endif /* APPLICATION_CORE_SYSTEM_NET_API_H_ */
