/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * system_api_func_def.h
 *
 *  Created on: 2 de nov. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file system_api_func_def.h
 */
#ifndef APPLICATION_CORE_SYSTEM_API_FUNC_DEF_H_
#define APPLICATION_CORE_SYSTEM_API_FUNC_DEF_H_

/* *********************************************************/
/* *********Inline functions definition region *************/
/* *********************************************************/
/**
 * @brief					FUNCTION to create and start a new process on runtime. The process will be launched after calling this function
 * @param name				Process name. For memory saving avoid large names (<16 bytes).
 * @param func				Function to be launched when the process starts
 * @param process_class		Class of the process to be created
 * @param stacksize			Amount of stack to reserve for this process
 * @param processUniqueId	Returned value of the process Id created. Use a pointer to NULL if is not desired to return the id
 * @param arg				Argument to the process function
 * @return					Return status
 */
API_FUNC retval_t ytStartProcess(char* name, ytProcessFunc_t func, ytProcessClass_t process_class, uint32_t stacksize,
		uint16_t* processUniqueId, void* arg){
	return startProcess(name, func, process_class, stacksize,processUniqueId, arg);
}

/**
 * @brief					Start a default process
 * @param name				(char*) Process name. For memory saving avoid large names.
 * @param func				(process_func_t) Function to be launched when the process starts. (process_func_t) => void (*)(const void* args)
 * @param processUniqueId	Returns the new process id generated. Use a pointer to NULL if is not desired to return the id
 * @return					Return status
 */
API_FUNC retval_t ytStartDefaultProcess(char* name, ytProcessFunc_t func, uint16_t* processUniqueId){
	return startDefaultProcess(name, func, processUniqueId);
}

/**
 * @brief			Exit the selected process.
 * @param processId	Process to be exited
 * @return			Return status
 */
API_FUNC retval_t ytExitProcess(uint16_t processId){
	return exitProcess(processId);
}
/**
 * @brief				Suspends the selected running process
 * @param process_id	Process id to be suspended
 * @return				Return status
 */
API_FUNC retval_t ytProcessSuspend(uint16_t processId){
	return processSuspend(processId);
}
/**
 * @brief				Resumes the selected suspended process
 * @param process_id	Process id to be resumed
 * @return				Return status
 */
API_FUNC retval_t ytProcessResume(uint16_t processId){
	return processResume(processId);
}
/**
 * @brief	Returns the current running process id
 * @return	Process id value
 */
API_FUNC uint16_t ytGetProcessId(void){
	return getProcessId();
}
/**
 * @brief	Returns the parent process id of the current running process
 * @return	Parent process id
 */
API_FUNC uint16_t ytGetCurrentParentProcessId(void){
	return getCurrentParentProcessId();
}
/**
 * @brief			Returns the parent process id of the selected process
 * @param processId	Process id
 * @return			Parent process id
 */
API_FUNC uint16_t ytGetParentProcessId(uint16_t processId){
	return getParentProcessId(processId);
}
/**
 * @brief	Prints in stdio readable information about current running processses
 * @return	Return Status
 */
API_FUNC retval_t ytPrintProcesses(void){
	return printProcesses();
}
/**
 * @brief		Allocate memory.
 * @note		This function is recommended for general use to allocate memory from user processes. The allocated memory is linked to the process where it is allocated
 * @note		The execution time of this function is variable so it is not recommended to be used in real-time applications. For real time apps use ytRtMalloc()
 * @param size	Size of memory to be allocated in bytes
 */
API_FUNC void* ytMalloc(size_t size){
	return y_Malloc(size);
}
/**
 * @brief		Frees a previously allocated memory pointer
 * @note		This function is recommended for general use to free memory from user processes.
 * @note 		The execution time of this function is variable so it is not recommended to be used in real-time applications. For real time apps use ytRtFree()
 * @param ptr	Memory pointer to be freed
 */
API_FUNC void ytFree(void* ptr){
	y_Free(ptr);
}

/**
 * @brief				Opens an existing device and returns a file descriptor
 * @param device_name	Device char string to be opened
 * @param flags			Device flags to be opened
 * @return				Returns opened file descriptor
 */
API_FUNC uint32_t ytOpen(const char* device_name, uint32_t flags){
	return y_open(device_name, flags);
}
/**
 * @brief			Close an opened device file descriptor
 * @param fildes	File descriptor of the device to be closed
 * @return			Return status
 */
API_FUNC retval_t ytClose(uint32_t fildes){
	return y_close(fildes);
}
/**
 * @brief			Read bytes from an opened device
 * @param fildes	File descriptor of the device to be read
 * @param buf		Pointer to store the read data
 * @param bytes		Number of bytes to be read
 * @return			Return number of bytes read
 */
API_FUNC size_t ytRead(uint32_t fildes, void* buf, size_t bytes){
	return y_read(fildes, buf, bytes);
}
/**
 * @brief			Write bytes to an opened device
 * @param fildes	File descriptor of the device to be written
 * @param buf		Pointer to the data to be written
 * @param bytes		Number of bytes to be written
 * @return			Return number of bytes written
 */
API_FUNC size_t ytWrite(uint32_t fildes, void* buf, size_t bytes){
	return y_write(fildes, buf, bytes);
}
/**
 * @brief			Performs IO control operations on an opened device. The available operations are customized for each device driver
 * @param fildes	File descriptor of the device to be operated
 * @param command	Command to be executed on the device
 * @param args		Argument used by the IO control operation
 * @return			Return status
 */
API_FUNC retval_t ytIoctl(uint32_t fildes, uint16_t command, void* args){
	return y_ioctl(fildes, command, args);
}
/**
 * @brief	Prints currently loaded devices
 * @return	Return status
 */
API_FUNC retval_t ytPrintDevices(void){
	return printDevices();
}
/**
 * @brief				Creates a new timer instance
 * @param timer_type	Timer Type. Could be ytTimerOnce or ytTimerPreiodic
 * @param timer_func	Timer Callback function. This function is called when the timer time has lapsed
 * @param argument		Argument passed to the timer callback function
 * @return				Returns the new created timer id. 0 if error.
 */
API_FUNC uint32_t ytTimerCreate(ytTimerType_t timer_type, ytProcessFunc_t timer_func, void *argument){
	return y_timerCreate((os_timer_type)timer_type, timer_func, argument);
}
/**
 * @brief			Starts a previously created timer
 * @param timer_id	Timer id instance to be started
 * @param millisec	Time to wait in milliseconds
 * @return			Return status
 */
API_FUNC retval_t ytTimerStart(uint32_t timer_id, uint32_t millisec){
	return y_timerStart(timer_id, millisec);
}
/**
 * @brief			Stops a previously created and started timer
 * @param timer_id	Timer id instance to be stoped
 * @return			Return status
 */
API_FUNC retval_t ytTimerStop(uint32_t timer_id){
	return y_timerStop(timer_id);
}
/**
 * @brief			Delete a previously created timer
 * @param timer_id	Timer id instance to be deleted
 * @return			Return status
 */
API_FUNC retval_t ytTimerDelete(uint32_t timer_id){
	return y_timerDelete(timer_id);
}
/**
 * @brief	Creates a new mutex instance
 * @return	Return the new mutex instance
 */
API_FUNC uint32_t ytMutexCreate(void){
	return y_mutexCreate();
}
/**
 * @brief			Locks the mutex
 * @param mutex_id	Mutex instance to be locked
 * @param millisec	Timeout time to be locked
 * @return			Return status
 */
API_FUNC retval_t ytMutexWait(uint32_t mutex_id, uint32_t millisec){
	return y_mutexWait(mutex_id, millisec);
}
/**
 * @brief			Release a locked mutex
 * @param mutex_id	Mutex instance to be unlocked
 * @return			Return status
 */
API_FUNC retval_t ytMutexRelease(uint32_t mutex_id){
	return y_mutexRelease(mutex_id);
}
/**
 * @brief			Delete a previously created Mutex instance
 * @param mutex_id	Mutex instance to be deleted
 * @return			Return status
 */
API_FUNC retval_t ytMutexDelete(uint32_t mutex_id){
	return y_mutexDelete(mutex_id);
}
/**
 * @brief		Create a new Semaphore instance
 * @param count	Number of counters available by the semaphore
 * @return		Returns new semaphore instance
 */
API_FUNC uint32_t ytSemaphoreCreate(int32_t count){
	return y_semaphoreCreate(count);
}
/**
 * @brief				Catch and wait if necessary for a semaphore token
 * @param semaphore_id	Semaphore instance to catch a token
 * @param millisec		Timeout time to wait
 * @return				Return status
 */
API_FUNC retval_t ytSemaphoreWait(uint32_t semaphore_id, uint32_t millisec){
	return y_semaphoreWait(semaphore_id, millisec);
}
/**
 * @brief				Release a semaphore token
 * @param semaphore_id	Semaphore instance to release a token
 * @return				Return status
 */
API_FUNC retval_t ytSemaphoreRelease(uint32_t semaphore_id){
	return y_semaphoreRelease(semaphore_id);
}
/**
 * @brief				Delete a previously create semaphore instance
 * @param semaphore_id	Semaphore instance to be deleted
 * @return				Return status
 */
API_FUNC retval_t ytSemaphoreDelete(uint32_t semaphore_id){
	return y_semaphoreDelete(semaphore_id);
}
/**
 * @brief			Create a new message queue
 * @note			The message queue is composed by pointers
 * @param q_size	Size of the message queue
 * @return			Return new message queue instance created
 */
API_FUNC uint32_t ytMessageqCreate(uint32_t q_size){
	return y_messageqCreate(q_size);
}
/**
 * @brief				Put a message in a message queue
 * @param messageq_id	Message queue instance where the message is put
 * @param msg			Meessage to be put
 * @param millisec		Timeout time to put
 * @return				Return status
 */
API_FUNC retval_t ytMessageqPut(uint32_t messageq_id, void* msg, uint32_t millisec){
	return y_messageqPut(messageq_id, msg, millisec);
}
/**
 * @brief				Gets a message from a message queue
 * @param messageq_id	Message queue instance where the message is get
 * @param millisec		Timeout time to get
 * @return				Message obtained. NULL if error
 */
API_FUNC void* ytMessageqGet(uint32_t messageq_id, uint32_t millisec){
	return y_messageqGet(messageq_id, millisec);
}
/**
 * @brief				Delete a previously created message queue
 * @param messageq_id	Message queue instance to be deleted
 * @return				Return status
 */
API_FUNC retval_t ytMessageqDelete(uint32_t messageq_id){
	return y_messageqDelete(messageq_id);
}


/**
 * @brief						Creates a new reactor instance
 * @param handle_event_timeout	Timeout to sleep/block in the handle events function
 * @return						Return new reactor instance
 */
API_FUNC ytReactor_t* ytReactorCreate (uint32_t handle_event_timeout){
	return new_reactor(handle_event_timeout);
}

/**
 * @brief			Delete a previously created reactor instance
 * @param reactor	Reactor instance to be deleted
 * @return			Return status
 */
API_FUNC retval_t ytReactorDelete(ytReactor_t* reactor){
	return delete_reactor(reactor);
}

/**
 * @brief			This functions handles events posted by the same or other tasks
 * @note			Use this function in a loop to continuously handle new events posted
 * @param reactor	Reactor instance
 * @return			Return status
 */
API_FUNC retval_t ytReactorHandleEvents(ytReactor_t* reactor){
	return reactor_handle_events(reactor);
}

/**
 * @brief			Post an event to an event handler. When an event is post the reactor will execute the handler function
 * @param reactor	Reactor where the event is posted
 * @param event_id	Event handler to be posted
 * @return			Return status
 */
API_FUNC retval_t ytPostEvent(ytReactor_t* reactor, ytHandleEventFunc_t event_handle_func, void* arg){
	return post_event(reactor, event_handle_func, arg);
}



/**
 * @brief		Switch on a led
 * @param led	Led to be switched on
 * @return		Return status
 */
API_FUNC retval_t ytLedsOn(leds_t led){
	return leds_on(led);
}
/**
 * @brief		Switch off a led
 * @param led	Led to be switched off
 * @return		Return status
 */
API_FUNC retval_t ytLedsOff(leds_t led){
	return leds_off(led);
}
/**
 * @brief		Toggle led
 * @param led	Led to be toggled
 * @return		Return status
 */
API_FUNC retval_t ytLedsToggle(leds_t led){
	return leds_toggle(led);
}

/**
 * @brief			Initializes an available GPIO pin to be used
 * @param gpio		Gpio pin to be initialized
 * @param gpio_mode	Gpio mode
 * @param gpio_pull	Gpio Pull mode
 * @return			Return status
 */
API_FUNC retval_t ytGpioInitPin(gpioPin_t gpio, gpioMode_t gpio_mode, gpioPull_t gpio_pull){
	return y_gpio_init_pin(gpio, gpio_mode, gpio_pull);
}
/**
 * @brief		De init an initialized GPIO pin
 * @param gpio	Gpio pin to be de-initialized
 * @return		Return status
 */
API_FUNC retval_t ytGpioDeInitPin(gpioPin_t gpio){
	return gpio_deinit_pin(gpio);
}
/**
 * @brief		Set gpio pin to high level
 * @param gpio	Gpio pin to be set
 * @return		Return status
 */
API_FUNC retval_t ytGpioPinSet(gpioPin_t gpio){
	return gpio_pin_set(gpio);
}
/**
 * @brief		Reset gpio pin to low value
 * @param gpio	Gpio pin to be reset
 * @return		Return status
 */
API_FUNC retval_t ytGpioPinReset(gpioPin_t gpio){
	return gpio_pin_reset(gpio);
}
/**
 * @brief		Toggle gpio pin
 * @param gpio	Gpio pin to be toggled
 * @return		Return status
 */
API_FUNC retval_t ytGpioPinToggle(gpioPin_t gpio){
	return gpio_pin_toggle(gpio);
}
/**
 * @brief		Get gpio pin value
 * @param gpio	Gpio pin to get value
 * @return		Return gpio pin value. 0xFFFF if error
 */
API_FUNC uint16_t ytGpioPinGet(gpioPin_t gpio){
	return gpio_pin_get(gpio);
}
/**
 * @brief						Set a callback function when the gpio is in Interrupt mode
 * @param gpio					Gpio pin with an interrupt associated
 * @param gpio_interrupt_func	Callback Interrupt function
 * @param args					Arguments to be passed to the callback function
 * @return						Return status
 */
API_FUNC retval_t ytGpioPinSetCallbackInterrupt(gpioPin_t gpio, ytProcessFunc_t gpio_interrupt_func, void* args){
	return gpio_set_callback_interrupt(gpio, gpio_interrupt_func, args);
}

/**
 * @brief		Sends a data stream on the standard output buffer
 * @param data	Data to be sent
 * @param size	Size of the data to be sent in bytes
 * @return		Return status
 */
API_FUNC retval_t ytStdoutSend(uint8_t* data, uint16_t size){
	return stdout_send(data, size);
}

/**
 * @brief			Reads a data stream on the standard input buffer
 * @param data		Data read
 * @param size		Size of the data to be read in bytes
 * @param timeout	Timeout time for read
 * @return			Return status
 */
API_FUNC retval_t ytStdinRead(uint8_t* data, uint16_t size, uint32_t timeout){
	return stdin_read(data, size, timeout);
}
/**
 * @brief			Reads a line on the standard input buffer. New line is detected when a \r or a \n is read
 * @param data		Line read
 * @param max_size	Max size of the line to be read
 * @param timeout	Tiemout time for read
 * @return			Return status
 */
API_FUNC uint16_t ytStdinReadLine(uint8_t* data, uint16_t max_size, uint32_t timeout){
	return stdin_read_line(data, max_size, timeout);
}

/**
 * @brief	Get current system timestamp in milliseconds
 * @return	Current Timestamp
 */
API_FUNC uint64_t ytTimeGetTimestamp(void){
	return rtc_time_get_timestamp();
}
/**
 * @brief				Set the system timestamp to the selected
 * @param new_timestamp	New timestamp to be set
 * @return				Return status
 */
API_FUNC retval_t ytTimeSetTimestamp(uint64_t new_timestamp){
	return rtc_time_set_timestamp(new_timestamp);
}
/**
 * @brief				Get the current system date
 * @param rtc_time_date	System date obtained
 * @return				Return status
 */
API_FUNC retval_t ytTimeGetDate(rtc_time_date_t* rtc_time_date){
	return rtc_time_get_date(rtc_time_date);
}
/**
 * @brief				Set the current system date
 * @param rtc_time_date	System date set
 * @return				Return status
 */
API_FUNC retval_t ytTimeSetDate(rtc_time_date_t* rtc_time_date){
	return rtc_time_set_date(rtc_time_date);
}


/**
 * @brief				Starts measuring times
 * @param time_meas		Time measure struct to store data
 * @return				Return status
 */
API_FUNC retval_t ytStartTimeMeasure(ytTimeMeas_t* time_meas){
	return start_time_measure(time_meas);
}

/**
 * @brief				Stops measuring times
 * @param time_meas		Time measure struct to store data
 * @return				Return status
 */
API_FUNC retval_t ytStopTimeMeasure(ytTimeMeas_t* time_meas){
	return stop_time_measure(time_meas);
}

/* *********************************************/

#endif /* APPLICATION_CORE_SYSTEM_API_FUNC_DEF_H_ */
