/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * stdio_input.c
 *
 *  Created on: 8 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file stdio_input.c
 */
#include "yetimote_stdio.h"
#include "platform-conf.h"
#include "system_api.h"

#if ENABLE_STDIO_FUNCS

#define STDIN_NO_READING	0
#define STDIN_READING_BYTES	1
#define STDIN_READING_LINE	2

enum{
	ECHO_OFF,
	ECHO_ON
};

uint32_t	stdin_mutex_id;

stdio_input_t* stdio_input = NULL;

retval_t get_new_input_data();
retval_t check_newline(void);
retval_t check_add_char(uint8_t new_char, int32_t* prev_new_char_number, uint16_t* command_index, uint16_t* echo_index);

retval_t init_stdio_input();

retval_t init_stdio_input(){

#if (STDIO_INTERFACE == USB_STDIO)
	#if USE_TRACEALYZER_SW == 1
	return RET_ERROR;
#endif
#endif
#if (USE_STOP_MODE == 1)
	return RET_ERROR;
#endif

	stdio_input = ( stdio_input_t*) ytMalloc(sizeof( stdio_input_t));

	stdio_input->rcv_buffer_counter = INPUT_BUFFER_SIZE;
	stdio_input->head = stdio_input->circular_buffer;
	stdio_input->tail = stdio_input->circular_buffer;

	stdio_input->stdin_api_read_buff = NULL;
	stdio_input->stdin_api_current_bytes_read = 0;
	stdio_input->stdin_api_num_bytes_to_read = 0;
	stdio_input->stdin_api_read_mode = STDIN_NO_READING;

#if USE_YETISHELL
	stdio_input->input_command_str_count = 0;
	stdio_input->input_command_str_current_count = 0;
	stdio_input->max_cmd_buffer_size = INPUT_BUFFER_SIZE;
	stdio_input->command = new_command_data();
	stdio_input->echo = ECHO_ON;
#endif

	stdin_mutex_id = ytMutexCreate();
return RET_OK;
}

/**
 *
 * @return
 */
retval_t get_new_input_data(void){
#if (STDIO_INTERFACE == USB_STDIO)
	#if USE_TRACEALYZER_SW == 1
	return RET_ERROR;
#endif
#endif
#if (USE_STOP_MODE == 1)
	return RET_ERROR;
#endif

	stdio_input->head = stdio_input->circular_buffer + INPUT_BUFFER_SIZE - stdio_input->rcv_buffer_counter;

	if(stdio_input->head > stdio_input->tail){
		uint16_t i;
		int32_t new_chars_number = stdio_input->head - stdio_input->tail;

		if(stdio_input->stdin_api_read_mode != STDIN_NO_READING){
			for(i=0; i<new_chars_number; i++){
				if(stdio_input->stdin_api_current_bytes_read < stdio_input->stdin_api_num_bytes_to_read){
					stdio_input->stdin_api_read_buff[stdio_input->stdin_api_current_bytes_read] = stdio_input->tail[i];
					stdio_input->stdin_api_current_bytes_read++;
				}
			}
		}

#if USE_YETISHELL
		uint16_t command_index = 0;
		uint16_t echo_index = 0;
		if((stdio_input->input_command_str_count + new_chars_number) > stdio_input->max_cmd_buffer_size){
			stdio_input->input_command_str_count = 0;
			stdio_input->input_command_str_current_count = 0;
			stdio_input->tail = stdio_input->head;
			return RET_ERROR;	//COMMAND BUFFER OVERFLOW
		}

		for(i=0; i<new_chars_number; i++){

			check_add_char(stdio_input->tail[i], &new_chars_number, &command_index, & echo_index);

			command_index ++;
			if(stdio_input->echo == ECHO_ON){
				echo_index ++;
			}
		}
		if(stdio_input->echo == ECHO_ON){
			stdio_input->echo_output[echo_index] = '\0';
			_printf("%s", (char*)stdio_input->echo_output);
		}

		stdio_input->input_command_str_count = stdio_input->input_command_str_count + new_chars_number;
#endif
		stdio_input->tail = stdio_input->head;
	}

	else if(stdio_input->head < stdio_input->tail){

		uint16_t i;
		uint32_t diff_end = stdio_input->circular_buffer + INPUT_BUFFER_SIZE - stdio_input->tail;
		uint32_t diff_start = stdio_input->head - stdio_input->circular_buffer;
		int32_t new_chars_number = (int32_t) diff_end + diff_start;

		if(stdio_input->stdin_api_read_mode != STDIN_NO_READING){
			for(i=0; i<new_chars_number; i++){
				if(stdio_input->stdin_api_current_bytes_read < stdio_input->stdin_api_num_bytes_to_read){
					stdio_input->stdin_api_read_buff[stdio_input->stdin_api_current_bytes_read] = stdio_input->tail[i];
					stdio_input->stdin_api_current_bytes_read++;
				}
			}
		}

#if USE_YETISHELL
		uint16_t command_index = 0;
		uint16_t echo_index = 0;
		if((stdio_input->input_command_str_count + new_chars_number) > stdio_input->max_cmd_buffer_size){
			stdio_input->input_command_str_count = 0;
			stdio_input->input_command_str_current_count = 0;
			stdio_input->tail = stdio_input->head;
			return RET_ERROR;	//COMMAND BUFFER OVERFLOW
		}

		for(i=0; i<new_chars_number; i++){
			if( i < diff_end){
				check_add_char(stdio_input->tail[i], &new_chars_number, &command_index, &echo_index);
			}
			else{
				check_add_char(stdio_input->circular_buffer[i-diff_end], &new_chars_number, &command_index, &echo_index);
			}
			command_index ++;
			if(stdio_input->echo == ECHO_ON){
				echo_index ++;
			}
		}

		if(stdio_input->echo == ECHO_ON){
			stdio_input->echo_output[echo_index] = '\0';
			_printf("%s", (char*)stdio_input->echo_output);
		}

		stdio_input->input_command_str_count = stdio_input->input_command_str_count + new_chars_number;
#endif

		stdio_input->tail = stdio_input->head;
	}

	return RET_OK;
}

#if USE_YETISHELL

retval_t check_newline(){

#if (STDIO_INTERFACE == USB_STDIO)
	#if USE_TRACEALYZER_SW == 1
	return RET_ERROR;
#endif
#endif
#if (USE_STOP_MODE == 1)
	return RET_ERROR;
#endif

	uint16_t i;
	uint16_t j;

	for(i=stdio_input->input_command_str_current_count; i <stdio_input->input_command_str_count; i++){		//Now check for a newline. The new line could be '\r' '\n' or '\r\n'

		if((stdio_input->input_commands_str[i] == '\r') || (stdio_input->input_commands_str[i] == '\n')){

			stdio_input->input_commands_str[i] = '\0';

			stdio_input->command->argv = str_split((char*) stdio_input->input_commands_str, ' ', (int*) &stdio_input->command->argc );	//Memory for argv is reserved inside str_split function. It is necessary to free it later

			uint16_t extra_bytes = stdio_input->input_command_str_count-i-1;		//Number of existing bytes after '\r' or '\n'

			uint16_t rpt_new_line = 0;
			if(extra_bytes > 0){
				if((stdio_input->input_commands_str[i+1] == '\r') || (stdio_input->input_commands_str[i+1] == '\n')){
					extra_bytes--;
					rpt_new_line = 1;
				}
			}
			for(j=0; j<extra_bytes; j++){				//Moves everything (if exists) after '\r' or '\n' to the start of the buffer

				stdio_input->input_commands_str[j] = stdio_input->input_commands_str[i+j+1+rpt_new_line];

			}

			stdio_input->input_command_str_current_count = 0;
			stdio_input->input_command_str_count = extra_bytes;

			return RET_OK;

		}
	}

	stdio_input->input_command_str_current_count = stdio_input->input_command_str_count;


	return RET_OK;

}

retval_t check_add_char(uint8_t new_char, int32_t* prev_new_char_number, uint16_t* command_index, uint16_t* echo_index){
#if (STDIO_INTERFACE == USB_STDIO)
	#if USE_TRACEALYZER_SW == 1
	return RET_ERROR;
#endif
#endif
#if (USE_STOP_MODE == 1)
	return RET_ERROR;
#endif

	switch (new_char){
	case '\b':				//Borrar caracter

		if(stdio_input->input_command_str_current_count > 0){			//Reduzco una posicion del buffer guardado
			stdio_input->input_command_str_current_count--;
		}

		if(stdio_input->input_command_str_count > 0){					//Se resta dos al numero de caracteres nuevos (el anterior a borrar y el propio \b)
			(*prev_new_char_number) = (*prev_new_char_number)- 2;
		}
		else{
			(*prev_new_char_number)--;									//Solo se resta el comando \b (no hay anteriores)
		}

		if((*command_index) > 0){										//Se resta dos al indice (el anterior a borrar y el propio \b)
			(*command_index) = (*command_index)- 2;
		}
		else{
			(*command_index)--;											//Solo se resta el indice de \b (no hay anteriores)
		}


		if(stdio_input->echo == ECHO_ON){
			if(stdio_input->input_command_str_count > 0){
				if((*echo_index) < INPUT_BUFFER_SIZE-2){
					stdio_input->echo_output[(*echo_index)] = '\b';
					stdio_input->echo_output[++(*echo_index)] = ' ';
					stdio_input->echo_output[++(*echo_index)] = '\b';
				}
			}
			else{
					(*echo_index)--;
			}
		}

		break;
	case '\r':				//En caso de recibir \r imprimir por echo \r\n
		if(((*command_index)+stdio_input->input_command_str_count) < stdio_input->max_cmd_buffer_size){
			stdio_input->input_commands_str[(*command_index)+stdio_input->input_command_str_count] = new_char;
		}

		if(stdio_input->echo == ECHO_ON){
			if((*echo_index) < INPUT_BUFFER_SIZE-1){
				stdio_input->echo_output[(*echo_index)] = '\r';
				stdio_input->echo_output[++(*echo_index)] = '\n';
			}
		}
		break;
	case '\n':				//En caso de recibir \n no imprimir nada por echo
		if(((*command_index)+stdio_input->input_command_str_count) < stdio_input->max_cmd_buffer_size){
			stdio_input->input_commands_str[(*command_index)+stdio_input->input_command_str_count] = new_char;
		}
		if(stdio_input->echo == ECHO_ON){
			(*echo_index)--;
		}
		break;
	default:
		if(((*command_index)+stdio_input->input_command_str_count) < stdio_input->max_cmd_buffer_size){
			stdio_input->input_commands_str[(*command_index)+stdio_input->input_command_str_count] = new_char;
		}
		if(stdio_input->echo == ECHO_ON){
			stdio_input->echo_output[(*echo_index)] = new_char;
		}
		break;
	}

	return RET_OK;
}
#endif


/**
 * @brief			Reads the selected num of bytes from STDIN (USB or UART)
 * @param data		Pointer to the buffer to store the read data
 * @param size		Number of bytes to read
 * @param timeout	Read timeout. 0 for infinite timeout
 * @return			Return status
 */
retval_t stdin_read(uint8_t* data, uint16_t size, uint64_t timeout){
#if (STDIO_INTERFACE == USB_STDIO)
	#if USE_TRACEALYZER_SW == 1
	return RET_ERROR;
#endif
#endif
#if (USE_STOP_MODE == 1)
	return RET_ERROR;
#endif
	uint64_t start_time;
	retval_t ret = RET_OK;
	ytMutexWait(stdin_mutex_id, YT_WAIT_FOREVER);
	start_time = ytTimeGetTimestamp();
	stdio_input->stdin_api_read_mode = STDIN_READING_BYTES;
	stdio_input->stdin_api_current_bytes_read = 0;
	stdio_input->stdin_api_num_bytes_to_read = size;
	stdio_input->stdin_api_read_buff = data;

	while(stdio_input->stdin_api_current_bytes_read < size){
		ytDelay(5);
		if(timeout > 0){
			if((ytTimeGetTimestamp() - start_time) > timeout){
				ret = RET_TIMEOUT;
				break;
			}
		}
	}

	stdio_input->stdin_api_read_mode = STDIN_NO_READING;
	stdio_input->stdin_api_num_bytes_to_read = 0;
	stdio_input->stdin_api_current_bytes_read = 0;
	stdio_input->stdin_api_read_buff = NULL;
	ytMutexRelease(stdin_mutex_id);
	return ret;
}

/**
 * @brief			Reads from STDIN (USB or UART) until a end of line is detected. If the max_size number of bytes param is reached before an end of line the function exits
 * @note			The '\n' command does not count and is not stored in the data buffer. Instead, the buffer is ended with '\0'
 * @param data		Pointer to the buffer to store the read data
 * @param max_size	Maximum number of bytes to read
 * @param timeout	Read timeout. 0 for infinite timeout
 * @return			The number of bytes actually read
 */
uint16_t stdin_read_line(uint8_t* data, uint16_t max_size, uint64_t timeout){
#if (STDIO_INTERFACE == USB_STDIO)
	#if USE_TRACEALYZER_SW == 1
	return 0;
#endif
#endif
#if (USE_STOP_MODE == 1)
	return 0;
#endif
	uint64_t start_time;
	uint16_t i;
	uint16_t read_bytes = 0;
	ytMutexWait(stdin_mutex_id, YT_WAIT_FOREVER);
	start_time = ytTimeGetTimestamp();
	stdio_input->stdin_api_read_mode = STDIN_READING_LINE;
	stdio_input->stdin_api_current_bytes_read = 0;
	stdio_input->stdin_api_num_bytes_to_read = max_size;
	stdio_input->stdin_api_read_buff = data;

	while(stdio_input->stdin_api_current_bytes_read < max_size){
		ytDelay(5);
		for(i=0; i<stdio_input->stdin_api_current_bytes_read; i++){
			if((data[i] == '\n') || (data[i] == '\r')){
				read_bytes = i;
				data[i] = '\0';
				goto STDIN_READ_LINE_END;
			}
		}
		if(timeout > 0){
			if((ytTimeGetTimestamp() - start_time) > timeout){
				break;
			}
		}
	}

STDIN_READ_LINE_END:

	stdio_input->stdin_api_read_mode = STDIN_NO_READING;
	stdio_input->stdin_api_num_bytes_to_read = 0;
	stdio_input->stdin_api_current_bytes_read = 0;
	stdio_input->stdin_api_read_buff = NULL;
	ytMutexRelease(stdin_mutex_id);
	return read_bytes;
}

#endif
