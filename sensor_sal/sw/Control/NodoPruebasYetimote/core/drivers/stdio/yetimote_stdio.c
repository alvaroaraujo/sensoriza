/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * yetimote_stdio.c
 *
 *  Created on: 7 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file yetimote_stdio.c
 */

#include "yetimote_stdio.h"
#include "system_api.h"

#if ENABLE_STDIO_FUNCS

extern retval_t init_stdio_input();
void StdioTask_callback(void const * argument);
static void null_printf(char* format, ...);


retval_t (* stdio_update_rcv_counter)(void);

retval_t stdio_init(void){

#if (USE_STOP_MODE == 1)
	_printf = null_printf;
	return RET_OK;
#endif

#if (STDIO_INTERFACE == USB_STDIO)
	#if USE_TRACEALYZER_SW == 0
		init_stdio_input();
		usb_stdio_init();
		_printf = usb_printf;
		stdio_update_rcv_counter = usb_stdio_update_rcv_counter;
	#else
		_printf = null_printf;
	#endif
#elif (STDIO_INTERFACE == UART_STDIO)
	init_stdio_input();
	uart_stdio_init();
	_printf = uart_printf;
	stdio_update_rcv_counter = uart_stdio_update_rcv_counter;
#endif

	ytStartProcess("StdioTask", StdioTask_callback, LOW_PRIORITY_PROCESS, 256, NULL, NULL);

	return RET_OK;
}

retval_t stdout_send(uint8_t* data, uint16_t size){
#if (USE_STOP_MODE == 1)
	return RET_ERROR;
#endif
#if (STDIO_INTERFACE == USB_STDIO)
	#if USE_TRACEALYZER_SW == 1
		return RET_ERROR;
	#endif
	return usb_stdout_send(data, size);
#elif (STDIO_INTERFACE == UART_STDIO)
	return uart_stdout_send(data, size);
#endif
}


static void null_printf(char* format, ...){
}
#endif
