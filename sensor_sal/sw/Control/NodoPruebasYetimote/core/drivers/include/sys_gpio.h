/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * sys_gpio.h
 *
 *  Created on: 20 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file sys_gpio.h
 */
#ifndef APPLICATION_CORE_DRIVERS_GPIO_SYS_GPIO_H_
#define APPLICATION_CORE_DRIVERS_GPIO_SYS_GPIO_H_

#include "generic_list.h"

typedef enum gpio_mode_{
	GPIO_PIN_INPUT,
	GPIO_PIN_OUTPUT_PUSH_PULL,
	GPIO_PIN_OUTPUT_OPEN_DRAIN,
	GPIO_PIN_INTERRUPT_FALLING,
	GPIO_PIN_INTERRUPT_RISING,
	GPIO_PIN_INTERRUPT_FALLING_RISING,
	GPIO_PIN_ANALOG,
	GPIO_PIN_ANALOG_ADC_CONTROL,
	GPIO_PIN_AF1_TIM1,
	GPIO_PIN_AF1_TIM2,
	GPIO_PIN_AF2_TIM3,
	GPIO_PIN_AF2_TIM4,
	GPIO_PIN_AF2_TIM5,
	GPIO_PIN_AF3_TIM8,
	GPIO_PIN_AF14_TIM15,
	GPIO_PIN_AF14_TIM16,
	GPIO_PIN_AF14_TIM17,
	GPIO_PIN_AF1_LPTIM1,
	GPIO_PIN_AF14_LPTIM2,
}gpio_mode_t;

typedef enum gpio_pull_{
	GPIO_PIN_NO_PULL,
	GPIO_PIN_PULLUP,
	GPIO_PIN_PULLDOWN,
}gpio_pull_t;


#include "gpio_arch.h"

typedef enum gpio_pin_{
	PLATFORM_PINS
}gpio_pin_t;


retval_t gpio_init(void);
retval_t gpio_exit(void);

retval_t y_gpio_init_pin(gpio_pin_t gpio, gpio_mode_t gpio_mode, gpio_pull_t gpio_pull);
retval_t gpio_deinit_pin(gpio_pin_t gpio);

retval_t gpio_pin_set(gpio_pin_t gpio);
retval_t gpio_pin_reset(gpio_pin_t gpio);
retval_t gpio_pin_toggle(gpio_pin_t gpio);

uint16_t gpio_pin_get(gpio_pin_t gpio);

retval_t gpio_set_callback_interrupt(gpio_pin_t gpio, void (*gpio_interrupt_func)(const void* args), void* args);

#endif /* APPLICATION_CORE_DRIVERS_GPIO_GPIO_H_ */
