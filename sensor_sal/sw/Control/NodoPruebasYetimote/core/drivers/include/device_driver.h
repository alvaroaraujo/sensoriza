/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * device_driver.h
 *
 *  Created on: 11 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file device_driver.h
 */
#ifndef APPLICATION_CORE_DRIVERS_INCLUDE_DEVICE_DRIVER_H_
#define APPLICATION_CORE_DRIVERS_INCLUDE_DEVICE_DRIVER_H_

#include "platform-conf.h"
#include <types.h>

typedef enum {
	DEV_FILE_CLOSED = 0,
	DEV_FILE_OPENED = 0xBCBC,
	DEV_FILE_LOCKED = 1,
}dev_file_state_t;

struct driver_ops_;

typedef struct dev_file_{
	uint32_t flags;
	dev_file_state_t fil_state;
	struct driver_ops_* ops;
	void* private_data;
}dev_file_t;


typedef struct driver_ops_{
	retval_t (*open)(device_t* device, dev_file_t* filep);
	retval_t (*close)(dev_file_t* filep);
	size_t (*read)(dev_file_t* filep, uint8_t* prx, size_t size);
	size_t (*write)(dev_file_t* filep, uint8_t* ptx, size_t size);
	retval_t (*ioctl)(dev_file_t* filep, uint16_t request, void* args);
}driver_ops_t;


uint32_t y_open(const char* device_name, uint32_t flags);
retval_t y_close(uint32_t fildes);
size_t y_read(uint32_t fildes, void* buf, size_t bytes);
size_t y_write(uint32_t fildes, void* buf, size_t bytes);
retval_t y_ioctl(uint32_t fildes, uint16_t command, void* args);

#endif /* APPLICATION_CORE_DRIVERS_INCLUDE_DEVICE_DRIVER_H_ */
