/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * device.h
 *
 *  Created on: 12 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file device.h
 */
#ifndef APPLICATION_CORE_DRIVERS_INCLUDE_DEVICE_H_
#define APPLICATION_CORE_DRIVERS_INCLUDE_DEVICE_H_

#include "platform-conf.h"


typedef retval_t (*init_func_t)(void);

typedef enum dev_state_{
	DEV_STATE_NOT_INIT,
	DEV_STATE_INIT,
	DEV_STATE_ERROR,
}dev_state_t;

struct driver_ops_;

typedef struct device_{
	uint16_t dev_id;
	dev_state_t device_state;
	struct driver_ops_* ops;
	char* device_name;
}device_t;

#include "device_driver.h"


//Hihgest priority device initialization function
#define init_device(fn) \
    static retval_t (*__initcall_##fn)(void) __attribute__((__used__)) \
    __attribute__((__section__(".dev_initcall_0"))) = fn

#define exit_device(fn) \
    static retval_t (*__exitcall_##fn)(void) __attribute__((__used__)) \
    __attribute__((__section__(".dev_exitcall_0"))) = fn

//Second priority device initialization function
#define init_device_1(fn) \
    static retval_t (*__initcall_##fn)(void) __attribute__((__used__)) \
    __attribute__((__section__(".dev_initcall_1"))) = fn

#define exit_device_1(fn) \
    static retval_t (*__exitcall_##fn)(void) __attribute__((__used__)) \
    __attribute__((__section__(".dev_exitcall_1"))) = fn

//Third priority device initialization function
#define init_device_2(fn) \
    static retval_t (*__initcall_##fn)(void) __attribute__((__used__)) \
    __attribute__((__section__(".dev_initcall_2"))) = fn

#define exit_device_2(fn) \
    static retval_t (*__exitcall_##fn)(void) __attribute__((__used__)) \
    __attribute__((__section__(".dev_exitcall_2"))) = fn

//Last priority device initialization function
#define init_device_3(fn) \
    static retval_t (*__initcall_##fn)(void) __attribute__((__used__)) \
    __attribute__((__section__(".dev_initcall_3"))) = fn

#define exit_device_3(fn) \
    static retval_t (*__exitcall_##fn)(void) __attribute__((__used__)) \
    __attribute__((__section__(".dev_exitcall_3"))) = fn


retval_t init_devices(void);
retval_t exit_devices(void);
device_t* getDeviceFromName(const char* name);

retval_t registerDevice(uint16_t device_id, driver_ops_t* ops, char* name);
retval_t unregisterDevice(uint16_t device_id, char* name);

retval_t printDevices(void);



#endif /* APPLICATION_CORE_DRIVERS_INCLUDE_DEVICE_H_ */
