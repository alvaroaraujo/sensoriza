/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * yetimote_stdio.h
 *
 *  Created on: 7 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file yetimote_stdio.h
 */
#ifndef APPLICATION_CORE_DRIVERS_INCLUDE_YETIMOTE_STDIO_H_
#define APPLICATION_CORE_DRIVERS_INCLUDE_YETIMOTE_STDIO_H_

#include "platform-conf.h"

#if ENABLE_STDIO_FUNCS

#include "platform_stdio.h"
#include "yetishell.h"
#include "strfunc.h"

typedef struct stdio_input_{
	uint8_t circular_buffer[INPUT_BUFFER_SIZE];		//Variables para la entrada de datos y su manejo en el buffer circular
	uint8_t* head;
	uint8_t* tail;
	uint16_t rcv_buffer_counter;

	uint8_t* stdin_api_read_buff;			//Variables necesarias para las funciones read de la api stdio
	uint16_t stdin_api_num_bytes_to_read;
	uint16_t stdin_api_current_bytes_read;
	uint8_t stdin_api_read_mode;			//Options: STDIN_NO_READING, STDIN_READING_BYTES, STDIN_READING_LINE

#if USE_YETISHELL								//Variables necesarias para el uso de la shell
	uint8_t input_commands_str[INPUT_BUFFER_SIZE+1];
	uint16_t input_command_str_count;
	uint16_t input_command_str_current_count;
	uint16_t max_cmd_buffer_size;
	command_data_t* command;
	uint8_t echo;
	uint8_t echo_output[INPUT_BUFFER_SIZE];
#endif

}stdio_input_t;




retval_t stdio_init(void); 	//Aqui lanzo la hebra y tal

void (* _printf)(char* format, ...);
retval_t stdout_send(uint8_t* data, uint16_t size);

retval_t stdin_read(uint8_t* data, uint16_t size, uint64_t timeout);
uint16_t stdin_read_line(uint8_t* data, uint16_t max_size, uint64_t timeout);

#endif

#endif /* APPLICATION_CORE_DRIVERS_INCLUDE_YETIMOTE_STDIO_H_ */
