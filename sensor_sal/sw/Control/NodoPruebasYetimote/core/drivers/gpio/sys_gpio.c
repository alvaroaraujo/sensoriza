/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * sys_gpio.c
 *
 *  Created on: 20 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file sys_gpio.c
 */

#include "sys_gpio.h"
#include "system_api.h"

#define ARRAY_LEN(x)  (sizeof(x) / sizeof((x)[0]))


typedef struct gpio_interrupt_callback_{
	void (*gpio_int_cb_func)(const void* args);
	void* args;
}gpio_interrupt_callback_t;

typedef struct gpio_config_{
	gpio_pin_t	gpio_pin;
	gpio_mode_t gpio_mode;
	gpio_pull_t gpio_pull;
	gpio_interrupt_callback_t gpio_interrupt_callback;
	uint16_t gpio_pin_state;
}gpio_config_t;

static gpio_pin_t available_gpio_pin[] = {
	PLATFORM_AVAILABLE_PINS
};

static gpio_pin_t reserved_gpio_pin[] = {
	PLATFORM_RESERVED_PINS
};

/* Private list with configured GPIOs. Private */
static gen_list* registeredGpio;

/*Private function called only by gpio_arch.c by the interrupt callback function (used as extern)*/
void sysGpioInterruptCallback(gpio_pin_t gpio_pin);


/*Private functions */
static retval_t gpio_init_pin(gpio_pin_t gpio, gpio_mode_t gpio_mode, gpio_pull_t gpio_pull);
static retval_t gpio_init_reserved_pin(gpio_pin_t gpio, gpio_mode_t gpio_mode, gpio_pull_t gpio_pull);


static gpio_config_t* get_gpio_config_from_list(gpio_pin_t gpio_pin);
static gpio_config_t* new_gpio_config(gpio_pin_t gpio_pin, gpio_mode_t gpio_mode, gpio_pull_t gpio_pull);

/**
 *
 * @return
 */
retval_t gpio_init(void){
	registeredGpio = gen_list_init();
	return RET_OK;
}

/**
 *
 * @return
 */
retval_t gpio_exit(void){
	gen_list_remove_all(registeredGpio);
	return RET_OK;
}



/**
 *
 * @param gpio
 * @param gpio_mode
 * @param gpio_pull
 * @return
 */
retval_t y_gpio_init_pin(gpio_pin_t gpio, gpio_mode_t gpio_mode, gpio_pull_t gpio_pull){

	if(gpio_init_pin(gpio, gpio_mode, gpio_pull) != RET_OK){
		if(gpio_init_reserved_pin(gpio, gpio_mode, gpio_pull) != RET_OK){
			return RET_ERROR;
		}
	}
	return RET_OK;
}

/**
 *
 * @param gpio
 * @param gpio_mode
 * @param gpio_pull
 * @return
 */
retval_t gpio_init_pin(gpio_pin_t gpio, gpio_mode_t gpio_mode, gpio_pull_t gpio_pull){
	uint16_t i;
	uint16_t gpio_available = 0;
	gpio_config_t* gpio_config;

	for(i=0; i<ARRAY_LEN(available_gpio_pin); i++){
		if(available_gpio_pin[i] == gpio){
			gpio_available = 1;
			break;
		}
	}
	if(gpio_available){

		if(gpio_arch_init_pin((uint32_t)gpio, gpio_mode, gpio_pull) == RET_ERROR){

			return RET_ERROR;
		}
		gpio_config =  new_gpio_config(gpio, gpio_mode, gpio_pull);
		gen_list_add(registeredGpio, gpio_config);
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

/**
 *
 * @param gpio
 * @param gpio_mode
 * @param gpio_pull
 * @return
 */
retval_t gpio_init_reserved_pin(gpio_pin_t gpio, gpio_mode_t gpio_mode, gpio_pull_t gpio_pull){
	uint16_t i;
	uint16_t gpio_available = 0;
	gpio_config_t* gpio_config;

	for(i=0; i<ARRAY_LEN(reserved_gpio_pin); i++){
		if(reserved_gpio_pin[i] == gpio){
			gpio_available = 1;
			break;
		}
	}
	if(gpio_available){

		if(gpio_arch_init_pin((uint32_t)gpio, gpio_mode, gpio_pull) == RET_ERROR){

			return RET_ERROR;
		}
		gpio_config =  new_gpio_config(gpio, gpio_mode, gpio_pull);
		gen_list_add(registeredGpio, gpio_config);
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

/*/
 *
 */
retval_t gpio_deinit_pin(gpio_pin_t gpio){
	gpio_config_t* gpio_config;
	if((gpio_config = get_gpio_config_from_list(gpio)) == NULL){
		return RET_ERROR;
	}
	gen_list_remove(registeredGpio, gpio_config);
	ytFree(gpio_config);

	return RET_OK;
}

/**
 *
 * @param gpio
 * @return
 */
retval_t gpio_pin_set(gpio_pin_t gpio){
	gpio_config_t* gpio_config;
	if((gpio_config = get_gpio_config_from_list(gpio)) == NULL){
		return RET_ERROR;
	}
	if((gpio_config->gpio_mode != GPIO_PIN_OUTPUT_PUSH_PULL) && (gpio_config->gpio_mode != GPIO_PIN_OUTPUT_OPEN_DRAIN)){
		return RET_ERROR;
	}
	gpio_arch_set_pin((uint32_t)gpio);
	gpio_config->gpio_pin_state = 1;
	return RET_OK;
}

/**
 *
 * @param gpio
 * @return
 */
retval_t gpio_pin_reset(gpio_pin_t gpio){
	gpio_config_t* gpio_config;
	if((gpio_config = get_gpio_config_from_list(gpio)) == NULL){
		return RET_ERROR;
	}
	if((gpio_config->gpio_mode != GPIO_PIN_OUTPUT_PUSH_PULL) && (gpio_config->gpio_mode != GPIO_PIN_OUTPUT_OPEN_DRAIN)){
		return RET_ERROR;
	}
	gpio_arch_reset_pin((uint32_t)gpio);
	gpio_config->gpio_pin_state = 0;
	return RET_OK;
}

/**
 *
 * @param gpio
 * @return
 */
retval_t gpio_pin_toggle(gpio_pin_t gpio){
	gpio_config_t* gpio_config;
	if((gpio_config = get_gpio_config_from_list(gpio)) == NULL){
		return RET_ERROR;
	}
	if((gpio_config->gpio_mode != GPIO_PIN_OUTPUT_PUSH_PULL) && (gpio_config->gpio_mode != GPIO_PIN_OUTPUT_OPEN_DRAIN)){
		return RET_ERROR;
	}
	gpio_arch_toggle_pin((uint32_t)gpio);
	if(gpio_config->gpio_pin_state == 0){
		gpio_config->gpio_pin_state = 1;
	}
	else{
		gpio_config->gpio_pin_state = 0;
	}
	return RET_OK;
}

/**
 *
 * @param gpio
 * @return
 */
uint16_t gpio_pin_get(gpio_pin_t gpio){
	gpio_config_t* gpio_config;
	if((gpio_config = get_gpio_config_from_list(gpio)) == NULL){
		return 0xFFFF;
	}
	if((gpio_config->gpio_mode != GPIO_PIN_INPUT) && (gpio_config->gpio_mode != GPIO_PIN_INTERRUPT_FALLING)
			&& (gpio_config->gpio_mode != GPIO_PIN_INTERRUPT_RISING) && (gpio_config->gpio_mode != GPIO_PIN_INTERRUPT_FALLING_RISING)){
		return 0xFFFF;
	}
	gpio_config->gpio_pin_state = gpio_arch_get_pin((uint32_t)gpio);
	return gpio_config->gpio_pin_state;
}

/**
 *
 * @param gpio
 * @param gpio_interrupt_func
 * @param args
 * @return
 */
retval_t gpio_set_callback_interrupt(gpio_pin_t gpio, void (*gpio_interrupt_func)(const void* args), void* args){
	gpio_config_t* gpio_config;
	if((gpio_config = get_gpio_config_from_list(gpio)) == NULL){
		return RET_ERROR;
	}
	if((gpio_config->gpio_mode != GPIO_PIN_INTERRUPT_FALLING) && (gpio_config->gpio_mode != GPIO_PIN_INTERRUPT_RISING)
			&& (gpio_config->gpio_mode != GPIO_PIN_INTERRUPT_FALLING_RISING)){
		return RET_ERROR;
	}
	gpio_config->gpio_interrupt_callback.gpio_int_cb_func = gpio_interrupt_func;
	gpio_config->gpio_interrupt_callback.args  = args;

	return RET_OK;
}

/**
 *
 * @param gpio_pin
 */
void sysGpioInterruptCallback(gpio_pin_t gpio_pin){
	gen_list* current = registeredGpio;
	gpio_config_t* gpio_config;

	while(current->next != NULL){
		gpio_config = (gpio_config_t*) current->next->item;
		if((gpio_config->gpio_mode == GPIO_PIN_INTERRUPT_FALLING) || (gpio_config->gpio_mode == GPIO_PIN_INTERRUPT_RISING) || (gpio_config->gpio_mode == GPIO_PIN_INTERRUPT_FALLING_RISING)){
			if(gpio_pin == ((gpio_config->gpio_pin)&0xFFFF0000)){
				if(gpio_config->gpio_interrupt_callback.gpio_int_cb_func != NULL){
					gpio_config->gpio_interrupt_callback.gpio_int_cb_func(gpio_config->gpio_interrupt_callback.args);
				}
				break;
			}
		}
		current = current->next;
	}

}

/**
 *
 * @param gpio_pin
 * @param gpio_mode
 * @param gpio_pull
 * @return
 */
static gpio_config_t* new_gpio_config(gpio_pin_t gpio_pin, gpio_mode_t gpio_mode, gpio_pull_t gpio_pull){
	gpio_config_t* gpio_config;
	if((gpio_config = (gpio_config_t*) ytMalloc(sizeof(gpio_config_t))) == NULL){
		return NULL;
	}
	gpio_config->gpio_pin = gpio_pin;
	gpio_config->gpio_mode = gpio_mode;
	gpio_config->gpio_pull = gpio_pull;
	gpio_config->gpio_pin_state = 0;		//Estado inicial fijado a 0 aunque podr�a no ser fisicamente el real
	gpio_config->gpio_interrupt_callback.gpio_int_cb_func = NULL;
	gpio_config->gpio_interrupt_callback.args = NULL;

	return gpio_config;
}


/**
 *
 * @param gpio_pin
 * @return
 */
static gpio_config_t* get_gpio_config_from_list(gpio_pin_t gpio_pin){
	gen_list* current = registeredGpio;
	gpio_config_t* gpio_config;

	while(current->next != NULL){
		gpio_config = (gpio_config_t*) current->next->item;

		if(gpio_pin == (gpio_config->gpio_pin)){
			return gpio_config;
		}

		current = current->next;
	}
	return NULL;
}
