/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * device.c
 *
 *  Created on: 12 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file device.c
 */

#include "device.h"
#include "generic_list.h"
#include <stdio.h>
#include <string.h>
#include "yetimote_stdio.h"
#include "system_api.h"

static gen_list* deviceList;

/* Private functions */
static uint16_t generateNewUniqueId(void);
static uint16_t checkDeviceId(uint16_t dev_id);
static device_t* getDeviceFromId(uint16_t dev_id);


/**
 * @brief	Call the init functions of the devices compiled
 * @return	Return status
 */
retval_t init_devices(void){
	extern uint32_t (*_dev_initcall_start_0)(void); // Defined by the linker
	extern uint32_t (*_dev_initcall_end_0)(void); // Defined by the linker

	extern uint32_t (*_dev_initcall_start_1)(void); // Defined by the linker
	extern uint32_t (*_dev_initcall_end_1)(void); // Defined by the linker

	extern uint32_t (*_dev_initcall_start_2)(void); // Defined by the linker
	extern uint32_t (*_dev_initcall_end_2)(void); // Defined by the linker

	extern uint32_t (*_dev_initcall_start_3)(void); // Defined by the linker
	extern uint32_t (*_dev_initcall_end_3)(void); // Defined by the linker

	uint32_t* start = (uint32_t*) &_dev_initcall_start_0;
	uint32_t* end = (uint32_t*) &_dev_initcall_end_0;

	deviceList = gen_list_init();

	while(((*start) != (uint32_t)NULL) && (start < end)){
		retval_t (*func)(void)=(retval_t(*)(void))(*start);
		if(func != NULL){
			func();
		}
		start++;
	}

	start = (uint32_t*) &_dev_initcall_start_1;
	end = (uint32_t*) &_dev_initcall_end_1;

	while(((*start) != (uint32_t)NULL) && (start < end)){
		retval_t (*func)(void)=(retval_t(*)(void))(*start);
		if(func != NULL){
			func();
		}
		start++;
	}

	start = (uint32_t*) &_dev_initcall_start_2;
	end = (uint32_t*) &_dev_initcall_end_2;

	while(((*start) != (uint32_t)NULL) && (start < end)){
		retval_t (*func)(void)=(retval_t(*)(void))(*start);
		if(func != NULL){
			func();
		}
		start++;
	}

	start = (uint32_t*) &_dev_initcall_start_3;
	end = (uint32_t*) &_dev_initcall_end_3;

	while(((*start) != (uint32_t)NULL) && (start < end)){
		retval_t (*func)(void)=(retval_t(*)(void))(*start);
		if(func != NULL){
			func();
		}
		start++;
	}

	return RET_OK;
};


/**
 * @brief	Call the exit functions of the devices compiled
 * @return	Return status
 */
retval_t exit_devices(void){
	extern uint32_t (*_dev_exitcall_start_0)(void); // Defined by the linker
	extern uint32_t (*_dev_exitcall_end_0)(void); // Defined by the linker

	extern uint32_t (*_dev_exitcall_start_1)(void); // Defined by the linker
	extern uint32_t (*_dev_exitcall_end_1)(void); // Defined by the linker

	extern uint32_t (*_dev_exitcall_start_2)(void); // Defined by the linker
	extern uint32_t (*_dev_exitcall_end_2)(void); // Defined by the linker

	extern uint32_t (*_dev_exitcall_start_3)(void); // Defined by the linker
	extern uint32_t (*_dev_exitcall_end_3)(void); // Defined by the linker

	uint32_t* start = (uint32_t*) &_dev_exitcall_start_0;
	uint32_t* end = (uint32_t*) &_dev_exitcall_end_0;

	while(((*start) != (uint32_t)NULL) && (start < end)){
		retval_t (*func)(void)=(retval_t(*)(void))(*start);
		if(func != NULL){
			func();
		}
		start++;
	}

	start = (uint32_t*) &_dev_exitcall_start_1;
	end = (uint32_t*) &_dev_exitcall_end_1;

	while(((*start) != (uint32_t)NULL) && (start < end)){
		retval_t (*func)(void)=(retval_t(*)(void))(*start);
		if(func != NULL){
			func();
		}
		start++;
	}

	start = (uint32_t*) &_dev_exitcall_start_2;
	end = (uint32_t*) &_dev_exitcall_end_2;

	while(((*start) != (uint32_t)NULL) && (start < end)){
		retval_t (*func)(void)=(retval_t(*)(void))(*start);
		if(func != NULL){
			func();
		}
		start++;
	}

	start = (uint32_t*) &_dev_exitcall_start_3;
	end = (uint32_t*) &_dev_exitcall_end_3;

	gen_list_remove_all(deviceList);
	while(((*start) != (uint32_t)NULL) && (start < end)){
		retval_t (*func)(void)=(retval_t(*)(void))(*start);
		if(func != NULL){
			func();
		}
		start++;
	}

	return RET_OK;
};

/**
 * @brief			Registers a device with the ID, name and operations specified
 * @param device_id	Device ID to be registered. If it already exists a new device Id is generated
 * @param ops		File operations to be used by the device driver
 * @param name		Name of the device. Max size: 16 chars otherwise returns error
 * @return			Return status
 */
retval_t registerDevice(uint16_t device_id, driver_ops_t* ops, char* name){
	device_t* new_device = (device_t*) ytMalloc(sizeof(device_t));
	if(new_device == NULL){
		return RET_ERROR;
	}
	if((new_device->device_name = ytMalloc(strlen(name) +1)) == NULL){
		ytFree(new_device);
		return RET_ERROR;
	}
	if(checkDeviceId(device_id)){	//Si ya existe la id, genero una nueva
		new_device->dev_id = generateNewUniqueId();
	}
	else{		//Sino le pongo la que se pasa por parametro
		new_device->dev_id = device_id;
	}
	new_device->ops = ops;
	strcpy(new_device->device_name, name);
	new_device->device_state = DEV_STATE_INIT;

	gen_list_add(deviceList, (void*) new_device);
	return RET_OK;
}

/**
 * @brief			Unregister a previously registered device. First checks the id and then the name
 * @param device_id	Device Id to be unregistered
 * @param name		Device name to be unregistered
 * @return			REturn status
 */
retval_t unregisterDevice(uint16_t device_id, char* name){
	device_t* device;
	if((device = getDeviceFromId(device_id)) == NULL){
		if((device = getDeviceFromName(name)) == NULL){
			return RET_ERROR;
		}
		else{
			//Unregister
			gen_list_remove(deviceList, (void*) device);
			ytFree(device->device_name);
			ytFree(device);
		}
	}
	else{
		if(strcmp(device->device_name, name) == 0){
			gen_list_remove(deviceList, (void*) device);
			ytFree(device->device_name);
			ytFree(device);
			//Unregister
		}
		else{
			if((device = getDeviceFromName(name)) == NULL){
				return RET_ERROR;
			}
			else{
				gen_list_remove(deviceList, (void*) device);
				ytFree(device->device_name);
				ytFree(device);
				//Unregister
			}
		}
	}
	return RET_OK;
}

/**
 * @brief			Gets the device struct handler from a name. Private function
 * @param name		Name of the device to be obtained
 * @return			Device struct handler. Null if the device id does not exist
 */
device_t* getDeviceFromName(const char* name){
	device_t* device;
	gen_list* current = deviceList;
	while(current->next != NULL){
		device = (device_t*) current->next->item;
		if(strcmp(device->device_name, name) == 0){
			return device;
			break;
		}
		current = current->next;
	}
	return NULL;
}


/**
 *
 * @return
 */
retval_t printDevices(void){
	device_t* device;
	gen_list* current = deviceList;
	_printf("ACTIVE DEVICES LIST:\r\n");
	while(current->next != NULL){
		device = (device_t*) current->next->item;
		_printf("Device: %s\tId: %d\r\n", device->device_name, (int)device->dev_id);
		current = current->next;
	}
	return RET_OK;
}


/**
 * @brief	Generate a new unique id. Private function
 * @return	Return status
 */
static uint16_t generateNewUniqueId(void){
	device_t* device;
	uint16_t current_val = 1;
	uint16_t repeat_search = 1;
	while(repeat_search){
	gen_list* current = deviceList;
	repeat_search = 0;
		while(current->next != NULL){
			device = (device_t*) current->next->item;
			if(device->dev_id == current_val){
				current_val++;
				repeat_search = 1;
				break;
			}
			current = current->next;
		}
	}
	return current_val;
}

/**
 * @brief			Checks if the id selected exists in the device list. Private function
 * @param dev_id	The device id to be checked
 * @return			Return distint to zero if the device id exists
 */
static uint16_t checkDeviceId(uint16_t dev_id){
	device_t* device;
	gen_list* current = deviceList;
	while(current->next != NULL){
		device = (device_t*) current->next->item;
		if(device->dev_id == dev_id){
			return 1;
			break;
		}
		current = current->next;
	}
	return 0;
}


/**
 * @brief			Gets the device struct handler from a device id. Private function
 * @param dev_id	Device id to be obtained
 * @return			Device struct handler. Null if the device id does not exist
 */
static device_t* getDeviceFromId(uint16_t dev_id){

	device_t* device;
	gen_list* current = deviceList;
	while(current->next != NULL){
		device = (device_t*) current->next->item;
		if(device->dev_id == dev_id){
			return device;
			break;
		}
		current = current->next;
	}
	return NULL;
}


