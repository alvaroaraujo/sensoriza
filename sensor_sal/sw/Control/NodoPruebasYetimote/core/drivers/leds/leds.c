/*
 * Copyright (c) 2005, Swedish Institute of Computer Science
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

#include "leds.h"

static unsigned char leds;

/**
 * @brief			Turn on or off the leds as selected by the param. Calls arch function
 * @param new_leds	Leds structure
 */
static void show_leds(unsigned char new_leds)
{
	 leds = new_leds;
	 leds_arch_set(leds);
}

/**
 * @brief	Initialize leds
 */
void leds_init(void)
{
  leds_arch_init();
  leds = 0;
}

/**
 * @brief	Get leds state
 * @return	Leds state
 */
unsigned char leds_get(void) {
  return leds_arch_get();
}

/**
 * @brief		Calls Show leds
 * @param ledv	Leds structure
 */
void leds_set(unsigned char ledv)
{
  show_leds(ledv);
}

/**
 * @brief		Turn on selected leds
 * @param ledv	Leds to be turned on
 */
retval_t leds_on(unsigned char ledv)
{
  show_leds(leds | ledv);
  return RET_OK;
}

/**
 * @brief		Turn off selected leds
 * @param ledv	Leds to be turned off
 */
retval_t leds_off(unsigned char ledv)
{
  show_leds(leds & ~ledv);
  return RET_OK;
}

/**
 * @brief		Toggle selected leds
 * @param ledv	Leds to be toggled
 */
retval_t leds_toggle(unsigned char ledv)
{
  show_leds(leds ^ ledv);
  return RET_OK;
}

