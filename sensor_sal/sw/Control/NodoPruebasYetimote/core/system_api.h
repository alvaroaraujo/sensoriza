/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * system_api.h
 *
 *  Created on: 2 de nov. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file system_api.h
 */
#ifndef APPLICATION_CORE_SYSTEM_API_H_
#define APPLICATION_CORE_SYSTEM_API_H_

#include "platform-conf.h"
#include "leds.h"
#include "process.h"
#include "device.h"
#include "device_driver.h"
#include "sys_gpio.h"
#include "yetimote_stdio.h"
#include "spi_driver.h"
#include "i2c_driver.h"
#include "uart_driver.h"
#include "adc_driver.h"
#include "rtc_time.h"
#include "reactor.h"
#include "time_meas.h"


#define API_FUNC	inline __attribute__((always_inline))

#define YT_WAIT_FOREVER	osWaitForever

/* *********************************************************/
/* ****************Processes Functions**********************/
/* *********************************************************/
/**
 * @brief 				MACRO to create processes at startup. Compilation Macro. Dont use it at runtime
 * @note 				This macro MUST be used only to set a process to be launched at startup. Never call this macro inside a function or at runtime
 * @param name			(char*) Process name. For memory saving avoid large names.
 * @param process_func	(process_func_t) Function to be launched when the process starts. (process_func_t) => void (*)(const void* args)
 * @param process_class	(process_class_t) Class of the process to be created.
 * @param stack			(uint32_t) Amount of stack to reserve for this process.
 * @param processId		(uint16_t*) Returned value of the process Id created.  Use a pointer to NULL if is not desired to return the id.
 * @param arg			(void*) Argument to the process function.
 *
 */
#define ytInitProcess(name, process_func, process_class, stack, processId, arg) INIT_PROCESS(name, process_func, process_class, stack, processId, arg)

#define YT_PROCESS	__USER_PROCESS

/**
 * @brief ytProcessFunc_t type equals to void (*)(const void* args)
 */
typedef process_func_t ytProcessFunc_t;

/**
 * @brief 	ytProcessClass_t equals to process_class_t
 * @note	Available Values: 	DEFAULT_PROCESS
 * 								LOW_PRIORITY_PROCESS
 *								HIGH_PRIORITY_PROCESS
 */
typedef process_class_t ytProcessClass_t;

API_FUNC retval_t ytStartProcess(char* name, ytProcessFunc_t func, ytProcessClass_t process_class, uint32_t stacksize, uint16_t* processUniqueId, void* arg);
API_FUNC retval_t ytStartDefaultProcess(char* name, ytProcessFunc_t func, uint16_t* processUniqueId);
API_FUNC retval_t ytExitProcess(uint16_t processId);

API_FUNC retval_t ytProcessSuspend(uint16_t processId);
API_FUNC retval_t ytProcessResume(uint16_t processId);


API_FUNC uint16_t ytGetProcessId(void);
API_FUNC uint16_t ytGetCurrentParentProcessId(void);
API_FUNC uint16_t ytGetParentProcessId(uint16_t processId);
API_FUNC retval_t ytPrintProcesses(void);

#define ytDelay(millisec)	osDelay(millisec)
/* *********************************************************/

/* *********************************************************/
/* ************Memory Allocation Functions******************/
/* *********************************************************/
/* **************Default memory allocation function ********/
API_FUNC void* ytMalloc(size_t size);
API_FUNC void ytFree(void* ptr);

/* *********************************************************/
/* ****Timers, Semaphores, mutexes and queues functions*****/
/* *********************************************************/
typedef enum  {
  ytTimerOnce             =     0,       ///< one-shot timer
  ytTimerPeriodic         =     1        ///< repeating timer
} ytTimerType_t;

API_FUNC uint32_t ytTimerCreate(ytTimerType_t timer_type, ytProcessFunc_t timer_func, void *argument);
API_FUNC retval_t ytTimerStart(uint32_t timer_id, uint32_t millisec);
API_FUNC retval_t ytTimerStop(uint32_t timer_id);
API_FUNC retval_t ytTimerDelete(uint32_t timer_id);

API_FUNC uint32_t ytMutexCreate(void);
API_FUNC retval_t ytMutexWait(uint32_t mutex_id, uint32_t millisec);
API_FUNC retval_t ytMutexRelease(uint32_t mutex_id);
API_FUNC retval_t ytMutexDelete(uint32_t mutex_id);

API_FUNC uint32_t ytSemaphoreCreate(int32_t count);
API_FUNC retval_t ytSemaphoreWait(uint32_t semaphore_id, uint32_t millisec);
API_FUNC retval_t ytSemaphoreRelease(uint32_t semaphore_id);
API_FUNC retval_t ytSemaphoreDelete(uint32_t semaphore_id);

API_FUNC uint32_t ytMessageqCreate(uint32_t q_size);
API_FUNC retval_t ytMessageqPut(uint32_t messageq_id, void* msg, uint32_t millisec);
API_FUNC void* ytMessageqGet(uint32_t messageq_id, uint32_t millisec);
API_FUNC retval_t ytMessageqDelete(uint32_t messageq_id);
/* *********************************************************/

/* *********************************************************/
/* ***********Reactor Event handling functions *************/
/* *********************************************************/
typedef reactor_t 			ytReactor_t;

typedef handle_event_func 	ytHandleEventFunc_t;


API_FUNC ytReactor_t* ytReactorCreate (uint32_t handle_event_timeout);
API_FUNC retval_t ytReactorDelete(ytReactor_t* reactor);
API_FUNC retval_t ytReactorHandleEvents(ytReactor_t* reactor);

API_FUNC retval_t ytPostEvent(ytReactor_t* reactor, ytHandleEventFunc_t event_handle_func, void* arg);
/* *********************************************************/


/* *********************************************************/
/* ************Device Drivers Functions*********************/
/* *********************************************************/
API_FUNC uint32_t ytOpen(const char* device_name, uint32_t flags);
API_FUNC retval_t ytClose(uint32_t fildes);
API_FUNC size_t ytRead(uint32_t fildes, void* buf, size_t bytes);
API_FUNC size_t ytWrite(uint32_t fildes, void* buf, size_t bytes);
API_FUNC retval_t ytIoctl(uint32_t fildes, uint16_t command, void* args);

API_FUNC retval_t ytPrintDevices(void);

/* *******************SPI DRIVER TYPES**********************/
/**
 * @brief	Enum containing the available IOCTL commands for SPI driver. Look at spi_driver.h
 */
typedef spi_driver_ioctl_cmd_t ytSpiDriverIoctlCmd_t;
/**
 * @brief	Struct that contains the RX and TX buffers used for the Read-Write Ioctl operation.
 */
typedef	spi_read_write_buffs_t ytSpiReadWriteBuffs_t;

/* *******************I2C DRIVER TYPES**********************/
/**
 * @brief	Enum containing the available IOCTL commands for I2C driver. Look at i2c_driver.h
 */
typedef i2c_driver_ioctl_cmd_t ytI2cDriverIoctlCmd_t;
/**
 * @brief	Struct that contains the data and buffers necessary for I2C IOCTL Write_Reg and Read_Reg operations
 */
typedef i2c_reg_ops_args_t ytI2cRegOpsArgs_t;

/* ******************UART DRIVER TYPES**********************/
/**
 * @brief	Enum containing the available IOCTL commands for UART driver. Look at uart_driver.h
 */
typedef uart_driver_ioctl_cmd_t ytUartDriverIoctlCmd_t;

/* *******************ADC DRIVER TYPES**********************/
/**
 * @brief	Enum containing the available IOCTL commands for ADC driver. Look at adc_driver.h
 */
typedef adc_driver_ioctl_cmd_t ytAdcDriverIoctlCmd_t;
/**
 * @brief	Struct containing the configuration for the ADC driver. This struct must be passed in the ADC_CONFIG IOCTL operation
 */
typedef adc_config_t ytAdcConfig_t;


/* *********************Leds********************************/
/**
 * @brief	Available leds type. Platform depending
 * @note 	Available leds: LEDS_GREEN, LEDS_BLUE, LEDS_RED1, LEDS_RED2, LEDS_ALL
 */
typedef unsigned char leds_t;

API_FUNC retval_t ytLedsOn(leds_t led);
API_FUNC retval_t ytLedsOff(leds_t led);
API_FUNC retval_t ytLedsToggle(leds_t led);
/* *********************GPIO********************************/
/**
 * @brief	Available gpio mode type. Platform depending
 * @note 	Gpio Modes: GPIO_PIN_INPUT, GPIO_PIN_OUTPUT_PUSH_PULL, GPIO_PIN_OUTPUT_OPEN_DRAIN, GPIO_PIN_INTERRUPT_FALLING,
 * 						GPIO_PIN_INTERRUPT_RISING, GPIO_PIN_INTERRUPT_FALLING_RISING, GPIO_PIN_ANALOG, GPIO_PIN_ANALOG_ADC_CONTROL
 */
typedef  gpio_mode_t gpioMode_t;

/**
 * @brief	Available gpio pull type. Platform depending
 * @note 	Gpio Pull mode: GPIO_PIN_NO_PULL, GPIO_PIN_PULLUP, GPIO_PIN_PULLDOWN
 */
typedef gpio_pull_t gpioPull_t;

/**
 * @brief	Available gpio pins. Platform Depending
 * @note	Go to gpio_arch.h to view the available gpio pins
 */
typedef gpio_pin_t gpioPin_t;

API_FUNC retval_t ytGpioInitPin(gpioPin_t gpio, gpioMode_t gpio_mode, gpioPull_t gpio_pull);
API_FUNC retval_t ytGpioDeInitPin(gpioPin_t gpio);
API_FUNC retval_t ytGpioPinSet(gpioPin_t gpio);
API_FUNC retval_t ytGpioPinReset(gpioPin_t gpio);
API_FUNC retval_t ytGpioPinToggle(gpioPin_t gpio);
API_FUNC uint16_t ytGpioPinGet(gpioPin_t gpio);
API_FUNC retval_t ytGpioPinSetCallbackInterrupt(gpioPin_t gpio, ytProcessFunc_t gpio_interrupt_func, void* args);

/* *********************STDIO********************************/
#define ytPrintf(...) _printf(__VA_ARGS__)

API_FUNC retval_t ytStdoutSend(uint8_t* data, uint16_t size);

API_FUNC retval_t ytStdinRead(uint8_t* data, uint16_t size, uint32_t timeout);
API_FUNC uint16_t ytStdinReadLine(uint8_t* data, uint16_t max_size, uint32_t timeout);
/* *********************************************************/

/* *********************************************************/
/* *********************Time Functions**********************/
/* *********************************************************/
/**
 * @brief Struct containig Date information
 */
typedef rtc_time_date_t rtcTimeDate_t;

API_FUNC uint64_t ytTimeGetTimestamp(void);
API_FUNC retval_t ytTimeSetTimestamp(uint64_t new_timestamp);
API_FUNC retval_t ytTimeGetDate(rtc_time_date_t* rtc_time_date);
API_FUNC retval_t ytTimeSetDate(rtc_time_date_t* rtc_time_date);


typedef time_meas_t ytTimeMeas_t;

API_FUNC retval_t ytStartTimeMeasure(ytTimeMeas_t* time_meas);
API_FUNC retval_t ytStopTimeMeasure(ytTimeMeas_t* time_meas);

/* *********************************************************/

#include "system_api_func_def.h"


#endif /* APPLICATION_CORE_SYSTEM_API_H_ */
