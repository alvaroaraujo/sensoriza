/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * cerberus_phy.c
 *
 *  Created on: 19 de dic. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file cerberus_phy.c
 */

#include "phy_layer.h"
#include "mac_layer.h"
#include "null_phy.h"
#include "packetbuffer.h"
#include "spirit1_core.h"


#if ENABLE_NETSTACK_ARCH

#include "platform_cerberus_phy.h"

#define DEBUG 0
#if DEBUG
#include "yetimote_stdio.h"
#define PRINTF(...) _printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

#define SEND_PACKET_TIMEOUT		30		//Un paquete de 64 bytes no deberia tardar mas de 5ms en enviarse a 250 kbps

/* *********SPIRIT1 433 DEFAULT CONFIG ***********/
#define SPIRIT1_433_DEFAULT_XTAL		50e6

#define SPIRIT1_433_DEFAULT_BAUD_RATE	250e3	// 250 kbps
#define SPIRIT1_433_DEFAULT_SPACING		250e3	// 250 KHz
#define SPIRIT1_433_DEFAULT_FREQ_DEV	127e3	// 127 KHz
#define SPIRIT1_433_DEFAULT_MODULATION	GFSK_BT1
#define SPIRIT1_433_DEFAULT_FREQ		433.05e6	// 433.05 MHz
#define SPIRIT1_433_DEFAULT_OUT_POWER	11		// 11 dBm
#define SPIRIT1_433_DEFAULT_RX_BW		540e3	// 540 KHz
#define SPIRIT1_433_DEFAULT_CHANNEL		0

#define SPIRIT1_433_DEFAULT_PACKET_LENGTH	FIXED_PACKET_SIZE
/* *********SPIRIT1 433 DEFAULT CONFIG ***********/

#define PROCESS_CHECK_TIMEOUT			5000


typedef struct __packed cerberus_phy_layer_data_{
	spirit1_data_t* spirit1_433_data;
	uint32_t proc_semph_id;
	net_packet_t* sending_packets_buff[PACKET_BUFFER_SIZE];
	uint32_t send_timeout_timer;
	uint16_t num_sending_packets;
	uint16_t cerb_proc_id;
	uint16_t pending_int;
}cerberus_phy_layer_data_t;

/* ****************************************************/
static retval_t cerberus_phy_layer_init(phy_layer_data_t** phy_layer_data);
static retval_t cerberus_phy_layer_deinit(phy_layer_data_t* phy_layer_data);

static retval_t cerberus_phy_send_packet(phy_layer_data_t* phy_layer_data, net_packet_t* packet);

static retval_t cerberus_phy_send_packet_done(phy_layer_data_t* phy_layer_data, net_packet_t* packet);
static retval_t cerberus_phy_rcv_packet_done(phy_layer_data_t* phy_layer_data, net_packet_t* packet);

//Funciones llamadas inmediatamente. No son posteadas en modo evento. Se puede hacer as� ya que solo deber�an ser llamadas desde la capa MAC
static retval_t cerberus_phy_set_mode_receiving(phy_layer_data_t* phy_layer_data);
static retval_t cerberus_phy_set_mode_idle(phy_layer_data_t* phy_layer_data);
static retval_t cerberus_phy_set_mode_sleep(phy_layer_data_t* phy_layer_data);

static retval_t cerberus_phy_check_channel_rssi(phy_layer_data_t* phy_layer_data, float32_t* read_rssi);
static retval_t cerberus_phy_get_last_rssi(phy_layer_data_t* phy_layer_data, float32_t* read_rssi);

static retval_t cerberus_phy_set_base_freq(phy_layer_data_t* phy_layer_data, uint32_t base_freq);
static retval_t cerberus_phy_get_base_freq(phy_layer_data_t* phy_layer_data, uint32_t* base_freq);

static retval_t cerberus_phy_get_channel_num(phy_layer_data_t* phy_layer_data, uint32_t* channel_num);	//Returns the maximun available channels in a freq_base
static retval_t cerberus_phy_set_freq_channel(phy_layer_data_t* phy_layer_data, uint32_t channel_num);
static retval_t cerberus_phy_get_freq_channel(phy_layer_data_t* phy_layer_data, uint32_t* channel_num);	//Get the current freq channel

static retval_t cerberus_phy_set_baud_rate(phy_layer_data_t* phy_layer_data, uint32_t baud_rate);
static retval_t cerberus_phy_set_out_power(phy_layer_data_t* phy_layer_data, int16_t baud_rate);

static retval_t cerberus_phy_encrypt_packet(phy_layer_data_t* phy_layer_data, net_packet_t* packet, uint8_t* key);
static retval_t cerberus_phy_decrypt_packet(phy_layer_data_t* phy_layer_data, net_packet_t* packet, uint8_t* key);
/* ****************************************************/

/* ******Cerberus process Func*************************/
static void cerberus_process_func(const void* args);
/* ****************************************************/

/* ******Cerberus private Funcs*************************/
static retval_t cerberus_phy_send_next_packet(cerberus_phy_layer_data_t* phy_layer_data);
static void send_timeout_func_cb(void const * argument);
/* ****************************************************/

/* SPIRIT1 433 PRIVATE FUNCS*/
static retval_t spirit1_433_init(spirit1_data_t* spirit1_data, char* spi_dev);
static void spirit1_433_irq_cb(const void* args);
/* SPIRIT1 433 PRIVATE FUNCS*/


phy_layer_funcs_t cerberus_phy_funcs = {
		.phy_layer_init = cerberus_phy_layer_init,
		.phy_layer_deinit = cerberus_phy_layer_deinit,
		.phy_send_packet = cerberus_phy_send_packet,
		.phy_send_packet_done = cerberus_phy_send_packet_done,
		.phy_rcv_packet_done = cerberus_phy_rcv_packet_done,
		.phy_set_mode_receiving = cerberus_phy_set_mode_receiving,
		.phy_set_mode_idle = cerberus_phy_set_mode_idle,
		.phy_set_mode_sleep = cerberus_phy_set_mode_sleep,
		.phy_check_channel_rssi = cerberus_phy_check_channel_rssi,
		.phy_get_last_rssi = cerberus_phy_get_last_rssi,
		.phy_set_base_freq = cerberus_phy_set_base_freq,
		.phy_get_base_freq = cerberus_phy_get_base_freq,
		.phy_get_channel_num = cerberus_phy_get_channel_num,
		.phy_set_freq_channel = cerberus_phy_set_freq_channel,
		.phy_get_freq_channel = cerberus_phy_get_freq_channel,
		.phy_set_baud_rate = cerberus_phy_set_baud_rate,
		.phy_set_out_power = cerberus_phy_set_out_power,
		.phy_encrypt_packet = cerberus_phy_encrypt_packet,
		.phy_decrypt_packet = cerberus_phy_decrypt_packet,
};


/**
 *
 * @return
 */
static retval_t cerberus_phy_layer_init(phy_layer_data_t** phy_layer_data){
	cerberus_phy_layer_data_t* new_data;

	if(phy_layer_data == NULL){
		return RET_ERROR;
	}
	new_data = (cerberus_phy_layer_data_t*) ytMalloc(sizeof(cerberus_phy_layer_data_t));

	if((new_data->spirit1_433_data = new_spirit1_data(SPIRIT1_433_CS_PIN, SPIRIT1_433_SDN_PIN, SPIRIT1_433_GPIO3_PIN, spirit1_433_irq_cb, new_data)) == NULL){
		ytFree(new_data);
		return RET_ERROR;
	}

	if(spirit1_433_init(new_data->spirit1_433_data, CERBERUS_SPI_DEV) != RET_OK){
		delete_spirit1_data(new_data->spirit1_433_data);
		ytFree(new_data);
		return RET_ERROR;
	}

	new_data->num_sending_packets = 0;
	new_data->pending_int = 0;
	new_data->proc_semph_id = ytSemaphoreCreate(1);
	new_data->send_timeout_timer = ytTimerCreate(ytTimerOnce, send_timeout_func_cb, (void*) new_data);
	ytSemaphoreWait(new_data->proc_semph_id, YT_WAIT_FOREVER);
	ytStartProcess("cerb_phy_proc", cerberus_process_func, HIGH_PRIORITY_PROCESS, 200, &(new_data->cerb_proc_id), new_data);

	(*phy_layer_data) = (phy_layer_data_t*) new_data;
	return RET_OK;;
}
/**
 *
 * @param phy_layer_data
 * @return
 */
static retval_t cerberus_phy_layer_deinit(phy_layer_data_t* phy_layer_data){
	cerberus_phy_layer_data_t* phy_data = (cerberus_phy_layer_data_t*) phy_layer_data;
	if(phy_layer_data == NULL){
		return RET_ERROR;
	}
	ytExitProcess(phy_data->cerb_proc_id);

	if(spirit1_hw_deinit(phy_data->spirit1_433_data) != RET_OK){
		return RET_ERROR;
	}
	if(delete_spirit1_data(phy_data->spirit1_433_data) != RET_OK){
		return RET_ERROR;
	}
	ytFree(phy_data);

	return RET_OK;
}

/**
 *
 * @param phy_layer_data
 * @param packet
 * @return
 */
static retval_t cerberus_phy_send_packet(phy_layer_data_t* phy_layer_data, net_packet_t* packet){
	cerberus_phy_layer_data_t* phy_data = (cerberus_phy_layer_data_t*) phy_layer_data;
	PRINTF("CERBERUS: SENDING\r\n");

	if(phy_data->num_sending_packets >= PACKET_BUFFER_SIZE){
		phy_post_event_packet_sent(packet);
		return RET_ERROR;
	}
	phy_data->sending_packets_buff[phy_data->num_sending_packets] = packet;
	phy_data->num_sending_packets++;
	if(phy_data->num_sending_packets <= 1){		//Si no hay paquetes enviandose, envio ahora. Sino enviare mas tarde
		return cerberus_phy_send_next_packet(phy_data);
	}

	return RET_OK;
}

/**
 *
 * @param phy_layer_data
 * @param packet
 * @return
 */
static retval_t cerberus_phy_send_packet_done(phy_layer_data_t* phy_layer_data, net_packet_t* packet){

	PRINTF("CERBERUS: SEND DONE\r\n");
	mac_packet_sent(packet);
	return RET_OK;
}

/**
 *
 * @param phy_layer_data
 * @param packet
 * @return
 */
static retval_t cerberus_phy_rcv_packet_done(phy_layer_data_t* phy_layer_data, net_packet_t* packet){
	float32_t rssi_val;

	cerberus_phy_get_last_rssi(phy_layer_data, &rssi_val);
	PRINTF("RSSI: %.2f\r\n", rssi_val);
	mac_packet_received(packet);		//Sigo en estado rx ya que la recepcion es persistente. Para salir de RX hay que hacerlo explicitamente llamando a mode_idle o mode_sleep
	return RET_OK;
}

//Funciones llamadas inmediatamente. No son posteadas en modo evento. Se puede hacer as� ya que solo deber�an ser llamadas desde la capa MAC
/**
 *
 * @param phy_layer_data
 * @return
 */
static retval_t cerberus_phy_set_mode_receiving(phy_layer_data_t* phy_layer_data){
	cerberus_phy_layer_data_t* phy_data = (cerberus_phy_layer_data_t*) phy_layer_data;

	if(phy_layer_data == NULL){
		return RET_ERROR;
	}
	return spirit1_set_mode_rx(phy_data->spirit1_433_data);

}
/**
 *
 * @param phy_layer_data
 * @return
 */
static retval_t cerberus_phy_set_mode_idle(phy_layer_data_t* phy_layer_data){
	cerberus_phy_layer_data_t* phy_data = (cerberus_phy_layer_data_t*) phy_layer_data;
	if(phy_layer_data == NULL){
		return RET_ERROR;
	}
	return spirit1_set_mode_idle(phy_data->spirit1_433_data);
}
/**
 *
 * @param phy_layer_data
 * @return
 */
static retval_t cerberus_phy_set_mode_sleep(phy_layer_data_t* phy_layer_data){
	cerberus_phy_layer_data_t* phy_data = (cerberus_phy_layer_data_t*) phy_layer_data;

	if(phy_layer_data == NULL){
		return RET_ERROR;
	}

	return spirit1_set_mode_sleep(phy_data->spirit1_433_data);
}

/**
 *
 * @param phy_layer_data
 * @param read_rssi
 * @return
 */
static retval_t cerberus_phy_check_channel_rssi(phy_layer_data_t* phy_layer_data, float32_t* read_rssi){
	cerberus_phy_layer_data_t* phy_data = (cerberus_phy_layer_data_t*) phy_layer_data;
	if(phy_layer_data == NULL){
		return RET_ERROR;
	}

	(*read_rssi) = spirit1_check_channel_rssi(phy_data->spirit1_433_data);

	return RET_OK;
}

/**
 *
 * @param phy_layer_data
 * @param read_rssi
 * @return
 */
static retval_t cerberus_phy_get_last_rssi(phy_layer_data_t* phy_layer_data, float32_t* read_rssi){
	cerberus_phy_layer_data_t* phy_data = (cerberus_phy_layer_data_t*) phy_layer_data;
	if(phy_layer_data == NULL){
		return RET_ERROR;
	}

	(*read_rssi) = spirit1_get_last_rssi(phy_data->spirit1_433_data);

	return RET_OK;

}
/**
 *
 * @param phy_layer_data
 * @param base_freq
 * @return
 */
static retval_t cerberus_phy_set_base_freq(phy_layer_data_t* phy_layer_data, uint32_t base_freq){
	return RET_ERROR;
}
/**
 *
 * @param phy_layer_data
 * @param base_freq
 * @return
 */
static retval_t cerberus_phy_get_base_freq(phy_layer_data_t* phy_layer_data, uint32_t* base_freq){
	return RET_ERROR;
}
/**
 *
 * @param phy_layer_data
 * @param base_freq
 * @param channel_num
 * @return
 */
static retval_t cerberus_phy_get_channel_num(phy_layer_data_t* phy_layer_data, uint32_t* channel_num){
	return RET_ERROR;
}
/**
 *
 * @param phy_layer_data
 * @param channel_num
 * @return
 */
static retval_t cerberus_phy_set_freq_channel(phy_layer_data_t* phy_layer_data, uint32_t channel_num){
	return RET_ERROR;
}
/**
 *
 * @param phy_layer_data
 * @param channel_num
 * @return
 */
static retval_t cerberus_phy_get_freq_channel(phy_layer_data_t* phy_layer_data, uint32_t* channel_num){
	return RET_ERROR;
}
/**
 *
 * @param phy_layer_data
 * @param baud_rate
 * @return
 */
static retval_t cerberus_phy_set_baud_rate(phy_layer_data_t* phy_layer_data, uint32_t baud_rate){
	return RET_ERROR;
}

/**
 *
 * @param phy_layer_data
 * @param baud_rate
 * @return
 */
static retval_t cerberus_phy_set_out_power(phy_layer_data_t* phy_layer_data, int16_t baud_rate){
	return RET_ERROR;
}

/**
 *
 * @param phy_layer_data
 * @param packet
 * @param key
 * @return
 */
static retval_t cerberus_phy_encrypt_packet(phy_layer_data_t* phy_layer_data, net_packet_t* packet, uint8_t* key){
	cerberus_phy_layer_data_t* phy_data = (cerberus_phy_layer_data_t*) phy_layer_data;
	if(phy_layer_data == NULL){
		return RET_ERROR;
	}

	return spirit1_aes_encrypt_data(phy_data->spirit1_433_data, (uint8_t*) packet, key, FIXED_PACKET_SIZE);
}

/**
 *
 * @param phy_layer_data
 * @param packet
 * @param key
 * @return
 */
static retval_t cerberus_phy_decrypt_packet(phy_layer_data_t* phy_layer_data, net_packet_t* packet, uint8_t* key){
	cerberus_phy_layer_data_t* phy_data = (cerberus_phy_layer_data_t*) phy_layer_data;
	if(phy_layer_data == NULL){
		return RET_ERROR;
	}

	return spirit1_aes_decrypt_data(phy_data->spirit1_433_data, (uint8_t*) packet, key, FIXED_PACKET_SIZE);
}

/* ******Cerberus process Func*************************/
static void cerberus_process_func(const void* args){
	net_packet_t* rcv_packet, *pckt_sent;
	cerberus_phy_layer_data_t* phy_data = (cerberus_phy_layer_data_t*) args;
	uint16_t ret, i;

	while(1){
		if(!phy_data->pending_int){
			ytSemaphoreWait(phy_data->proc_semph_id, PROCESS_CHECK_TIMEOUT);
		}
		PRINTF("CERBERUS: CALLBACK ENTERED\r\n");
		phy_data->pending_int = 0;

		ret = spirit1_irq_routine(phy_data->spirit1_433_data);


		if(ret & RET_PCKT_SENT){
			if(phy_data->num_sending_packets){
				pckt_sent = phy_data->sending_packets_buff[0];
				for(i=0; i<phy_data->num_sending_packets-1; i++){
					phy_data->sending_packets_buff[i] = phy_data->sending_packets_buff[i+1];	//Adelanto una posicion los paquetes que esten esperando a ser enviados
				}
				phy_data->num_sending_packets--;
				ytTimerStop(phy_data->send_timeout_timer);
				if(phy_data->num_sending_packets){
					cerberus_phy_send_next_packet(phy_data); //Si sigue habiendo paquetes listos para ser enviados, envio el siguiente
				}

				phy_post_event_packet_sent(pckt_sent);
			}

		}
		if(ret & RET_PCKT_RCV){
			if((rcv_packet = rx_packetbuffer_get_free_packet()) != NULL){
				spirit1_read_rcv_data(phy_data->spirit1_433_data, (uint8_t*) rcv_packet, FIXED_PACKET_SIZE);
				rcv_packet->pckt_rssi = spirit1_get_last_rssi(phy_data->spirit1_433_data);
				phy_post_event_packet_received(rcv_packet);
			}
			else{
				spirit1_flush_last_rcv_data(phy_data->spirit1_433_data, FIXED_PACKET_SIZE);	//Descarto el paquete que me ha llegado si no hay hueco en el packetbuff
			}
		}


	}
}
/* ****************************************************/


/**
 *
 * @param phy_data
 * @return
 */
static retval_t cerberus_phy_send_next_packet(cerberus_phy_layer_data_t* phy_data){
	if(!phy_data->num_sending_packets){
		return RET_ERROR;
	}
	if(spirit1_send_data(phy_data->spirit1_433_data, (uint8_t*) phy_data->sending_packets_buff[0], FIXED_PACKET_SIZE) != RET_OK){	//Envio siempre el primero guardado
		ytTimerStart(phy_data->send_timeout_timer, SEND_PACKET_TIMEOUT);
		return RET_ERROR;
	}
	ytTimerStart(phy_data->send_timeout_timer, SEND_PACKET_TIMEOUT);
	return RET_OK;
}

/**
 *
 * @param spirit1_data
 * @return
 */
static retval_t spirit1_433_init(spirit1_data_t* spirit1_data, char* spi_dev){
	spirit1_config_t* spirit1_init_config;
	spirit1_init_config = (spirit1_config_t*) ytMalloc(sizeof(spirit1_config_t));

	spirit1_init_config->baud_rate = SPIRIT1_433_DEFAULT_BAUD_RATE;
	spirit1_init_config->channel_num = SPIRIT1_433_DEFAULT_CHANNEL;
	spirit1_init_config->channel_spacing = SPIRIT1_433_DEFAULT_SPACING;
	spirit1_init_config->freq_deviation = SPIRIT1_433_DEFAULT_FREQ_DEV;
	spirit1_init_config->modulation = SPIRIT1_433_DEFAULT_MODULATION;
	spirit1_init_config->modulation_freq = SPIRIT1_433_DEFAULT_FREQ;
	spirit1_init_config->output_power = SPIRIT1_433_DEFAULT_OUT_POWER;
	spirit1_init_config->packet_length = SPIRIT1_433_DEFAULT_PACKET_LENGTH;
	spirit1_init_config->rx_bandwidth = SPIRIT1_433_DEFAULT_RX_BW;
	spirit1_init_config->xtal_freq = SPIRIT1_433_DEFAULT_XTAL;

	if(spirit1_hw_init(spirit1_data, spirit1_init_config, spi_dev) != RET_OK){

		ytFree(spirit1_init_config);
		return RET_ERROR;
	}

	ytFree(spirit1_init_config);
	return RET_OK;
}

/**
 *
 * @param argument
 */
static void send_timeout_func_cb(void const * argument){
	uint16_t i;
	cerberus_phy_layer_data_t* phy_data = (cerberus_phy_layer_data_t*) argument;
	ytTimerStop(phy_data->send_timeout_timer);

	if(phy_data->num_sending_packets){
		tx_packetbuffer_release_packet(phy_data->sending_packets_buff[0]);
		for(i=0; i<phy_data->num_sending_packets-1; i++){
			phy_data->sending_packets_buff[i] = phy_data->sending_packets_buff[i+1];	//Adelanto una posicion los paquetes que esten esperando a ser enviados
		}
		phy_data->num_sending_packets--;
		if(phy_data->num_sending_packets){
			cerberus_phy_send_next_packet(phy_data); //Si sigue habiendo paquetes listos para ser enviados, envio el siguiente
		}
	}

}

/**
 *
 * @param args
 */
static void spirit1_433_irq_cb(const void* args){

	cerberus_phy_layer_data_t* phy_data = (cerberus_phy_layer_data_t*) args;

	phy_data->pending_int++;
	ytSemaphoreRelease(phy_data->proc_semph_id);

}
#endif
