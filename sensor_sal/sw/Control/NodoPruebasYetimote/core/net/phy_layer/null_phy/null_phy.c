/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * null_phy.c
 *
 *  Created on: 28 de nov. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file null_phy.c
 */

#include "phy_layer.h"
#include "mac_layer.h"
#include "null_phy.h"
#include "packetbuffer.h"


#if ENABLE_NETSTACK_ARCH

#define TEST_TIMER_TIME	2000

typedef struct __packed null_phy_layer_data_{
	uint32_t phy_timer_test_id;
}null_phy_layer_data_t;



static retval_t null_phy_layer_init(phy_layer_data_t** phy_layer_data);
static retval_t null_phy_layer_deinit(phy_layer_data_t* phy_layer_data);

static retval_t null_phy_send_packet(phy_layer_data_t* phy_layer_data, net_packet_t* packet);

static retval_t null_phy_send_packet_done(phy_layer_data_t* phy_layer_data, net_packet_t* packet);
static retval_t null_phy_rcv_packet_done(phy_layer_data_t* phy_layer_data, net_packet_t* packet);

//Funciones llamadas inmediatamente. No son posteadas en modo evento. Se puede hacer as� ya que solo deber�an ser llamadas desde la capa MAC
static retval_t null_phy_set_mode_receiving(phy_layer_data_t* phy_layer_data);
static retval_t null_phy_set_mode_idle(phy_layer_data_t* phy_layer_data);
static retval_t null_phy_set_mode_sleep(phy_layer_data_t* phy_layer_data);

static retval_t null_phy_set_base_freq(phy_layer_data_t* phy_layer_data, uint32_t base_freq);
static retval_t null_phy_get_base_freq(phy_layer_data_t* phy_layer_data, uint32_t* base_freq);

static retval_t null_phy_get_channel_num(phy_layer_data_t* phy_layer_data, uint32_t* channel_num);	//Returns the maximun available channels in a freq_base
static retval_t null_phy_set_freq_channel(phy_layer_data_t* phy_layer_data, uint32_t channel_num);
static retval_t null_phy_get_freq_channel(phy_layer_data_t* phy_layer_data, uint32_t* channel_num);	//Get the current freq channel

static retval_t null_phy_set_baud_rate(phy_layer_data_t* phy_layer_data, uint32_t baud_rate);
static retval_t null_phy_set_out_power(phy_layer_data_t* phy_layer_data, int16_t baud_rate);

static retval_t null_phy_encrypt_packet(phy_layer_data_t* phy_layer_data, net_packet_t* packet, uint8_t* key);
static retval_t null_phy_decrypt_packet(phy_layer_data_t* phy_layer_data, net_packet_t* packet, uint8_t* key);

static void null_phy_test_timer_callback(const void* args);


phy_layer_funcs_t null_phy_funcs = {
		.phy_layer_init = null_phy_layer_init,
		.phy_layer_deinit = null_phy_layer_deinit,
		.phy_send_packet = null_phy_send_packet,
		.phy_send_packet_done = null_phy_send_packet_done,
		.phy_rcv_packet_done = null_phy_rcv_packet_done,
		.phy_set_mode_receiving = null_phy_set_mode_receiving,
		.phy_set_mode_idle = null_phy_set_mode_idle,
		.phy_set_mode_sleep = null_phy_set_mode_sleep,
		.phy_set_base_freq = null_phy_set_base_freq,
		.phy_get_base_freq = null_phy_get_base_freq,
		.phy_get_channel_num = null_phy_get_channel_num,
		.phy_set_freq_channel = null_phy_set_freq_channel,
		.phy_get_freq_channel = null_phy_get_freq_channel,
		.phy_set_baud_rate = null_phy_set_baud_rate,
		.phy_set_out_power = null_phy_set_out_power,
		.phy_encrypt_packet = null_phy_encrypt_packet,
		.phy_decrypt_packet = null_phy_decrypt_packet,
};


/**
 *
 * @return
 */
static retval_t null_phy_layer_init(phy_layer_data_t** phy_layer_data){
	null_phy_layer_data_t* new_data;
	if(phy_layer_data == NULL){
		return RET_ERROR;
	}
	new_data = (null_phy_layer_data_t*) ytMalloc(sizeof(null_phy_layer_data_t));
	new_data->phy_timer_test_id = ytTimerCreate(ytTimerOnce, null_phy_test_timer_callback, (void*) new_data);
	(*phy_layer_data) = (phy_layer_data_t*) new_data;
	return RET_OK;;
}
/**
 *
 * @param phy_layer_data
 * @return
 */
static retval_t null_phy_layer_deinit(phy_layer_data_t* phy_layer_data){
	null_phy_layer_data_t* phy_data = (null_phy_layer_data_t*) phy_layer_data;
	if(phy_layer_data == NULL){
		return RET_ERROR;
	}
	ytTimerDelete(phy_data->phy_timer_test_id);
	ytFree(phy_data);

	return RET_OK;
}

/**
 *
 * @param phy_layer_data
 * @param packet
 * @return
 */
static retval_t null_phy_send_packet(phy_layer_data_t* phy_layer_data, net_packet_t* packet){
//	ytStdoutSend((uint8_t*) packet->data, PACKET_DATA_SIZE);
//	ytPrintf("\r\n");
	phy_post_event_packet_sent(packet);
	return RET_OK;
}

/**
 *
 * @param phy_layer_data
 * @param packet
 * @return
 */
static retval_t null_phy_send_packet_done(phy_layer_data_t* phy_layer_data, net_packet_t* packet){
	mac_packet_sent(packet);
	return RET_OK;
}

/**
 *
 * @param phy_layer_data
 * @param packet
 * @return
 */
static retval_t null_phy_rcv_packet_done(phy_layer_data_t* phy_layer_data, net_packet_t* packet){
	mac_packet_received(packet);
	return RET_OK;
}

//Funciones llamadas inmediatamente. No son posteadas en modo evento. Se puede hacer as� ya que solo deber�an ser llamadas desde la capa MAC
/**
 *
 * @param phy_layer_data
 * @return
 */
static retval_t null_phy_set_mode_receiving(phy_layer_data_t* phy_layer_data){
	null_phy_layer_data_t* phy_data = (null_phy_layer_data_t*) phy_layer_data;
	if(phy_layer_data == NULL){
		return RET_ERROR;
	}
	ytTimerStart(phy_data->phy_timer_test_id, TEST_TIMER_TIME);

	return RET_OK;
}
/**
 *
 * @param phy_layer_data
 * @return
 */
static retval_t null_phy_set_mode_idle(phy_layer_data_t* phy_layer_data){
	null_phy_layer_data_t* phy_data = (null_phy_layer_data_t*) phy_layer_data;
	if(phy_layer_data == NULL){
		return RET_ERROR;
	}
	ytTimerStop(phy_data->phy_timer_test_id);

	return RET_OK;
}
/**
 *
 * @param phy_layer_data
 * @return
 */
static retval_t null_phy_set_mode_sleep(phy_layer_data_t* phy_layer_data){
	null_phy_layer_data_t* phy_data = (null_phy_layer_data_t*) phy_layer_data;
	if(phy_layer_data == NULL){
		return RET_ERROR;
	}
	ytTimerStop(phy_data->phy_timer_test_id);

	return RET_OK;
}
/**
 *
 * @param phy_layer_data
 * @param base_freq
 * @return
 */
static retval_t null_phy_set_base_freq(phy_layer_data_t* phy_layer_data, uint32_t base_freq){
	return RET_ERROR;
}
/**
 *
 * @param phy_layer_data
 * @param base_freq
 * @return
 */
static retval_t null_phy_get_base_freq(phy_layer_data_t* phy_layer_data, uint32_t* base_freq){
	return RET_ERROR;
}
/**
 *
 * @param phy_layer_data
 * @param base_freq
 * @param channel_num
 * @return
 */
static retval_t null_phy_get_channel_num(phy_layer_data_t* phy_layer_data, uint32_t* channel_num){
	return RET_ERROR;
}
/**
 *
 * @param phy_layer_data
 * @param channel_num
 * @return
 */
static retval_t null_phy_set_freq_channel(phy_layer_data_t* phy_layer_data, uint32_t channel_num){
	return RET_ERROR;
}
/**
 *
 * @param phy_layer_data
 * @param channel_num
 * @return
 */
static retval_t null_phy_get_freq_channel(phy_layer_data_t* phy_layer_data, uint32_t* channel_num){
	return RET_ERROR;
}
/**
 *
 * @param phy_layer_data
 * @param baud_rate
 * @return
 */
static retval_t null_phy_set_baud_rate(phy_layer_data_t* phy_layer_data, uint32_t baud_rate){
	return RET_ERROR;
}

/**
 *
 * @param phy_layer_data
 * @param baud_rate
 * @return
 */
static retval_t null_phy_set_out_power(phy_layer_data_t* phy_layer_data, int16_t baud_rate){
	return RET_ERROR;
}

/**
 *
 * @param phy_layer_data
 * @param packet
 * @param key
 * @return
 */
static retval_t null_phy_encrypt_packet(phy_layer_data_t* phy_layer_data, net_packet_t* packet, uint8_t* key){
	return RET_ERROR;
}

/**
 *
 * @param phy_layer_data
 * @param packet
 * @param key
 * @return
 */
static retval_t null_phy_decrypt_packet(phy_layer_data_t* phy_layer_data, net_packet_t* packet, uint8_t* key){
	return RET_ERROR;
}

/**
 *
 * @param args
 */
static void null_phy_test_timer_callback(const void* args){
	net_packet_t*  packet =  rx_packetbuffer_get_free_packet();
	sprintf((char*)packet->data,"RCV_PACKET\r\n");
	packet->header.tp_hdr.data_size = strlen("RCV_PACKET\r\n");
	packet->header.tp_hdr.port_num = 123;
	packet->header.mac_hdr.dest_mac_addr = 0xFFFF;
	packet->header.mac_hdr.src_mac_addr = 0xB105;
	phy_post_event_packet_received(packet);
}
#endif
