/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * phy_layer.c
 *
 *  Created on: 27 de nov. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file phy_layer.c
 */

#include "phy_layer.h"
#include "netstack.h"

#if ENABLE_NETSTACK_ARCH

#define DEBUG 0
#if DEBUG
#include "yetimote_stdio.h"
#define PRINTF(...) _printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

static retval_t packet_sent_event(void* args);
static retval_t packet_received_event(void* args);

/**
 *
 * @param phy_layer_funcs
 * @return
 */
phy_layer_t* init_phy_layer(phy_layer_funcs_t* phy_layer_funcs){
	phy_layer_t* new_phy_layer = ytMalloc(sizeof(phy_layer_t));

	if(new_phy_layer == NULL){
		return NULL;
	}

	new_phy_layer->phy_layer_funcs = phy_layer_funcs;
	new_phy_layer->phy_layer_private_data = NULL;

	PRINTF("PHY_LAYER: INIT\r\n");
	if(new_phy_layer->phy_layer_funcs->phy_layer_init(&(new_phy_layer->phy_layer_private_data)) != RET_OK){
		ytFree(new_phy_layer);
		PRINTF("PHY_LAYER: INIT ERROR\r\n");
		return NULL;
	}

	PRINTF("PHY_LAYER: INIT DONE\r\n");
	return new_phy_layer;
}
/**
 *
 * @param phy_layer
 * @return
 */
retval_t deinit_phy_layer(phy_layer_t* phy_layer){
	if(phy_layer == NULL){
		return RET_ERROR;
	}

	PRINTF("PHY_LAYER: DEINIT\r\n");

	phy_layer->phy_layer_funcs->phy_layer_deinit(phy_layer->phy_layer_private_data);

	ytFree(phy_layer);

	PRINTF("PHY_LAYER: DEINIT DONE\r\n");
	return RET_OK;
}


//* Called from mac layer
/**
 *
 * @param packet
 * @return
 */
retval_t phy_send_packet(net_packet_t* packet){
	phy_layer_t* phy_layer =  netstack_get_phy_layer();

	netstack_state = NS_PHY_SEND_PACKET;
	PRINTF("PHY_LAYER: EVENT SEND PACKET\r\n");
	return phy_layer->phy_layer_funcs->phy_send_packet(phy_layer->phy_layer_private_data, packet);

}

//* Called from phy layer
/**
 *
 * @param packet
 * @return
 */
retval_t phy_post_event_packet_sent(net_packet_t* packet){
	phy_layer_t* phy_layer =  netstack_get_phy_layer();

	phy_layer_func_args_t* phy_layer_args = (phy_layer_func_args_t*) ytMalloc(sizeof(phy_layer_func_args_t));

	phy_layer_args->phy_layer = phy_layer;
	phy_layer_args->packet = packet;

	PRINTF("PHY_LAYER: POST PACKET SENT DONE\r\n");
	netstack_post_event(packet_sent_event, phy_layer_args);

	return RET_OK;
}
/**
 *
 * @param packet
 * @return
 */
retval_t phy_post_event_packet_received(net_packet_t* packet){
	phy_layer_t* phy_layer =  netstack_get_phy_layer();

	phy_layer_func_args_t* phy_layer_args = (phy_layer_func_args_t*) ytMalloc(sizeof(phy_layer_func_args_t));

	phy_layer_args->phy_layer = phy_layer;
	phy_layer_args->packet = packet;

	PRINTF("PHY_LAYER: POST PACKET RECEIVED\r\n");
	netstack_post_event(packet_received_event, phy_layer_args);

	return RET_OK;
}


//* Called from mac layer. Inmediate functions: No event posted
/**
 *
 * @return
 */
retval_t phy_set_mode_receiving(void){
	phy_layer_t* phy_layer =  netstack_get_phy_layer();

	netstack_state = NS_PHY_SET_RECEIVING;
	PRINTF("PHY_LAYER: SET MODE RX\r\n");
	return phy_layer->phy_layer_funcs->phy_set_mode_receiving(phy_layer->phy_layer_private_data);
}
/**
 *
 * @return
 */
retval_t phy_set_mode_idle(void){
	phy_layer_t* phy_layer =  netstack_get_phy_layer();

	netstack_state = NS_PHY_SET_IDLE;
	PRINTF("PHY_LAYER: SET MODE IDLE\r\n");
	return phy_layer->phy_layer_funcs->phy_set_mode_idle(phy_layer->phy_layer_private_data);
}
/**
 *
 * @return
 */
retval_t phy_set_mode_sleep(void){
	phy_layer_t* phy_layer =  netstack_get_phy_layer();

	netstack_state = NS_PHY_SET_SLEEP;
	PRINTF("PHY_LAYER: SET MODE SLEEP\r\n");
	return phy_layer->phy_layer_funcs->phy_set_mode_sleep(phy_layer->phy_layer_private_data);
}

/**
 *
 * @return
 */
float32_t phy_check_channel_rssi(void){
	float32_t ret_rssi;
	phy_layer_t* phy_layer =  netstack_get_phy_layer();

	netstack_state = NS_PHY_CHECK_RSSI;
	PRINTF("PHY_LAYER: SET MODE CHECK RSSI\r\n");
	phy_layer->phy_layer_funcs->phy_check_channel_rssi(phy_layer->phy_layer_private_data, &ret_rssi);

	return ret_rssi;

}

/**
 *
 * @return
 */
float32_t phy_get_last_rssi(void){

	float32_t ret_rssi;
	phy_layer_t* phy_layer =  netstack_get_phy_layer();

	phy_layer->phy_layer_funcs->phy_get_last_rssi(phy_layer->phy_layer_private_data, &ret_rssi);

	return ret_rssi;
}

/**
 *
 * @param base_freq
 * @return
 */
retval_t phy_set_base_freq(uint32_t base_freq){
	phy_layer_t* phy_layer =  netstack_get_phy_layer();

	return phy_layer->phy_layer_funcs->phy_set_base_freq(phy_layer->phy_layer_private_data, base_freq);
}
/**
 *
 * @return
 */
uint32_t phy_get_base_freq(void){
	uint32_t ret_base_freq;
	phy_layer_t* phy_layer =  netstack_get_phy_layer();
	if(phy_layer->phy_layer_funcs->phy_get_base_freq(phy_layer->phy_layer_private_data, &ret_base_freq) != RET_OK){
		return 0;
	}
	return ret_base_freq;
}

/**
 *
 * @param base_freq
 * @return
 */
uint32_t phy_get_channel_num(void){
	uint32_t ret_channel_num;
	phy_layer_t* phy_layer =  netstack_get_phy_layer();
	if(phy_layer->phy_layer_funcs->phy_get_channel_num(phy_layer->phy_layer_private_data, &ret_channel_num) != RET_OK){
		return 0xFFFFFFFF;
	}
	return ret_channel_num;
}
/**
 *
 * @param channel_num
 * @return
 */
retval_t phy_set_freq_channel(uint32_t channel_num){
	phy_layer_t* phy_layer =  netstack_get_phy_layer();

	return phy_layer->phy_layer_funcs->phy_set_freq_channel(phy_layer->phy_layer_private_data, channel_num);
}
/**
 *
 * @return
 */
uint32_t phy_get_freq_channel(void){
	uint32_t ret_channel;
	phy_layer_t* phy_layer =  netstack_get_phy_layer();
	if(phy_layer->phy_layer_funcs->phy_get_freq_channel(phy_layer->phy_layer_private_data, &ret_channel) != RET_OK){
		return 0;
	}
	return ret_channel;
}

/**
 *
 * @param baud_rate
 * @return
 */
retval_t phy_set_baud_rate(uint32_t baud_rate){
	phy_layer_t* phy_layer =  netstack_get_phy_layer();

	return phy_layer->phy_layer_funcs->phy_set_baud_rate(phy_layer->phy_layer_private_data, baud_rate);
}

/**
 *
 * @param out_power
 * @return
 */
retval_t phy_set_out_power(int16_t out_power){
	phy_layer_t* phy_layer =  netstack_get_phy_layer();

	return phy_layer->phy_layer_funcs->phy_set_out_power(phy_layer->phy_layer_private_data, out_power);
}

/**
 *
 * @param phy_layer_data
 * @param packet
 * @param key
 * @return
 */
retval_t phy_encrypt_packet(phy_layer_data_t* phy_layer_data, net_packet_t* packet, uint8_t* key){
	phy_layer_t* phy_layer =  netstack_get_phy_layer();

	return phy_layer->phy_layer_funcs->phy_encrypt_packet(phy_layer->phy_layer_private_data, packet, key);
}

/**
 *
 * @param phy_layer_data
 * @param packet
 * @param key
 * @return
 */
retval_t phy_decrypt_packet(phy_layer_data_t* phy_layer_data, net_packet_t* packet, uint8_t* key){
	phy_layer_t* phy_layer =  netstack_get_phy_layer();

	return phy_layer->phy_layer_funcs->phy_decrypt_packet(phy_layer->phy_layer_private_data, packet, key);
}

/**
 *
 * @param args
 * @return
 */
static retval_t packet_sent_event(void* args){
	phy_layer_func_args_t* phy_layer_func_args;
	phy_layer_t* phy_layer;
	if(args == NULL){
		return RET_ERROR;
	}
	phy_layer_func_args = (phy_layer_func_args_t*) args;
	phy_layer = phy_layer_func_args->phy_layer;

	netstack_state = NS_PHY_PACKET_SENT;
	PRINTF("PHY_LAYER: EVENT SEND PACKET DONE\r\n");
	phy_layer->phy_layer_funcs->phy_send_packet_done(phy_layer->phy_layer_private_data, phy_layer_func_args->packet);

	ytFree(phy_layer_func_args);
	return RET_OK;
}

/**
 *
 * @param args
 * @return
 */
static retval_t packet_received_event(void* args){
	phy_layer_func_args_t* phy_layer_func_args;
	phy_layer_t* phy_layer;
	if(args == NULL){
		return RET_ERROR;
	}
	phy_layer_func_args = (phy_layer_func_args_t*) args;
	phy_layer = phy_layer_func_args->phy_layer;

	netstack_state = NS_PHY_PACKET_RECEIVED;
	PRINTF("PHY_LAYER: EVENT RCV PACKET DONE\r\n");
	phy_layer->phy_layer_funcs->phy_rcv_packet_done(phy_layer->phy_layer_private_data, phy_layer_func_args->packet);

	ytFree(phy_layer_func_args);
	return RET_OK;
}

#endif

