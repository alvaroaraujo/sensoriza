/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * null_mac.c
 *
 *  Created on: 28 de nov. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file null_mac.c
 */

#include "mac_layer.h"
#include "phy_layer.h"
#include "routing_layer.h"
#include "null_mac.h"
#include "packetbuffer.h"

#if ENABLE_NETSTACK_ARCH

#define DEFAULT_MAC_ADDR	0xb105
#define DEFAULT_MAC_BC_ADDR 0xFFFF

#define RSSI_THR		-70
#define NUM_RSSI_CHECKS	3

typedef struct __packed null_mac_addr_{
	uint16_t mac_val;
}null_mac_addr_t;

//typedef enum null_mac_state_{
//	NULL_MAC_STATE_RX,
//	NULL_MAC_STATE_TX,
//	NULL_MAC_STATE_TX_RX,	//Este estado significa que se ha dado orden de escuchar y enviar, pero a nivel fisico no se puede hacer ambas a la vez
//	NULL_MAC_STATE_IDLE,
//}null_mac_state_t;

typedef struct __packed null_mac_layer_data_{
	null_mac_addr_t node_mac_addr;
//	null_mac_state_t mac_state;
//	uint16_t packets_to_send;
//	uint16_t packets_to_rcv;
}null_mac_layer_data_t;



static retval_t null_mac_layer_init(mac_layer_data_t** mac_layer_data);
static retval_t null_mac_layer_deinit(mac_layer_data_t* mac_layer_data);

static retval_t null_mac_send_packet(mac_layer_data_t* mac_layer_data, mac_addr_t mac_addr_dest_fd, net_packet_t* packet);
static retval_t null_mac_rcv_packet(mac_layer_data_t* mac_layer_data);

static retval_t null_mac_send_packet_done(mac_layer_data_t* mac_layer_data, net_packet_t* packet);
static retval_t null_mac_rcv_packet_done(mac_layer_data_t* mac_layer_data, net_packet_t* packet);

static retval_t null_mac_set_addr(mac_layer_data_t* mac_layer_data, mac_addr_t mac_addr_fd);

static retval_t null_mac_new_addr(mac_layer_data_t* mac_layer_data, char* mac_str_val, char format, mac_addr_t* mac_addr_fd);
static retval_t null_mac_delete_addr(mac_layer_data_t* mac_layer_data, mac_addr_t mac_addr_fd);
static retval_t null_mac_addr_to_string(mac_layer_data_t* mac_layer_data, mac_addr_t mac_addr_fd, char* mac_str_val, char format);
static retval_t null_mac_addr_cpy(mac_layer_data_t* mac_layer_data, mac_addr_t dest_addr_fd, mac_addr_t from_addr_fd);
static retval_t null_mac_addr_cmp(mac_layer_data_t* mac_layer_data, mac_addr_t a_addr_fd, mac_addr_t b_addr_fd);

mac_layer_funcs_t null_mac_funcs = {
		.mac_layer_init = null_mac_layer_init,
		.mac_layer_deinit = null_mac_layer_deinit,
		.mac_send_packet = null_mac_send_packet,
		.mac_rcv_packet = null_mac_rcv_packet,
		.mac_send_packet_done = null_mac_send_packet_done,
		.mac_rcv_packet_done = null_mac_rcv_packet_done,
		.mac_set_addr = null_mac_set_addr,
		.mac_new_addr = null_mac_new_addr,
		.mac_delete_addr = null_mac_delete_addr,
		.mac_addr_to_string = null_mac_addr_to_string,
		.mac_addr_cpy = null_mac_addr_cpy,
		.mac_addr_cmp = null_mac_addr_cmp,
};


/**
 *
 * @return
 */
static retval_t null_mac_layer_init(mac_layer_data_t** mac_layer_data){
	if(mac_layer_data == NULL){
		return RET_ERROR;
	}
	null_mac_layer_data_t* new_data = (null_mac_layer_data_t*) ytMalloc(sizeof(null_mac_layer_data_t));
	new_data->node_mac_addr.mac_val = DEFAULT_MAC_ADDR;
//	new_data->mac_state = NULL_MAC_STATE_IDLE;
//	new_data->packets_to_send = 0;
//	new_data->packets_to_rcv = 0;
	(*mac_layer_data) = (mac_layer_data_t*) new_data;

	phy_set_mode_receiving();
	return RET_OK;
}

/**
 *
 * @param mac_layer_data
 * @return
 */
static retval_t null_mac_layer_deinit(mac_layer_data_t* mac_layer_data){
	null_mac_layer_data_t* null_mac_data = (null_mac_layer_data_t*) mac_layer_data;
	if(mac_layer_data == NULL){
		return RET_ERROR;
	}
	ytFree(null_mac_data);
	return RET_OK;
}

/**
 *
 * @param mac_layer_data
 * @param mac_addr_dest_fd
 * @param packet
 * @return
 */
static retval_t null_mac_send_packet(mac_layer_data_t* mac_layer_data, mac_addr_t mac_addr_dest_fd, net_packet_t* packet){
	null_mac_layer_data_t* null_mac_data = (null_mac_layer_data_t*) mac_layer_data;
	null_mac_addr_t*  dest_mac_addr = (null_mac_addr_t*) mac_addr_dest_fd;
//	uint16_t num_checks = 0;
	if(mac_layer_data == NULL){
		return RET_ERROR;
	}
//	if(null_mac_data->mac_state == NULL_MAC_STATE_RX){	//Si estoy recibiendo dejo de recibir y me pongo a transmitir
////		phy_set_mode_idle();
//		null_mac_data->mac_state = NULL_MAC_STATE_TX_RX;
//	}
//	else if(null_mac_data->mac_state == NULL_MAC_STATE_IDLE){
//		null_mac_data->mac_state = NULL_MAC_STATE_TX;
//	}
//	null_mac_data->packets_to_send++;
	packet->header.mac_hdr.dest_mac_addr = dest_mac_addr->mac_val;
	packet->header.mac_hdr.src_mac_addr = null_mac_data->node_mac_addr.mac_val;
//	while(phy_check_channel_rssi() > RSSI_THR){
//		num_checks++;
//		if(num_checks >= NUM_RSSI_CHECKS){
//			break;
//		}
//		ytDelay(2);
//	}


	return phy_send_packet(packet);

}

/**
 *
 * @param mac_layer_data
 * @return
 */
static retval_t null_mac_rcv_packet(mac_layer_data_t* mac_layer_data){
	if(mac_layer_data == NULL){
		return RET_ERROR;
	}

	phy_set_mode_receiving();
	return RET_OK;
}

/**
 *
 * @param mac_layer_data
 * @param packet
 * @return
 */
static retval_t null_mac_send_packet_done(mac_layer_data_t* mac_layer_data, net_packet_t* packet){
	if(mac_layer_data == NULL){
		return RET_ERROR;
	}
	phy_set_mode_receiving();
	rt_packet_sent(packet);
	return RET_OK;
}

/**
 *
 * @param mac_layer_data
 * @param packet
 * @return
 */
static retval_t null_mac_rcv_packet_done(mac_layer_data_t* mac_layer_data, net_packet_t* packet){
	null_mac_layer_data_t* null_mac_data = (null_mac_layer_data_t*) mac_layer_data;
	null_mac_addr_t* null_mac_addr;
	if(mac_layer_data == NULL){
		return RET_ERROR;
	}
	phy_set_mode_receiving();

	if((packet->header.mac_hdr.dest_mac_addr == DEFAULT_MAC_BC_ADDR) || (packet->header.mac_hdr.dest_mac_addr == null_mac_data->node_mac_addr.mac_val)){
//		if(null_mac_data->packets_to_rcv){
//			null_mac_data->packets_to_rcv--;
//		}
//		if(!null_mac_data->packets_to_rcv){
//			if (null_mac_data->mac_state == NULL_MAC_STATE_TX_RX){
//				null_mac_data->mac_state = NULL_MAC_STATE_TX;
//			}
//			if (null_mac_data->mac_state == NULL_MAC_STATE_RX){
//				null_mac_data->mac_state = NULL_MAC_STATE_IDLE;
//				phy_set_mode_sleep();
//			}
//		}

		null_mac_addr = (null_mac_addr_t*) ytMalloc(sizeof(null_mac_addr_t));
		null_mac_addr->mac_val = packet->header.mac_hdr.src_mac_addr;
		rt_packet_received(packet, (mac_addr_t) null_mac_addr);

	}
	else{	//No es para mi el paquete
		return rx_packetbuffer_release_packet(packet);
	}


	return RET_OK;
}

/**
 *
 * @param mac_layer_data
 * @param mac_addr_fd
 * @return
 */
static retval_t null_mac_set_addr(mac_layer_data_t* mac_layer_data, mac_addr_t mac_addr_fd){
	null_mac_layer_data_t* null_mac_data = (null_mac_layer_data_t*) mac_layer_data;
	null_mac_addr_t*  dest_mac_addr = (null_mac_addr_t*) mac_addr_fd;
	if(mac_layer_data == NULL){
		return RET_ERROR;
	}
	null_mac_data->node_mac_addr.mac_val = dest_mac_addr->mac_val;

	return RET_OK;
}

/**
 *
 * @param mac_layer_data
 * @param mac_str_val
 * @param mac_addr_fd
 * @return
 */
static retval_t null_mac_new_addr(mac_layer_data_t* mac_layer_data, char* mac_str_val, char format, mac_addr_t* mac_addr_fd){
	null_mac_addr_t* null_mac_addr;
	int32_t addr_val;
	(*mac_addr_fd) = NULL;
	if(mac_addr_fd == NULL){
		return RET_ERROR;
	}
	if((mac_str_val == NULL) && (format == 0)){
		null_mac_addr = (null_mac_addr_t*) ytMalloc(sizeof(null_mac_addr_t));

		null_mac_addr->mac_val = 0;
		(*mac_addr_fd) = (mac_addr_t) null_mac_addr;

	}
	else{
		if((format != 'D') && (format != 'd')){
			return RET_ERROR;
		}
		if(strlen(mac_str_val)>5){	//Max addr value 65535
			return RET_ERROR;
		}
		addr_val = atoi(mac_str_val);
		if(addr_val <= 0){
			return RET_ERROR;
		}
		addr_val &= 0x0000FFFF;

		null_mac_addr = (null_mac_addr_t*) ytMalloc(sizeof(null_mac_addr_t));

		null_mac_addr->mac_val = (uint16_t) addr_val;

		(*mac_addr_fd) = (mac_addr_t) null_mac_addr;


	}
	return RET_OK;
}

/**
 *
 * @param mac_layer_data
 * @param mac_addr_fd
 * @return
 */
static retval_t null_mac_delete_addr(mac_layer_data_t* mac_layer_data, mac_addr_t mac_addr_fd){
	if(mac_addr_fd == NULL){
		return RET_ERROR;
	}
	ytFree(mac_addr_fd);
	return RET_OK;
}

/**
 *
 * @param mac_layer_data
 * @param mac_addr_fd
 * @param mac_str_val
 * @return
 */
static retval_t null_mac_addr_to_string(mac_layer_data_t* mac_layer_data, mac_addr_t mac_addr_fd, char* mac_str_val, char format){
	null_mac_addr_t* null_mac_addr = (null_mac_addr_t*) mac_addr_fd;
	if((mac_addr_fd == NULL) || (mac_str_val == NULL)){
		return RET_ERROR;
	}
	if((format != 'D') && (format != 'd')){
		return RET_ERROR;
	}
	sprintf(mac_str_val, "%d", null_mac_addr->mac_val);
	return RET_OK;
}

/**
 *
 * @param mac_layer_data
 * @param dest_addr_fd
 * @param from_addr_fd
 * @return
 */
static retval_t null_mac_addr_cpy(mac_layer_data_t* mac_layer_data, mac_addr_t dest_addr_fd, mac_addr_t from_addr_fd){
	null_mac_addr_t* null_mac_addr_dest = (null_mac_addr_t*) dest_addr_fd;
	null_mac_addr_t* null_mac_addr_from = (null_mac_addr_t*) from_addr_fd;
	if((dest_addr_fd == NULL) || (from_addr_fd == NULL)){
		return RET_ERROR;
	}
	null_mac_addr_dest->mac_val = null_mac_addr_from->mac_val;
	return RET_OK;
}

/**
 *
 * @param mac_layer_data
 * @param a_addr_fd
 * @param b_addr_fd
 * @return
 */
static retval_t null_mac_addr_cmp(mac_layer_data_t* mac_layer_data, mac_addr_t a_addr_fd, mac_addr_t b_addr_fd){
	null_mac_addr_t* null_mac_addr_a = (null_mac_addr_t*) a_addr_fd;
	null_mac_addr_t* null_mac_addr_b = (null_mac_addr_t*) b_addr_fd;
	if((a_addr_fd == NULL) || (b_addr_fd == NULL)){
		return RET_ERROR;
	}
	if(null_mac_addr_a->mac_val == null_mac_addr_b->mac_val){
		return RET_OK;
	}
	return RET_ERROR;
}
#endif
