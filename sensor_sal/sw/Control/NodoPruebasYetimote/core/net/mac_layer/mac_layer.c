/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * mac_layer.c
 *
 *  Created on: 27 de nov. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file mac_layer.c
 */

#include "mac_layer.h"
#include "netstack.h"

#if ENABLE_NETSTACK_ARCH

#define DEBUG 0
#if DEBUG
#include "yetimote_stdio.h"
#define PRINTF(...) _printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

//* Init Deinit functions
/**
 *
 * @param mac_layer_funcs
 * @return
 */
mac_layer_t* init_mac_layer(mac_layer_funcs_t* mac_layer_funcs){
	mac_layer_t* new_mac_layer = ytMalloc(sizeof(mac_layer_t));

	if(new_mac_layer == NULL){
		return NULL;
	}
	new_mac_layer->mac_layer_funcs = mac_layer_funcs;
	new_mac_layer->mac_layer_private_data = NULL;

	PRINTF("MAC_LAYER: INIT\r\n");
	if((new_mac_layer->mac_layer_funcs->mac_layer_init(&(new_mac_layer->mac_layer_private_data))) != RET_OK){
		ytFree(new_mac_layer);
		PRINTF("MAC_LAYER: INIT ERROR\r\n");
		return NULL;
	}

	PRINTF("MAC_LAYER: INIT DONE\r\n");
	return new_mac_layer;
}
/**
 *
 * @param rt_layer
 * @return
 */
retval_t deinit_mac_layer(mac_layer_t* mac_layer){
	if(mac_layer == NULL){
		return RET_ERROR;
	}

	PRINTF("MAC_LAYER: DEINIT\r\n");
	mac_layer->mac_layer_funcs->mac_layer_deinit(mac_layer->mac_layer_private_data);

	ytFree(mac_layer);
	PRINTF("MAC_LAYER: DEINIT DONE\r\n");
	return RET_OK;
}


//* Called from Routing layer
/**
 *
 * @param dest_mac_addr_fd
 * @param packet
 * @return
 */
retval_t mac_send_packet(mac_addr_t dest_mac_addr_fd, net_packet_t* packet){
	mac_layer_t* mac_layer =  netstack_get_mac_layer();

	netstack_state = NS_MAC_SEND_PACKET;
	PRINTF("MAC_LAYER: EVENT SEND PACKET\r\n");
	return mac_layer->mac_layer_funcs->mac_send_packet(mac_layer->mac_layer_private_data, dest_mac_addr_fd, packet);
}
/**
 *
 * @return
 */
retval_t mac_read_packet(void){
	mac_layer_t* mac_layer =  netstack_get_mac_layer();

	netstack_state = NS_MAC_RCV_PACKET;
	PRINTF("MAC_LAYER: EVENT READ PACKET\r\n");
	return mac_layer->mac_layer_funcs->mac_rcv_packet(mac_layer->mac_layer_private_data);
}

//* Called from phy layer
/**
 *
 * @param packet
 * @return
 */
retval_t mac_packet_sent(net_packet_t* packet){
	mac_layer_t* mac_layer =  netstack_get_mac_layer();

	netstack_state = NS_MAC_PACKET_SENT;
	PRINTF("MAC_LAYER: EVENT SEND PACKET DONE\r\n");
	return mac_layer->mac_layer_funcs->mac_send_packet_done(mac_layer->mac_layer_private_data, packet);
}
/**
 *
 * @param packet
 * @return
 */
retval_t mac_packet_received(net_packet_t* packet){
	mac_layer_t* mac_layer =  netstack_get_mac_layer();

	netstack_state = NS_MAC_PACKET_RECEIVED;
	PRINTF("MAC_LAYER: EVENT RCV PACKET DONE\r\n");
	return mac_layer->mac_layer_funcs->mac_rcv_packet_done(mac_layer->mac_layer_private_data, packet);

}

//* Called from user space or same layer
retval_t mac_set_node_addr(mac_addr_t mac_addr_fd){
	mac_layer_t* mac_layer =  netstack_get_mac_layer();

	PRINTF("MAC_LAYER: SET MAC ADDR\r\n");
	return mac_layer->mac_layer_funcs->mac_set_addr(mac_layer->mac_layer_private_data, mac_addr_fd);
}

//* Called from any layer. Inmediate Functions
/**
 *
 * @param mac_str_val
 * @return
 */
mac_addr_t mac_new_addr(char* mac_str_val, char format){
	mac_addr_t mac_addr;
	mac_layer_t* mac_layer =  netstack_get_mac_layer();
	PRINTF("MAC_LAYER: NEW MAC ADDR\r\n");
	mac_layer->mac_layer_funcs->mac_new_addr(mac_layer->mac_layer_private_data, mac_str_val, format, &mac_addr);
	return mac_addr;
}
/**
 *
 * @param mac_addr
 * @return
 */
retval_t mac_delete_addr(mac_addr_t mac_addr){
	mac_layer_t* mac_layer =  netstack_get_mac_layer();
	if(mac_addr == NULL){
		return RET_ERROR;
	}
	PRINTF("MAC_LAYER: DELETE MAC ADDR\r\n");
	return mac_layer->mac_layer_funcs->mac_delete_addr(mac_layer->mac_layer_private_data, mac_addr);
}
/**
 *
 * @param mac_addr
 * @return
 */
retval_t mac_addr_to_string(mac_addr_t mac_addr, char* mac_str_val, char format){
	mac_layer_t* mac_layer =  netstack_get_mac_layer();
	if((mac_addr == NULL) || (mac_str_val == NULL)){
		return RET_ERROR;
	}
	return mac_layer->mac_layer_funcs->mac_addr_to_string(mac_layer->mac_layer_private_data, mac_addr, mac_str_val, format);
}

/**
 *
 * @param dest_addr
 * @param from_addr
 * @return
 */
retval_t mac_addr_cpy(mac_addr_t dest_addr, mac_addr_t from_addr){
	mac_layer_t* mac_layer =  netstack_get_mac_layer();
	if((dest_addr == NULL) || (from_addr == NULL)){
		return RET_ERROR;
	}

	return mac_layer->mac_layer_funcs->mac_addr_cpy(mac_layer->mac_layer_private_data, dest_addr, from_addr);
}

/**
 *
 * @param a_addr
 * @param b_addr
 * @return
 */
uint16_t mac_addr_cmp(mac_addr_t a_addr, mac_addr_t b_addr){
	mac_layer_t* mac_layer =  netstack_get_mac_layer();
	if((a_addr == NULL) || (b_addr == NULL)){
		return 0;
	}

	if(mac_layer->mac_layer_funcs->mac_addr_cmp(mac_layer->mac_layer_private_data, a_addr, b_addr) == RET_OK){
		return 1;
	}
	return 0;
}

#endif
