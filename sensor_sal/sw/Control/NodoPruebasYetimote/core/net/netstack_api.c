/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * netstack_api.c
 *
 *  Created on: 21 de nov. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file netstack_api.c
 */

#include "system_api.h"
#include "netstack.h"
#include "net_api.h"

#define DEBUG 0
#if DEBUG
#include "yetimote_stdio.h"
#define PRINTF(...) _printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif


#define NET_API_TIMEOUT		YT_WAIT_FOREVER
#define SEND_TIMEOUT		YT_WAIT_FOREVER
#define RCV_TIMEOUT			YT_WAIT_FOREVER
#define CONNECT_TIMEOUT		YT_WAIT_FOREVER

extern netstack_t netstack;

/* API FUNCS */
/* UNRELIABLE FUNCS */
/**
 *
 * @param port_number
 * @return
 */
retval_t net_uc_open(uint16_t port_number){
#if ENABLE_NETSTACK_ARCH
	uint32_t op_semph_id;
	tp_layer_func_args_t* tp_layer_args;
	retval_t ret;

	if(netstack.netstack_data != NULL){
		PRINTF("N_API: UC OPEN\r\n");
		tp_layer_args = (tp_layer_func_args_t*) ytMalloc(sizeof(tp_layer_func_args_t));
		ret = RET_OK;

		op_semph_id = ytSemaphoreCreate(1);
		ytSemaphoreWait(op_semph_id, YT_WAIT_FOREVER);

		tp_layer_args->port = port_number;
		tp_layer_args->tp_layer = netstack.transport_layer;
		tp_layer_args->tp_layer_event_type = TP_EVENT_U_OPEN;
		tp_layer_args->ret_val = &ret;
		tp_layer_args->semaphore_id = op_semph_id;

		netstack_post_event(tp_event_u_open, tp_layer_args);

		if(ytSemaphoreWait(op_semph_id, NET_API_TIMEOUT) != RET_OK){
			PRINTF("N_API: UC OPEN ERROR TIMEOUT\r\n");
			ret = RET_ERROR;
		}
		ytSemaphoreDelete(op_semph_id);

		if(ret == RET_OK){
			PRINTF("N_API: UC OPEN DONE\r\n");
		}
		else{
			PRINTF("N_API: UC OPEN ERROR\r\n");
		}
		return ret;
	}
	else{
		PRINTF("N_API: UC OPEN HARD ERROR\r\n");
		return RET_ERROR;
	}
#else
	PRINTF("N_API: UC OPEN UNINIT STACK ERROR\r\n");
	return RET_ERROR;
#endif
}

/**
 *
 * @param port_number
 * @return
 */
retval_t net_uc_close(uint16_t port_number){
#if ENABLE_NETSTACK_ARCH
	uint32_t op_semph_id;
	tp_layer_func_args_t* tp_layer_args;
	retval_t ret;

	if(netstack.netstack_data != NULL){
		PRINTF("N_API: UC CLOSE\r\n");
		tp_layer_args = (tp_layer_func_args_t*) ytMalloc(sizeof(tp_layer_func_args_t));
		ret = RET_OK;

		op_semph_id = ytSemaphoreCreate(1);
		ytSemaphoreWait(op_semph_id, YT_WAIT_FOREVER);

		tp_layer_args->port = port_number;
		tp_layer_args->tp_layer = netstack.transport_layer;
		tp_layer_args->ret_val = &ret;
		tp_layer_args->tp_layer_event_type = TP_EVENT_U_CLOSE;
		tp_layer_args->semaphore_id = op_semph_id;

		netstack_post_event(tp_event_u_close, tp_layer_args);

		if(ytSemaphoreWait(op_semph_id, NET_API_TIMEOUT) != RET_OK){
			PRINTF("N_API: UC CLOSE ERROR TIMEOUT\r\n");
			ret = RET_ERROR;
		}
		ytSemaphoreDelete(op_semph_id);
		if(ret == RET_OK){
			PRINTF("N_API: UC CLOSE DONE\r\n");
		}
		else{
			PRINTF("N_API: UC CLOSE ERROR\r\n");
		}
		return ret;
	}
	else{
		PRINTF("N_API: UC CLOSE HARD ERROR\r\n");
		return RET_ERROR;
	}
#else
	PRINTF("N_API: UC CLOSE UNINIT STACK ERROR\r\n");
	return RET_ERROR;
#endif
}

/**
 *
 * @param port_number
 * @param dest_addr_fd
 * @param data
 * @param size
 * @return
 */
uint32_t net_uc_send(uint16_t port_number, net_addr_t dest_addr_fd, uint8_t* data, uint32_t size){
#if ENABLE_NETSTACK_ARCH
	uint32_t op_semph_id;
	tp_layer_func_args_t* tp_layer_args;
	uint32_t size_ret;

	if(netstack.netstack_data != NULL){
		PRINTF("N_API: UC SEND\r\n");
		tp_layer_args = (tp_layer_func_args_t*) ytMalloc(sizeof(tp_layer_func_args_t));
		size_ret = size;
		op_semph_id = ytSemaphoreCreate(1);
		ytSemaphoreWait(op_semph_id, YT_WAIT_FOREVER);

		tp_layer_args->port = port_number;
		tp_layer_args->data = data;
		tp_layer_args->size = &size_ret;
		tp_layer_args->addr_fd = dest_addr_fd;
		tp_layer_args->tp_layer = netstack.transport_layer;
		tp_layer_args->tp_layer_event_type = TP_EVENT_U_SEND;
		tp_layer_args->semaphore_id = op_semph_id;

		netstack_post_event(tp_event_u_send, tp_layer_args);

		if(ytSemaphoreWait(op_semph_id, SEND_TIMEOUT) != RET_OK){
			PRINTF("N_API: UC SEND ERROR TIMEOUT\r\n");
			size_ret = 0;
		}
		ytSemaphoreDelete(op_semph_id);
		if(!size_ret){
			PRINTF("N_API: UC SEND ERROR\r\n");
		}
		else{
			PRINTF("N_API: UC SEND DONE\r\n");
		}
		return size_ret;
	}
	else{
		PRINTF("N_API: UC SEND HARD ERROR\r\n");
		return 0;
	}
#else
	PRINTF("N_API: UC SEND UNINIT STACK ERROR\r\n");
	return 0;
#endif
}

/**
 *
 * @param port_number
 * @param from_addr_fd
 * @param data
 * @param size
 * @return
 */
uint32_t net_uc_rcv(uint16_t port_number, net_addr_t from_addr_fd, uint8_t* data, uint32_t size){
#if ENABLE_NETSTACK_ARCH
	uint32_t op_semph_id;
	tp_layer_func_args_t* tp_layer_args;
	uint32_t size_ret;

	if(netstack.netstack_data != NULL){
		PRINTF("N_API: UC RCV\r\n");
		tp_layer_args = (tp_layer_func_args_t*) ytMalloc(sizeof(tp_layer_func_args_t));
		size_ret = size;
		op_semph_id = ytSemaphoreCreate(1);
		ytSemaphoreWait(op_semph_id, YT_WAIT_FOREVER);

		tp_layer_args->port = port_number;
		tp_layer_args->data = data;
		tp_layer_args->size = &size_ret;
		tp_layer_args->addr_fd = from_addr_fd;
		tp_layer_args->tp_layer = netstack.transport_layer;
		tp_layer_args->tp_layer_event_type = TP_EVENT_U_RCV;
		tp_layer_args->semaphore_id = op_semph_id;
		tp_layer_args->read_async = 0;

		netstack_post_event(tp_event_u_rcv, tp_layer_args);

		if(ytSemaphoreWait(op_semph_id, RCV_TIMEOUT) != RET_OK){
			PRINTF("N_API: UC RCV ERROR TIMEOUT\r\n");
			size_ret = 0;
		}
		ytSemaphoreDelete(op_semph_id);
		if(!size_ret){
			PRINTF("N_API: UC RCV ERROR\r\n");
		}
		else{
			PRINTF("N_API: UC RCV DONE\r\n");
		}
		return size_ret;
	}
	else{
		PRINTF("N_API: UC RCV HARD ERROR\r\n");
		return 0;
	}
#else
	PRINTF("N_API: UC RCV UNINIT STACK ERROR\r\n");
	return 0;
#endif
}

/**
 *
 * @param port_number
 * @param from_addr_fd
 * @param data
 * @param size
 * @param callback_func
 * @param args
 * @return
 */
retval_t net_uc_rcv_async(uint16_t port_number, net_addr_t from_addr_fd, uint8_t* data, uint32_t size, net_read_cb_func_t callback_func, void* args){
#if ENABLE_NETSTACK_ARCH
	tp_layer_func_args_t* tp_layer_args;

	if(netstack.netstack_data != NULL){
		PRINTF("N_API: UC ASYNC RCV\r\n");
		tp_layer_args = (tp_layer_func_args_t*) ytMalloc(sizeof(tp_layer_func_args_t));

		tp_layer_args->port = port_number;
		tp_layer_args->data = data;
		tp_layer_args->size = (uint32_t*) ytMalloc(sizeof(uint32_t));
		(*tp_layer_args->size) = size;
		tp_layer_args->addr_fd = from_addr_fd;
		tp_layer_args->tp_layer = netstack.transport_layer;
		tp_layer_args->tp_layer_event_type = TP_EVENT_U_RCV;
		tp_layer_args->read_callback = callback_func;
		tp_layer_args->args = args;
		tp_layer_args->read_async = 1;

		netstack_post_event(tp_event_u_rcv, tp_layer_args);

		PRINTF("N_API: UC RCV ASYNC.. READING... \r\n");

		return RET_OK;
	}
	else{
		PRINTF("N_API: UC RCV ASYNC HARD ERROR\r\n");
		return RET_ERROR;
	}
#else
	PRINTF("N_API: UC RCV ASYNC UNINIT STACK ERROR\r\n");
	return RET_ERROR;
#endif
}

float32_t net_uc_get_last_signal_level(uint16_t port){
#if ENABLE_NETSTACK_ARCH
	float32_t signal_level = -180;
	if(netstack.netstack_data != NULL){
		tp_u_get_last_signal_level(port, &signal_level);
		return signal_level;
	}
	else{
		return -180;
	}
#else
	return -180;
#endif
}

/*RELIABLE FUNCS */
/**
 *
 * @param port_number
 * @return
 */
uint32_t net_rc_create_server(uint16_t port_number){
#if ENABLE_NETSTACK_ARCH
	uint32_t op_semph_id;
	tp_layer_func_args_t* tp_layer_args;
	uint32_t ret_server_fd;

	if(netstack.netstack_data != NULL){
		PRINTF("N_API: CREATE SERVER\r\n");
		tp_layer_args = (tp_layer_func_args_t*) ytMalloc(sizeof(tp_layer_func_args_t));

		op_semph_id = ytSemaphoreCreate(1);
		ytSemaphoreWait(op_semph_id, YT_WAIT_FOREVER);

		tp_layer_args->port = port_number;
		tp_layer_args->server_fd = &ret_server_fd;
		tp_layer_args->tp_layer = netstack.transport_layer;
		tp_layer_args->tp_layer_event_type = TP_EVENT_R_CREATE_SERVER;
		tp_layer_args->semaphore_id = op_semph_id;

		netstack_post_event(tp_event_r_create_server, tp_layer_args);

		if(ytSemaphoreWait(op_semph_id, NET_API_TIMEOUT) != RET_OK){
			PRINTF("N_API: CREATE SERVER ERROR TIMEOUT\r\n");
			ret_server_fd = 0;
		}
		ytSemaphoreDelete(op_semph_id);
		if(!ret_server_fd){
			PRINTF("N_API: CREATE SERVER ERROR\r\n");
		}
		else{
			PRINTF("N_API: CREATE SERVER DONE\r\n");
		}
		return ret_server_fd;
	}
	else{
		PRINTF("N_API: CREATE SERVER HARD ERROR\r\n");
		return 0;
	}
#else
	PRINTF("N_API: CREATE SERVER UNINIT STACK ERROR\r\n");
	return 0;
#endif
}

/**
 *
 * @param server_fd
 * @return
 */
retval_t net_rc_delete_server(uint32_t server_fd){
#if ENABLE_NETSTACK_ARCH
	uint32_t op_semph_id;
	tp_layer_func_args_t* tp_layer_args;
	retval_t ret;

	if(netstack.netstack_data != NULL){
		PRINTF("N_API: DELETE SERVER\r\n");
		tp_layer_args = (tp_layer_func_args_t*) ytMalloc(sizeof(tp_layer_func_args_t));
		ret = RET_OK;
		op_semph_id = ytSemaphoreCreate(1);
		ytSemaphoreWait(op_semph_id, YT_WAIT_FOREVER);

		tp_layer_args->server_fd = &server_fd;
		tp_layer_args->ret_val = &ret;
		tp_layer_args->tp_layer = netstack.transport_layer;
		tp_layer_args->tp_layer_event_type = TP_EVENT_R_DELETE_SERVER;
		tp_layer_args->semaphore_id = op_semph_id;

		netstack_post_event(tp_event_r_delete_server, tp_layer_args);

		if(ytSemaphoreWait(op_semph_id, NET_API_TIMEOUT) != RET_OK){
			PRINTF("N_API: DELETE SERVER ERROR TIMEOUT\r\n");
			ret = RET_ERROR;
		}
		ytSemaphoreDelete(op_semph_id);
		if(ret != RET_OK){
			PRINTF("N_API: DELETE SERVER ERROR\r\n");
		}
		else{
			PRINTF("N_API: DELETE SERVER DONE\r\n");
		}
		return ret;
	}
	else{
		PRINTF("N_API: DELETE SERVER HARD ERROR\r\n");
		return RET_ERROR;
	}
#else
	PRINTF("N_API: DELETE SERVER UNINIT STACK ERROR\r\n");
	return RET_ERROR;
#endif
}

/**
 *
 * @param server_fd
 * @param from_addr_fd
 * @return
 */
uint32_t net_rc_server_accept_connection(uint32_t server_fd, net_addr_t from_addr_fd){
#if ENABLE_NETSTACK_ARCH
	uint32_t op_semph_id;
	tp_layer_func_args_t* tp_layer_args;
	uint32_t ret_conn_fd;

	if(netstack.netstack_data != NULL){
		PRINTF("N_API: ACCEPT CONN\r\n");
		tp_layer_args = (tp_layer_func_args_t*) ytMalloc(sizeof(tp_layer_func_args_t));
		op_semph_id = ytSemaphoreCreate(1);
		ytSemaphoreWait(op_semph_id, YT_WAIT_FOREVER);

		tp_layer_args->server_fd = &server_fd;
		tp_layer_args->addr_fd = from_addr_fd;
		tp_layer_args->connection_fd = &ret_conn_fd;
		tp_layer_args->tp_layer = netstack.transport_layer;
		tp_layer_args->tp_layer_event_type = TP_EVENT_R_ACCEPT_CONN;
		tp_layer_args->semaphore_id = op_semph_id;

		netstack_post_event(tp_event_r_accept_connection, tp_layer_args);

		if(ytSemaphoreWait(op_semph_id, NET_API_TIMEOUT) != RET_OK){
			PRINTF("N_API: ACCEPT CONN ERROR TIMEOUT\r\n");
			ret_conn_fd = 0;
		}
		ytSemaphoreDelete(op_semph_id);
		if(!ret_conn_fd){
			PRINTF("N_API: ACCEPT CONN ERROR\r\n");
		}
		else{
			PRINTF("N_API: ACCEPT CONN DONE\r\n");
		}
		return ret_conn_fd;
	}
	else{
		PRINTF("N_API: ACCEPT CONN HARD ERROR\r\n");
		return 0;
	}
#else
	PRINTF("N_API: ACCEPT CONN UNINIT STACK ERROR\r\n");
	return 0;
#endif
}

/**
 *
 * @param dest_addr_fd
 * @param port_number
 * @return
 */
uint32_t net_rc_connect_to_server(net_addr_t dest_addr_fd, uint16_t port_number){
#if ENABLE_NETSTACK_ARCH
	uint32_t op_semph_id;
	tp_layer_func_args_t* tp_layer_args;
	uint32_t ret_conn_fd;

	if(netstack.netstack_data != NULL){
		PRINTF("N_API: CONNECT TO\r\n");
		tp_layer_args = (tp_layer_func_args_t*) ytMalloc(sizeof(tp_layer_func_args_t));
		op_semph_id = ytSemaphoreCreate(1);
		ytSemaphoreWait(op_semph_id, YT_WAIT_FOREVER);

		tp_layer_args->port = port_number;
		tp_layer_args->addr_fd = dest_addr_fd;
		tp_layer_args->connection_fd = &ret_conn_fd;
		tp_layer_args->tp_layer = netstack.transport_layer;
		tp_layer_args->tp_layer_event_type = TP_EVENT_R_CONNECT_TO;
		tp_layer_args->semaphore_id = op_semph_id;

		netstack_post_event(tp_event_r_connect_to_server, tp_layer_args);

		if(ytSemaphoreWait(op_semph_id, CONNECT_TIMEOUT) != RET_OK){
			PRINTF("N_API: CONNECT TO ERROR TIMEOUT\r\n");
			ret_conn_fd = 0;
		}
		ytSemaphoreDelete(op_semph_id);
		if(!ret_conn_fd){
			PRINTF("N_API: ACCEPT TO ERROR\r\n");
		}
		else{
			PRINTF("N_API: CONNECT TO DONE\r\n");
		}
		return ret_conn_fd;
	}
	else{
		PRINTF("N_API: CONNECT TO HARD ERROR\r\n");
		return 0;
	}
#else
	PRINTF("N_API: CONNECT TO UNINIT STACK ERROR\r\n");
	return 0;
#endif
}

/**
 *
 * @param connection_fd
 * @return
 */
retval_t net_rc_close_connection(uint32_t connection_fd){
#if ENABLE_NETSTACK_ARCH
	uint32_t op_semph_id;
	tp_layer_func_args_t* tp_layer_args;
	retval_t ret;

	if(netstack.netstack_data != NULL){
		PRINTF("N_API: CLOSE CONN\r\n");
		tp_layer_args = (tp_layer_func_args_t*) ytMalloc(sizeof(tp_layer_func_args_t));
		ret = RET_OK;
		op_semph_id = ytSemaphoreCreate(1);
		ytSemaphoreWait(op_semph_id, YT_WAIT_FOREVER);

		tp_layer_args->connection_fd = &connection_fd;
		tp_layer_args->ret_val = &ret;
		tp_layer_args->tp_layer = netstack.transport_layer;
		tp_layer_args->tp_layer_event_type = TP_EVENT_R_CLOSE_CONN;
		tp_layer_args->semaphore_id = op_semph_id;

		netstack_post_event(tp_event_r_close_connection, tp_layer_args);

		if(ytSemaphoreWait(op_semph_id, CONNECT_TIMEOUT) != RET_OK){
			PRINTF("N_API: CLOSE CONN ERROR TIMEOUT\r\n");
			ret = RET_ERROR;
		}
		ytSemaphoreDelete(op_semph_id);
		if(ret != RET_OK){
			PRINTF("N_API: CLOSE CONN ERROR\r\n");
		}
		else{
			PRINTF("N_API: CLOSE CONN DONE\r\n");
		}
		return ret;
	}
	else{
		PRINTF("N_API: CLOSE CONN HARD ERROR\r\n");
		return RET_ERROR;
	}
#else
	PRINTF("N_API: CLOSE CONN UNINIT STACK ERROR\r\n");
	return RET_ERROR;
#endif
}

/**
 *
 * @param connection_fd
 * @param data
 * @param size
 * @return
 */
uint32_t net_rc_send(uint32_t connection_fd, uint8_t* data, uint32_t size){
#if ENABLE_NETSTACK_ARCH
	uint32_t op_semph_id;
	tp_layer_func_args_t* tp_layer_args;
	uint32_t ret_size;

	if(netstack.netstack_data != NULL){
		PRINTF("N_API: RC SEND\r\n");
		tp_layer_args = (tp_layer_func_args_t*) ytMalloc(sizeof(tp_layer_func_args_t));
		ret_size = size;
		op_semph_id = ytSemaphoreCreate(1);
		ytSemaphoreWait(op_semph_id, YT_WAIT_FOREVER);

		tp_layer_args->connection_fd = &connection_fd;
		tp_layer_args->data = data;
		tp_layer_args->size = &ret_size;
		tp_layer_args->tp_layer = netstack.transport_layer;
		tp_layer_args->tp_layer_event_type = TP_EVENT_R_SEND;
		tp_layer_args->semaphore_id = op_semph_id;

		netstack_post_event(tp_event_r_send, tp_layer_args);

		if(ytSemaphoreWait(op_semph_id, SEND_TIMEOUT) != RET_OK){
			PRINTF("N_API: RC SEND ERROR TIMEOUT\r\n");
			ret_size = 0;
		}
		ytSemaphoreDelete(op_semph_id);
		if(!ret_size){
			PRINTF("N_API: RC SEND ERROR\r\n");
		}
		else{
			PRINTF("N_API: RC SEND DONE\r\n");
		}
		return ret_size;
	}
	else{
		PRINTF("N_API: RC SEND HARD ERROR\r\n");
		return 0;
	}
#else
	PRINTF("N_API: RC SEND UNINIT STACK ERROR\r\n");
	return 0;
#endif
}

/**
 *
 * @param connection_fd
 * @param data
 * @param size
 * @return
 */
uint32_t net_rc_rcv(uint32_t connection_fd, uint8_t* data, uint32_t size){
#if ENABLE_NETSTACK_ARCH
	uint32_t op_semph_id;
	uint32_t ret_size = size;
	tp_layer_func_args_t* tp_layer_args;

	if(netstack.netstack_data != NULL){
		PRINTF("N_API: RC RCV\r\n");
		tp_layer_args = (tp_layer_func_args_t*) ytMalloc(sizeof(tp_layer_func_args_t));
		ret_size = size;
		op_semph_id = ytSemaphoreCreate(1);
		ytSemaphoreWait(op_semph_id, YT_WAIT_FOREVER);

		tp_layer_args->connection_fd = &connection_fd;
		tp_layer_args->data = data;
		tp_layer_args->size = &ret_size;
		tp_layer_args->tp_layer = netstack.transport_layer;
		tp_layer_args->tp_layer_event_type = TP_EVENT_R_RCV;
		tp_layer_args->semaphore_id = op_semph_id;
		tp_layer_args->read_async = 0;

		netstack_post_event(tp_event_r_rcv, tp_layer_args);

		if(ytSemaphoreWait(op_semph_id, RCV_TIMEOUT) != RET_OK){
			PRINTF("N_API: RC RCV ERROR TIMEOUT\r\n");
			ret_size = 0;
		}
		ytSemaphoreDelete(op_semph_id);
		if(!ret_size){
			PRINTF("N_API: RC RCV ERROR\r\n");
		}
		else{
			PRINTF("N_API: RC RCV DONE\r\n");
		}
		return ret_size;
	}
	else{
		PRINTF("N_API: RC RCV HARD ERROR\r\n");
		return 0;
	}
#else
	PRINTF("N_API: RC RCV UNINIT STACK ERROR\r\n");
	return 0;
#endif
}

/**
 *
 * @param connection_fd
 * @param data
 * @param size
 * @param callback_func
 * @param args
 * @return
 */
retval_t net_rc_rcv_async(uint32_t connection_fd, uint8_t* data, uint32_t size, net_read_cb_func_t callback_func, void* args){
#if ENABLE_NETSTACK_ARCH
	tp_layer_func_args_t* tp_layer_args;

	if(netstack.netstack_data != NULL){

		PRINTF("N_API: RC RC ASYNCV\r\n");
		tp_layer_args = (tp_layer_func_args_t*) ytMalloc(sizeof(tp_layer_func_args_t));

		tp_layer_args->connection_fd = (uint32_t*) ytMalloc(sizeof(uint32_t));
		(*tp_layer_args->connection_fd) = connection_fd;
		tp_layer_args->data = data;
		tp_layer_args->size = (uint32_t*) ytMalloc(sizeof(uint32_t));
		(*tp_layer_args->size) = size;
		tp_layer_args->tp_layer = netstack.transport_layer;
		tp_layer_args->tp_layer_event_type = TP_EVENT_R_RCV;
		tp_layer_args->read_callback = callback_func;
		tp_layer_args->args = args;
		tp_layer_args->read_async = 1;

		netstack_post_event(tp_event_r_rcv, tp_layer_args);

		PRINTF("N_API: RC RCV ASYNC.. READING...\r\n");

		return RET_OK;
	}
	else{
		PRINTF("N_API: RC RCV ASYNC HARD ERROR\r\n");
		return RET_ERROR;
	}
#else
	PRINTF("N_API: RC RCV UNINIT ASYNC STACK ERROR\r\n");
	return RET_ERROR;
#endif
}


uint16_t net_rc_check_connection(uint32_t connection_fd){
#if ENABLE_NETSTACK_ARCH
	uint32_t op_semph_id;
	tp_layer_func_args_t* tp_layer_args;
	retval_t ret;

	if(netstack.netstack_data != NULL){
		PRINTF("N_API: CHECK CONN\r\n");
		tp_layer_args = (tp_layer_func_args_t*) ytMalloc(sizeof(tp_layer_func_args_t));
		ret = RET_OK;

		op_semph_id = ytSemaphoreCreate(1);
		ytSemaphoreWait(op_semph_id, YT_WAIT_FOREVER);

		tp_layer_args->tp_layer = netstack.transport_layer;
		tp_layer_args->tp_layer_event_type = TP_EVENT_R_CHECK_CONN;
		tp_layer_args->ret_val = &ret;
		tp_layer_args->connection_fd = &connection_fd;
		tp_layer_args->semaphore_id = op_semph_id;

		netstack_post_event(tp_event_r_check_connection, tp_layer_args);

		if(ytSemaphoreWait(op_semph_id, NET_API_TIMEOUT) != RET_OK){
			PRINTF("N_API: CHECK CONN ERROR TIMEOUT\r\n");
			ret = RET_ERROR;
		}
		ytSemaphoreDelete(op_semph_id);

		if(ret == RET_OK){
			PRINTF("N_API: CHECK CONN: CONNECTION ON\r\n");
			return 1;
		}
		else{
			PRINTF("N_API: CHECK CONN: CONNECTION OFF\r\n");
			return 0;
		}
	}
	else{
		PRINTF("N_API: CHECK CONN HARD ERROR\r\n");
		return RET_ERROR;
	}
#else
	PRINTF("N_API: CHECK CONN UNINIT STACK ERROR\r\n");
	return 0;
#endif
}

/**
 *
 * @param port
 * @return
 */
float32_t net_rc_get_last_signal_level(uint32_t connection_fd){
#if ENABLE_NETSTACK_ARCH
	float32_t signal_level = -180;
	if(netstack.netstack_data != NULL){
		tp_r_get_last_signal_level(connection_fd, &signal_level);
		return signal_level;
	}
	else{
		return -180;
	}
#else
	return -180;
#endif
}

retval_t net_rc_set_connection_timeout(uint32_t connection_fd, uint32_t timeout){
#if ENABLE_NETSTACK_ARCH
	if(netstack.netstack_data != NULL){
		return tp_r_set_connection_timeout(connection_fd, timeout);
	}
	else{
		return RET_ERROR;
	}
#else
	return RET_ERROR;
#endif
}
/*Network Addresses Functions*/
/**
 *
 * @param net_str_val
 * @param format
 * @return
 */
net_addr_t new_net_addr(char* net_str_val, char format){
#if ENABLE_NETSTACK_ARCH
	if(netstack.netstack_data != NULL){
		return rt_new_net_addr(net_str_val, format);
	}
	else{
		return NULL;
	}
#else
	return NULL;
#endif
}

/**
 *
 * @return
 */
net_addr_t new_empty_net_addr(void){
#if ENABLE_NETSTACK_ARCH
	if(netstack.netstack_data != NULL){
		return rt_new_empty_net_addr();
	}
	else{
		return NULL;
	}
#else
	return NULL;
#endif
}
/**
 *
 * @param net_addr
 * @return
 */
retval_t delete_net_addr(net_addr_t net_addr){
#if ENABLE_NETSTACK_ARCH
	if(netstack.netstack_data != NULL){
		return rt_delete_net_addr(net_addr);
	}
	else{
		return RET_ERROR;
	}
#else
	return RET_ERROR;
#endif
}
/**
 *
 * @param net_addr
 * @param net_addr_str_val
 * @param format
 * @return
 */
retval_t net_addr_to_string(net_addr_t net_addr, char* net_addr_str_val, char format){
#if ENABLE_NETSTACK_ARCH
	if(netstack.netstack_data != NULL){
		return rt_net_addr_to_string(net_addr,net_addr_str_val,format);
	}
	else{
		return RET_ERROR;
	}
#else
	return RET_ERROR;
#endif
}
/**
 *
 * @param dest_addr
 * @param from_addr
 * @return
 */
retval_t net_addr_cpy(net_addr_t dest_addr, net_addr_t from_addr){
#if ENABLE_NETSTACK_ARCH
	if(netstack.netstack_data != NULL){
		return rt_net_addr_cpy(dest_addr, from_addr);
	}
	else{
		return RET_ERROR;
	}
#else
	return RET_ERROR;
#endif
}
/**
 *
 * @param a_addr
 * @param b_addr
 * @return
 */
uint16_t net_addr_cmp(net_addr_t a_addr, net_addr_t b_addr){
#if ENABLE_NETSTACK_ARCH
	if(netstack.netstack_data != NULL){
		return rt_net_addr_cmp(a_addr, b_addr);
	}
	else{
		return 0;
	}
#else
	return 0;
#endif
}

/**
 *
 * @param index
 * @return
 */
net_addr_t get_node_addr(uint16_t index){
#if ENABLE_NETSTACK_ARCH
	if(netstack.netstack_data != NULL){
		return rt_get_node_addr(index);
	}
	else{
		return NULL;
	}
#else
	return NULL;
#endif
}

/**
 *
 * @param node_addr
 * @return
 */
retval_t net_add_node_addr(net_addr_t node_addr){
#if ENABLE_NETSTACK_ARCH
	if(netstack.netstack_data != NULL){
		return rt_add_node_addr(node_addr);
	}
	else{
		return RET_ERROR;
	}
#else
	return RET_ERROR;
#endif
}

/**
 *
 * @param node_addr
 * @return
 */
retval_t net_remove_node_addr(net_addr_t node_addr){
#if ENABLE_NETSTACK_ARCH
	if(netstack.netstack_data != NULL){
		return rt_remove_node_addr(node_addr);
	}
	else{
		return RET_ERROR;
	}
#else
	return RET_ERROR;
#endif
}


/**
 *
 * @param dest_addr
 * @param next_addr
 * @param hops
 * @return
 */
retval_t net_add_route(net_addr_t dest_addr, net_addr_t next_addr, uint16_t hops){
#if ENABLE_NETSTACK_ARCH
	if(netstack.netstack_data != NULL){
		return rt_add_node_route(dest_addr, next_addr, hops);
	}
	else{
		return RET_ERROR;
	}
#else
	return RET_ERROR;
#endif
}

/**
 *
 * @param dest_addr
 * @param next_addr
 * @param hops
 * @return
 */
retval_t net_remove_route(net_addr_t dest_addr, net_addr_t next_addr, uint16_t hops){
#if ENABLE_NETSTACK_ARCH
	if(netstack.netstack_data != NULL){
		return rt_remove_node_route(dest_addr, next_addr, hops);
	}
	else{
		return RET_ERROR;
	}
#else
	return RET_ERROR;
#endif
}

/* END API FUNCS */
