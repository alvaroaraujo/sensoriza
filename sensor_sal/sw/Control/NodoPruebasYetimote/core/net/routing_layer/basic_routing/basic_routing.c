/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * basic_routing.c
 *
 *  Created on: 20 de dic. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file basic_routing.c
 */

#include "routing_layer.h"
#include "mac_layer.h"
#include "transport_layer.h"
#include "basic_routing.h"
#include "packetbuffer.h"

#if ENABLE_NETSTACK_ARCH

#define BC_DEST_ADDR		0xFFFF
#define DEFAULT_NODE_ADDR	0xb105

typedef struct __packed basic_net_addr_{
	uint16_t b_net_addr;
}basic_net_addr_t;

typedef struct basic_route_{
	basic_net_addr_t dest_addr;
	basic_net_addr_t next_addr;
	uint16_t num_hops;
}basic_route_t;

typedef struct basic_rt_layer_data_{
	gen_list* route_list;
	basic_net_addr_t node_addr;
//	basic_net_addr_t next_hop_addr;
//	uint8_t sending_data;
	uint8_t resending_flag;
}basic_rt_layer_data_t;

typedef mac_addr_t null_rt_net_addr_t;

static retval_t basic_rt_layer_init(rt_layer_data_t** rt_layer_data);
static retval_t basic_rt_layer_deinit(rt_layer_data_t* rt_layer_data);

static retval_t basic_rt_send_packet(rt_layer_data_t* rt_layer_data, net_addr_t addr_fd, net_packet_t* packet);
static retval_t basic_rt_rcv_packet(rt_layer_data_t* rt_layer_data);

static retval_t basic_rt_send_packet_done(rt_layer_data_t* rt_layer_data, net_packet_t* packet);
static retval_t basic_rt_rcv_packet_done(rt_layer_data_t* rt_layer_data, net_packet_t* packet, mac_addr_t mac_from_addr);

static retval_t basic_rt_get_node_addr(rt_layer_data_t* rt_layer_data, uint16_t index, net_addr_t* node_addr);
static retval_t basic_rt_add_node_addr(rt_layer_data_t* rt_layer_data, net_addr_t new_addr_fd);
static retval_t basic_rt_remove_node_addr(rt_layer_data_t* rt_layer_data, net_addr_t addr_fd);
static retval_t basic_rt_add_node_route(rt_layer_data_t* rt_layer_data, net_addr_t dest_addr_fd, net_addr_t next_addr_fd, uint16_t hops);
static retval_t basic_rt_remove_node_route(rt_layer_data_t* rt_layer_data, net_addr_t dest_addr_fd, net_addr_t next_addr_fd, uint16_t hops);

static retval_t basic_rt_new_net_addr(rt_layer_data_t* rt_layer_data, char* net_str_val, char format, net_addr_t* net_addr_fd);
static retval_t basic_rt_delete_net_addr(rt_layer_data_t* rt_layer_data, net_addr_t net_addr_fd);
static retval_t basic_rt_net_addr_to_string(rt_layer_data_t* rt_layer_data, net_addr_t net_addr_fd, char* net_str_val, char format);
static retval_t basic_rt_net_addr_cpy(rt_layer_data_t* rt_layer_data, net_addr_t dest_addr_fd, net_addr_t from_addr_fd);
static retval_t basic_rt_net_addr_cmp(rt_layer_data_t* rt_layer_data, net_addr_t a_addr_fd, net_addr_t b_addr_fd);


/* PRIVATE FUNCS */
static retval_t find_dest_addr_route(rt_layer_data_t* rt_layer_data, basic_net_addr_t* dest_addr, basic_net_addr_t* next_addr);


rt_layer_funcs_t basic_routing_funcs = {
		.rt_layer_init = basic_rt_layer_init,
		.rt_layer_deinit = basic_rt_layer_deinit,
		.rt_send_packet = basic_rt_send_packet,
		.rt_rcv_packet = basic_rt_rcv_packet,
		.rt_send_packet_done = basic_rt_send_packet_done,
		.rt_rcv_packet_done = basic_rt_rcv_packet_done,
		.rt_add_node_addr = basic_rt_add_node_addr,
		.rt_remove_node_addr = basic_rt_remove_node_addr,
		.rt_add_node_route = basic_rt_add_node_route,
		.rt_remove_node_route = basic_rt_remove_node_route,
		.rt_new_net_addr = basic_rt_new_net_addr,
		.rt_delete_net_addr = basic_rt_delete_net_addr,
		.rt_net_addr_to_string = basic_rt_net_addr_to_string,
		.rt_net_addr_cpy = basic_rt_net_addr_cpy,
		.rt_net_addr_cmp = basic_rt_net_addr_cmp,
		.rt_get_node_addr = basic_rt_get_node_addr,
};

/**
 *
 * @param rt_layer_data
 * @return
 */
static retval_t basic_rt_layer_init(rt_layer_data_t** rt_layer_data){
	basic_rt_layer_data_t* layer_data = (basic_rt_layer_data_t*) ytMalloc(sizeof(basic_rt_layer_data_t));

	layer_data->route_list = gen_list_init();
//	layer_data->sending_data = 0;
	layer_data->resending_flag = 0;
//	layer_data->next_hop_addr.b_net_addr = 0;
	layer_data->node_addr.b_net_addr = DEFAULT_NODE_ADDR;
	mac_set_node_addr((mac_addr_t) DEFAULT_NODE_ADDR);
	(*rt_layer_data) = (rt_layer_data_t*) layer_data;
	mac_read_packet();	//Todos los nodos son routers asi que est�n siempre escuchando para poder rutar paquetes
	mac_read_packet();	//Duplico el numero de paquetes esperados. As� si se da la m�s que improbable situacion de recibir dos paquetes
									//seguidos, seguimos en modo reccepcion para poder rutar paquetes
	return RET_OK;
}

/**
 *
 * @param rt_layer_data
 * @return
 */
static retval_t basic_rt_layer_deinit(rt_layer_data_t* rt_layer_data){

	basic_rt_layer_data_t* layer_data = (basic_rt_layer_data_t*) rt_layer_data;

	gen_list_remove_all(layer_data->route_list);
	ytFree(layer_data);
	return RET_OK;
}

/**
 *
 * @param rt_layer_data
 * @param addr_fd
 * @param packet
 * @return
 */
static retval_t basic_rt_send_packet(rt_layer_data_t* rt_layer_data, net_addr_t addr_fd, net_packet_t* packet){
	basic_rt_layer_data_t* layer_data = (basic_rt_layer_data_t*) rt_layer_data;
	basic_net_addr_t* dest_addr = (basic_net_addr_t*) addr_fd;
	basic_net_addr_t next_addr;
//	if(!layer_data->sending_data){	//Necesario este control de flujo para asegurarse que nadie pisa next_hop_addr
//		layer_data->sending_data++;

	if(find_dest_addr_route(rt_layer_data, dest_addr, &next_addr) != RET_OK){	//No encontrado en la tabla de rutas el objetivo. En tal caso no envio el paquete
		basic_rt_send_packet_done(rt_layer_data, packet);
		return RET_ERROR;
	}
	else{	//He encontrado el next hop en la tabla de rutas y guardado dentro su valor

		if(!layer_data->resending_flag){	//Si no estoy reenviando pongo mi direccion como source y la que se pasa como destino
			packet->header.rt_hdr.src_addr = layer_data->node_addr.b_net_addr;
			packet->header.rt_hdr.dest_addr = dest_addr->b_net_addr;
		}
		else{	//Si reenvio ya est� la direccion de source puesta en el paquete que se ha copiado
			layer_data->resending_flag = 0;
		}

		return mac_send_packet((mac_addr_t) (&next_addr), packet);

	}
//	}

	return RET_OK;
}

/**
 *
 * @param rt_layer_data
 * @return
 */
static retval_t basic_rt_rcv_packet(rt_layer_data_t* rt_layer_data){	//Me pongo a escuchar
	mac_read_packet();
	return RET_OK;
}

/**
 *
 * @param rt_layer_data
 * @param packet
 * @return
 */
static retval_t basic_rt_send_packet_done(rt_layer_data_t* rt_layer_data, net_packet_t* packet){

//	if(!layer_data->sending_data){	//Deber�a ser imposible que se de esto
//		return RET_ERROR;
//	}

	return tp_packet_sent(packet);
//	layer_data->sending_data--;

	return RET_OK;
}

/**
 *
 * @param rt_layer_data
 * @param packet
 * @param mac_from_addr
 * @return
 */
static retval_t basic_rt_rcv_packet_done(rt_layer_data_t* rt_layer_data, net_packet_t* packet, mac_addr_t mac_from_addr){
	basic_rt_layer_data_t* layer_data = (basic_rt_layer_data_t*) rt_layer_data;
	net_packet_t* re_send_packet;
	basic_net_addr_t resend_dest_addr;
	basic_net_addr_t* net_from_addr;

	basic_rt_delete_net_addr(rt_layer_data, (net_addr_t) mac_from_addr);//Elimino la direccion, ya que no llega mas arriba del stack

	if((layer_data->node_addr.b_net_addr == packet->header.rt_hdr.dest_addr) || (packet->header.rt_hdr.dest_addr == BC_DEST_ADDR)){	//Soy el destinatario

		net_from_addr = (basic_net_addr_t*) ytMalloc(sizeof(basic_net_addr_t));
		net_from_addr->b_net_addr = packet->header.rt_hdr.src_addr;
		tp_packet_received(packet, (net_addr_t) net_from_addr);

	}
	else{	//No soy el destinatario. Reenvio
		resend_dest_addr.b_net_addr = packet->header.rt_hdr.dest_addr;

		if((re_send_packet = tx_packetbuffer_get_free_packet())!= NULL){	//Capturo el primer paquete sin usar
			memcpy(re_send_packet, packet, FIXED_PACKET_SIZE);						//Copio el paquete

			mac_read_packet();	//Todos los nodos son routers asi que est�n siempre escuchando para poder rutar paquetes
			layer_data->resending_flag = 1;
			basic_rt_send_packet(rt_layer_data, (net_addr_t) &resend_dest_addr, re_send_packet);	//Reenvio

		}
		rx_packetbuffer_release_packet(packet);
	}



	return RET_OK;
}

/**
 *
 * @param rt_layer_data
 * @param index
 * @param node_addr
 * @return
 */
static retval_t basic_rt_get_node_addr(rt_layer_data_t* rt_layer_data, uint16_t index, net_addr_t* node_addr){
	basic_net_addr_t* basic_node_addr;
	basic_rt_layer_data_t* layer_data = (basic_rt_layer_data_t*) rt_layer_data;
	if(index != 0){
		return RET_ERROR;
	}
	(*node_addr) = (net_addr_t) mac_new_addr(NULL, 0);
	basic_node_addr = (basic_net_addr_t*) (*node_addr);
	basic_node_addr->b_net_addr = layer_data->node_addr.b_net_addr;
	return RET_OK;

}
/**
 *
 * @param rt_layer_data
 * @param new_addr_fd
 * @return
 */
static retval_t basic_rt_add_node_addr(rt_layer_data_t* rt_layer_data, net_addr_t new_addr_fd){	//Overwrites node addr
	basic_rt_layer_data_t* layer_data = (basic_rt_layer_data_t*) rt_layer_data;
	basic_net_addr_t* node_addr = (basic_net_addr_t*) new_addr_fd;
	layer_data->node_addr.b_net_addr = node_addr->b_net_addr;
	mac_set_node_addr((mac_addr_t) new_addr_fd);
	return RET_OK;
}

/**
 *
 * @param rt_layer_data
 * @param addr_fd
 * @return
 */
static retval_t basic_rt_remove_node_addr(rt_layer_data_t* rt_layer_data, net_addr_t addr_fd){	//Only one node addr. Imposible to delete
	return RET_ERROR;
}
/**
 *
 * @param rt_layer_data
 * @param dest_addr_fd
 * @param next_addr_fd
 * @param hops
 * @return
 */
static retval_t basic_rt_add_node_route(rt_layer_data_t* rt_layer_data, net_addr_t dest_addr_fd, net_addr_t next_addr_fd, uint16_t hops){
	basic_rt_layer_data_t* layer_data = (basic_rt_layer_data_t*) rt_layer_data;
	basic_route_t* new_route = (basic_route_t*) ytMalloc(sizeof(basic_route_t));
	new_route->dest_addr = *((basic_net_addr_t*)dest_addr_fd);
	new_route->next_addr = *((basic_net_addr_t*)next_addr_fd);
	new_route->num_hops = hops;
	gen_list_add(layer_data->route_list, new_route);
	return RET_OK;
}

/**
 *
 * @param rt_layer_data
 * @param dest_addr_fd
 * @param next_addr_fd
 * @param hops
 * @return
 */
static retval_t basic_rt_remove_node_route(rt_layer_data_t* rt_layer_data, net_addr_t dest_addr_fd, net_addr_t next_addr_fd, uint16_t hops){
	basic_route_t* route;
	basic_rt_layer_data_t* layer_data = (basic_rt_layer_data_t*) rt_layer_data;
	basic_net_addr_t* dest_addr = (basic_net_addr_t*) dest_addr_fd;
	basic_net_addr_t* next_addr = (basic_net_addr_t*) next_addr_fd;
	gen_list* current = layer_data->route_list;

	while(current->next != NULL){
		route = (basic_route_t*) current->next->item;
		if(route->dest_addr.b_net_addr == dest_addr->b_net_addr){
			if(route->next_addr.b_net_addr == next_addr->b_net_addr){
				if(route->num_hops == hops){
					gen_list* next = current->next->next;
					vPortFree(current->next);
					ytFree(route);
					current->next = next;
				}
				else{
					current = current->next;
				}
			}
			else{
				current = current->next;
			}
		}
		else{
			current = current->next;
		}

	}


	return RET_OK;
}
/**
 *
 * @param rt_layer_data
 * @param net_str_val
 * @param net_addr_fd
 * @return
 */
static retval_t basic_rt_new_net_addr(rt_layer_data_t* rt_layer_data, char* net_str_val, char format, net_addr_t* net_addr_fd){
	if((net_addr_fd == NULL)){
		return RET_ERROR;
	}
	(*net_addr_fd) = (net_addr_t) mac_new_addr(net_str_val, format);
	return RET_OK;
}

/**
 *
 * @param rt_layer_data
 * @param net_addr_fd
 * @return
 */
static retval_t basic_rt_delete_net_addr(rt_layer_data_t* rt_layer_data, net_addr_t net_addr_fd){
	if(net_addr_fd == NULL){
		return RET_ERROR;
	}

	return mac_delete_addr((mac_addr_t) net_addr_fd);
}

/**
 *
 * @param rt_layer_data
 * @param net_addr_fd
 * @param net_str_val
 * @return
 */
static retval_t basic_rt_net_addr_to_string(rt_layer_data_t* rt_layer_data, net_addr_t net_addr_fd, char* net_str_val, char format){
	if((net_addr_fd == NULL) || (net_str_val == NULL)){
		return RET_ERROR;
	}
	return mac_addr_to_string((mac_addr_t) net_addr_fd, net_str_val, format);
}

/**
 *
 * @param rt_layer_data
 * @param dest_addr_fd
 * @param from_addr_fd
 * @return
 */
static retval_t basic_rt_net_addr_cpy(rt_layer_data_t* rt_layer_data, net_addr_t dest_addr_fd, net_addr_t from_addr_fd){
	if((dest_addr_fd == NULL) || (from_addr_fd == NULL)){
		return RET_ERROR;
	}

	return mac_addr_cpy((mac_addr_t) dest_addr_fd, (mac_addr_t) from_addr_fd);
}

/**
 *
 * @param rt_layer_data
 * @param a_addr_fd
 * @param b_addr_fd
 * @return
 */
static retval_t basic_rt_net_addr_cmp(rt_layer_data_t* rt_layer_data, net_addr_t a_addr_fd, net_addr_t b_addr_fd){
	if((a_addr_fd == NULL) || (b_addr_fd == NULL)){
		return RET_ERROR;
	}

	return mac_addr_cmp((mac_addr_t) a_addr_fd, (mac_addr_t) b_addr_fd);
}


/**
 *
 * @param rt_layer_data
 * @param dest_addr
 * @param next_addr
 * @return
 */
static retval_t find_dest_addr_route(rt_layer_data_t* rt_layer_data, basic_net_addr_t* dest_addr, basic_net_addr_t* next_addr){
	basic_route_t* route;
	basic_rt_layer_data_t* layer_data = (basic_rt_layer_data_t*) rt_layer_data;
	gen_list* current = layer_data->route_list;

	if(dest_addr->b_net_addr == BC_DEST_ADDR){
		next_addr->b_net_addr = BC_DEST_ADDR;
		return RET_OK;
	}

	while(current->next != NULL){
		route = (basic_route_t*) current->next->item;
		if(route->dest_addr.b_net_addr == dest_addr->b_net_addr){

			next_addr->b_net_addr = route->next_addr.b_net_addr;
			return RET_OK;

		}
		else{
			current = current->next;
		}

	}

	return RET_ERROR;
}
#endif
