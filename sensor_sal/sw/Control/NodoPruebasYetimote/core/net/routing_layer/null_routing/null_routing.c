/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * null_routing.c
 *
 *  Created on: 28 de nov. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file null_routing.c
 */

#include "routing_layer.h"
#include "mac_layer.h"
#include "transport_layer.h"
#include "null_routing.h"
#include "packetbuffer.h"

#if ENABLE_NETSTACK_ARCH

typedef struct null_rt_layer_data_{
	uint16_t nc;
}null_rt_layer_data_t;

typedef mac_addr_t null_rt_net_addr_t;

static retval_t null_rt_layer_init(rt_layer_data_t** rt_layer_data);
static retval_t null_rt_layer_deinit(rt_layer_data_t* rt_layer_data);

static retval_t null_rt_send_packet(rt_layer_data_t* rt_layer_data, net_addr_t addr_fd, net_packet_t* packet);
static retval_t null_rt_rcv_packet(rt_layer_data_t* rt_layer_data);

static retval_t null_rt_send_packet_done(rt_layer_data_t* rt_layer_data, net_packet_t* packet);
static retval_t null_rt_rcv_packet_done(rt_layer_data_t* rt_layer_data, net_packet_t* packet, mac_addr_t mac_from_addr);

static retval_t null_rt_add_node_addr(rt_layer_data_t* rt_layer_data, net_addr_t new_addr_fd);
static retval_t null_rt_remove_node_addr(rt_layer_data_t* rt_layer_data, net_addr_t new_addr_fd);
static retval_t null_rt_add_node_route(rt_layer_data_t* rt_layer_data, net_addr_t dest_addr_fd, net_addr_t next_addr_fd, uint16_t hops);
static retval_t null_rt_remove_node_route(rt_layer_data_t* rt_layer_data, net_addr_t dest_addr_fd, net_addr_t next_addr_fd, uint16_t hops);

static retval_t null_rt_new_net_addr(rt_layer_data_t* rt_layer_data, char* net_str_val, char format, net_addr_t* net_addr_fd);
static retval_t null_rt_delete_net_addr(rt_layer_data_t* rt_layer_data, net_addr_t net_addr_fd);
static retval_t null_rt_net_addr_to_string(rt_layer_data_t* rt_layer_data, net_addr_t net_addr_fd, char* net_str_val, char format);
static retval_t null_rt_net_addr_cpy(rt_layer_data_t* rt_layer_data, net_addr_t dest_addr_fd, net_addr_t from_addr_fd);
static retval_t null_rt_net_addr_cmp(rt_layer_data_t* rt_layer_data, net_addr_t a_addr_fd, net_addr_t b_addr_fd);

rt_layer_funcs_t null_routing_funcs = {
		.rt_layer_init = null_rt_layer_init,
		.rt_layer_deinit = null_rt_layer_deinit,
		.rt_send_packet = null_rt_send_packet,
		.rt_rcv_packet = null_rt_rcv_packet,
		.rt_send_packet_done = null_rt_send_packet_done,
		.rt_rcv_packet_done = null_rt_rcv_packet_done,
		.rt_add_node_addr = null_rt_add_node_addr,
		.rt_remove_node_addr = null_rt_remove_node_addr,
		.rt_add_node_route = null_rt_add_node_route,
		.rt_remove_node_route = null_rt_remove_node_route,
		.rt_new_net_addr = null_rt_new_net_addr,
		.rt_delete_net_addr = null_rt_delete_net_addr,
		.rt_net_addr_to_string = null_rt_net_addr_to_string,
		.rt_net_addr_cpy = null_rt_net_addr_cpy,
		.rt_net_addr_cmp = null_rt_net_addr_cmp,
};


static retval_t null_rt_layer_init(rt_layer_data_t** rt_layer_data){
	return RET_OK;
}
static retval_t null_rt_layer_deinit(rt_layer_data_t* rt_layer_data){
	return RET_OK;
}

static retval_t null_rt_send_packet(rt_layer_data_t* rt_layer_data, net_addr_t addr_fd, net_packet_t* packet){

	//Una capa de routing bien hecha deberia traducir la direccion ip que le llega por una mac.
	//Esta sin embargo no hace nada bypasea la direccion que le llega de arriba y envia el paquete sin tocarlo
	mac_send_packet((mac_addr_t) addr_fd, packet);
	return RET_OK;
}
static retval_t null_rt_rcv_packet(rt_layer_data_t* rt_layer_data){
	mac_read_packet();
	return RET_OK;
}

static retval_t null_rt_send_packet_done(rt_layer_data_t* rt_layer_data, net_packet_t* packet){
	tp_packet_sent(packet);
	return RET_OK;
}
static retval_t null_rt_rcv_packet_done(rt_layer_data_t* rt_layer_data, net_packet_t* packet, mac_addr_t mac_from_addr){
	tp_packet_received(packet, (net_addr_t) mac_from_addr);
	return RET_OK;
}

static retval_t null_rt_add_node_addr(rt_layer_data_t* rt_layer_data, net_addr_t new_addr_fd){
	mac_set_node_addr((mac_addr_t) new_addr_fd);
	return RET_OK;
}

static retval_t null_rt_remove_node_addr(rt_layer_data_t* rt_layer_data, net_addr_t new_addr_fd){
	return RET_ERROR;
}

static retval_t null_rt_add_node_route(rt_layer_data_t* rt_layer_data, net_addr_t dest_addr_fd, net_addr_t next_addr_fd, uint16_t hops){
	return RET_ERROR;
}

static retval_t null_rt_remove_node_route(rt_layer_data_t* rt_layer_data, net_addr_t dest_addr_fd, net_addr_t next_addr_fd, uint16_t hops){
	return RET_ERROR;
}
/**
 *
 * @param rt_layer_data
 * @param net_str_val
 * @param net_addr_fd
 * @return
 */
static retval_t null_rt_new_net_addr(rt_layer_data_t* rt_layer_data, char* net_str_val, char format, net_addr_t* net_addr_fd){
	if((net_str_val == NULL) || (net_addr_fd == NULL)){
		return RET_ERROR;
	}
	(*net_addr_fd) = (net_addr_t) mac_new_addr(net_str_val, format);
	return RET_OK;
}

/**
 *
 * @param rt_layer_data
 * @param net_addr_fd
 * @return
 */
static retval_t null_rt_delete_net_addr(rt_layer_data_t* rt_layer_data, net_addr_t net_addr_fd){
	if(net_addr_fd == NULL){
		return RET_ERROR;
	}

	return mac_delete_addr((mac_addr_t) net_addr_fd);
}

/**
 *
 * @param rt_layer_data
 * @param net_addr_fd
 * @param net_str_val
 * @return
 */
static retval_t null_rt_net_addr_to_string(rt_layer_data_t* rt_layer_data, net_addr_t net_addr_fd, char* net_str_val, char format){
	if((net_addr_fd == NULL) || (net_str_val == NULL)){
		return RET_ERROR;
	}
	return mac_addr_to_string((mac_addr_t) net_addr_fd, net_str_val, format);
}

/**
 *
 * @param rt_layer_data
 * @param dest_addr_fd
 * @param from_addr_fd
 * @return
 */
static retval_t null_rt_net_addr_cpy(rt_layer_data_t* rt_layer_data, net_addr_t dest_addr_fd, net_addr_t from_addr_fd){
	if((dest_addr_fd == NULL) || (from_addr_fd == NULL)){
		return RET_ERROR;
	}

	return mac_addr_cpy((mac_addr_t) dest_addr_fd, (mac_addr_t) from_addr_fd);
}

/**
 *
 * @param rt_layer_data
 * @param a_addr_fd
 * @param b_addr_fd
 * @return
 */
static retval_t null_rt_net_addr_cmp(rt_layer_data_t* rt_layer_data, net_addr_t a_addr_fd, net_addr_t b_addr_fd){
	if((a_addr_fd == NULL) || (b_addr_fd == NULL)){
		return RET_ERROR;
	}

	return mac_addr_cmp((mac_addr_t) a_addr_fd, (mac_addr_t) b_addr_fd);
}

#endif
