/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * routing_layer.c
 *
 *  Created on: 24 de nov. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file routing_layer.c
 */

#include "routing_layer.h"
#include "netstack.h"

#if ENABLE_NETSTACK_ARCH

#define DEBUG 0
#if DEBUG
#include "yetimote_stdio.h"
#define PRINTF(...) _printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

/**
 *
 * @param rt_layer_funcs
 * @return
 */

routing_layer_t* init_routing_layer(rt_layer_funcs_t* rt_layer_funcs){

	routing_layer_t* new_rt_layer = ytMalloc(sizeof(routing_layer_t));

	if(new_rt_layer == NULL){
		return NULL;
	}
	new_rt_layer->rt_layer_funcs = rt_layer_funcs;
	new_rt_layer->rt_layer_private_data = NULL;

	PRINTF("RT_LAYER: INIT\r\n");
	if(new_rt_layer->rt_layer_funcs->rt_layer_init(&(new_rt_layer->rt_layer_private_data)) != RET_OK){
		ytFree(new_rt_layer);
		PRINTF("RT_LAYER: INIT ERROR\r\n");
		return NULL;
	}

	PRINTF("RT_LAYER: INIT DONE\r\n");
	return new_rt_layer;
}


/**
 *
 * @param rt_layer
 * @return
 */
retval_t deinit_routing_layer(routing_layer_t* rt_layer){
	if(rt_layer == NULL){
		return RET_ERROR;
	}

	PRINTF("RT_LAYER: DEINIT\r\n");
	rt_layer->rt_layer_funcs->rt_layer_deinit(rt_layer->rt_layer_private_data);

	ytFree(rt_layer);

	PRINTF("RT_LAYER: DEINIT DONE\r\n");
	return RET_OK;
}

//* Called from transport layer
/**
 *
 * @param packet
 * @return
 */
retval_t rt_send_packet(net_addr_t dest_addr_fd, net_packet_t* packet){
	routing_layer_t* rt_layer =  netstack_get_rt_layer();

	netstack_state = NS_RT_SEND_PACKET;
	PRINTF("RT_LAYER: EVENT SEND PACKET\r\n");
	return rt_layer->rt_layer_funcs->rt_send_packet(rt_layer->rt_layer_private_data, dest_addr_fd, packet);
}

/**
 *
 * @return
 */
retval_t rt_read_packet(void){
	routing_layer_t* rt_layer =  netstack_get_rt_layer();


	netstack_state = NS_RT_READ_PACKET;
	PRINTF("RT_LAYER: EVENT READ PACKET\r\n");
	return rt_layer->rt_layer_funcs->rt_rcv_packet(rt_layer->rt_layer_private_data);

}

//* Called from mac layer
/**
 *
 * @param packet
 * @return
 */
retval_t rt_packet_sent(net_packet_t* packet){
	routing_layer_t* rt_layer =  netstack_get_rt_layer();

	netstack_state = NS_RT_PACKET_SENT;
	PRINTF("RT_LAYER: EVENT SEND PACKET DONE\r\n");
	return rt_layer->rt_layer_funcs->rt_send_packet_done(rt_layer->rt_layer_private_data, packet);

}
/**
 *
 * @param packet
 * @return
 */
retval_t rt_packet_received(net_packet_t* packet, mac_addr_t mac_from_addr){
	routing_layer_t* rt_layer =  netstack_get_rt_layer();

	netstack_state = NS_RT_PACKET_RECEIVED;
	PRINTF("RT_LAYER: EVENT READ PACKET DONE\r\n");
	return rt_layer->rt_layer_funcs->rt_rcv_packet_done(rt_layer->rt_layer_private_data, packet, mac_from_addr);

}


//* Called from user space or same layer
/**
 *
 * @param index
 * @return
 */
net_addr_t rt_get_node_addr(uint16_t index){
	net_addr_t node_addr;
	routing_layer_t* rt_layer =  netstack_get_rt_layer();

	PRINTF("RT_LAYER: GET NODE ADDR\r\n");

	if((rt_layer->rt_layer_funcs->rt_get_node_addr(rt_layer->rt_layer_private_data, index, &node_addr)) == RET_OK){
		 return node_addr;
	}
	else{
		return NULL;
	}
}

/**
 *
 * @param addr_fd
 * @return
 */
retval_t rt_add_node_addr(net_addr_t addr_fd){
	routing_layer_t* rt_layer =  netstack_get_rt_layer();

	PRINTF("RT_LAYER: ADD NODE ADDR\r\n");
	return rt_layer->rt_layer_funcs->rt_add_node_addr(rt_layer->rt_layer_private_data, addr_fd);

}

/**
 *
 * @param addr_fd
 * @return
 */
retval_t rt_remove_node_addr(net_addr_t addr_fd){
	routing_layer_t* rt_layer =  netstack_get_rt_layer();

	PRINTF("RT_LAYER: REMOVE NODE ADDR\r\n");
	return rt_layer->rt_layer_funcs->rt_remove_node_addr(rt_layer->rt_layer_private_data, addr_fd);
}
/**
 *
 * @param dest_addr_fd
 * @param next_addr_fd
 * @param hops
 * @return
 */
retval_t rt_add_node_route(net_addr_t dest_addr_fd, net_addr_t next_addr_fd, uint16_t hops){
	routing_layer_t* rt_layer =  netstack_get_rt_layer();

	PRINTF("RT_LAYER: ADD NODE ROUTE\r\n");
	return rt_layer->rt_layer_funcs->rt_add_node_route(rt_layer->rt_layer_private_data, dest_addr_fd, next_addr_fd, hops);
}

/**
 *
 * @param dest_addr_fd
 * @param next_addr_fd
 * @param hops
 * @return
 */
retval_t rt_remove_node_route(net_addr_t dest_addr_fd, net_addr_t next_addr_fd, uint16_t hops){
	routing_layer_t* rt_layer =  netstack_get_rt_layer();

	PRINTF("RT_LAYER: REMOVE NODE ROUTE\r\n");
	return rt_layer->rt_layer_funcs->rt_remove_node_route(rt_layer->rt_layer_private_data, dest_addr_fd, next_addr_fd, hops);
}
/**
 *
 * @param net_str_val
 * @return
 */
net_addr_t rt_new_net_addr(char* net_str_val, char format){
	net_addr_t net_addr;
	routing_layer_t* rt_layer =  netstack_get_rt_layer();
	if(net_str_val == NULL){
		return NULL;
	}
	PRINTF("RT_LAYER: NEW NET ADDR\r\n");
	rt_layer->rt_layer_funcs->rt_new_net_addr(rt_layer->rt_layer_private_data, net_str_val, format, &net_addr);
	return net_addr;
}

/**
 *
 * @return
 */
net_addr_t rt_new_empty_net_addr(void){
	net_addr_t net_addr;
	routing_layer_t* rt_layer =  netstack_get_rt_layer();
	PRINTF("RT_LAYER: NEW NET ADDR\r\n");
	rt_layer->rt_layer_funcs->rt_new_net_addr(rt_layer->rt_layer_private_data, NULL, 0, &net_addr);
	return net_addr;
}

/**
 *
 * @param net_addr
 * @return
 */
retval_t rt_delete_net_addr(net_addr_t net_addr){
	routing_layer_t* rt_layer =  netstack_get_rt_layer();
	if(net_addr == NULL){
		return RET_ERROR;
	}
	PRINTF("RT_LAYER: DELETE NET ADDR\r\n");
	return rt_layer->rt_layer_funcs->rt_delete_net_addr(rt_layer->rt_layer_private_data, net_addr);
}
/**
 *
 * @param net_addr
 * @return
 */
retval_t rt_net_addr_to_string(net_addr_t net_addr, char* net_addr_str_val, char format){
	routing_layer_t* rt_layer =  netstack_get_rt_layer();
	if((net_addr == NULL) || (net_addr_str_val == NULL)){
		return RET_ERROR;
	}
	return rt_layer->rt_layer_funcs->rt_net_addr_to_string(rt_layer->rt_layer_private_data, net_addr, net_addr_str_val, format);
}
/**
 *
 * @param dest_addr
 * @param from_addr
 * @return
 */
retval_t rt_net_addr_cpy(net_addr_t dest_addr, net_addr_t from_addr){
	routing_layer_t* rt_layer =  netstack_get_rt_layer();
	if((dest_addr == NULL) || (from_addr == NULL)){
		return RET_ERROR;
	}
	return rt_layer->rt_layer_funcs->rt_net_addr_cpy(rt_layer->rt_layer_private_data, dest_addr, from_addr);
}

/**
 *
 * @param a_addr
 * @param b_addr
 * @return
 */
uint16_t rt_net_addr_cmp(net_addr_t a_addr, net_addr_t b_addr){
	routing_layer_t* rt_layer =  netstack_get_rt_layer();
	if((a_addr == NULL) || (b_addr == NULL)){
		return 0;
	}
	if(rt_layer->rt_layer_funcs->rt_net_addr_cmp(rt_layer->rt_layer_private_data, a_addr, b_addr) == RET_OK){
		return 1;
	}
	return 0;
}

#endif
