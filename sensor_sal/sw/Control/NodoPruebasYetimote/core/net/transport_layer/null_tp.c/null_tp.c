/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * null_tp.c
 *
 *  Created on: 21 de nov. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file null_tp.c
 */

#include "transport_layer.h"
#include "routing_layer.h"
#include "null_tp.h"
#include "packetbuffer.h"

#if ENABLE_NETSTACK_ARCH

#define READING_FLAG		0x01
#define TRANSMITTING_FLAG	0x02

#define DEFAULT_CONNECTION_TIMEOUT	30000

#define PACKET_TX_TIMEOUT			250
#define MAX_NUM_RETRANSMISSIONS		2

typedef enum tp_packet_type_{
	PACKET_TYPE_U_SEND				= 0x01,
	PACKET_TYPE_R_SEND				= 0x02,
	PACKET_TYPE_R_SEND_ACK			= 0x03,
	PACKET_TYPE_R_CONNECT			= 0x04,
	PACKET_TYPE_R_CONNECT_ACK		= 0x05,
	PACKET_TYPE_R_DISCONNECT		= 0x06,
	PACKET_TYPE_R_DISCONNECT_ACK	= 0x07,
}tp_packet_type_t;

typedef struct tp_server_{

}tp_server_t;

typedef struct tp_connection_{

}tp_connection_t;


typedef struct null_tp_layer_data_{
	gen_list* unrel_port_list;
	gen_list* rel_conn_list;
	gen_list* rel_server_list;

}null_tp_layer_data_t;

typedef enum port_state_{
	NULL_TP_PORT_RX,
	NULL_TP_PORT_TX,
	NULL_TP_PORT_TX_RX,

	NULL_TP_PORT_CONNECTING,
	NULL_TP_PORT_DISCONNECTING,
	NULL_TP_PORT_CONNECTED,

	NULL_TP_SERVER_IDLE,
	NULL_TP_SERVER_LISTENING,

	NULL_TP_PORT_IDLE,
}port_state_t;


typedef struct unrel_tp_port_{
	uint8_t* rx_data_ptr;
	float32_t last_rssi;
	uint16_t port_number;
	port_state_t port_state;
}unrel_tp_port_t;

#if USE_RELIABLE_FUNCS

typedef struct rel_tp_server_{
	port_state_t port_state;
	uint16_t port_number;
}rel_tp_server_t;

typedef struct rel_tp_conn_{
	uint8_t* rx_data_ptr;
	net_packet_t* sending_packet;
	rel_tp_server_t* associated_server;
	tp_layer_data_t* tp_layer_data;
	uint32_t remote_conn_fd;
	net_addr_t dest_addr;
	uint32_t retransmission_timer;
	uint32_t inactive_timer;
	uint32_t connection_timeout;
	float32_t last_rssi;
	uint16_t port_number;
	port_state_t port_state;
	uint16_t last_packet_size;
	uint8_t retransmission_num;
	uint8_t reading_transmitting;
}rel_tp_conn_t;

#endif

static retval_t null_tp_layer_init(tp_layer_data_t** tp_layer_data);
static retval_t null_tp_layer_deinit(tp_layer_data_t* tp_layer_data);

static retval_t null_tp_u_open_port(tp_layer_data_t* tp_layer_data, uint16_t port_number);
static retval_t null_tp_u_close_port(tp_layer_data_t* tp_layer_data, uint16_t port_number);
static retval_t null_tp_u_send(tp_layer_data_t* tp_layer_data, net_addr_t dest_addr_fd, uint16_t port_number, uint8_t* data, uint32_t size);
static retval_t null_tp_u_rcv(tp_layer_data_t* tp_layer_data, uint16_t port_number, uint8_t* data, uint32_t size);
static retval_t null_tp_u_get_last_signal_level(tp_layer_data_t* tp_layer_data, uint16_t port, float32_t* signal_level);

static retval_t null_tp_send_packet_done(tp_layer_data_t* tp_layer_data, net_packet_t* packet_sent);
static retval_t null_tp_rcv_packet_done(tp_layer_data_t* tp_layer_data, net_packet_t* rcv_packet, net_addr_t from_addr);

static retval_t null_tp_r_create_server(tp_layer_data_t* tp_layer_data, uint16_t port_number, uint32_t* server_fd);
static retval_t null_tp_r_delete_server(tp_layer_data_t* tp_layer_data, uint32_t server_fd);
static retval_t null_tp_r_server_accept_connection(tp_layer_data_t* tp_layer_data, uint32_t server_fd);
static retval_t null_tp_r_connect_to_server(tp_layer_data_t* tp_layer_data, net_addr_t dest_addr_fd, uint16_t port_number);
static retval_t null_tp_r_send(tp_layer_data_t* tp_layer_data, uint32_t connection_fd, uint8_t* data, uint32_t size);
static retval_t null_tp_r_rcv(tp_layer_data_t* tp_layer_data, uint32_t connection_fd, uint8_t* data, uint32_t size);
static retval_t null_tp_r_close_connection(tp_layer_data_t* tp_layer_data, uint32_t connection_fd);
static retval_t null_tp_r_check_conn(tp_layer_data_t* tp_layer_data, uint32_t connection_fd);
static retval_t null_tp_r_get_last_signal_level(tp_layer_data_t* tp_layer_data, uint32_t connection_fd, float32_t* signal_level);
static retval_t null_tp_r_set_connection_timeout(tp_layer_data_t* tp_layer_data, uint32_t connection_fd, uint32_t timeout);

static unrel_tp_port_t* get_unrel_port_in_list(null_tp_layer_data_t* tp_layer_data, uint16_t port_number);
static uint16_t is_port_used(null_tp_layer_data_t* tp_layer_data, uint16_t port_number);

#if USE_RELIABLE_FUNCS
static retval_t delete_rel_conn(null_tp_layer_data_t* tp_layer_data, rel_tp_conn_t* rel_conn);
static rel_tp_conn_t* get_rel_conn_in_list(null_tp_layer_data_t* tp_layer_data, uint16_t port_number);
static rel_tp_server_t* get_rel_server_in_list(null_tp_layer_data_t* tp_layer_data, uint16_t port_number);
static uint16_t is_server_in_list(null_tp_layer_data_t* tp_layer_data, rel_tp_server_t* server);
static uint16_t check_rel_conn_port_in_list(null_tp_layer_data_t* tp_layer_data, rel_tp_conn_t* rel_conn, uint16_t rel_port);
static uint16_t is_rel_conn_in_list(null_tp_layer_data_t* tp_layer_data, rel_tp_conn_t* rel_conn);

static void inactive_timeout_cb(void const* args);
static void retransmission_timeout_cb(void const* args);
#endif

tp_layer_funcs_t null_transport_funcs = {
		.tp_layer_init = null_tp_layer_init,
		.tp_layer_deinit = null_tp_layer_deinit,
		.u_open_port = null_tp_u_open_port,
		.u_close_port = null_tp_u_close_port,
		.u_send = null_tp_u_send,
		.u_rcv = null_tp_u_rcv,
		.u_get_last_signal_level = null_tp_u_get_last_signal_level,
		.r_create_server = null_tp_r_create_server,
		.r_delete_server = null_tp_r_delete_server,
		.r_server_accept_connection = null_tp_r_server_accept_connection,
		.r_connect_to_server = null_tp_r_connect_to_server,
		.r_send = null_tp_r_send,
		.r_rcv = null_tp_r_rcv,
		.r_close_connection = null_tp_r_close_connection,
		.r_check_conn = null_tp_r_check_conn,
		.r_get_last_signal_level = null_tp_r_get_last_signal_level,
		.r_set_connection_timeout = null_tp_r_set_connection_timeout,
		.send_packet_done = null_tp_send_packet_done,
		.rcv_packet_done = null_tp_rcv_packet_done,
};

/**
 *
 * @return
 */
static retval_t null_tp_layer_init(tp_layer_data_t** tp_layer_data){
	null_tp_layer_data_t* new_data;
	if(tp_layer_data == NULL){
		return RET_ERROR;
	}
	new_data = (null_tp_layer_data_t*) ytMalloc(sizeof(null_tp_layer_data_t));

	new_data->unrel_port_list = gen_list_init();
	new_data->rel_conn_list = gen_list_init();
	new_data->rel_server_list = gen_list_init();

	(*tp_layer_data) = (tp_layer_data_t*) new_data;
	return RET_OK;


}
/**
 *
 * @param tp_layer_data
 * @return
 */
static retval_t null_tp_layer_deinit(tp_layer_data_t* tp_layer_data){

	null_tp_layer_data_t* layer_data = (null_tp_layer_data_t*)tp_layer_data;
	if(tp_layer_data == NULL){
		return RET_ERROR;
	}
	gen_list_remove_all(layer_data->unrel_port_list);
	gen_list_remove_all(layer_data->rel_conn_list);
	gen_list_remove_all(layer_data->rel_server_list);

	ytFree(tp_layer_data);

	return RET_OK;

}



/* Unreliable funcs */
/**
 *
 * @param tp_layer_data
 * @param port_number
 * @return
 */
static retval_t null_tp_u_open_port(tp_layer_data_t* tp_layer_data, uint16_t port_number){
	null_tp_layer_data_t* layer_data = (null_tp_layer_data_t*)tp_layer_data;
	unrel_tp_port_t* new_port;
	if(tp_layer_data == NULL){
		return RET_ERROR;
	}
	if(is_port_used(layer_data, port_number)){	//The port is already in the list, so it can not be opened again
		return RET_ERROR;
	}

	new_port = (unrel_tp_port_t*) ytMalloc(sizeof(unrel_tp_port_t));
	new_port->port_number = port_number;
	new_port->port_state = NULL_TP_PORT_IDLE;
	new_port->last_rssi = -180;
	new_port->rx_data_ptr = NULL;
	gen_list_add(layer_data->unrel_port_list, (void*) new_port);

	return RET_OK;
}
/**
 *
 * @param tp_layer_data
 * @param port_number
 * @return
 */
static retval_t null_tp_u_close_port(tp_layer_data_t* tp_layer_data, uint16_t port_number){
	null_tp_layer_data_t* layer_data = (null_tp_layer_data_t*)tp_layer_data;
	unrel_tp_port_t* port;
	if(tp_layer_data == NULL){
		return RET_ERROR;
	}
	if((port = get_unrel_port_in_list(layer_data, port_number)) == NULL){	//The port is not in the list, so it can not be closed
		return RET_ERROR;
	}

	gen_list_remove(layer_data->unrel_port_list, (void*) port);
	ytFree(port);

	return RET_OK;
}

/**
 *
 * @param tp_layer_data
 * @param dest_addr_fd
 * @param port_number
 * @param data
 * @param size
 * @return
 */
static retval_t null_tp_u_send(tp_layer_data_t* tp_layer_data, net_addr_t dest_addr_fd, uint16_t port_number, uint8_t* data, uint32_t size){
	null_tp_layer_data_t* layer_data = (null_tp_layer_data_t*)tp_layer_data;
	net_packet_t* packet;
	unrel_tp_port_t* port;
	port_state_t prev_state;
	if(tp_layer_data == NULL){
		return RET_ERROR;
	}
	if(size > PACKET_DATA_SIZE){		//Not enought space in a single packet
		return RET_ERROR;
	}
	if((port = get_unrel_port_in_list(layer_data, port_number)) == NULL){	//The port is not in the list, so it cannot be used
		return RET_ERROR;
	}

	prev_state = port->port_state;
	if(port->port_state == NULL_TP_PORT_RX){
		port->port_state = NULL_TP_PORT_TX_RX;
	}
	else if(port->port_state == NULL_TP_PORT_IDLE){
		port->port_state = NULL_TP_PORT_TX;
	}
	else{		//If the port is already transmitting return error
		return RET_ERROR;
	}

	if((packet = tx_packetbuffer_get_free_packet()) == NULL){
		port->port_state = prev_state;
		return RET_ERROR;
	}

	packet->header.tp_hdr.port_num = port_number;
	packet->header.tp_hdr.data_size = (uint16_t) size;
	packet->header.interlayer_hdr.inter_transport_hdr_bits = (uint8_t) PACKET_TYPE_U_SEND;
	memcpy(packet->data, data, size);

	return rt_send_packet(dest_addr_fd, packet);

	return RET_OK;
}
/**
 *
 * @param tp_layer_data
 * @param port_number
 * @param data
 * @param size
 * @return
 */
static retval_t null_tp_u_rcv(tp_layer_data_t* tp_layer_data, uint16_t port_number, uint8_t* data, uint32_t size){
	null_tp_layer_data_t* layer_data = (null_tp_layer_data_t*)tp_layer_data;
	unrel_tp_port_t* port;
	if(tp_layer_data == NULL){
		return RET_ERROR;
	}
	if(size > PACKET_DATA_SIZE){		//Not enought space in a single packet
		return RET_ERROR;
	}
	if((port = get_unrel_port_in_list(layer_data, port_number)) == NULL){	//The port is not in the list, so it cannot be used
		return RET_ERROR;
	}
	if(port->port_state == NULL_TP_PORT_TX){
		port->port_state = NULL_TP_PORT_TX_RX;
	}
	else if(port->port_state == NULL_TP_PORT_IDLE){
		port->port_state = NULL_TP_PORT_RX;
	}
	else{		//If the port is already receiving return error
		return RET_ERROR;
	}
	port->rx_data_ptr = data;

	rt_read_packet();

	return RET_OK;
}

/**
 *
 * @param tp_layer_data
 * @param port
 * @param signal_level
 * @return
 */
static retval_t null_tp_u_get_last_signal_level(tp_layer_data_t* tp_layer_data, uint16_t port, float32_t* signal_level){
	null_tp_layer_data_t* layer_data = (null_tp_layer_data_t*)tp_layer_data;
	unrel_tp_port_t* port_fd;
	if(tp_layer_data == NULL){
		return RET_ERROR;
	}
	if((port_fd = get_unrel_port_in_list(layer_data, port)) == NULL){	//The port is not in the list, so it cannot be used
		return RET_ERROR;
	}
	(*signal_level) = port_fd->last_rssi;

	return RET_OK;
}

/* Reliable funcs */
/**
 *
 * @param tp_layer_data
 * @param port_number
 * @param server_fd
 * @return
 */
static retval_t null_tp_r_create_server(tp_layer_data_t* tp_layer_data, uint16_t port_number, uint32_t* server_fd){
#if USE_RELIABLE_FUNCS
	null_tp_layer_data_t* layer_data = (null_tp_layer_data_t*)tp_layer_data;
	rel_tp_server_t* new_server;

	if(tp_layer_data == NULL){
		return RET_ERROR;
	}
	if(is_port_used(tp_layer_data, port_number)){	//The port is already in the list, so it can not be opened again
		return RET_ERROR;
	}

	new_server = (rel_tp_server_t*) ytMalloc(sizeof(rel_tp_server_t));
	new_server->port_number = port_number;
	new_server->port_state = NULL_TP_SERVER_IDLE;

	gen_list_add(layer_data->rel_server_list, (void*) new_server);

	(*server_fd) =  (uint32_t) new_server;
	return RET_OK;
#else
	(*server_fd) =  0;
	return RET_ERROR;
#endif

}

/**
 *
 * @param tp_layer_data
 * @param server_fd
 * @return
 */
static retval_t null_tp_r_delete_server(tp_layer_data_t* tp_layer_data, uint32_t server_fd){
#if USE_RELIABLE_FUNCS
	rel_tp_conn_t* rel_conn;
	null_tp_layer_data_t* layer_data = (null_tp_layer_data_t*)tp_layer_data;
	rel_tp_server_t* server = (rel_tp_server_t*) server_fd;
	gen_list* current = layer_data->rel_conn_list;

	if(server == NULL){
		return RET_ERROR;
	}

	while(current->next != NULL){		//Cierro todas las conexiones asociadas a este server
		rel_conn = (rel_tp_conn_t*) current->next->item;
		if(rel_conn->associated_server == server){
			null_tp_r_close_connection(tp_layer_data, (uint32_t) rel_conn);
		}

		current = current->next;
	}

	return RET_OK;
#else
	return RET_ERROR;
#endif
}

/**
 *
 * @param tp_layer_data
 * @param server_fd
 * @return
 */
static retval_t null_tp_r_server_accept_connection(tp_layer_data_t* tp_layer_data, uint32_t server_fd){
#if USE_RELIABLE_FUNCS
	rel_tp_server_t* server = (rel_tp_server_t*) server_fd;

	if(tp_layer_data == NULL){
		return RET_ERROR;
	}
	if(!is_server_in_list(tp_layer_data, server)){
		return RET_ERROR;
	}


	server->port_state = NULL_TP_SERVER_LISTENING;

	rt_read_packet();	//Espero recibir un mensaje de conexion
	return RET_OK;
#else
	tp_post_r_accepted_connection((uint32_t) NULL, (uint32_t) server_fd, (net_addr_t) NULL);
	return RET_ERROR;
#endif
}


/**
 *
 * @param tp_layer_data
 * @param dest_addr_fd
 * @param port_number
 * @return
 */
static retval_t null_tp_r_connect_to_server(tp_layer_data_t* tp_layer_data, net_addr_t dest_addr_fd, uint16_t port_number){
#if USE_RELIABLE_FUNCS
	net_packet_t* packet;
	null_tp_layer_data_t* layer_data = (null_tp_layer_data_t*)tp_layer_data;
	rel_tp_conn_t* new_conn;

	if(tp_layer_data == NULL){
		return RET_ERROR;
	}
	if(is_port_used(layer_data, port_number)){	//The port is already in the list, so it can not be opened again
		return RET_ERROR;
	}
	if((packet = tx_packetbuffer_get_free_packet()) ==NULL){
		return RET_ERROR;
	}

	new_conn = (rel_tp_conn_t*) ytMalloc(sizeof(rel_tp_conn_t));
	new_conn->port_number = port_number;
	new_conn->port_state = NULL_TP_PORT_CONNECTING;
	new_conn->rx_data_ptr = NULL;
	new_conn->retransmission_timer = ytTimerCreate(ytTimerOnce, retransmission_timeout_cb, (void*) new_conn);
	new_conn->inactive_timer = ytTimerCreate(ytTimerOnce, inactive_timeout_cb, (void*) new_conn);
	new_conn->retransmission_num = 0;
	new_conn->dest_addr = rt_new_empty_net_addr();
	rt_net_addr_cpy(new_conn->dest_addr, dest_addr_fd);
	new_conn->associated_server = NULL;
	new_conn->tp_layer_data = layer_data;
	new_conn->remote_conn_fd = 0;
	new_conn->reading_transmitting = 0;
	new_conn->last_packet_size = 0;
	new_conn->connection_timeout = DEFAULT_CONNECTION_TIMEOUT;
	gen_list_add(layer_data->rel_conn_list, (void*) new_conn);


	packet->header.tp_hdr.port_num = port_number;
	packet->header.tp_hdr.dest_conn_num = 0;	//Aun no tengo identificador de conexion remoto (me debera llegar con el ack)
	packet->header.interlayer_hdr.inter_transport_hdr_bits = (uint8_t) PACKET_TYPE_R_CONNECT;

	packet->header.tp_hdr.data_size = 4;		//Envio 4 bytes con mi identificador de conexion
	memcpy(packet->data, &new_conn, 4);

	new_conn->sending_packet = packet;

	rt_read_packet();	//Espero recibir un ACK

	rt_send_packet(new_conn->dest_addr, packet);

	return RET_OK;
#else
	tp_post_r_connected((uint32_t) NULL, port_number);
	return RET_ERROR;
#endif
}

/**
 *
 * @param tp_layer_data
 * @param connection_fd
 * @param data
 * @param size
 * @return
 */
static retval_t null_tp_r_send(tp_layer_data_t* tp_layer_data, uint32_t connection_fd, uint8_t* data, uint32_t size){
#if USE_RELIABLE_FUNCS
	net_packet_t* packet;
	null_tp_layer_data_t* layer_data = (null_tp_layer_data_t*)tp_layer_data;
	rel_tp_conn_t* rel_conn = (rel_tp_conn_t*) connection_fd;

	if(tp_layer_data == NULL){
		return RET_ERROR;
	}
	if(rel_conn == NULL){
		return RET_ERROR;
	}
	if((size <= 0) || (size > PACKET_DATA_SIZE)){
		return RET_ERROR;
	}
	if(!is_rel_conn_in_list(layer_data, rel_conn)){
		return RET_ERROR;
	}
	if(rel_conn->port_state != NULL_TP_PORT_CONNECTED){
		return RET_ERROR;
	}
	if(rel_conn->reading_transmitting & TRANSMITTING_FLAG){	//No puedo hacer varias transmisiones a la vez
		return RET_ERROR;
	}

	rel_conn->reading_transmitting |= TRANSMITTING_FLAG;

	if((packet = tx_packetbuffer_get_free_packet()) == NULL){
		rel_conn->reading_transmitting &= ~TRANSMITTING_FLAG;
		return RET_ERROR;
	}
	rel_conn->last_packet_size = size;

	packet->header.tp_hdr.port_num = rel_conn->port_number;
	packet->header.tp_hdr.dest_conn_num = rel_conn->remote_conn_fd;
	packet->header.interlayer_hdr.inter_transport_hdr_bits = (uint8_t) PACKET_TYPE_R_SEND;

	packet->header.tp_hdr.data_size = size;		//Relleno el paquete
	memcpy(packet->data, data, size);

	rel_conn->sending_packet = packet;

	packet->extra_info = (void*) rel_conn;	//Relleno este campo con mi instancia para ser utilizado en el callback de sent done

	rt_read_packet();	//Espero recibir un ACK
	rt_send_packet(rel_conn->dest_addr, packet);

	return RET_OK;
#else
	tp_post_r_send_done((uint32_t) connection_fd, 0);
	return RET_ERROR;
#endif
}

/**
 *
 * @param tp_layer_data
 * @param connection_fd
 * @param data
 * @param size
 * @return
 */
static retval_t null_tp_r_rcv(tp_layer_data_t* tp_layer_data, uint32_t connection_fd, uint8_t* data, uint32_t size){
#if USE_RELIABLE_FUNCS
	null_tp_layer_data_t* layer_data = (null_tp_layer_data_t*)tp_layer_data;
	rel_tp_conn_t* rel_conn = (rel_tp_conn_t*) connection_fd;

	if(tp_layer_data == NULL){
		return RET_ERROR;
	}
	if(rel_conn == NULL){
		return RET_ERROR;
	}
	if((size <= 0) || (size > PACKET_DATA_SIZE)){
		return RET_ERROR;
	}
	if(!is_rel_conn_in_list(layer_data, rel_conn)){
		return RET_ERROR;
	}
	if(rel_conn->port_state != NULL_TP_PORT_CONNECTED){
		return RET_ERROR;
	}
	if(rel_conn->reading_transmitting & READING_FLAG){	//No puedo hacer varias lecturas a la vez
		return RET_ERROR;
	}

	rel_conn->rx_data_ptr = data;
	rel_conn->reading_transmitting |= READING_FLAG;

	rt_read_packet();	//Espero leer un paquete

	return RET_OK;
#else
	tp_post_r_rcv_done((uint32_t) connection_fd, 0);
	return RET_ERROR;
#endif
}

/**
 *
 * @param tp_layer_data
 * @param connection_fd
 * @return
 */
static retval_t null_tp_r_close_connection(tp_layer_data_t* tp_layer_data, uint32_t connection_fd){
#if USE_RELIABLE_FUNCS
	net_packet_t* packet;
	null_tp_layer_data_t* layer_data = (null_tp_layer_data_t*)tp_layer_data;
	rel_tp_conn_t* rel_conn = (rel_tp_conn_t*) connection_fd;

	if(tp_layer_data == NULL){
		return RET_ERROR;
	}
	if(rel_conn == NULL){
		return RET_ERROR;
	}
	if(!is_rel_conn_in_list(layer_data, rel_conn)){
		return RET_ERROR;
	}
	if(rel_conn->port_state != NULL_TP_PORT_CONNECTED){
		return RET_ERROR;
	}

	if((packet = tx_packetbuffer_get_free_packet()) == NULL){
		return RET_ERROR;
	}
	packet->header.tp_hdr.port_num = rel_conn->port_number;
	packet->header.tp_hdr.dest_conn_num = rel_conn->remote_conn_fd;
	packet->header.interlayer_hdr.inter_transport_hdr_bits = (uint8_t) PACKET_TYPE_R_DISCONNECT;

	packet->header.tp_hdr.data_size = 4;		//Envio 4 bytes con mi identificador de conexion
	memcpy(packet->data, &rel_conn, 4);

	rel_conn->sending_packet = packet;
	rel_conn->port_state = NULL_TP_PORT_DISCONNECTING;
	rt_read_packet();	//Espero recibir un ACK

	rt_send_packet(rel_conn->dest_addr, packet);

	return RET_OK;
#else
	tp_post_r_disconnected(connection_fd, RET_ERROR);
	return RET_ERROR;
#endif
}

/**
 *
 * @param tp_layer_data
 * @param connection_fd
 * @return
 */
static retval_t null_tp_r_check_conn(tp_layer_data_t* tp_layer_data, uint32_t connection_fd){
#if USE_RELIABLE_FUNCS
	null_tp_layer_data_t* layer_data = (null_tp_layer_data_t*)tp_layer_data;
	rel_tp_conn_t* rel_conn = (rel_tp_conn_t*) connection_fd;

	if(tp_layer_data == NULL){
		return RET_ERROR;
	}
	if(rel_conn == NULL){
		return RET_ERROR;
	}
	if(!is_rel_conn_in_list(layer_data, rel_conn)){
		return RET_ERROR;
	}
	if(rel_conn->port_state != NULL_TP_PORT_CONNECTED){
		return RET_ERROR;
	}

	return RET_OK;
#else
	return RET_ERROR;
#endif
}

/**
 *
 * @param tp_layer_data
 * @param connection_fd
 * @param signal_level
 * @return
 */
static retval_t null_tp_r_get_last_signal_level(tp_layer_data_t* tp_layer_data, uint32_t connection_fd, float32_t* signal_level){
#if USE_RELIABLE_FUNCS
	null_tp_layer_data_t* layer_data = (null_tp_layer_data_t*)tp_layer_data;
	rel_tp_conn_t* rel_conn = (rel_tp_conn_t*) connection_fd;

	if(tp_layer_data == NULL){
		return RET_ERROR;
	}
	if(rel_conn == NULL){
		return RET_ERROR;
	}
	if(!is_rel_conn_in_list(layer_data, rel_conn)){
		return RET_ERROR;
	}
	(*signal_level) = rel_conn->last_rssi;

	return RET_OK;
#else
	return RET_ERROR;
#endif
}

/**
 *
 * @param tp_layer_data
 * @param connection_fd
 * @param timeout
 * @return
 */
static retval_t null_tp_r_set_connection_timeout(tp_layer_data_t* tp_layer_data, uint32_t connection_fd, uint32_t timeout){
#if USE_RELIABLE_FUNCS
	null_tp_layer_data_t* layer_data = (null_tp_layer_data_t*)tp_layer_data;
	rel_tp_conn_t* rel_conn = (rel_tp_conn_t*) connection_fd;

	if(tp_layer_data == NULL){
		return RET_ERROR;
	}
	if(rel_conn == NULL){
		return RET_ERROR;
	}
	if(!is_rel_conn_in_list(layer_data, rel_conn)){
		return RET_ERROR;
	}
	if(timeout == 0){
		timeout = YT_WAIT_FOREVER;
	}
	rel_conn->connection_timeout = timeout;

	return RET_OK;
#else
	return RET_ERROR;
#endif
}
/* Send and receive packets callbacks */
/**
 *
 * @param tp_layer_data
 * @param packet_sent
 * @return
 */
static retval_t null_tp_send_packet_done(tp_layer_data_t* tp_layer_data, net_packet_t* packet_sent){
	null_tp_layer_data_t* layer_data = (null_tp_layer_data_t*)tp_layer_data;
	unrel_tp_port_t* unrel_port;
#if USE_RELIABLE_FUNCS
	rel_tp_conn_t* rel_conn;
#endif
	if(tp_layer_data == NULL){
		tx_packetbuffer_release_packet(packet_sent);
		return RET_ERROR;
	}
	/*  ********* PACKET_TYPE_U_SEND **********/
	if(packet_sent->header.interlayer_hdr.inter_transport_hdr_bits == PACKET_TYPE_U_SEND){
		if((unrel_port = get_unrel_port_in_list(layer_data, packet_sent->header.tp_hdr.port_num)) == NULL){	//The port is not in the list, so it cannot be used
			tx_packetbuffer_release_packet(packet_sent);
			return RET_ERROR;
		}
		if(unrel_port->port_state == NULL_TP_PORT_TX){
			unrel_port->port_state = NULL_TP_PORT_IDLE;
		}
		else if(unrel_port->port_state == NULL_TP_PORT_TX_RX){
			unrel_port->port_state = NULL_TP_PORT_RX;
		}
		else{
			tx_packetbuffer_release_packet(packet_sent);
			return RET_ERROR;
		}

		tx_packetbuffer_release_packet(packet_sent);
		return tp_u_send_done(packet_sent->header.tp_hdr.port_num, packet_sent->header.tp_hdr.data_size);
	}
#if USE_RELIABLE_FUNCS
	/*  *********** PACKET_TYPE_R_CONNECT ********/
	else if(packet_sent->header.interlayer_hdr.inter_transport_hdr_bits == PACKET_TYPE_R_CONNECT){
		memcpy(&rel_conn, packet_sent->data, 4);
		if(rel_conn == NULL){							//Mi numero de conexion enviado al otro nodo
			tx_packetbuffer_release_packet(packet_sent);
			return RET_ERROR;
		}
		if(!check_rel_conn_port_in_list(layer_data, rel_conn, packet_sent->header.tp_hdr.port_num)){		//Confirmo que la conexion esta en mi lista
			tx_packetbuffer_release_packet(packet_sent);
			return RET_ERROR;
		}
		if(rel_conn->port_state != NULL_TP_PORT_CONNECTING){
			tx_packetbuffer_release_packet(packet_sent);
			return RET_ERROR;
		}
		ytTimerStart(rel_conn->retransmission_timer, PACKET_TX_TIMEOUT);
	}

	/*  ********* PACKET_TYPE_R_DISCONNECT **********/
	else if(packet_sent->header.interlayer_hdr.inter_transport_hdr_bits == PACKET_TYPE_R_DISCONNECT){
		memcpy(&rel_conn, packet_sent->data, 4);
		if(rel_conn == NULL){							//Mi numero de conexion enviado al otro nodo
			tx_packetbuffer_release_packet(packet_sent);
			return RET_ERROR;
		}
		if(!check_rel_conn_port_in_list(layer_data, rel_conn, packet_sent->header.tp_hdr.port_num)){		//Confirmo que la conexion esta en mi lista
			tx_packetbuffer_release_packet(packet_sent);
			return RET_ERROR;
		}
		if(rel_conn->port_state != NULL_TP_PORT_DISCONNECTING){
			tx_packetbuffer_release_packet(packet_sent);
			return RET_ERROR;
		}
		ytTimerStart(rel_conn->retransmission_timer, PACKET_TX_TIMEOUT);
	}

	/*  ********* PACKET_TYPE_R_CONNECT_ACK **********/
	else if(packet_sent->header.interlayer_hdr.inter_transport_hdr_bits == PACKET_TYPE_R_CONNECT_ACK){

	}

	/*  ********* PACKET_TYPE_R_DISCONNECT_ACK **********/
	else if(packet_sent->header.interlayer_hdr.inter_transport_hdr_bits == PACKET_TYPE_R_DISCONNECT_ACK){
	}

	/*  ********* PACKET_TYPE_R_SEND **********/
	else if(packet_sent->header.interlayer_hdr.inter_transport_hdr_bits == PACKET_TYPE_R_SEND){
		if((rel_conn = (rel_tp_conn_t*) packet_sent->extra_info) == NULL){	//Mi numero de conexion
			tx_packetbuffer_release_packet(packet_sent);
			return RET_ERROR;
		}
		if(!check_rel_conn_port_in_list(layer_data, rel_conn, packet_sent->header.tp_hdr.port_num)){		//Confirmo que la conexion esta en mi lista
			tx_packetbuffer_release_packet(packet_sent);
			return RET_ERROR;
		}
		if(rel_conn->port_state != NULL_TP_PORT_CONNECTED){
			tx_packetbuffer_release_packet(packet_sent);
			return RET_ERROR;
		}
		ytTimerStart(rel_conn->retransmission_timer, PACKET_TX_TIMEOUT);
	}

	/*  ********* PACKET_TYPE_R_SEND_ACK **********/
	else if(packet_sent->header.interlayer_hdr.inter_transport_hdr_bits == PACKET_TYPE_R_SEND_ACK){

	}
#endif
	tx_packetbuffer_release_packet(packet_sent);
	return RET_OK;
}
/**
 *
 * @param tp_layer_data
 * @param rcv_packet
 * @return
 */
static retval_t null_tp_rcv_packet_done(tp_layer_data_t* tp_layer_data, net_packet_t* rcv_packet, net_addr_t from_addr){
	null_tp_layer_data_t* layer_data = (null_tp_layer_data_t*)tp_layer_data;
	unrel_tp_port_t* unrel_port;
#if USE_RELIABLE_FUNCS
	net_packet_t* ack_packet;
	rel_tp_conn_t* rel_conn;
	rel_tp_server_t* rel_server;
#endif
	if(tp_layer_data == NULL){
		rx_packetbuffer_release_packet(rcv_packet);
		return RET_ERROR;
	}
	/*  ******** PACKET_TYPE_U_SEND ***********/
	if(rcv_packet->header.interlayer_hdr.inter_transport_hdr_bits == PACKET_TYPE_U_SEND){
		if((unrel_port = get_unrel_port_in_list(layer_data, rcv_packet->header.tp_hdr.port_num)) == NULL){	//The port is not in the list, so it cannot be used
			goto TP_RCV_ERROR;
		}
		if(unrel_port->port_state == NULL_TP_PORT_RX){
			unrel_port->port_state = NULL_TP_PORT_IDLE;
		}
		else if(unrel_port->port_state == NULL_TP_PORT_TX_RX){
			unrel_port->port_state = NULL_TP_PORT_TX;
		}
		else{
			goto TP_RCV_ERROR;
		}
		memcpy(unrel_port->rx_data_ptr, rcv_packet->data, rcv_packet->header.tp_hdr.data_size);
		unrel_port->last_rssi = rcv_packet->pckt_rssi;
		tp_u_rcv_done(rcv_packet->header.tp_hdr.port_num, rcv_packet->header.tp_hdr.data_size, from_addr);
	}
#if USE_RELIABLE_FUNCS
	/*  ******* PACKET_TYPE_R_CONNECT ************/
	else if(rcv_packet->header.interlayer_hdr.inter_transport_hdr_bits == PACKET_TYPE_R_CONNECT){
		if((rel_server = get_rel_server_in_list(layer_data, rcv_packet->header.tp_hdr.port_num)) == NULL){	//The server is not in the list, so it cannot be used
			goto TP_RCV_ERROR;
		}
		if(rel_server->port_state != NULL_TP_SERVER_LISTENING){
			goto TP_RCV_ERROR;
		}
		if((ack_packet = tx_packetbuffer_get_free_packet()) == NULL){
			goto TP_RCV_ERROR;
		}
		rel_server->port_state = NULL_TP_SERVER_IDLE;

		rel_conn = (rel_tp_conn_t*) ytMalloc(sizeof(rel_tp_conn_t));
		rel_conn->port_number = rel_server->port_number;
		rel_conn->port_state = NULL_TP_PORT_CONNECTED;
		rel_conn->rx_data_ptr = NULL;
		rel_conn->retransmission_timer = ytTimerCreate(ytTimerOnce, retransmission_timeout_cb, (void*) rel_conn);
		rel_conn->inactive_timer = ytTimerCreate(ytTimerOnce, inactive_timeout_cb, (void*) rel_conn);
		rel_conn->retransmission_num = 0;
		rel_conn->reading_transmitting = 0;
		rel_conn->last_packet_size = 0;
		rel_conn->connection_timeout = DEFAULT_CONNECTION_TIMEOUT;
		rel_conn->dest_addr = rt_new_empty_net_addr();
		rt_net_addr_cpy(rel_conn->dest_addr, from_addr);
		rel_conn->associated_server = rel_server;
		rel_conn->tp_layer_data = layer_data;
		memcpy(&(rel_conn->remote_conn_fd),rcv_packet->data, 4); //Guardo el numero de conexion remoto que me han enviado
		gen_list_add(layer_data->rel_conn_list, (void*) rel_conn);

		ytTimerStart(rel_conn->inactive_timer, rel_conn->connection_timeout);	//Empiezo a correr el timer de actividad

		//ENVIO DEL ACK

		ack_packet->header.tp_hdr.port_num = rel_conn->port_number;
		ack_packet->header.tp_hdr.dest_conn_num = rel_conn->remote_conn_fd;
		ack_packet->header.interlayer_hdr.inter_transport_hdr_bits = (uint8_t) PACKET_TYPE_R_CONNECT_ACK;

		ack_packet->header.tp_hdr.data_size = 4;//Envio 4 bytes con mi identificador de conexion
		memcpy(ack_packet->data, &rel_conn, 4);

		rel_conn->sending_packet = ack_packet;
		rel_conn->last_rssi = rcv_packet->pckt_rssi;
		rt_send_packet(rel_conn->dest_addr, ack_packet);	//Envio el ack

		tp_r_accepted_connection((uint32_t) rel_conn, (uint32_t) rel_server, from_addr);	//Informo de que he aceptado una conexion

	}

	/*  ******* PACKET_TYPE_R_DISCONNECT ************/
	else if(rcv_packet->header.interlayer_hdr.inter_transport_hdr_bits == PACKET_TYPE_R_DISCONNECT){
		if((rel_conn = (rel_tp_conn_t*) rcv_packet->header.tp_hdr.dest_conn_num) == NULL){	//Mi numero de conexion enviado por el otro nodo
			goto TP_RCV_ERROR;
		}
		if(!check_rel_conn_port_in_list(layer_data, rel_conn, rcv_packet->header.tp_hdr.port_num)){		//Confirmo que la conexion esta en mi lista
			goto TP_RCV_ERROR;
		}
		if((ack_packet = tx_packetbuffer_get_free_packet()) == NULL){
			goto TP_RCV_ERROR;
		}
		rel_conn->port_state = NULL_TP_PORT_DISCONNECTING;
		//ENVIO DEL ACK
		ack_packet->header.tp_hdr.port_num = rel_conn->port_number;
		ack_packet->header.tp_hdr.data_size = 0;
		ack_packet->header.tp_hdr.dest_conn_num = rel_conn->remote_conn_fd;
		ack_packet->header.interlayer_hdr.inter_transport_hdr_bits = (uint8_t) PACKET_TYPE_R_DISCONNECT_ACK;

		rt_send_packet(rel_conn->dest_addr, ack_packet);

		/*Hago esto para cerrar con error todas las posibles llamadas que hubiera hechas a esta conexion)*/
		tp_r_send_done((uint32_t) rel_conn, 0);
		tp_r_rcv_done((uint32_t) rel_conn, 0);
		tp_r_connected(0, rel_conn->port_number);
		tp_r_accepted_connection(0, (uint32_t) rel_conn->associated_server, NULL);
		tp_r_disconnected((uint32_t) rel_conn, RET_OK);

		delete_rel_conn(layer_data, rel_conn);	//Elimino la conexion
		rt_delete_net_addr(from_addr);
	}

	/*  ********** PACKET_TYPE_R_CONNECT_ACK *********/
	else if(rcv_packet->header.interlayer_hdr.inter_transport_hdr_bits == PACKET_TYPE_R_CONNECT_ACK){
		if((rel_conn = (rel_tp_conn_t*) rcv_packet->header.tp_hdr.dest_conn_num) == NULL){	//Mi numero de conexion enviado por el otro nodo
			goto TP_RCV_ERROR;
		}
		if(!check_rel_conn_port_in_list(layer_data, rel_conn, rcv_packet->header.tp_hdr.port_num)){		//Confirmo que la conexion esta en mi lista
			goto TP_RCV_ERROR;
		}
		if(rel_conn->port_state != NULL_TP_PORT_CONNECTING){
			goto TP_RCV_ERROR;
		}
		ytTimerStop(rel_conn->retransmission_timer);
		memcpy(&(rel_conn->remote_conn_fd),rcv_packet->data, 4); //Guardo el numero de conexion remoto
		rel_conn->retransmission_num = 0;
		rel_conn->port_state = NULL_TP_PORT_CONNECTED;
		rel_conn->sending_packet = NULL;
		rel_conn->last_rssi = rcv_packet->pckt_rssi;

		ytTimerStart(rel_conn->inactive_timer, rel_conn->connection_timeout);	//Empiezo a correr el timer de actividad

		rt_read_packet();	//Espero recibir un DISCONNECT en algun momento
		tp_r_connected((uint32_t) rel_conn, rcv_packet->header.tp_hdr.port_num);	//Informo de que me he conectado
		rt_delete_net_addr(from_addr);
	}

	/*  ********** PACKET_TYPE_R_DISCONNECT_ACK *********/
	else if(rcv_packet->header.interlayer_hdr.inter_transport_hdr_bits == PACKET_TYPE_R_DISCONNECT_ACK){
		if((rel_conn = (rel_tp_conn_t*) rcv_packet->header.tp_hdr.dest_conn_num) == NULL){	//Mi numero de conexion enviado por el otro nodo
			goto TP_RCV_ERROR;
		}
		if(!check_rel_conn_port_in_list(layer_data, rel_conn, rcv_packet->header.tp_hdr.port_num)){		//Confirmo que la conexion esta en mi lista
			goto TP_RCV_ERROR;
		}
		if(rel_conn->port_state != NULL_TP_PORT_DISCONNECTING){
			goto TP_RCV_ERROR;
		}
		ytTimerStop(rel_conn->retransmission_timer);

		tp_r_disconnected((uint32_t) rel_conn, RET_OK);

		delete_rel_conn(layer_data, rel_conn);	//Elimino la conexion
		rt_delete_net_addr(from_addr);
	}


	/*  ********** PACKET_TYPE_R_SEND *********/
	else if(rcv_packet->header.interlayer_hdr.inter_transport_hdr_bits == PACKET_TYPE_R_SEND){
		if((rel_conn = (rel_tp_conn_t*) rcv_packet->header.tp_hdr.dest_conn_num) == NULL){	//Mi numero de conexion enviado por el otro nodo
			goto TP_RCV_ERROR;
		}
		if(!check_rel_conn_port_in_list(layer_data, rel_conn, rcv_packet->header.tp_hdr.port_num)){		//Confirmo que la conexion esta en mi lista
			goto TP_RCV_ERROR;
		}
		if(rel_conn->port_state != NULL_TP_PORT_CONNECTED){
			goto TP_RCV_ERROR;
		}
		if((rel_conn->reading_transmitting & READING_FLAG) != READING_FLAG){	//Solo acepto mensajes si estoy leyendo
			goto TP_RCV_ERROR;
		}
		rel_conn->reading_transmitting &= ~READING_FLAG;
		if((ack_packet = tx_packetbuffer_get_free_packet()) == NULL){
			rel_conn->reading_transmitting |= READING_FLAG;
			goto TP_RCV_ERROR;
		}

		ytTimerStop(rel_conn->inactive_timer);
		ytTimerStart(rel_conn->inactive_timer, rel_conn->connection_timeout);	//Reinicio el timer de actividad
		//ENVIO DEL ACK
		ack_packet->header.tp_hdr.port_num = rel_conn->port_number;
		ack_packet->header.tp_hdr.data_size = 0;
		ack_packet->header.tp_hdr.dest_conn_num = rel_conn->remote_conn_fd;
		ack_packet->header.interlayer_hdr.inter_transport_hdr_bits = (uint8_t) PACKET_TYPE_R_SEND_ACK;

		rt_send_packet(rel_conn->dest_addr, ack_packet);

		memcpy(rel_conn->rx_data_ptr, rcv_packet->data, rcv_packet->header.tp_hdr.data_size);
		rel_conn->last_rssi = rcv_packet->pckt_rssi;

		tp_r_rcv_done((uint32_t) rel_conn, rcv_packet->header.tp_hdr.data_size);
		rt_delete_net_addr(from_addr);
	}

	/*  ********** PACKET_TYPE_R_SEND_ACK *********/
	else if(rcv_packet->header.interlayer_hdr.inter_transport_hdr_bits == PACKET_TYPE_R_SEND_ACK){
		if((rel_conn = (rel_tp_conn_t*) rcv_packet->header.tp_hdr.dest_conn_num) == NULL){	//Mi numero de conexion enviado por el otro nodo
			goto TP_RCV_ERROR;
		}
		if(!check_rel_conn_port_in_list(layer_data, rel_conn, rcv_packet->header.tp_hdr.port_num)){		//Confirmo que la conexion esta en mi lista
			goto TP_RCV_ERROR;
		}
		if(rel_conn->port_state != NULL_TP_PORT_CONNECTED){
			goto TP_RCV_ERROR;
		}
		if((rel_conn->reading_transmitting & TRANSMITTING_FLAG) != TRANSMITTING_FLAG){	//No me deberia llegar un ack si no estoy transmitiendo
			goto TP_RCV_ERROR;
		}
		rel_conn->reading_transmitting &= ~TRANSMITTING_FLAG;
		ytTimerStop(rel_conn->retransmission_timer);
		rel_conn->retransmission_num = 0;
		rel_conn->sending_packet = NULL;
		rel_conn->last_rssi = rcv_packet->pckt_rssi;

		ytTimerStop(rel_conn->inactive_timer);
		ytTimerStart(rel_conn->inactive_timer, rel_conn->connection_timeout);	//Reinicio el timer de actividad

		tp_r_send_done((uint32_t) rel_conn, rel_conn->last_packet_size);
		rt_delete_net_addr(from_addr);
	}
#endif
	rx_packetbuffer_release_packet(rcv_packet);
	rt_read_packet();	//Siempre espero recibir algun paquete
	return RET_OK;

TP_RCV_ERROR:
	rx_packetbuffer_release_packet(rcv_packet);
	rt_read_packet();	//Siempre espero recibir algun paquete
	rt_delete_net_addr(from_addr);
	return RET_ERROR;
}


/**
 *
 * @param tp_layer_data
 * @param port_number
 * @return
 */
static unrel_tp_port_t* get_unrel_port_in_list(null_tp_layer_data_t* tp_layer_data, uint16_t port_number){
	unrel_tp_port_t* port;
	gen_list* current = tp_layer_data->unrel_port_list;

	while(current->next != NULL){
		port = (unrel_tp_port_t*) current->next->item;
		if(port->port_number == port_number){
			return port;
		}
		current = current->next;
	}

	return NULL;
}



/**
 *
 * @param tp_layer_data
 * @param port_number
 * @return
 */
static uint16_t is_port_used(null_tp_layer_data_t* tp_layer_data, uint16_t port_number){
	if(get_unrel_port_in_list(tp_layer_data, port_number)){
		return 1;
	}
#if USE_RELIABLE_FUNCS
	if(get_rel_conn_in_list(tp_layer_data, port_number)){
		return 1;
	}
	if(get_rel_server_in_list(tp_layer_data, port_number)){
		return 1;
	}
#endif
	return 0;

}

#if USE_RELIABLE_FUNCS
/**
 *
 * @param tp_layer_data
 * @param port_number
 * @return
 */
static rel_tp_conn_t* get_rel_conn_in_list(null_tp_layer_data_t* tp_layer_data, uint16_t port_number){
	rel_tp_conn_t* conn;
	gen_list* current = tp_layer_data->rel_conn_list;

	while(current->next != NULL){
		conn = (rel_tp_conn_t*) current->next->item;
		if(conn->port_number == port_number){
			return conn;
		}
		current = current->next;
	}

	return NULL;
}


/**
 *
 * @param tp_layer_data
 * @param rel_conn
 * @param rel_port
 * @return
 */
static uint16_t check_rel_conn_port_in_list(null_tp_layer_data_t* tp_layer_data, rel_tp_conn_t* rel_conn, uint16_t rel_port){
	rel_tp_conn_t* conn;
	gen_list* current = tp_layer_data->rel_conn_list;

	while(current->next != NULL){
		conn = (rel_tp_conn_t*) current->next->item;
		if(conn->port_number == rel_port){
			if(conn == rel_conn){
				return 1;
			}
		}
		current = current->next;
	}

	return 0;
}
/**
 *
 * @param tp_layer_data
 * @param rel_conn
 * @return
 */


static uint16_t is_rel_conn_in_list(null_tp_layer_data_t* tp_layer_data, rel_tp_conn_t* rel_conn){
	rel_tp_conn_t* conn;
	gen_list* current = tp_layer_data->rel_conn_list;

	while(current->next != NULL){
		conn = (rel_tp_conn_t*) current->next->item;
		if(conn == rel_conn){
			return 1;
		}
		current = current->next;
	}

	return 0;
}



/**
 *
 * @param tp_layer_data
 * @param port_number
 * @return
 */
static rel_tp_server_t* get_rel_server_in_list(null_tp_layer_data_t* tp_layer_data, uint16_t port_number){
	rel_tp_server_t* server;
	gen_list* current = tp_layer_data->rel_server_list;

	while(current->next != NULL){
		server = (rel_tp_server_t*) current->next->item;
		if(server->port_number == port_number){
			return server;
		}
		current = current->next;
	}

	return NULL;
}

/**
 *
 * @param tp_layer_data
 * @param server
 * @return
 */
static uint16_t is_server_in_list(null_tp_layer_data_t* tp_layer_data, rel_tp_server_t* server){

	gen_list* current = tp_layer_data->rel_server_list;

	while(current->next != NULL){

		if(current->next->item == server){
			return 1;
		}
		current = current->next;
	}

	return 0;
}


/**
 *
 * @param tp_layer_data
 * @param rel_port
 * @return
 */
static retval_t delete_rel_conn(null_tp_layer_data_t* tp_layer_data, rel_tp_conn_t* rel_conn){

	if(tp_layer_data == NULL){
		return RET_ERROR;
	}


	ytTimerDelete(rel_conn->retransmission_timer);
	ytTimerDelete(rel_conn->inactive_timer);

	rt_delete_net_addr(rel_conn->dest_addr);

	gen_list_remove(tp_layer_data->rel_conn_list, rel_conn);

	ytFree(rel_conn);

	return RET_OK;
}

/**
 *
 * @param args
 */
static void retransmission_timeout_cb(void const* args){
	rel_tp_conn_t* rel_conn = (rel_tp_conn_t*) args;

	//Reenvio el paquete
	if(rel_conn->retransmission_num < MAX_NUM_RETRANSMISSIONS){
		rt_read_packet();	//Espero recibir un ACK
		rt_send_packet(rel_conn->dest_addr, rel_conn->sending_packet);
		rel_conn->retransmission_num++;
	}
	else{
		tx_packetbuffer_release_packet(rel_conn->sending_packet);
		ytTimerStop(rel_conn->retransmission_timer);

		if(rel_conn->port_state == NULL_TP_PORT_CONNECTING){
			//Maximo de retransmisiones. Cancelo el envio y digo que ha habido error, enviando una instancia a un puerto null
			tp_r_connected(0, rel_conn->port_number);
			delete_rel_conn(rel_conn->tp_layer_data, rel_conn);
		}
		else if(rel_conn->port_state == NULL_TP_PORT_DISCONNECTING){
			tp_r_disconnected((uint32_t) rel_conn, RET_ERROR);
			delete_rel_conn(rel_conn->tp_layer_data, rel_conn);
		}
		else if(rel_conn->port_state == NULL_TP_PORT_CONNECTED){
			if(rel_conn->reading_transmitting & TRANSMITTING_FLAG){
				rel_conn->reading_transmitting &= ~TRANSMITTING_FLAG;
				tp_r_send_done((uint32_t) rel_conn, 0);
			}
		}
	}

}

/**
 *
 * @param args
 */
static void inactive_timeout_cb(void const* args){
	rel_tp_conn_t* rel_port = (rel_tp_conn_t*) args;	//Transcurrido el tiempo de inactividad cierro la conexion

	null_tp_r_close_connection(rel_port->tp_layer_data, (uint32_t) rel_port);

}
#endif


#endif
