/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * transport_layer.c
 *
 *  Created on: 21 de nov. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file transport_layer.c
 */

#include "transport_layer.h"
#include "netstack.h"
#include "time_meas.h"

#if ENABLE_NETSTACK_ARCH

#define DEBUG 0
#if DEBUG
#include "yetimote_stdio.h"
#define PRINTF(...) _printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

static tp_layer_func_args_t* get_func_args_from_server(gen_list* tp_args_list, tp_layer_event_type_t tp_layer_event_type, uint32_t server_id);
static tp_layer_func_args_t* get_func_args_from_conn(gen_list* tp_args_list, tp_layer_event_type_t tp_layer_event_type, uint32_t connection_id);
static tp_layer_func_args_t* get_func_args_from_port(gen_list* tp_args_list, tp_layer_event_type_t tp_layer_event_type, uint16_t port);


/**
 *
 * @param tp_layer_funcs
 * @return
 */
transport_layer_t* init_transport_layer(tp_layer_funcs_t* tp_layer_funcs){

	transport_layer_t* new_tp_layer = ytMalloc(sizeof(transport_layer_t));

	if(new_tp_layer == NULL){
		return NULL;
	}

	new_tp_layer->tp_layer_funcs = tp_layer_funcs;
	new_tp_layer->tp_args_list = gen_list_init();
	new_tp_layer->tp_layer_private_data = NULL;

	PRINTF("TP_LAYER: INIT\r\n");
	if(new_tp_layer->tp_layer_funcs->tp_layer_init(&(new_tp_layer->tp_layer_private_data)) != RET_OK){
		gen_list_remove_all(new_tp_layer->tp_args_list);
		ytFree(new_tp_layer);
		PRINTF("TP_LAYER: INIT ERROR\r\n");
		return NULL;
	}

	PRINTF("TP_LAYER: INIT DONE\r\n");
	return new_tp_layer;
}

/**
 *
 * @param tp_layer
 * @return
 */
retval_t deinit_transport_layer(transport_layer_t* tp_layer){
	if(tp_layer == NULL){
		return RET_ERROR;
	}

	PRINTF("TP_LAYER: DEINIT\r\n");
	gen_list_remove_all(tp_layer->tp_args_list);

	tp_layer->tp_layer_funcs->tp_layer_deinit(tp_layer->tp_layer_private_data);

	ytFree(tp_layer);
	PRINTF("TP_LAYER: DEINIT DONE\r\n");
	return RET_OK;
}

/**
 *
 * @param port
 * @param signal_level
 * @return
 */
retval_t tp_u_get_last_signal_level(uint16_t port, float32_t* signal_level){
	transport_layer_t* tp_layer =  netstack_get_tp_layer();

	return tp_layer->tp_layer_funcs->u_get_last_signal_level(tp_layer->tp_layer_private_data, port, signal_level);
}

/**
 *
 * @param connection_fd
 * @param signal_level
 * @return
 */
retval_t tp_r_get_last_signal_level(uint32_t connection_fd, float32_t* signal_level){
	transport_layer_t* tp_layer =  netstack_get_tp_layer();

	return tp_layer->tp_layer_funcs->r_get_last_signal_level(tp_layer->tp_layer_private_data, connection_fd, signal_level);
}

/**
 *
 * @param connection_fd
 * @param timeout
 * @return
 */
retval_t tp_r_set_connection_timeout(uint32_t connection_fd, uint32_t timeout){

	transport_layer_t* tp_layer =  netstack_get_tp_layer();

	return tp_layer->tp_layer_funcs->r_set_connection_timeout(tp_layer->tp_layer_private_data, connection_fd, timeout);
}

/**
 *
 * @param port
 * @param sent_size
 * @return
 */
retval_t tp_u_send_done(uint16_t port, uint32_t sent_size){
	transport_layer_t* tp_layer =  netstack_get_tp_layer();
	tp_layer_func_args_t* stored_func_args;
	uint32_t op_semph_id;
	PRINTF("TP_LAYER: U SEND DONE\r\n");

	if((stored_func_args = get_func_args_from_port(tp_layer->tp_args_list, TP_EVENT_U_SEND, port)) == NULL){
		return RET_ERROR;
	}
	netstack_state = NS_TP_U_SEND_DONE;
	*(stored_func_args->size) = sent_size;
	op_semph_id = stored_func_args->semaphore_id;
	gen_list_remove(tp_layer->tp_args_list, stored_func_args);
	ytFree(stored_func_args);

	return netstack_op_done(op_semph_id);
}

/**
 *
 * @param port
 * @param rcv_size
 * @param from_addr_fd
 * @return
 */
retval_t tp_u_rcv_done(uint16_t port, uint32_t rcv_size, net_addr_t from_addr_fd){
	transport_layer_t* tp_layer =  netstack_get_tp_layer();
	tp_layer_func_args_t* stored_func_args;
	uint32_t op_semph_id;

	PRINTF("TP_LAYER: EVENT U RCV DONE \r\n");
	if((stored_func_args = get_func_args_from_port(tp_layer->tp_args_list, TP_EVENT_U_RCV, port)) == NULL){
		return RET_ERROR;
	}
	netstack_state = NS_TP_U_RCV_DONE;
	if(!stored_func_args->read_async){
		rt_net_addr_cpy(stored_func_args->addr_fd, from_addr_fd);
		rt_delete_net_addr(from_addr_fd);
		*(stored_func_args->size) = rcv_size;
		op_semph_id = stored_func_args->semaphore_id;
		gen_list_remove(tp_layer->tp_args_list, stored_func_args);
		ytFree(stored_func_args);
		return netstack_op_done(op_semph_id);
	}
	else{
		stored_func_args->read_callback(rcv_size, stored_func_args->data, (void*) from_addr_fd, stored_func_args->args);
		rt_delete_net_addr(from_addr_fd);
		ytFree(stored_func_args->size);
		gen_list_remove(tp_layer->tp_args_list, stored_func_args);
		ytFree(stored_func_args);
	}
	return RET_OK;
}

/**
 *
 * @param connection_fd
 * @param sent_size
 * @return
 */
retval_t tp_r_send_done(uint32_t connection_fd, uint32_t sent_size){
	transport_layer_t* tp_layer =  netstack_get_tp_layer();
	tp_layer_func_args_t* stored_func_args;
	uint32_t op_semph_id;

	PRINTF("TP_LAYER: EVENT R SEND DONE \r\n");
	if((stored_func_args = get_func_args_from_conn(tp_layer->tp_args_list, TP_EVENT_R_SEND, connection_fd)) == NULL){
		return RET_ERROR;
	}
	netstack_state = NS_TP_R_SEND_DONE;
	*(stored_func_args->size) = sent_size;
	op_semph_id = stored_func_args->semaphore_id;
	gen_list_remove(tp_layer->tp_args_list, stored_func_args);
	ytFree(stored_func_args);
	return netstack_op_done(op_semph_id);
}

/**
 *
 * @param connection_fd
 * @param sent_size
 * @return
 */
retval_t tp_r_rcv_done(uint32_t connection_fd, uint32_t sent_size){
	transport_layer_t* tp_layer =  netstack_get_tp_layer();
	tp_layer_func_args_t* stored_func_args;
	uint32_t op_semph_id;

	PRINTF("TP_LAYER: EVENT R RCV DONE \r\n");
	if((stored_func_args = get_func_args_from_conn(tp_layer->tp_args_list, TP_EVENT_R_RCV, connection_fd)) == NULL){
		return RET_ERROR;
	}
	if(!stored_func_args->read_async){
		netstack_state = NS_TP_R_RCV_DONE;
		*(stored_func_args->size) = sent_size;
		op_semph_id = stored_func_args->semaphore_id;
		gen_list_remove(tp_layer->tp_args_list, stored_func_args);
		ytFree(stored_func_args);
		return netstack_op_done(op_semph_id);
	}
	else{
		stored_func_args->read_callback(sent_size, stored_func_args->data, (void*) connection_fd, stored_func_args->args);
		ytFree(stored_func_args->size);
		ytFree(stored_func_args->connection_fd);
		gen_list_remove(tp_layer->tp_args_list, stored_func_args);
		ytFree(stored_func_args);
	}

	return RET_OK;
}

/**
 *
 * @param connection_fd
 * @param server_fd
 * @param from_addr_fd
 * @return
 */
retval_t tp_r_accepted_connection(uint32_t connection_fd, uint32_t server_fd, net_addr_t from_addr_fd){
	transport_layer_t* tp_layer =  netstack_get_tp_layer();
	tp_layer_func_args_t* stored_func_args;
	uint32_t op_semph_id;

	PRINTF("TP_LAYER: EVENT R ACCEPTED CONN \r\n");
	if((stored_func_args = get_func_args_from_server(tp_layer->tp_args_list, TP_EVENT_R_ACCEPT_CONN, server_fd)) == NULL){
		return RET_ERROR;
	}
	else{
		netstack_state = NS_TP_R_ACEPTED_CONNECTION;
		*(stored_func_args->connection_fd) = connection_fd;
		rt_net_addr_cpy(stored_func_args->addr_fd, from_addr_fd);
		rt_delete_net_addr(from_addr_fd);
	}
	op_semph_id = stored_func_args->semaphore_id;
	gen_list_remove(tp_layer->tp_args_list, stored_func_args);
	ytFree(stored_func_args);
	return netstack_op_done(op_semph_id);
}

/**
 *
 * @param connection_fd
 * @param port
 * @return
 */
retval_t tp_r_connected(uint32_t connection_fd, uint16_t port){
	transport_layer_t* tp_layer =  netstack_get_tp_layer();
	tp_layer_func_args_t* stored_func_args;
	uint32_t op_semph_id;

	PRINTF("TP_LAYER: EVENT R CONN DONE \r\n");

	if((stored_func_args = get_func_args_from_port(tp_layer->tp_args_list, TP_EVENT_R_CONNECT_TO, port)) == NULL){
		return RET_ERROR;
	}
	else{
		netstack_state = NS_TP_R_CONNECTED;
		*(stored_func_args->connection_fd) = connection_fd;
	}
	op_semph_id = stored_func_args->semaphore_id;
	gen_list_remove(tp_layer->tp_args_list, stored_func_args);
	ytFree(stored_func_args);

	return netstack_op_done(op_semph_id);
}

/**
 *
 * @param connection_fd
 * @return
 */
retval_t tp_r_disconnected(uint32_t connection_fd, retval_t ret){
	transport_layer_t* tp_layer =  netstack_get_tp_layer();
	tp_layer_func_args_t* stored_func_args;
	uint32_t op_semph_id;

	PRINTF("TP_LAYER: EVENT R DISCONN DONE \r\n");

	if((stored_func_args = get_func_args_from_conn(tp_layer->tp_args_list, TP_EVENT_R_CLOSE_CONN, connection_fd)) == NULL){
		return RET_ERROR;
	}
	else{
		netstack_state = NS_TP_R_DISCONNECTED;
		*(stored_func_args->ret_val) = ret;
	}

	op_semph_id = stored_func_args->semaphore_id;
	gen_list_remove(tp_layer->tp_args_list, stored_func_args);
	ytFree(stored_func_args);
	return netstack_op_done(op_semph_id);
}

/**
 *
 * @param packet
 * @return
 */
retval_t tp_packet_sent(net_packet_t* packet){
	transport_layer_t* tp_layer =  netstack_get_tp_layer();

	netstack_state = NS_TP_PACKET_SENT;
	PRINTF("TP_LAYER: EVENT PACKET SENT DONE \r\n");
	return tp_layer->tp_layer_funcs->send_packet_done(tp_layer->tp_layer_private_data, packet);
}

/**
 *
 * @param packet
 * @return
 */
retval_t tp_packet_received(net_packet_t* packet, net_addr_t from_addr){
	transport_layer_t* tp_layer =  netstack_get_tp_layer();

	netstack_state = NS_TP_PACKET_RECEIVED;
	PRINTF("TP_LAYER: EVENT PACKET RCV DONE \r\n");
	return tp_layer->tp_layer_funcs->rcv_packet_done(tp_layer->tp_layer_private_data, packet, from_addr);
}


retval_t tp_event_u_open(void* args){
	tp_layer_func_args_t* event_caller_func_args;
	retval_t ret = RET_ERROR;
	transport_layer_t* tp_layer;
	uint32_t op_semph_id;

	event_caller_func_args = (tp_layer_func_args_t*) args;
	tp_layer = event_caller_func_args->tp_layer;

	netstack_state = NS_TP_U_OPEN;
	PRINTF("TP_LAYER: EVENT U OPEN \r\n");
	ret = tp_layer->tp_layer_funcs->u_open_port(tp_layer->tp_layer_private_data, event_caller_func_args->port);
	*(event_caller_func_args->ret_val) = ret;
	op_semph_id = event_caller_func_args->semaphore_id;
	ytFree(event_caller_func_args);
	netstack_op_done(op_semph_id);
	return RET_OK;
}

retval_t tp_event_u_close(void* args){
	tp_layer_func_args_t* event_caller_func_args;
	retval_t ret = RET_ERROR;
	transport_layer_t* tp_layer;
	uint32_t op_semph_id;

	event_caller_func_args = (tp_layer_func_args_t*) args;
	tp_layer = event_caller_func_args->tp_layer;

	netstack_state = NS_TP_U_CLOSE;
	PRINTF("TP_LAYER: EVENT U CLOSE \r\n");
	ret = tp_layer->tp_layer_funcs->u_close_port(tp_layer->tp_layer_private_data, event_caller_func_args->port);
	*(event_caller_func_args->ret_val) = ret;
	op_semph_id = event_caller_func_args->semaphore_id;
	ytFree(event_caller_func_args);
	netstack_op_done(op_semph_id);
	return RET_OK;
}

retval_t tp_event_u_send(void* args){
	tp_layer_func_args_t* event_caller_func_args;
	retval_t ret = RET_ERROR;
	transport_layer_t* tp_layer;
	uint32_t op_semph_id;

	event_caller_func_args = (tp_layer_func_args_t*) args;
	tp_layer = event_caller_func_args->tp_layer;

	netstack_state = NS_TP_U_SEND;
	PRINTF("TP_LAYER: EVENT U SEND \r\n");
	ret = tp_layer->tp_layer_funcs->u_send(tp_layer->tp_layer_private_data, event_caller_func_args->addr_fd, event_caller_func_args->port, event_caller_func_args->data, *(event_caller_func_args->size));
	if(ret != RET_OK){
		*(event_caller_func_args->size) = 0;
		op_semph_id = event_caller_func_args->semaphore_id;
		ytFree(event_caller_func_args);
		netstack_op_done(op_semph_id);
	}
	else{
		gen_list_add(tp_layer->tp_args_list,  (void*) event_caller_func_args);
	}
	return RET_OK;
}

retval_t tp_event_u_rcv(void* args){
	tp_layer_func_args_t* event_caller_func_args;
	retval_t ret = RET_ERROR;
	transport_layer_t* tp_layer;
	uint32_t op_semph_id;

	event_caller_func_args = (tp_layer_func_args_t*) args;
	tp_layer = event_caller_func_args->tp_layer;

	netstack_state = NS_TP_U_RCV;
	PRINTF("TP_LAYER: EVENT U RCV \r\n");
	ret = tp_layer->tp_layer_funcs->u_rcv(tp_layer->tp_layer_private_data, event_caller_func_args->port,event_caller_func_args->data, *(event_caller_func_args->size));
	if(ret != RET_OK){
		(*event_caller_func_args->size) = 0;
		op_semph_id = event_caller_func_args->semaphore_id;
		ytFree(event_caller_func_args);
		netstack_op_done(op_semph_id);
	}
	else{
		gen_list_add(tp_layer->tp_args_list,  (void*) event_caller_func_args);
	}
	return RET_OK;

}

retval_t tp_event_r_create_server(void* args){
	tp_layer_func_args_t* event_caller_func_args;
	transport_layer_t* tp_layer;
	uint32_t op_semph_id;

	event_caller_func_args = (tp_layer_func_args_t*) args;
	tp_layer = event_caller_func_args->tp_layer;

	netstack_state = NS_TP_R_CREATE_SERVER;
	PRINTF("TP_LAYER: EVENT R CREATE SERVER \r\n");
	tp_layer->tp_layer_funcs->r_create_server(tp_layer->tp_layer_private_data, event_caller_func_args->port, event_caller_func_args->server_fd);
	op_semph_id = event_caller_func_args->semaphore_id;
	ytFree(event_caller_func_args);
	netstack_op_done(op_semph_id);
	return RET_OK;
}

retval_t tp_event_r_delete_server(void* args){
	tp_layer_func_args_t* event_caller_func_args;
	retval_t ret = RET_ERROR;
	transport_layer_t* tp_layer;
	uint32_t op_semph_id;

	event_caller_func_args = (tp_layer_func_args_t*) args;
	tp_layer = event_caller_func_args->tp_layer;

	netstack_state = NS_TP_R_DELETE_SERVER;
	PRINTF("TP_LAYER: EVENT R DELETE SERVER \r\n");
	ret = tp_layer->tp_layer_funcs->r_delete_server(tp_layer->tp_layer_private_data, *(event_caller_func_args->server_fd));
	*(event_caller_func_args->ret_val) = ret;
	op_semph_id = event_caller_func_args->semaphore_id;
	ytFree(event_caller_func_args);
	netstack_op_done(op_semph_id);
	return RET_OK;
}

retval_t tp_event_r_connect_to_server(void* args){
	tp_layer_func_args_t* event_caller_func_args;
	retval_t ret = RET_ERROR;
	transport_layer_t* tp_layer;
	uint32_t op_semph_id;

	event_caller_func_args = (tp_layer_func_args_t*) args;
	tp_layer = event_caller_func_args->tp_layer;

	netstack_state = NS_TP_R_CONNECT_TO;
	PRINTF("TP_LAYER: EVENT R CONNECT TO \r\n");
	ret = tp_layer->tp_layer_funcs->r_connect_to_server(tp_layer->tp_layer_private_data, event_caller_func_args->addr_fd, event_caller_func_args->port);
	if(ret != RET_OK){
		*(event_caller_func_args->connection_fd) = 0;
		op_semph_id = event_caller_func_args->semaphore_id;
		ytFree(event_caller_func_args);
		netstack_op_done(op_semph_id);
	}
	else{
		gen_list_add(tp_layer->tp_args_list,  (void*) event_caller_func_args);
	}
	return RET_OK;
}

retval_t tp_event_r_accept_connection(void* args){
	tp_layer_func_args_t* event_caller_func_args;
	retval_t ret = RET_ERROR;
	transport_layer_t* tp_layer;
	uint32_t op_semph_id;

	event_caller_func_args = (tp_layer_func_args_t*) args;
	tp_layer = event_caller_func_args->tp_layer;

	netstack_state = NS_TP_R_ACCEPT_CONN;
	PRINTF("TP_LAYER: EVENT R ACCEPT CONN \r\n");
	ret = tp_layer->tp_layer_funcs->r_server_accept_connection(tp_layer->tp_layer_private_data, *(event_caller_func_args->server_fd));
	if(ret != RET_OK){
		*(event_caller_func_args->connection_fd) = 0;
		op_semph_id = event_caller_func_args->semaphore_id;
		ytFree(event_caller_func_args);
		netstack_op_done(op_semph_id);
	}
	else{
		gen_list_add(tp_layer->tp_args_list,  (void*) event_caller_func_args);
	}
	return RET_OK;
}

retval_t tp_event_r_close_connection(void* args){
	tp_layer_func_args_t* event_caller_func_args;
	retval_t ret = RET_ERROR;
	transport_layer_t* tp_layer;
	uint32_t op_semph_id;

	event_caller_func_args = (tp_layer_func_args_t*) args;
	tp_layer = event_caller_func_args->tp_layer;

	netstack_state = NS_TP_R_CLOSE_CONN;
	PRINTF("TP_LAYER: EVENT R CLOSE CONN \r\n");
	ret = tp_layer->tp_layer_funcs->r_close_connection(tp_layer->tp_layer_private_data, *(event_caller_func_args->connection_fd));
	if(ret != RET_OK){
		*(event_caller_func_args->ret_val) = ret;
		op_semph_id = event_caller_func_args->semaphore_id;
		ytFree(event_caller_func_args);
		netstack_op_done(op_semph_id);
	}
	else{
		gen_list_add(tp_layer->tp_args_list,  (void*) event_caller_func_args);
	}
	return RET_OK;
}

retval_t tp_event_r_send(void* args){
	tp_layer_func_args_t* event_caller_func_args;
	retval_t ret = RET_ERROR;
	transport_layer_t* tp_layer;
	uint32_t op_semph_id;

	event_caller_func_args = (tp_layer_func_args_t*) args;
	tp_layer = event_caller_func_args->tp_layer;

	netstack_state = NS_TP_R_SEND;
	PRINTF("TP_LAYER: EVENT R SEND \r\n");
	ret = tp_layer->tp_layer_funcs->r_send(tp_layer->tp_layer_private_data, *(event_caller_func_args->connection_fd), event_caller_func_args->data, *(event_caller_func_args->size));
	if(ret != RET_OK){
		*(event_caller_func_args->size) = 0;
		op_semph_id = event_caller_func_args->semaphore_id;
		ytFree(event_caller_func_args);
		netstack_op_done(op_semph_id);
	}
	else{
		gen_list_add(tp_layer->tp_args_list,  (void*) event_caller_func_args);
	}
	return RET_OK;
}

retval_t tp_event_r_rcv(void* args){
	tp_layer_func_args_t* event_caller_func_args;
	retval_t ret = RET_ERROR;
	transport_layer_t* tp_layer;
	uint32_t op_semph_id;

	event_caller_func_args = (tp_layer_func_args_t*) args;
	tp_layer = event_caller_func_args->tp_layer;

	netstack_state = NS_TP_R_RCV;
	PRINTF("TP_LAYER: EVENT R RCV \r\n");
	ret = tp_layer->tp_layer_funcs->r_rcv(tp_layer->tp_layer_private_data, *(event_caller_func_args->connection_fd), event_caller_func_args->data, *(event_caller_func_args->size));
	if(ret != RET_OK){
		*(event_caller_func_args->size) = 0;
		op_semph_id = event_caller_func_args->semaphore_id;
		ytFree(event_caller_func_args);
		netstack_op_done(op_semph_id);
	}
	else{
		gen_list_add(tp_layer->tp_args_list,  (void*) event_caller_func_args);
	}
	return RET_OK;
}

retval_t tp_event_r_check_connection(void* args){
	tp_layer_func_args_t* event_caller_func_args;
	retval_t ret = RET_ERROR;
	transport_layer_t* tp_layer;
	uint32_t op_semph_id;

	event_caller_func_args = (tp_layer_func_args_t*) args;
	tp_layer = event_caller_func_args->tp_layer;

	netstack_state = NS_TP_R_CHECK_CONN;
	PRINTF("TP_LAYER: EVENT R CHECK CONN \r\n");
	ret = tp_layer->tp_layer_funcs->r_check_conn(tp_layer->tp_layer_private_data, *(event_caller_func_args->connection_fd));
	*(event_caller_func_args->ret_val) = ret;
	op_semph_id = event_caller_func_args->semaphore_id;
	ytFree(event_caller_func_args);
	netstack_op_done(op_semph_id);

	return RET_OK;
}


/**
 *
 * @param tp_args_list
 * @param tp_layer_event_type
 * @param port
 * @return
 */
static tp_layer_func_args_t* get_func_args_from_port(gen_list* tp_args_list, tp_layer_event_type_t tp_layer_event_type, uint16_t port){
	tp_layer_func_args_t* func_args = NULL;
	gen_list* current = tp_args_list;

	while(current->next != NULL){

		func_args = (tp_layer_func_args_t*) current->next->item;

		if(func_args->tp_layer_event_type == tp_layer_event_type){
			if(func_args->port == port){
				return func_args;
			}
		}
		current = current->next;
	}

	return func_args;
}

/**
 *
 * @param tp_args_list
 * @param tp_layer_event_type
 * @param connection_id
 * @return
 */
static tp_layer_func_args_t* get_func_args_from_conn(gen_list* tp_args_list, tp_layer_event_type_t tp_layer_event_type, uint32_t connection_id){
	tp_layer_func_args_t* func_args = NULL;
	gen_list* current = tp_args_list;

	while(current->next != NULL){

		func_args = (tp_layer_func_args_t*) current->next->item;

		if(func_args->tp_layer_event_type == tp_layer_event_type){
			if((*(func_args->connection_fd)) == connection_id){
				return func_args;
			}
		}
		current = current->next;
	}

	return func_args;
}


/**
 *
 * @param tp_args_list
 * @param tp_layer_event_type
 * @param server_id
 * @return
 */
static tp_layer_func_args_t* get_func_args_from_server(gen_list* tp_args_list, tp_layer_event_type_t tp_layer_event_type, uint32_t server_id){
	tp_layer_func_args_t* func_args = NULL;
	gen_list* current = tp_args_list;

	while(current->next != NULL){

		func_args = (tp_layer_func_args_t*) current->next->item;

		if(func_args->tp_layer_event_type == tp_layer_event_type){
			if((*(func_args->server_fd)) == server_id){
				return func_args;
			}
		}
		current = current->next;
	}

	return func_args;
}

#endif
