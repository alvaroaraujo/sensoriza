/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * transport_layer.h
 *
 *  Created on: 21 de nov. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file transport_layer.h
 */
#ifndef APPLICATION_CORE_NET_INCLUDE_TRANSPORT_LAYER_H_
#define APPLICATION_CORE_NET_INCLUDE_TRANSPORT_LAYER_H_

#include "system_api.h"
#include "routing_layer.h"
#include "transport_layer_core.h"

typedef void tp_layer_data_t;	//Can be casted to any type specified by transport layer developer

typedef struct tp_layer_funcs_{
	retval_t (*tp_layer_init)(tp_layer_data_t** tp_layer_data);
	retval_t (*tp_layer_deinit)(tp_layer_data_t* tp_layer_data);

	/* Unreliable funcs */
	retval_t (*u_open_port)(tp_layer_data_t* tp_layer_data, uint16_t port_number);
	retval_t (*u_close_port)(tp_layer_data_t* tp_layer_data, uint16_t port_number);
	retval_t (*u_send)(tp_layer_data_t* tp_layer_data, net_addr_t dest_addr_fd, uint16_t port_number, uint8_t* data, uint32_t size);
	retval_t (*u_rcv)(tp_layer_data_t* tp_layer_data, uint16_t port_number, uint8_t* data, uint32_t size);
	retval_t (*u_get_last_signal_level)(tp_layer_data_t* tp_layer_data, uint16_t port, float32_t* signal_level);
	/* Reliable funcs */
	retval_t (*r_create_server)(tp_layer_data_t* tp_layer_data, uint16_t port_number, uint32_t* server_fd);						//Return server_fd
	retval_t (*r_delete_server)(tp_layer_data_t* tp_layer_data, uint32_t server_fd);
	retval_t (*r_server_accept_connection)(tp_layer_data_t* tp_layer_data, uint32_t server_fd);
	retval_t (*r_connect_to_server)(tp_layer_data_t* tp_layer_data, net_addr_t dest_addr_fd, uint16_t port_number);
	retval_t (*r_send)(tp_layer_data_t* tp_layer_data, uint32_t connection_fd, uint8_t* data, uint32_t size);
	retval_t (*r_rcv)(tp_layer_data_t* tp_layer_data, uint32_t connection_fd, uint8_t* data, uint32_t size);
	retval_t (*r_close_connection)(tp_layer_data_t* tp_layer_data, uint32_t connection_fd);
	retval_t (*r_check_conn)(tp_layer_data_t* tp_layer_data, uint32_t connection_fd);
	retval_t (*r_get_last_signal_level)(tp_layer_data_t* tp_layer_data, uint32_t connection_fd, float32_t* signal_level);
	retval_t (*r_set_connection_timeout)(tp_layer_data_t* tp_layer_data, uint32_t connection_fd, uint32_t timeout);

	/* Send and receive packets callbacks */
	retval_t (*send_packet_done)(tp_layer_data_t* tp_layer_data, net_packet_t* packet_sent);
	retval_t (*rcv_packet_done)(tp_layer_data_t* tp_layer_data, net_packet_t* rcv_packet, net_addr_t from_addr);

}tp_layer_funcs_t;

/* *************************************************/

/* Called from net_api*/
retval_t tp_event_u_open(void* args);
retval_t tp_event_u_close(void* args);
retval_t tp_event_u_send(void* args);
retval_t tp_event_u_rcv(void* args);
retval_t tp_event_r_create_server(void* args);
retval_t tp_event_r_delete_server(void* args);
retval_t tp_event_r_connect_to_server(void* args);
retval_t tp_event_r_accept_connection(void* args);
retval_t tp_event_r_close_connection(void* args);
retval_t tp_event_r_check_connection(void* args);
retval_t tp_event_r_send(void* args);
retval_t tp_event_r_rcv(void* args);

/* Called from user process*/
retval_t tp_u_get_last_signal_level(uint16_t port, float32_t* signal_level);
retval_t tp_r_get_last_signal_level(uint32_t connection_fd, float32_t* signal_level);
retval_t tp_r_set_connection_timeout(uint32_t connection_fd, uint32_t timeout);

/*Callbacks from transport layer*/
retval_t tp_u_send_done(uint16_t port, uint32_t sent_size);
retval_t tp_u_rcv_done(uint16_t port, uint32_t rcv_size, net_addr_t from_addr_fd);
retval_t tp_r_send_done(uint32_t connection_fd, uint32_t sent_size);
retval_t tp_r_rcv_done(uint32_t connection_fd, uint32_t sent_size);
retval_t tp_r_accepted_connection(uint32_t connection_fd, uint32_t server_fd, net_addr_t from_addr_fd);
retval_t tp_r_connected(uint32_t connection_fd, uint16_t port);
retval_t tp_r_disconnected(uint32_t connection_fd, retval_t ret);


/*Callbacks from routing layer*/
retval_t tp_packet_sent(net_packet_t* packet);
retval_t tp_packet_received(net_packet_t* packet, net_addr_t from_addr);

#endif /* APPLICATION_CORE_NET_INCLUDE_TRANSPORT_LAYER_H_ */
