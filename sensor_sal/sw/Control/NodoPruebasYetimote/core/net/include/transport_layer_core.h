/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * transport_layer_core.h
 *
 *  Created on: 24 de nov. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file transport_layer_core.h
 */
#ifndef APPLICATION_CORE_NET_INCLUDE_TRANSPORT_LAYER_CORE_H_
#define APPLICATION_CORE_NET_INCLUDE_TRANSPORT_LAYER_CORE_H_

#include "packetbuffer.h"
#include "net_api.h"

struct tp_layer_funcs_;

typedef struct transport_layer_{
	struct tp_layer_funcs_* tp_layer_funcs;
	gen_list* tp_args_list;
	void* tp_layer_private_data;
}transport_layer_t;


typedef enum tp_layer_event_type_{
	TP_EVENT_U_OPEN,			//From upper layer
	TP_EVENT_U_CLOSE,			//From upper layer
	TP_EVENT_U_SEND,			//From upper layer
	TP_EVENT_U_RCV,				//From upper layer

	TP_EVENT_R_CREATE_SERVER,	//From upper layer
	TP_EVENT_R_DELETE_SERVER,	//From upper layer
	TP_EVENT_R_ACCEPT_CONN,		//From upper layer
	TP_EVENT_R_CONNECT_TO,		//From upper layer
	TP_EVENT_R_SEND,			//From upper layer
	TP_EVENT_R_RCV,				//From upper layer
	TP_EVENT_R_CLOSE_CONN,		//From upper layer
	TP_EVENT_R_CHECK_CONN,		//From upper layer

	TP_EVENT_U_SEND_DONE,		//From same layer
	TP_EVENT_R_SEND_DONE,		//From same layer
	TP_EVENT_U_RCV_DONE,		//From same layer
	TP_EVENT_R_RCV_DONE,		//From same layer
	TP_EVENT_ACCEPTED_CONN, 	//From same layer
	TP_EVENT_CONN_DONE,			//From same layer
	TP_EVENT_DISCONN_DONE,		//From same layer

	TP_EVENT_PACKET_SEND_DONE,	//From lower layer. Single packet
	TP_EVENT_PACKET_RCV_DONE,	//From lower layer. Single packet

}tp_layer_event_type_t;


typedef struct tp_layer_func_args_{
	transport_layer_t* tp_layer;
	retval_t* ret_val;
	uint32_t semaphore_id;

	uint16_t port;
	uint8_t* data;
	uint32_t* size;
	net_addr_t addr_fd;
	uint32_t* server_fd;
	uint32_t* connection_fd;
	net_packet_t* packet;

	net_read_cb_func_t read_callback;
	void* args;
	uint8_t read_async;

	tp_layer_event_type_t tp_layer_event_type;
}tp_layer_func_args_t;

transport_layer_t* init_transport_layer(struct tp_layer_funcs_* tp_layer_funcs);
retval_t deinit_transport_layer(transport_layer_t* tp_layer);

#endif /* APPLICATION_CORE_NET_INCLUDE_TRANSPORT_LAYER_CORE_H_ */
