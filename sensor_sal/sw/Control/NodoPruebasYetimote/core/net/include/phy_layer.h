/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * phy_layer.h
 *
 *  Created on: 22 de nov. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file phy_layer.h
 */
#ifndef APPLICATION_CORE_NET_INCLUDE_PHY_LAYER_H_
#define APPLICATION_CORE_NET_INCLUDE_PHY_LAYER_H_


#include "phy_layer_core.h"
#include "arm_math.h"
#include "system_api.h"

typedef void phy_layer_data_t;

typedef struct phy_layer_funcs_{
	retval_t (*phy_layer_init)(phy_layer_data_t** phy_layer_data);
	retval_t (*phy_layer_deinit)(phy_layer_data_t* phy_layer_data);

	retval_t (*phy_send_packet)(phy_layer_data_t* phy_layer_data, net_packet_t* packet);

	retval_t (*phy_send_packet_done)(phy_layer_data_t* phy_layer_data, net_packet_t* packet);
	retval_t (*phy_rcv_packet_done)(phy_layer_data_t* phy_layer_data, net_packet_t* packet);

	//Funciones llamadas inmediatamente. No son posteadas en modo evento. Se puede hacer as� ya que solo deber�an ser llamadas desde la capa MAC
	retval_t (*phy_set_mode_receiving)(phy_layer_data_t* phy_layer_data);
	retval_t (*phy_set_mode_idle)(phy_layer_data_t* phy_layer_data);
	retval_t (*phy_set_mode_sleep)(phy_layer_data_t* phy_layer_data);

	retval_t (*phy_check_channel_rssi)(phy_layer_data_t* phy_layer_data, float32_t* read_rssi);
	retval_t (*phy_get_last_rssi)(phy_layer_data_t* phy_layer_data, float32_t* read_rssi);

	retval_t (*phy_set_base_freq)(phy_layer_data_t* phy_layer_data, uint32_t base_freq);
	retval_t (*phy_get_base_freq)(phy_layer_data_t* phy_layer_data, uint32_t* base_freq);

	retval_t (*phy_get_channel_num)(phy_layer_data_t* phy_layer_data, uint32_t* channel_num);
	retval_t (*phy_set_freq_channel)(phy_layer_data_t* phy_layer_data, uint32_t channel_num);
	retval_t (*phy_get_freq_channel)(phy_layer_data_t* phy_layer_data, uint32_t* channel_num);	//Get the current freq channel

	retval_t (*phy_set_baud_rate)(phy_layer_data_t* phy_layer_data, uint32_t baud_rate);
	retval_t (*phy_set_out_power)(phy_layer_data_t* phy_layer_data, int16_t out_power);

	retval_t (*phy_encrypt_packet)(phy_layer_data_t* phy_layer_data, net_packet_t* packet, uint8_t* key);
	retval_t (*phy_decrypt_packet)(phy_layer_data_t* phy_layer_data, net_packet_t* packet, uint8_t* key);
}phy_layer_funcs_t;



//* Called from mac layer
retval_t phy_send_packet(net_packet_t* packet);

//* Called from phy layer
retval_t phy_post_event_packet_sent(net_packet_t* packet);
retval_t phy_post_event_packet_received(net_packet_t* packet);

//* Called from mac layer. Inmediate functions: No event posted
retval_t phy_set_mode_receiving(void);
retval_t phy_set_mode_idle(void);
retval_t phy_set_mode_sleep(void);

float32_t phy_check_channel_rssi(void);
float32_t phy_get_last_rssi(void);

retval_t phy_set_base_freq(uint32_t base_freq);
uint32_t phy_get_base_freq(void);

uint32_t phy_get_channel_num_in_freq(uint32_t base_freq);
retval_t phy_set_freq_channel(uint32_t channel_num);
uint32_t phy_get_freq_channel(void);

retval_t phy_set_baud_rate(uint32_t baud_rate);
retval_t phy_set_out_power(int16_t out_power);

retval_t phy_encrypt_packet(phy_layer_data_t* phy_layer_data, net_packet_t* packet, uint8_t* key);
retval_t phy_decrypt_packet(phy_layer_data_t* phy_layer_data, net_packet_t* packet, uint8_t* key);

#endif /* APPLICATION_CORE_NET_INCLUDE_PHY_LAYER_H_ */
