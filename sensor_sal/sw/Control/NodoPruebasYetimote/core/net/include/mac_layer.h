/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * mac_layer.h
 *
 *  Created on: 22 de nov. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file mac_layer.h
 */
#ifndef APPLICATION_CORE_NET_INCLUDE_MAC_LAYER_H_
#define APPLICATION_CORE_NET_INCLUDE_MAC_LAYER_H_

#include "system_api.h"

typedef void* mac_addr_t;

#include "mac_layer_core.h"

typedef void mac_layer_data_t;

typedef struct mac_layer_funcs_{
	retval_t (*mac_layer_init)(mac_layer_data_t** mac_layer_data);
	retval_t (*mac_layer_deinit)(mac_layer_data_t* mac_layer_data);

	retval_t (*mac_send_packet)(mac_layer_data_t* mac_layer_data, mac_addr_t mac_addr_dest_fd, net_packet_t* packet);
	retval_t (*mac_rcv_packet)(mac_layer_data_t* mac_layer_data);

	retval_t (*mac_send_packet_done)(mac_layer_data_t* mac_layer_data, net_packet_t* packet);
	retval_t (*mac_rcv_packet_done)(mac_layer_data_t* mac_layer_data, net_packet_t* packet);

	retval_t (*mac_set_addr)(mac_layer_data_t* mac_layer_data, mac_addr_t mac_addr_fd);

	retval_t (*mac_new_addr)(mac_layer_data_t* mac_layer_data, char* mac_str_val, char fromat, mac_addr_t* mac_addr_fd);
	retval_t (*mac_delete_addr)(mac_layer_data_t* mac_layer_data, mac_addr_t mac_addr_fd);
	retval_t (*mac_addr_to_string)(mac_layer_data_t* mac_layer_data, mac_addr_t mac_addr_fd, char* mac_str_val, char format);
	retval_t (*mac_addr_cpy)(mac_layer_data_t* mac_layer_data, mac_addr_t dest_addr_fd, mac_addr_t from_addr_fd);
	retval_t (*mac_addr_cmp)(mac_layer_data_t* mac_layer_data, mac_addr_t a_addr_fd, mac_addr_t b_addr_fd);
}mac_layer_funcs_t;


//* Called from transport layer
retval_t mac_send_packet(mac_addr_t dest_mac_addr_fd, net_packet_t* packet);
retval_t mac_read_packet(void);

//* Called from mac layer
retval_t mac_packet_sent(net_packet_t* packet);
retval_t mac_packet_received(net_packet_t* packet);

//* Called from user space or same layer. Inmediate Function
retval_t mac_set_node_addr(mac_addr_t mac_addr_fd);

//* Called from any layer. Inmediate Functions
mac_addr_t mac_new_addr(char* mac_str_val, char format);
retval_t mac_delete_addr(mac_addr_t mac_addr);
retval_t mac_addr_to_string(mac_addr_t mac_addr, char* mac_str_val, char format);
retval_t mac_addr_cpy(mac_addr_t dest_addr, mac_addr_t from_addr);
uint16_t mac_addr_cmp(mac_addr_t a_addr, mac_addr_t b_addr);


#endif /* APPLICATION_CORE_NET_INCLUDE_MAC_LAYER_H_ */
