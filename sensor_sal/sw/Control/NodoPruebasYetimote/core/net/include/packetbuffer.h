/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * packetbuffer.h
 *
 *  Created on: 22 de nov. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file packetbuffer.h
 */
#ifndef APPLICATION_CORE_NET_INCLUDE_PACKETBUFFER_H_
#define APPLICATION_CORE_NET_INCLUDE_PACKETBUFFER_H_

#include "system_api.h"
#include "netstack-conf.h"

#define FIXED_PACKET_SIZE	64


/* PHY */
#define PHY_LAYER_HDR_T			CONCAT(PHY_LAYER, _hdr_t)
//#define INTER_PHY_HDR_T			CONCAT(PHY_LAYER, _inter_hdr_t)
#define INTER_PHY_LAYER_BITS	CONCAT(PHY_LAYER, _INTERLAYER_BITS)
#define PHY_HEADER_SIZE			(sizeof(PHY_LAYER_HDR_T)

/* MAC */
#define MAC_LAYER_HDR_T			CONCAT(MAC_LAYER, _hdr_t)
//#define INTER_MAC_HDR_T			CONCAT(MAC_LAYER, _inter_hdr_t)
#define INTER_MAC_LAYER_BITS	CONCAT(MAC_LAYER, _INTERLAYER_BITS)
#define MAC_HEADER_SIZE			(sizeof(MAC_LAYER_HDR_T))

/* ROUTING */
#define ROUTING_LAYER_HDR_T		CONCAT(ROUTING_LAYER, _hdr_t)
//#define INTER_ROUTING_HDR_T		CONCAT(ROUTING_LAYER, _inter_hdr_t)
#define INTER_RT_LAYER_BITS		CONCAT(ROUTING_LAYER, _INTERLAYER_BITS)
#define ROUTING_HEADER_SIZE		(sizeof(ROUTING_LAYER_HDR_T))

/* TRANSPORT */
#define TRANSPORT_LAYER_HDR_T	CONCAT(TRANSPORT_LAYER, _hdr_t)
//#define INTER_TRANSPORT_HDR_T	CONCAT(TRANSPORT_LAYER, _inter_hdr_t)
#define INTER_TP_LAYER_BITS		CONCAT(TRANSPORT_LAYER, _INTERLAYER_BITS)
#define TRANSPORT_HEADER_SIZE	(sizeof(TRANSPORT_LAYER_HDR_T))


typedef struct __packed interlayer_header_{
#if INTER_PHY_LAYER_BITS > 0
	uint8_t inter_phy_hdr_bits:INTER_PHY_LAYER_BITS;
#endif
#if INTER_MAC_LAYER_BITS > 0
	uint8_t inter_mac_hdr_bits:INTER_MAC_LAYER_BITS;
#endif
#if INTER_RT_LAYER_BITS > 0
	uint8_t inter_routing_hdr_bits:INTER_RT_LAYER_BITS;
#endif
#if INTER_TP_LAYER_BITS > 0
	uint8_t inter_transport_hdr_bits:INTER_TP_LAYER_BITS;
#endif
}interlayer_header_t;

typedef struct __packed packet_header_{
	PHY_LAYER_HDR_T phy_hdr;
	MAC_LAYER_HDR_T mac_hdr;
	ROUTING_LAYER_HDR_T rt_hdr;
	TRANSPORT_LAYER_HDR_T tp_hdr;
	interlayer_header_t interlayer_hdr;
}packet_header_t;


#define PACKET_HEADER_SIZE				(sizeof(packet_header_t))

#define PACKET_DATA_SIZE				(FIXED_PACKET_SIZE-PACKET_HEADER_SIZE)

#define PACKET_BUFFER_SIZE				2			//Number of packets in each packet buffer: 2 packets => 128 bytes

#define MAX_PACKET_BUFFER_DATA_SIZE		PACKET_DATA_SIZE*PACKET_BUFFER_SIZE	//Maximun size of data storable in packetbuffer


typedef struct __packed net_packet_{
	packet_header_t header;
	uint8_t data[PACKET_DATA_SIZE];

	//* Exra data used internally. This wont me sent in a packet */
	void* extra_info;
	float32_t pckt_rssi;
	uint8_t used_packet_flag;
}net_packet_t;

typedef struct __packed packetbuffer_{
	net_packet_t packet_buffer[PACKET_BUFFER_SIZE];
	uint32_t packetbuffer_mutex;
	uint16_t current_num_packets;
}packetbuffer_t;


retval_t rx_packetbuffer_init(void);	//Initialize packetbuffer values, flags and mutex
retval_t rx_packetbuffer_deinit(void);	//DeInitialize packetbuffer values, flags and mutex
net_packet_t* rx_packetbuffer_get_free_packet(void);
retval_t rx_packetbuffer_release_packet(net_packet_t* packet);


retval_t tx_packetbuffer_init(void);	//Initialize packetbuffer values, flags and mutex
retval_t tx_packetbuffer_deinit(void);	//DeInitialize packetbuffer values, flags and mutex
net_packet_t* tx_packetbuffer_get_free_packet(void);
retval_t tx_packetbuffer_release_packet(net_packet_t* packet);


#endif /* APPLICATION_CORE_NET_INCLUDE_PACKETBUFFER_H_ */
