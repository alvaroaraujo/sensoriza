/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * netstack.c
 *
 *  Created on: 24 de nov. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file netstack.c
 */
#include "netstack.h"
#include "system_api.h"
#include "netstack-conf.h"

netstack_state_t netstack_state = NS_STACK_NO_INIT;

#if ENABLE_NETSTACK_ARCH

#define DEBUG 1
#if DEBUG
#include "yetimote_stdio.h"
#define PRINTF(...) _printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif


#define NETSTACK_REACTOR_HANDLE_TIMEOUT	50

#define NETSTACK_THREAD_STACK_SIZE		320

#define TRANSPORT_LAYER_FUNCS			CONCAT(TRANSPORT_LAYER, _funcs)
#define ROUTING_LAYER_FUNCS				CONCAT(ROUTING_LAYER, _funcs)
#define MAC_LAYER_FUNCS					CONCAT(MAC_LAYER, _funcs)
#define PHY_LAYER_FUNCS					CONCAT(PHY_LAYER, _funcs)

extern tp_layer_funcs_t TRANSPORT_LAYER_FUNCS;
extern rt_layer_funcs_t ROUTING_LAYER_FUNCS;
extern mac_layer_funcs_t MAC_LAYER_FUNCS;
extern phy_layer_funcs_t PHY_LAYER_FUNCS;

netstack_t netstack;

void netstackProc(void const * argument);

/* INIT DE INIT FUNCTIONS*/
/**
 *
 * @param netstack
 * @return
 */
retval_t netstack_init(void){

	netstack.netstack_data  = (netstack_data_t*) ytMalloc(sizeof(netstack_data_t));
	netstack.netstack_data->netstack_reactor = ytReactorCreate (NETSTACK_REACTOR_HANDLE_TIMEOUT);

	tx_packetbuffer_init();
	rx_packetbuffer_init();


	if((netstack.phy_layer = init_phy_layer(&PHY_LAYER_FUNCS)) == NULL){
		ytReactorDelete(netstack.netstack_data->netstack_reactor);
		ytFree(netstack.netstack_data);
		netstack.netstack_data = NULL;
		PRINTF(">ERROR: UNABLE TO INIT PHY LAYER\r\n");
		PRINTF(">NETSTACK ERROR\r\n");
		return RET_ERROR;
	}
	PRINTF(">PHY Layer Init\r\n");

	if((netstack.mac_layer = init_mac_layer(&MAC_LAYER_FUNCS)) == NULL){
		deinit_phy_layer(netstack.phy_layer);
		ytReactorDelete(netstack.netstack_data->netstack_reactor);
		ytFree(netstack.netstack_data);
		netstack.netstack_data = NULL;
		PRINTF(">ERROR: UNABLE TO INIT MAC LAYER\r\n");
		PRINTF(">NETSTACK ERROR\r\n");
		return RET_ERROR;
	}
	PRINTF(">MAC Layer Init\r\n");

	if((netstack.routing_layer = init_routing_layer(&ROUTING_LAYER_FUNCS)) == NULL){
		deinit_mac_layer(netstack.mac_layer);
		deinit_phy_layer(netstack.phy_layer);
		ytReactorDelete(netstack.netstack_data->netstack_reactor);
		ytFree(netstack.netstack_data);
		netstack.netstack_data = NULL;
		PRINTF(">ERROR: UNABLE TO INIT ROUTING LAYER\r\n");
		PRINTF(">NETSTACK ERROR\r\n");
		return RET_ERROR;
	}
	PRINTF(">ROUTING Layer Init\r\n");

	if((netstack.transport_layer = init_transport_layer(&TRANSPORT_LAYER_FUNCS)) == NULL){
		deinit_routing_layer(netstack.routing_layer);
		deinit_mac_layer(netstack.mac_layer);
		deinit_phy_layer(netstack.phy_layer);
		ytReactorDelete(netstack.netstack_data->netstack_reactor);
		ytFree(netstack.netstack_data);
		netstack.netstack_data = NULL;
		PRINTF(">ERROR: UNABLE TO INIT TRANSPORT LAYER\r\n");
		PRINTF(">NETSTACK ERROR\r\n");
		return RET_ERROR;
	}
	PRINTF(">TRANSPORT Layer Init\r\n");


	ytStartProcess("NETSTACK_PROCESS", netstackProc, HIGH_PRIORITY_PROCESS, NETSTACK_THREAD_STACK_SIZE, &(netstack.netstack_data->netstack_thread_id), &netstack);

	PRINTF(">NETSTACK RUNNING\r\n");

	netstack_state = NS_STACK_INIT;
	return RET_OK;
}

/**
 *
 * @param netstack
 * @return
 */
retval_t netstack_deinit(void){

	if (deinit_transport_layer(netstack.transport_layer) != RET_OK){
		return RET_ERROR;
	}
	if(deinit_routing_layer(netstack.routing_layer) != RET_OK){
		return RET_ERROR;
	}
	if(deinit_mac_layer(netstack.mac_layer) != RET_OK){
		return RET_ERROR;
	}
	if(deinit_phy_layer(netstack.phy_layer) != RET_OK){
		return RET_ERROR;
	}

	tx_packetbuffer_deinit();
	rx_packetbuffer_deinit();
	ytReactorDelete(netstack.netstack_data->netstack_reactor);
	ytExitProcess(netstack.netstack_data->netstack_thread_id);

	ytFree(netstack.netstack_data);
	netstack.netstack_data = NULL;

	netstack_state = NS_STACK_NO_INIT;
	return RET_OK;
}
/* END INIT DE INIT FUNCTIONS*/



/* Functiones usadas solo internamente */
/**
 *
 * @param semph_id
 * @return
 */
retval_t netstack_op_done(uint32_t semph_id){
	if(ytSemaphoreRelease(semph_id) != RET_OK){
		return RET_ERROR;
	}
	return RET_OK;
}

/**
 *
 * @return
 */
transport_layer_t* netstack_get_tp_layer(void){
	return netstack.transport_layer;
}
/**
 *
 * @return
 */
routing_layer_t* netstack_get_rt_layer(void){
	return netstack.routing_layer;
}
/**
 *
 * @return
 */
mac_layer_t* netstack_get_mac_layer(void){
	return netstack.mac_layer;
}
/**
 *
 * @return
 */
phy_layer_t* netstack_get_phy_layer(void){
	return netstack.phy_layer;
}

/* Funciones que pueden ser usadas por el desarrollador de capas*/
/**
 *
 * @param ev_handler
 * @param args
 * @return
 */
retval_t netstack_post_event(handle_event_func ev_handler_func, void* args){
	return ytPostEvent(netstack.netstack_data->netstack_reactor, ev_handler_func, args);
}

/**
 *
 * @return
 */
netstack_state_t get_netstack_state(void){
	return netstack_state;
}

#endif
