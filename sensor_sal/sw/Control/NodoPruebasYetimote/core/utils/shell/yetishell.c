/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * yetishell.c
 *
 *  Created on: 11 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file yetishell.c
 */

#include "yetishell.h"
#include "system_api.h"

#if USE_YETISHELL

extern stdio_input_t* stdio_input;

retval_t null_func(uint16_t argc, char** argv);

retval_t no_cmd_command(uint16_t argc, char** argv);
retval_t error_wrong_params(uint16_t argc, char** argv);
retval_t error_no_param_command(uint16_t argc, char** argv);

retval_t list_cmd_command(uint16_t argc, char** argv);
retval_t print_mem_command(uint16_t argc, char** argv);
retval_t print_addr_command(uint16_t argc, char** argv);
retval_t open_comm_channel_command(uint16_t argc, char** argv);
retval_t send_message_on_channel(uint16_t argc, char** argv);
retval_t change_radio(uint16_t argc, char** argv);
retval_t get_time(uint16_t argc, char** argv);
retval_t set_time(uint16_t argc, char** argv);


command_data_t* new_command_data(void){

	command_data_t* new_command = (command_data_t*) ytMalloc(sizeof(command_data_t));
	new_command->argc = 0;
	new_command->command_func = null_func;

	return new_command;
}
retval_t remove_command_data(command_data_t* command_data){

	if(command_data != NULL){
		ytFree(command_data);
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t select_command(){
	if(stdio_input->command != NULL){
		if(stdio_input->command->argc != 0){
			/////////////////////////////////////////////////////////////////////
			if(strcmp(stdio_input->command->argv[0],LIST_CMD) == 0){
				stdio_input->command->command_func = list_cmd_command;
			}
			else if(strcmp(stdio_input->command->argv[0],PRINT_MEM) == 0){
				stdio_input->command->command_func = print_mem_command;
			}
			else if(strcmp(stdio_input->command->argv[0],PRINT_ADDR) == 0){
				stdio_input->command->command_func = print_addr_command;
			}
			else if(strcmp(stdio_input->command->argv[0],OPEN_COMM_CHANNEL) == 0){
				stdio_input->command->command_func = open_comm_channel_command;
			}
			else if(strcmp(stdio_input->command->argv[0],SEND_MESSAGE_ON_CHANNEL) == 0){
				stdio_input->command->command_func = send_message_on_channel;
			}
			else if(strcmp(stdio_input->command->argv[0],CHANGE_RADIO) == 0){
				stdio_input->command->command_func = change_radio;
			}
			else if(strcmp(stdio_input->command->argv[0],SET_TIME) == 0){
				stdio_input->command->command_func = set_time;
			}
			else if(strcmp(stdio_input->command->argv[0],GET_TIME) == 0){
				stdio_input->command->command_func = get_time;
			}
			else if(strcmp(stdio_input->command->argv[0],TB_CONFIG) == 0){
				//tb_cmd_packet_t* cmd_to_send = init_tb_cmd_packet();
//				stdio_input->command->command_func = send_config;
			}
			else if(strcmp(stdio_input->command->argv[0],ACC) == 0){
//				stdio_input->command->command_func = acc_config;
			}
			else{
				stdio_input->command->command_func = no_cmd_command;
				return RET_ERROR;
			}
			return RET_OK;
		}
		else{
			return RET_OK;
		}

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t execute_command(){
	if(stdio_input->command != NULL){
		if(stdio_input->command->argc != 0){
			uint16_t i = 0;

			stdio_input->command->command_func(stdio_input->command->argc, stdio_input->command->argv);

			stdio_input->command->command_func = null_func;

			for (i=0; i<stdio_input->command->argc; i++){			//When a command is executed, the memory occupied by the args must be freed
				if(stdio_input->command->argv[i] != NULL){
					ytFree(stdio_input->command->argv[i]);
					stdio_input->command->argv[i] = NULL;
				}
			}
			if(stdio_input->command->argv != NULL){
				ytFree(stdio_input->command->argv);
				stdio_input->command->argv = NULL;
			}

			stdio_input->command->argc = 0;

			_printf(">");
		}

		return RET_OK;

	}
	else{
		return RET_ERROR;
	}
}


retval_t null_func(uint16_t argc, char** argv){
	return RET_OK;
}




/**
 * @brief	Default message shown when entered an invalid command
 */
retval_t no_cmd_command(uint16_t argc, char** argv){
	_printf(">Invalid command\r\n");
	return RET_OK;
}

/**
 * @brief	Function that showsa list of the existing available commands
 */
retval_t list_cmd_command(uint16_t argc, char** argv){

	_printf("Available commands:\r\n");

//////////////START COMMAND ZONE//////////////////////
#ifdef LIST_CMD
	_printf("-%s\r\n",LIST_CMD);
#endif

#ifdef PRINT_MEM
	_printf("-%s\r\n",PRINT_MEM);
#endif

#ifdef PRINT_ADDR
	_printf("-%s\r\n",PRINT_ADDR);
#endif

#ifdef OPEN_COMM_CHANNEL
	_printf("-%s\r\n",OPEN_COMM_CHANNEL);
#endif
#ifdef SEND_MESSAGE_ON_CHANNEL
	_printf("-%s\r\n",SEND_MESSAGE_ON_CHANNEL);
#endif
#ifdef CHANGE_RADIO
	_printf("-%s\r\n",CHANGE_RADIO);
#endif
#ifdef SET_TIME
	_printf("-%s\r\n",SET_TIME);
#endif
#ifdef GET_TIME
	_printf("-%s\r\n",GET_TIME);
#endif
#ifdef TB_CONFIG
	_printf("-%s\r\n",TB_CONFIG);
#endif

//////////////END COMMAND ZONE//////////////////////
	return RET_OK;
}


/**
 * @brief	Shows the available memory on FreeRTOS in KB
 */
retval_t print_mem_command(uint16_t argc, char** argv){
	size_t freeMEM = xPortGetFreeHeapSize();
	_printf( ">Available Memory: %.3f KB\r\n", (float) ((float)freeMEM/1000));
	return RET_OK;
}

/**
 * @brief	Shows the rime address of the node
 */
retval_t print_addr_command(uint16_t argc, char** argv){
	uint32_t addrByte = 0xFFFFFFFF;

	//	addrByte = linkaddr_node_addr.u8[0];

	_printf("Node address (Bytes): %d.",(int) addrByte);

//	addrByte = linkaddr_node_addr.u8[1];

	_printf("%d\r\n",(int) addrByte);

//	addrByte = linkaddr_node_addr.u16;

	_printf("Node address (Dec): %d\r\n",(int) addrByte);
	return RET_OK;
}

/**
 * @brief	Prints an error when there are some missing params for command execution
 */
retval_t error_no_param_command(uint16_t argc, char** argv){

	_printf("Command error. Missing params\r\n");
	return RET_OK;
}

/**
 * @brief	Prints an error when there are too much params or wrong params
 */
retval_t error_wrong_params(uint16_t argc, char** argv){
	_printf("Command error. Wrong params\r\n");
	return RET_OK;
}


/**
 * @brief 			Open a radio communication channel
 * @param argc		Number of args passed to the command
 * @param argv		Pointer to args passed to the command
 */
retval_t open_comm_channel_command(uint16_t argc, char** argv){

	///Solo se ha escrito el nombre del comando sin argumentos. Da error
	if(argc == 1){
		error_no_param_command(argc, argv);
		_printf("Write -h for command usage\r\n");
	}

	///Con solo el nombre y otro argumento la �nica opci�n es haber marcado -h. En caso contrario error
	else if(argc == 2){
		if (strcmp(argv[1], "-h") == 0){
			_printf("Usage: ");
			_printf(OPEN_COMM_CHANNEL);
			_printf(" [OPTIONS] [channel]\r\n");
			_printf("Open a communication channel\r\n\r\n");
			_printf("\t-h\t: print command help\r\n");
			_printf("\t-b\t: Open the specified broadcast channel\r\n");
			_printf("\t-u\t: Open the specified unicast channel\r\n");
			_printf("\t-m\t: Open the specified mesh channel. Mesh connection opens 3 consecutive channels directly\r\n");
			_printf("\t-p\t: Select this option to print received data on the opened channel\r\n");
			_printf("\tchannel\t: The desired channel to be opened (from 1 to 65535)\r\n\r\n");
			_printf("Usage Example:\r\n");
			_printf("\t");
			_printf(OPEN_COMM_CHANNEL);
			_printf(" -p -u 312\r\n");
			_printf("\tThis will open the 312 unicast channel and will print the received data on this channel\r\n\r\n");
		}
		else{
			error_wrong_params(argc, argv);
			_printf("Write -h for command usage\r\n");
		}
	}

	///Con el comando y 2 argumentos la opci�n -p nunca va a existir. Solo un tipo de canal -b, -u, -m. (Adem�s del canal)
//	else if(argc == 3){
//
//		if (strcmp(argv[1], "-b") == 0){
//			uint16_t channel = atoi(argv[2]);
//			yetimote_broadcast_open(bc_comm, channel, broadcast_noprint);
//			_printf("Broadcast channel ");
//			_printf(argv[2]);
//			_printf(" successfully opened\r\n");
//		}
//		else if (strcmp(argv[1], "-u") == 0){
//			uint16_t channel = atoi(argv[2]);
//			yetimote_unicast_open(uc_comm, channel, unicast_noprint);
//			_printf("Unicast channel ");
//			_printf(argv[2]);
//			_printf(" successfully opened\r\n");
//		}
//		else if (strcmp(argv[1], "-m") == 0){
//			uint16_t channel = atoi(argv[2]);
//			yetimote_mesh_open(msh_comm, channel, mesh_noprint);
//			_printf("Mesh channel ");
//			_printf(argv[2]);
//			_printf(" successfully opened\r\n");
//		}
//		else{
//			error_wrong_params(argc, argv);
//			_printf("Write -h for command usage\r\n");
//		}
//
//	}
//
//	///Con el comando y 3 argumentos tiene que aparecer un tipo de canal y la opci�n -p. (Adem�s del canal)
//	else if(argc == 4){
//
//		if (strcmp(argv[1], "-p") == 0){
//
//			if (strcmp(argv[2], "-b") == 0){
//				uint16_t channel = atoi(argv[3]);
//				yetimote_broadcast_open(bc_comm, channel, broadcast_print);
//				_printf("Broadcast channel ");
//				_printf(argv[3]);
//				_printf(" successfully opened\r\n");
//			}
//			else if (strcmp(argv[2], "-u") == 0){
//				uint16_t channel = atoi(argv[3]);
//				yetimote_unicast_open(uc_comm, channel, unicast_print);
//				_printf("Unicast channel ");
//				_printf(argv[3]);
//				_printf(" successfully opened\r\n");
//			}
//			else if (strcmp(argv[2], "-m") == 0){
//				uint16_t channel = atoi(argv[3]);
//				yetimote_mesh_open(msh_comm, channel, mesh_print);
//				_printf("Mesh channel ");
//				_printf(argv[3]);
//				_printf(" successfully opened\r\n");
//			}
//			else{
//				error_wrong_params(argc, argv);
//				_printf("Write -h for command usage\r\n");
//			}
//
//		}
//
//		else if (strcmp(argv[1], "-b") == 0){
//
//			if (strcmp(argv[2], "-p") == 0){
//
//				uint16_t channel = atoi(argv[3]);
//				yetimote_broadcast_open(bc_comm, channel, broadcast_print);
//				_printf("Broadcast channel ");
//				_printf(argv[3]);
//				_printf(" successfully opened\r\n");
//			}
//			else{
//				error_wrong_params(argc, argv);
//				_printf("Write -h for command usage\r\n");
//			}
//
//		}
//		else if (strcmp(argv[1], "-u") == 0){
//
//			if (strcmp(argv[2], "-p") == 0){
//				uint16_t channel = atoi(argv[3]);
//				yetimote_unicast_open(uc_comm, channel, unicast_print);
//				_printf("Unicast channel ");
//				_printf(argv[3]);
//				_printf(" successfully opened\r\n");
//			}
//			else{
//				error_wrong_params(argc, argv);
//				_printf("Write -h for command usage\r\n");
//			}
//
//		}
//		else if (strcmp(argv[1], "-m") == 0){
//
//			if (strcmp(argv[2], "-p") == 0){
//				uint16_t channel = atoi(argv[3]);
//				yetimote_mesh_open(msh_comm, channel, mesh_print);
//				_printf("Mesh channel ");
//				_printf(argv[3]);
//				_printf(" successfully opened\r\n");
//			}
//			else{
//				error_wrong_params(argc, argv);
//				_printf("Write -h for command usage\r\n");
//			}
//
//		}
//		else{
//			error_wrong_params(argc, argv);
//			_printf("Write -h for command usage\r\n");
//		}
//
//	}

	else{
		error_wrong_params(argc, argv);
		_printf("Write -h for command usage\r\n");
	}

	return RET_OK;
}


/**
 * @brief 			Sends a message on an opened radio channel. The message to send must be under quotes "".
 * @param argc		Number of args passed to the command
 * @param argv		Pointer to args passed to the command
 */
retval_t send_message_on_channel(uint16_t argc, char** argv){

	///Solo se ha escrito el nombre del comando sin argumentos. Da error
	if(argc == 1){
		error_no_param_command(argc, argv);
		_printf("Write -h for command usage\r\n");
	}

	///Con solo el nombre y otro argumento la �nica opci�n es haber marcado -h. En caso contrario error
	else if(argc == 2){
		if (strcmp(argv[1], "-h") == 0){
			_printf("Usage: ");
			_printf(SEND_MESSAGE_ON_CHANNEL);
			_printf(" [OPTIONS] [channel] [addr (bc, mesh)] [\"message\"]\r\n");
			_printf("Send a message on an opened channel to the specified address (only for unicast and mesh)\r\n\r\n");
			_printf("\t-h\t: print command help\r\n");
			_printf("\t-b\t: The channel to send the message is mesh broadcast\r\n");
			_printf("\t-u\t: The channel to send the message is mesh unicast\r\n");
			_printf("\t-m\t: The channel to send the message is mesh\r\n");
			_printf("\tchannel\t: The desired channel on which send the message (from 1 to 65535)\r\n");
			_printf("\tmessage\t: The message to send. Under quotes\r\n\r\n");
			_printf("Usage Example:\r\n");
			_printf("\t");
			_printf(SEND_MESSAGE_ON_CHANNEL);
			_printf(" -b 312 \"HELLO\"\r\n");
			_printf("\tThis will send the message HELLO on the previously opened 312 broadcast channel\r\n\r\n");
		}
		else{
			error_wrong_params(argc, argv);
			_printf("Write -h for command usage\r\n");
		}
	}


	/// Error al meter los parametros
	else{
		error_wrong_params(argc, argv);
		_printf("Write -h for command usage\r\n");
	}
	return RET_OK;

}


retval_t change_radio(uint16_t argc, char** argv){

	if(argc == 1){
			error_no_param_command(argc, argv);
			_printf("Write -h for command usage\r\n");
		}

		///Con solo el nombre y otro argumento la �nica opci�n es haber marcado -h. En caso contrario error
	else if(argc == 2){
		if (strcmp(argv[1], "-h") == 0){
			_printf("Usage: ");
			_printf(CHANGE_RADIO);
			_printf(" [RADIO_INTERFACE]\r\n");
			_printf("Change the radio interface used\r\n\r\n");
			_printf("\t-h\t: print command help\r\n");
			_printf("\t  \t: cc2500\r\n");
			_printf("\t  \t: spirit_433\r\n");
			_printf("\t  \t: spirit_868\r\n");
			_printf("Usage Example:\r\n");
			_printf("\t");
			_printf(CHANGE_RADIO);
			_printf(" cc1101 \r\n");
			_printf("\tThis will change the radio interface to the CC1101\r\n\r\n");
		}

		else{
			error_wrong_params(argc, argv);
			_printf("Write -h for command usage\r\n");
		}
	}
	else{
		error_wrong_params(argc, argv);
		_printf("Write -h for command usage\r\n");
	}
	return RET_OK;
}


retval_t get_time(uint16_t argc, char** argv){
	if(argc == 1){

	}
	else if(argc == 2){
		if (strcmp(argv[1], "-h") == 0){
			_printf("Usage: ");
			_printf(GET_TIME);
			_printf("\r\n");
			_printf("Shows the current system time\r\n\r\n");
			_printf("\t-h\t: print command help\r\n\r\n");
		}
		else{
			error_wrong_params(argc, argv);
		}
	}
	else{
		error_wrong_params(argc, argv);
	}
	return RET_OK;
}

retval_t set_time(uint16_t argc, char** argv){
	if(argc == 1){

		}
		else if(argc == 2){
			if (strcmp(argv[1], "-h") == 0){
				_printf("Usage: ");
				_printf(SET_TIME);
				_printf(" [timestamp ms]\r\n");
				_printf("Sets the system time in ms\r\n\r\n");
				_printf("\t-h\t: print command help\r\n\r\n");
			}
			else{

			}
		}
		else{
			error_wrong_params(argc, argv);
		}
		return RET_OK;
}

#endif
