/*
 * circ_buf.c
 *
 *  Created on: 27 jul. 2018
 *      Author: miguelvp
 */

#include "circ_buf.h"
#include "system_api.h"

int circular_buf_init(circular_buf_t *cbuf, uint16_t size)
{
	cbuf->size = size;
	cbuf->buffer = (uint16_t*) ytMalloc(size*sizeof(cbuf->size));
	return circular_buf_reset(cbuf);
}

int circular_buf_reset(circular_buf_t *cbuf)
{
    int r = -1;

    if(cbuf)
    {
        cbuf->head = 0;
        cbuf->tail = 0;
        cbuf->full = 0;
        r = 0;
    }

    return r;
}

int circular_buf_empty(circular_buf_t cbuf)
{
    return (cbuf.head == cbuf.tail) && !cbuf.full;
}

int circular_buf_full(circular_buf_t cbuf)
{
    return cbuf.full;
}

int circular_buf_put(circular_buf_t *cbuf, uint16_t data)
{
    int r = -1;

    if(cbuf)
    {
        cbuf->buffer[cbuf->head] = data;
        cbuf->head = (cbuf->head + 1) % cbuf->size;

        if(cbuf->head == cbuf->tail)
        {
            cbuf->tail = (cbuf->tail + 1) % cbuf->size;
            cbuf->full = 1;
        }

        r = 0;
    }

    return r;
}

int circular_buf_get(circular_buf_t * cbuf, uint16_t * data)
{
    int r = -1;

    if(cbuf && data && !circular_buf_empty(*cbuf))
    {
        *data = cbuf->buffer[cbuf->tail];
        cbuf->tail = (cbuf->tail + 1) % cbuf->size;

        r = 0;
    }

    return r;
}
