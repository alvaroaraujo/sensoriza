/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * time_meas.c
 *
 *  Created on: 7 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file time_meas.c
 */

#include "time_meas.h"
#include "system_api.h"



/**
 * @brief	Initialize the HW necessary for time measurements
 * @return	Return status
 */
retval_t init_time_meas(void){
#if ENABLE_TIME_MEASURE_FUNCS
	#if USE_HIGH_RES_TIMER
		return  init_hs_timer_arch();
	#endif
	#if USE_LOW_RES_TIMER
		return RET_OK; //Do nothing, as RTC should be initialized on startup
	#endif
#else
		return RET_ERROR;
#endif
}

/**
 *
 * @return
 */
time_meas_t* new_time_meas(){
#if ENABLE_TIME_MEASURE_FUNCS
	time_meas_t* new_time_meas = (time_meas_t*) ytMalloc(sizeof(time_meas_t));
	new_time_meas->elapsed_time = 0;
	new_time_meas->start_time = 0;
	new_time_meas->stop_time = 0;

	return new_time_meas;
#else
	return NULL;
#endif
}

/**
 *
 * @param time_meas
 * @return
 */
retval_t delete_time_meas(time_meas_t* time_meas){
#if ENABLE_TIME_MEASURE_FUNCS
	if(time_meas == NULL){
		return RET_ERROR;
	}
	ytFree(time_meas);
	return RET_OK;
#else
	return RET_ERROR;
#endif
}

/**
 * @brief			Start the time measurement
 * @note			The HW HS timer count is reseted when starting measure. Therefore only one thread should access to these functions to get reliable values
 * @param time_meas	Time measurement struct
 * @return			Return Status
 */
retval_t start_time_measure(time_meas_t* time_meas){
#if ENABLE_TIME_MEASURE_FUNCS
	#if USE_HIGH_RES_TIMER
		hs_timer_reset_count();
		time_meas->start_time = hs_timer_get_count();
	#endif
	#if USE_LOW_RES_TIMER
		time_meas->start_time = (rtc_time_get_timestamp()*1000);
	#endif
		return RET_OK;
#else
	return RET_ERROR;
#endif
}

/**
 * @brief			Acquire the elapsed time since the previous start_time_measure() call
 * @param time_meas	Time mesasurement struct
 * @return			Return Status
 */
retval_t stop_time_measure(time_meas_t* time_meas){
#if ENABLE_TIME_MEASURE_FUNCS
	#if USE_HIGH_RES_TIMER
		time_meas->stop_time = hs_timer_get_count();
	#endif
	#if USE_LOW_RES_TIMER
		time_meas->stop_time = (rtc_time_get_timestamp()*1000);
	#endif

		time_meas->elapsed_time = time_meas->stop_time - time_meas->start_time;
		return RET_OK;
#else
		return RET_ERROR;
#endif
}
