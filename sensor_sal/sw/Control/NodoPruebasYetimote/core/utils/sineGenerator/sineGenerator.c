/*
 * sineGenerator.c
 *
 *  Created on: 30 jul. 2018
 *      Author: miguelvp
 */

#include "sineGenerator.h"
#include "system_api.h"
#include "arm_math.h"

static circular_buf_t *sine;
extern DAC_HandleTypeDef hdac1;

void sineInit(void)
{
	sine = (circular_buf_t*) ytMalloc(sizeof(circular_buf_t));
	circular_buf_init(sine, 100);
	HAL_DAC_Start(&hdac1, DAC1_CHANNEL_1);
}

void sineGenerate(void)
{
	int16_t nMuestras = sine->size;
	int16_t tamBuffer = 25;
	uint16_t i, j, min, max, factor, tmp;
	int32_t dest[tamBuffer], sour[tamBuffer];
	arm_biquad_casd_df1_inst_q31 S;
	int r;

	// Valores de b y a para filtrar un seno dada una se�al cuadrada con 100 muestras finales. Si cambia el numero de muestras, tambien cambian los coeficientes siguientes
	q31_t coeffSine[15] = {0x00000008, 0x00000011, 0x00000008, 0xFFFFE222, 0x00000E00, 0x00000008, 0x00000011, 0x00000008, 0xFFFFE222, 0x00000E00, 0x00000008, 0x00000011, 0x00000008, 0xFFFFE222, 0x00000E00};
	q31_t *estado = (q31_t*) ytMalloc(12*sizeof(q31_t));
	for(j = 0; j < tamBuffer; j++){
		sour[j] = 0;
		dest[j] = 0;
	}
	min = 4096;
	max = 0;
	arm_biquad_cascade_df1_init_q31(&S,3,coeffSine,estado, 19);
	for (i = 0; i < 3*nMuestras/tamBuffer; i++) {
		for(j = 0; j < tamBuffer; j++)
			sour[j] = 3800*((i+1) % 2)+150;
		arm_biquad_cascade_df1_q31(&S, sour, dest, tamBuffer);

		if (i > (2*nMuestras/tamBuffer - 1)) {
			for (j = 0; j < tamBuffer; j++) {
				circular_buf_put(sine, (uint16_t) dest[j]);
				if (dest[j] < min)
					min = dest[j];
				if (dest[j] > max)
					max = dest[j];
			}
		}
	}

	sine->tail = 0;
	factor = 2048/(max - min);

	for (i = 0; i < nMuestras; i++) {
		circular_buf_get(sine, &tmp);
		tmp = (tmp - min) * factor;
		circular_buf_put(sine, (uint16_t) tmp);
		if (i < nMuestras - 2)
			sine->tail--;
		else
			sine->tail = nMuestras - 1;
	}

	do {
		r = circular_buf_get(sine, &tmp);
	} while (tmp != 0 && r == 0);
}

void sineNext(void)
{
	uint16_t tmp;
	circular_buf_get(sine, &tmp);
	HAL_DAC_SetValue(&hdac1, DAC1_CHANNEL_1, DAC_ALIGN_12B_R, (uint32_t) tmp);
}

void sineReset(void)
{
	uint16_t min, tmp, i;
	min = 32000;
	for (i = 0; i < sine->size; i++) {
		circular_buf_get(sine, &tmp);
		if (tmp < min)
			min = tmp;
	}

	do {
		circular_buf_get(sine, &tmp);
	} while (tmp != min);
}
