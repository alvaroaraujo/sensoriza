/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * reactor.c
 *
 *  Created on: 16 de nov. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file reactor.c
 */

#include "reactor.h"
#include "system_api.h"

static event_handler_t* get_first_event(gen_list* handle_list);

/**
 *
 * @param handle_event_timeout
 * @return
 */
reactor_t*  new_reactor(uint32_t handle_event_timeout){
	reactor_t* new_reactor;

	if((new_reactor = (reactor_t*) ytMalloc(sizeof(reactor_t))) == NULL){
		return NULL;
	}
	if((new_reactor->reactor_semaphore_fd = ytSemaphoreCreate(1)) == 0){
		ytFree(new_reactor);
		return NULL;
	}
	if((new_reactor->reactor_mutex_fd = ytMutexCreate()) == 0){
		ytSemaphoreDelete(new_reactor->reactor_semaphore_fd);
		ytFree(new_reactor);
		return NULL;
	}
	if((new_reactor->handle_list = gen_list_init()) == NULL){
		ytSemaphoreDelete(new_reactor->reactor_semaphore_fd);
		ytMutexDelete(new_reactor->reactor_mutex_fd);
		ytFree(new_reactor);
		return NULL;
	}


	ytSemaphoreWait(new_reactor->reactor_semaphore_fd, YT_WAIT_FOREVER);
	new_reactor->handle_event_timeout = handle_event_timeout;
	new_reactor->pending_events = 0;

	return new_reactor;
}

/**
 *
 * @param reactor
 * @return
 */
retval_t delete_reactor(reactor_t* reactor){
	if(reactor == NULL){
		return RET_ERROR;
	}
	ytMutexDelete(reactor->reactor_mutex_fd);
	ytSemaphoreDelete(reactor->reactor_semaphore_fd);
	gen_list_remove_all(reactor->handle_list);

	ytFree(reactor);

	return RET_OK;
}

///**
// *
// * @param handle_func
// * @return
// */
//event_handler_t* new_event_handler(handle_event_func handle_func){
//	event_handler_t* new_event_handler;
//	if((new_event_handler = (event_handler_t*) ytMalloc(sizeof(event_handler_t))) == NULL){
//		return NULL;
//	}
//	new_event_handler->handle_func = handle_func;
//	new_event_handler->args = NULL;
//	return new_event_handler;
//}
//
///**
// *
// * @param event_handler
// * @return
// */
//retval_t delete_event_handler(event_handler_t* event_handler){
//	if(event_handler == NULL){
//		return RET_ERROR;
//	}
//	ytFree(event_handler);
//	return RET_OK;
//}

///**
// *
// * @param reactor
// * @param event_handler
// * @return
// */
//retval_t register_event_handler(reactor_t* reactor, event_handler_t* event_handler){
//	if((reactor == NULL) || (event_handler == NULL)){
//		return RET_ERROR;
//	}
//	ytMutexWait(reactor->reactor_mutex_fd, YT_WAIT_FOREVER);
//	gen_list_add(reactor->handle_list, (void*) event_handler);
//	ytMutexRelease(reactor->reactor_mutex_fd);
//	return RET_OK;
//}
//
///**
// *
// * @param reactor
// * @param event_handler
// * @return
// */
//retval_t unregister_event_handler(reactor_t* reactor, event_handler_t* event_handler){
//	if((reactor == NULL) || (event_handler == NULL)){
//		return RET_ERROR;
//	}
//	ytMutexWait(reactor->reactor_mutex_fd, YT_WAIT_FOREVER);
//	gen_list_remove(reactor->handle_list, (void*) event_handler);
//	ytMutexRelease(reactor->reactor_mutex_fd);
//	return RET_OK;
//}


/**
 *
 * @param reactor
 * @param event_id
 * @return
 */
retval_t post_event(reactor_t* reactor, handle_event_func handle_func, void* arg){
	event_handler_t* new_event_id;
	if((reactor == NULL) || (handle_func == NULL)){
		return RET_ERROR;
	}
	ytMutexWait(reactor->reactor_mutex_fd, YT_WAIT_FOREVER);

	new_event_id = (event_handler_t*) ytMalloc(sizeof(event_handler_t));

	new_event_id->handle_func = handle_func;
	new_event_id->args = arg;

	gen_list_add(reactor->handle_list, (void*) new_event_id);

	reactor->pending_events++;
	ytSemaphoreRelease(reactor->reactor_semaphore_fd);

	ytMutexRelease(reactor->reactor_mutex_fd);
	return RET_OK;

}



/**
 *
 * @param reactor
 * @return
 */
retval_t reactor_handle_events(reactor_t* reactor){
	event_handler_t* event_handler;

	if(reactor == NULL){
		return RET_ERROR;
	}

	if(!reactor->pending_events){
		ytSemaphoreWait(reactor->reactor_semaphore_fd, reactor->handle_event_timeout);
	}
	while(reactor->pending_events){

		ytMutexWait(reactor->reactor_mutex_fd, YT_WAIT_FOREVER);
		if((event_handler = get_first_event(reactor->handle_list)) != NULL){
			ytMutexRelease(reactor->reactor_mutex_fd);

			if(event_handler->handle_func){
				event_handler->handle_func(event_handler->args);
			}

			ytMutexWait(reactor->reactor_mutex_fd, YT_WAIT_FOREVER);
			ytFree(event_handler);
			reactor->pending_events--;
			ytMutexRelease(reactor->reactor_mutex_fd);
		}
		else{
			ytMutexRelease(reactor->reactor_mutex_fd);
			return RET_ERROR;
		}
	}

	return RET_OK;
}

/**
 *
 * @param handle_list
 * @return
 */
static event_handler_t* get_first_event(gen_list* handle_list){
	event_handler_t* event_handler;
	gen_list* element;
	gen_list* current;
	if(handle_list == NULL){
		return NULL;
	}

	current = handle_list;

	if(current->next != NULL){
		if(current->next->item != NULL){
			element = current->next;
			event_handler = (event_handler_t*) element->item;
			current->next = current->next->next;
			ytFree(element);
			return event_handler;
		}
	}

	return NULL;
}

///**
// *
// * @param handle_list
// * @param event_handler
// * @return
// */
//static retval_t move_event_to_last_unsignaled(gen_list* handle_list, event_handler_t* event_handler){
//
//	if((handle_list == NULL) || (event_handler == NULL)){
//		return RET_ERROR;
//	}
//	gen_list* current = handle_list;
//	gen_list* element = NULL;
//
//	while(current->next != NULL){
//
//		if(current->next->item == event_handler){		//Encontrado el elemento en la lista
//			gen_list* next = current->next->next;
//			element = current->next;
//			current->next = next;
//			break;
//		}
//
//		else{
//			current = current->next;
//		}
//
//	}
//	return RET_ERROR;	//Element not found
//
//ELEMENT_TO_END:
//	while(current->next != NULL){	//Go to the end of the list and place it the element
//		current = current->next;
//	}
//	current->next = element;
//	current->next->next = NULL;
//	return RET_OK;
//}
//
