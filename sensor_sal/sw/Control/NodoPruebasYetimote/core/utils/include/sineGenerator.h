/*
 * sineGenerator.h
 *
 *  Created on: 30 jul. 2018
 *      Author: miguelvp
 */

#ifndef YETIOS_CORE_UTILS_SINEGENERATOR_SINEGENERATOR_H_
#define YETIOS_CORE_UTILS_SINEGENERATOR_SINEGENERATOR_H_

#include "circ_buf.h"
#include "stm32l4xx_hal_dac.h"

void sineInit(void);
void sineGenerate(void);
void sineNext(void);
void sineReset(void);


#endif /* YETIOS_CORE_UTILS_SINEGENERATOR_SINEGENERATOR_H_ */
