/*
 * circ_buf.h
 *
 *  Modified on: 27 jul. 2018
 *      Author: Phillip Johnston
 *      Web: https://embeddedartistry.com/blog/2017/4/6/circular-buffers-in-cc
 *      Modifier: miguelvp
 */

#ifndef YETIOS_CORE_UTILS_INCLUDE_CIRC_BUF_H_
#define YETIOS_CORE_UTILS_INCLUDE_CIRC_BUF_H_

#include "platform-conf.h"

typedef struct circular_buf {
    uint16_t *buffer;
    uint16_t head;
    uint16_t tail;
    uint16_t size; //of the buffer
    uint16_t full;
} circular_buf_t;

int circular_buf_init(circular_buf_t *cbuf, uint16_t size);
int circular_buf_reset(circular_buf_t *cbuf);
int circular_buf_put(circular_buf_t *cbuf, uint16_t data);
int circular_buf_get(circular_buf_t *cbuf, uint16_t *data);
int circular_buf_empty(circular_buf_t cbuf);
int circular_buf_full(circular_buf_t cbuf);

#endif /* YETIOS_CORE_UTILS_INCLUDE_CIRC_BUF_H_ */
