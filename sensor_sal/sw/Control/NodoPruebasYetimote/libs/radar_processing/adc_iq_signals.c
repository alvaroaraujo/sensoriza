/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * adc_iq_signals.c
 *
 *  Created on: 18/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file adc_iq_signals.c
 */

#include "adc_iq_signals.h"
#include "system_api.h"

#if USE_RADAR_PROCESSING

adc_iq_signal_t* new_adc_iq_signal(uint16_t adc_sample_number, uint16_t guard_samples){

	adc_iq_signal_t* new_adc_iq = (adc_iq_signal_t*) ytMalloc(sizeof(adc_iq_signal_t));
	int16_t* tmp_ptr = (int16_t*) ytMalloc(((adc_sample_number + guard_samples)*2)*sizeof(uint16_t));

	new_adc_iq->ordered_iq_samples = (float32_t*) ytMalloc((adc_sample_number*2)*sizeof(float32_t));

	new_adc_iq->adc_sample_number = adc_sample_number;
	new_adc_iq->guard_samples = guard_samples;
	new_adc_iq->i_signal = tmp_ptr;
	new_adc_iq->q_signal = tmp_ptr+new_adc_iq->adc_sample_number+new_adc_iq->guard_samples;

	return new_adc_iq;
}


retval_t remove_adc_iq_signal(adc_iq_signal_t* adc_iq_signal){
	ytFree(adc_iq_signal->i_signal);
	ytFree(adc_iq_signal->ordered_iq_samples);
	ytFree(adc_iq_signal);
	return RET_OK;
}


//retval_t iq_signal_to_float(adc_iq_signal_t* adc_iq_signal){
//
//	uint16_t i;
//
//	for(i=0; i<adc_iq_signal->adc_sample_number*2;i++){
//	  adc_iq_signal->ordered_iq_samples[i] = (float32_t) adc_iq_signal->ordered_iq_samples[i]*0.00005f;
//
//	}
//	return RET_OK;
//}
//retval_t reorder_iq_signal(adc_iq_signal_t* adc_iq_signal){
//
//	uint16_t i;
//
//	for(i=0; i<adc_iq_signal->adc_sample_number*2;i++){
//	  if((i%2) == 0){
//		  adc_iq_signal->ordered_iq_samples[i] = (float32_t) adc_iq_signal->i_signal[adc_iq_signal->guard_samples+ i/2];			//Descarto las guard_samples primeras muestras
//	  }
//	  else{
//
//		  adc_iq_signal->ordered_iq_samples[i]= (float32_t) adc_iq_signal->i_signal[adc_iq_signal->adc_sample_number+(2*adc_iq_signal->guard_samples)+((i-1)/2)];		//Descarto las guard_samples primeras muestras
//	  }
//	}
//
//	return RET_OK;
//}

retval_t change_adc_sample_number(adc_iq_signal_t* adc_iq_signal, uint16_t adc_sample_number){

	if(adc_iq_signal != NULL){
		ytFree(adc_iq_signal->i_signal);
		ytFree(adc_iq_signal->ordered_iq_samples);

		adc_iq_signal->adc_sample_number = adc_sample_number;

		int16_t* tmp_ptr = (int16_t*) ytMalloc(((adc_sample_number + adc_iq_signal->guard_samples)*2)*sizeof(uint16_t));

		adc_iq_signal->ordered_iq_samples = (float32_t*) ytMalloc((adc_sample_number*2)*sizeof(float32_t));

		adc_iq_signal->i_signal = tmp_ptr;
		adc_iq_signal->q_signal = tmp_ptr+adc_iq_signal->adc_sample_number+adc_iq_signal->guard_samples;

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t change_adc_guard_sample(adc_iq_signal_t* adc_iq_signal, uint16_t adc_guard_samples){


	if(adc_iq_signal != NULL){
		ytFree(adc_iq_signal->i_signal);

		adc_iq_signal->guard_samples = adc_guard_samples;

		int16_t* tmp_ptr = (int16_t*) ytMalloc(((adc_iq_signal->adc_sample_number + adc_iq_signal->guard_samples)*2)*sizeof(uint16_t));

		adc_iq_signal->i_signal = tmp_ptr;
		adc_iq_signal->q_signal = tmp_ptr+adc_iq_signal->adc_sample_number+adc_iq_signal->guard_samples;

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}
#endif
