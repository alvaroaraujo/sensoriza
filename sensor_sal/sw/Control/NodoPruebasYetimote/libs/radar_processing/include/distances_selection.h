/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * distances_selection.h
 *
 *  Created on: 31/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file distances_selection.h
 */

#ifndef APPLICATION_USER_S4BIM_DSP_CODE_PREPROCESSING_DISTANCES_SELECTION_H_
#define APPLICATION_USER_S4BIM_DSP_CODE_PREPROCESSING_DISTANCES_SELECTION_H_

#include "snr.h"
#include "fft_signal.h"
#include "os_cfar.h"
//#include "modulation_scheme.h"
#include "peaks_selection.h"
#include "speeds_selection.h"

#define DEFAULT_DISTANCES_CALIBRATION_FACTOR	1

#define DEFAULT_NEAR_DISTANCES_SAMPLES		-15			//~5m
#define DEFAULT_MID_DISTANCES_SAMPLES		-32			//~10m
#define DEFAULT_FAR_DISTANCES_SAMPLES		-60			//~20m
#define DEFAULT_DIST_SIGNAL_THR				4
#define DEFAULT_DIST_SNR_THR				6.5f

#define DISCARD_SPEED						1
#define	NO_DISCARD_SPEED					0

#define DOWNRAMP_DISTANCE_OFFSET			2


typedef struct distances_selection{
	float32_t* distances;
	float32_t* distances_levels;
	float32_t* distances_snr;
	uint16_t max_num_distances;
	uint16_t current_num_distances;
	float32_t calibration_factor;
	float32_t downramp_calibration_factor;
	int16_t num_guard_low_distances;
	float32_t threshold_lvl_calibration_near;
	float32_t threshold_lvl_calibration_mid;
	float32_t threshold_lvl_calibration_far;
	float32_t threshold_lvl_calibration_vfar;
	float32_t threshold_snr_calibration_near;
	float32_t threshold_snr_calibration_mid;
	float32_t threshold_snr_calibration_far;
	float32_t threshold_snr_calibration_vfar;
	uint16_t discard_speed;
}distances_selection_t;

distances_selection_t* new_distances_selection(uint16_t max_num_distances, uint16_t num_guard_low_distances);
retval_t remove_distances_selection(distances_selection_t* distances_selection);

retval_t set_max_num_distances(distances_selection_t* distances_selection, uint16_t max_num_distances);

//retval_t do_distances_calculation(distances_selection_t* distances_selection, speeds_selection_t* speeds_selection, peaks_selection_t* peaks_selection, modulation_scheme_t* mod_schemes, fft_signal_t* fft_signal);

#endif /* APPLICATION_USER_S4BIM_DSP_CODE_PREPROCESSING_DISTANCES_SELECTION_H_ */
