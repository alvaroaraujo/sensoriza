/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * peaks_selection.h
 *
 *  Created on: 30/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file peaks_selection.h
 */

#ifndef APPLICATION_USER_S4BIM_DSP_CODE_PREPROCESSING_PEAKS_SELECTION_H_
#define APPLICATION_USER_S4BIM_DSP_CODE_PREPROCESSING_PEAKS_SELECTION_H_

#include "fft_signal.h"
#include "os_cfar.h"
#include "snr.h"


typedef struct peaks_selection{
	int16_t* peak_positions;
	float32_t* peak_levels;
	float32_t* snr_levels;
	uint16_t current_num_peaks;
	uint16_t max_num_peaks;
}peaks_selection_t;


peaks_selection_t* new_peaks_selection(uint16_t max_num_peaks);
retval_t remove_peaks_selection(peaks_selection_t* peaks_selection);

retval_t set_max_num_peaks(peaks_selection_t* peaks_selection, uint16_t new_max_num_peaks);

retval_t do_peaks_selection(peaks_selection_t* peaks_selection, fft_signal_t* fft_signal, snr_t* snr, os_cfar_t* os_cfar);

#endif /* APPLICATION_USER_S4BIM_DSP_CODE_PREPROCESSING_PEAKS_SELECTION_H_ */
