/*
 * radar_proccessing.h
 *
 *  Created on: 9 de jul. de 2017
 *      Author: Yo
 */

#ifndef APPLICATION_USER_BSP_YETIMOTE_RADAR_RADAR_PROCCESSING_H_
#define APPLICATION_USER_BSP_YETIMOTE_RADAR_RADAR_PROCCESSING_H_

#include "platform-conf.h"
#include "preprocessing.h"

retval_t radar_processData(preprocessing_block_t* preprocessing_block);
retval_t prepare_signal(adc_iq_signal_t* adc_iq_signal, uint32_t* read_data, uint16_t num_samples);

#endif /* APPLICATION_USER_BSP_YETIMOTE_RADAR_RADAR_PROCCESSING_H_ */
