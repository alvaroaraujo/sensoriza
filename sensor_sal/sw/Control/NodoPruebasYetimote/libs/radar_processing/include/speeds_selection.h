/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * speeds_selection.h
 *
 *  Created on: 31/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file speeds_selection.h
 */

#ifndef APPLICATION_USER_S4BIM_DSP_CODE_PREPROCESSING_SPEEDS_SELECTION_H_
#define APPLICATION_USER_S4BIM_DSP_CODE_PREPROCESSING_SPEEDS_SELECTION_H_

#include "snr.h"
#include "fft_signal.h"
#include "os_cfar.h"
#include "peaks_selection.h"

#define	DEFAULT_SPEED_CALIBRATION_FACTOR	0.4

#define DEFAULT_NEAR_SPEEDS_SAMPLES			-30			//~5m
#define DEFAULT_MID_SPEEDS_SAMPLES			-65			//~10m
#define DEFAULT_FAR_SPEEDS_SAMPLES			-120		//~20m
#define DEFAULT_SPEED_SIGNAL_THR					1.4f
#define DEFAULT_SPEED_SNR_THR						2.4f

typedef struct speeds_selection{
	uint16_t* speeds_positions;
	float32_t* speeds;
	float32_t* speeds_levels;
	uint16_t max_num_speeds;
	uint16_t current_num_speeds;
	float32_t calibration_factor;
	int16_t num_guard_low_speeds;
	float32_t threshold_lvl_calibration_near;
	float32_t threshold_lvl_calibration_mid;
	float32_t threshold_lvl_calibration_far;
	float32_t threshold_lvl_calibration_vfar;
	float32_t threshold_snr_calibration_near;
	float32_t threshold_snr_calibration_mid;
	float32_t threshold_snr_calibration_far;
	float32_t threshold_snr_calibration_vfar;
}speeds_selection_t;

speeds_selection_t* new_speeds_selection(uint16_t max_num_speeds, uint16_t num_guard_low_speeds);
retval_t remove_speeds_selection(speeds_selection_t* speeds_selection);

retval_t set_max_num_speeds(speeds_selection_t* speeds_selection, uint16_t max_num_speeds);

retval_t do_speeds_calculation(speeds_selection_t* speeds_selection, peaks_selection_t* peaks_selection, fft_signal_t* fft_signal);


#endif /* APPLICATION_USER_S4BIM_DSP_CODE_PREPROCESSING_SPEEDS_SELECTION_H_ */
