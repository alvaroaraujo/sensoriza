/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * adc_iq_signals.h
 *
 *  Created on: 18/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file adc_iq_signals.h
 */

#ifndef APPLICATION_USER_S4BIM_ADC_CODE_ADC_IQ_SIGNALS_H_
#define APPLICATION_USER_S4BIM_ADC_CODE_ADC_IQ_SIGNALS_H_

#include "dma.h"
#include "tim.h"
#include "spi.h"
#include "types.h"
#include "arm_math.h"

typedef struct adc_iq_signal{
	uint16_t* i_signal;
	uint16_t* q_signal;
	uint16_t adc_sample_number;
	uint16_t guard_samples;
	float32_t* ordered_iq_samples;
}adc_iq_signal_t;

adc_iq_signal_t* new_adc_iq_signal(uint16_t adc_sample_number, uint16_t guard_samples);
retval_t remove_adc_iq_signal(adc_iq_signal_t* adc_iq_signal);

retval_t iq_signal_to_float(adc_iq_signal_t* adc_iq_signal);
retval_t reorder_iq_signal(adc_iq_signal_t* adc_iq_signal);

retval_t change_adc_sample_number(adc_iq_signal_t* adc_iq_signal, uint16_t adc_sample_number);
retval_t change_adc_guard_sample(adc_iq_signal_t* adc_iq_signal, uint16_t adc_guard_samples);

#endif /* APPLICATION_USER_S4BIM_ADC_CODE_ADC_IQ_SIGNALS_H_ */
