/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * os_cfar.h
 *
 *  Created on: 30/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file os_cfar.h
 */

#ifndef APPLICATION_USER_S4BIM_DSP_CODE_PREPROCESSING_OS_CFAR_H_
#define APPLICATION_USER_S4BIM_DSP_CODE_PREPROCESSING_OS_CFAR_H_

#include "fft_signal.h"
#include <stdlib.h>

#define DEFAULT_WINDOW_SIZE		10		//Orig 8
#define DEFAULT_K_ORDER			9		//Orig 7
#define DEFAULT_OSCFAR_ALPHA	0.96f	//Orig 0.96f

typedef enum{
	Standard_os_cfar,
	No_os_cfar,
}os_cfar_type_t;

typedef struct os_cfar{
	retval_t (*os_cfar_func_ptr)(struct os_cfar* os_cfar, fft_signal_t* fft_signal);
	float32_t* signal_threshold_output;
	uint16_t effective_sample_number;
	uint16_t window_size;
	uint16_t k_order;
	float32_t alpha;
	os_cfar_type_t os_cfar_type;
}os_cfar_t;


os_cfar_t* new_os_cfar(uint16_t fft_sample_number, uint16_t effective_sample_number, os_cfar_type_t new_os_cfar_type);
retval_t remove_os_cfar(os_cfar_t* os_cfar);

retval_t change_os_cfar_type(os_cfar_t* os_cfar, os_cfar_type_t new_os_cfar_type);

retval_t change_cfar_effective_sample_number(os_cfar_t* os_cfar, uint16_t effective_sample_number);

#endif /* APPLICATION_USER_S4BIM_DSP_CODE_PREPROCESSING_OS_CFAR_H_ */
