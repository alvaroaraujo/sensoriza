/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * preprocessing.h
 *
 *  Created on: 27/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file preprocessing.h
 */

#ifndef APPLICATION_USER_S4BIM_DSP_CODE_PREPROCESSING_PREPROCESSING_H_
#define APPLICATION_USER_S4BIM_DSP_CODE_PREPROCESSING_PREPROCESSING_H_

//#include "DSP_conf.h"
#include "adc_iq_signals.h"
#include "signal_window.h"
#include "interpolation.h"
#include "fft_signal.h"
#include "freq_signal_transform.h"
#include "snr.h"
//#include "modulation_scheme.h"
#include "peaks_selection.h"
#include "distances_selection.h"
#include "speeds_selection.h"


#define SPEED_MODE				1
#define DISTANCE_MODE			0

#define DEFAULT_ADC_SAMPLES 		128
#define	DEFAULT_ADC_GUARD_SAMPLES	0
#define DEFAULT_WINDOW_TYPE			Hanning
#define DEFAULT_INTERPOLATION_TYPE	Zero_Padding
#define DEFAULT_FFT_TYPE			Standard_fft
#define DEFAULT_OSCFAR_TYPE			Standard_os_cfar
#define DEFAULT_SNR_TYPE			Standard_snr
#define DEFAULT_FREQ_TRANSFORM		Noise_reduction

#define DEFAULT_MAX_NUM_PEAKS		48

#define DEFAULT_MAX_NUM_DISTANCES		12
#define	DEFAULT_DISTANCES_GUARD_SAMPLES	5

#define DEFAULT_MAX_NUM_SPEEDS			12
#define	DEFAULT_SPEEDS_GUARD_SAMPLES	0

#define DEFAULT_VMIN				0			//!<Default min voltage of the ramp
#define	DEFAULT_VMAX				10			//!<Default max voltage of the ramp
#define	DEFAULT_VAVG				5			//!<Default avg constant voltage
#define	DEFAULT_RAMP_TIME			7000		//!<Default ramp time in us
#define	DEFAULT_RAMP				UPRAMP		//!<Default ramp kind

#define DEFAULT_SPEED_CALC_RATE		10

#define DEFAULT_UPDOWN_CALC_RATE	10

typedef struct preprocessing_block_{
	adc_iq_signal_t* adc_iq_signal;
	signal_window_t* window;
	interpolation_t* interpolation;
	fft_signal_t* fft_block;
	os_cfar_t* os_cfar;
	snr_t* snr;
	freq_signal_transform_t* freq_signal_transform;
	peaks_selection_t* peaks_selection;
//	modulation_scheme_t* modulation_schemes;
	speeds_selection_t* speed_selection;
	distances_selection_t* distances_selection;
	uint16_t adc_sample_number;
	uint16_t fft_sample_number;
	uint16_t freq_valid_samples;
	uint32_t delay;
	uint16_t speed_calc_rate;
	uint16_t updown_calc_rate;
}preprocessing_block_t;

preprocessing_block_t* new_preprocessing_block(uint16_t adc_sample_number, uint16_t fft_sample_number, uint16_t freq_valid_samples, uint32_t delay);
retval_t remove_preprocessing_block(preprocessing_block_t* preprocessing_block);

retval_t processData(preprocessing_block_t* preprocessing_block);

retval_t prep_change_adc_sample_number(preprocessing_block_t* preprocessing_block, uint16_t adc_sample_number);
retval_t change_freq_sample_number(preprocessing_block_t* preprocessing_block, uint16_t fft_sample_number);
retval_t change_freq_valid_samples(preprocessing_block_t* preprocessing_block, uint16_t freq_valid_samples);
retval_t change_delay(preprocessing_block_t* preprocessing_block, uint32_t delay);
retval_t change_speed_calc_rate(preprocessing_block_t* preprocessing_block, uint16_t speed_calc_rate);

#endif /* APPLICATION_USER_S4BIM_DSP_CODE_PREPROCESSING_PREPROCESSING_H_ */
