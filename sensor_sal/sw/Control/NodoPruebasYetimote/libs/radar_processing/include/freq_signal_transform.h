/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * freq_signal_transform.h
 *
 *  Created on: 30/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file freq_signal_transform.h
 */

#ifndef APPLICATION_USER_S4BIM_DSP_CODE_PREPROCESSING_FREQ_SIGNAL_TRANSFORM_H_
#define APPLICATION_USER_S4BIM_DSP_CODE_PREPROCESSING_FREQ_SIGNAL_TRANSFORM_H_

#include "fft_signal.h"

typedef enum{
	No_transform,
	Noise_reduction,
}freq_transform_type_t;

typedef struct freq_signal_transform{
	retval_t (*freq_transform_func_ptr)(struct freq_signal_transform* freq_signal_transform, fft_signal_t* fft_signal);
	freq_transform_type_t freq_transform_type;
}freq_signal_transform_t;


freq_signal_transform_t* new_freq_signal_transform(freq_transform_type_t freq_transform_type);
retval_t remove_freq_signal_transform(freq_signal_transform_t* freq_signal_transform);

retval_t change_freq_signal_transform_type(freq_signal_transform_t* freq_signal_transform, freq_transform_type_t new_freq_transform_type);

#endif /* APPLICATION_USER_S4BIM_DSP_CODE_PREPROCESSING_FREQ_SIGNAL_TRANSFORM_H_ */
