/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * fft_signal.c
 *
 *  Created on: 30/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file fft_signal.c
 */


#include "fft_signal.h"
#include "system_api.h"

#if USE_RADAR_PROCESSING

retval_t no_fft(struct fft_signal* fft_signal, interpolation_t* interpolation);
retval_t do_fft(struct fft_signal* fft_signal, interpolation_t* interpolation);

fft_signal_t* new_fft_signal(uint16_t fft_sample_number, uint16_t effective_sample_number, fft_type_t fft_type){
	fft_signal_t* new_fft_signal = (fft_signal_t*) ytMalloc(sizeof(fft_signal_t));
	new_fft_signal->fft_sample_number = fft_sample_number;
	new_fft_signal->effective_sample_number = effective_sample_number;

	new_fft_signal->fft_input_output_signal = (float32_t*) ytMalloc(new_fft_signal->fft_sample_number*2*sizeof(float32_t));
	new_fft_signal->fft_mag_output_ordered_signal = (float32_t*) ytMalloc(new_fft_signal->effective_sample_number*sizeof(float32_t));
	new_fft_signal->fft_mag_output_signal = (float32_t*) ytMalloc(new_fft_signal->fft_sample_number*sizeof(float32_t));
	new_fft_signal->ifft_flag = DEFAULT_IFFT_FLAG;

	new_fft_signal->fft_type = fft_type;

	switch(fft_type){
	case Standard_fft:
		new_fft_signal->fft_func_ptr = do_fft;
		break;
	case No_fft:
		new_fft_signal->fft_func_ptr = no_fft;
		break;
	default:
		new_fft_signal->fft_func_ptr = no_fft;
		break;
	}

	return new_fft_signal;
}


retval_t remove_fft_signal(fft_signal_t* fft_signal){
	if(fft_signal != NULL){
		ytFree(fft_signal->fft_mag_output_signal);
		ytFree(fft_signal->fft_input_output_signal);
		ytFree(fft_signal->fft_mag_output_ordered_signal);
		ytFree(fft_signal);
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t change_fft_type(fft_signal_t* fft_signal, fft_type_t new_fft_type){
	if(fft_signal != NULL){
		fft_signal->fft_type = new_fft_type;

		switch(new_fft_type){
		case Standard_fft:
			fft_signal->fft_func_ptr = do_fft;
			break;
		case No_fft:
			fft_signal->fft_func_ptr = no_fft;
			break;
		default:
			fft_signal->fft_func_ptr = no_fft;
			break;
		}
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t no_fft(struct fft_signal* fft_signal, interpolation_t* interpolation){
	if((interpolation != NULL) && (fft_signal != NULL)){
		  uint16_t i = 0;

		  for(i=0; i<fft_signal->fft_sample_number; i++){
			  fft_signal->fft_input_output_signal[i*2] = interpolation->signal_post_interpolation[i*2];			//Real and Imaginary Parts.
			  fft_signal->fft_input_output_signal[(i*2)+1] = interpolation->signal_post_interpolation[(i*2)+1];

			  if(i<fft_signal->effective_sample_number){
				  fft_signal->fft_mag_output_signal[i] = 0;
			  }
		  }

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}
retval_t do_fft(struct fft_signal* fft_signal, interpolation_t* interpolation){
	if((interpolation != NULL) && (fft_signal != NULL)){


		if(fft_signal->fft_sample_number != interpolation->output_sample_number){
			return RET_ERROR;
		}
		uint16_t i = 0;

		for(i=0; i<fft_signal->fft_sample_number; i++){
		  fft_signal->fft_input_output_signal[i*2] = interpolation->signal_post_interpolation[i*2];			//Real and Imaginary Parts.
		  fft_signal->fft_input_output_signal[(i*2)+1] = interpolation->signal_post_interpolation[(i*2)+1];
		}


		switch(fft_signal->fft_sample_number){
		case 16:
				arm_cfft_f32(&arm_cfft_sR_f32_len16, fft_signal->fft_input_output_signal, fft_signal->ifft_flag, DEFAULT_DOBITREVERSE);
			break;
		case 32:
				arm_cfft_f32(&arm_cfft_sR_f32_len32, fft_signal->fft_input_output_signal, fft_signal->ifft_flag, DEFAULT_DOBITREVERSE);
			break;
		case 64:
				arm_cfft_f32(&arm_cfft_sR_f32_len64, fft_signal->fft_input_output_signal, fft_signal->ifft_flag, DEFAULT_DOBITREVERSE);
			break;
		case 128:
				arm_cfft_f32(&arm_cfft_sR_f32_len128, fft_signal->fft_input_output_signal, fft_signal->ifft_flag, DEFAULT_DOBITREVERSE);
			break;
		  case 256:
				arm_cfft_f32(&arm_cfft_sR_f32_len256, fft_signal->fft_input_output_signal, fft_signal->ifft_flag, DEFAULT_DOBITREVERSE);
			break;
		case 512:
				arm_cfft_f32(&arm_cfft_sR_f32_len512, fft_signal->fft_input_output_signal, fft_signal->ifft_flag, DEFAULT_DOBITREVERSE);
			break;
		case 1024:
				arm_cfft_f32(&arm_cfft_sR_f32_len1024, fft_signal->fft_input_output_signal, fft_signal->ifft_flag, DEFAULT_DOBITREVERSE);
			break;
		case 2048:
				arm_cfft_f32(&arm_cfft_sR_f32_len2048, fft_signal->fft_input_output_signal, fft_signal->ifft_flag, DEFAULT_DOBITREVERSE);
			break;
		case 4096:
				arm_cfft_f32(&arm_cfft_sR_f32_len4096, fft_signal->fft_input_output_signal, fft_signal->ifft_flag, DEFAULT_DOBITREVERSE);
			break;
		default:
			return RET_ERROR;
			break;
		}

		/* Process the data through the Complex Magnitude Module for
		calculating the magnitude at each bin */

		arm_cmplx_mag_f32(fft_signal->fft_input_output_signal, fft_signal->fft_mag_output_signal, fft_signal->fft_sample_number);

		//Reorder the output signal
	  	for(i=0; i<fft_signal->effective_sample_number/2; i++){
	  		fft_signal->fft_mag_output_ordered_signal[i] = fft_signal->fft_mag_output_signal[fft_signal->fft_sample_number - (fft_signal->effective_sample_number/2) + i];
	  		fft_signal->fft_mag_output_ordered_signal[(fft_signal->effective_sample_number/2) + i] = fft_signal->fft_mag_output_signal[i];
	  	}

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}

}


retval_t change_fft_sample_number(fft_signal_t* fft_signal, uint16_t fft_samples){
	if(fft_signal != NULL){
		ytFree(fft_signal->fft_mag_output_signal);
		ytFree(fft_signal->fft_input_output_signal);

		fft_signal->fft_sample_number = fft_samples;

		fft_signal->fft_input_output_signal = (float32_t*) ytMalloc(fft_signal->fft_sample_number*2*sizeof(float32_t));
		fft_signal->fft_mag_output_signal = (float32_t*) ytMalloc(fft_signal->fft_sample_number*sizeof(float32_t));
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_fft_effective_sample_number(fft_signal_t* fft_signal, uint16_t fft_effective_samples){

	if(fft_signal != NULL){
		ytFree(fft_signal->fft_mag_output_ordered_signal);

		fft_signal->effective_sample_number = fft_effective_samples;

		fft_signal->fft_mag_output_ordered_signal = (float32_t*) ytMalloc(fft_signal->effective_sample_number*sizeof(float32_t));
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}

}

#endif
