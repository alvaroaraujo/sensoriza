/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * interpolation.c
 *
 *  Created on: 30/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file interpolation.c
 */

#include "interpolation.h"
#include "system_api.h"


#if USE_RADAR_PROCESSING

retval_t no_interpolation(struct interpolation* interpolation, signal_window_t* window);
retval_t zero_padding(struct interpolation* interpolation, signal_window_t* window);

interpolation_t* new_interpolation(uint16_t input_sample_number, uint16_t output_sample_number, interpolation_type_t interpolation_type){

	interpolation_t* new_interpolation = (interpolation_t*) ytMalloc(sizeof(interpolation_t));

	new_interpolation->input_sample_number = input_sample_number;
	new_interpolation->output_sample_number = output_sample_number;
	new_interpolation->signal_post_interpolation = (float32_t*) ytMalloc(new_interpolation->output_sample_number*2*sizeof(float32_t));
	new_interpolation->interpolation_type = interpolation_type;


	switch(interpolation_type){
	case Zero_Padding:
		new_interpolation->interpolation_func_ptr = zero_padding;
		break;
	case No_Interpolation:
		new_interpolation->interpolation_func_ptr = no_interpolation;
		break;
	default:
		new_interpolation->interpolation_func_ptr = no_interpolation;
		break;
	}

	return new_interpolation;
}
retval_t remove_interpolation(interpolation_t* interpolation){
	if(interpolation != NULL){
		ytFree(interpolation->signal_post_interpolation);
		ytFree(interpolation);
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t change_interpolation_type(interpolation_t* interpolation, interpolation_type_t new_interpolation_type, uint16_t new_output_sample_number){
	if(interpolation != NULL){
		interpolation->output_sample_number = new_output_sample_number;
		interpolation->interpolation_type = new_interpolation_type;

		switch(new_interpolation_type){
		case Zero_Padding:
			interpolation->interpolation_func_ptr = zero_padding;
			break;
		case No_Interpolation:
			interpolation->interpolation_func_ptr = no_interpolation;
			break;
		default:
			interpolation->interpolation_func_ptr = no_interpolation;
			break;
		}
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t no_interpolation(struct interpolation* interpolation, signal_window_t* window){

	interpolation->input_sample_number = window->sample_number;
	interpolation->output_sample_number = window->sample_number;

	if((interpolation != NULL) && (window != NULL)){
		uint16_t i = 0;

		  for(i=0; i<interpolation->input_sample_number; i++){
			  interpolation->signal_post_interpolation[i*2] = window->signal_post_window[i*2];			//Real and Imaginary Parts.
			  interpolation->signal_post_interpolation[(i*2)+1] = window->signal_post_window[(i*2)+1];
		  }

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}
retval_t zero_padding(struct interpolation* interpolation, signal_window_t* window){
	if((interpolation != NULL) && (window != NULL)){
		  uint16_t i = 0;

		  for(i=0; i<interpolation->input_sample_number; i++){
			  interpolation->signal_post_interpolation[i*2] = window->signal_post_window[i*2];			//Real and Imaginary Parts.
			  interpolation->signal_post_interpolation[(i*2)+1] = window->signal_post_window[(i*2)+1];
		  }
		  for(i=interpolation->input_sample_number; i<interpolation->output_sample_number; i++){
			  interpolation->signal_post_interpolation[i*2] = 0;										//Real and Imaginary Parts.
			  interpolation->signal_post_interpolation[(i*2)+1] = 0;
		  }
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}


retval_t change_interpolation_intput_sample_number(interpolation_t* interpolation, uint16_t adc_sample_number){
	if(interpolation != NULL){

		interpolation->input_sample_number = adc_sample_number;

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_interpolation_output_sample_number(interpolation_t* interpolation, uint16_t fft_sample_number){

	if(interpolation != NULL){

		if(fft_sample_number >= interpolation->input_sample_number){
			ytFree(interpolation->signal_post_interpolation);

			interpolation->output_sample_number = fft_sample_number;

			interpolation->signal_post_interpolation = (float32_t*) ytMalloc(interpolation->output_sample_number*2*sizeof(float32_t));


		}
		else{
			ytFree(interpolation->signal_post_interpolation);

			interpolation->output_sample_number = interpolation->input_sample_number;

			interpolation->signal_post_interpolation = (float32_t*) ytMalloc(interpolation->output_sample_number*2*sizeof(float32_t));
		}

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}
#endif
