/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * peaks_selection.c
 *
 *  Created on: 30/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file peaks_selection.c
 */

#include "peaks_selection.h"
#include "system_api.h"

#if USE_RADAR_PROCESSING

peaks_selection_t* new_peaks_selection(uint16_t max_num_peaks){
	peaks_selection_t* new_peaks_selection = (peaks_selection_t*) ytMalloc(sizeof(peaks_selection_t));

	new_peaks_selection->peak_positions = (int16_t*) ytMalloc(max_num_peaks*sizeof(int16_t));
	new_peaks_selection->peak_levels = (float32_t*) ytMalloc(max_num_peaks*sizeof(float32_t));
	new_peaks_selection->snr_levels = (float32_t*) ytMalloc(max_num_peaks*sizeof(float32_t));

	new_peaks_selection->current_num_peaks = 0;
	new_peaks_selection->max_num_peaks = max_num_peaks;

	return new_peaks_selection;
}
retval_t remove_peaks_selection(peaks_selection_t* peaks_selection){
	if(peaks_selection != NULL){
		ytFree(peaks_selection->peak_positions);
		ytFree(peaks_selection->peak_levels);
		ytFree(peaks_selection->snr_levels);
		ytFree(peaks_selection);
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t set_max_num_peaks(peaks_selection_t* peaks_selection, uint16_t new_max_num_peaks){
	if(peaks_selection != NULL){
		ytFree(peaks_selection->peak_positions);
		ytFree(peaks_selection->peak_levels);
		ytFree(peaks_selection->snr_levels);

		peaks_selection->max_num_peaks = new_max_num_peaks;

		peaks_selection->peak_positions = (int16_t*) ytMalloc(new_max_num_peaks*sizeof(int16_t));
		peaks_selection->peak_levels = (float32_t*) ytMalloc(new_max_num_peaks*sizeof(float32_t));
		peaks_selection->snr_levels = (float32_t*) ytMalloc(new_max_num_peaks*sizeof(float32_t));

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t do_peaks_selection(peaks_selection_t* peaks_selection, fft_signal_t* fft_signal, snr_t* snr, os_cfar_t* os_cfar){


	if((peaks_selection != NULL) && (fft_signal != NULL) && (snr != NULL) && (os_cfar != NULL)){
		int16_t i = 0;
		uint32_t sizeCount = 0;

		if(snr->noise_lvl > 0.04f){
			peaks_selection->current_num_peaks = 0;	//Signal too noisy
			return RET_OK;
		}
		float32_t* tempDist = (float32_t*) ytMalloc(fft_signal->effective_sample_number*sizeof(float32_t));

		uint16_t signalSize = fft_signal->effective_sample_number;
		float32_t* threesholdInput = os_cfar->signal_threshold_output;
		float32_t* signalInput = fft_signal->fft_mag_output_ordered_signal;


		for(i=0; i<signalSize; i++){

			if((signalInput[i] > threesholdInput[i])){
				tempDist[i] = signalInput[i];
			}
			else{
				tempDist[i] = 0;
			}



		}


		for(i=0; i<signalSize-2; i++){

			if (tempDist[i] != 0){
				if(tempDist[i+1] != 0){
					if(tempDist[i+1]>tempDist[i]){
						peaks_selection->peak_levels[sizeCount] = signalInput[i+1];
						peaks_selection->peak_positions[sizeCount] = (i+1);
						peaks_selection->snr_levels[sizeCount] = snr->snr_signal[i+1];
						sizeCount++;
						if(tempDist[i+2] != 0){
							i++;
							if(tempDist[i+2] != 0){
								i++;
							}
						}
					}
					else{
						peaks_selection->peak_levels[sizeCount] = signalInput[i];
						peaks_selection->peak_positions[sizeCount] = i;
						peaks_selection->snr_levels[sizeCount] = snr->snr_signal[i];
						sizeCount++;
						if(tempDist[i+2] != 0){
							i++;
							if(tempDist[i+2] != 0){
								i++;
							}
						}
					}
					i++;
				}
				else{
					peaks_selection->peak_levels[sizeCount] = signalInput[i];
					peaks_selection->peak_positions[sizeCount] = i;
					peaks_selection->snr_levels[sizeCount] = snr->snr_signal[i];
					sizeCount++;
				}
			}

			if(sizeCount>=peaks_selection->max_num_peaks){
				break;
			}

		}

		peaks_selection->current_num_peaks = sizeCount;

		ytFree(tempDist);
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}
#endif
