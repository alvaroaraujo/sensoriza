/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * preprocessing.c
 *
 *  Created on: 27/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file preprocessing.c
 */

#include "preprocessing.h"
#include "system_api.h"

#if USE_RADAR_PROCESSING
preprocessing_block_t* new_preprocessing_block(uint16_t adc_sample_number, uint16_t fft_sample_number, uint16_t freq_valid_samples, uint32_t delay){
	preprocessing_block_t* new_preprocessing_block = (preprocessing_block_t*) ytMalloc(sizeof(preprocessing_block_t));

	new_preprocessing_block->adc_sample_number= adc_sample_number;
	new_preprocessing_block->fft_sample_number= fft_sample_number;
	new_preprocessing_block->freq_valid_samples= freq_valid_samples;
	new_preprocessing_block->delay = delay;
	new_preprocessing_block->speed_calc_rate = DEFAULT_SPEED_CALC_RATE;
	new_preprocessing_block->updown_calc_rate = DEFAULT_UPDOWN_CALC_RATE;

	new_preprocessing_block->adc_iq_signal = new_adc_iq_signal(adc_sample_number, DEFAULT_ADC_GUARD_SAMPLES);
	new_preprocessing_block->window = new_signal_window(adc_sample_number, DEFAULT_WINDOW_TYPE);
	new_preprocessing_block->interpolation = new_interpolation(adc_sample_number, fft_sample_number, DEFAULT_INTERPOLATION_TYPE);
	new_preprocessing_block->fft_block = new_fft_signal(fft_sample_number, freq_valid_samples, DEFAULT_FFT_TYPE);
	new_preprocessing_block->os_cfar = new_os_cfar(fft_sample_number, freq_valid_samples, DEFAULT_OSCFAR_TYPE);
	new_preprocessing_block->snr = new_snr(fft_sample_number, freq_valid_samples, DEFAULT_SNR_TYPE);
	new_preprocessing_block->freq_signal_transform = new_freq_signal_transform(DEFAULT_FREQ_TRANSFORM);
	new_preprocessing_block->peaks_selection = new_peaks_selection(DEFAULT_MAX_NUM_PEAKS);
//	new_preprocessing_block->modulation_schemes = mod_schemes_init();
	new_preprocessing_block->speed_selection = new_speeds_selection(DEFAULT_MAX_NUM_SPEEDS, DEFAULT_SPEEDS_GUARD_SAMPLES);
	new_preprocessing_block->distances_selection = new_distances_selection(DEFAULT_MAX_NUM_DISTANCES, DEFAULT_DISTANCES_GUARD_SAMPLES);



//	add_ramp_scheme(new_preprocessing_block->modulation_schemes, new_preprocessing_block->adc_iq_signal, DEFAULT_VMAX, DEFAULT_VMIN, DEFAULT_RAMP_TIME, adc_sample_number, UPRAMP);
//	add_ramp_scheme(new_preprocessing_block->modulation_schemes, new_preprocessing_block->adc_iq_signal, DEFAULT_VMAX, DEFAULT_VMIN, DEFAULT_RAMP_TIME, adc_sample_number, DOWNRAMP);
//	add_const_scheme(new_preprocessing_block->modulation_schemes, new_preprocessing_block->adc_iq_signal, DEFAULT_VAVG, DEFAULT_RAMP_TIME, adc_sample_number);

	return  new_preprocessing_block;
}
retval_t remove_preprocessing_block(preprocessing_block_t* preprocessing_block){
	if(preprocessing_block != NULL){

		remove_adc_iq_signal(preprocessing_block->adc_iq_signal);
		remove_signal_window(preprocessing_block->window);
		remove_interpolation(preprocessing_block->interpolation);
		remove_fft_signal(preprocessing_block->fft_block);
		remove_os_cfar(preprocessing_block->os_cfar);
		remove_snr(preprocessing_block->snr);
		remove_freq_signal_transform(preprocessing_block->freq_signal_transform);
		remove_peaks_selection(preprocessing_block->peaks_selection);
//		mod_schemes_deInit(preprocessing_block->modulation_schemes);
		remove_speeds_selection(preprocessing_block->speed_selection);
		remove_distances_selection(preprocessing_block->distances_selection);
		ytFree(preprocessing_block);

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}

}


retval_t prep_change_adc_sample_number(preprocessing_block_t* preprocessing_block, uint16_t adc_sample_number){
	if(preprocessing_block != NULL){
		preprocessing_block->adc_sample_number = adc_sample_number;

		change_adc_sample_number(preprocessing_block->adc_iq_signal, adc_sample_number);
		change_window_sample_number(preprocessing_block->window, adc_sample_number);
		change_interpolation_intput_sample_number(preprocessing_block->interpolation, adc_sample_number);

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t change_freq_sample_number(preprocessing_block_t* preprocessing_block, uint16_t fft_sample_number){
	if(preprocessing_block != NULL){
		preprocessing_block->fft_sample_number= fft_sample_number;

		change_interpolation_output_sample_number(preprocessing_block->interpolation, fft_sample_number);
		change_fft_sample_number(preprocessing_block->fft_block, fft_sample_number);

		preprocessing_block->snr->fft_sample_number = fft_sample_number;

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_freq_valid_samples(preprocessing_block_t* preprocessing_block, uint16_t freq_valid_samples){

	if(preprocessing_block != NULL){
		preprocessing_block->freq_valid_samples = freq_valid_samples;

		change_fft_effective_sample_number(preprocessing_block->fft_block, freq_valid_samples);
		change_cfar_effective_sample_number(preprocessing_block->os_cfar, freq_valid_samples);
		change_snr_effective_sample_number(preprocessing_block->snr, freq_valid_samples);

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t change_delay(preprocessing_block_t* preprocessing_block, uint32_t delay){
	if(preprocessing_block != NULL){

		preprocessing_block->delay = delay;

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}
retval_t change_speed_calc_rate(preprocessing_block_t* preprocessing_block, uint16_t speed_calc_rate){
	if(preprocessing_block != NULL){

		preprocessing_block->speed_calc_rate = speed_calc_rate;

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

#endif
