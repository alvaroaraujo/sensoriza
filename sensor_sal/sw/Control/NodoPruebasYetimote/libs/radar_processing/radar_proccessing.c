/*
 * radar_proccessing.c
 *
 *  Created on: 9 de jul. de 2017
 *      Author: Yo
 */


#include "radar_proccessing.h"

#if USE_RADAR_PROCESSING

#define TEST_LENGTH_SAMPLES  128
#define BLOCK_SIZE           128
#define NUM_TAPS 			 20

float32_t aux_i_signal[TEST_LENGTH_SAMPLES];
float32_t aux_q_signal[TEST_LENGTH_SAMPLES];

float32_t aux_i_signal_output[TEST_LENGTH_SAMPLES];
float32_t aux_q_signal_output[TEST_LENGTH_SAMPLES];


/* -------------------------------------------------------------------
 * Declare State buffer of size (numTaps + blockSize - 1)
 * ------------------------------------------------------------------- */
static float32_t firStateF32[BLOCK_SIZE + NUM_TAPS - 1];

/* ----------------------------------------------------------------------
** FIR Coefficients buffer generated using fir1() MATLAB function.
** ------------------------------------------------------------------- */
static float32_t filter_coefficients[NUM_TAPS] =
{
	-0.069021604, -0.067870763, -0.022526474, -0.044602857, -0.044991572, -0.067670144,
	-0.082339294, -0.12640395, -0.20670358, -0.63861972, 0.63861972, 0.20670358,
	0.12640395, 0.082339294, 0.067670144, 0.044991572, 0.044602857, 0.022526474,
	0.067870763, 0.069021604
};

retval_t prepare_signal(adc_iq_signal_t* adc_iq_signal, uint32_t* read_data, uint16_t num_samples){
	uint16_t i;
	float32_t mean_i, mean_q;
	uint16_t* temp_i;
	uint16_t* temp_q;

	mean_i = 0;
	mean_q = 0;
	for(i=0; i<num_samples; i++){
		temp_i = (uint16_t*) &read_data[i];
		temp_q = temp_i + 1;

		adc_iq_signal->i_signal[i] = (*temp_i);
		aux_i_signal[i] = (float32_t) (((float32_t)(*temp_i))*3.3f/65536);
		mean_i += aux_i_signal[i];

		adc_iq_signal->q_signal[i] = (*temp_q);
		aux_q_signal[i] = (float32_t) (((float32_t)(*temp_q))*3.3f/65536);
		mean_q += aux_q_signal[i];
	}
	mean_i = (float32_t) (mean_i/(float32_t)num_samples);
	mean_q = (float32_t) (mean_q/(float32_t)num_samples);

	for(i=0; i<num_samples; i++){			//Elimino la continua
		aux_i_signal[i] -= mean_i;
		aux_q_signal[i] -= mean_q;
	}

	//ARM FIR FILTER (TIME 2 MS)
	arm_fir_instance_f32 S;
	arm_fir_init_f32(&S, NUM_TAPS, (float32_t *)&filter_coefficients[0], &firStateF32[0], NUM_TAPS);

	arm_fir_f32(&S, (float*) &aux_i_signal[0], (float*) &aux_i_signal_output[0], BLOCK_SIZE);
	arm_fir_f32(&S, (float*) &aux_q_signal[0], (float*) &aux_q_signal_output[0], BLOCK_SIZE);


	for(i=0; i<num_samples; i++){			//Elimino la continua
		adc_iq_signal->ordered_iq_samples[i*2] = aux_i_signal_output[i];
		adc_iq_signal->ordered_iq_samples[(i*2)+1] = aux_q_signal_output[i];
	}

	return RET_OK;
}


retval_t radar_processData(preprocessing_block_t* preprocessing_block){
	if(preprocessing_block != NULL){
		//IQ SIGNAL//
//		reorder_iq_signal(preprocessing_block->adc_iq_signal);
//		iq_signal_to_float(preprocessing_block->adc_iq_signal);
		//WINDOW//

		preprocessing_block->window->window_func_ptr(preprocessing_block->window, preprocessing_block->adc_iq_signal);
		//INTERPOLATION//
		preprocessing_block->interpolation->interpolation_func_ptr(preprocessing_block->interpolation, preprocessing_block->window);
		//FFT//
		preprocessing_block->fft_block->fft_func_ptr(preprocessing_block->fft_block, preprocessing_block->interpolation);
		//FREQUENCY SIGNAL TRANSFORMS//
		preprocessing_block->freq_signal_transform->freq_transform_func_ptr(preprocessing_block->freq_signal_transform, preprocessing_block->fft_block);
		//SNR//
		preprocessing_block->snr->snr_func_ptr(preprocessing_block->snr, preprocessing_block->fft_block);
		//OS_CFAR//
		preprocessing_block->os_cfar->os_cfar_func_ptr(preprocessing_block->os_cfar, preprocessing_block->fft_block);
		//PEAKS SELECTION//
		do_peaks_selection(preprocessing_block->peaks_selection, preprocessing_block->fft_block, preprocessing_block->snr, preprocessing_block->os_cfar);
		//SPEEDS SELECTION//
		do_speeds_calculation(preprocessing_block->speed_selection, preprocessing_block->peaks_selection, preprocessing_block->fft_block);
		//DISTANCES SELECTION//
//		do_distances_calculation(preprocessing_block->distances_selection, preprocessing_block->speed_selection, preprocessing_block->peaks_selection, preprocessing_block->modulation_schemes, preprocessing_block->fft_block);
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}
#endif
