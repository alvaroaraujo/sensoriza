/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * os_cfar.c
 *
 *  Created on: 30/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file os_cfar.c
 */

#include "os_cfar.h"
#include "system_api.h"

#if USE_RADAR_PROCESSING

retval_t no_os_cfar(struct os_cfar* os_cfar, fft_signal_t* fft_signal);
retval_t do_os_cfar(struct os_cfar* os_cfar, fft_signal_t* fft_signal);


int cmpfunc_float32 (const void * a, const void * b);
int cmpfunc_uint16 (const void * a, const void * b);


os_cfar_t* new_os_cfar(uint16_t fft_sample_number, uint16_t effective_sample_number, os_cfar_type_t new_os_cfar_type){

	os_cfar_t* new_os_cfar = (os_cfar_t*) ytMalloc(sizeof(os_cfar_t));
	new_os_cfar->effective_sample_number = effective_sample_number;

	new_os_cfar->signal_threshold_output = (float32_t*) ytMalloc(new_os_cfar->effective_sample_number*sizeof(float32_t));

	new_os_cfar->window_size = DEFAULT_WINDOW_SIZE;
	new_os_cfar->k_order = DEFAULT_K_ORDER;
	new_os_cfar->alpha = DEFAULT_OSCFAR_ALPHA;

	new_os_cfar->os_cfar_type = new_os_cfar_type;

	switch(new_os_cfar_type){
	case Standard_os_cfar:
		new_os_cfar->os_cfar_func_ptr = do_os_cfar;
		break;
	case No_os_cfar:
		new_os_cfar->os_cfar_func_ptr = no_os_cfar;
		break;
	default:
		new_os_cfar->os_cfar_func_ptr = no_os_cfar;
		break;
	}

	return new_os_cfar;
}
retval_t remove_os_cfar(os_cfar_t* os_cfar){
	if(os_cfar != NULL){
		ytFree(os_cfar->signal_threshold_output);
		ytFree(os_cfar);
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t change_os_cfar_type(os_cfar_t* os_cfar, os_cfar_type_t new_os_cfar_type){
	if(os_cfar != NULL){
		os_cfar->os_cfar_type = new_os_cfar_type;

		switch(new_os_cfar_type){
		case Standard_os_cfar:
			os_cfar->os_cfar_func_ptr = do_os_cfar;
			break;
		case No_os_cfar:
			os_cfar->os_cfar_func_ptr = no_os_cfar;
			break;
		default:
			os_cfar->os_cfar_func_ptr = no_os_cfar;
			break;
		}

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}


retval_t no_os_cfar(struct os_cfar* os_cfar, fft_signal_t* fft_signal){
	if((os_cfar != NULL) && (fft_signal != NULL)){

		int32_t i = 0;
		for(i=0; i<os_cfar->effective_sample_number; i++){
			os_cfar->signal_threshold_output[i] = 0;
		}
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t do_os_cfar(struct os_cfar* os_cfar, fft_signal_t* fft_signal){
	if((os_cfar != NULL) && (fft_signal != NULL)){

		int32_t i = 0;
		int32_t j = 0;
		float32_t* window = (float32_t*) ytMalloc(os_cfar->window_size*sizeof(float32_t));

		for(i=0; i<os_cfar->effective_sample_number; i++){

			if(i<os_cfar->window_size/2){									//FIRST ELEMENTS
				for(j=0;j<os_cfar->window_size;j++){
					if(j<os_cfar->window_size/2){
						if((i-(os_cfar->window_size/2)+j)<0){
							window[j] = fft_signal->fft_mag_output_ordered_signal[os_cfar->effective_sample_number+(i-(os_cfar->window_size/2)+j)];
						}
						else{
						window[j] = fft_signal->fft_mag_output_ordered_signal[i-(os_cfar->window_size/2)+j];
						}
					}
					else{
						window[j] = fft_signal->fft_mag_output_ordered_signal[i-((os_cfar->window_size/2)-1)+j];
					}
				}
			}

			else if((os_cfar->effective_sample_number-i)<os_cfar->window_size/2){									//LAST ELEMENTS
				for(j=0;j<os_cfar->window_size;j++){
					if(j<os_cfar->window_size/2){
						window[j] = fft_signal->fft_mag_output_ordered_signal[i-(os_cfar->window_size/2)+j];
					}
					else{
						if((i-(os_cfar->window_size/2)+j)<os_cfar->effective_sample_number){
							window[j] = fft_signal->fft_mag_output_ordered_signal[i-(os_cfar->window_size/2)+j];
						}
						else{
							window[j] = fft_signal->fft_mag_output_ordered_signal[(i-(os_cfar->window_size/2)+j)-os_cfar->effective_sample_number];
						}
					}
				}
			}


			else{										//ALL OTHER ELEMENTS
				for(j=0;j<os_cfar->window_size;j++){
					if(j<os_cfar->window_size/2){
						window[j] = fft_signal->fft_mag_output_ordered_signal[i-(os_cfar->window_size/2)+j];
					}
					else{
						window[j] = fft_signal->fft_mag_output_ordered_signal[i-((os_cfar->window_size/2)-1)+j];
					}
				}
			}

			qsort(window, os_cfar->window_size, sizeof(float32_t), cmpfunc_float32);
			os_cfar->signal_threshold_output[i] = window[os_cfar->k_order] * os_cfar->alpha;
		}

		ytFree(window);
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}


retval_t change_cfar_effective_sample_number(os_cfar_t* os_cfar, uint16_t effective_sample_number){

	if(os_cfar != NULL){

		ytFree(os_cfar->signal_threshold_output);

		os_cfar->effective_sample_number = effective_sample_number;

		os_cfar->signal_threshold_output = (float32_t*) ytMalloc(os_cfar->effective_sample_number*sizeof(float32_t));

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}


int cmpfunc_float32 (const void * a, const void * b)
{
	  if (*(float32_t*)a > *(float32_t*)b) return 1;
	  else if (*(float32_t*)a < *(float32_t*)b) return -1;
	  else return 0;
}

int cmpfunc_uint16 (const void * a, const void * b)
{
	  if (*(uint16_t*)a > *(uint16_t*)b) return 1;
	  else if (*(uint16_t*)a < *(uint16_t*)b) return -1;
	  else return 0;
}
#endif
