/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * freq_signal_transform.c
 *
 *  Created on: 30/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file freq_signal_transform.c
 */

#include "freq_signal_transform.h"
#include "preprocessing.h"
#include "system_api.h"


#if USE_RADAR_PROCESSING

retval_t no_transform(struct freq_signal_transform* freq_signal_transform, fft_signal_t* fft_signal);
retval_t noise_reduction(struct freq_signal_transform* freq_signal_transform, fft_signal_t* fft_signal);

freq_signal_transform_t* new_freq_signal_transform(freq_transform_type_t freq_transform_type){

	freq_signal_transform_t* new_freq_signal_transform = (freq_signal_transform_t*) ytMalloc(sizeof(freq_signal_transform_t));

	new_freq_signal_transform->freq_transform_type = freq_transform_type;

	switch(freq_transform_type){
	case No_transform:
		new_freq_signal_transform->freq_transform_func_ptr = no_transform;
		break;
	case Noise_reduction:
		new_freq_signal_transform->freq_transform_func_ptr = noise_reduction;
		break;
	default:
		new_freq_signal_transform->freq_transform_func_ptr = no_transform;
		break;
	}

	return new_freq_signal_transform;
}
retval_t remove_freq_signal_transform(freq_signal_transform_t* freq_signal_transform){
	if(freq_signal_transform != NULL){
		ytFree(freq_signal_transform);
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t change_freq_signal_transform_type(freq_signal_transform_t* freq_signal_transform, freq_transform_type_t new_freq_transform_type){
	if(freq_signal_transform != NULL){
		freq_signal_transform->freq_transform_type = new_freq_transform_type;

		switch(new_freq_transform_type){
		case No_transform:
			freq_signal_transform->freq_transform_func_ptr = no_transform;
			break;
		case Noise_reduction:
			freq_signal_transform->freq_transform_func_ptr = noise_reduction;
			break;
		default:
			freq_signal_transform->freq_transform_func_ptr = no_transform;
			break;
		}
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}


retval_t no_transform(struct freq_signal_transform* freq_signal_transform, fft_signal_t* fft_signal){
	if((freq_signal_transform != NULL) && (fft_signal != NULL)){
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

#define NUM_SMOOTHING_SAMPLES	5

retval_t noise_reduction(struct freq_signal_transform* freq_signal_transform, fft_signal_t* fft_signal){

	float32_t noise_level;

#if SPEED_MODE
	noise_level = 0.04f;
#endif
#if DISTANCE_MODE
	noise_level = 0.8f;
#endif

	if((freq_signal_transform != NULL) && (fft_signal != NULL)){

		uint16_t i, j, samples;
		float32_t* signal = fft_signal->fft_mag_output_ordered_signal;
		uint16_t sample_num = fft_signal->effective_sample_number;

		float32_t* out_signal = ytMalloc(sample_num* sizeof(float32_t));
		memset(out_signal, 0, sample_num*sizeof(float32_t));


		for(i=0; i<sample_num; i++){
			samples = 0;
			if(i < (NUM_SMOOTHING_SAMPLES/2)){
				for(j=0; j<=(i+ (NUM_SMOOTHING_SAMPLES/2)); j++){
					out_signal[i] += signal[j];
					samples++;
				}
				out_signal[i] = out_signal[i]/samples;
			}
			else if(i > (sample_num - (NUM_SMOOTHING_SAMPLES/2) - 1)){
				for(j=(i-(NUM_SMOOTHING_SAMPLES/2)); j<sample_num; j++){
					out_signal[i] += signal[j];
					samples++;
				}
				out_signal[i] = out_signal[i]/samples;
			}
			else{
				for(j=(i-(NUM_SMOOTHING_SAMPLES/2)); j<(i+ ((NUM_SMOOTHING_SAMPLES+1)/2)); j++){
					out_signal[i] += signal[j];
				}
				out_signal[i] = out_signal[i]/NUM_SMOOTHING_SAMPLES;
			}
		}

		memcpy(signal, out_signal, sample_num*sizeof(float32_t));
		ytFree(out_signal);

//		for(i=0; i<sample_num; i++){
//			if(signal[i] < noise_level){
//				signal[i] = 0;
//			}
//		}

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

#endif
