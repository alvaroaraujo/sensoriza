/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * distances_selection.c
 *
 *  Created on: 31/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file distances_selection.c
 */

#include "distances_selection.h"
#include "system_api.h"

#if USE_RADAR_PROCESSING

distances_selection_t* new_distances_selection(uint16_t max_num_distances, uint16_t num_guard_low_distances){
	uint16_t i;
	distances_selection_t* new_distances_selection = (distances_selection_t*) ytMalloc(sizeof(distances_selection_t));

	new_distances_selection->distances = (float32_t*) ytMalloc(max_num_distances*sizeof(float32_t));
	new_distances_selection->distances_levels = (float32_t*) ytMalloc(max_num_distances*sizeof(float32_t));
	new_distances_selection->distances_snr = (float32_t*) ytMalloc(max_num_distances*sizeof(float32_t));

	new_distances_selection->max_num_distances = max_num_distances;
	new_distances_selection->calibration_factor = 1.24f;
	new_distances_selection->downramp_calibration_factor = 1.06f;

	new_distances_selection->current_num_distances = 0;			//First set everything to 0

	new_distances_selection->num_guard_low_distances = num_guard_low_distances;
	new_distances_selection->threshold_lvl_calibration_near = 0.60f;
	new_distances_selection->threshold_lvl_calibration_mid = 0.55f;
	new_distances_selection->threshold_lvl_calibration_far = 0.54f;
	new_distances_selection->threshold_lvl_calibration_vfar = 0.4f;
	new_distances_selection->threshold_snr_calibration_near = 0.5f;
	new_distances_selection->threshold_snr_calibration_mid = 0.5f;
	new_distances_selection->threshold_snr_calibration_far = 0.4f;
	new_distances_selection->threshold_snr_calibration_vfar = 0.4f;

	new_distances_selection->discard_speed = DISCARD_SPEED;

	for(i=0; i<new_distances_selection->max_num_distances; i++){
		new_distances_selection->distances[i]	= 0;
		new_distances_selection->distances_levels[i] = 0;
	}

	return new_distances_selection;
}


retval_t remove_distances_selection(distances_selection_t* distances_selection){
	if(distances_selection != NULL){
		if ((distances_selection->distances != NULL) && (distances_selection->distances_levels != NULL)){
			ytFree(distances_selection->distances);
			ytFree(distances_selection->distances_levels);
			ytFree(distances_selection->distances_snr);
		}
		else{
			return RET_ERROR;
		}
		ytFree(distances_selection);
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t set_max_num_distances(distances_selection_t* distances_selection, uint16_t max_num_distances){
	if(distances_selection != NULL){
		ytFree(distances_selection->distances);
		ytFree(distances_selection->distances_levels);

		distances_selection->max_num_distances = max_num_distances;

		distances_selection->distances = (float32_t*) ytMalloc(max_num_distances*sizeof(float32_t));
		distances_selection->distances_levels = (float32_t*) ytMalloc(max_num_distances*sizeof(float32_t));

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

//retval_t do_distances_calculation(distances_selection_t* distances_selection, speeds_selection_t* speeds_selection, peaks_selection_t* peaks_selection, modulation_scheme_t* mod_schemes, fft_signal_t* fft_signal){
//
//	if ((speeds_selection != NULL) && (peaks_selection != NULL) && (mod_schemes != NULL) && (fft_signal != NULL) && (distances_selection != NULL)){
//		uint16_t i = 0;
//		uint16_t j = 0;
//
//		int16_t temp_dist = 0;
//		int16_t temp_speed = 0;
//
//		uint16_t no_distance_flag = 0;
//
//		switch(mod_schemes->mod[mod_schemes->current_scheme]){
//		case UPRAMP:				//In upramp the distances frequencies are negative
//
//			distances_selection->current_num_distances = 0;			//First set everything to 0
//
//			for(i=0; i<distances_selection->max_num_distances; i++){
//				distances_selection->distances[i]	= 0;
//				distances_selection->distances_levels[i] = 0;
//			}
//
//			for(i=0; i<peaks_selection->current_num_peaks; i++){	//Now select the peaks as distances
//
//				if(distances_selection->current_num_distances>= distances_selection->max_num_distances){
//					break;
//				}
//
//				if(peaks_selection->peak_positions[i] < fft_signal->effective_sample_number/2){		//For upramp there are only negative frequencies
//
//					no_distance_flag = 0;
//					temp_dist = (int16_t) (peaks_selection->peak_positions[i] - (fft_signal->effective_sample_number/2));		//Negative freq index
//
//					//Descarto los picos por debajo de los thresholds
//					if(temp_dist < DEFAULT_FAR_DISTANCES_SAMPLES){		//Very Far peaks	>23m
//						if((peaks_selection->peak_levels[i] < (distances_selection->threshold_lvl_calibration_vfar*DEFAULT_DIST_SIGNAL_THR)) || (peaks_selection->snr_levels[i] < (distances_selection->threshold_snr_calibration_vfar*DEFAULT_DIST_SNR_THR))){
//							no_distance_flag = 1;
//						}
//					}
//					else if(temp_dist < DEFAULT_MID_DISTANCES_SAMPLES){	//Far Peaks			>10m
//						if((peaks_selection->peak_levels[i] < (distances_selection->threshold_lvl_calibration_far*DEFAULT_DIST_SIGNAL_THR)) || (peaks_selection->snr_levels[i] < (distances_selection->threshold_snr_calibration_far*DEFAULT_DIST_SNR_THR))){
//							no_distance_flag = 1;
//						}
//					}
//					else if(temp_dist < DEFAULT_NEAR_DISTANCES_SAMPLES){//Mid Peaks 		>5m
//						if((peaks_selection->peak_levels[i] < (distances_selection->threshold_lvl_calibration_mid*DEFAULT_DIST_SIGNAL_THR)) || (peaks_selection->snr_levels[i] < (distances_selection->threshold_snr_calibration_mid*DEFAULT_DIST_SNR_THR))){
//							no_distance_flag = 1;
//						}
//					}
//					else{												//Near Peaks		<5m
//						if((peaks_selection->peak_levels[i] < (distances_selection->threshold_lvl_calibration_near*DEFAULT_DIST_SIGNAL_THR)) || (peaks_selection->snr_levels[i] < (distances_selection->threshold_snr_calibration_near*DEFAULT_DIST_SNR_THR))){
//							no_distance_flag = 1;
//						}
//					}
//
//					//Descarto los picos cercanos a 0
//					if((temp_dist > ((-1)*distances_selection->num_guard_low_distances)) && (temp_dist < distances_selection->num_guard_low_distances)){
//						no_distance_flag = 1;
//					}
//
//					//Descarto los picos debidos a las velocidades
//					if(distances_selection->discard_speed == DISCARD_SPEED){
//						for(j=0; j<speeds_selection->current_num_speeds; j++){
//
//							temp_speed = (int16_t) (speeds_selection->speeds_positions[j] - (fft_signal->effective_sample_number/2));		//Negative or positive
//							if(temp_speed > 0){
//								temp_speed = (-1)*temp_speed;		//Paso todas a negativas
//							}
//							if(((temp_dist - (temp_speed-3)) > -4) && ((temp_dist - (temp_speed-3)) < 4)){		//En este caso se descarta este pico de distancia
//								no_distance_flag = 1;
//							}
//
//						}
//					}
//
//					if(!no_distance_flag){		//No se ha descartado el pico debido a velocidad, ni al nivel de se�al, asi que calculo la distancia
//
//						distances_selection->distances[distances_selection->current_num_distances] = (float32_t) ((-1)*temp_dist*C_SPEED*(mod_schemes->time[mod_schemes->current_scheme]-PREMOD_RAMP_TIME_US)* distances_selection->calibration_factor)/(2*mod_schemes->bandwidth[mod_schemes->current_scheme]*TIME_ADC_SAMPLE_US*fft_signal->fft_sample_number);
//						distances_selection->distances_levels[distances_selection->current_num_distances] = peaks_selection->peak_levels[i];
//						distances_selection->distances_snr[distances_selection->current_num_distances] = peaks_selection->snr_levels[i];
//						distances_selection->current_num_distances++;
//					}
//
//				}
//
//			}
//			break;
//
//		case DOWNRAMP:				//In downramp the distances frequencies are positive
//
//			distances_selection->current_num_distances = 0;			//First set everything to 0
//
//			for(i=0; i<distances_selection->max_num_distances; i++){
//				distances_selection->distances[i]	= 0;
//				distances_selection->distances_levels[i] = 0;
//			}
//
//			for(i=0; i<peaks_selection->current_num_peaks; i++){	//Now select the peaks as distances
//
//				if(distances_selection->current_num_distances>= distances_selection->max_num_distances){
//					break;
//				}
//
//				if(peaks_selection->peak_positions[i] > fft_signal->effective_sample_number/2){		//For downramp there are only positive frequencies
//
//					no_distance_flag = 0;
//					temp_dist = (int16_t) (peaks_selection->peak_positions[i] - (fft_signal->effective_sample_number/2));		//Positive freq index
//
//					//Descarto los picos por debajo de los thresholds
//					if(temp_dist > DEFAULT_FAR_DISTANCES_SAMPLES){		//Very Far peaks	>23m
//						if(((peaks_selection->peak_levels[i]*1.3f) < (distances_selection->threshold_lvl_calibration_vfar*DEFAULT_DIST_SIGNAL_THR)) || (peaks_selection->snr_levels[i] < (distances_selection->threshold_snr_calibration_vfar*DEFAULT_DIST_SNR_THR))){
//							no_distance_flag = 1;
//						}
//					}
//					else if(temp_dist > DEFAULT_MID_DISTANCES_SAMPLES){	//Far Peaks			>10m
//						if(((peaks_selection->peak_levels[i]*1.3f) < (distances_selection->threshold_lvl_calibration_far*DEFAULT_DIST_SIGNAL_THR)) || (peaks_selection->snr_levels[i] < (distances_selection->threshold_snr_calibration_far*DEFAULT_DIST_SNR_THR))){
//							no_distance_flag = 1;
//						}
//					}
//					else if(temp_dist > DEFAULT_NEAR_DISTANCES_SAMPLES){//Mid Peaks 		>5m
//						if(((peaks_selection->peak_levels[i]*1.3f) < (distances_selection->threshold_lvl_calibration_mid*DEFAULT_DIST_SIGNAL_THR)) || (peaks_selection->snr_levels[i] < (distances_selection->threshold_snr_calibration_mid*DEFAULT_DIST_SNR_THR))){
//							no_distance_flag = 1;
//						}
//					}
//					else{												//Near Peaks		<5m
//						if(((peaks_selection->peak_levels[i]*1.3f) < (distances_selection->threshold_lvl_calibration_near*DEFAULT_DIST_SIGNAL_THR)) || (peaks_selection->snr_levels[i] < (distances_selection->threshold_snr_calibration_near*DEFAULT_DIST_SNR_THR))){
//							no_distance_flag = 1;
//						}
//					}
//
//					//Descarto los picos cercanos a 0
//					if((temp_dist > ((-1)*distances_selection->num_guard_low_distances)) && (temp_dist < distances_selection->num_guard_low_distances)){
//						no_distance_flag = 1;
//					}
//
//					//Descarto los picos debidos a las velocidades
//					if(distances_selection->discard_speed == DISCARD_SPEED){
//						for(j=0; j<speeds_selection->current_num_speeds; j++){
//
//							temp_speed = (int16_t) (speeds_selection->speeds_positions[j] - (fft_signal->effective_sample_number/2));		//Negative or positive
//							if(temp_speed > 0){
//								temp_speed = (-1)*temp_speed;		//Paso todas a negativas
//							}
//							if(((temp_dist + (temp_speed-3)) > -4) && ((temp_dist + (temp_speed-3)) < 4)){		//En este caso se descarta este pico de distancia
//								no_distance_flag = 1;
//							}
//
//						}
//					}
//
//					if(!no_distance_flag){		//No se ha descartado el pico debido a velocidad, ni al nivel de se�al, asi que calculo la distancia
//
//						distances_selection->distances[distances_selection->current_num_distances] = (float32_t) ((temp_dist)*C_SPEED*(mod_schemes->time[mod_schemes->current_scheme]-PREMOD_RAMP_TIME_US)* distances_selection->calibration_factor* distances_selection->downramp_calibration_factor)/(2*mod_schemes->bandwidth[mod_schemes->current_scheme]*TIME_ADC_SAMPLE_US*fft_signal->fft_sample_number);
//						distances_selection->distances_levels[distances_selection->current_num_distances] = peaks_selection->peak_levels[i]*1.3f;
//						distances_selection->distances_snr[distances_selection->current_num_distances] = peaks_selection->snr_levels[i];
//						distances_selection->current_num_distances++;
//					}
//
//				}
//
//			}
//			break;
//
//			//TODO HACER LO MISMO QUE EN UPRAMP. DE MOMENTO NO USO DOWNRAMP NUNCA
//
//			break;
//		case CONSTANT:				//If constant there are no distances, just speeds
//			break;
//		default:
//			break;
//		}
//		return RET_OK;
//	}
//	else{
//		return RET_ERROR;
//	}
//}
#endif
