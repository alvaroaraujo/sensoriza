/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * snr.c
 *
 *  Created on: 30/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file snr.c
 */

#include "snr.h"
#include "system_api.h"

#if USE_RADAR_PROCESSING

retval_t no_snr(struct snr* snr, fft_signal_t* fft_signal);
retval_t do_snr(struct snr* snr, fft_signal_t* fft_signal);

snr_t* new_snr(uint16_t fft_sample_number, uint16_t effective_sample_number, snr_calc_t snr_calc){

	snr_t* new_snr = (snr_t*) ytMalloc(sizeof(snr_t));
	new_snr->effective_sample_number = effective_sample_number;
	new_snr->fft_sample_number = fft_sample_number;

	new_snr->snr_signal = (float32_t*) ytMalloc(new_snr->effective_sample_number*sizeof(float32_t));
	new_snr->noise_lvl = 0;
	new_snr->far_freq_sample = DEFAULT_FAR_FREQ_SAMPLE;
	new_snr->far_freq_constant_value = DEFAULT_FAR_CONSTANT_VALUE;



	new_snr->snr_calc = snr_calc;

	switch(snr_calc){
	case Standard_snr:
		new_snr->snr_func_ptr = do_snr;
		break;
	case No_snr:
		new_snr->snr_func_ptr = no_snr;
		break;
	default:
		new_snr->snr_func_ptr = no_snr;
		break;
	}

	return new_snr;
}
retval_t remove_snr(snr_t* snr){
	if(snr != NULL){
		ytFree(snr->snr_signal);
		ytFree(snr);
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t change_snr_type(snr_t* snr, snr_calc_t new_snr_calc){
	if(snr != NULL){
		snr->snr_calc = new_snr_calc;

			switch(new_snr_calc){
			case Standard_snr:
				snr->snr_func_ptr = do_snr;
				break;
			case No_snr:
				snr->snr_func_ptr = no_snr;
				break;
			default:
				snr->snr_func_ptr = no_snr;
				break;
			}
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}


retval_t no_snr(struct snr* snr, fft_signal_t* fft_signal){

	if((snr != NULL) && (fft_signal != NULL)){
		uint16_t i = 0;
		snr->noise_lvl = 0;
		for(i=0; i<snr->effective_sample_number; i++){
			snr->snr_signal[i] = 1000;
		}
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}

}
retval_t do_snr(struct snr* snr, fft_signal_t* fft_signal){


	if((snr != NULL) && (fft_signal != NULL)){
		uint16_t i = 0;
		float32_t noise = 0;

//		for(i=0; i<snr->fft_sample_number; i++){			//Se hace esto para que si el ruido es muy elevado fuera de la banda analizada reducirlo a un valor coherente
//															//As� se evitan problemas en el snr en pasillos muy largos con muchas reflexiones a altas frecuencias
//			if(fft_signal->fft_mag_output_signal[i] > 4){
//				if ((i > snr->far_freq_sample) && (i < (snr->fft_sample_number - snr->far_freq_sample))){
//
//					noise += snr->far_freq_constant_value;
//
//				}
//				else{
//					noise += fft_signal->fft_mag_output_signal[i];
//				}
//			}
//			else{
//				noise += fft_signal->fft_mag_output_signal[i];
//			}
//		}
//		noise = (noise/snr->fft_sample_number) + 0.001f; //Sumo una cantidad peque�a para evitar que sea 0



		for(i=0; i<fft_signal->fft_sample_number; i++){

			noise += fft_signal->fft_mag_output_signal[i];
		}
		noise = (noise/fft_signal->fft_sample_number) + 0.001f; //Sumo una cantidad peque�a para evitar que sea 0


		snr->noise_lvl = noise;
		/// SNR CALCULATION
		for(i=0; i<snr->effective_sample_number; i++){
			snr->snr_signal[i] = fft_signal->fft_mag_output_ordered_signal[i]/noise;
		}
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t change_snr_effective_sample_number(snr_t* snr, uint16_t effective_sample_number){
	if(snr != NULL){
		ytFree(snr->snr_signal);

		snr->effective_sample_number = effective_sample_number;

		snr->snr_signal = (float32_t*) ytMalloc(snr->effective_sample_number*sizeof(float32_t));
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

#endif
