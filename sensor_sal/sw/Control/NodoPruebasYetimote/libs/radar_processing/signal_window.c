/*
 * Copyright (c) 2016, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * signal_window.c
 *
 *  Created on: 30/5/2016
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@die.upm.es>
 *
 */
/**
 * @file signal_window.c
 */

#include "signal_window.h"
#include "system_api.h"

#if USE_RADAR_PROCESSING

retval_t no_window(struct signal_window* window, adc_iq_signal_t* adc_iq_signals);
retval_t tukey_window(struct signal_window* window, adc_iq_signal_t* adc_iq_signals);
retval_t hanning_window(struct signal_window* window, adc_iq_signal_t* adc_iq_signals);
retval_t hamming_window(struct signal_window* window, adc_iq_signal_t* adc_iq_signals);

signal_window_t* new_signal_window(uint16_t sample_number, window_type_t window_type){

	signal_window_t* new_signal_window = (signal_window_t*) ytMalloc(sizeof(signal_window_t));
	new_signal_window->sample_number = sample_number;
	new_signal_window->signal_post_window = (float32_t*) ytMalloc(new_signal_window->sample_number*2*sizeof(float32_t));
	new_signal_window->window_type = window_type;
	new_signal_window->tukey_alpha = DEFAULT_TUKEY_ALPHA;

	switch(window_type){
	case Tukey:
		new_signal_window->window_func_ptr = tukey_window;
		break;
	case Hanning:
		new_signal_window->window_func_ptr = hanning_window;
		break;
	case Hamming:
		new_signal_window->window_func_ptr = hamming_window;
		break;
	case No_Window:
		new_signal_window->window_func_ptr = no_window;
		break;
	default:
		new_signal_window->window_func_ptr = no_window;
		break;
	}

	return new_signal_window;
}
retval_t remove_signal_window(signal_window_t* signal_window){
	if(signal_window != NULL){
		ytFree(signal_window->signal_post_window);
		ytFree(signal_window);
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t change_window_type(signal_window_t* signal_window, window_type_t new_window_type){
	if(signal_window != NULL){
		signal_window->window_type = new_window_type;

		switch(new_window_type){
		case Tukey:
			signal_window->window_func_ptr = tukey_window;
			break;
		case Hanning:
			signal_window->window_func_ptr = hanning_window;
			break;
		case Hamming:
			signal_window->window_func_ptr = hamming_window;
			break;
		case No_Window:
			signal_window->window_func_ptr = no_window;
			break;
		default:
			signal_window->window_func_ptr = no_window;
			break;
		}
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t no_window(struct signal_window* window, adc_iq_signal_t* adc_iq_signals){
	if((window != NULL) && (adc_iq_signals != NULL)){
		uint16_t i = 0;

		for(i=0; i<window->sample_number; i++){
		  window->signal_post_window[i*2] = adc_iq_signals->ordered_iq_samples[i*2];

		  window->signal_post_window[(i*2)+1] = adc_iq_signals->ordered_iq_samples[(i*2) +1];

		}
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

retval_t tukey_window(struct signal_window* window, adc_iq_signal_t* adc_iq_signals){

	if((window != NULL) && (adc_iq_signals != NULL)){
		uint16_t i = 0;

		float32_t alpha = window->tukey_alpha;

		uint16_t low_limit = (uint16_t) (alpha*(window->sample_number-1))/2;
		uint16_t high_limit = (uint16_t) (window->sample_number-1)*(1-(alpha/2));



		for(i=0; i<window->sample_number; i++){
		  float32_t val;

		  if(i<low_limit){									//alpha*(N-1)/2=64 N=256
			  val = PI*(((2*i)/(alpha*(window->sample_number-1)))-1);
			  val = arm_cos_f32(val);
			  window->signal_post_window[i*2] = adc_iq_signals->ordered_iq_samples[i*2]*0.5f*(1 + val);

			  window->signal_post_window[(i*2)+1] = adc_iq_signals->ordered_iq_samples[(i*2)+1]*0.5f*(1 + val);

		  }

		  if(i>high_limit){									//(N-1)(1-alpha/2)=192 N=256
			  val = PI*(((2*i)/(alpha*(window->sample_number-1)))+1-(2/alpha));
			  val = arm_cos_f32(val);
			  window->signal_post_window[i*2] = adc_iq_signals->ordered_iq_samples[i*2]*0.5f*(1 + val);

			  window->signal_post_window[(i*2)+1] = adc_iq_signals->ordered_iq_samples[(i*2)+1]*0.5f*(1 + val);

		  }

		  else{
			  window->signal_post_window[i*2] = adc_iq_signals->ordered_iq_samples[i*2];

			  window->signal_post_window[(i*2)+1] = adc_iq_signals->ordered_iq_samples[(i*2)+1];
		  }


		}
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}

}

retval_t hanning_window(struct signal_window* window, adc_iq_signal_t* adc_iq_signals){

	if((window != NULL) && (adc_iq_signals != NULL)){
		uint16_t i = 0;

		for(i=0; i<window->sample_number; i++){
		  float32_t val;
		  val = (2*PI*i)/(window->sample_number-1);
		  val = arm_cos_f32(val);
		  window->signal_post_window[i*2] = adc_iq_signals->ordered_iq_samples[i*2]*0.5f*(1-val);

		  window->signal_post_window[(i*2)+1] = adc_iq_signals->ordered_iq_samples[(i*2) +1]*0.5f*(1-val);

		}

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}


retval_t hamming_window(struct signal_window* window, adc_iq_signal_t* adc_iq_signals){

	if((window != NULL) && (adc_iq_signals != NULL)){
		uint16_t i = 0;

		for(i=0; i<window->sample_number; i++){
		  float32_t val;
		  val = (2*PI*i)/(window->sample_number-1);
		  val = arm_cos_f32(val);
		  window->signal_post_window[i*2] = adc_iq_signals->ordered_iq_samples[i*2]*(0.54f-(0.46f*val));

		  window->signal_post_window[(i*2)+1] = adc_iq_signals->ordered_iq_samples[(i*2)+1]*(0.54f-(0.46f*val));

		}

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}


retval_t change_window_sample_number(signal_window_t* signal_window, uint16_t adc_sample_number){

	if(signal_window != NULL){
		ytFree(signal_window->signal_post_window);

		signal_window->sample_number = adc_sample_number;

		signal_window->signal_post_window = (float32_t*) ytMalloc(signal_window->sample_number*2*sizeof(float32_t));

		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

#endif
