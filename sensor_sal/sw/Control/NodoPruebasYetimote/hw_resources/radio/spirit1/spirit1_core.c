/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * spirit1_core.c
 *
 *  Created on: 4 de dic. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file spirit1_core.c
 */

#include "spirit1_core.h"
#include "SPIRIT_config.h"
#include "SPIRIT_Aes.h"


#define DEBUG 0
#if DEBUG
#include "yetimote_stdio.h"
#define PRINTF(...) _printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

/* PACKET DEFAULT CONFIG */
#define PREAMBLE_LENGTH             PKT_PREAMBLE_LENGTH_04BYTES
#define SYNC_LENGTH                 PKT_SYNC_LENGTH_4BYTES
#define SYNC_WORD                   0xC5C5C5C5
#define LENGTH_TYPE                 PKT_LENGTH_FIX
#define LENGTH_WIDTH                0
#define CRC_MODE                    PKT_CRC_MODE_8BITS
#define CONTROL_LENGTH              PKT_CONTROL_LENGTH_0BYTES
#define EN_FEC                      S_ENABLE
#define EN_WHITENING                S_ENABLE
#define EN_ADDRESS                  S_DISABLE

/* DEFAULT XTAL PPM */
#define SPIRIT1_XTAL_PPM		0


#define SPIRIT1_MAX_FIFO_LEN	96

#define AES_TIMEOUT		100

#define SPIRIT1_SEND_TIMEOUT 	50
#define SPIRIT1_RCV_TIMEOUT 	50

static retval_t spirit1_set_mode(spirit1_data_t* spirit1_data, spirit1_state_t state);

static void spirit1_rx_timeout_cb(const void* args);
static void spirit1_tx_timeout_cb(const void* args);

/**
 *
 * @param cs_pin
 * @param sdn_pin
 * @param gpio3_int_pin
 * @param interrupt_cb_func
 * @return
 */
spirit1_data_t* new_spirit1_data(gpio_pin_t cs_pin, gpio_pin_t sdn_pin, gpio_pin_t gpio3_int_pin, ytProcessFunc_t interrupt_cb_func, void* args){
	spirit1_data_t* new_spirit1_data;
	if((new_spirit1_data = (spirit1_data_t*) ytMalloc(sizeof(spirit1_data_t))) == NULL){
		return NULL;
	}
	new_spirit1_data->cs_pin = cs_pin;
	new_spirit1_data->gpio3_int_pin = gpio3_int_pin;
	new_spirit1_data->sdn_pin = sdn_pin;
	new_spirit1_data->spi_id = 0;
	new_spirit1_data->interrupt_cb_func = interrupt_cb_func;
	new_spirit1_data->interrupt_cb_arg = args;
	new_spirit1_data->transmitting_packet = 0;
	new_spirit1_data->receiving_packet = 0;
	new_spirit1_data->checking_rssi = 0;
	new_spirit1_data->spirit1_rx_timeout_timer = ytTimerCreate(ytTimerOnce, spirit1_rx_timeout_cb, (void*) new_spirit1_data);
	new_spirit1_data->spirit1_tx_timeout_timer = ytTimerCreate(ytTimerOnce, spirit1_tx_timeout_cb, (void*) new_spirit1_data);

	new_spirit1_data->spirit1_aes_semph_id = ytSemaphoreCreate(1);
	ytSemaphoreWait(new_spirit1_data->spirit1_aes_semph_id, 1);

	return new_spirit1_data;
}

/**
 *
 * @param spirit1_data
 * @return
 */
retval_t delete_spirit1_data(spirit1_data_t* spirit1_data){
	if(spirit1_data == NULL){
		return RET_ERROR;
	}
	ytSemaphoreDelete(spirit1_data->spirit1_aes_semph_id);
//	ytSemaphoreDelete(spirit1_data->spirit1_tx_semph_id);
	ytFree(spirit1_data);
	return RET_OK;

}


/**
 *
 * @param spirit1_data
 * @param spirit1_init_config
 * @return
 */
retval_t spirit1_hw_init(spirit1_data_t* spirit1_data, spirit1_config_t* spirit1_init_config, char* spi_dev){
	spirit1_status_t status_b;
	uint64_t t1;

	SGpioInit xGpioIRQ={
	  SPIRIT_GPIO_3,
	  SPIRIT_GPIO_MODE_DIGITAL_OUTPUT_LP,
	  SPIRIT_GPIO_DIG_OUT_IRQ
	};

	if(spirit1_init_config->packet_length > SPIRIT1_MAX_FIFO_LEN){
		return RET_ERROR;
	}

	spirit1_arch_init(spirit1_data, spi_dev);	//Lo primero es inicializar todos el HW necesario del uC. SPI y GPIOs

	spirit1_arch_power_on(spirit1_data);	//Me enciendo y espero
	ytDelay(2);

	spirit1_arch_send_cmd_strobe(spirit1_data, COMMAND_READY);	//Mando un strobe inicial aunque deberia estar en READY ya

	spirit1_arch_send_cmd_strobe(spirit1_data, COMMAND_SRES);	//Reseteo el Spirit1 al principio del todos

	status_b = spirit1_arch_get_status(spirit1_data);
	t1 = ytTimeGetTimestamp();
		while(status_b.SPIRIT1_STATE != SPIRIT1_STATE_READY){

			spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_READY);

			if((ytTimeGetTimestamp()-t1) > SPIRIT1_FUNCS_TIMEOUT){
				return RET_ERROR;
			}

			status_b = spirit1_arch_get_status(spirit1_data);
		}

	if(spirit1_check_device_info_reg(spirit1_data) != RET_OK){	//Compruebo el registro de info del spirit1. Si no es el que debe da error
		spirit1_arch_power_off(spirit1_data);
		return RET_ERROR;
	}

	if(spirit1_set_mode(spirit1_data, SPIRIT1_STATE_STANDBY) != RET_OK){		//Me aseguro de entrar en modo standby
		spirit1_arch_power_off(spirit1_data);
		return RET_ERROR;
	}

	if (spirit1_init_config->xtal_freq < DOUBLE_XTAL_THR) {			//Una vez en standby pongo el clock divider como sea preciso segun el cristal
		SpiritRadioSetDigDiv(spirit1_data, S_DISABLE);
		SpiritRadioSetRefDiv(spirit1_data, S_DISABLE);
	}
	else {
		SpiritRadioSetDigDiv(spirit1_data, S_ENABLE);
		SpiritRadioSetRefDiv(spirit1_data, S_ENABLE);
	}

	if(spirit1_set_mode(spirit1_data, SPIRIT1_STATE_READY) != RET_OK){		//Vuelvo a modo Ready
		spirit1_arch_power_off(spirit1_data);
		return RET_ERROR;
	}

	SpiritRadioSetDatarate(spirit1_data, spirit1_init_config->baud_rate, spirit1_init_config->xtal_freq);
	SpiritRadioSetFrequencyDev(spirit1_data, spirit1_init_config->freq_deviation, spirit1_init_config->xtal_freq);
	SpiritRadioSetChannelBW(spirit1_data, spirit1_init_config->rx_bandwidth, spirit1_init_config->xtal_freq);
	SpiritRadioSetModulation(spirit1_data, spirit1_init_config->modulation);

	SpiritSetAnalogIfOffset(spirit1_data, spirit1_init_config->xtal_freq);
	SpiritSetDigitalIfOffset(spirit1_data, spirit1_init_config->xtal_freq);

	/* Sets Xtal configuration */
	if (spirit1_init_config->xtal_freq > DOUBLE_XTAL_THR) {
		SpiritRadioSetXtalFlag(spirit1_data, XTAL_FLAG((spirit1_init_config->xtal_freq / 2)));
	} else {
		SpiritRadioSetXtalFlag(spirit1_data, XTAL_FLAG(spirit1_init_config->xtal_freq));
	}

	SpiritRadioSetChannelSpace(spirit1_data, spirit1_init_config->channel_spacing, spirit1_init_config->xtal_freq);
	SpiritRadioSetChannel(spirit1_data, (uint8_t)spirit1_init_config->channel_num);

	/* Enable the freeze option of the AFC on the SYNC word */
	SpiritRadioAFCFreezeOnSync(spirit1_data, S_ENABLE);

	SpiritRadioSetFrequencyBase(spirit1_data, spirit1_init_config->modulation_freq, spirit1_init_config->xtal_freq);	//Inicializo la frecuencia de modulacion

	SpiritRadioSetFrequencyOffsetPpm(spirit1_data, SPIRIT1_XTAL_PPM, spirit1_init_config->xtal_freq);	//Inicializo xtal ppm

	SpiritRadioSetPALeveldBm(spirit1_data, 0, spirit1_init_config->output_power, spirit1_init_config->xtal_freq);	//Inicializo potencia transmitida
	SpiritRadioSetPALevelMaxIndex(spirit1_data, 0);

	PktBasicInit x_basic_init = {	//Constant packet config
		  PREAMBLE_LENGTH,
		  SYNC_LENGTH,
		  SYNC_WORD,
		  LENGTH_TYPE,
		  LENGTH_WIDTH,
		  CRC_MODE,
		  CONTROL_LENGTH,
		  EN_ADDRESS,
		  EN_FEC,
		  EN_WHITENING
	};
	SpiritPktBasicInit(spirit1_data, &x_basic_init);	//Inicializo el tipo de paquete

	SpiritPktBasicSetPayloadLength(spirit1_data, spirit1_init_config->packet_length);	//Fijo el tama�o de paquetes

	SpiritRadioPersistenRx(spirit1_data, S_ENABLE);			//Rx en modo persistent
	SpiritQiSetSqiThreshold(spirit1_data, SQI_TH_0);		//Fijo el threshold SQI. Se toleran 2 bits de error en la sync word
	SpiritQiSqiCheck(spirit1_data, S_ENABLE);				//Activo el checkeo de sqi
	SpiritTimerSetRxTimeoutStopCondition(spirit1_data, SQI_ABOVE_THRESHOLD);	//Selecciono la condicion para parar el timeout timer. IMPORTANTE que esta funcion este, ya que sino no salta nunca la interrupccion RX_DATA_READY


	SpiritGpioInit(spirit1_data, &xGpioIRQ);	//Init GPIO3 as IRQ output pin

	SpiritIrqDeInit(spirit1_data, NULL);			//Disable and clear al Spirit IRQs
	SpiritIrqClearStatus(spirit1_data);

	SpiritIrq(spirit1_data, VALID_SYNC, S_ENABLE);		//Activo interrupciones que me interesan
	SpiritIrq(spirit1_data, TX_DATA_SENT, S_ENABLE);
	SpiritIrq(spirit1_data, RX_DATA_READY, S_ENABLE);
	SpiritIrq(spirit1_data, TX_FIFO_ERROR, S_ENABLE);
	SpiritIrq(spirit1_data, RX_FIFO_ERROR, S_ENABLE);
	SpiritIrq(spirit1_data, AES_END, S_ENABLE);

	spirit1_data->packet_length = spirit1_init_config->packet_length;
	spirit1_data->xtal_freq = spirit1_init_config->xtal_freq;

	spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_FRX);
	spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_FTX);

	if(spirit1_set_mode(spirit1_data, SPIRIT1_STATE_STANDBY) != RET_OK){		//Dejo el spirit en modo dormido inicialmente
		spirit1_arch_power_off(spirit1_data);
		return RET_ERROR;
	}

	spirit1_arch_disable_interrupt(spirit1_data);
	return RET_OK;

}

/**
 *
 * @param spirit1_data
 * @return
 */
retval_t spirit1_hw_deinit(spirit1_data_t* spirit1_data){

	if(spirit1_data == NULL){
		return RET_ERROR;
	}
	spirit1_arch_power_off(spirit1_data);
	spirit1_arch_deInit(spirit1_data);

	return RET_OK;
}

/**
 *
 * @param spirit1_data
 * @param baud_rate
 * @return
 */
retval_t spirit1_set_baud_rate(spirit1_data_t* spirit1_data, uint32_t baud_rate){
	if(spirit1_data == NULL){
		return RET_ERROR;
	}
	if(spirit1_data->transmitting_packet || spirit1_data->receiving_packet || spirit1_data->checking_rssi){
		return RET_ERROR;
	}

	if (baud_rate > 128000){
		SpiritRadioSetDatarate(spirit1_data, baud_rate, spirit1_data->xtal_freq);
		SpiritRadioSetFrequencyDev(spirit1_data, 128e3, spirit1_data->xtal_freq);
		SpiritRadioSetChannelBW(spirit1_data, 540e3, spirit1_data->xtal_freq);
		SpiritRadioSetChannelSpace(spirit1_data, 540e3, spirit1_data->xtal_freq);
	}
	else if(baud_rate > 64000){
		SpiritRadioSetDatarate(spirit1_data, baud_rate, spirit1_data->xtal_freq);
		SpiritRadioSetFrequencyDev(spirit1_data, 64e3, spirit1_data->xtal_freq);
		SpiritRadioSetChannelBW(spirit1_data, 280e3, spirit1_data->xtal_freq);
		SpiritRadioSetChannelSpace(spirit1_data, 280e3, spirit1_data->xtal_freq);
	}
	else if(baud_rate > 40000){
		SpiritRadioSetDatarate(spirit1_data, baud_rate, spirit1_data->xtal_freq);
		SpiritRadioSetFrequencyDev(spirit1_data, 40e3, spirit1_data->xtal_freq);
		SpiritRadioSetChannelBW(spirit1_data, 150e3, spirit1_data->xtal_freq);
		SpiritRadioSetChannelSpace(spirit1_data, 150e3, spirit1_data->xtal_freq);
	}
	else if(baud_rate > 20000){
		SpiritRadioSetDatarate(spirit1_data, baud_rate, spirit1_data->xtal_freq);
		SpiritRadioSetFrequencyDev(spirit1_data, 20e3, spirit1_data->xtal_freq);
		SpiritRadioSetChannelBW(spirit1_data, 100e3, spirit1_data->xtal_freq);
		SpiritRadioSetChannelSpace(spirit1_data, 100e3, spirit1_data->xtal_freq);
	}
	else if(baud_rate > 10000){
		SpiritRadioSetDatarate(spirit1_data, baud_rate, spirit1_data->xtal_freq);
		SpiritRadioSetFrequencyDev(spirit1_data, 10e3, spirit1_data->xtal_freq);
		SpiritRadioSetChannelBW(spirit1_data, 58e3, spirit1_data->xtal_freq);
		SpiritRadioSetChannelSpace(spirit1_data, 58e3, spirit1_data->xtal_freq);
	}
	else if(baud_rate > 2000){
		SpiritRadioSetDatarate(spirit1_data, baud_rate, spirit1_data->xtal_freq);
		SpiritRadioSetFrequencyDev(spirit1_data, 8e3, spirit1_data->xtal_freq);
		SpiritRadioSetChannelBW(spirit1_data, 58e3, spirit1_data->xtal_freq);
		SpiritRadioSetChannelSpace(spirit1_data, 58e3, spirit1_data->xtal_freq);
	}
	else{
		SpiritRadioSetDatarate(spirit1_data, baud_rate, spirit1_data->xtal_freq);
		SpiritRadioSetFrequencyDev(spirit1_data, 4.8e3, spirit1_data->xtal_freq);
		SpiritRadioSetChannelBW(spirit1_data, 58e3, spirit1_data->xtal_freq);
		SpiritRadioSetChannelSpace(spirit1_data, 58e3, spirit1_data->xtal_freq);
	}
	return RET_OK;
}

/**
 *
 * @param spirit1_data
 * @param output_power
 * @return
 */
retval_t spirit1_set_output_power(spirit1_data_t* spirit1_data, int16_t output_power){
	if(spirit1_data == NULL){
		return RET_ERROR;
	}
	if(spirit1_data->transmitting_packet || spirit1_data->receiving_packet || spirit1_data->checking_rssi){
		return RET_ERROR;
	}
	SpiritRadioSetPALeveldBm(spirit1_data, 0, output_power, spirit1_data->xtal_freq);
	return RET_OK;
}

/**
 *
 * @param spirit1_data
 * @param channel_num
 * @return
 */
retval_t spirit1_set_channel_num(spirit1_data_t* spirit1_data, uint16_t channel_num){
	if(spirit1_data == NULL){
		return RET_ERROR;
	}
	if(spirit1_data->transmitting_packet || spirit1_data->receiving_packet || spirit1_data->checking_rssi){
		return RET_ERROR;
	}
	SpiritRadioSetChannel(spirit1_data, (uint8_t)channel_num);
	return RET_OK;
}

/**
 *
 * @param spirit1_data
 * @param base_freq
 * @return
 */
retval_t spirit1_set_base_freq(spirit1_data_t* spirit1_data, uint32_t base_freq){
	if(spirit1_data == NULL){
		return RET_ERROR;
	}
	if(spirit1_data->transmitting_packet || spirit1_data->receiving_packet || spirit1_data->checking_rssi){
		return RET_ERROR;
	}
	SpiritRadioSetFrequencyBase(spirit1_data, base_freq, spirit1_data->xtal_freq);
	SpiritRadioSetFrequencyOffsetPpm(spirit1_data, SPIRIT1_XTAL_PPM, spirit1_data->xtal_freq);

	return RET_OK;
}

/**
 *
 * @param spirit1_data
 * @param packet_size
 * @return
 */
retval_t spirit1_set_packet_size(spirit1_data_t* spirit1_data, uint16_t packet_size){
	if(spirit1_data == NULL){
		return RET_ERROR;
	}
	if(spirit1_data->transmitting_packet || spirit1_data->receiving_packet || spirit1_data->checking_rssi){
		return RET_ERROR;
	}
	SpiritPktBasicSetPayloadLength(spirit1_data, packet_size);

	return RET_OK;
}

/**
 *
 * @param spirit1_data
 * @param data
 * @param size
 * @return
 */
retval_t spirit1_send_data(spirit1_data_t* spirit1_data, uint8_t* data, uint16_t size){
	if(spirit1_data == NULL){
		return RET_ERROR;
	}
	if(size != spirit1_data->packet_length) {		//FIXED PACKET SIZE
		return RET_ERROR;
	}

	if(spirit1_data->transmitting_packet){
		return RET_ERROR;
	}
	spirit1_data->transmitting_packet = 1;

	if (spirit1_data->receiving_packet){
		spirit1_data->transmitting_packet = 0;
		return RET_ERROR;
	}


	spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_FTX);	//Primero limpio la tx fifo
	spirit1_arch_write_txfifo(spirit1_data, data, size);			//Relleno la tx fifo

	spirit1_arch_enable_interrupt(spirit1_data);

	ytTimerStart(spirit1_data->spirit1_tx_timeout_timer, SPIRIT1_SEND_TIMEOUT);
	spirit1_set_mode(spirit1_data, SPIRIT1_STATE_TX);				//Voy al estado tx (este donde este, aunque este dormido o recibiendo)


	spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_FRX);	//Limpio la rx fifo por lo que pueda haber pasado raro.
	return RET_OK;
}

/**
 *
 * @param spirit1_data
 * @return
 */
retval_t spirit1_set_mode_rx(spirit1_data_t* spirit1_data){
	if(spirit1_data == NULL){
		return RET_ERROR;
	}
	if(spirit1_data->transmitting_packet){	//Si estoy transmitiendo no me puedo poner a recibir
		return RET_ERROR;
	}

	spirit1_arch_enable_interrupt(spirit1_data);

	spirit1_set_mode(spirit1_data, SPIRIT1_STATE_RX);	//Me pongo en modo RX este donde este

	return RET_OK;
}

/**
 *
 * @param spirit1_data
 * @return
 */
retval_t spirit1_set_mode_sleep(spirit1_data_t* spirit1_data){
	if(spirit1_data == NULL){
		return RET_ERROR;
	}

	spirit1_data->transmitting_packet = 0;
	spirit1_data->receiving_packet = 0;

	spirit1_arch_disable_interrupt(spirit1_data);

	spirit1_set_mode(spirit1_data, SPIRIT1_STATE_STANDBY);	//Me pongo en modo STANDBY este donde este

	spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_FTX);	//Limpio las fifos
	spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_FRX);	//Limpio las fifos

	return RET_OK;
}

/**
 *
 * @param spirit1_data
 * @return
 */
retval_t spirit1_set_mode_idle(spirit1_data_t* spirit1_data){
	if(spirit1_data == NULL){
		return RET_ERROR;
	}

	spirit1_data->transmitting_packet = 0;
	spirit1_data->receiving_packet = 0;

	spirit1_arch_disable_interrupt(spirit1_data);

	spirit1_set_mode(spirit1_data, SPIRIT1_STATE_READY);	//Me pongo en modo READY este donde este

	spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_FTX);	//Limpio las fifos
	spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_FRX);	//Limpio las fifos

	return RET_OK;
}

/**
 *
 * @param spirit1_data
 * @return
 */
retval_t spirit1_power_off(spirit1_data_t* spirit1_data){
	spirit1_arch_disable_interrupt(spirit1_data);
	return spirit1_arch_power_off(spirit1_data);
}

/**
 *
 * @param spirit1_data
 * @return
 */
retval_t spirit1_power_on(spirit1_data_t* spirit1_data){
	return spirit1_arch_power_on(spirit1_data);
}

/**
 *
 * @param spirit1_data
 * @return
 */
retval_t spirit1_check_device_info_reg(spirit1_data_t* spirit1_data){
	uint16_t tmp_reg_val;

	if(spirit1_data == NULL){
		return RET_ERROR;
	}

	spirit1_arch_read_multi_reg(spirit1_data, DEVICE_INFO1_PARTNUM, (uint8_t*) &tmp_reg_val, 2);

	if(tmp_reg_val != 0x3001){
		return RET_ERROR;
	}

	return RET_OK;
}

/**
 *
 * @param spirit1_data
 * @return
 */
float32_t spirit1_check_channel_rssi(spirit1_data_t* spirit1_data){
	if(spirit1_data == NULL){
		return 999;
	}
	if(spirit1_data->transmitting_packet || spirit1_data->receiving_packet || spirit1_data->checking_rssi){
		return 999;
	}
	float32_t val;
	spirit1_data->checking_rssi = 1;

	if(spirit1_set_mode(spirit1_data, SPIRIT1_STATE_RX) != RET_OK){
		spirit1_data->checking_rssi = 0;
		return 999;
	}

	spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_SABORT);

	spirit1_data->checking_rssi = 0;


	val = SpiritQiGetRssidBm(spirit1_data);

	return val;
}

/**
 *
 * @param spirit1_data
 * @return
 */
float32_t spirit1_get_last_rssi(spirit1_data_t* spirit1_data){
	if(spirit1_data == NULL){
		return 99;
	}
	return SpiritQiGetRssidBm(spirit1_data);
}

/**
 *
 * @param spirit1_data
 * @param data
 * @param key
 * @param size
 * @return
 */
retval_t spirit1_aes_encrypt_data(spirit1_data_t* spirit1_data, uint8_t* data, uint8_t* key, uint16_t size){
	uint16_t i;
	uint16_t num_conv;
	uint16_t processed_bytes = 0;
	if(spirit1_data == NULL){
		return RET_ERROR;
	}
	num_conv = size/16;

	spirit1_arch_enable_interrupt(spirit1_data);
	SpiritAesMode(spirit1_data, S_ENABLE);

	if(size < 16){
		SpiritAesWriteKey(spirit1_data, key);
		SpiritAesWriteDataIn(spirit1_data, data, size);
		spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_AES_ENC);
		if(ytSemaphoreWait(spirit1_data->spirit1_aes_semph_id, AES_TIMEOUT) != RET_OK){
			SpiritAesMode(spirit1_data, S_DISABLE);
			return RET_ERROR;
		}
		SpiritAesReadDataOut(spirit1_data, data, size);
		processed_bytes += size;
	}

	else{
		for(i=0; i<num_conv; i++){
			SpiritAesWriteKey(spirit1_data, key);
			SpiritAesWriteDataIn(spirit1_data, &data[i*16], 16);
			spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_AES_ENC);
			if(ytSemaphoreWait(spirit1_data->spirit1_aes_semph_id, AES_TIMEOUT) != RET_OK){
				SpiritAesMode(spirit1_data, S_DISABLE);
				return RET_ERROR;
			}
			SpiritAesReadDataOut(spirit1_data, &data[i*16], 16);
			processed_bytes += 16;
		}

		if(size-processed_bytes > 0){
			SpiritAesWriteKey(spirit1_data, key);
			SpiritAesWriteDataIn(spirit1_data, &data[i*16], (size-processed_bytes));
			spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_AES_ENC);
			if(ytSemaphoreWait(spirit1_data->spirit1_aes_semph_id, AES_TIMEOUT) != RET_OK){
				SpiritAesMode(spirit1_data, S_DISABLE);
				return RET_ERROR;
			}
			SpiritAesReadDataOut(spirit1_data, &data[i*16], (size-processed_bytes));
			processed_bytes += (size-processed_bytes);
		}
	}

	SpiritAesMode(spirit1_data, S_DISABLE);
	spirit1_arch_disable_interrupt(spirit1_data);
	return RET_OK;
}

/**
 *
 * @param spirit1_data
 * @param data
 * @param key
 * @param size
 * @return
 */
retval_t spirit1_aes_decrypt_data(spirit1_data_t* spirit1_data, uint8_t* data, uint8_t* key, uint16_t size){
	uint16_t i;
	uint16_t num_conv;
	uint16_t processed_bytes = 0;
	if(spirit1_data == NULL){
		return RET_ERROR;
	}
	num_conv = size/16;

	spirit1_arch_enable_interrupt(spirit1_data);
	SpiritAesMode(spirit1_data, S_ENABLE);

	if(size < 16){
		SpiritAesWriteKey(spirit1_data, key);
		SpiritAesWriteDataIn(spirit1_data, data, size);
		spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_AES_KEYDESC);
		if(ytSemaphoreWait(spirit1_data->spirit1_aes_semph_id, AES_TIMEOUT) != RET_OK){
			SpiritAesMode(spirit1_data, S_DISABLE);
			return RET_ERROR;
		}
		SpiritAesReadDataOut(spirit1_data, data, size);
		processed_bytes += size;
	}

	else{
		for(i=0; i<num_conv; i++){
			SpiritAesWriteKey(spirit1_data, key);
			SpiritAesWriteDataIn(spirit1_data, &data[i*16], 16);
			spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_AES_KEYDESC);
			if(ytSemaphoreWait(spirit1_data->spirit1_aes_semph_id, AES_TIMEOUT) != RET_OK){
				SpiritAesMode(spirit1_data, S_DISABLE);
				return RET_ERROR;
			}
			SpiritAesReadDataOut(spirit1_data, &data[i*16], 16);
			processed_bytes += 16;
		}

		if(size-processed_bytes > 0){
			SpiritAesWriteKey(spirit1_data, key);
			SpiritAesWriteDataIn(spirit1_data, &data[i*16], (size-processed_bytes));
			spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_AES_KEYDESC);
			if(ytSemaphoreWait(spirit1_data->spirit1_aes_semph_id, AES_TIMEOUT) != RET_OK){
				SpiritAesMode(spirit1_data, S_DISABLE);
				return RET_ERROR;
			}
			SpiritAesReadDataOut(spirit1_data, &data[i*16], (size-processed_bytes));
			processed_bytes += (size-processed_bytes);
		}
	}

	SpiritAesMode(spirit1_data, S_DISABLE);
	spirit1_arch_disable_interrupt(spirit1_data);
	return RET_OK;
}

//Esta rutina es llamada al saltar una interrupcion del spirit. OJO! No es llamada por la interrupcion sino por una hebra. Es decir por la capa phy
//Si se da el caso de que la interrupcion recibe un paquete, este packete se guarda en la variable pasada por parametro
//Devuelve distinto de 0 si ha recibido un packete
/**
 *
 * @param spirit1_data
 * @param packet_data
 * @param size
 * @return
 */
uint16_t spirit1_irq_routine(spirit1_data_t* spirit1_data){

	SpiritIrqs x_irq_status;
	uint16_t ret = 0;
	uint8_t fifo_elements;
	SpiritIrqGetStatus(spirit1_data, &x_irq_status);

	/* AES operation DONE */
	if(x_irq_status.IRQ_AES_END) {
		ytSemaphoreRelease(spirit1_data->spirit1_aes_semph_id);
	}

	/* The IRQ_VALID_SYNC is used to notify a new packet is coming */
	if(x_irq_status.IRQ_VALID_SYNC) {
		PRINTF("SPIRIT1: IRQ VALID SYNC\r\n");
		if((!spirit1_data->transmitting_packet) && (!spirit1_data->checking_rssi)){
			ytTimerStart(spirit1_data->spirit1_rx_timeout_timer, SPIRIT1_RCV_TIMEOUT);
			spirit1_data->receiving_packet = 1;
		}
	}

	/* The IRQ_TX_DATA_SENT notifies the packet received. Puts the SPIRIT1 in RX */
	if(x_irq_status.IRQ_TX_DATA_SENT) {
		PRINTF("SPIRIT1: IRQ DATA SENT\r\n");
		if(spirit1_data->transmitting_packet){
			ytTimerStop(spirit1_data->spirit1_tx_timeout_timer);
			spirit1_data->transmitting_packet = 0;
			ret |= RET_PCKT_SENT;
		}
	}

	/* The IRQ_RX_DATA_READY notifies a new packet arrived */
	if(x_irq_status.IRQ_RX_DATA_READY) {
		PRINTF("SPIRIT1: IRQ DATA RECEIVED\r\n");
		if((!spirit1_data->receiving_packet)  && (spirit1_data->checking_rssi)){
		   spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_FRX);
		}
		else{
			fifo_elements = SpiritLinearFifoReadNumElementsRxFifo(spirit1_data);
			if( fifo_elements >= spirit1_data->packet_length){
				ret |= RET_PCKT_RCV;
				if(fifo_elements > spirit1_data->packet_length){
					spirit1_data->receiving_packet = 1;
				}
			}


		}
	}


	if(x_irq_status.IRQ_RX_FIFO_ERROR) {
		PRINTF("SPIRIT1: IRQ RX FIFO ERROR\r\n");
		if((ret & RET_PCKT_RCV) != RET_PCKT_RCV){
			spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_FRX);
			spirit1_data->receiving_packet = 0;
		}
	}

	if(x_irq_status.IRQ_TX_FIFO_ERROR) {
		PRINTF("SPIRIT1: IRQ TX FIFO ERROR\r\n");
		spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_FTX);
		spirit1_data->transmitting_packet = 0;
	}

	return ret;
}

/**
 *
 * @param spirit1_data
 * @param packet_data
 * @param size
 * @return
 */
retval_t spirit1_read_rcv_data(spirit1_data_t* spirit1_data, uint8_t* packet_data, uint16_t size){
	if(spirit1_data->packet_length != size){
		return RET_ERROR;
	}
	ytTimerStop(spirit1_data->spirit1_rx_timeout_timer);
	spirit1_arch_read_rxfifo(spirit1_data, packet_data, size);
	spirit1_data->receiving_packet = 0;
	return RET_OK;
}

/**
 *
 * @param spirit1_data
 * @param size
 * @return
 */
retval_t spirit1_flush_last_rcv_data(spirit1_data_t* spirit1_data, uint16_t size){
	uint8_t* null_read;
	if(spirit1_data->packet_length != size){
		return RET_ERROR;
	}
	null_read = (uint8_t*) ytMalloc(size);
	spirit1_arch_read_rxfifo(spirit1_data, null_read, size);
	spirit1_data->receiving_packet = 0;
	ytFree(null_read);
	return RET_OK;

}

static void spirit1_rx_timeout_cb(const void* args){
	spirit1_data_t* spirit1_data = (spirit1_data_t*) args;

	spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_FRX);
	spirit1_data->receiving_packet = 0;
}
static void spirit1_tx_timeout_cb(const void* args){
	spirit1_data_t* spirit1_data = (spirit1_data_t*) args;

	spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_FTX);
	spirit1_data->transmitting_packet = 0;
}

/**
 *
 * @param spirit1_data
 * @param state
 * @return
 */
static retval_t spirit1_set_mode(spirit1_data_t* spirit1_data, spirit1_state_t state){
	uint64_t t1;
	spirit1_status_t status_b;
	if(spirit1_data == NULL){
		return RET_ERROR;
	}
	switch(state){
	case SPIRIT1_STATE_STANDBY:
		status_b = spirit1_arch_get_status(spirit1_data);

		if(status_b.SPIRIT1_STATE != SPIRIT1_STATE_STANDBY){
			spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_SABORT);
			spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_READY);
			spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_STANDBY);

			status_b = spirit1_arch_get_status(spirit1_data);
			t1 = ytTimeGetTimestamp();
			while(status_b.SPIRIT1_STATE != state){

				spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_READY);
				spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_STANDBY);

				if((ytTimeGetTimestamp()-t1) > SPIRIT1_FUNCS_TIMEOUT){
					return RET_ERROR;
				}

	//			ytDelay(1);
				status_b = spirit1_arch_get_status(spirit1_data);
			}
		}
		break;
	case SPIRIT1_STATE_SLEEP:
		status_b = spirit1_arch_get_status(spirit1_data);

		if(status_b.SPIRIT1_STATE != SPIRIT1_STATE_SLEEP){
			spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_SABORT);
			spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_READY);
			spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_SLEEP);

			status_b = spirit1_arch_get_status(spirit1_data);
			t1 = ytTimeGetTimestamp();
			while(status_b.SPIRIT1_STATE != state){

				spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_READY);
				spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_SLEEP);

				if((ytTimeGetTimestamp()-t1) > SPIRIT1_FUNCS_TIMEOUT){
					return RET_ERROR;
				}

	//			ytDelay(1);
				status_b = spirit1_arch_get_status(spirit1_data);
			}
		}
		break;
	case SPIRIT1_STATE_READY:
		status_b = spirit1_arch_get_status(spirit1_data);

		if(status_b.SPIRIT1_STATE != SPIRIT1_STATE_READY){
			spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_SABORT);
			spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_READY);

			status_b = spirit1_arch_get_status(spirit1_data);
			t1 = ytTimeGetTimestamp();
			while(status_b.SPIRIT1_STATE != state){

				spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_SABORT);
				spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_READY);

				if((ytTimeGetTimestamp()-t1) > SPIRIT1_FUNCS_TIMEOUT){
					return RET_ERROR;
				}

	//			ytDelay(1);
				status_b = spirit1_arch_get_status(spirit1_data);
			}
		}
		break;
	case SPIRIT1_STATE_RX:
		status_b = spirit1_arch_get_status(spirit1_data);

		if(status_b.SPIRIT1_STATE != SPIRIT1_STATE_RX){
			spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_SABORT);
			spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_READY);

			status_b = spirit1_arch_get_status(spirit1_data);
			t1 = ytTimeGetTimestamp();
			while(status_b.SPIRIT1_STATE != SPIRIT1_STATE_READY){

				spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_READY);

				if((ytTimeGetTimestamp()-t1) > SPIRIT1_FUNCS_TIMEOUT){
					return RET_ERROR;
				}

	//			ytDelay(1);
				status_b = spirit1_arch_get_status(spirit1_data);
			}

			spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_RX);

			status_b = spirit1_arch_get_status(spirit1_data);
			t1 = ytTimeGetTimestamp();
			while(status_b.SPIRIT1_STATE != state){

				spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_READY);
				spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_RX);

				if((ytTimeGetTimestamp()-t1) > SPIRIT1_FUNCS_TIMEOUT){
					return RET_ERROR;
				}

	//			ytDelay(1);
				status_b = spirit1_arch_get_status(spirit1_data);
			}
		}
		break;
	case SPIRIT1_STATE_TX:
		status_b = spirit1_arch_get_status(spirit1_data);

		if(status_b.SPIRIT1_STATE != SPIRIT1_STATE_TX){
			spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_SABORT);
			spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_READY);

			status_b = spirit1_arch_get_status(spirit1_data);
			t1 = ytTimeGetTimestamp();
			while(status_b.SPIRIT1_STATE != SPIRIT1_STATE_READY){

				spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_READY);

				if((ytTimeGetTimestamp()-t1) > SPIRIT1_FUNCS_TIMEOUT){
					return RET_ERROR;
				}

	//			ytDelay(1);
				status_b = spirit1_arch_get_status(spirit1_data);
			}

			spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_TX);

			status_b = spirit1_arch_get_status(spirit1_data);
			t1 = ytTimeGetTimestamp();
			while(status_b.SPIRIT1_STATE != state){

				spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_READY);
				spirit1_arch_send_cmd_strobe(spirit1_data, SPIRIT1_STROBE_TX);

				if((ytTimeGetTimestamp()-t1) > SPIRIT1_FUNCS_TIMEOUT){
					return RET_ERROR;
				}

	//			ytDelay(1);
				status_b = spirit1_arch_get_status(spirit1_data);
			}
		}
		break;
	default:
		return RET_ERROR;
		break;
	}

	return RET_OK;

}
