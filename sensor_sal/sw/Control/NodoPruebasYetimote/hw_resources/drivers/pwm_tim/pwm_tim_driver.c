/*
 * pwm_tim_driver.c
 *
 *  Created on: 27 feb. 2018
 *      Author: miguelvp
 */

/**
 * @file pwm_tim_driver.c
 */

#include "pwm_tim_driver.h"
#include "pwm_tim_arch.h"
#include "device.h"
#include "platform-conf.h"
#include "system_api.h"

#define DEBUG 1
#if DEBUG
#include "yetimote_stdio.h"
#define PRINTF(...) _printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

#if ENABLE_PWM_TIM_DRIVER

#define PWM_TIM_DEVICE_NAME PWM_TIM_DEV

/* Device Init functions */
static retval_t pwm_tim_init(void);
static retval_t pwm_tim_exit(void);

/* Local variables */
static uint32_t pwm_tim_mutex_id;
static pwm_tim_data_t* pwm_tim_data = NULL;

/* Device driver operation functions declaration */
static retval_t pwm_tim_open(device_t* device, dev_file_t* filep);
static retval_t pwm_tim_close(dev_file_t* filep);
static size_t pwm_tim_read(dev_file_t* filep, uint8_t* prx, size_t size);
static size_t pwm_tim_write(dev_file_t* filep, uint8_t* ptx, size_t size);
static retval_t pwm_tim_ioctl(dev_file_t* filep, uint16_t request, void* args);

/* Define driver operations */
static driver_ops_t pwm_tim_driver_ops = {
		.open = pwm_tim_open,
		.close = pwm_tim_close,
		.read = pwm_tim_read,
		.write = pwm_tim_write,
		.ioctl = pwm_tim_ioctl,
};


/**
 *
 * @return Code with error or success
 */
static retval_t pwm_tim_init(void)
{
	if((pwm_tim_data = new_pwm_tim_data()) == NULL){
		return RET_ERROR;
	}

	if(PWM_TIM_init(pwm_tim_data) != RET_OK){
		delete_pwm_tim_data(pwm_tim_data);
		pwm_tim_data = NULL;
		return RET_ERROR;
	}

	if(registerDevice(PWM_TIM_DEVICE_ID, &pwm_tim_driver_ops, PWM_TIM_DEVICE_NAME) != RET_OK){
		return RET_ERROR;
	}

	pwm_tim_mutex_id = ytMutexCreate();
	PRINTF(">PWM_TIM Init Done\r\n");

	return RET_OK;
}

/**
 *
 * @return Code with error or success
 */
static retval_t pwm_tim_exit(void)
{
	if(PWM_TIM_deinit(pwm_tim_data) != RET_OK)
		return RET_ERROR;

	// Mutex for other access
	ytMutexWait(pwm_tim_mutex_id, YT_WAIT_FOREVER);

	delete_pwm_tim_data(pwm_tim_data);
	unregisterDevice(PWM_TIM_DEVICE_ID, PWM_TIM_DEVICE_NAME);
	ytMutexRelease(pwm_tim_mutex_id);
	ytMutexDelete(pwm_tim_mutex_id);
	pwm_tim_data = NULL;

	PRINTF(">PWM_TIM Exit Done\r\n");

	return RET_OK;
}

/**
 *
 * @param device
 * @param filep
 * @return Code with error or success
 */
static retval_t pwm_tim_open(device_t* device, dev_file_t* filep)
{
	if(device->device_state != DEV_STATE_INIT){
		return RET_ERROR;
	}

	if(pwm_tim_data == NULL){
		return RET_ERROR;
	}

	return RET_OK;
}

/**
 *
 * @param filep
 * @return Code with error or success
 */
static retval_t pwm_tim_close(dev_file_t* filep)
{
	if((pwm_tim_data == NULL) || (filep == NULL)){
		return RET_ERROR;
	}

	return RET_OK;
}

/**
 * Not use
 * @param filep
 * @param prx
 * @param size
 * @return
 */
static size_t pwm_tim_read(dev_file_t* filep, uint8_t* prx, size_t size)
{
	return 0;
}

/**
 * Not use
 * @param filep
 * @param ptx
 * @param size
 * @return
 */
static size_t pwm_tim_write(dev_file_t* filep, uint8_t* ptx, size_t size)
{
	return 0;
}

/**
 * Every instruction should be a ioctl function
 * @param filep		Driver name
 * @param request	Instruction. It could be:
 * 						- PWM_TIM_NEW: creates a new PWM in the timer specified
 * 						- PWM_TIM_DELETE: deletes a PWM in the timer specified
 * 						- PWM_TIM_SEL_PWM: selects the PWM to use
 * 						- PWM_TIM_SET_FREQ: sets a specified frequency in the selected PWM
 * 						- PWM_TIM_SET_DUTY: sets a specified duty cycle in the selected PWM
 * 						- PWM_TIM_START: starts the PWM specified
 * 						- PWM_TIM_STOP: stops the PWM specified
 * @param args		Parameter to render in function to the request option. It could be a:
 *  					- void* number: A number generated with makeFixPoint macro that allows to use a 3 decimal precision. Used in PWM_TIM_SET_FREQ and PWM_TIM_SET_DUTY
 *  					- void* timer: A timer channel specified in the pwm_tim_driver.h file. Used in other ones
 * @return Code with error or success
 */
static retval_t pwm_tim_ioctl(dev_file_t* filep, uint16_t request, void* args)
{
	pwm_tim_driver_ioctl_cmd_t cmd;
	pwm_tim_driver_channel_t channel;
	uint8_t timer, gpio, pin, canal;
	fix_point_t freq, duty;
	int tmp;
	retval_t devolver;

	if ((pwm_tim_data == NULL) || (filep == NULL)){
		return RET_ERROR;
	}

	ytMutexWait(pwm_tim_mutex_id, YT_WAIT_FOREVER);
	cmd = (pwm_tim_driver_ioctl_cmd_t) request;

	// Recovery args values to use
	channel = (pwm_tim_driver_channel_t) args;
	timer = (uint8_t) (channel & 0xF);
	gpio = (uint8_t) ((channel >> 12) & 0xF);
	pin = (uint8_t) ((channel >> 8) & 0xF);
	canal = (uint8_t) ((channel >> 4) & 0xF);

	switch(cmd){
	case PWM_TIM_NEW:
		devolver = PWM_TIM_NewChannel(pwm_tim_data, timer, gpio, pin, canal);
		break;

	case PWM_TIM_DELETE:
		devolver = PWM_TIM_EraseChannel(pwm_tim_data, timer, gpio, pin, canal);
		break;

	case PWM_TIM_SEL_PWM:
		devolver = PWM_TIM_SelPWM(pwm_tim_data, timer, gpio, pin, canal);
		break;

	case PWM_TIM_SET_FREQ:
		tmp = (int) args;
		freq.h_fix = tmp/1000;
		freq.l_fix = tmp%1000;
		devolver = PWM_TIM_SetFrequency(pwm_tim_data, freq);
		break;

	case PWM_TIM_SET_DUTY:
		tmp = (int) args;
		duty.h_fix = tmp/1000;
		duty.l_fix = tmp%1000;
		devolver = PWM_TIM_SetDuty(pwm_tim_data, duty);
		break;

	case PWM_TIM_START:
		devolver = PWM_TIM_StartChannel(pwm_tim_data, timer, gpio, pin, canal);
		break;

	case PWM_TIM_STOP:
		devolver = PWM_TIM_StopChannel(pwm_tim_data, timer, gpio, pin, canal);
		break;

	case PWM_TIM_STOP_UP:
		devolver = PWM_TIM_StopUpChannel(pwm_tim_data, timer, gpio, pin, canal);
		break;

	case PWM_TIM_STOP_DOWN:
		devolver = PWM_TIM_StopDownChannel(pwm_tim_data, timer, gpio, pin, canal);
		break;

	default:
		devolver = RET_ERROR;
	}

	ytMutexRelease(pwm_tim_mutex_id);

	return devolver;
}

init_device_1(pwm_tim_init);	// Necesita el driver de GPIO
exit_device_1(pwm_tim_exit);

#endif
