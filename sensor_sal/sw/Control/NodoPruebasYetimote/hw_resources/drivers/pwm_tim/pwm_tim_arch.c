/*
 * pwm_tim_arch.c
 *
 *  Created on: 27 feb. 2018
 *      Author: miguelvp
 */

#include "pwm_tim_arch.h"

/* Private Prototypes */
static void encenderTIM(void* TIMx, int tipo);
static void apagarTIM(void* TIMx);
static retval_t makeChannel(channel_data_t* new_channel, uint8_t timer, uint8_t gpio, uint8_t pin, uint8_t canal, uint8_t* tipo, uint8_t* port, gpio_mode_t* mode);

/**
 * Initialize the list where timers will be saved
 *
 * @return Pointer to the list
 */
pwm_tim_data_t* new_pwm_tim_data(void)
{
	pwm_tim_data_t* new_pwm_tim_data = (pwm_tim_data_t*) ytMalloc(sizeof(pwm_tim_data_t));
	new_pwm_tim_data->duty.h_fix = 0;
	new_pwm_tim_data->duty.l_fix = 0;
	new_pwm_tim_data->Enable = HEAD;
	new_pwm_tim_data->frequency.h_fix = 0;
	new_pwm_tim_data->frequency.l_fix = 0;
	new_pwm_tim_data->channel_data = NULL;
	new_pwm_tim_data->type = 0;					// Indicates the timer selected in the header (0)
	new_pwm_tim_data->next = NULL;
	new_pwm_tim_data->changeMode = 0;

	return new_pwm_tim_data;
}

/**
 * Deletes all timers saved in the list
 *
 * @param pwm_tim_data List with the saved timers
 * @return Code with error or success
 */
retval_t delete_pwm_tim_data(pwm_tim_data_t* pwm_tim_data)
{
	pwm_tim_data_t* actual, *previo;

	if(pwm_tim_data == NULL){
		return RET_ERROR;
	}

	// Look for in the list the timer to delete and update the positions
	while(pwm_tim_data->next != NULL){
		previo = pwm_tim_data;
		actual = pwm_tim_data->next;
		while(actual->next != NULL){
			previo = actual;
			actual = actual->next;
		}
		ytFree(actual->channel_data);
		ytFree(actual);
		previo->next = NULL;
	}

	ytFree(pwm_tim_data);
	return RET_OK;
}

/**
 * Check if the driver is loaded
 *
 * @param pwm_tim_data List with the saved timers
 * @return Code with error or success
 */
retval_t PWM_TIM_init(pwm_tim_data_t* pwm_tim_data)
{
	if(pwm_tim_data == NULL){
		return RET_ERROR;
	}

	return RET_OK;
}

/**
 * Check if the driver is loaded
 *
 * @param pwm_tim_data List with the saved timers
 * @return Code with error or success
 */
retval_t PWM_TIM_deinit(pwm_tim_data_t* pwm_tim_data)
{
	if(pwm_tim_data == NULL){
		return RET_ERROR;
	}

	return RET_OK;
}

/**
 * Create a new timer with its initialization and saved it in the list with other timers
 *
 * @param pwm_tim_data List with the saved timers
 * @param timer Indicates the timer to use
 * @param gpio Selects the gpio use in the internal multiplexor
 * @param pin Selects the pin corresponding to the gpio to use
 * @param canal Selects the channel used in the timer that provides the output to gpio-pin couple
 * @return Code with error or success
 */
retval_t PWM_TIM_NewChannel(pwm_tim_data_t* pwm_tim_data, uint8_t timer, uint8_t gpio, uint8_t pin, uint8_t canal)
{
	pwm_tim_data_t* actual;
	channel_data_t* new_channel;
	uint8_t existe, tipo, port;
	gpio_mode_t mode;
	uint8_t seleccion = 0;
	int mismo = 0;

	if((new_channel = (channel_data_t*) ytMalloc(sizeof(channel_data_t))) == NULL)
		return RET_ERROR;

	// Generate the timer structure
	if(makeChannel(new_channel, timer, gpio, pin, canal, &tipo, &port, &mode) == RET_ERROR){
		ytFree(new_channel);
		return RET_ERROR;
	}

	// Check if next position is empty and, if not, check if the same timer has been created yet. Also save the type of timer: Normal (1) or Low-Power (0)
	existe = 0;
	actual = pwm_tim_data;
	while(actual->next != NULL){
		seleccion++;
		actual = actual->next;

		if((actual->channel_data->GPIOx == new_channel->GPIOx) && (actual->channel_data->GPIO_Pin == new_channel->GPIO_Pin)){
			ytFree(new_channel);
			return RET_ERROR;
		}

		if(tipo){
			mismo = actual->channel_data->Timer.Timer == new_channel->Timer.Timer;
			if(mismo){
				existe = 1;
			}
		} else {
			mismo = actual->channel_data->Timer.LPTimer == new_channel->Timer.LPTimer;
			if(mismo){
				existe = 1;
			}
		}
	}

	// Enable GPIO related
	if(y_gpio_init_pin((gpio_pin_t)((1 << (((uint32_t)pin)+16)) | (port << 8)), mode, GPIO_PIN_NO_PULL) == RET_ERROR){
		ytFree(new_channel);
		return RET_ERROR;
	}

	if((actual->next = (pwm_tim_data_t*) ytMalloc(sizeof(pwm_tim_data_t))) == NULL){
		ytFree(new_channel);
		return RET_ERROR;
	}

	// Enable the peripheral related
	if(!existe){
		if(tipo) {
			encenderTIM(new_channel->Timer.Timer, tipo);
		} else {
			encenderTIM(new_channel->Timer.LPTimer, tipo);
		}
	}

	// Configure the register associated to the channel of timer
	if(tipo){
		new_channel->Timer.Timer->DIER &= ~((RESET_DIER << (canal%8)) | UPDATE_ENABLE);
		new_channel->Timer.Timer->DIER |= ((INTERRUPT_CH << (canal%8)) | UPDATE_ENABLE);
		new_channel->Timer.Timer->CR1 |= AUTORELOAD;
		if (canal/3 == 0){
			new_channel->Timer.Timer->CCMR1 &= ~(RESET_CCMR_CH << (8*((canal%8)-1)));
			new_channel->Timer.Timer->CCMR1 |= ((PWM_MODE | PRELOAD_ENABLE | FAST_ENABLE) << (8*((canal%8)-1)));
		} else {
			new_channel->Timer.Timer->CCMR2 &= ~(RESET_CCMR_CH << (8*((canal%8)-3)));
			new_channel->Timer.Timer->CCMR2 |= ((PWM_MODE | PRELOAD_ENABLE | FAST_ENABLE) << (8*((canal%8)-3)));
		}
	}

	// Save the new timer structure
	pwm_tim_data->type = ++seleccion;
	actual->next = (pwm_tim_data_t*) ytMalloc(sizeof(pwm_tim_data_t));
	actual = actual->next;
	actual->duty.h_fix = 0;
	actual->duty.l_fix = 0;
	actual->Enable = OFF;
	actual->frequency.h_fix = 0;
	actual->frequency.l_fix = 0;
	actual->channel_data = (channel_data_t*) ytMalloc(sizeof(channel_data_t));
	actual->channel_data->Timer = new_channel->Timer;
	actual->channel_data->Channel = new_channel->Channel;
	actual->channel_data->GPIO_Pin = new_channel->GPIO_Pin;
	actual->channel_data->GPIOx = new_channel->GPIOx;
	actual->type = tipo;
	actual->changeMode = 0;
	actual->next = NULL;

	ytFree(new_channel);

	return RET_OK;
}

/**
 * Delete a timer that belongs to the list
 *
 * @param pwm_tim_data List with the saved timers
 * @param timer Indicates the timer to use
 * @param gpio Selects the gpio use in the internal multiplexor
 * @param pin Selects the pin corresponding to the gpio to use
 * @param canal Selects the channel used in the timer that provides the output to gpio-pin couple
 * @return Code with error or success
 */
retval_t PWM_TIM_EraseChannel(pwm_tim_data_t* pwm_tim_data, uint8_t timer, uint8_t gpio, uint8_t pin, uint8_t canal)
{
	pwm_tim_data_t* actual, *previo;
	channel_data_t* quit_channel;
	uint8_t duplicado, tipo, port, difPin, posicion, i;
	gpio_mode_t mode;

	if((quit_channel = (channel_data_t*) ytMalloc(sizeof(channel_data_t))) == NULL)
		return RET_ERROR;

	// Generate the timer structure
	if(makeChannel(quit_channel, timer, gpio, pin, canal, &tipo, &port, &mode) == RET_ERROR){
		ytFree(quit_channel);
		return RET_ERROR;
	}

	// Check the position of the timer to delete
	duplicado = 0;
	posicion = 0;
	actual = pwm_tim_data;
	while(actual->next != NULL){
		previo = actual;
		actual = actual->next;
		posicion++;

		difPin = quit_channel->Channel != actual->channel_data->Channel
				|| quit_channel->GPIO_Pin != actual->channel_data->GPIO_Pin
				|| quit_channel->GPIOx != actual->channel_data->GPIOx;

		if(tipo){
			if(actual->channel_data->Timer.Timer == quit_channel->Timer.Timer){
				if(difPin)
					duplicado = 1;
				else
					break;
			}
		} else {
			if(actual->channel_data->Timer.LPTimer == quit_channel->Timer.LPTimer){
				if(difPin)
					duplicado = 1;
				else
					break;
			}
		}
	}

	// Free the pointer in the list
	actual = pwm_tim_data;
	for(i = 0; i < posicion; ++i){
		actual = actual->next;
		if(actual == NULL){
			ytFree(quit_channel);
			return RET_ERROR;
		}
	}

	// Disable the GPIO related
	if(gpio_deinit_pin((gpio_pin_t)((1 << (((uint32_t)pin)+16)) | (port << 8))) == RET_ERROR){
		ytFree(quit_channel);
		return RET_ERROR;
	}

	// Disable the peripheral related if there is no other channel being used by the timer
	if(!duplicado){
		if(tipo) {
			apagarTIM(quit_channel->Timer.Timer);
		} else {
			apagarTIM(quit_channel->Timer.LPTimer);
		}
	}

	// Update the list
	pwm_tim_data->type = 0;
	previo->next = actual->next;
	ytFree(actual->channel_data);
	ytFree(actual);

	return RET_OK;
}

/**
 * Select the timer which attend the following commands
 *
 * @param pwm_tim_data List with the saved timers
 * @param timer Indicates the timer to use
 * @param gpio Selects the gpio use in the internal multiplexor
 * @param pin Selects the pin corresponding to the gpio to use
 * @param canal Selects the channel used in the timer that provides the output to gpio-pin couple
 * @return Code with error or success
 */
retval_t PWM_TIM_SelPWM(pwm_tim_data_t* pwm_tim_data, uint8_t timer, uint8_t gpio, uint8_t pin, uint8_t canal)
{
	pwm_tim_data_t* actual;
	channel_data_t* new_channel;
	uint8_t tipo, port;
	gpio_mode_t mode;
	uint8_t seleccion = 0;
	int igual = 0;

	if((new_channel = (channel_data_t*) ytMalloc(sizeof(channel_data_t))) == NULL)
		return RET_ERROR;

	// Generate the timer structure
	if(makeChannel(new_channel, timer, gpio, pin, canal, &tipo, &port, &mode) == RET_ERROR){
		ytFree(new_channel);
		return RET_ERROR;
	}

	// Look for the timer and update the header
	actual = pwm_tim_data;
	while(actual->next != NULL){
		seleccion++;
		actual = actual->next;
		if(actual->type)
			igual = actual->channel_data->Timer.Timer == new_channel->Timer.Timer;
		else
			igual = actual->channel_data->Timer.LPTimer == new_channel->Timer.LPTimer;
		if( 	igual
				&& actual->channel_data->Channel == new_channel->Channel
				&& actual->channel_data->GPIO_Pin == new_channel->GPIO_Pin
				&& actual->channel_data->GPIOx == new_channel->GPIOx){
			pwm_tim_data->type = seleccion;
			ytFree(new_channel);
			return RET_OK;
		}
	}
	ytFree(new_channel);
	return RET_ERROR;
}

/**
 * Set the frequency of the last timer selected
 *
 * @param pwm_tim_data List with the saved timers
 * @param freq Frequency that will show the output configured. The value has to be passed with makeFixPoint(float) macro
 * @return Code with error or success
 */
retval_t PWM_TIM_SetFrequency(pwm_tim_data_t* pwm_tim_data, fix_point_t freq)
{
	pwm_tim_data_t* actual;
	uint8_t i;
	uint32_t presc = 0;
	uint32_t n_arr, duty, mod;
	uint32_t intCLK = 0;
	uint16_t paso;

	// Recovery the timer specified in header
	actual = pwm_tim_data;
	if(pwm_tim_data->type == 0)
		return RET_ERROR;
	for(i = 0; i < pwm_tim_data->type; i++)
		actual = actual->next;

	// Distinct between Normal and Low-Power due to their registers
	if(!actual->type){

		// Low-Power Timer (LPTimer)

		// Get the reference clock for the timer to use
		if(actual->channel_data->Timer.LPTimer == LPTIM1)
			intCLK = HAL_RCCEx_GetPeriphCLKFreq(RCC_PERIPHCLK_LPTIM1);
		else
			intCLK = HAL_RCCEx_GetPeriphCLKFreq(RCC_PERIPHCLK_LPTIM2);

		// Set the prescaler to ensure minimize the error committed
		presc = 1;
		while((intCLK/(1000*freq.h_fix+freq.l_fix))/presc != 0 && presc < 256)
				presc = presc << 1;

		// Counteract the excess in the shifting
		if(presc != 1)
			presc = presc >> 1;

		actual->channel_data->Timer.LPTimer->CFGR &= ~(LP_PRESC);
		switch(presc) {
		case 2:
			actual->channel_data->Timer.LPTimer->CFGR |= (LP_PRESC_2);
			break;
		case 4:
			actual->channel_data->Timer.LPTimer->CFGR |= (LP_PRESC_4);
			break;
		case 8:
			actual->channel_data->Timer.LPTimer->CFGR |= (LP_PRESC_8);
			break;
		case 16:
			actual->channel_data->Timer.LPTimer->CFGR |= (LP_PRESC_16);
			break;
		case 32:
			actual->channel_data->Timer.LPTimer->CFGR |= (LP_PRESC_32);
			break;
		case 64:
			actual->channel_data->Timer.LPTimer->CFGR |= (LP_PRESC_64);
			break;
		case 128:
			actual->channel_data->Timer.LPTimer->CFGR |= (LP_PRESC_128);
			break;
		}

		// Prepare the register values to restart the timer and maintain the duty. Difference between be below 1 Hz, and if we can calculate with more precision
		if(freq.h_fix == 0){
			n_arr = intCLK/presc*1000/freq.l_fix;
		} else if(intCLK/presc/(1000*freq.h_fix) == 0){
			n_arr = intCLK/presc/freq.h_fix;
			if((1000*intCLK/presc/n_arr - freq.h_fix*1000) > (freq.h_fix*1000 - 1000*intCLK/presc/(n_arr+1)))
				n_arr++;
		} else {
			n_arr = intCLK/(freq.h_fix*presc);
			while((((freq.h_fix*1000+freq.l_fix) - (intCLK/presc*1000/n_arr)) & ((uint32_t) 0x1 << 31)) == 0)
				n_arr--;
		}
		duty = (n_arr*(1000*actual->duty.h_fix+actual->duty.l_fix))/(100*1000);

		// Enable the timer to change the registers auto-reload and comparison
		actual->channel_data->Timer.LPTimer->CR &= ~(LP_RESET_CR);
		actual->channel_data->Timer.LPTimer->CR |= LP_ENABLE;

		// Clean register with flags associated with changes in other registers
		actual->channel_data->Timer.LPTimer->ICR = (LP_ARROK | LP_CMPOK);

		// Reset registers auto-reload and comparison
		actual->channel_data->Timer.LPTimer->ARR &= ~(LP_RESET_ARR);
		actual->channel_data->Timer.LPTimer->CMP &= ~(LP_RESET_CMP);
		// Wait until auto-reload register is written and clear flag
		while((actual->channel_data->Timer.LPTimer->ISR & LP_ARROK) == 0) {}
		actual->channel_data->Timer.LPTimer->ICR = LP_ARROK;

		// Write auto-reload value
		actual->channel_data->Timer.LPTimer->ARR |= n_arr;
		// Wait until auto-reload register is written and clear flag
		while((actual->channel_data->Timer.LPTimer->ISR & LP_ARROK) == 0) {}
		actual->channel_data->Timer.LPTimer->ICR = (LP_ARROK | LP_CMPOK);

		// Write comparison value
		actual->channel_data->Timer.LPTimer->CMP |= duty;
		// Wait until comparison register is written and clear flag
		while((actual->channel_data->Timer.LPTimer->ISR & LP_CMPOK) == 0) {}
		actual->channel_data->Timer.LPTimer->ICR = LP_CMPOK;

		// Disable timer
		actual->channel_data->Timer.LPTimer->CR &= ~(LP_RESET_CR);

	} else {

		// Normal Timer (Timer)

		// Get internal clock
		intCLK = HAL_RCC_GetSysClockFreq();

		// Set the prescaler to ensure minimize the error committed. Binary search
		paso = 0x4000U;
		presc = 0x8000U;
		while(paso != 0){
			if ((intCLK/(1000*freq.h_fix+freq.l_fix))/presc == 0){
				presc = presc - paso;
			} else {
				presc = presc + paso;
			}
			paso = paso >> 1;
		}

		// ????
		mod = ((1000*freq.h_fix+freq.l_fix)*(presc+1))/1000;
		if((intCLK)%(((1000*freq.h_fix+freq.l_fix)*presc)/1000) > (mod-(intCLK)%(((1000*freq.h_fix+freq.l_fix)*(presc+1))/1000)))
			presc++;
		if(presc != 0){
			presc--;
		}

		// Disable Counter
		actual->channel_data->Timer.Timer->CR1 &= ~(CEN);

		// Prepare the register values to restart the timer and maintain the duty. Difference between be below 1 Hz, and if we can calculate with more precision
		if(freq.h_fix == 0) {
			n_arr = intCLK/(presc+1)*1000/freq.l_fix;
		} else if((intCLK/(presc+1)/(1000*freq.h_fix)) == 0) {
			n_arr = intCLK/(presc+1)/freq.h_fix;
		} else {
			n_arr = intCLK/(((1000*freq.h_fix+freq.l_fix)*(presc+1))/1000);
		}
		duty = (n_arr*(1000*actual->duty.h_fix+actual->duty.l_fix))/(100*1000);

		// Set auto-reload value, update event and prescaler value
		actual->channel_data->Timer.Timer->ARR &= ~(RESET_ARR);
		actual->channel_data->Timer.Timer->ARR |= n_arr;
		actual->channel_data->Timer.Timer->EGR |= (UG);
		actual->channel_data->Timer.Timer->PSC &= ~(RESET_PSC);
		actual->channel_data->Timer.Timer->PSC |= presc;

		// Change the register associated with the related channel of the timer
		switch (actual->channel_data->Channel%8){
		case 1:
			actual->channel_data->Timer.Timer->CCR1 &= ~(RESET_CCR);
			actual->channel_data->Timer.Timer->CCR1 |= duty;
			break;
		case 2:
			actual->channel_data->Timer.Timer->CCR2 &= ~(RESET_CCR);
			actual->channel_data->Timer.Timer->CCR2 |= duty;
			break;
		case 3:
			actual->channel_data->Timer.Timer->CCR3 &= ~(RESET_CCR);
			actual->channel_data->Timer.Timer->CCR3 |= duty;
			break;
		case 4:
			actual->channel_data->Timer.Timer->CCR4 &= ~(RESET_CCR);
			actual->channel_data->Timer.Timer->CCR4 |= duty;
			break;
		}

		actual->channel_data->Timer.Timer->EGR |= (UG);

		// Disable output from channel related
		if(actual->channel_data->Channel/8){
			actual->channel_data->Timer.Timer->CCER &= ~(OUT_N_ENABLE << (4*((actual->channel_data->Channel%8)-1)));
		} else {
			actual->channel_data->Timer.Timer->CCER &= ~(OUT_ENABLE << (4*((actual->channel_data->Channel%8)-1)));
		}

		// Enable Counter
		actual->channel_data->Timer.Timer->CR1 |= (CEN);
	}

	// Update timer information in the list
	actual->frequency.h_fix = freq.h_fix;
	actual->frequency.l_fix = freq.l_fix;

	return RET_OK;
}

/**
 * Set the duty of the last timer selected
 *
 * @param pwm_tim_data List with the saved timers
 * @param duty Duty cycle that will show the output configured. The value has to be passed with makeFixPoint(float) macro
 * @return Code with error or success
 */
retval_t PWM_TIM_SetDuty(pwm_tim_data_t* pwm_tim_data, fix_point_t duty)
{
	pwm_tim_data_t* actual;
	uint8_t i;
	uint32_t n_duty, max;

	// Pick the timer selected
	actual = pwm_tim_data;
	if(pwm_tim_data->type == 0)
		return RET_ERROR;
	for(i = 0; i < pwm_tim_data->type; i++)
		actual = actual->next;

	if(!actual->type){

		// Low-Power Timer (LPTimer)

		// Set the duty getting the max value due to auto-reload register
		max = actual->channel_data->Timer.LPTimer->ARR;
		if(duty.h_fix > 100){
			duty.h_fix = 100;
			duty.l_fix = 0;
		}
		n_duty = max*(1000*duty.h_fix+duty.l_fix)/(100*1000);

		// Enable the timer to change the comparison register
		actual->channel_data->Timer.LPTimer->CR &= ~(LP_RESET_CR);
		actual->channel_data->Timer.LPTimer->CR |= LP_ENABLE;

		// Clear comparison register
		actual->channel_data->Timer.LPTimer->CMP &= ~(LP_RESET_CMP);
		// Wait until comparison register is written and clear flag
		while((actual->channel_data->Timer.LPTimer->ISR & LP_CMPOK) == 0) {}
		actual->channel_data->Timer.LPTimer->ICR = LP_CMPOK;

		// Write comparison value
		actual->channel_data->Timer.LPTimer->CMP |= n_duty;
		// Wait until comparison register is written and clear flag
		while((actual->channel_data->Timer.LPTimer->ISR & LP_CMPOK) == 0) {}
		actual->channel_data->Timer.LPTimer->ICR = LP_CMPOK;

		// Disable timer
		actual->channel_data->Timer.LPTimer->CR &= ~(LP_RESET_CR);
	} else {

		// Normal Timer

		// Disable Counter
		actual->channel_data->Timer.Timer->CR1 &= ~(CEN);

		// Set the duty getting the max value due to auto-reload register
		max = actual->channel_data->Timer.Timer->ARR;
		n_duty = max*(1000*duty.h_fix+duty.l_fix)/(100*1000);

		// Set the value in the related channel
		switch (actual->channel_data->Channel%8){
		case 1:
			actual->channel_data->Timer.Timer->CCR1 &= ~(RESET_CCR);
			actual->channel_data->Timer.Timer->CCR1 |= n_duty;
			break;
		case 2:
			actual->channel_data->Timer.Timer->CCR2 &= ~(RESET_CCR);
			actual->channel_data->Timer.Timer->CCR2 |= n_duty;
			break;
		case 3:
			actual->channel_data->Timer.Timer->CCR3 &= ~(RESET_CCR);
			actual->channel_data->Timer.Timer->CCR3 |= n_duty;
			break;
		case 4:
			actual->channel_data->Timer.Timer->CCR4 &= ~(RESET_CCR);
			actual->channel_data->Timer.Timer->CCR4 |= n_duty;
			break;
		}

		actual->channel_data->Timer.Timer->EGR |= (UG);

		// Disable output in the related channel
		if(actual->channel_data->Channel/8){
			actual->channel_data->Timer.Timer->CCER &= ~(OUT_N_ENABLE << (4*((actual->channel_data->Channel%8)-1)));
		} else {
			actual->channel_data->Timer.Timer->CCER &= ~(OUT_ENABLE << (4*((actual->channel_data->Channel%8)-1)));
		}

		// Enable Counter
		actual->channel_data->Timer.Timer->CR1 |= (CEN);
	}

	// Update timer information in the list
	actual->duty.h_fix = duty.h_fix;
	actual->duty.l_fix = duty.l_fix;

	return RET_OK;
}

/**
 * Enable the output of last timer selected
 *
 * @param pwm_tim_data List with the saved timers
 * @param timer Indicates the timer to use
 * @param gpio Selects the gpio use in the internal multiplexor
 * @param pin Selects the pin corresponding to the gpio to use
 * @param canal Selects the channel used in the timer that provides the output to gpio-pin couple
 * @return Code with error or success
 */
retval_t PWM_TIM_StartChannel(pwm_tim_data_t* pwm_tim_data, uint8_t timer, uint8_t gpio, uint8_t pin, uint8_t canal)
{
	pwm_tim_data_t* actual;
	channel_data_t* new_channel;
	uint8_t tipo, port;
	gpio_mode_t mode;
	int existe = 0;
	int igual = 0;

	if((new_channel = (channel_data_t*) ytMalloc(sizeof(channel_data_t))) == NULL)
		return RET_ERROR;

	// Generate the timer structure
	if(makeChannel(new_channel, timer, gpio, pin, canal, &tipo, &port, &mode) == RET_ERROR){
		ytFree(new_channel);
		return RET_ERROR;
	}

	// Look for the selected timer
	actual = pwm_tim_data;
	while((actual = actual->next) != NULL){
		if(actual->type)
			igual = actual->channel_data->Timer.Timer == new_channel->Timer.Timer;
		else
			igual = actual->channel_data->Timer.LPTimer == new_channel->Timer.LPTimer;

		if(!existe && igual
				&& actual->channel_data->Channel == new_channel->Channel
				&& actual->channel_data->GPIO_Pin == new_channel->GPIO_Pin
				&& actual->channel_data->GPIOx == new_channel->GPIOx){
			existe = 1;
			break;
		}
	}

	if(existe){

		if (actual->changeMode){
			if(gpio_deinit_pin((gpio_pin_t)((1 << (((uint32_t)pin)+16)) | (port << 8))) == RET_ERROR)
				return RET_ERROR;
			if(y_gpio_init_pin((gpio_pin_t)((1 << (((uint32_t)pin)+16)) | (port << 8)), mode, GPIO_PIN_NO_PULL) == RET_ERROR){
				return RET_ERROR;
			}
			actual->changeMode = 0;
		}

		if(!actual->type){

			// Low-Power Timer

			// Disable Timer and enable again in continuous mode
			actual->channel_data->Timer.LPTimer->CR &= ~(LP_RESET_CR);
			actual->channel_data->Timer.LPTimer->CR |= (LP_ENABLE);
			actual->channel_data->Timer.LPTimer->CR |= (LP_CNT);

		} else if(actual->channel_data->Channel/5 == 0) {

			// Normal Timer

			// Normal Channel enable
			actual->channel_data->Timer.Timer->CR1 &= (CEN);
			actual->channel_data->Timer.Timer->CCER &= ~(RESET_CCER_CH << (4 *(actual->channel_data->Channel-1)));
			actual->channel_data->Timer.Timer->CCER |= (OUT_ENABLE << (4 *(actual->channel_data->Channel-1)));
			actual->channel_data->Timer.Timer->CR1 |= (CEN);
		} else {

			// Negative Channel enable
			actual->channel_data->Timer.Timer->CR1 &= (CEN);
			actual->channel_data->Timer.Timer->CCER &= ~(RESET_CCER_CH_N << (4 *((actual->channel_data->Channel%8)-1)));
			actual->channel_data->Timer.Timer->CCER |= (OUT_N_ENABLE << (4 *((actual->channel_data->Channel%8)-1)));
			actual->channel_data->Timer.Timer->CR1 |= (CEN);
		}
	}

	ytFree(new_channel);
	return RET_OK;
}

/**
 * Disable the output of last timer selected
 *
 * @param pwm_tim_data List with the saved timers
 * @param timer Indicates the timer to use
 * @param gpio Selects the gpio use in the internal multiplexor
 * @param pin Selects the pin corresponding to the gpio to use
 * @param canal Selects the channel used in the timer that provides the output to gpio-pin couple
 * @return Code with error or success
 */
retval_t PWM_TIM_StopChannel(pwm_tim_data_t* pwm_tim_data, uint8_t timer, uint8_t gpio, uint8_t pin, uint8_t canal)
{
	pwm_tim_data_t* actual;
	channel_data_t* new_channel;
	uint8_t tipo, port;
	gpio_mode_t mode;
	int existe = 0;
	int igual = 0;

	if((new_channel = (channel_data_t*) ytMalloc(sizeof(channel_data_t))) == NULL)
		return RET_ERROR;

	// Generate the timer structure
	if(makeChannel(new_channel, timer, gpio, pin, canal, &tipo, &port, &mode) == RET_ERROR){
		ytFree(new_channel);
		return RET_ERROR;
	}

	// Look for the selected timer
	actual = pwm_tim_data;
	while((actual = actual->next) != NULL){
		if(actual->type)
			igual = actual->channel_data->Timer.Timer == new_channel->Timer.Timer;
		else
			igual = actual->channel_data->Timer.LPTimer == new_channel->Timer.LPTimer;

		if(!existe && igual
				&& actual->channel_data->Channel == new_channel->Channel
				&& actual->channel_data->GPIO_Pin == new_channel->GPIO_Pin
				&& actual->channel_data->GPIOx == new_channel->GPIOx){
			existe = 1;
			break;
		}
	}

	if(existe){
		if(!actual->type){

			// Low-Power Timer

			// Disable Timer
			actual->channel_data->Timer.LPTimer->CR &= ~(LP_RESET_CR);
		} else if(actual->channel_data->Channel/5 == 0) {

			// Normal Timer

			// Disable normal Channel
			actual->channel_data->Timer.Timer->CCER &= ~(RESET_CCER_CH << (4 *(actual->channel_data->Channel-1)));
		} else {

			// Disable negative Channel
			actual->channel_data->Timer.Timer->CCER &= ~(RESET_CCER_CH_N << (4 *((actual->channel_data->Channel%8)-1)));
		}
	}

	ytFree(new_channel);
	return RET_OK;
}

/**
 * Disable the output of last timer selected
 *
 * @param pwm_tim_data List with the saved timers
 * @param timer Indicates the timer to use
 * @param gpio Selects the gpio use in the internal multiplexor
 * @param pin Selects the pin corresponding to the gpio to use
 * @param canal Selects the channel used in the timer that provides the output to gpio-pin couple
 * @return Code with error or success
 */
retval_t PWM_TIM_StopUpChannel(pwm_tim_data_t* pwm_tim_data, uint8_t timer, uint8_t gpio, uint8_t pin, uint8_t canal)
{
	pwm_tim_data_t* actual;
	channel_data_t* new_channel;
	uint8_t tipo, port;
	gpio_mode_t mode;
	int existe = 0;
	int igual = 0;

	if((new_channel = (channel_data_t*) ytMalloc(sizeof(channel_data_t))) == NULL)
		return RET_ERROR;

	// Generate the timer structure
	if(makeChannel(new_channel, timer, gpio, pin, canal, &tipo, &port, &mode) == RET_ERROR){
		ytFree(new_channel);
		return RET_ERROR;
	}

	// Look for the selected timer
	actual = pwm_tim_data;
	while((actual = actual->next) != NULL){
		if(actual->type)
			igual = actual->channel_data->Timer.Timer == new_channel->Timer.Timer;
		else
			igual = actual->channel_data->Timer.LPTimer == new_channel->Timer.LPTimer;

		if(!existe && igual
				&& actual->channel_data->Channel == new_channel->Channel
				&& actual->channel_data->GPIO_Pin == new_channel->GPIO_Pin
				&& actual->channel_data->GPIOx == new_channel->GPIOx){
			existe = 1;
			break;
		}
	}

	if(existe){
		if(!actual->type){

			// Low-Power Timer

			// Disable Timer
			actual->channel_data->Timer.LPTimer->CR &= ~(LP_RESET_CR);
		} else if(actual->channel_data->Channel/5 == 0) {

			// Normal Timer

			// Disable normal Channel
			actual->channel_data->Timer.Timer->CCER &= ~(RESET_CCER_CH << (4 *(actual->channel_data->Channel-1)));
		} else {

			// Disable negative Channel
			actual->channel_data->Timer.Timer->CCER &= ~(RESET_CCER_CH_N << (4 *((actual->channel_data->Channel%8)-1)));
		}
		actual->changeMode = 1;
	}

	if(gpio_deinit_pin((gpio_pin_t)((1 << (((uint32_t)pin)+16)) | (port << 8))) == RET_ERROR)
		return RET_ERROR;
	if(y_gpio_init_pin((gpio_pin_t)((1 << (((uint32_t)pin)+16)) | (port << 8)), GPIO_PIN_OUTPUT_PUSH_PULL, GPIO_PIN_NO_PULL) == RET_ERROR)
		return RET_ERROR;
	if(gpio_pin_set((gpio_pin_t)((1 << (((uint32_t)pin)+16)) | (port << 8))) == RET_ERROR)
		return RET_ERROR;

	ytFree(new_channel);
	return RET_OK;
}

/**
 * Disable the output of last timer selected
 *
 * @param pwm_tim_data List with the saved timers
 * @param timer Indicates the timer to use
 * @param gpio Selects the gpio use in the internal multiplexor
 * @param pin Selects the pin corresponding to the gpio to use
 * @param canal Selects the channel used in the timer that provides the output to gpio-pin couple
 * @return Code with error or success
 */
retval_t PWM_TIM_StopDownChannel(pwm_tim_data_t* pwm_tim_data, uint8_t timer, uint8_t gpio, uint8_t pin, uint8_t canal)
{
	pwm_tim_data_t* actual;
	channel_data_t* new_channel;
	uint8_t tipo, port;
	gpio_mode_t mode;
	int existe = 0;
	int igual = 0;

	if((new_channel = (channel_data_t*) ytMalloc(sizeof(channel_data_t))) == NULL)
		return RET_ERROR;

	// Generate the timer structure
	if(makeChannel(new_channel, timer, gpio, pin, canal, &tipo, &port, &mode) == RET_ERROR){
		ytFree(new_channel);
		return RET_ERROR;
	}

	// Look for the selected timer
	actual = pwm_tim_data;
	while((actual = actual->next) != NULL){
		if(actual->type)
			igual = actual->channel_data->Timer.Timer == new_channel->Timer.Timer;
		else
			igual = actual->channel_data->Timer.LPTimer == new_channel->Timer.LPTimer;

		if(!existe && igual
				&& actual->channel_data->Channel == new_channel->Channel
				&& actual->channel_data->GPIO_Pin == new_channel->GPIO_Pin
				&& actual->channel_data->GPIOx == new_channel->GPIOx){
			existe = 1;
			break;
		}
	}

	if(existe){
		if(!actual->type){

			// Low-Power Timer

			// Disable Timer
			actual->channel_data->Timer.LPTimer->CR &= ~(LP_RESET_CR);
		} else if(actual->channel_data->Channel/5 == 0) {

			// Normal Timer

			// Disable normal Channel
			actual->channel_data->Timer.Timer->CCER &= ~(RESET_CCER_CH << (4 *(actual->channel_data->Channel-1)));
		} else {

			// Disable negative Channel
			actual->channel_data->Timer.Timer->CCER &= ~(RESET_CCER_CH_N << (4 *((actual->channel_data->Channel%8)-1)));
		}
		actual->changeMode = 1;
	}

	if(gpio_deinit_pin((gpio_pin_t)((1 << (((uint32_t)pin)+16)) | (port << 8))) == RET_ERROR)
		return RET_ERROR;
	if(y_gpio_init_pin((gpio_pin_t)((1 << (((uint32_t)pin)+16)) | (port << 8)), GPIO_PIN_OUTPUT_PUSH_PULL, GPIO_PIN_NO_PULL) == RET_ERROR)
		return RET_ERROR;
	if(gpio_pin_reset((gpio_pin_t)((1 << (((uint32_t)pin)+16)) | (port << 8))) == RET_ERROR)
		return RET_ERROR;

	ytFree(new_channel);
	return RET_OK;
}

/**
 * Internal function that enable the internal clock of the timer passed as parameter
 *
 * @param TIMx Timer to enable
 * @param tipo Could be used a Timer (1) or a Low-Power Timer (0)
 */
static void encenderTIM(void* TIMx, int tipo)
{
	Timer timer;

	if(TIMx == NULL)
		return;

	if(tipo)
		timer.Timer = (TIM_TypeDef*) TIMx;
	else
		timer.LPTimer = (LPTIM_TypeDef*) TIMx;

	// Enable peripheral
	if (TIMx == TIM1)
		__HAL_RCC_TIM1_CLK_ENABLE();

	else if (TIMx == TIM2)
		__HAL_RCC_TIM2_CLK_ENABLE();

	else if (TIMx == TIM3)
		__HAL_RCC_TIM3_CLK_ENABLE();

	else if (TIMx == TIM4)
		__HAL_RCC_TIM4_CLK_ENABLE();

	else if (TIMx == TIM5)
		__HAL_RCC_TIM5_CLK_ENABLE();

	else if (TIMx == TIM8)
		__HAL_RCC_TIM8_CLK_ENABLE();

	else if (TIMx == TIM15)
		__HAL_RCC_TIM15_CLK_ENABLE();

	else if (TIMx == TIM16)
		__HAL_RCC_TIM16_CLK_ENABLE();

	else if (TIMx == TIM17)
		__HAL_RCC_TIM17_CLK_ENABLE();

	else if (TIMx == LPTIM1)
		__HAL_RCC_LPTIM1_CLK_ENABLE();

	else if (TIMx == LPTIM2)
		__HAL_RCC_LPTIM2_CLK_ENABLE();

	else
		return;

	if(tipo){

		// Normal Timer

		// Clear all control register
		timer.Timer->CR1 &= ~(RESET_CR1);

		// Set control register values with differences between some general-purpose timers
		if(TIMx == TIM15 || TIMx == TIM16 || TIMx == TIM17)
			timer.Timer->CR1 |= (AUTORELOAD | URS);
		else
			timer.Timer->CR1 |= (AUTORELOAD | DIR | URS);

		// Clear prescaler and auto-reload registers and enable main output in some timers
		timer.Timer->PSC &= ~(RESET_PSC);
		timer.Timer->ARR |= (RESET_ARR);
		if (TIMx == TIM1
				|| TIMx == TIM8
				|| TIMx == TIM15
				|| TIMx == TIM16
				|| TIMx == TIM17)
			timer.Timer->BDTR |= (MOE);
	} else {

		// Low-Power Timer

		// Configure option and configuration registers
		timer.LPTimer->OR &= ~(LP_RESET_OR);
		timer.LPTimer->CFGR &= ~(LP_RESET_CFGR);
		timer.LPTimer->CFGR |= (LP_PRELOAD | LP_WAVPOL | LP_POLARITY);
	}
}

/**
 * Internal function that disable the internal clock of the timer passed as parameter
 *
 * @param TIMx Timer to disable
 */
static void apagarTIM(void* TIMx)
{
	if(TIMx == NULL)
		return;

	// Disable peripheral
	if (TIMx == TIM1)
		__HAL_RCC_TIM1_CLK_DISABLE();

	else if (TIMx == TIM2)
		__HAL_RCC_TIM2_CLK_DISABLE();

	else if (TIMx == TIM3)
		__HAL_RCC_TIM3_CLK_DISABLE();

	else if (TIMx == TIM4)
		__HAL_RCC_TIM4_CLK_DISABLE();

	else if (TIMx == TIM5)
		__HAL_RCC_TIM5_CLK_DISABLE();

	else if (TIMx == TIM8)
		__HAL_RCC_TIM8_CLK_DISABLE();

	else if (TIMx == TIM15)
		__HAL_RCC_TIM15_CLK_DISABLE();

	else if (TIMx == TIM16)
		__HAL_RCC_TIM16_CLK_DISABLE();

	else if (TIMx == TIM17)
		__HAL_RCC_TIM17_CLK_DISABLE();

	else if (TIMx == LPTIM1)
		__HAL_RCC_LPTIM1_CLK_DISABLE();

	else if (TIMx == LPTIM2)
		__HAL_RCC_LPTIM2_CLK_DISABLE();

	else
		return;
}

/**
 * Internal function to generate the parameters of a new timer.
 *
 * @param new_channel Variable where the new timer will be saved
 * @param timer Indicates the timer to use
 * @param gpio Selects the gpio use in the internal multiplexor
 * @param pin Selects the pin corresponding to the gpio to use
 * @param canal Selects the channel used in the timer that provides the output to gpio-pin couple
 * @param tipo Could be used a Timer (1) or a Low-Power Timer (0)
 * @param port Variable that indicates the gpio to use
 * @param mode Variable that contains the mode to use in the gpio configuration
 * @return Code with error or success
 */
static retval_t makeChannel(channel_data_t* new_channel, uint8_t timer, uint8_t gpio, uint8_t pin, uint8_t canal, uint8_t* tipo, uint8_t* port, gpio_mode_t* mode)
{
	new_channel->Channel = canal;
	new_channel->GPIO_Pin = 1 << pin;

	switch(gpio){
	case 0xA:
		new_channel->GPIOx = GPIOA;
		*port = 0x0;
		break;
	case 0xB:
		new_channel->GPIOx = GPIOB;
		*port = 0x4;
		break;
	case 0xC:
		new_channel->GPIOx = GPIOC;
		*port = 0x8;
		break;
	default:
		return RET_ERROR;
	}

	*tipo = 1;
	switch(timer){
	case 1:
		new_channel->Timer.Timer = TIM1;
		*mode = GPIO_PIN_AF1_TIM1;
		break;
	case 2:
		new_channel->Timer.Timer = TIM2;
		*mode = GPIO_PIN_AF1_TIM2;
		break;
	case 3:
		new_channel->Timer.Timer = TIM3;
		*mode = GPIO_PIN_AF2_TIM3;
		break;
	case 4:
		new_channel->Timer.Timer = TIM4;
		*mode = GPIO_PIN_AF2_TIM4;
		break;
	case 5:
		new_channel->Timer.Timer = TIM5;
		*mode = GPIO_PIN_AF2_TIM5;
		break;
	case 8:
		new_channel->Timer.Timer = TIM8;
		*mode = GPIO_PIN_AF3_TIM8;
		break;
	case 15:
		new_channel->Timer.Timer = TIM15;
		*mode = GPIO_PIN_AF14_TIM15;
		break;
	case 6:
		new_channel->Timer.Timer = TIM16;
		*mode = GPIO_PIN_AF14_TIM16;
		break;
	case 7:
		new_channel->Timer.Timer = TIM17;
		*mode = GPIO_PIN_AF14_TIM17;
		break;
	case 10:
		new_channel->Timer.LPTimer = LPTIM1;
		*mode = GPIO_PIN_AF1_LPTIM1;
		*tipo = 0;
		break;
	case 11:
		new_channel->Timer.LPTimer = LPTIM2;
		*mode = GPIO_PIN_AF14_LPTIM2;
		*tipo = 0;
		break;
	default:
		return RET_ERROR;
	}

	return RET_OK;
}
