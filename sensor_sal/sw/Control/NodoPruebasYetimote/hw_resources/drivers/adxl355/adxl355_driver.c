/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * adxl355_driver.c
 *
 *  Created on: 8 de ene. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file adxl355_driver.c
 */


#include "adxl355_arch.h"
#include "device.h"
#include "platform-conf.h"
#include "spi_driver.h"
#include "system_api.h"
#include "arm_math.h"

#define DEBUG 1
#if DEBUG
#include "yetimote_stdio.h"
#define PRINTF(...) _printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

#if ENABLE_ADXL355_DRIVER
#include "adxl355_plaform.h"

#define ADXL355_FIFO_THREESHOLD	20

/* Device name and Id */
#define ADXL355_DEVICE_NAME ADXL355_DEV


/*Local variables*/
static uint16_t dev_opened = 0;
static adxl355_data_t* adxl355_data = NULL;

/* Device Init functions */
static retval_t adxl355_init(void);
static retval_t adxl355_exit(void);


/* Device driver operation functions declaration */
static retval_t adxl355_open(device_t* device, dev_file_t* filep);
static retval_t adxl355_close(dev_file_t* filep);
static size_t adxl355_read(dev_file_t* filep, uint8_t* prx, size_t size);
static size_t adxl355_write(dev_file_t* filep, uint8_t* ptx, size_t size);
static retval_t adxl355_ioctl(dev_file_t* filep, uint16_t request, void* args);


/* Define driver operations */
static driver_ops_t adxl355_driver_ops = {
	.open = adxl355_open,
	.close = adxl355_close,
	.read = adxl355_read,
	.write = adxl355_write,
	.ioctl = adxl355_ioctl,
};


/**
 *
 * @return
 */
static retval_t adxl355_init(void){


	if((adxl355_data = new_adxl355_data()) == NULL){
		return RET_ERROR;
	}
	if(ADXL355_init(adxl355_data, ADXL355_ISHTAR_CS_PIN, ADXL355_SPI_DEV) != RET_OK){
		delete_adxl355_data(adxl355_data);
		adxl355_data = NULL;
		return RET_ERROR;
	}


	if(registerDevice(ADXL355_DEVICE_ID, &adxl355_driver_ops, ADXL355_DEVICE_NAME) != RET_OK){
		ADXL355_deinit(adxl355_data);
		delete_adxl355_data(adxl355_data);
		adxl355_data = NULL;
		return RET_ERROR;
	}

	dev_opened = 0;

	PRINTF(">ADXL355 Init Done\r\n");

	return RET_OK;
}

/**
 *
 * @return
 */
static retval_t adxl355_exit(void){

	if(ADXL355_deinit(adxl355_data) != RET_OK){				//De-Inicialización del HW del spi 2
		return RET_ERROR;
	}

	delete_adxl355_data(adxl355_data);

	unregisterDevice(ADXL355_DEVICE_ID, ADXL355_DEVICE_NAME);

	dev_opened = 0;
	adxl355_data = NULL;

	PRINTF(">ADXL355 Exit Done\r\n");
	return RET_OK;
}

/**
 *
 * @param device
 * @param filep
 * @return
 */
static retval_t adxl355_open(device_t* device, dev_file_t* filep){
	if(device->device_state != DEV_STATE_INIT){
		return RET_ERROR;
	}
	if(adxl355_data == NULL){
		return RET_ERROR;
	}
	if(dev_opened){			//This driver can only be opened once
		return RET_ERROR;
	}
	dev_opened++;


	return RET_OK;
}

/**
 *
 * @param filep
 * @return
 */
static retval_t adxl355_close(dev_file_t* filep){
	if((adxl355_data == NULL) || (filep == NULL)){
		return RET_ERROR;
	}
	if(!dev_opened){
		return RET_ERROR;
	}
	dev_opened--;

	return RET_OK;
}

/**
 *
 * @param filep
 * @param ptx
 * @param size
 * @return
 */
static size_t adxl355_write(dev_file_t* filep, uint8_t* ptx, size_t size){
	return 0;
}


/**
 *
 * @param filep
 * @param prx
 * @param size
 * @return
 */
static size_t adxl355_read(dev_file_t* filep, uint8_t* prx, size_t size){	//La funcion lee un numero size de float64_t.
																			//Segun el numero de ejes configurados por cada size se leen uno, dos o tres ejes (representados en float64)
	AxesRaw_adxl_t readData;
	uint16_t num_to_read;
	uint16_t i, j;
	uint16_t readSamples = 0;
	uint16_t remaingSamples = size - readSamples;
	uint8_t fifo_samples;
    float64_t resolution = (float64_t) pow(2,((float64_t) adxl355_data->scale)+1-20);

	float64_t* out_data = (float64_t*) prx;

	if((adxl355_data == NULL) || (filep == NULL)){
		return RET_ERROR;
	}
	if(!dev_opened){
		return RET_ERROR;
	}

	if(size < 32){
		num_to_read = size;
	}
	else{
		num_to_read = ADXL355_FIFO_THREESHOLD;
	}

	while(readSamples < size){

		remaingSamples = size - readSamples;

		ADXL355_GetFifoStoredSamples(adxl355_data, &fifo_samples);

		if((remaingSamples) < num_to_read){							//In this case the remaining samples to be read are lower to the FIFO Threeshold. Active wait till get this samples
			while((fifo_samples/3) < remaingSamples){
				ytDelay(6);
				ADXL355_GetFifoStoredSamples(adxl355_data, &fifo_samples);			//Wait till the FIFO has the specified sample number
			}

			for(i=0; i<remaingSamples; i++){

				ADXL355_GetAccAxesRaw_FIFO_Mode(adxl355_data, &readData);

				j = 0;

				if(adxl355_data->axis & AXIS_X_ENABLE){
					out_data[readSamples+(size*j)] =	((float64_t) (((float64_t)readData.AXIS_X) * resolution))/4096;
					j++;
				}
				if(adxl355_data->axis & AXIS_Y_ENABLE){
					out_data[readSamples+(size*j)] =	((float64_t) (((float64_t)readData.AXIS_Y) * resolution))/4096;
					j++;
				}
				if(adxl355_data->axis & AXIS_Z_ENABLE){
					out_data[readSamples+(size*j)] =	((float64_t) (((float64_t)readData.AXIS_Z) * resolution))/4096;
					j++;
				}

				readSamples++;
			}
			break;
		}

		while((fifo_samples/3) < num_to_read){			//In case of fifo samples lower to threeshold wait poll for the FIFO_THREESHOLD level
			ytDelay(6);
			ADXL355_GetFifoStoredSamples(adxl355_data, &fifo_samples);
		}

		for(i=0; i<num_to_read; i++){

			ADXL355_GetAccAxesRaw_FIFO_Mode(adxl355_data, &readData);

			j = 0;

			if(adxl355_data->axis & AXIS_X_ENABLE){
				out_data[readSamples+(size*j)] =	((float64_t) (((float64_t)readData.AXIS_X) * resolution))/4096;
				j++;
			}
			if(adxl355_data->axis & AXIS_Y_ENABLE){
				out_data[readSamples+(size*j)] =	((float64_t) (((float64_t)readData.AXIS_Y) * resolution))/4096;
				j++;
			}
			if(adxl355_data->axis & AXIS_Z_ENABLE){
				out_data[readSamples+(size*j)] =	((float64_t) (((float64_t)readData.AXIS_Z) * resolution))/4096;
				j++;
			}

			readSamples++;
		}
	}

	return readSamples;

}


/**
 *
 * @param filep
 * @param request
 * @param args
 * @return
 */
static retval_t adxl355_ioctl(dev_file_t* filep, uint16_t request, void* args){

	adxl355_driver_ioctl_cmd_t cmd;

	if((adxl355_data == NULL) || (filep == NULL)){
		return RET_ERROR;
	}
	if(!dev_opened){
		return RET_ERROR;
	}

	cmd = (spi_driver_ioctl_cmd_t) request;

	switch(cmd){
	case ADXL355_SET_MODE:
		ADXL355_SetMode(adxl355_data, (ADXL355_Mode_t) args);
		break;

	case ADXL355_SET_ODR:
		ADXL355_SetODR(adxl355_data, (ADXL355_ODR_t) args);
		break;

	case ADXL355_SET_SCALE:
		ADXL355_SetFullScale(adxl355_data, (ADXL355_Fullscale_t) args);
		break;

	case ADXL355_SET_AXIS:
		ADXL355_SetAxis(adxl355_data, (uint32_t) args);
		break;

	default:
		return RET_ERROR;
		break;
	}

	return RET_OK;
}


/* Register the init functions in the kernel Init system */
init_device_1(adxl355_init);	//Level 1 Driver. It is initialized after Level 0 drivers do
exit_device_1(adxl355_exit);

#endif
