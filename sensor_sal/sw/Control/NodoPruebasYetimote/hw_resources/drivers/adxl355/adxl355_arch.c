/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * adxl355_arch.c
 *
 *  Created on: 8 de ene. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file adxl355_arch.c
 */

#include "adxl355_arch.h"
#include "platform-conf.h"

#define ADXL355_SPI_SPEED	6000000

#define READ_MASK	0x01
#define WRITE_MASK	0xFE

#define INIT_ODR	ADXL355_ODR_4000Hz
#define INIT_SCALE	ADXL355_FULLSCALE_2
#define INIT_MODE	ADXL355_POWER_DOWN
#define INIT_AXIS	(AXIS_X_ENABLE | AXIS_Y_ENABLE | AXIS_Z_ENABLE)	//All axis enabled by default


static retval_t ADXL355_ReadReg(adxl355_data_t* adxl355_data, uint8_t reg, uint8_t* data);
static retval_t ADXL355_WriteReg(adxl355_data_t* adxl355_data, uint8_t reg, uint8_t data);
static retval_t ADXL355_CheckDevice(adxl355_data_t* adxl355_data);
static retval_t ADXL355_config_spi_device(adxl355_data_t* adxl355_data,	gpioPin_t adxl355_csPin, char* spi_dev);

/**
 *
 * @return
 */
adxl355_data_t* new_adxl355_data(void){
	adxl355_data_t* new_adxl355_data = (adxl355_data_t*) ytMalloc(sizeof(adxl355_data_t));
	new_adxl355_data->spi_fd = 0;
	new_adxl355_data->mode = ADXL355_NO_INIT;
	new_adxl355_data->odr = ADXL355_ODR_NO_INIT;
	new_adxl355_data->scale = ADXL355_FULLSCALE_NO_INIT;

	return new_adxl355_data;
}

/**
 *
 * @param adxl_data
 * @return
 */
retval_t delete_adxl355_data(adxl355_data_t* adxl355_data){

	if(adxl355_data == NULL){
		return RET_ERROR;
	}

	ytFree(adxl355_data);
	return RET_OK;
}

/**
 *
 * @param adxl355_data
 * @return
 */
retval_t ADXL355_init(adxl355_data_t* adxl355_data, gpioPin_t adxl355_csPin, char* spi_dev){
	if(adxl355_data == NULL){
		return RET_ERROR;
	}
	if(ADXL355_config_spi_device(adxl355_data, adxl355_csPin, spi_dev) != RET_OK){
		return RET_ERROR;
	}

	if(ADXL355_CheckDevice(adxl355_data) != RET_OK){
		ytClose(adxl355_data->spi_fd);
		return RET_ERROR;
	}
	if(ADXL355_SetODR(adxl355_data, INIT_ODR) != RET_OK){
		ytClose(adxl355_data->spi_fd);
		return RET_ERROR;
	}
	if(ADXL355_SetFullScale(adxl355_data, INIT_SCALE) != RET_OK){
		ytClose(adxl355_data->spi_fd);
		return RET_ERROR;
	}
	if(ADXL355_SetAxis(adxl355_data, INIT_AXIS) != RET_OK){
		ytClose(adxl355_data->spi_fd);
		return RET_ERROR;
	}
	if(ADXL355_SetMode(adxl355_data, INIT_MODE) != RET_OK){
		ytClose(adxl355_data->spi_fd);
		return RET_ERROR;
	}

	return RET_OK;
}

/**
 *
 * @param adxl355_data
 * @return
 */
retval_t ADXL355_deinit(adxl355_data_t* adxl355_data){
	if(adxl355_data == NULL){
		return RET_ERROR;
	}

	if(ADXL355_SetMode(adxl355_data, ADXL355_POWER_DOWN) != RET_OK){
		return RET_ERROR;
	}
	adxl355_data->mode = ADXL355_NO_INIT;
	adxl355_data->odr = ADXL355_ODR_NO_INIT;
	adxl355_data->scale = ADXL355_FULLSCALE_NO_INIT;

	ytClose(adxl355_data->spi_fd);
	adxl355_data->spi_fd = 0;
	return RET_OK;
}

/**
 *
 * @param adxl355_data
 * @param odr
 * @return
 */
retval_t ADXL355_SetODR(adxl355_data_t* adxl355_data, ADXL355_ODR_t odr){
	uint8_t value;
	if(adxl355_data == NULL){
		return RET_ERROR;
	}
	if(adxl355_data->odr == odr){
	  return RET_ERROR;
	}
	if( ADXL355_ReadReg(adxl355_data, ADXL355_POWER_CTL, &value) != RET_OK){	//Set to standby to change odr
	return RET_ERROR;
	}

	value |= 0x03;

	if( ADXL355_WriteReg(adxl355_data, ADXL355_POWER_CTL, value) != RET_OK ){
	return RET_ERROR;
	}


	if( ADXL355_ReadReg(adxl355_data, ADXL355_FILTER, &value) != RET_OK ){	//ODR
	return RET_ERROR;
	}

	value &= 0xF0;
	value |= odr<<ADXL355_ODR_BIT;

	if( ADXL355_WriteReg(adxl355_data, ADXL355_FILTER, value) != RET_OK ){
	return RET_ERROR;
	}

	if( ADXL355_ReadReg(adxl355_data, ADXL355_POWER_CTL, &value) != RET_OK ){	//Set to standby to change range
	return RET_ERROR;
	}

	value &= 0xFE;			//Set to active mode

	if( ADXL355_WriteReg(adxl355_data, ADXL355_POWER_CTL, value) != RET_OK){
	return RET_ERROR;
	}
	adxl355_data->odr = odr;
	return RET_OK;
}


/**
 *
 * @param adxl355_data
 * @param md
 * @return
 */
retval_t ADXL355_SetMode(adxl355_data_t* adxl355_data, ADXL355_Mode_t md) {
	uint8_t value;
	if(adxl355_data == NULL){
		return RET_ERROR;
	}
	if(adxl355_data->mode == md){
	  return RET_ERROR;
	}

	if( ADXL355_ReadReg(adxl355_data, ADXL355_POWER_CTL, &value) != RET_OK){
	return RET_ERROR;
	}

	value &= 0xFE;
	value |= md<<ADXL355_STANDBY;

	if( ADXL355_WriteReg(adxl355_data, ADXL355_POWER_CTL, value) != RET_OK){
	return RET_ERROR;
	}
	adxl355_data->mode = md;
	return RET_OK;
}

/**
 *
 * @param adxl355_data
 * @param axis
 * @return
 */
retval_t ADXL355_SetAxis(adxl355_data_t* adxl355_data, uint8_t axis){
	if(adxl355_data == NULL){
		return RET_ERROR;
	}

	adxl355_data->axis = axis;
	return RET_OK;
}

/**
 *
 * @param adxl355_data
 * @param fs
 * @return
 */
retval_t ADXL355_SetFullScale(adxl355_data_t* adxl355_data, ADXL355_Fullscale_t fs) {
	uint8_t value;
	if(adxl355_data == NULL){
		return RET_ERROR;
	}
	if(adxl355_data->scale == fs){
	  return RET_ERROR;
	}
	if( ADXL355_ReadReg(adxl355_data, ADXL355_POWER_CTL, &value) != RET_OK){	//Set to standby to change range
	return RET_ERROR;
	}

	value |= 0x03;

	if( ADXL355_WriteReg(adxl355_data, ADXL355_POWER_CTL, value) != RET_OK ){
	return RET_ERROR;
	}


	if( ADXL355_ReadReg(adxl355_data, ADXL355_RANGE, &value) != RET_OK ){
	return RET_ERROR;
	}


	value &= 0xFC;
	value |= (fs<<ADXL355_RANGE_BIT);

	if( ADXL355_WriteReg(adxl355_data, ADXL355_RANGE, value) != RET_OK){
	return RET_ERROR;
	}


	if( ADXL355_ReadReg(adxl355_data, ADXL355_POWER_CTL, &value) != RET_OK ){	//Set to standby to change range
	return RET_ERROR;
	}

	value &= 0xFE;			//Set to active mode

	if( ADXL355_WriteReg(adxl355_data, ADXL355_POWER_CTL, value) != RET_OK ){
	return RET_ERROR;
	}
	adxl355_data->scale = fs;
	return RET_OK;
}


/**
 *
 * @param adxl355_data
 * @param buff
 * @return
 */
retval_t ADXL355_GetAccAxesRaw(adxl355_data_t* adxl355_data, AxesRaw_adxl_t* buff) {
	int32_t value = 0;
	uint8_t *valueL = (uint8_t *)(&value);
	uint8_t *valueM = ((uint8_t *)(&value)+1);
	uint8_t *valueH = ((uint8_t *)(&value)+2);
	if(adxl355_data == NULL){
		return RET_ERROR;
	}
	if( ADXL355_ReadReg(adxl355_data, ADXL355_OUT_X_L, valueL) != RET_OK ){
	return RET_ERROR;
	}

	if( ADXL355_ReadReg(adxl355_data, ADXL355_OUT_X_M, valueM) != RET_OK ){
	return RET_ERROR;
	}

	if( ADXL355_ReadReg(adxl355_data, ADXL355_OUT_X_H, valueH) != RET_OK ){
	return RET_ERROR;
	}

	buff->AXIS_X = (value<<8);		//Output data in 32 bit format instead 24 bits

	if( ADXL355_ReadReg(adxl355_data, ADXL355_OUT_Y_L, valueL) != RET_OK ){
	return RET_ERROR;
	}

	if( ADXL355_ReadReg(adxl355_data, ADXL355_OUT_Y_M, valueM) != RET_OK ){
	return RET_ERROR;
	}

	if( ADXL355_ReadReg(adxl355_data, ADXL355_OUT_Y_H, valueH) != RET_OK ){
	return RET_ERROR;
	}

	buff->AXIS_Y = (value<<8);

	if( ADXL355_ReadReg(adxl355_data, ADXL355_OUT_Z_L, valueL) != RET_OK ){
	return RET_ERROR;
	}

	if( ADXL355_ReadReg(adxl355_data, ADXL355_OUT_Z_M, valueM) != RET_OK){
	return RET_ERROR;
	}

	if( ADXL355_ReadReg(adxl355_data, ADXL355_OUT_Z_H, valueH) != RET_OK){
	return RET_ERROR;
	}

	buff->AXIS_Z = (value<<8);

	return RET_OK;
}

/**
 *
 * @param adxl355_data
 * @param buff
 * @return
 */
retval_t ADXL355_GetAccAxesRaw_FIFO_Mode(adxl355_data_t* adxl355_data, AxesRaw_adxl_t* buff) {
	spi_read_write_buffs_t read_write_buffs;
	int32_t value;
	uint8_t flags;
	uint8_t *valueL = (uint8_t *)(&value);
	uint8_t *valueM = ((uint8_t *)(&value)+1);
	uint8_t *valueH = ((uint8_t *)(&value)+2);
	if(adxl355_data == NULL){
		return RET_ERROR;
	}

	uint8_t* tx_values = (uint8_t*) ytMalloc(20*sizeof(uint8_t));	//Reservo el doble de espacio y asigno el puntero a read_values a partir de 10;
	uint8_t* read_values = tx_values + 10;

	read_write_buffs.prx = read_values;
	read_write_buffs.ptx = tx_values;
	read_write_buffs.size = 10;

	tx_values[0] = ((ADXL355_FIFO_DATA<<1) | READ_MASK);	//Read

	if(ytIoctl(adxl355_data->spi_fd, SPI_READ_WRITE_OP, &read_write_buffs) != RET_OK){
		ytFree(tx_values);
		return RET_ERROR;
	}

	*valueL = read_values[3];
	*valueM = read_values[2];
	*valueH = read_values[1];
	flags = (read_values[3] & 0x03);
	if(flags != 0x01){
		ytFree(tx_values);
		return RET_ERROR;
	}
	buff->AXIS_X = (value<<8);

	*valueL = read_values[6];
	*valueM = read_values[5];
	*valueH = read_values[4];
	flags = (read_values[6] & 0x03);
	if(flags != 0x00){
		ytFree(tx_values);
		return RET_ERROR;
	}
	buff->AXIS_Y = (value<<8);

	*valueL = read_values[9];
	*valueM = read_values[8];
	*valueH = read_values[7];
	flags = (read_values[9] & 0x03);
	if(flags != 0x00){
		ytFree(tx_values);
		return RET_ERROR;
	}
	buff->AXIS_Z = (value<<8);

	ytFree(tx_values);

	return RET_OK;
}


/**
 *
 * @param adxl355_data
 * @param val
 * @return
 */
retval_t ADXL355_GetFifoStoredSamples(adxl355_data_t* adxl355_data, uint8_t* val){
	if(adxl355_data == NULL){
		return RET_ERROR;
	}
	if( ADXL355_ReadReg(adxl355_data, ADXL355_FIFO_ENTRIES, val) != RET_OK ){
		return RET_ERROR;
	}

	(*val) &= 0x7F;

	return RET_OK;
}


/**
 *
 * @param adxl355_data
 * @param reg
 * @param data
 * @return
 */
static retval_t ADXL355_ReadReg(adxl355_data_t* adxl355_data, uint8_t reg, uint8_t* data) {
	spi_read_write_buffs_t read_write_buffs;
	uint8_t spi_bus_data_tx[2];
	uint8_t spi_bus_data_rx[2];

	if(adxl355_data == NULL){
		return RET_ERROR;
	}

	read_write_buffs.prx = &spi_bus_data_rx[0];
	read_write_buffs.ptx = &spi_bus_data_tx[0];
	read_write_buffs.size = 2;
	spi_bus_data_tx[0] = ((reg<<1) | READ_MASK);	//Read


	if(ytIoctl(adxl355_data->spi_fd, SPI_READ_WRITE_OP, &read_write_buffs) != RET_OK){
		return RET_ERROR;
	}

	(*data) = spi_bus_data_rx[1];

  return RET_OK;
}


/**
 *
 * @param adxl355_data
 * @param reg
 * @param data
 * @return
 */
static retval_t ADXL355_WriteReg(adxl355_data_t* adxl355_data, uint8_t reg, uint8_t data){
	uint8_t spi_bus_data[2];

	if(adxl355_data == NULL){
		return RET_ERROR;
	}

	spi_bus_data[0] = ((reg<<1) & WRITE_MASK);	//Write
	spi_bus_data[1] = data;

	if( ytWrite(adxl355_data->spi_fd, &spi_bus_data[0], 2) != 2){
		return RET_ERROR;
	}

  return RET_OK;
}

/**
 *
 * @param adxl355_data
 * @return
 */
static retval_t ADXL355_CheckDevice(adxl355_data_t* adxl355_data){
	uint8_t value;
	if(adxl355_data == NULL){
		return RET_ERROR;
	}

	if( ADXL355_ReadReg(adxl355_data, ADXL355_DEVID_AD, &value) != RET_OK ){
		return RET_ERROR;
	}
	if(value != 0xAD){
		return RET_ERROR;
	}

	if( ADXL355_ReadReg(adxl355_data, ADXL355_DEVID_MST, &value) != RET_OK ){
		return RET_ERROR;
	}
	if(value != 0x1D){
		return RET_ERROR;
	}

	if( ADXL355_ReadReg(adxl355_data, ADXL355_PARTID, &value) != RET_OK ){
		return RET_ERROR;
	}
	if(value != 0xED){
		return RET_ERROR;
	}

	return RET_OK;
}


/**
 *
 * @param adxl355_data
 * @param adxl355_csPin
 * @return
 */
static retval_t ADXL355_config_spi_device(adxl355_data_t* adxl355_data,	gpioPin_t adxl355_csPin, char* spi_dev){

	ytSpiDriverIoctlCmd_t spi_ioctl_cmd;
	if(adxl355_data == NULL){
		return RET_ERROR;
	}

	adxl355_data->spi_fd = ytOpen(spi_dev, 0);
	if(!adxl355_data->spi_fd){
		return RET_ERROR;
	}
	spi_ioctl_cmd = ENABLE_SW_CS;
	if(ytIoctl(adxl355_data->spi_fd, spi_ioctl_cmd, (void*) adxl355_csPin) != RET_OK){
		return RET_ERROR;
	}
	spi_ioctl_cmd = DISABLE_SW_CS_EACH_BYTE;
	if(ytIoctl(adxl355_data->spi_fd, spi_ioctl_cmd, NULL) != RET_OK){
		return RET_ERROR;
	}
	spi_ioctl_cmd = SET_POLARITY_LOW;
	if(ytIoctl(adxl355_data->spi_fd, spi_ioctl_cmd, NULL) != RET_OK){
		return RET_ERROR;
	}
	spi_ioctl_cmd = SET_EDGE_1;
	if(ytIoctl(adxl355_data->spi_fd, spi_ioctl_cmd, NULL) != RET_OK){
		return RET_ERROR;
	}
	spi_ioctl_cmd = SET_SPI_MODE_MASTER;
	if(ytIoctl(adxl355_data->spi_fd, spi_ioctl_cmd, NULL) != RET_OK){
		return RET_ERROR;
	}
	spi_ioctl_cmd = SET_SPI_SPEED;
	if(ytIoctl(adxl355_data->spi_fd, spi_ioctl_cmd, (void*) ADXL355_SPI_SPEED) != RET_OK){
		return RET_ERROR;
	}
	return RET_OK;
}
