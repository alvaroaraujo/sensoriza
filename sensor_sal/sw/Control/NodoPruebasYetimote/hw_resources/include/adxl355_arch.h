/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * adxl355_arch.h
 *
 *  Created on: 8 de ene. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file adxl355_arch.h
 */
#ifndef APPLICATION_BOARD_PLATFORM_INCLUDE_ADXL355_ARCH_H_
#define APPLICATION_BOARD_PLATFORM_INCLUDE_ADXL355_ARCH_H_

#include "platform-conf.h"
#include "system_api.h"
#include "adxl355_driver.h"

#define ADXL355_FILTER					0x28
#define ADXL355_ODR_BIT				    0
#define ADXL355_HPF_BIT			    	4


#define ADXL355_DEVID_AD				0x00
#define ADXL355_DEVID_MST				0x01
#define ADXL355_PARTID					0x02

#define ADXL355_POWER_CTL				0x2D
#define ADXL355_STANDBY				    0
#define ADXL355_TEMP_OFF			    1

#define ADXL355_RANGE					0x2C
#define ADXL355_RANGE_BIT				0

//OUTPUT REGISTER
#define ADXL355_OUT_X_L					0x0A
#define ADXL355_OUT_X_M					0x09
#define ADXL355_OUT_X_H					0x08
#define ADXL355_OUT_Y_L					0x0D
#define ADXL355_OUT_Y_M					0x0C
#define ADXL355_OUT_Y_H					0x0B
#define ADXL355_OUT_Z_L					0x10
#define ADXL355_OUT_Z_M					0x0F
#define ADXL355_OUT_Z_H					0x0E

#define ADXL355_FIFO_DATA				0x11

#define ADXL355_FIFO_ENTRIES			0x05

typedef struct {
  int32_t AXIS_X;
  int32_t AXIS_Y;
  int32_t AXIS_Z;
} AxesRaw_adxl_t;


typedef struct adxl355_data_{
	uint32_t spi_fd;
	ADXL355_ODR_t odr;
	ADXL355_Mode_t mode;
	ADXL355_Fullscale_t scale;
	uint8_t axis;
}adxl355_data_t;

adxl355_data_t* new_adxl355_data(void);
retval_t delete_adxl355_data(adxl355_data_t* adxl355_data);
retval_t ADXL355_init(adxl355_data_t* adxl355_data, gpioPin_t adxl355_csPin, char* spi_dev);
retval_t ADXL355_deinit(adxl355_data_t* adxl355_data);

retval_t ADXL355_SetODR(adxl355_data_t* adxl355_data, ADXL355_ODR_t odr);
retval_t ADXL355_SetMode(adxl355_data_t* adxl355_data, ADXL355_Mode_t md);
retval_t ADXL355_SetAxis(adxl355_data_t* adxl355_data, uint8_t axis);
retval_t ADXL355_SetFullScale(adxl355_data_t* adxl355_data, ADXL355_Fullscale_t fs);
retval_t ADXL355_GetAccAxesRaw(adxl355_data_t* adxl355_data, AxesRaw_adxl_t* buff);

retval_t ADXL355_GetAccAxesRaw_FIFO_Mode(adxl355_data_t* adxl355_data, AxesRaw_adxl_t* buff);
retval_t ADXL355_GetFifoStoredSamples(adxl355_data_t* adxl355_data, uint8_t* val);


#endif /* APPLICATION_BOARD_PLATFORM_INCLUDE_ADXL355_ARCH_H_ */
