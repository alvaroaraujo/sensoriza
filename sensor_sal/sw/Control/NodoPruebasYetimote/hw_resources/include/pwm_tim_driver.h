/*
 * pwm_tim_driver.h
 *
 *  Created on: 27 feb. 2018
 *      Author: miguelvp
 */

#ifndef YETIOS_HW_RESOURCES_INCLUDE_PWM_TIM_DRIVER_H_
#define YETIOS_HW_RESOURCES_INCLUDE_PWM_TIM_DRIVER_H_

#include "sys_gpio.h"

#define makeFixPoint(decimal)		((int) (decimal*1000))

/* Device driver IOCTL commands */
typedef enum pwm_tim_driver_ioctl_cmd_{
	PWM_TIM_NEW 		=	1,
	PWM_TIM_DELETE 		=	2,
	PWM_TIM_SEL_PWM 	=	3,
	PWM_TIM_SET_FREQ 	=	4,
	PWM_TIM_SET_DUTY 	=	5,
	PWM_TIM_START 		=	6,
	PWM_TIM_STOP 		=	7,
	PWM_TIM_STOP_UP 	=	8,
	PWM_TIM_STOP_DOWN	=	9,
} pwm_tim_driver_ioctl_cmd_t;

/* Channel selection */
typedef enum {
	PWM_TIM1_B1			=		0xB1B1,
	PWM_TIM2_A0			=		0xA012,
	PWM_TIM2_A1			=		0xA122,
	PWM_TIM2_B10		=		0xBA32,
	PWM_TIM2_B11		=		0xBB42,
	PWM_TIM3_B1			=		0xB143,
	PWM_TIM4_B8			=		0xB834,
	PWM_TIM4_B9			=		0xB944,
	PWM_TIM5_A0			=		0xA015,
	PWM_TIM5_A1			=		0xA125,
	PWM_TIM8_B1			=		0xB1B8,
	PWM_TIM15_A1		=		0xA19F,			// Falla en la placa de test. Revisar funcionamiento
	PWM_TIM16_B8		=		0xB816,
	PWM_TIM17_B9		=		0xB917,
	PWM_LPTIM1_C1		=		0xC11A,
	PWM_LPTIM2_A4		=		0xA41B,
} pwm_tim_driver_channel_t;

#endif /* YETIOS_HW_RESOURCES_INCLUDE_PWM_TIM_DRIVER_H_ */
