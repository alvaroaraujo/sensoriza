/*
 * pwm_tim_arch.h
 *
 *  Created on: 27 feb. 2018
 *      Author: miguelvp
 */

#ifndef YETIOS_HW_RESOURCES_INCLUDE_PWM_TIM_ARCH_H_
#define YETIOS_HW_RESOURCES_INCLUDE_PWM_TIM_ARCH_H_

#include "platform-conf.h"
#include "system_api.h"
#include "pwm_tim_driver.h"
#include "sys_gpio.h"

 // Registro CR1
#define RESET_CR1 (0xBFFU)
#define UIFREMAP (0x1U << 11)
#define PSC_CLK (0x3U << 8)
#define PSC_CLK_2 (0x1U << 8)
#define PSC_CLK_4 (0x2U << 8)
#define AUTORELOAD (0x1U << 7)
#define DIR (0x1U << 4)
#define URS (0x1U << 2)
#define UDIS (0x1U << 1)
#define CEN (0x1U)

 // Registro DIER
#define RESET_DIER (0x101U)
#define UPDATE_ENABLE (0x1U)
#define INTERRUPT_CH (0x1U)

 // Registro CCMRx
#define RESET_CCMR_CH (0x100FFU)
#define PWM_MODE (0x6U << 4)
#define PRELOAD_ENABLE (0x1U << 3)
#define FAST_ENABLE (0x1U << 2)

 // Registro CCER
#define RESET_CCER_CH (0xBU)
#define RESET_CCER_CH_N (0xFU)
#define OUT_N_ENABLE (0x1U << 2)
#define OUT_ENABLE (0x1U)

 // Registro PSC
#define RESET_PSC (0xFFFFFFFFU)

 // Registro ARR
#define RESET_ARR (0xFFFFFFFFU)
#define MAX_16_ARR (0xFFFFU)
#define MAX_32_ARR (0xFFFFFFFFU)

 // Registro CCRx
#define RESET_CCR (0xFFFFFFFFU)

 // Registro EGR
#define UG (0x1U)

// Registro BDTR
#define MOE (0x1U << 15)

// Registro ISR & ICR LP
#define LP_ARROK (0x1U << 4)
#define LP_CMPOK (0x1U << 3)

 // Registro CFGR LP
#define LP_RESET_CFGR (0xFEEEDFU)
#define LP_PRELOAD (0x1U << 22)
#define LP_WAVPOL (0x1U << 21)
#define LP_PRESC (0x7U << 9)
#define LP_PRESC_2 (0x1U << 9)
#define LP_PRESC_4 (0x2U << 9)
#define LP_PRESC_8 (0x3U << 9)
#define LP_PRESC_16 (0x4U << 9)
#define LP_PRESC_32 (0x5U << 9)
#define LP_PRESC_64 (0x6U << 9)
#define LP_PRESC_128 (0x7U << 9)
#define LP_POLARITY (0x3U << 1)

 // Registro OR LP
#define LP_RESET_OR (0x3U)

 // Registro CR LP
#define LP_RESET_CR (0x7U)
#define LP_CNT (0x1U << 2)
#define LP_ENABLE (0x1U)

 // Registro CMP LP
#define LP_RESET_CMP (0xFFFFU)

 // Registro ARR LP
 #define LP_RESET_ARR (0xFFFFU)

typedef union
{
	TIM_TypeDef* Timer;
	LPTIM_TypeDef* LPTimer;
} Timer;

typedef enum
{
	ON,
	OFF,
	HEAD,
} State;

typedef struct
{
	uint32_t h_fix;
	uint32_t l_fix;
} fix_point_t;

typedef struct
{
	  Timer Timer;

	  GPIO_TypeDef* GPIOx;

	  uint16_t GPIO_Pin;

	  uint8_t Channel;

} channel_data_t;

typedef struct pwm_data
{
  fix_point_t duty;

  fix_point_t frequency;

  channel_data_t* channel_data;

  State Enable;

  uint8_t type;

  struct pwm_data* next;

  char changeMode;

} pwm_tim_data_t;

pwm_tim_data_t* new_pwm_tim_data(void);
retval_t delete_pwm_tim_data(pwm_tim_data_t* pwm_tim_data);
retval_t PWM_TIM_init(pwm_tim_data_t* pwm_tim_data);
retval_t PWM_TIM_deinit(pwm_tim_data_t* pwm_tim_data);

retval_t PWM_TIM_NewChannel(pwm_tim_data_t* pwm_tim_data, uint8_t timer, uint8_t gpio, uint8_t pin, uint8_t canal);
retval_t PWM_TIM_EraseChannel(pwm_tim_data_t* pwm_tim_data, uint8_t timer, uint8_t gpio, uint8_t pin, uint8_t canal);
retval_t PWM_TIM_SelPWM(pwm_tim_data_t* pwm_tim_data, uint8_t timer, uint8_t gpio, uint8_t pin, uint8_t canal);
retval_t PWM_TIM_SetFrequency(pwm_tim_data_t* pwm_tim_data, fix_point_t freq);
retval_t PWM_TIM_SetDuty(pwm_tim_data_t* pwm_tim_data, fix_point_t duty);

retval_t PWM_TIM_StartChannel(pwm_tim_data_t* pwm_tim_data, uint8_t timer, uint8_t gpio, uint8_t pin, uint8_t canal);
retval_t PWM_TIM_StopChannel(pwm_tim_data_t* pwm_tim_data, uint8_t timer, uint8_t gpio, uint8_t pin, uint8_t canal);
retval_t PWM_TIM_StopUpChannel(pwm_tim_data_t* pwm_tim_data, uint8_t timer, uint8_t gpio, uint8_t pin, uint8_t canal);
retval_t PWM_TIM_StopDownChannel(pwm_tim_data_t* pwm_tim_data, uint8_t timer, uint8_t gpio, uint8_t pin, uint8_t canal);

#endif /* YETIOS_HW_RESOURCES_INCLUDE_PWM_TIM_ARCH_H_ */
