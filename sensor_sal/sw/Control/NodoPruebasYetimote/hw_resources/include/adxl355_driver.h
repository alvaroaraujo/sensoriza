/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * adxl355_driver.h
 *
 *  Created on: 8 de ene. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file adxl355_driver.h
 */
#ifndef APPLICATION_BOARD_PLATFORM_INCLUDE_ADXL355_DRIVER_H_
#define APPLICATION_BOARD_PLATFORM_INCLUDE_ADXL355_DRIVER_H_


/* Device driver IOCTL commands */
typedef enum adxl355_driver_ioctl_cmd_{
	ADXL355_SET_MODE = 		1,
	ADXL355_SET_ODR = 		2,
	ADXL355_SET_SCALE = 	3,
	ADXL355_SET_AXIS = 		4,
	ADXL355_NO_CMD = 0xFFFF,
}adxl355_driver_ioctl_cmd_t;


#define AXIS_X_ENABLE					0x01
#define AXIS_Y_ENABLE					0x02
#define AXIS_Z_ENABLE					0x04

typedef enum {
  ADXL355_ODR_4000Hz		    =		0x00,
  ADXL355_ODR_2000Hz            =		0x01,
  ADXL355_ODR_1000Hz		  	=		0x02,
  ADXL355_ODR_500Hz		        =		0x03,
  ADXL355_ODR_250Hz		        =		0x04,
  ADXL355_ODR_125Hz		    	=		0x05,
  ADXL355_ODR_62_5Hz		    =		0x06,
  ADXL355_ODR_31_25Hz		    =		0x07,
  ADXL355_ODR_15_625Hz		    =		0x08,
  ADXL355_ODR_7_813Hz		    =		0x09,
  ADXL355_ODR_3_906Hz		    =		0x0A,
  ADXL355_ODR_NO_INIT		    =		0xFF,
} ADXL355_ODR_t;

typedef enum {
  ADXL355_POWER_DOWN                =		0x01,
  ADXL355_NORMAL					=		0x00,
  ADXL355_NO_INIT					=		0xFF,
} ADXL355_Mode_t;


typedef enum {
  ADXL355_FULLSCALE_2                   =               0x01,
  ADXL355_FULLSCALE_4                   =               0x02,
  ADXL355_FULLSCALE_8                   =               0x03,
  ADXL355_FULLSCALE_NO_INIT             =               0xFF,
} ADXL355_Fullscale_t;

#endif /* APPLICATION_BOARD_PLATFORM_INCLUDE_ADXL355_DRIVER_H_ */
