%% B�squeda de las dimensiones �ptimas del array por simulaci�n para recepci�n

% Tama�o f�sico del fotodiodo
tamanoLD = 0.00265;

wavelength = 610e-9;            % Longitud de onda de inter�s
thetadeg = 120/2;               % �ngulo mitad
theta = thetadeg*pi/180;        % radianes
m = -(log(2)/log(cos(theta)));  % Factor de propagaci�n de LED
distMin=1e-3;                   % Distancia m�nima de evaluaci�n hasta el cristal
distMax=17e-3;                  % Distancia m�xima de evaluaci�n hasta el cristal
precision=.1e-3;                % Resoluci�n de las distancias
Dgui = distMin:precision:distMax;   % Distancias entre placa y cristal

% Tri�ngulo
Ndiodos = 3;
xdiodo = [0 3.118e-3 -3.118e-3];    % Ptos x de los diodos
ydiodo = [-3.6e-3 1.8e-3 1.8e-3]';  % Ptos y de los diodos

% Caracter�sticas del cristal
paso=1e-3;
grosor=sqrt(3)*paso/2;
radioFiltro=10.5e-3;

% Creaci�n del mapa
resolucion = 1000;
z = Dgui;
x = linspace(-0.015,0.015, resolucion);
y = linspace(-0.015,0.015, resolucion)';

radiacion = ones(1,Ndiodos);        % Intensidad de radiaci�n de los diodos

% Gr�fica de Recepci�n
fot = 0;
tof = [];
figure(1);
surf([0,.000001],[0,.000001],[0 0;0 0])     % Gr�fica en 3D para poner luego la vista desde el frente
hold on
pause(.1)

% Iteraciones sucesivas con el corte de la intensidad recibida en el plano
% indicado
rectangle('Position',[xdiodo(1)-tamanoLD/2 ydiodo(1)-tamanoLD/2 tamanoLD tamanoLD]);
rectangle('Position',[xdiodo(2)-tamanoLD/2 ydiodo(2)-tamanoLD/2 tamanoLD tamanoLD]);
rectangle('Position',[xdiodo(3)-tamanoLD/2 ydiodo(3)-tamanoLD/2 tamanoLD tamanoLD]);
rectangle('Position',[-radioFiltro -radioFiltro 2*radioFiltro 2*radioFiltro], 'Curvature', [1 1]);
for p = 1:length(z)
    I = 0;
    for index = 1:Ndiodos
        I = I + z(p)^m*radiacion(index).*(ones(length(resolucion))*((x-xdiodo(index)).^2)+ones(length(resolucion))*((y-ydiodo(index)).^2)+ones(length(resolucion))*(z(p)^2)).^(-(m+2)/2);
    end
    I = I/(max(max(I)));
    umbral = max(max(I)) / 2;
    sel = I > umbral;
    Ieff = I .* sel;
    [fot,tof] = contour(x(length(x)/2:end),y,I(:,length(x)/2:length(x)),.1:.1:1);
    tof.ContourZLevel = -z(p);
    shading flat
    colormap(jet);
    colorbar
    axis([-0.015 0.015 -0.015 0.015]);
    title(['Distancia de placa a filtro ' num2str(1000*z(p)) ' mm'])
    pause(.1)
    
end
hold off
campos([-0.26 0 -distMax/2]);

%% Intensidad a altura deseada 
% Con los datos anteriores, determina la intensidad teniendo en cuenta la 
% distancia al filtro deseada
distanciaFiltro=13.5e-3;

% Dimensiones del fotodiodo
width=4e-3;
long1=2.935e-3;
long2=2.155e-3;
dist=6.236e-3;

% C�lculo de la intensidad normalizada
for index = 1:Ndiodos
    I = I + distanciaFiltro^m*radiacion(index).*(ones(length(resolucion))*((x-xdiodo(index)).^2)+ones(length(resolucion))*((y-ydiodo(index)).^2)+ones(length(resolucion))*(distanciaFiltro^2)).^(-(m+2)/2);
end
I = I/(max(max(I)));
umbral = max(max(I)) / 2;
sel = I > umbral;
Ieff = I .* sel;
[fot,tof] = contour(x,y,I,.1:.1:1);
shading flat
colormap(jet);
colorbar
axis([-0.015 0.015 -0.015 0.015]);

hold on
rectangle('Position',[-12.7e-3 -12.7e-3 25.4e-3 25.4e-3], 'Curvature', [1 1]);
f = hgtransform;
g = hgtransform;
h = hgtransform;
rectangle('Position',[-long2 -width/2 long1+long2 width], 'Parent', f);
rectangle('Position',[-long1 -width/2 long1+long2 width], 'Parent', g);
rectangle('Position',[-long1 -width/2 long1+long2 width], 'Parent', h);
f.Matrix = makehgtform('translate', [0 dist/(2*cos(pi/6)) 0]);
g.Matrix = makehgtform('translate', [dist/2 -tan(pi/6)*dist/2 0],'zrotate', pi/3);
h.Matrix = makehgtform('translate', [-dist/2 -tan(pi/6)*dist/2 0],'zrotate', -pi/3);
    title(['Distancia de placa a filtro ' num2str(1000*distanciaFiltro) ' mm'])
hold off