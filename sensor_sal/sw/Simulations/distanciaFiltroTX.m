%% B�squeda de las dimensiones �ptimas del array por simulaci�n para transmisi�n (Cuadrado)

% Tama�o f�sico del diodo
tamanoLD = 0.0035;

wavelength = 273e-9;                % -> 273 deepUV  -> 480 Azul
thetadeg = 125/2;                   % �ngulo mitad de apertura
theta = thetadeg*pi/180;            % radianes
m = -(log(2)/log(cos(theta)));      % Factor de propagaci�n
distMin=1e-3;                       % Distancia m�nima al cristal
distMax=14e-3;                      % Distancia m�xima al cristal
precision=.05e-3;                   % Resoluci�n
Dgui = distMin:precision:distMax;   % Posibles distancias al cristal
dgui = 8e-3;                        % Distancia entre diodos

% Cuadrado
Ndiodos = 4;
xdiodo = [-dgui/2 -dgui/2 dgui/2 dgui/2];
ydiodo = [dgui/2 -dgui/2 -dgui/2 dgui/2]';

% Caracter�sticas del cristal
paso=1e-3;
grosor=sqrt(3)*paso/2;
radioFiltro=12.5e-3 - grosor;

% Creaci�n del mapa
resolucion = 1000;
z = Dgui;
x = linspace(-.2,.2, resolucion);   % Ptos x de los diodos
y = linspace(-.2,.2, resolucion)';  % Ptos y de los diodos

% Intensidad de radiaci�n de los diodos
radiacion = ones(1,Ndiodos);

% Gr�fica de Transmisi�n
fot = 0;
tof = [];
figure(2);
surf([0,.000001],[0,.000001],[0 0;0 0]) % Gr�fica en 3D para poner luego la vista desde el frente
hold on
pause(.1)

% Iteraciones sucesivas con el corte de la intensidad recibida en el plano
% indicado
rectangle('Position',[xdiodo(1)-tamanoLD/2 ydiodo(1)-tamanoLD/2 tamanoLD tamanoLD]);
rectangle('Position',[xdiodo(2)-tamanoLD/2 ydiodo(2)-tamanoLD/2 tamanoLD tamanoLD]);
rectangle('Position',[xdiodo(3)-tamanoLD/2 ydiodo(3)-tamanoLD/2 tamanoLD tamanoLD]);
rectangle('Position',[xdiodo(4)-tamanoLD/2 ydiodo(4)-tamanoLD/2 tamanoLD tamanoLD]);
rectangle('Position',[-radioFiltro -radioFiltro 2*radioFiltro 2*radioFiltro], 'Curvature', [1 1]);
for p = 1:length(z)
    I = 0;
    for index = 1:Ndiodos
        I = I + z(p)^m*radiacion(index).*(ones(length(resolucion))*((x-xdiodo(index)).^2)+ones(length(resolucion))*((y-ydiodo(index)).^2)+ones(length(resolucion))*(z(p)^2)).^(-(m+2)/2);
    end
    I = I/(max(max(I)));
    umbral = max(max(I)) / 2;
    sel = I > umbral;
    Ieff = I .* sel;
    [fot,tof] = contour(x(length(x)/2:end),y,I(:,length(x)/2:length(x)));
    tof.ContourZLevel = -z(p);
    shading flat
    colormap(jet);
    colorbar
    axis([-0.015 0.015 -0.015 0.015]);
    title(['Distancia de placa a filtro ' num2str(1000*z(p)) ' mm'])
    pause(.1)
    
end
hold off
campos([-0.26 0 -distMax/2]);

%% B�squeda de las dimensiones �ptimas del array por simulaci�n para transmisi�n (Tri�ngulo)

% Tama�o f�sico del diodo
tamanoLD = 0.0035;

wavelength = 480e-9;                % -> 273 deepUV  -> 480 Azul
thetadeg = 125/2;                   % �ngulo mitad de apertura
theta = thetadeg*pi/180;            % radianes
m = -(log(2)/log(cos(theta)));      % Factor de propagaci�n
distMin=1e-3;                       % Distancia m�nima al cristal
distMax=14e-3;                      % Distancia m�xima al cristal
precision=.05e-3;                   % Resoluci�n
Dgui = distMin:precision:distMax;   % Posibles distancias al cristal
dgui = 8e-3;                        % Distancia entre diodos

% Tri�ngulo
Ndiodos = 4;
xdiodo = [0 0 dgui/2 -dgui/2];    % Ptos x de los diodos
ydiodo = [0 dgui/(2*cos(pi/6)) -tan(pi/6)*dgui/2 -tan(pi/6)*dgui/2]'; % Ptos y

% Caracter�sticas del cristal
paso=1e-3;
grosor=sqrt(3)*paso/2;
radioFiltro=12.5e-3 - grosor;

% Creaci�n del mapa
resolucion = 1000;
z = Dgui;
x = linspace(-.2,.2, resolucion);   % Ptos x de los diodos
y = linspace(-.2,.2, resolucion)';  % Ptos y de los diodos

% Intensidad de radiaci�n de los diodos
radiacion = ones(1,Ndiodos);

% Gr�fica de Transmisi�n
fot = 0;
tof = [];
figure(2);
surf([0,.000001],[0,.000001],[0 0;0 0]) % Gr�fica en 3D para poner luego la vista desde el frente
hold on
pause(.1)

% Iteraciones sucesivas con el corte de la intensidad recibida en el plano
% indicado
rectangle('Position',[xdiodo(1)-tamanoLD/2 ydiodo(1)-tamanoLD/2 tamanoLD tamanoLD]);
rectangle('Position',[xdiodo(2)-tamanoLD/2 ydiodo(2)-tamanoLD/2 tamanoLD tamanoLD]);
rectangle('Position',[xdiodo(3)-tamanoLD/2 ydiodo(3)-tamanoLD/2 tamanoLD tamanoLD]);
rectangle('Position',[xdiodo(4)-tamanoLD/2 ydiodo(4)-tamanoLD/2 tamanoLD tamanoLD]);
rectangle('Position',[-radioFiltro -radioFiltro 2*radioFiltro 2*radioFiltro], 'Curvature', [1 1]);
for p = 1:length(z)
    I = 0;
    for index = 1:Ndiodos
        I = I + z(p)^m*radiacion(index).*(ones(length(resolucion))*((x-xdiodo(index)).^2)+ones(length(resolucion))*((y-ydiodo(index)).^2)+ones(length(resolucion))*(z(p)^2)).^(-(m+2)/2);
    end
    I = I/(max(max(I)));
    umbral = max(max(I)) / 2;
    sel = I > umbral;
    Ieff = I .* sel;
    [fot,tof] = contour(x(length(x)/2:end),y,I(:,length(x)/2:length(x)));
    tof.ContourZLevel = -z(p);
    shading flat
    colormap(jet);
    colorbar
    axis([-0.015 0.015 -0.015 0.015]);
    title(['Distancia de placa a filtro ' num2str(1000*z(p)) ' mm'])
    pause(.1)
    
end
hold off
campos([-0.26 0 -distMax/2]);