%% C�lculo de la gr�fica que relaciona distancia de los diodos LED con el �rea
% que abarca y la intensidad que hay dentro

tamanoLD = 0.0035;

wavelength = 273e-9;                % -> 273 deepUV  -> 480 Azul
thetadeg = 125/2;                   % �ngulo de apertura de los diodos
theta = thetadeg*pi/180;            % radianes
m = -(log(2)/log(cos(theta)));      % Factor de propagaci�n de LED
Dgui = 150e-3;                      % Distancia de la placa al suelo
dgui = linspace(3.5e-3,40e-3,400);  % Distancias entre diodos emisores

Dmax = 0.25;                        % Distancia m�xima del �rea a evaluar

% Cuadrado
Ndiodos = 4;                        % Si se modifica, cambiar la topolog�a
xdiodo = [-dgui'/2 -dgui'/2 dgui'/2 dgui'/2];   % Ptos x de los diodos
ydiodo = [dgui'/2 -dgui'/2 -dgui'/2 dgui'/2]';  % Ptos y de los diodos

% Creci�n del mapa
resolucion = 1000;
z = Dgui;
x = zeros(length(dgui),resolucion);
y = x';
for p=1:length(dgui)
    x(p,:) = linspace(-Dmax,Dmax, resolucion);
    y(:,p) = linspace(-Dmax,Dmax, resolucion)';
end

radiacion = ones(1,Ndiodos);            % Intensidad de radiaci�n de los diodos


% C�lculo de la intensidad emitida
Ccuad = zeros(1,length(dgui));      % Area donde la intensidad es la mitad de la maxima
IcuadC = zeros(1,length(dgui));     % Suma de la intensidad en dicho area
ItotalCuad = zeros(1,length(dgui)); % Suma de la intensidad
CtotalCuad = zeros(1,length(dgui)); % Suma de la intensidad
for p = 1:length(dgui)
    I = 0;
    for index = 1:Ndiodos
        I = I + z^m*radiacion(index).*((x(p,:)-xdiodo(p,index)).^2+(y(:,p)-ydiodo(index,p)).^2+z^2).^(-(m+2)/2);
    end
    umbral = max(max(I)) / 2;
    sel = I > umbral;
    Ieff = I .* sel;
    Ccuad(p) = sum(sum(sel))*(x(p,2)-x(p,1))^2*10000; % 10000 para que sea cm2
    IcuadC(p) = sum(sum(Ieff)).*(x(p,2)-x(p,1))^2;
    ItotalCuad(p) = sum(sum(I)).*(x(p,2)-x(p,1))^2;
    CtotalCuad(p) = sum(sum(I > 0.001*max(max(I))))*(x(p,2)-x(p,1))^2*10000; % 10000 para que sea cm2
    
end

% Cambio de la topolog�a
dgui2 = linspace(7.3e-3,40e-3,400);

% Tri�ngulo
Ndiodos = 4;
xdiodo = [zeros(length(dgui2),1) zeros(length(dgui2),1) dgui2'/2 -dgui2'/2];    % Ptos x de los diodos
ydiodo = [zeros(length(dgui2),1) dgui2'/(2*cos(pi/6)) -tan(pi/6)*dgui2'/2 -tan(pi/6)*dgui2'/2]'; % Ptos y

% Creaci�n del mapa
resolucion = 1000;
z = Dgui;
x = zeros(length(dgui2),resolucion);
y = x';
for p=1:length(dgui2)
    x(p,:) = linspace(-Dmax,Dmax, resolucion);
    y(:,p) = linspace(-Dmax,Dmax, resolucion)';
end

radiacion = ones(1,11);         % Intensidad de radiaci�n de los diodos

% C�lculo de la intensidad emitida
Ctri = zeros(1,length(dgui2));      % Area donde la intensidad es la mitad de la maxima
ItriC = zeros(1,length(dgui2));     % Suma de la intensidad en dicho area
ItotalTri = zeros(1,length(dgui2)); % Suma de la intensidad
CtotalTri = zeros(1,length(dgui2)); % Suma de la intensidad
for p = 1:length(dgui2)
    I = 0;
    for index = 1:Ndiodos
        I = I + z^m*radiacion(index).*((x(p,:)-xdiodo(p,index)).^2+(y(:,p)-ydiodo(index,p)).^2+z^2).^(-(m+2)/2);
    end
    umbral = max(max(I)) / 2;
    sel = I > umbral;
    Ieff = I .* sel;
    Ctri(p) = sum(sum(sel))*(x(p,2)-x(p,1))^2*10000; % 10000 para que sea cm2
    ItriC(p) = sum(sum(Ieff)).*(x(p,2)-x(p,1))^2;
    ItotalTri(p) = sum(sum(I)).*(x(p,2)-x(p,1))^2;
    CtotalTri(p) = sum(sum(I > 0.001*max(max(I))))*(x(p,2)-x(p,1))^2*10000;
    
end

% Representaci�n de comparativa entre ambas distribuciones
figure(3)
subplot(2,4,[1 2])
plot(dgui,Ccuad);
hold on
plot(dgui2,Ctri,'r');
title('�rea de intensidad mitad');
legend('Cuadrado','Tri�ngulo','Location','NorthWest');
xlabel('Distancia diodos');
grid
grid minor
hold off
subplot(2,4,[3 4])
plot(dgui,IcuadC);
hold on
plot(dgui2,ItriC,'r');
title('Intensidad en �rea mitad');
legend('Cuadrado','Tri�ngulo','Location','NorthWest');
xlabel('Distancia diodos');
grid
grid minor
hold off
subplot(2,4,[6 7])
plot(dgui,(IcuadC)./(Ccuad));
hold on
plot(dgui2,(ItriC)./(Ctri));
title('Relacion Intensidad/�rea')
legend('Cuadrado','Tri�ngulo');
xlabel('Distancia diodos');
grid
grid minor
hold off


figure(4)
subplot(4,4,[3 4 7 8])
plot(dgui,ItotalCuad);
hold on
plot(dgui2,ItotalTri,'r');
title('Intensidad total')
legend('Cuadrado','Tri�ngulo','Location','SouthEast');
xlabel('Distancia diodos');
grid
grid minor
hold off
subplot(4,4,[1 2 5 6])
plot(dgui,CtotalCuad);
hold on
plot(dgui2,CtotalTri,'r');
title('�rea total')
legend('Cuadrado','Tri�ngulo','Location','SouthEast');
xlabel('Distancia diodos');
grid
grid minor
hold off
subplot(4,4,[9 10 13 14])
plot(dgui,ItotalCuad./CtotalCuad);
hold on
plot(dgui2,ItotalTri./CtotalTri,'r');
title('Relaci�n IntensidadTotal/�reaTotal')
legend('Cuadrado','Tri�ngulo');
xlabel('Distancia diodos');
grid
grid minor
hold off
subplot(4,4,11)
axis([-0.025 0.025 -0.025 0.025]);
hold on
rectangle('Position',[5e-3-0.0035/2 5e-3-0.0035/2 0.0035 0.0035]);
rectangle('Position',[5e-3-0.0035/2 -5e-3-0.0035/2 0.0035 0.0035]);
rectangle('Position',[-5e-3-0.0035/2 5e-3-0.0035/2 0.0035 0.0035]);
rectangle('Position',[-5e-3-0.0035/2 -5e-3-0.0035/2 0.0035 0.0035]);
rectangle('Position',[-12e-3 -12e-3 24e-3 24e-3], 'Curvature', [1 1]);
title('Topolog�a cuadrada')
hold off
subplot(4,4,16)
axis([-0.025 0.025 -0.025 0.025]);
hold on
g = hgtransform;
h = hgtransform;
rectangle('Position',[-0.0035/2 -0.0035/2 0.0035 0.0035]);
rectangle('Position',[-0.0035/2 10e-3/(2*cos(pi/6))-0.0035/2 0.0035 0.0035]);
rectangle('Position',[-0.0035/2 -0.0035/2 0.0035 0.0035], 'Parent', g);
rectangle('Position',[-0.0035/2 -0.0035/2 0.0035 0.0035], 'Parent', h);
rectangle('Position',[-12e-3 -12e-3 24e-3 24e-3], 'Curvature', [1 1]);
g.Matrix = makehgtform('translate', [10e-3/2 -tan(pi/6)*10e-3/2 0],'zrotate', -pi/6);
h.Matrix = makehgtform('translate', [-10e-3/2 -tan(pi/6)*10e-3/2 0],'zrotate', pi/6);
title('Topolog�a triangular')
hold off

%% C�lculo de la gr�fica que relaciona distancia de los diodos L�ser con el �rea
% que abarca y la intensidad normalizada que hay dentro

tamanoLD = 0.0035;

wavelength = 475e-9;                % -> 273 deepUV  -> 480 Azul
M = 1;                              % Factor de propagaci�n
thetadeg = 125/2;                   % �ngulo mitad de apertura
theta = thetadeg*pi/180;            % Paso a radianes
Dgui = 150e-3;                      % Altura de la placa
dgui = linspace(3.5e-3,40e-3,400);  % Distancias a comprobar entre diodos
w0 = M^2*wavelength/(pi*theta);

Dmax = 0.25;                        % Distancia media m�xima del �rea a testear

% Cuadrado
Ndiodos = 4;    % Si cambia, hay que cambiar topolog�a
xdiodo = [-dgui'/2 -dgui'/2 dgui'/2 dgui'/2];   % Ptos x donde est�n los diodos
ydiodo = [dgui'/2 -dgui'/2 -dgui'/2 dgui'/2]';  % Ptos y donde est�n los diodos

resolucion = 1000;                  % Resoluci�n del mapa generado
z = Dgui;

% Creaci�n del mapa
x = zeros(length(dgui),resolucion);
y = x';
for p=1:length(dgui)
    x(p,:) = linspace(-Dmax,Dmax, resolucion);
    y(:,p) = linspace(-Dmax,Dmax, resolucion)';
end

% Intensidad de los diodos en el array
radiacion = ones(1,11);


% C�lculo de la intensidad emitida
A = M^2*wavelength.*z/(pi*(w0)^2);
w = w0*sqrt(1+(A).^2);
R = z.*(1+(1./A).^2);

Ccuad = zeros(1,length(dgui));      % Area donde la intensidad es la mitad de la maxima
IcuadC = zeros(1,length(dgui));     % Suma de la intensidad en dicho area
ItotalCuad = zeros(1,length(dgui)); % Suma de la intensidad
CtotalCuad = zeros(1,length(dgui)); % Suma de la intensidad
for p = 1:length(dgui)
    I = radiacion(1).*(exp(-2*(ones(length(resolucion))*((x(p,:)-xdiodo(p,1)).^2)+ones(length(resolucion))*((y(:,p)-ydiodo(1,p)).^2))/(w(1).^2))) + ...
        radiacion(2).*(exp(-2*(ones(length(resolucion))*((x(p,:)-xdiodo(p,2)).^2)+ones(length(resolucion))*((y(:,p)-ydiodo(2,p)).^2))/(w(1).^2))) + ...
        radiacion(3).*(exp(-2*(ones(length(resolucion))*((x(p,:)-xdiodo(p,3)).^2)+ones(length(resolucion))*((y(:,p)-ydiodo(3,p)).^2))/(w(1).^2))) + ...
        radiacion(4).*(exp(-2*(ones(length(resolucion))*((x(p,:)-xdiodo(p,4)).^2)+ones(length(resolucion))*((y(:,p)-ydiodo(4,p)).^2))/(w(1).^2)));
    umbral = max(max(I)) / 2;       % Umbral para intensidad mitad
    sel = I > umbral;
    Ieff = I .* sel;
    Ccuad(p) = sum(sum(sel))*(x(p,2)-x(p,1))^2*10000; % 10000 para que sea cm2
    IcuadC(p) = sum(sum(Ieff)).*(x(p,2)-x(p,1))^2;
    ItotalCuad(p) = sum(sum(I)).*(x(p,2)-x(p,1))^2;
    CtotalCuad(p) = sum(sum(I > 0.001*max(max(I))))*(x(p,2)-x(p,1))^2*10000; % 10000 para que sea cm2
    
end



% Cambio de la topolog�a de la disposici�n de los diodos
dgui2 = linspace(7.3e-3,40e-3,400);     % Var�a distancia m�nima por la topolog�a ya que chocan si no

% Tri�ngulo
Ndiodos = 4;    % Si cambia, hay que cambiar topolog�a
xdiodo = [zeros(length(dgui2),1) zeros(length(dgui2),1) dgui2'/2 -dgui2'/2];    % Ptos x donde est�n los diodos
ydiodo = [zeros(length(dgui2),1) dgui2'/(2*cos(pi/6)) -tan(pi/6)*dgui2'/2 -tan(pi/6)*dgui2'/2]';    % Ptos y ...

% Creaci�n del mapa
x = zeros(length(dgui2),resolucion);
y = x';
for p=1:length(dgui2)
    x(p,:) = linspace(-Dmax,Dmax, resolucion);
    y(:,p) = linspace(-Dmax,Dmax, resolucion)';
end

% Intensidad de los diodos en el array
radiacion = ones(1,11); 

% C�lculo de la intensidad emitida
A = M^2*wavelength.*z/(pi*(w0)^2);
w = w0*sqrt(1+(A).^2);
R = z.*(1+(1./A).^2);
Ctri = zeros(1,length(dgui2));      % Area donde la intensidad es la mitad de la maxima
ItriC = zeros(1,length(dgui2));     % Suma de la intensidad en dicho area
ItotalTri = zeros(1,length(dgui2)); % Suma de la intensidad
CtotalTri = zeros(1,length(dgui2)); % Suma de la intensidad
for p = 1:length(dgui2)
    I = radiacion(1).*(exp(-2*(ones(length(resolucion))*((x(p,:)-xdiodo(p,1)).^2)+ones(length(resolucion))*((y(:,p)-ydiodo(1,p)).^2))/(w(1).^2))) + ...
        radiacion(2).*(exp(-2*(ones(length(resolucion))*((x(p,:)-xdiodo(p,2)).^2)+ones(length(resolucion))*((y(:,p)-ydiodo(2,p)).^2))/(w(1).^2))) + ...
        radiacion(3).*(exp(-2*(ones(length(resolucion))*((x(p,:)-xdiodo(p,3)).^2)+ones(length(resolucion))*((y(:,p)-ydiodo(3,p)).^2))/(w(1).^2))) + ...
        radiacion(4).*(exp(-2*(ones(length(resolucion))*((x(p,:)-xdiodo(p,4)).^2)+ones(length(resolucion))*((y(:,p)-ydiodo(4,p)).^2))/(w(1).^2)));
    umbral = max(max(I)) / 2;       % Umbral de intensidad mitad
    sel = I > umbral;
    Ieff = I .* sel;
    Ctri(p) = sum(sum(sel))*(x(p,2)-x(p,1))^2*10000; % 10000 para que sea cm2
    ItriC(p) = sum(sum(Ieff)).*(x(p,2)-x(p,1))^2;
    ItotalTri(p) = sum(sum(I)).*(x(p,2)-x(p,1))^2;
    CtotalTri(p) = sum(sum(I > 0.001*max(max(I))))*(x(p,2)-x(p,1))^2*10000;
    
end


% Representaci�n de comparativa entre ambas distribuciones
figure(3)
subplot(2,4,[1 2])
plot(dgui,Ccuad);
hold on
plot(dgui2,Ctri,'r');
title('�rea de intensidad mitad');
legend('Cuadrado','Tri�ngulo','Location','NorthWest');
xlabel('Distancia diodos');
hold off
subplot(2,4,[3 4])
plot(dgui,IcuadC);
hold on
plot(dgui2,ItriC,'r');
title('Intensidad en dicho �rea');
legend('Cuadrado','Tri�ngulo','Location','NorthWest');
xlabel('Distancia diodos');
hold off
subplot(2,4,[6 7])
plot(dgui,(IcuadC)./(Ccuad));
hold on
plot(dgui2,(ItriC)./(Ctri));
title('Relacion Intensidad/�rea')
legend('Cuadrado','Tri�ngulo', 'Location', 'SouthWest');
xlabel('Distancia diodos');
hold off
figure(4)
subplot(2,4,[3 4])
plot(dgui,ItotalCuad);
hold on
plot(dgui2,ItotalTri,'r');
title('Intensidad total')
legend('Cuadrado','Tri�ngulo','Location','SouthWest');
xlabel('Distancia diodos');
hold off
subplot(2,4,[1 2])
plot(dgui,CtotalCuad);
hold on
plot(dgui2,CtotalTri,'r');
title('�rea total')
legend('Cuadrado','Tri�ngulo','Location','NorthWest');
xlabel('Distancia diodos');
hold off
subplot(2,4,[6 7])
plot(dgui,ItotalCuad./CtotalCuad);
hold on
plot(dgui2,ItotalTri./CtotalTri,'r');
title('Relaci�n IntensidadTotal/�reaTotal')
legend('Cuadrado','Tri�ngulo', 'Location', 'SouthWest');
xlabel('Distancia diodos');
hold off