/*
 * json_helper.c
 *
 *  Created on: 28 nov. 2018
 *      Author: albarc
 */

/* Includes ------------------------------------------------------------------*/
#include "json_helper.h"

/* Variables ------------------------------------------------------------------*/
const char* full_json_message_format = "{%s:%s,%s:%s,%s:%s,%s:%d,%s:%d,%s:%u,%s:%u,%s:%lu,%s:%u,%s:%u}";

/**************************************************************************/
/*!
    Full JSON struct writer
    @param json_message: char pointer to store the message
    @param fields: struct with all possible values
*/
/**************************************************************************/
void full_json_struct_writer (char* json_message, json_all_fields_t fields)
{
	/* Create name field from id */
#ifdef JSON_DYNAMIC_MEM
	char* name_value = pvPortMalloc(11); // Format "Nodo ###", id up to three digits, plus /0 termination, plus escaped quotes
	char* lat_value = pvPortMalloc(14); // Up to 3 integer digits, 8 decimal digits, dot, minus sign, termination
	char* long_value = pvPortMalloc(14); // Up to 3 integer digits, 8 decimal digits, dot, minus sign, termination
#else
	char name_value[11];   // Format "Nodo ###", id up to three digits, plus /0 termination, plus es
	char lat_value[14];   // Up to 3 integer digits, 8 decimal digits, dot, minus sign, termination
	char long_value[14];   // Up to 3 integer digits, 8 decimal digits, dot, minus sign, termination
#endif
//	char* name_value = malloc(11); // Format "Nodo ###", id up to three digits, plus /0 termination, plus escaped quotes
	sprintf(name_value, "\"Nodo %d\"", fields.node_id);

	/* Convert double values since our sprintf doesn't support them */
//	char* lat_value = malloc(14); // Up to 3 integer digits, 8 decimal digits, dot, minus sign, termination
//	char* long_value = malloc(14); // Up to 3 integer digits, 8 decimal digits, dot, minus sign, termination
	dtostrf(fields.latitude, 6, lat_value);
	dtostrf(fields.longitude, 6, long_value);

	/* Create json_message */
	sprintf(json_message, full_json_message_format,
			NAME_KEY,
			name_value,
			LATITUDE_KEY,
			lat_value,
			LONGITUDE_KEY,
			long_value,
			AIR_TEMP_KEY,
			fields.air_temp,
			GROUND_TEMP_KEY,
			fields.ground_temp,
			HUM_KEY,
			fields.hum,
			PRESS_KEY,
			fields.press,
			LUM_KEY,
			fields.lum,
			RAIN_KEY,
			fields.rain,
			SALT_KEY,
			fields.salt
	);

#ifdef JSON_DYNAMIC_MEM
	// Free allocated space
	vPortFree(name_value);
	vPortFree(lat_value);
	vPortFree(long_value);
#endif
}

/**************************************************************************/
/*!
    JSON message writer, only requested fields
    @param json_message: char pointer to store the message
    @param requested_fields: variable with requested fields bit-wise in its last 9 bits (node id always present)
    	8: LAT, 7: LONG, 6: AIR_TEMP, 5: GROUND_TEMP, 4: HUM, 3: PRESS, 2: LUM, 1: RAIN, 0: SALT
    @param fields: struct with all possible values
*/
/**************************************************************************/
void requested_fields_json_writer (char* json_message, uint16_t requested_fields, json_all_fields_t fields)
{
	uint8_t active_fields_index = 0;	// variable to index the requested fields to include in sprintf
	char* key[9];
	uint32_t value[9];

#ifdef JSON_DYNAMIC_MEM
	char* name_value = pvPortMalloc(11);// Format "Nodo ###", id up to three digits, plus /0 termination, plus escaped quotes
	char* lat_value = pvPortMalloc(14); // Up to 3 integer digits, 8 decimal digits, dot, minus sign, termination
	char* long_value = pvPortMalloc(14); // Up to 3 integer digits, 8 decimal digits, dot, minus sign, termination
	char* format = pvPortMalloc(63);
#else
	char name_value[11];// Format "Nodo ###", id up to three digits, plus /0 termination, plus escaped quotes
	char lat_value[14]; // Up to 3 integer digits, 8 decimal digits, dot, minus sign, termination
	char long_value[14]; // Up to 3 integer digits, 8 decimal digits, dot, minus sign, termination
	char format[63];
#endif

//	char* lat_value = malloc(14); // Up to 3 integer digits, 8 decimal digits, dot, minus sign, termination
//	char* long_value = malloc(14); // Up to 3 integer digits, 8 decimal digits, dot, minus sign, termination

	/* Create sprintf format string */
//	char* format = malloc(63);
	strcpy(format, "{%s:%s,");	// Node name always present

	/* Create name field from id */
//	char* name_value = malloc(11); // Format "Nodo ###", id up to three digits, plus /0 termination, plus escaped quotes
	sprintf(name_value, "\"Nodo %d\"", fields.node_id);

	/* LATITUDE AND LONGITUDE */
	if ((requested_fields & LATITUDE_MASK) | (requested_fields & LONGITUDE_MASK)) {
		/* Convert double values since our sprintf doesn't support them */
		dtostrf(fields.latitude, 6, lat_value);
		dtostrf(fields.longitude, 6, long_value);

		strcat(format, "%s:%s,%s:%s,");
	}

	/* AIR TEMPERATURE */
	if (requested_fields & AIR_TEMP_MASK) {
		strcat(format, "%s:%d,");

		key[active_fields_index] = AIR_TEMP_KEY;
		value[active_fields_index] = fields.air_temp;
		active_fields_index++;
	}

	/* GROUND TEMPERATURE */
	if (requested_fields & GROUND_TEMP_MASK) {
		strcat(format, "%s:%d,");

		key[active_fields_index] = GROUND_TEMP_KEY;
		value[active_fields_index] = fields.ground_temp;
		active_fields_index++;
	}

	/* HUMIDITY */
	if (requested_fields & HUM_MASK) {
		strcat(format, "%s:%u,");

		key[active_fields_index] = HUM_KEY;
		value[active_fields_index] = fields.hum;
		active_fields_index++;
	}

	/* PRESSURE */
	if (requested_fields & PRESS_MASK) {
		strcat(format, "%s:%u,");

		key[active_fields_index] = PRESS_KEY;
		value[active_fields_index] = fields.press;
		active_fields_index++;
	}

	/* LUMINOSITY */
	if (requested_fields & LUM_MASK) {
		strcat(format, "%s:%lu,");

		key[active_fields_index] = LUM_KEY;
		value[active_fields_index] = fields.lum;
		active_fields_index++;
	}

	/* RAIN */
	if (requested_fields & RAIN_MASK) {
		strcat(format, "%s:%u,");

		key[active_fields_index] = RAIN_KEY;
		value[active_fields_index] = fields.rain;
		active_fields_index++;
	}

	/* SALT */
	if (requested_fields & SALT_MASK) {
		strcat(format, "%s:%u");	// No trailing comma, last field

		key[active_fields_index] = SALT_KEY;
		value[active_fields_index] = fields.salt;
		active_fields_index++;
	} else {
		format[strlen(format) - 1] = 0;	// If this wasn't the last field, remove trailing comma
	}

	/* Last curly bracket */
	strcat(format, "}");

	/* Create json_message */
	if ((requested_fields & LATITUDE_MASK) | (requested_fields & LONGITUDE_MASK)) {	// value[1] and [2] are strings
		sprintf(json_message, format,
				NAME_KEY,
				name_value,
				LATITUDE_KEY,
				lat_value,
				LONGITUDE_KEY,
				long_value,
				key[0],
				value[0],
				key[1],
				value[1],
				key[2],
				value[2],
				key[3],
				value[3],
				key[4],
				value[4],
				key[5],
				value[5],
				key[6],
				value[6]
		);
	} else {
		sprintf(json_message, format,
				NAME_KEY,
				name_value,
				key[0],
				value[0],
				key[1],
				value[1],
				key[2],
				value[2],
				key[3],
				value[3],
				key[4],
				value[4],
				key[5],
				value[5],
				key[6],
				value[6]
		);
	}

#ifdef JSON_DYNAMIC_MEM
	// Free allocated space
	vPortFree(name_value);
	vPortFree(lat_value);
	vPortFree(long_value);
	vPortFree(format);
#endif
}

/**************************************************************************/
/*
    Converts double value to string, given that our sprintf implementation can't
    @param val: double value to be converted
    @param decimal_width: number of digits of the decimal part
    @param sout: pointer where result is stored
*/
/**************************************************************************/
void dtostrf (double val, uint8_t decimal_width, char *sout) {
  char fmt[20];
  uint8_t integer_width;

  // Integer part
  int whole = val;

  // Size of integer part
  if (whole <= -100) {
	  integer_width = 4;
  } else if (((whole >= -99) && (whole <= -10)) || (whole >= 100)) {
	  integer_width = 3;
  } else if (((whole >= -9) && (whole <= -1)) || ((whole <= 99) && (whole >= 10))) {
	  integer_width = 2;
  } else {
	  integer_width = 1;
  }

  double mantissa = val - whole;

  int32_t frac = mantissa * powf(10, decimal_width);
  if(frac < 0) frac = -frac;

  sprintf(fmt, "%%0%dd.%%0%dd", integer_width, decimal_width);
  sprintf(sout, fmt, whole, frac);
}
