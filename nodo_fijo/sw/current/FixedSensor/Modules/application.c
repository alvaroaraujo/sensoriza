/*
 * application.c
 *
 *  Created on: 2 ene. 2018
 *      Author: jose
 */

#include "application.h"

#include "board.h"
#include "collector.h"
//#include "about.h"
//#include "power.h"
//#include "powerApp.h"
//#include "tetrisApp.h"
//#include "test.h"
#include "ui.h"

ui_t* ui;
//power_t* power;

//extern ssd1306_t* ssd1306;
//extern void MX_USB_DEVICE_Init(void);

retval_t
application_init(void)
{
//    /* Init modules */
//    if (power_create (&power) != RET_OK)
//    {
//        return RET_ERROR;
//    }
//    if (power_init (power, ui) != RET_OK)
//    {
//        return RET_ERROR;
//    }

    /* Init applications */
//    test_init(ui);
//    powerApp_init(ui);
//    tetris_init(ui);
//    radioApp_init(ui); //XXX-GuilJa
//    about_init(ui);
	collectorApp_init(ui);

    /* Turn on screen */
//    power_lcd_enable (power);
//    ssd1306_attach (ui->screen, true);

    /* Splash screen */
//    ui_showSplash(ui);
    osDelay(500);

    /* Launch UI */
//    display_clear_display(ui->screen->framebuffer);
//XXX-GuilJa    ui_showAppMenu(ui);

    return RET_OK;
}


void StartInitialTask(void const * argument)
{
  /* init code for USB_DEVICE */
//  MX_USB_DEVICE_Init();

  /* Perform initial operations that require OS (like I2C or SPI access) */
  board_init_os();

  /* Init UI */
  ui_init(&ui);

  /* Launch applicaiton */
  application_init();

  osThreadTerminate(NULL);
}
