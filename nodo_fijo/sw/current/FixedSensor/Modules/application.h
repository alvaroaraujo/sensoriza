/*
 * application.h
 *
 *  Created on: 2 ene. 2018
 *      Author: jose
 */

#ifndef APPLICATION_H_
#define APPLICATION_H_

#include "types.h"

retval_t
application_init(void);

void
StartInitialTask(void const * argument);

#endif /* APPLICATION_H_ */
