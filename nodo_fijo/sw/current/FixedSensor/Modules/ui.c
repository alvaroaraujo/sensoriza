/*
 * ui.c
 *
 *  Created on: 2 ene. 2018
 *      Author: jose
 */

#include "ui.h"
//#include "uiResources.h"

//#include "buttons.h"
//#include "debouncer.h"
//#include <string.h>
//#include <stdlib.h>
//#include "cmsis_os.h"
//#include "board_conf.h"

extern const button_t buttonTable[];

osMutexDef (ui_lock);

/* Internal UI functions */
static void
ui_buttonTask (void const * argument);

static retval_t
ui_drawStatusBar (ui_t* ui);

static void
ui_changeApp (ui_t* ui, uint8_t appIndex);

/* Menu app functions */
static retval_t
ui_menu_suspend (app_t* app);

static retval_t
ui_menu_resume (app_t* app);

static retval_t
ui_menu_buttonEvent (app_t* app, uiButton_t button, uiButtonEvent_t event);

retval_t
ui_init (ui_t** ui)//,
//		 ssd1306_t* ssd1306)
{
	/* Allocate memory */
	ui_t* new_ui = pvPortMalloc(sizeof(ui_t));
	app_t* menu = pvPortMalloc (sizeof(app_t));

	/* Initialize */
	new_ui->status = UI_RUNNING;
//	new_ui->screen = ssd1306;
	new_ui->apps = gen_list_init();
	new_ui->lock = osMutexCreate(osMutex(ui_lock));
	new_ui->drawLock = osMutexCreate(osMutex(ui_lock));


	new_ui->title = "";
	new_ui->statusBarInfo.batteryLevel = 0;
	new_ui->statusBarInfo.usbPower = false;

	/* Create menu */
	menu->menuTitle = "Menu";
	menu->mode = UIAPP_NORMAL;
	menu->app_funcs.ui_resume = &ui_menu_resume;
	menu->app_funcs.ui_suspend = &ui_menu_suspend;
	menu->app_funcs.ui_buttonEvent = &ui_menu_buttonEvent;
	ui_addApp(new_ui, menu);

	new_ui->currentApp = NULL;

	/* Launch button thread */
	osThreadDef(buttonThread, ui_buttonTask, osPriorityNormal, 0, 512);
  	new_ui->buttonThread = osThreadCreate(osThread(buttonThread), (void* ) new_ui);

	*ui = new_ui;
	return RET_OK;
}

retval_t
ui_drawLock_adquire (ui_t* ui)
{
	osMutexWait(ui->drawLock, osWaitForever);
	return RET_OK;
}

retval_t
ui_drawLock_release (ui_t* ui)
{
	osMutexRelease(ui->drawLock);
	return RET_OK;
}

retval_t
ui_showSplash (ui_t* ui)
{
	ui_drawLock_adquire (ui);

//XXX-GuilJa	memcpy (ui->screen->framebuffer->buffer, B105ESL, sizeof(B105ESL));
//XXX-GuilJa	display_refresh(ui->screen->framebuffer);

	ui_drawLock_release (ui);
	return RET_OK;
}

retval_t
ui_showAppMenu (ui_t* ui)
{
	/* Menu is always first app */
	ui_changeApp (ui, 0);

	return RET_OK;
}

retval_t
ui_addApp (ui_t* ui,
		   app_t* app)
{
	osMutexWait(ui->lock, osWaitForever);

	gen_list_add(ui->apps, (void*) app);
	app->index = gen_list_get_count(ui->apps);
	app->uiInstance = ui;
	app->state = UISTATUS_NOTRUNNING;

	osMutexRelease(ui->lock);
	return RET_OK;
}

retval_t
ui_setTitle (ui_t* ui,
			 const char *title)
{
	osMutexWait(ui->lock, osWaitForever);

	ui->title = title;

	osMutexRelease(ui->lock);

	/* Update status bar */
	ui_drawStatusBar(ui);
	return RET_OK;
}

retval_t
ui_updateStatusBar (ui_t* ui)
{
	if ((ui->currentApp != NULL) && (ui->currentApp->mode == UIAPP_NORMAL))
	{
		ui_drawStatusBar (ui);
	}

	return RET_OK;
}

static void
ui_changeApp (ui_t* ui, uint8_t appIndex)
{
	uint8_t appCount = gen_list_get_count(ui->apps);

	if (appIndex > appCount)
	{
		return;
	}

	/* Retrieve required app from list */
	gen_list* app_list_element = ui->apps;
	app_t* app = NULL;
	while(app_list_element->next != NULL)
	{
		app = (app_t*) app_list_element->next->item;
		if(app->index == (appIndex + 1))
		{
			break;
		}
		app_list_element = app_list_element->next;
	}
	if (app_list_element == NULL)
	{
		return;
	}

	/* Suspend current app */
	if(ui->currentApp != NULL)
	{
		ui->currentApp->app_funcs.ui_suspend(ui->currentApp);
	}

	/* Set new app as current one */
	osMutexWait(ui->lock, osWaitForever);

	if(ui->currentApp != NULL)
	{
		ui->currentApp->state = UISTATUS_NOTRUNNING;
	}
	ui->currentApp = app;
	ui->currentApp->state = UISTATUS_RUNNING;

	osMutexRelease(ui->lock);

	/* Erase app zone */
	ui_drawLock_adquire (ui);
//XXX-GuilJa	display_clear_display (ui->screen->framebuffer);
	ui_drawLock_release (ui);

	/* Load new app */
	ui->currentApp->app_funcs.ui_resume(ui->currentApp);
}

static retval_t
ui_drawStatusBar (ui_t* ui)
{
	ui_drawLock_adquire (ui);

	/* Erase current status bar */
//XXX-GuilJa	display_fill_rect(ui->screen->framebuffer, 0,0, 128,8,BLACK);
//XXX-GuilJa comento toda la regi�n por ser de display.
//	/* Write new status bar */
//	display_set_cursor(ui->screen->framebuffer, 0, 0);
//	display_set_textsize(ui->screen->framebuffer, 1);
//    display_set_textcolor(ui->screen->framebuffer, WHITE);
//	display_puts (ui->screen->framebuffer, ui->title);
//    display_draw_fast_hline(ui->screen->framebuffer, 0, 7, 128, WHITE);
//
//    /* Battery icon */
//    display_draw_rect(ui->screen->framebuffer, 117,0,10,6, WHITE);
//    display_fill_rect(ui->screen->framebuffer, 118,1,ui->statusBarInfo.batteryLevel * 2,4, WHITE);
//    display_draw_line(ui->screen->framebuffer, 127,1,127,4, WHITE);
//
//    /* Power connected icon */
//    if (ui->statusBarInfo.usbPower)
//    {
//	    display_draw_line(ui->screen->framebuffer, 114,0,112,2, WHITE);
//		display_draw_line(ui->screen->framebuffer, 114,2,112,4, WHITE);
//		display_draw_line(ui->screen->framebuffer, 114,2,112,2, WHITE);
//    }
//
//    /* Perform screen update */
//    display_refresh(ui->screen->framebuffer);

	ui_drawLock_release (ui);

    return RET_OK;
}

static void
ui_buttonTask (void const * argument)
{
	ui_t* ui = (ui_t*) argument;

	while (1)
	{
		/* Read regular buttons */
		for (uint8_t i = 0; i < BUTTON_GPIO_COUNT; i++)
		{
			/* Read button values */
			button_update(&buttonTable[i]);
			/* Check if it was pressed for the first time or unpressed */
			if (buttonTable[i].debouncer->state.currentStateCount == buttonTable[i].debouncer->settings->stableStateCount)
			{
				if ((ui->currentApp != NULL) && (ui->currentApp->app_funcs.ui_buttonEvent!= NULL))
				{
					uiButtonEvent_t event = (buttonTable[i].debouncer->state.currentState == true) ? UIBUTTON_PRESSED : UIBUTTON_RELEASED;
					ui->currentApp->app_funcs.ui_buttonEvent (ui->currentApp, i, event);
				}
			}
			/* Check if there was a long press or a repetition press */
			if (buttonTable[i].debouncer->state.currentState == true)
			{
				if (buttonTable[i].debouncer->state.currentStateCount == UI_BUTTONS_TICKS_LONGPRESS)
				{
					if ((ui->currentApp != NULL) && (ui->currentApp->app_funcs.ui_buttonEvent!= NULL))
					{
						ui->currentApp->app_funcs.ui_buttonEvent (ui->currentApp, i, UIBUTTON_LONGPRESS);
					}
				}
				if ((buttonTable[i].debouncer->state.currentStateCount > UI_BUTTONS_TICKS_REPEAT_THRESHOLD) && 
					(buttonTable[i].debouncer->state.currentStateCount % UI_BUTTONS_TICKS_REPEAT == 0))
				{
					if ((ui->currentApp != NULL) && (ui->currentApp->app_funcs.ui_buttonEvent!= NULL))
					{
						ui->currentApp->app_funcs.ui_buttonEvent (ui->currentApp, i, UIBUTTON_REPEATEDPRESS);
					}
				}
			}
		}

//XXX-GuilJa. comento este cacho porque es del power button de deperita
//		/* Read power button */
//		bq25120buttonState_t buttonState;
//		bq25120_button_read (bq25120, &buttonState);
//		switch (buttonState)
//		{
//			case BQ25120BUTTON_PRESSED:
//				if ((ui->currentApp != NULL) && (ui->currentApp->app_funcs.ui_buttonEvent!= NULL))
//				{
//					ui->currentApp->app_funcs.ui_buttonEvent (ui->currentApp, UIBUTTON_PWR, UIBUTTON_PRESSED);
//				}
//			break;
//			case BQ25120BUTTON_LONGPRESS:
//				ui_showAppMenu(ui);
//			break;
//			default:
//			break;
//		}
		
		osDelay(UI_BUTTONS_TICK_PERIOD);
	}
}

static void
ui_menu_draw (ui_t* ui)
{
	ui_drawLock_adquire (ui);

//	XXX-GuilJa comento todo porque no necestiamos dibujar el menu.
//	/* Erase current menu */
//	display_fill_rect(ui->screen->framebuffer, 0,8, 128,24,BLACK);
//
//	/* Count apps to show (menu should not be shown) */
//	uint8_t appCount = gen_list_get_count(ui->apps) - 1;
//
//	uint8_t verticalCursorPosition = 8;
//	display_set_textsize(ui->screen->framebuffer, 1);
//
//	/* Access to app list. Skip first one (menu) */
//	gen_list* app_list_element = ui->apps->next;
//
//	/* Draw apps */
//	for (uint16_t currentAppIndex = 0; currentAppIndex < appCount; currentAppIndex++)
//	{
//		app_t* app = (app_t*) app_list_element->next->item;
//		app_list_element = app_list_element->next;
//
//		/* If there are more than 3 applications, avoid the ones that not fit into the screen */
//		if (appCount > 3)
//		{
//			uint8_t distance = abs ((int8_t)currentAppIndex - (int8_t)ui->menuSelectedItem);
//
//			if ( ((ui->menuSelectedItem == 0) || (ui->menuSelectedItem == appCount - 1)) &&
//				(distance > 2))
//			{
//				continue;
//			}
//
//			if ( ((ui->menuSelectedItem != 0) && (ui->menuSelectedItem != appCount - 1)) &&
//				(distance > 1))
//			{
//				continue;
//			}
//		}
//
//
//
//		/* Selected app has an '>' at the left */
//		if (currentAppIndex == ui->menuSelectedItem)
//		{
//			display_set_cursor(ui->screen->framebuffer, 0, verticalCursorPosition);
//			display_putstring (ui->screen->framebuffer, ">");
//		}
//
//		/* Write app name */
//		display_set_cursor(ui->screen->framebuffer, 7, verticalCursorPosition);
//		display_puts (ui->screen->framebuffer, app->menuTitle);
//
//		verticalCursorPosition += 8;
//	}
//
//	/* Draw arrows when some apps are not shown */
//	if (appCount > 3)
//	{
//		if (ui->menuSelectedItem > 1)
//		{
//			display_fill_triangle(ui->screen->framebuffer, 120, 14, 124, 14, 122, 12, WHITE);
//		}
//		if (ui->menuSelectedItem < (appCount - 2))
//		{
//			display_fill_triangle(ui->screen->framebuffer, 120, 28, 124, 28, 122, 30, WHITE);
//		}
//	}
//
//	display_refresh(ui->screen->framebuffer);

	ui_drawLock_release (ui);
}

static retval_t
ui_menu_suspend (app_t* app)
{
	return RET_OK;
}

static retval_t
ui_menu_resume (app_t* app)
{
	ui_t* ui = (ui_t*) app->uiInstance;

	ui->menuSelectedItem = 0;

	ui_setTitle (ui, "MENU");
	ui_menu_draw(ui);

	ui_drawLock_adquire (ui);
//XXX-GuilJa	display_refresh(ui->screen->framebuffer);
	ui_drawLock_release (ui);

	return RET_OK;
}

static retval_t
ui_menu_buttonEvent (app_t* app, uiButton_t button, uiButtonEvent_t event)
{
	ui_t* ui = (ui_t*) app->uiInstance;

	switch (button)
	{
		case UIBUTTON_B:
		/* Up */
		if (event == UIBUTTON_PRESSED || event == UIBUTTON_REPEATEDPRESS)
		{
			if(ui->menuSelectedItem > 0)
			{
				ui->menuSelectedItem--;
				ui_menu_draw(ui);
			}
			else
			{
				ui->menuSelectedItem = (gen_list_get_count(ui->apps) - 2);
				ui_menu_draw(ui);
			}
		}
		break;
		case UIBUTTON_A:
		/* Launch selected app (first one is 2) */
		if (event == UIBUTTON_PRESSED)
		{
			ui_changeApp (ui, ui->menuSelectedItem + 1);
		}
		break;
		default:
		break;
	}
	return RET_OK;
}
