/*
 * ui.h
 *
 *  Created on: 5 jun. 2018
 *      Author: telek
 */

#ifndef UI_H_
#define UI_H_

#include "types.h"
#include "generic_list.h"
#include "cmsis_os.h"
#include "debouncer.h"
#include "buttons.h"
#include "board_conf.h"

#include <string.h>
#include <stdlib.h>
#include <stm32l1/gpio_stm32l1.h>

#define UI_BUTTONS_TICK_PERIOD 10
#define UI_BUTTONS_TICKS_LONGPRESS 150
#define UI_BUTTONS_TICKS_REPEAT  5
#define UI_BUTTONS_TICKS_REPEAT_THRESHOLD 30

typedef enum{
	UIBUTTON_PRESSED,
	UIBUTTON_RELEASED,
	UIBUTTON_REPEATEDPRESS,
	UIBUTTON_LONGPRESS
}uiButtonEvent_t;

typedef enum{
	UIBUTTON_A = 0,
	UIBUTTON_B = 1
}uiButton_t;

typedef struct app_{
	uint16_t index;
	struct app_funcs_{
		retval_t (*ui_resume) (struct app_* app);
		retval_t (*ui_suspend) (struct app_* app);
		retval_t (*ui_buttonEvent) (struct app_* app, uiButton_t button, uiButtonEvent_t event);
	}app_funcs;

	const char* menuTitle;

	enum{
		UISTATUS_NOTRUNNING,
		UISTATUS_RUNNING
	}state;

	enum{
		UIAPP_NORMAL,
		UIAPP_FULLSCREEN
	}mode;

	void* uiInstance;
}app_t;

typedef struct ui_ {
	enum{
		UI_NOT_INIT,
		UI_RUNNING
	}status;
	uint8_t menuSelectedItem;

//	ssd1306_t* screen;
	void* screen;

	gen_list* apps;
	app_t* currentApp;

	osThreadId buttonThread;
	osMutexId lock;

	osMutexId drawLock;

	const char* title;
	struct statusBarInfo_{
		uint8_t batteryLevel;
		bool usbPower;
	}statusBarInfo;
}ui_t;

retval_t
ui_init (ui_t** ui);//,
//		 ssd1306_t* ssd1306);

retval_t
ui_showSplash (ui_t* ui);

retval_t
ui_showAppMenu (ui_t* ui);

retval_t
ui_addApp (ui_t* ui,
		   app_t* app);

retval_t
ui_setTitle (ui_t* ui,
			 const char *title);

retval_t
ui_updateStatusBar (ui_t* ui);

retval_t
ui_drawLock_adquire (ui_t* ui);

retval_t
ui_drawLock_release (ui_t* ui);




#endif /* UI_H_ */
