/*
 * buttons.c
 *
 *  Created on: 24 april 2018
 *      Author: guilja
 */

#include "leds.h"

void
led_init(led_t* led)
{
	led->state = OFF;
}

void
led_on(led_t* led)
{
	led->state = ON;
}

void
led_off(led_t* led)
{
	led->state = OFF;
}

void
led_blink(led_t* led)
{
	//TODO
}
