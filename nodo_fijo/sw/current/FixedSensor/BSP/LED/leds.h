#ifndef BSP_LED_LED_H_
#define BSP_LED_LED_H_

#include "os_gpio.h"

typedef enum ledState_ {OFF, ON} ledState_t;

typedef struct led_ {
	gpioPort_t port;
	gpioPin_t pin;
	ledState_t state;
}led_t;

/*
 * Initialize an LED.
 */
void led_init(led_t* button);

/*
 *  Turn on an LED
 */
void led_on(led_t* led);

/*
 *  Turn off an LED
 */
void led_off(led_t* led);

#endif
