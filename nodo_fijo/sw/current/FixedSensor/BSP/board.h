/*
 * board.h
 *
 *  Created on: 23 de may. de 2017
 *      Author: jmartin
 */

#ifndef BOARD_H_
#define BOARD_H_

#include "types.h"
//#include "Buttons/debouncer.h"


retval_t
board_init_bare(void);

retval_t 
board_init_os(void);

#endif /* BOARD_H_ */
