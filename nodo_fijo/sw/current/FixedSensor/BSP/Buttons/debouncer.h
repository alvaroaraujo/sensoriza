/*
 * debouncer.h
 *
 *  Created on: 2 ene. 2018
 *      Author: jose
 */

#ifndef BSP_BUTTONS_DEBOUNCER_H_
#define BSP_BUTTONS_DEBOUNCER_H_

#include "types.h"

typedef struct debouncerSettings_ {
	uint16_t stableStateCount; //! Minimun to consider a state stable
}debouncerSettings_t;

typedef struct debouncerState_ {
	uint16_t currentStateCount; //! Consecutive counts of the same state
	bool currentState;
}debouncerState_t;

typedef struct debouncer_ {
	const debouncerSettings_t* settings;
	debouncerState_t state;
}debouncer_t;

/*!
 * Initialize the debouncer.
 */
void debouncer_init(debouncer_t* debouncer);

/*!
 * Update the debouncer with new value.
 */
void debouncer_update(debouncer_t* debouncer, bool val);

/*!
 * Get the debounced value.
 */
bool debouncer_getValue(debouncer_t* debouncer);

#endif /* BSP_BUTTONS_DEBOUNCER_H_ */
