/*
 * buttons.c
 *
 *  Created on: 2 ene. 2018
 *      Author: jose
 */

#include "buttons.h"

void 
button_init(const button_t* button)
{
	if (button->debouncer != NULL)
	{
		debouncer_init (button->debouncer);
	}
}


void
button_update(const button_t* button)
{
	if (button->debouncer != NULL)
	{
		GPIOstate_t state;
		gpio_pin_read (button->port, button->pin, &state);
		debouncer_update (button->debouncer, state == GPIO_LOW);
	}
}

bool
button_is_pressed(const button_t* button)
{
	if (button->debouncer != NULL)
	{
		return debouncer_getValue (button->debouncer);
	}
	else
	{
		GPIOstate_t state;
		gpio_pin_read (button->port, button->pin, &state);
		return state == GPIO_LOW;
	}
}
