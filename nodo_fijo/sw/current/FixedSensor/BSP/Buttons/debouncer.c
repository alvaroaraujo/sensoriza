/*
 * debouncer.c
 *
 *  Created on: 2 ene. 2018
 *      Author: jose
 */

#include "debouncer.h"

void
debouncer_init(debouncer_t* debouncer)
{
	debouncer->state.currentState = false;
	debouncer->state.currentStateCount = debouncer->settings->stableStateCount;
}

void 
debouncer_update(debouncer_t* debouncer, bool val)
{
	if (debouncer->state.currentState == val)
	{
		debouncer->state.currentStateCount++;
	}
	else
	{
		debouncer->state.currentState = val;
		debouncer->state.currentStateCount = 0;
	}
}

bool
debouncer_getValue(debouncer_t* debouncer)
{
	if (debouncer->state.currentStateCount >= debouncer->settings->stableStateCount)
	{
		return debouncer->state.currentState;
	}
	else
	{
		if (debouncer->state.currentState == true)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
}
