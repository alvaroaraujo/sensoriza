/*
 * buttons.h
 *
 *  Created on: 2 ene. 2018
 *      Author: jose
 */

#ifndef BSP_BUTTONS_BUTTONS_H_
#define BSP_BUTTONS_BUTTONS_H_

#include "types.h"
#include "os_gpio.h"

#include "debouncer.h"

typedef struct button_ {
	const gpioPort_t port;
	const gpioPin_t pin; 
	debouncer_t* debouncer; //! Optional debouncer (if NULL then no debouncing)
}button_t;

/*!
 * Initialize a button.
 * Debouncer (if not NULL) is initialized.
 */
void button_init(const button_t* button);

/*!
 * Update the button: read new pin state and update internal state.
 */
void button_update(const button_t* button);

/*!
 * Check if the button is pressed.
 * If the button is configured with a debouncer (i.e. debouncer!=NULL) then the button is debounced.
 */
bool button_is_pressed(const button_t* button);

#endif /* BSP_BUTTONS_BUTTONS_H_ */
