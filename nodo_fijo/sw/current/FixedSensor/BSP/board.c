/*
 * board.c
 *
 *  Created on: 23 de may. de 2017
 *      Author: jmartin
 */

#include "board.h"
#include "board_conf.h"
#include "os_i2c.h"
#include "os_uart.h"
//#include "os_spi.h"
#include "os_gpio.h"
//#include "drv2605.h"
//#include "ssd1306.h"
//#include "bq25120.h"
//#include "lp5562.h"
//#include "tpa2100p1.h"
#include "Buttons/buttons.h"
//#include "stm32l4xx_hal.h"
//#include "stm32l433xx.h"
#include "stm32l1xx_hal.h"
#include "stm32l1xx.h"
//#include "Accelerometer/adxl362.h"
//#include "Radio/cc1200.h"
#include "tmp006.h"
#include "bme280.h"
#include "tsl2561.h"
#include "bc95.h"

extern tmp006_t* tmp006;
extern uint8_t tmp006_config_data[2];
extern bme280_t* bme280;
extern tsl2561_t* tsl2561;
extern bc95_t* bc95;

///* Peripheral drivers */
i2c_driver_t* i2c1_driver = NULL; //TMP006 driver
i2c_driver_t* i2c2_driver = NULL; //BME280 driver
i2c_driver_t* i2c3_driver = NULL; //TSL2561 driver
uart_driver_t* uart1_driver = NULL; //BC95 driver
//XXX-GuilJa Aqu� no hay estos perif�ricos
//spi_driver_t* spi1_driver = NULL;
//spi_driver_t* spi2_driver = NULL;
//spi_driver_t* usart1spi_driver = NULL;
//
///* IC drivers */
//bq25120_t* bq25120 = NULL;
//drv2605_t* drv2605 = NULL;
//drv2667_t* drv2667 = NULL;
//tpa2100p1_t* tpa2100p1 = NULL;
//ssd1306_t* ssd1306 = NULL;
//lp5562_t* lp5562 = NULL;
//dataflash_t* at45db041e = NULL;
//cc1200_t* cc1200 = NULL;
//adxl362_t* adxl362 = NULL;
//framebuffer_t* framebuffer = NULL;

/* Buttons */
static const debouncerSettings_t debouncer_setts = {
.stableStateCount = BUTTON_STABLE_COUNT
};
static debouncer_t buttonDebouncerA = {
.settings = &debouncer_setts
};

const button_t buttonTable[] = {
{ .port = STM_GPIOA, .pin = STM_GPIO_PIN_0, .debouncer = &buttonDebouncerA }};//,

retval_t
board_init_bare(void)
{
	/* Peripheral drivers */
	if(gpio_init() != RET_OK)
	{
		return RET_ERROR;
	}

	/*I2C driver initialization for TMP006*/
	if(i2c_driver_create(&i2c1_driver, I2C1_DRIVER) != RET_OK)
	{
		return RET_ERROR;
	}
	if(i2c_driver_init(i2c1_driver, 0 , I2C1_SPEED) != RET_OK)
	{
		return RET_ERROR;
	}

	/*I2C driver initialization for BME280*/
	if(i2c_driver_create(&i2c2_driver, I2C1_DRIVER) != RET_OK)
	{
		return RET_ERROR;
	}
	if(i2c_driver_init(i2c2_driver, 0 , I2C1_SPEED) != RET_OK)
	{
		return RET_ERROR;
	}

	/*I2C driver initialization for TSL2561*/
	if(i2c_driver_create(&i2c3_driver, I2C1_DRIVER) != RET_OK)
	{
		return RET_ERROR;
	}
	if(i2c_driver_init(i2c3_driver, 0 , I2C1_SPEED) != RET_OK)
	{
		return RET_ERROR;
	}

	/*UART driver initialization for BC95*/
	if(uart_driver_create(&uart1_driver, UART1_DRIVER) != RET_OK)
	{
		return RET_ERROR;
	}
	if(uart_driver_init(uart1_driver, 0) != RET_OK)
	{
		return RET_ERROR;
	}

	return RET_OK;
}



retval_t 
board_init_os (void)
{
	/* Set red GPIO LED as on */
	gpio_pin_set (STM_GPIOC, STM_GPIO_PIN_8);

//	if(tmp006_create(&tmp006) != RET_OK)
//	{
//		return RET_ERROR;
//	}
//	if(tmp006_init (tmp006,
//					i2c1_driver,
//					tmp006_config_data,
//					STM_GPIOC,
//					STM_GPIO_PIN_8) != RET_OK)
//	{
//		return RET_ERROR;
//	}
//
//	if(bme280_create(&bme280) != RET_OK)
//	{
//		return RET_ERROR;
//	}
//	if(bme280_init(bme280, i2c2_driver) != RET_OK)
//	{
//		return RET_ERROR;
//	}

//	if(tsl2561_create(&tsl2561) != RET_OK)
//	{
//		return RET_ERROR;
//	}
//	if(tsl2561_init(tsl2561, i2c2_driver) != RET_OK)
//	{
//		return RET_ERROR;
//	}

	if(bc95_create(&bc95) != RET_OK)
	{
		return RET_ERROR;
	}
	if(bc95_init(bc95, uart1_driver) != RET_OK)
	{
		return RET_ERROR;
	}

    return RET_OK;
}
