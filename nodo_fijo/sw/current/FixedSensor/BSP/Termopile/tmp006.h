/*
 * tmp006.h
 *
 *  Created on: 10 sept. 2018
 *      Author: telek
 *
 *  Nota: NRND (Not Recomended New Designs) according to Texas Instruments.
 *
 */

#ifndef TERMOPILE_TMP006_H_
#define TERMOPILE_TMP006_H_

#include "types.h"
#include "os_i2c.h"
#include "os_gpio.h"

//CONSTANT VALUES
#define TMP006_B0 -0.0000294
#define TMP006_B1 -0.00000057
#define TMP006_B2 0.00000000463
#define TMP006_C2 13.4
#define TMP006_TREF 298.15
#define TMP006_A2 -0.00001678
#define TMP006_A1 0.00175
#define TMP006_S0 6.4  // * 10^-14

//CONFIG VALUES
#define TMP006_CFG_RESET    0x8000
#define TMP006_CFG_MODEON   0x7000
#define TMP006_CFG_1SAMPLE  0x0000
#define TMP006_CFG_2SAMPLE  0x0200
#define TMP006_CFG_4SAMPLE  0x0400
#define TMP006_CFG_8SAMPLE  0x0600
#define TMP006_CFG_16SAMPLE 0x0800
#define TMP006_CFG_DRDYEN   0x0100
#define TMP006_CFG_DRDY     0x0080

#define TMP006_I2C_ADDRESS	0x80

//Register Addresses
#define TMP006_VOLT_OBJ		0x00
#define TMP006_TEMP_AMB		0x01
#define TMP006_CONFIG_REG	0x02
#define TMP006_MANUF_ID		0xFE
#define TMP006_DEVICE_ID	0xFF

/*XXX-GuilJa. Primero probar sin usar este tipo.*/
//typedef union __attribute__((__packed__)) tmp006_config_ {
//	uint16_t param_val;
//	uint8_t param_bytes[2];
//	struct __attribute__((__packed__))
//	{
//		uint8_t Resrvd: 9;
//		uint8_t EN:1;
//		uint8_t CR:3;
//		uint8_t MOD:3;
//		uint8_t RST:1;
//	}param_bits;
//}tmp06_config_t;

typedef struct tmp006_ {
	i2c_driver_t* i2cDriver;
	uint16_t i2cHandle;
	driver_state_t state;
	osMutexId lock;

	gpioPort_t nDRDYport;
	gpioPin_t nDRDYpin;
}tmp006_t;

///**
//* @brief 				Read the register pointed by regAdd into data.
//* @param device			Pointer to an instance of a tmp006_t.
//* @param regAdd			Address of the register to be read.
//* @param data			Pointer to an array to store de register read.
//* @retval  				RET_OK if successful.
//*/
//static retval_t
//tmp006_register_read (tmp006_t* device,
//						   uint8_t regAdd,
//						   uint8_t* data);
//
///**
//* @brief 				Write the register pointed by regAdd into data.
//* @param device			Pointer to an instance of a tmp006_t.
//* @param regAdd			Address of the register to be written.
//* @param data			Pointer to an array to store de register read.
//* @retval  				RET_OK if successful.
//*/
//static retval_t
//tmp006_register_write (tmp006_t* device,
//						   uint8_t regAdd,
//						   uint8_t* data);

/**
* @brief 				Create an instance of a tmp006 object.
* @param device			Double pointer of an instance of a tmp006_t.
* @retval  				RET_OK if successful.
*/
retval_t
tmp006_create (tmp006_t** device);

/**
* @brief 				Delete an instance of a tmp006 object.
* @param device			Pointer to an instance of a tmp006_t.
* @retval  				RET_OK if successful.
*/
retval_t
tmp006_delete (tmp006_t* device);

/**
* @brief 				Initialization of a tmp006 module.
* @param device			Pointer to an instance of a tmp006_t.
* @param i2cDriver		Pointer to an instance of an i2c driver.
* @param data_config	2 bytes array with the configuration bits set as desired.
* @param nDRDYport		Port of the MCU connected to the DRDY line.
* @param nDRDYpin		Pin of the MCU connected to the DRDY line.
* @retval  				RET_OK if successful.
*/
retval_t
tmp006_init (tmp006_t* device,
              i2c_driver_t* i2cDriver,
//              tmp006_config_t* tmp006config,
			  uint8_t* data_config,
              gpioPort_t nDRDYport,
              gpioPin_t nDRDYpin);

/**
* @brief 				De-initialization of an instance of a tmp006 module. Basically frees
* 						memory used by the instance referred.
* @param device			Pointer to an instance of a tmp006_t.
* @retval  				RET_OK if successful.
*/
retval_t
tmp006_deinit (tmp006_t* device);

/**
* @brief 				Read the value of the temperature register. This temperature is a
* 						value of the local die temperature.
* @param device			Pointer to an instance of a tmp006_t.
* @retval				value of the ambtemp register
*/
int16_t
tmp006_ambTemp_reg(tmp006_t* device);

/***
 @brief 				Read the value of the sensor voltage register.
* @param device			Pointer to an instance of a tmp006_t.
* @retval   			value of the voltage register
*/
int16_t
tmp006_voltage_reg(tmp006_t* device);

/***
 @brief 				Calculate the pointed surface temperature.
* @param device			Pointer to an instance of a tmp006_t.
* @retval  				Calculated temperature x10.
*/
double
tmp006_calcTemp(tmp006_t* device);




#endif /* TERMOPILE_TMP006_H_ */
