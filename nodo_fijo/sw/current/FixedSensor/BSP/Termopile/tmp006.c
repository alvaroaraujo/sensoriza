/*
 * tmp006.c
 *
 *  Created on: 10 sept. 2018
 *      Author: telek
 *
 *  Nota: NRND (Not Recomended New Designs) according to Texas Instruments.
 *
 */

#include "tmp006.h"
#include <math.h>

tmp006_t* tmp006;

uint8_t pointerRegister;

uint8_t tmp006_config_data[2] = {0x79, 0x00}; //MSB first (tmp006 datasheet)

osMutexDef (tmp006_mutex);

static retval_t
tmp006_enable (tmp006_t* device);
static retval_t
tmp006_disable (tmp006_t* device);
static retval_t
tmp006_reset (tmp006_t* device);

static retval_t
tmp006_register_read (tmp006_t* device,
						   uint8_t reg,
						   uint8_t* data)
{
	return i2c_register_read(device->i2cDriver, device->i2cHandle, reg, 1, data, 2);
}

static retval_t
tmp006_register_write (tmp006_t* device,
						   uint8_t reg,
						   uint8_t* data)
{
	return i2c_register_write(device->i2cDriver, device->i2cHandle, reg, 1, data, 2);
}

/* Put the device in continuous mode.*/
static retval_t
tmp006_enable (tmp006_t* device)
{
	uint8_t rdata[2];
	uint8_t wdata[2];
	wdata[0] = 0x00;
	wdata[1] = 0x70; //MOD[2:0] bits to b111.
	tmp006_register_read(device, TMP006_CONFIG_REG, rdata);
	wdata[0] |= rdata[0];
	wdata[1] |= rdata[1];

    return tmp006_register_write(device, TMP006_CONFIG_REG, wdata);
}

/* Put the device in power down mode.*/
static retval_t
tmp006_disable (tmp006_t* device)
{
	uint8_t rdata[2];
	uint8_t wdata[2];
	wdata[0] = 0x00;
	wdata[1] = 0x8F; //MOD[2:0] bits to b000.
	tmp006_register_read(device, TMP006_CONFIG_REG, rdata);
	wdata[0] |= rdata[0];
	wdata[1] &= rdata[1];

    return tmp006_register_write(device, TMP006_CONFIG_REG, wdata);
}

static retval_t
tmp006_reset (tmp006_t* device)
{
	uint8_t data[2];
	data[0] = 0x80; //RESET bit to 1.
	data[1] = 0x00;
    return tmp006_register_write(device, TMP006_CONFIG_REG, data);
}

retval_t
tmp006_create (tmp006_t** device)
{
    /* Allocate memory */
    tmp006_t* new_device = pvPortMalloc(sizeof(tmp006_t));
    if (new_device == NULL)
    {
        return RET_ERROR;
    }

    new_device->lock = osMutexCreate(osMutex(tmp006_mutex));
    new_device->state = DRIVER_NOT_INIT;

    *device = new_device;
    return RET_OK;
}

retval_t
tmp006_delete (tmp006_t* device)
{
    if (device == NULL)
    {
        return RET_ERROR;
    }

    if (device->state != DRIVER_NOT_INIT)
    {
        return RET_ERROR;
    }

    /* Wait for mutex */
    osMutexWait(device->lock, osWaitForever);
    device->state = DRIVER_NOT_INIT;
    osMutexRelease(device->lock);

    /* Delete mutex */
    osMutexDelete(device->lock);

    /* Free memory */
    vPortFree(device);

    return RET_OK;
}

retval_t
tmp006_init (tmp006_t* device,
              i2c_driver_t* i2cDriver,
//              tmp006_config_t* tmp006config,
			  uint8_t* data_config,
              gpioPort_t nDRDYport,
              gpioPin_t nDRDYpin)
{
    if (device == NULL)
    {
        return RET_ERROR;
    }

    if (i2cDriver == NULL)
    {
        return RET_ERROR;
    }

    /* Wait for mutex */
    osMutexWait(device->lock, osWaitForever);

    /* Check for already initialized driver */
    if(device->state != DRIVER_NOT_INIT)
    {
        osMutexRelease(device->lock);
        return RET_ERROR;
    }

    device->i2cDriver = i2cDriver;

    /* Create I2C handle */
    device->i2cHandle = i2c_port_open(device->i2cDriver, TMP006_I2C_ADDRESS, I2C_PORT_MASTER);
    if (device->i2cHandle == -1)
    {
        osMutexRelease(device->lock);
        return RET_ERROR;
    }

//    /* Reset device */
//    if (tmp006_reset(device) != RET_OK)
//    {
//        tmp006_disable(device);
//        i2c_port_close (device->i2cDriver, device->i2cHandle);
//        osMutexRelease(device->lock);
//        return RET_ERROR;
//    }

    /* Setup config parameters */
    if (tmp006_register_write (device,
                                TMP006_CONFIG_REG,
                                data_config) != RET_OK)
    {
        tmp006_disable(device);
        i2c_port_close (device->i2cDriver, device->i2cHandle);
        osMutexRelease(device->lock);
        return RET_ERROR;
    }

    device->state = DRIVER_READY;

    osMutexRelease(device->lock);
    return RET_OK;
}

retval_t
tmp006_deinit (tmp006_t* device)
{
    if (device == NULL)
    {
        return RET_ERROR;
    }

    /* Wait for mutex */
    osMutexWait(device->lock, osWaitForever);

    /* Close I2C port */
    i2c_port_close(device->i2cDriver, device->i2cHandle);

    device->state = DRIVER_NOT_INIT;
    osMutexRelease(device->lock);
    return RET_OK;
}

/**
* @brief 				Read the value of the temperature register. This temperature is a
* 						value of the local die temperature.
* @param device			Pointer to an instance of a tmp006_t.
* @retval				value of the ambtemp register
*/
int16_t
tmp006_ambTemp_reg(tmp006_t* device)
{
	int16_t ambTempReg;
	int8_t ambTemp_data[2];
	int8_t ambTemp_data_aux[2];

	tmp006_register_read(device, TMP006_TEMP_AMB, (uint8_t*)ambTemp_data_aux);

	ambTemp_data[0] = ambTemp_data_aux[1];
	ambTemp_data[1] = ambTemp_data_aux[0];

	ambTempReg = *(int16_t*)ambTemp_data;

	ambTempReg >>= 2;

	return ambTempReg;
}

/***
 @brief 				Read the value of the sensor voltage register.
* @param device			Pointer to an instance of a tmp006_t.
* @retval   			value of the voltage register
*/
int16_t
tmp006_voltage_reg(tmp006_t* device)
{
	int16_t vObjReg;
	int8_t vObj_data[2];
	int8_t vObj_data_aux[2];

	tmp006_register_read(device, TMP006_VOLT_OBJ, (uint8_t*)vObj_data_aux);

	vObj_data[0] = vObj_data_aux[1];
	vObj_data[1] = vObj_data_aux[0];

	vObjReg = *(int16_t*)vObj_data;

	return vObjReg;
}

/***
 @brief 				Calculate the pointed surface temperature.
* @param device			Pointer to an instance of a tmp006_t.
* @retval  				Calculated temperature x10.
*/
double
tmp006_calcTemp(tmp006_t* device)
{

	double Vobj = (double)tmp006_voltage_reg(device);
	double Tdie = (double)tmp006_ambTemp_reg(device);
	Tdie *= 0.03125; // convert to celsius
	Tdie += 273.15; // convert to kelvin

	Vobj *= 156.25;  // 156.25 nV per LSB
	Vobj /= 1000; // nV -> uV
	Vobj /= 1000; // uV -> mV
	Vobj /= 1000; // mV -> V

   double tdie_tref = Tdie - TMP006_TREF;
   double S = (1 + TMP006_A1*tdie_tref + TMP006_A2*tdie_tref*tdie_tref);
   S *= TMP006_S0;
   S /= 10000000;
   S /= 10000000;

   double Vos = TMP006_B0 + TMP006_B1*tdie_tref + TMP006_B2*tdie_tref*tdie_tref;

   double fVobj = (Vobj - Vos) + TMP006_C2*(Vobj-Vos)*(Vobj-Vos);

   double Tobj = sqrt(sqrt(Tdie * Tdie * Tdie * Tdie + fVobj/S));

   Tobj -= 273.15; // Kelvin -> *C

   return Tobj;

/**************************************************************/


}
