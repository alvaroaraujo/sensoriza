/*
 * os_uart.c
 *
 *  Created on: 10 dic. 2018
 *      Author: telek
 *
 *  XXX-GuilJa
 *  Basic UART functions.
 *  Implemented:
 *  	driver_create
 *  	driver_delete
 *  	driver_init
 *  	driver_deinit
 *  	port_open
 *  	port_close
 *  	port_check
 *  	uart_write
 *  Not implemented:
 *  	uart_read
 *  	uart_read_reg
 *  	uart_write_reg
 */

#include "os_uart.h"
#include "board_conf.h"


osMutexDef (os_uart_mutex);

#ifdef OS_UART_DRIVER_FUNCS
extern uart_funcs_t OS_UART_DRIVER_FUNCS;
#else
#error	"UART FUNCTION PREFIX NOT DEFINED"
#endif

retval_t
uart_driver_create(uart_driver_t** uart_driver,
				   uint8_t uart_periph_number)
{
	if(uart_periph_number >16)
	{
		return RET_ERROR;
	}

	/* Allocate memory */
	uart_driver_t* newUartdriver = pvPortMalloc(sizeof(uart_driver_t));
	newUartdriver->uart_vars = (uart_vars_t*) pvPortMalloc(sizeof(uart_vars_t));

	/* Initialize driver */
	newUartdriver->uart_vars->uart_periph_number = uart_periph_number;
	newUartdriver->uart_vars->opened_ports = NULL;
	newUartdriver->uart_vars->driver_state = DRIVER_NOT_INIT;
	newUartdriver->uart_vars->uart_mutex_id = osMutexCreate(osMutex(os_uart_mutex));
	newUartdriver->uart_funcs = OS_UART_DRIVER_FUNCS;

	*uart_driver = newUartdriver;
	return RET_OK;
}

retval_t
uart_driver_delete(uart_driver_t* uart_driver)
{
	if(uart_driver == NULL)
	{
		return RET_ERROR;
	}

	/* Wait until driver is free */
	osMutexWait(uart_driver->uart_vars->uart_mutex_id, osWaitForever);
	while(uart_driver->uart_vars->driver_state == DRIVER_BUSY);

	/* Deinitialize driver */
	uart_driver->uart_vars->opened_ports = NULL;
	uart_driver->uart_vars->driver_state = DRIVER_NOT_INIT;
	uart_driver->uart_vars->uart_periph_number = 255;
	osMutexRelease(uart_driver->uart_vars->uart_mutex_id);

	/* Free memory */
	osMutexDelete(uart_driver->uart_vars->uart_mutex_id);
	vPortFree(uart_driver->uart_vars);
	uart_driver->uart_vars = NULL;
	vPortFree(uart_driver);

	return RET_OK;
}


retval_t
uart_driver_init (uart_driver_t* uart_driver,
				 uint32_t uart_timeout)
{
	if(uart_driver == NULL)
	{
		return RET_ERROR;
	}

	osMutexWait(uart_driver->uart_vars->uart_mutex_id, osWaitForever);

	/* Check for already initialized driver */
	if(uart_driver->uart_vars->driver_state != DRIVER_NOT_INIT)
	{
		osMutexRelease(uart_driver->uart_vars->uart_mutex_id);
		return RET_ERROR;
	}

	/* Set timeout accordingly */
	if(uart_timeout == 0)
	{
		uart_driver->uart_vars->default_uart_timeout = osWaitForever;
	}
	else
	{
		uart_driver->uart_vars->default_uart_timeout = uart_timeout;
	}

	/* Try to initialize peripheral */
	if(uart_driver->uart_funcs.uart_init(uart_driver, uart_timeout) != RET_OK)
	{
		osMutexRelease(uart_driver->uart_vars->uart_mutex_id);
		return RET_ERROR;
	}

	/* Create port list */
	uart_driver->uart_vars->opened_ports = gen_list_init();
	/* Set as ready and return */
	uart_driver->uart_vars->driver_state = DRIVER_READY;

	osMutexRelease(uart_driver->uart_vars->uart_mutex_id);
	return RET_OK;
}

retval_t
uart_driver_deinit (uart_driver_t* uart_driver)
{
	if(uart_driver == NULL)
	{
		return RET_ERROR;
	}

	osMutexWait(uart_driver->uart_vars->uart_mutex_id, osWaitForever);

	/* Avoid deinit of driver if it is being used */
	if(uart_driver->uart_vars->driver_state != DRIVER_READY)
	{
		osMutexRelease(uart_driver->uart_vars->uart_mutex_id);
		return RET_ERROR;
	}

	/* Try to deinitialize peripheral */
	if(uart_driver->uart_funcs.uart_deinit(uart_driver) != RET_OK)
	{
		osMutexRelease(uart_driver->uart_vars->uart_mutex_id);
		return RET_ERROR;
	}

	/* Free all driver ports */
	gen_list* current = uart_driver->uart_vars->opened_ports;
	uart_port_t* opened_port;
	/* Get next opened port*/
	while(current->next != NULL)
	{
		opened_port = (uart_port_t*) current->next->item;

		/* Wait until port is ready */
		while(opened_port->port_state !=  DRIVER_PORT_OPENED_READY);

		vPortFree(opened_port);
		gen_list* next = current->next->next;
		vPortFree(current->next);
		current->next = next;
	}

	gen_list_remove_all(uart_driver->uart_vars->opened_ports);
	uart_driver->uart_vars->opened_ports = NULL;
	uart_driver->uart_vars->driver_state = DRIVER_NOT_INIT;

	osMutexRelease(uart_driver->uart_vars->uart_mutex_id);
	return RET_OK;
}

/**
 *
 * @param uart_driver->uart_vars
 * @param device_addr
 * @return
 */
int16_t
uart_port_open(uart_driver_t* uart_driver)
{
	if(uart_driver == NULL)
	{
		return -1;
	}

	osMutexWait(uart_driver->uart_vars->uart_mutex_id, osWaitForever);

	if(uart_driver->uart_vars->driver_state == DRIVER_NOT_INIT)
	{		//Return error if the driver is not initialized
		osMutexRelease(uart_driver->uart_vars->uart_mutex_id);
		return -1;
	}

	uart_port_t* new_uart_port = (uart_port_t*) pvPortMalloc(sizeof(uart_port_t));

	new_uart_port->port_id = 0;								//Check the opened port list to select an ordered available port_id

	uart_port_t* temp_list_uart_port;
	gen_list* current = uart_driver->uart_vars->opened_ports;
	uint8_t repeat_loop = 1;
	while(repeat_loop)
	{
		repeat_loop = 0;
		current = uart_driver->uart_vars->opened_ports;
		while(current->next != NULL){
			temp_list_uart_port = (uart_port_t*) current->next->item;
			if(temp_list_uart_port->port_id == new_uart_port->port_id){
				repeat_loop = 1;
				new_uart_port->port_id++;
			}
			current = current->next;
		}
	}

	new_uart_port->port_state = DRIVER_PORT_OPENED_READY;

	gen_list_add(uart_driver->uart_vars->opened_ports, (void*) new_uart_port);
	osMutexRelease(uart_driver->uart_vars->uart_mutex_id);
	return new_uart_port->port_id;
}

/**
 *
 * @param uart_driver->uart_vars
 * @param port_number
 * @return
 */
retval_t
uart_port_close(uart_driver_t* uart_driver,
			   int16_t port_number)
{
	if(uart_driver == NULL)
	{		//return error if the driver variables does not exist
		return RET_ERROR;
	}

	osMutexWait(uart_driver->uart_vars->uart_mutex_id, osWaitForever);

	if(uart_driver->uart_vars->driver_state == DRIVER_NOT_INIT)
	{		//Return error if the driver is not initialized
		osMutexRelease(uart_driver->uart_vars->uart_mutex_id);
		return RET_ERROR;
	}

	gen_list* current = uart_driver->uart_vars->opened_ports;
	uart_port_t* temp_list_uart_port;

	while(current->next != NULL)
	{

		temp_list_uart_port = (uart_port_t*) current->next->item;
		if(temp_list_uart_port->port_id == port_number)
		{

			while(temp_list_uart_port->port_state !=  DRIVER_PORT_OPENED_READY);			//Wait till the port is ready to close it(end all transmissions)

			temp_list_uart_port->port_id = -1;
			gen_list* next = current->next->next;
			temp_list_uart_port->port_state = DRIVER_PORT_CLOSED;
			vPortFree(temp_list_uart_port);
			vPortFree(current->next);
			current->next = next;

			osMutexRelease(uart_driver->uart_vars->uart_mutex_id);
			return RET_OK;										//Port succesfully deleted
		}
		else
		{
			current = current->next;
		}

	}

	osMutexRelease(uart_driver->uart_vars->uart_mutex_id);
	return RET_ERROR;										//Port not found. Not deleted
}

/**
 *
 * @param uart_driver->uart_vars
 * @param port_number
 * @return
 */
port_state_t
uart_port_check(uart_driver_t* uart_driver,
			   int16_t port_number)
{
	/* Check that the driver exists */
	if(uart_driver == NULL)
	{
		return DRIVER_PORT_CLOSED;
	}

	/* Adquire mutex and check if the driver has been initialized */
	osMutexWait(uart_driver->uart_vars->uart_mutex_id, osWaitForever);
	if(uart_driver->uart_vars->driver_state == DRIVER_NOT_INIT)
	{
		osMutexRelease(uart_driver->uart_vars->uart_mutex_id);
		return DRIVER_PORT_CLOSED;
	}

	/* Check that the supplied port number exists */
	gen_list* current = uart_driver->uart_vars->opened_ports;
	uart_port_t* temp_list_uart_port;
	while(current->next != NULL)
	{
		temp_list_uart_port = (uart_port_t*) current->next->item;
		if(temp_list_uart_port->port_id == port_number)
		{
			port_state_t port_state = temp_list_uart_port->port_state;
			osMutexRelease(uart_driver->uart_vars->uart_mutex_id);

			return port_state;
		}
		else
		{
			current = current->next;
		}
	}

	osMutexRelease(uart_driver->uart_vars->uart_mutex_id);
	return DRIVER_PORT_CLOSED;
}

retval_t
uart_write (uart_driver_t* uart_driver,
		   int16_t port_number,
		   uint8_t *ptx,
		   uint16_t size)
{
	/* Check that the driver exists */
	if(uart_driver == NULL)
	{
		return RET_ERROR;
	}

	/* Adquire mutex and check if the driver has been initialized */
	osMutexWait(uart_driver->uart_vars->uart_mutex_id, osWaitForever);
	if(uart_driver->uart_vars->driver_state == DRIVER_NOT_INIT)
	{
		osMutexRelease(uart_driver->uart_vars->uart_mutex_id);
		return RET_ERROR;
	}

	/* Wait until the driver is available */
	while(uart_driver->uart_vars->driver_state == DRIVER_BUSY);

	/* Check that the supplied port number exists and has the adequate configuration for this operation */
	gen_list* current = uart_driver->uart_vars->opened_ports;
	uart_port_t* temp_list_uart_port;
	uart_port_t* currentPort = NULL;
	while(current->next != NULL)
	{
		temp_list_uart_port = (uart_port_t*) current->next->item;
		if(temp_list_uart_port->port_id == port_number)
		{
			/* Wait till the port is ready to read(end all transmissions) */
			while(temp_list_uart_port->port_state !=  DRIVER_PORT_OPENED_READY);
			/* Store port to be operated */
			currentPort = (uart_port_t*) current->next->item;
			break;
		}
		else
		{
			/* Get the next element in the opened port list */
			current = current->next;
		}
	}
	/* If the port was not found, return error */
	if (currentPort == NULL)
	{
		osMutexRelease(uart_driver->uart_vars->uart_mutex_id);
		return RET_ERROR;
	}

	currentPort->port_state = DRIVER_PORT_WRITING;

	retval_t result = uart_driver->uart_funcs.uart_write(uart_driver,
													  ptx,
													  size);

	currentPort->port_state = DRIVER_PORT_OPENED_READY;

	/* Release the mutex */
	osMutexRelease(uart_driver->uart_vars->uart_mutex_id);
	return result;
}
