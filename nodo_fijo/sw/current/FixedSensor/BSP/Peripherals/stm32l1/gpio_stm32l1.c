/*
 * gpio_stm32l1.c
 *
 *  Created on: 5 mar. 2018
 *      Author: jmartin
 */
#include <stm32l1/gpio_stm32l1.h>
#include "stm32l1xx_hal.h"
#include "generic_list.h"
#include "os_gpio.h"
#include "FreeRTOS.h"

typedef struct gpioInterrupt_{
	gpioPort_t port;
	gpioPin_t pin;
	void (*callback)(void*);
	void* instance;
}gpioInterrupt_t;

gen_list* gpioInteruptList = 0;

retval_t
gpio_init (void)
{
	gpioInteruptList = gen_list_init();
	return RET_OK;
}

retval_t
gpio_pin_init(gpioPort_t port,
		 	  gpioPin_t pin, 
		 	  GPIOmode_t mode, 
		 	  GPIOpull_t pull)
{
	/* Check that pin information is inside bounds */
	if((port > STM32L1_MAX_PORT) || (pin > STM32L1_MAX_PIN))
	{
		return RET_ERROR;
	}

	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_TypeDef* GPIO_Port = (GPIO_TypeDef*) (GPIOA_BASE + ((port*4)<<8));
	GPIO_InitStruct.Pin = (0x0001<<pin);

	switch(mode)
	{
	case GPIO_PIN_INPUT:
		GPIO_InitStruct.Mode = GPIO_MODE_INPUT ;
		break;
	case GPIO_PIN_OUTPUT_PUSH_PULL:
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		break;
	case GPIO_PIN_OUTPUT_OPEN_DRAIN:
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
		break;
	case GPIO_PIN_INTERRUPT_FALLING:
		GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
		break;
	case GPIO_PIN_INTERRUPT_RISING:
		GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
		break;
	case GPIO_PIN_INTERRUPT_FALLING_RISING:
		GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
		break;
	case GPIO_PIN_ANALOG:
		GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
		break;
	default:
		return RET_ERROR;
		break;
	}

	switch(pull)
	{
	case GPIO_PIN_PULLUP:
		GPIO_InitStruct.Pull = GPIO_PULLUP;
		break;
	case GPIO_PIN_PULLDOWN:
		GPIO_InitStruct.Pull = GPIO_PULLDOWN;
		break;
	case GPIO_PIN_NO_PULL:
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		break;
	default:
		return RET_ERROR;
		break;
	}

	HAL_GPIO_Init(GPIO_Port, &GPIO_InitStruct);

	if((mode == GPIO_PIN_INTERRUPT_FALLING) || 
	   (mode == GPIO_PIN_INTERRUPT_RISING) || 
	   (mode == GPIO_PIN_INTERRUPT_FALLING_RISING))
	{
		switch (GPIO_InitStruct.Pin)
		{
			case GPIO_PIN_0:
			HAL_NVIC_SetPriority(EXTI0_IRQn, 5, 0);
			HAL_NVIC_EnableIRQ(EXTI0_IRQn);
			break;
			case GPIO_PIN_1:
			HAL_NVIC_SetPriority(EXTI1_IRQn, 5, 0);
			HAL_NVIC_EnableIRQ(EXTI1_IRQn);
			break;
			case GPIO_PIN_2:
			HAL_NVIC_SetPriority(EXTI2_IRQn, 5, 0);
			HAL_NVIC_EnableIRQ(EXTI2_IRQn);
			break;
			case GPIO_PIN_3:
			HAL_NVIC_SetPriority(EXTI3_IRQn, 5, 0);
			HAL_NVIC_EnableIRQ(EXTI3_IRQn);
			break;
			case GPIO_PIN_4:
			HAL_NVIC_SetPriority(EXTI4_IRQn, 5, 0);
			HAL_NVIC_EnableIRQ(EXTI4_IRQn);
			break;
			case GPIO_PIN_5:
			case GPIO_PIN_6:
			case GPIO_PIN_7:
			case GPIO_PIN_8:
			case GPIO_PIN_9:
			HAL_NVIC_SetPriority(EXTI9_5_IRQn, 5, 0);
			HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
			break;
			case GPIO_PIN_10:
			case GPIO_PIN_11:
			case GPIO_PIN_12:
			case GPIO_PIN_13:
			case GPIO_PIN_14:
			case GPIO_PIN_15:
			HAL_NVIC_SetPriority(EXTI15_10_IRQn, 5, 0);
			HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
			break;
		}
	}

	
	/*switch(port)
	{
	case 0:
		__HAL_RCC_GPIOA_CLK_ENABLE();
		break;
	case 1:
		__HAL_RCC_GPIOB_CLK_ENABLE();
		break;
	case 2:
		__HAL_RCC_GPIOC_CLK_ENABLE();
		break;
	case 3:
		__HAL_RCC_GPIOD_CLK_ENABLE();
		break;
	case 4:
		__HAL_RCC_GPIOE_CLK_ENABLE();
		break;
	case 7:
		__HAL_RCC_GPIOH_CLK_ENABLE();
		break;
	default:
		return RET_ERROR;
		break;
	}*/

	return RET_OK;
}

retval_t
gpio_pin_set (gpioPort_t port,
			  gpioPin_t pin)
{
	GPIO_TypeDef* GPIO_Port = (GPIO_TypeDef*) (GPIOA_BASE + ((port*4)<<8));
	uint16_t hwPin = (0x0001<<pin);
	HAL_GPIO_WritePin(GPIO_Port, hwPin, GPIO_PIN_SET);
	return RET_OK;
}

retval_t
gpio_pin_reset (gpioPort_t port,
				gpioPin_t pin)
{
	GPIO_TypeDef* GPIO_Port = (GPIO_TypeDef*) (GPIOA_BASE + ((port*4)<<8));
	uint16_t hwPin = (0x0001<<pin);
	HAL_GPIO_WritePin(GPIO_Port, hwPin, GPIO_PIN_RESET);
	return RET_OK;
}

retval_t
gpio_pin_toggle (gpioPort_t port,
				 gpioPin_t pin)
{
	GPIO_TypeDef* GPIO_Port = (GPIO_TypeDef*) (GPIOA_BASE + ((port*4)<<8));
	uint16_t hwPin = (0x0001<<pin);
	HAL_GPIO_TogglePin(GPIO_Port, hwPin);
	return RET_OK;
}


retval_t
gpio_pin_read (gpioPort_t port,
			   gpioPin_t pin,
			   GPIOstate_t* state)
{
	GPIO_TypeDef* GPIO_Port = (GPIO_TypeDef*) (GPIOA_BASE + ((port*4)<<8));
	uint16_t hwPin = (0x0001<<pin);
	*state = (HAL_GPIO_ReadPin(GPIO_Port, hwPin) == GPIO_PIN_RESET) ? GPIO_LOW : GPIO_HIGH;
	return RET_OK;
}

retval_t
gpio_pin_setIntCallback (gpioPort_t port,
						 gpioPin_t pin,
						 void (*callback)(void*),
						 void* instance)
{
	/* Aux vars to use on the pin list */
	gen_list* currentItem = gpioInteruptList;
	gpioInterrupt_t* currentPin;

	/* Check if the pin callback has already been configured */
	while(currentItem->next != NULL)
	{
		currentPin = (gpioInterrupt_t*) currentItem->next->item;

		if (currentPin->port == port &&
			currentPin->pin == pin)
		{
			/* Replace previous configuration */
			currentPin->callback = callback;
			currentPin->instance = instance;
			return RET_OK;
		}
		currentItem = currentItem->next;
	}

	/* Add the new callback for that pin */
	gpioInterrupt_t* pinInt = (gpioInterrupt_t*) pvPortMalloc(sizeof(gpioInterrupt_t));
	pinInt->port = port;
	pinInt->pin = pin;
	pinInt->callback = callback;
	pinInt->instance = instance;
	gen_list_add (gpioInteruptList, pinInt);

	return RET_OK;
}

void
HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if (gpioInteruptList == NULL)
	{
		return;
	}

	/* Decode pin number */
	uint8_t pinNumber = 0;
	while (pinNumber <= STM32L1_MAX_PIN)
	{
		if ((GPIO_Pin >> pinNumber) & 0x01)
		{
			break;
		}
		pinNumber++;
	}

	if (pinNumber > STM32L1_MAX_PIN)
	{
		return;
	}

	/* Aux vars to use on the pin list */
	gen_list* currentItem = gpioInteruptList;
	gpioInterrupt_t* currentPin;

	/* Check if the pin callback is configured */
	while(currentItem->next != NULL)
	{
		currentPin = (gpioInterrupt_t*) currentItem->next->item;

		if (currentPin->pin == pinNumber &&
			currentPin->callback != NULL)
		{
			/* Call provided callback */
			currentPin->callback (currentPin->instance);
		}
		currentItem = currentItem->next;
	}
}

/**
* @brief This function handles EXTI line0 interrupt.
*/
void EXTI0_IRQHandler(void)
{
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);
}

/**
* @brief This function handles EXTI line1 interrupt.
*/
void EXTI1_IRQHandler(void)
{
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1);
}

/**
* @brief This function handles EXTI line2 interrupt.
*/
void EXTI2_IRQHandler(void)
{
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_2);
}

/**
* @brief This function handles EXTI line3 interrupt.
*/
void EXTI3_IRQHandler(void)
{
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_3);
}

/**
* @brief This function handles EXTI line4 interrupt.
*/
void EXTI4_IRQHandler(void)
{
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_4);
}


/**
* @brief This function handles EXTI line[9:5] interrupts.
*/
void EXTI9_5_IRQHandler(void)
{
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_5);
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_6);
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_7);
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_8);
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_9);
}

/**
* @brief This function handles EXTI line[15:10] interrupts.
*/
void EXTI15_10_IRQHandler(void)
{
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_10);
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_11);
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_12);
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_13);
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_14);
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_15);
}
