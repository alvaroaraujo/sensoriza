/*
 * gpio_stm32l1.h
 *
 *  Created on: 7 mar. 2018
 *      Author: jmartin
 */

#ifndef GPIO_STM32L1_H_
#define GPIO_STM32L1_H_

#define STM32L1_MAX_PORT	8
#define STM32L1_MAX_PIN		15

#define STM_GPIOA			0
#define STM_GPIOB			1
#define STM_GPIOC			2
#define STM_GPIOD			3
#define STM_GPIOE			4
#define STM_GPIOF			5
#define STM_GPIOG			6
#define STM_GPIOH			7
#define STM_GPIOI			8

#define STM_GPIO_PIN_0		0
#define STM_GPIO_PIN_1		1
#define STM_GPIO_PIN_2		2
#define STM_GPIO_PIN_3		3
#define STM_GPIO_PIN_4		4
#define STM_GPIO_PIN_5		5
#define STM_GPIO_PIN_6		6
#define STM_GPIO_PIN_7		7
#define STM_GPIO_PIN_8		8
#define STM_GPIO_PIN_9		9
#define STM_GPIO_PIN_10		10
#define STM_GPIO_PIN_11		11
#define STM_GPIO_PIN_12		12
#define STM_GPIO_PIN_13		13
#define STM_GPIO_PIN_14		14
#define STM_GPIO_PIN_15		15


#endif /* GPIO_STM32L1_H_ */
