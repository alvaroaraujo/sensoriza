/*
 * i2c_stm32l1.c
 *
 *  Created on: 10 sept. 2018
 *      Author: telek
 */

//XXX-GuilJa. BORRAR. Est� en main.c #include "i2c.h"
#include "../os_i2c.h"
#include "cmsis_os.h"
#include "types.h"
#include "stm32l1xx_hal.h"

#include "board_conf.h"

extern I2C_HandleTypeDef hi2c1;

i2c_funcs_t stm32l1_i2c_funcs;

osSemaphoreDef(os_i2c_semaphore);

osSemaphoreId i2c1_semaphore_id;
retval_t i2c1_result;
osSemaphoreId i2c2_semaphore_id;


// CUIDADO. LAS FUNCIONES NON BLOCKING USANDO DMA EN EL BUS I2C NO TERMINAN DE FUNCIONAR. SE CUELGAN A VECES

/*XXX-GuilJa. REVISAR En las diferentes operaciones se marca el estado del driver como busy y ready. Sin embargo, no
 * se comprueba como est� para realizar la operaci�n. Creo que, en �ltimo termino eso puede hacer que se
 * quede bloqueado. En el supuesto de que hagamos un read y estemos a la espera de que salga del sem�foro
 * porque no haya terminado la operaci�n de la HAL, podr�amos llamar a una operaci�n write (u otra read), en
 * ambos casos se llegar�a a llamar a la "HAL_DMA" que no har�a nada por estar busy. Sin emabrgo nosotros
 * entrar�amos en espera en el sem�foro. Esto podr�a darse varias veces y sin embargo todos los semaforos se
 * liberar�an (supongo) por ser binarios. Adem�s hay que tener en cuenta que las llamadas a HAL_DMA no habr�an
 * sido satisfactorias (excepto la primera) y ni siquiera tendr�amos constancia de ello. Hay que tener en cuenta que
 * a estas funciones se las llama de forma indirecta a trav�s de os_i2c.c y all� si hay un while que espera a que el
 * driver no est� busy. Luego lo mismo s� que est� bien la implementaci�n.*/

/* I2C FUNCTIONS FOR stm32l1*/
retval_t
stm32l1_i2c_init(i2c_driver_t* i2c_driver,
				 uint8_t own_addr,
				 uint32_t default_i2c_timeout);
retval_t
stm32l1_i2c_deinit(i2c_driver_t* i2c_driver);
retval_t
stm32l1_i2c_read(i2c_driver_t* i2c_driver,
				 uint16_t device_addr,
				 uint8_t *prx,
				 uint16_t size);
retval_t
stm32l1_i2c_write(i2c_driver_t* i2c_driver,
				  uint16_t device_addr,
				  uint8_t *ptx,
				  uint16_t size);
retval_t
stm32l1_i2c_reg_read(i2c_driver_t* i2c_driver,
					 uint16_t device_addr,
					 uint16_t reg_addr,
					 uint16_t reg_addr_size,
					 uint8_t *prx,
					 uint16_t size);
retval_t
stm32l1_i2c_reg_write(i2c_driver_t* i2c_driver,
					  uint16_t device_addr,
					  uint16_t reg_addr,
					  uint16_t reg_addr_size,
					  uint8_t *ptx,
					  uint16_t size);

i2c_funcs_t stm32l1_i2c_funcs =
{
		stm32l1_i2c_init,
		stm32l1_i2c_deinit,
		stm32l1_i2c_read,
		stm32l1_i2c_write,
		stm32l1_i2c_reg_read,
		stm32l1_i2c_reg_write,
};


/**
 *
 * @param i2c_driver->i2c_vars
 * @param own_addr
 * @param default_i2c_timeout
 * @return
 */
retval_t stm32l1_i2c_init(i2c_driver_t* i2c_driver, uint8_t own_addr, uint32_t default_i2c_timeout)
{
	/* Initialize peripheral through HAL interface */
	switch(i2c_driver->i2c_vars->i2c_periph_number)
	{
	case 1:
		/* Initialize peripheral variables */
		  hi2c1.Instance = I2C1;
		  hi2c1.Init.ClockSpeed = 100000;
		  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
		  hi2c1.Init.OwnAddress1 = 0;
		  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
		  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
		  hi2c1.Init.OwnAddress2 = 0;
		  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
		  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
		  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
		  {
			  return RET_ERROR;
		  }

		/* Create semaphore for non-blocking operations */
		if(i2c1_semaphore_id == NULL)
		{
			i2c1_semaphore_id = osSemaphoreCreate(osSemaphore(os_i2c_semaphore), 1);
			osSemaphoreWait(i2c1_semaphore_id, osWaitForever);
		}
		break;
	case 2:
		break;
	case 3:
		break;
	default:
		return RET_ERROR;
		break;
	}
	return RET_OK;
}

/**
 *
 * @param i2c_driver->i2c_vars
 * @return
 */
retval_t stm32l1_i2c_deinit(i2c_driver_t* i2c_driver)
{
	switch(i2c_driver->i2c_vars->i2c_periph_number)
	{
	case 1:
		if (HAL_I2C_DeInit(&hi2c1) != HAL_OK)
		{
			return RET_ERROR;
		}
		osSemaphoreRelease(i2c1_semaphore_id);
		osSemaphoreDelete(i2c1_semaphore_id);
		i2c1_semaphore_id = NULL;
		break;
	case 2:
		break;
	case 3:
		break;
	default:
		return RET_ERROR;
		break;
	}
	return RET_OK;
}

/**
 *
 * @param i2c_driver->i2c_vars
 * @param port_number
 * @param prx
 * @param size
 * @return
 */

retval_t
stm32l1_i2c_read(i2c_driver_t* i2c_driver,
				 uint16_t device_addr,
				 uint8_t *prx,
				 uint16_t size)
{
	HAL_StatusTypeDef opResult;

	switch(i2c_driver->i2c_vars->i2c_periph_number)
	{
	case 1:
		i2c1_result = RET_OK;

		/* Put driver in busy mode */
		i2c_driver->i2c_vars->driver_state = DRIVER_BUSY;

#ifdef I2C_NONBLOCKINGMODE
		/* Do the actual read */
		opResult = HAL_I2C_Master_Receive_DMA(&hi2c1,
									  		 device_addr,
										     prx,
											 size);

		osSemaphoreWait(i2c1_semaphore_id, i2c_driver->i2c_vars->default_i2c_timeout);
#else
		/* Do the actual read */
		opResult = HAL_I2C_Master_Receive(&hi2c1,
								    	  device_addr,
								    	  prx, size,
								    	  i2c_driver->i2c_vars->default_i2c_timeout);
#endif

		i2c_driver->i2c_vars->driver_state = DRIVER_READY;

		if (opResult != HAL_OK)
		{
			return RET_ERROR;
		}

		if (i2c1_result != RET_OK)
		{
			return RET_ERROR;
		}

		opResult = i2c1_result;

		break;
	case 2:
		break;
	case 3:
		break;
	default:
		return RET_ERROR;
		break;
	}

	/* Return the result of the opration. It will be RET_ERROR if the Error callback has been called by the HAL. */
	return opResult;
}

/**
 *
 * @param i2c_driver->i2c_vars
 * @param port_number
 * @param ptx
 * @param size
 * @return
 */
retval_t
stm32l1_i2c_write(i2c_driver_t* i2c_driver,
				  uint16_t device_addr,
				  uint8_t *ptx,
				  uint16_t size)
{
	HAL_StatusTypeDef opResult;

	switch(i2c_driver->i2c_vars->i2c_periph_number)
	{
	case 1:
		i2c1_result = RET_OK;

		/* Put driver in busy mode */
		i2c_driver->i2c_vars->driver_state = DRIVER_BUSY;

#ifdef I2C_NONBLOCKINGMODE
		/* Do the actual read */
		opResult = HAL_I2C_Master_Transmit_DMA(&hi2c1,
										       device_addr,
										       ptx,
										       size);
		if (opResult != HAL_OK)
		{
			i2c_driver->i2c_vars->driver_state = DRIVER_READY;
			return RET_ERROR;
		}
		osSemaphoreWait(i2c1_semaphore_id, i2c_driver->i2c_vars->default_i2c_timeout);
#else
		/* Do the actual write */
		opResult = HAL_I2C_Master_Transmit(&hi2c1,
									 	  device_addr,
									 	  ptx,
									 	  size,
									 	  i2c_driver->i2c_vars->default_i2c_timeout);
#endif

		i2c_driver->i2c_vars->driver_state = DRIVER_READY;

		if (opResult != HAL_OK)
		{
			return RET_ERROR;
		}

		if (i2c1_result != RET_OK)
		{
			return RET_ERROR;
		}

		opResult = i2c1_result;

		break;
	case 2:
		break;
	case 3:
		break;
	default:
		return RET_ERROR;
		break;
	}
	return RET_OK;
}

/**
 *
 * @param i2c_driver->i2c_vars
 * @param port_number
 * @param reg_addr
 * @param reg_addr_size
 * @param prx
 * @param size
 * @return
 */
retval_t
stm32l1_i2c_reg_read(i2c_driver_t* i2c_driver,
					 uint16_t device_addr,
					 uint16_t reg_addr,
					 uint16_t reg_addr_size,
					 uint8_t *prx,
					 uint16_t size)
{
	HAL_StatusTypeDef opResult;

	switch(i2c_driver->i2c_vars->i2c_periph_number)
	{
	case 1:
		i2c1_result = RET_OK;

		/* Put driver in busy mode */
		i2c_driver->i2c_vars->driver_state = DRIVER_BUSY;

#ifdef I2C_NONBLOCKINGMODE
		/* Do the actual read */
		opResult = HAL_I2C_Mem_Read_DMA(&hi2c1,
									  	device_addr,
										reg_addr,
										reg_addr_size,
										prx,
										size);

		osSemaphoreWait(i2c1_semaphore_id, i2c_driver->i2c_vars->default_i2c_timeout);
#else
		/* Do the actual read */
		opResult = HAL_I2C_Mem_Read(&hi2c1,
								    device_addr,
								    reg_addr,
								    reg_addr_size,
								    prx, size,
								    i2c_driver->i2c_vars->default_i2c_timeout);
#endif

		i2c_driver->i2c_vars->driver_state = DRIVER_READY;

		if (opResult != HAL_OK)
		{
			return RET_ERROR;
		}

		if (i2c1_result != RET_OK)
		{
			return RET_ERROR;
		}

		opResult = i2c1_result;

		break;
	case 2:
		break;
	case 3:
		break;
	default:
		return RET_ERROR;
		break;
	}

	/* Return the result of the opration. It will be RET_ERROR if the Error callback has been called by the HAL. */
	return opResult;
}

/**
 *
 * @param i2c_driver->i2c_vars
 * @param device_addr
 * @param reg_addr
 * @param reg_addr_size
 * @param ptx
 * @param size
 * @return
 */
retval_t
stm32l1_i2c_reg_write(i2c_driver_t* i2c_driver,
					  uint16_t device_addr,
					  uint16_t reg_addr,
					  uint16_t reg_addr_size,
					  uint8_t *ptx,
					  uint16_t size)
{
	HAL_StatusTypeDef opResult;

	switch(i2c_driver->i2c_vars->i2c_periph_number)
	{
	case 1:
		i2c1_result = RET_OK;

		/* Put driver in busy mode */
		i2c_driver->i2c_vars->driver_state = DRIVER_BUSY;

#ifdef I2C_NONBLOCKINGMODE
		/* Do the actual read */
		opResult = HAL_I2C_Mem_Write_DMA(&hi2c1,
									     device_addr,
									     reg_addr,
									     reg_addr_size,
									     ptx,
									     size);
		if (opResult != HAL_OK)
		{
			i2c_driver->i2c_vars->driver_state = DRIVER_READY;
			return RET_ERROR;
		}
		osSemaphoreWait(i2c1_semaphore_id, i2c_driver->i2c_vars->default_i2c_timeout);
#else
		/* Do the actual write */
		opResult = HAL_I2C_Mem_Write(&hi2c1,
									 device_addr,
									 reg_addr,
									 reg_addr_size,
									 ptx,
									 size,
									 i2c_driver->i2c_vars->default_i2c_timeout);
#endif

		i2c_driver->i2c_vars->driver_state = DRIVER_READY;

		if (opResult != HAL_OK)
		{
			return RET_ERROR;
		}

		if (i2c1_result != RET_OK)
		{
			return RET_ERROR;
		}

		opResult = i2c1_result;

		break;
	case 2:
		break;
	case 3:
		break;
	default:
		return RET_ERROR;
		break;
	}
	return RET_OK;
}

void
HAL_I2C_MemTxCpltCallback(I2C_HandleTypeDef *hi2c)
{
	if(hi2c->Instance == I2C1)
	{
		osSemaphoreRelease(i2c1_semaphore_id);
	}
}
void
HAL_I2C_MemRxCpltCallback(I2C_HandleTypeDef *hi2c)
{
	if(hi2c->Instance == I2C1)
	{
		osSemaphoreRelease(i2c1_semaphore_id);
	}
}
void
HAL_I2C_MasterTxCpltCallback(I2C_HandleTypeDef *hi2c)
{
	if(hi2c->Instance == I2C1)
	{
		osSemaphoreRelease(i2c1_semaphore_id);
	}
}
void
HAL_I2C_MasterRxCpltCallback(I2C_HandleTypeDef *hi2c)
{
	if(hi2c->Instance == I2C1)
	{
		osSemaphoreRelease(i2c1_semaphore_id);
	}
}
void
HAL_I2C_SlaveTxCpltCallback(I2C_HandleTypeDef *hi2c)
{
	if(hi2c->Instance == I2C1)
	{
		osSemaphoreRelease(i2c1_semaphore_id);
	}
}
void
HAL_I2C_SlaveRxCpltCallback(I2C_HandleTypeDef *hi2c)
{
	if(hi2c->Instance == I2C1)
	{
		osSemaphoreRelease(i2c1_semaphore_id);
	}
}

void
HAL_I2C_ErrorCallback (I2C_HandleTypeDef *hi2c)
{
#ifdef	I2C1_DRIVER
	if(hi2c->Instance == I2C1)
	{
		i2c1_result = RET_ERROR;
	}
#endif
#ifdef	I2C2_DRIVER
	if(hi2c->Instance == I2C2)
	{
		i2c2_result = RET_ERROR;
	}
#endif
}
