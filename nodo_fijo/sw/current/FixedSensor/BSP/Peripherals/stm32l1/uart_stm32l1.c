/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * spi_arch.c
 *
 *  Created on: 20 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file spi_arch.c
 */

#include "../os_uart.h"
#include "cmsis_os.h"
#include "types.h"
#include "stm32l1xx_hal.h"

#include "board_conf.h"


//#include <uart_stm32l1.h>
//#include "stm32l1xx_hal.h"

//#include "usart.h"

//#define DEFAULT_TIMEOUT			500
//
//#define UART_RX_BUFFER_SIZE		256
//#define UART_RX_TIMER_TIMEOUT	95		//An overflow may occur in the timer if the uart speed and data flow are very high.
//										//8 ms timeout prevents overflow with a 256 buffer and up to 250 kbps of continous datarate. Higher speed require a larger buffer
//										//When overflow occurs old data is discarded and replaced by new one
//#define UART_RX_POLL_TIME		100
//
//#define UART_RX_MAX_READ_SIZE	192
//
//#define UART_DEFAULT_ECHO		0

extern UART_HandleTypeDef huart1;
uart_funcs_t stm32l1_uart_funcs;
osSemaphoreDef(os_uart_semaphore);

//osMutexDef(os_uart_tx_mutex);
//osMutexDef(os_uart_rx_mutex);



static osSemaphoreId uart1_tx_semaphore_id;
retval_t uart1_result;
//static osTimerId uart_rx_timer_id;
//static osMutexId uart_tx_mutex_id;
//static osMutexId uart_rx_mutex_id;

//static uint8_t uart_rx_buffer[UART_RX_BUFFER_SIZE];
//static uint8_t* uart_rx_ptr_head;			//Last byte stored in the buffer
//static uint8_t* uart_rx_ptr_tail;			//First byte stored in the buffer
//static uint16_t num_stored_bytes;
//
//static uint16_t enable_local_echo = UART_DEFAULT_ECHO;
//
//static void update_stored_bytes(void);
//static void uart_rx_timer_cb(void const * argument);

retval_t
stm32l1_uart_init(uart_driver_t* uart_driver, uint32_t default_uart_timeout);
retval_t
stm32l1_uart_deinit(uart_driver_t* uart_driver);
retval_t
stm32l1_uart_write(uart_driver_t* uart_driver,
				  uint8_t *ptx,
				  uint16_t size);

uart_funcs_t stm32l1_uart_funcs =
{
		stm32l1_uart_init,
		stm32l1_uart_deinit,
		stm32l1_uart_write,
};


/**
 *
 * @return
 */
retval_t
stm32l1_uart_init(uart_driver_t* uart_driver, uint32_t default_uart_timeout)
{

	/* Initialize peripheral through HAL interface */
	switch(uart_driver->uart_vars->uart_periph_number)
	{
	case 1:
		/* Initialize peripheral variables */
		huart1.Instance = USART1;
		huart1.Init.BaudRate = 9600;
		huart1.Init.WordLength = UART_WORDLENGTH_8B;
		huart1.Init.StopBits = UART_STOPBITS_1;
		huart1.Init.Parity = UART_PARITY_NONE;
		huart1.Init.Mode = UART_MODE_TX_RX;
		huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
		huart1.Init.OverSampling = UART_OVERSAMPLING_16;
		if (HAL_UART_Init(&huart1) != HAL_OK)
		{
			return RET_ERROR;
//			Error_Handler(__FILE__, __LINE__);
		}

		/* Create semaphore for non-blocking operations */
		if(uart1_tx_semaphore_id == NULL)
		{
			uart1_tx_semaphore_id = osSemaphoreCreate(osSemaphore(os_uart_semaphore), 1);
			osSemaphoreWait(uart1_tx_semaphore_id, osWaitForever);
		}
//		uart_tx_mutex_id = osMutexCreate(osMutex(os_uart_tx_mutex));
//		uart_rx_mutex_id = osMutexCreate(osMutex(os_uart_rx_mutex));
		break;
	case 2:
		break;
	case 3:
		break;
	default:
		return RET_ERROR;
		break;
	}
	return RET_OK;


//	osSemaphoreDef(semph);
//	uart_tx_semaphore_id = osSemaphoreCreate(osSemaphore(semph), 1);

//	osMutexDef(mutex1);
//	uart_tx_mutex_id = osMutexCreate(osMutex(mutex1));
//	osMutexDef(mutex2);
//	uart_rx_mutex_id = osMutexCreate(osMutex(mutex2));
//
//	osSemaphoreWait(uart_tx_semaphore_id, osWaitForever);

//	osTimerDef(timer, uart_rx_timer_cb);
//	uart_rx_timer_id = osTimerCreate(osTimer(timer), osTimerPeriodic, NULL);
//
//
//	num_stored_bytes = 0;
//
//	MX_USART1_UART_Init();
//
//	HAL_UART_Receive_DMA(&huart1, uart_rx_buffer, UART_RX_BUFFER_SIZE);			//Continuously read by DMA and store in the buffer
//
//
//	return RET_OK;
}

/**
 *
 * @return
 */
retval_t stm32l1_uart_deinit(uart_driver_t* uart_driver)
{
	switch(uart_driver->uart_vars->uart_periph_number)
	{
	case 1:
		if (HAL_UART_DeInit(&huart1) != HAL_OK)
		{
			return RET_ERROR;
		}
		osSemaphoreRelease(uart1_tx_semaphore_id);
		osSemaphoreDelete(uart1_tx_semaphore_id);
		uart1_tx_semaphore_id = NULL;
		break;
	case 2:
		break;
	case 3:
		break;
	default:
		return RET_ERROR;
		break;
	}
	return RET_OK;


//	osSemaphoreDelete(uart_tx_semaphore_id);
//	osMutexDelete(uart_tx_mutex_id);
//	osMutexDelete(uart_rx_mutex_id);
//	osTimerStop (uart_rx_timer_id);
//	osTimerDelete (uart_rx_timer_id);

//	HAL_UART_DMAStop(&huart1);
//	HAL_UART_DeInit(&huart1);
//
//	return RET_OK;
}

///**
// *
// * @return
// */
//retval_t uart_arch_start_storing_data(void){
//
//	uart_rx_ptr_head = uart_rx_buffer + UART_RX_BUFFER_SIZE - __HAL_DMA_GET_COUNTER(huart1.hdmarx);	//Start storing the samples
//	uart_rx_ptr_tail = uart_rx_ptr_head;
//	num_stored_bytes = 0;
//	return osTimerStart(uart_rx_timer_id, UART_RX_TIMER_TIMEOUT);
//
//
//}
//
///**
// *
// * @return
// */
//retval_t uart_arch_stop_storing_data(void){
//	uart_rx_ptr_head = uart_rx_buffer + UART_RX_BUFFER_SIZE - __HAL_DMA_GET_COUNTER(huart1.hdmarx);	//Start storing the samples
//	uart_rx_ptr_tail = uart_rx_ptr_head;
//	num_stored_bytes = 0;
//	return osTimerStop(uart_rx_timer_id);
//}


/**
 *
 * @param txData
 * @param size
 * @return
 */
//retval_t uart_arch_write(uint8_t* txData, uint16_t size, uint32_t timeout){
//
//	if(!size){
//		return RET_ERROR;
//	}
//	osMutexWait(uart_tx_mutex_id, osWaitForever);
//	HAL_UART_Transmit_DMA(&huart1, txData, size);
//	if(osSemaphoreWait(uart_tx_semaphore_id, timeout) != RET_OK){
//		osMutexRelease(uart_tx_mutex_id);
//		return RET_ERROR;
//	}
//	osMutexRelease(uart_tx_mutex_id);
//	return RET_OK;
retval_t
stm32l1_uart_write(uart_driver_t* uart_driver,
				  uint8_t *ptx,
				  uint16_t size)
{
	HAL_StatusTypeDef opResult;

	switch(uart_driver->uart_vars->uart_periph_number)
	{
	case 1:
		uart1_result = RET_OK;

		/* Put driver in busy mode */
		uart_driver->uart_vars->driver_state = DRIVER_BUSY;

#ifdef UART_NONBLOCKINGMODE
		/* Do the actual read */
		opResult = HAL_UART_Transmit_DMA(&huart1,
										       ptx,
										       size);
		if (opResult != HAL_OK)
		{
			uart_driver->uart_vars->driver_state = DRIVER_READY;
			return RET_ERROR;
		}
		osSemaphoreWait(uart1_tx_semaphore_id, uart_driver->uart_vars->default_uart_timeout);
#else
		/* Do the actual write */
		opResult = HAL_UART_Transmit(&huart1,
									 	  device_addr,
									 	  ptx,
									 	  size,
									 	  uart_driver->uart_vars->default_uart_timeout);
#endif

		uart_driver->uart_vars->driver_state = DRIVER_READY;

		if (opResult != HAL_OK)
		{
			return RET_ERROR;
		}

		if (uart1_result != RET_OK)
		{
			return RET_ERROR;
		}

		opResult = uart1_result;

		break;
	case 2:
		break;
	case 3:
		break;
	default:
		return RET_ERROR;
		break;
	}
	return RET_OK;

}

///**
// *
// * @param rxData
// * @param size
// * @return
// */
//retval_t uart_arch_read(uint8_t* rxData, uint16_t size, uint32_t timeout){
//	uint32_t start_time;
//	uint16_t i;
//	uint16_t read_bytes = 0;
//
//
//	osMutexWait(uart_rx_mutex_id, osWaitForever);
//	update_stored_bytes();
//	start_time = osKernelSysTick();
//	//Now read the buffer depending on the parameter size
//	if(size <= UART_RX_MAX_READ_SIZE){
//		while(num_stored_bytes<size){		//Wait till the required bytes are available
//			osDelay(UART_RX_POLL_TIME);
//			if((osKernelSysTick() - start_time) > timeout){
//				osMutexRelease(uart_rx_mutex_id);
//				return RET_ERROR;
//			}
//		}
//		while (read_bytes < size){
//			rxData[read_bytes] = *uart_rx_ptr_tail;
//			uart_rx_ptr_tail++;
//			num_stored_bytes--;
//			read_bytes++;
//			if (uart_rx_ptr_tail >= &uart_rx_buffer[UART_RX_BUFFER_SIZE]){
//				uart_rx_ptr_tail = uart_rx_buffer;
//			}
//			if(enable_local_echo){
//				uart_arch_write(&rxData[read_bytes-1], 1, timeout);
//			}
//		}
//
//	}
//	else{
//		while(read_bytes < size){
//
//			if((size-read_bytes) > UART_RX_MAX_READ_SIZE){	//If there is no room in the buffer for the remaining bytes
//				while(num_stored_bytes < UART_RX_MAX_READ_SIZE){		//Wait till the required bytes are available
//					osDelay(UART_RX_POLL_TIME);
//					if((osKernelSysTick() - start_time) > timeout){
//						osMutexRelease(uart_rx_mutex_id);
//						return RET_ERROR;
//					}
//				}
//				for(i=0; i<UART_RX_MAX_READ_SIZE; i++){
//					rxData[read_bytes] = *uart_rx_ptr_tail;
//					uart_rx_ptr_tail++;
//					num_stored_bytes--;
//					read_bytes++;
//					if (uart_rx_ptr_tail >= &uart_rx_buffer[UART_RX_BUFFER_SIZE]){
//						uart_rx_ptr_tail = uart_rx_buffer;
//					}
//					if(enable_local_echo){
//						uart_arch_write(&rxData[read_bytes-1], 1, timeout);
//					}
//				}
//
//			}
//			else{						//There is room in the buffer for the remaining bytes
//				while(num_stored_bytes < (size-read_bytes)){		//Wait till the required bytes are available
//					osDelay(UART_RX_POLL_TIME);
//					if((osKernelSysTick() - start_time) > timeout){
//						osMutexRelease(uart_rx_mutex_id);
//						return RET_ERROR;
//					}
//				}
//				while(read_bytes < size){		//Read the last bytes
//					rxData[read_bytes] = *uart_rx_ptr_tail;
//					uart_rx_ptr_tail++;
//					num_stored_bytes--;
//					read_bytes++;
//					if (uart_rx_ptr_tail >= &uart_rx_buffer[UART_RX_BUFFER_SIZE]){
//						uart_rx_ptr_tail = uart_rx_buffer;
//					}
//					if(enable_local_echo){
//						uart_arch_write(&rxData[read_bytes-1], 1, timeout);
//					}
//				}
//			}
//
//
//
//		}
//
//	}
//
//	osMutexRelease(uart_rx_mutex_id);
//	return RET_OK;
//}

///**
// *
// * @param rxData
// * @param size
// * @return
// */
//retval_t uart_arch_read_line(uint8_t* rxData, uint16_t size, uint32_t timeout)
//{
//	uint32_t start_time;
//	uint16_t read_bytes = 0;
//
//	osMutexWait(uart_rx_mutex_id, osWaitForever);
//	update_stored_bytes();
//
//	start_time = osKernelSysTick();
//	while(read_bytes < size){
//			while(num_stored_bytes){
//				if(((*uart_rx_ptr_tail) != '\r') && ((*uart_rx_ptr_tail) != '\n')){		//If not line end, store the character
//
//					if((*uart_rx_ptr_tail) != '\b'){
//
//						rxData[read_bytes] = *uart_rx_ptr_tail;
//						read_bytes++;
//						uart_rx_ptr_tail++;
//						num_stored_bytes--;
//						if (uart_rx_ptr_tail >= &uart_rx_buffer[UART_RX_BUFFER_SIZE]){
//							uart_rx_ptr_tail = uart_rx_buffer;
//						}
//						if(read_bytes >= size){
//							osMutexRelease(uart_rx_mutex_id);
//							return RET_ERROR;
//						}
//						if(enable_local_echo){
//							uart_arch_write(&rxData[read_bytes-1], 1, timeout);
//						}
//					}else{			//Move Backwards. Dont store the character and reduce one
//						if(read_bytes){
//							read_bytes--;
//						}
//						uart_rx_ptr_tail++;
//						num_stored_bytes--;
//						if (uart_rx_ptr_tail >= &uart_rx_buffer[UART_RX_BUFFER_SIZE]){
//							uart_rx_ptr_tail = uart_rx_buffer;
//						}
//						if(enable_local_echo){
//							uart_arch_write((uint8_t*)"\b \b", 3, timeout);
//						}
//					}
//
//				}
//				else{		//Line end detected.
//					rxData[read_bytes] = '\0';
//					uart_rx_ptr_tail++;
//					num_stored_bytes--;
//					read_bytes++;
//					if (uart_rx_ptr_tail >= &uart_rx_buffer[UART_RX_BUFFER_SIZE]){
//						uart_rx_ptr_tail = uart_rx_buffer;
//					}
//
//					//Check if the next element is a '\r' or a '\n' which is very feasible when the line end is "\r\n"
//					if(num_stored_bytes){
//						if(((*uart_rx_ptr_tail) == '\r') || ((*uart_rx_ptr_tail) == '\n')){
//							uart_rx_ptr_tail++;
//							num_stored_bytes--;
//							if (uart_rx_ptr_tail >= &uart_rx_buffer[UART_RX_BUFFER_SIZE]){
//								uart_rx_ptr_tail = uart_rx_buffer;
//							}
//						}
//					}
//
//					if(enable_local_echo){
//						uart_arch_write((uint8_t*)"\r\n", 2, timeout);
//					}
//					osMutexRelease(uart_rx_mutex_id);
//					return RET_OK;
//				}
//
//
//			}
//			osDelay(UART_RX_POLL_TIME);
//			if((osKernelSysTick() - start_time) > timeout){
//				osMutexRelease(uart_rx_mutex_id);
//				return RET_ERROR;
//			}
//		}
//		osMutexRelease(uart_rx_mutex_id);
//		return RET_ERROR;
//}

///**
// *
// * @param speed
// * @return
// */
//retval_t uart_arch_set_speed(uint32_t speed){
//	huart1.Instance = USART1;
//	huart1.Init.BaudRate = speed;
//	osMutexWait(uart_rx_mutex_id, osWaitForever);
//	osMutexWait(uart_tx_mutex_id, osWaitForever);
//
//	if (HAL_UART_Init(&huart1) != HAL_OK)
//	{
//	_Error_Handler(__FILE__, __LINE__);
//	}
//	osMutexRelease(uart_rx_mutex_id);
//	osMutexRelease(uart_tx_mutex_id);
//	return RET_OK;
//}
//
///**
// *
// * @param parity
// * @return
// */
//retval_t uart_arch_set_parity(uint16_t parity){
//	huart1.Instance = USART1;
//
//	osMutexWait(uart_rx_mutex_id, osWaitForever);
//	osMutexWait(uart_tx_mutex_id, osWaitForever);
//
//	switch(parity){
//	case 0:
//		huart1.Init.Parity = UART_PARITY_NONE;
//		break;
//	case 1:
//		huart1.Init.Parity = UART_PARITY_EVEN;
//		break;
//	case 2:
//		huart1.Init.Parity = UART_PARITY_ODD;
//		break;
//	default:
//		osMutexRelease(uart_rx_mutex_id);
//		osMutexRelease(uart_tx_mutex_id);
//		return RET_ERROR;
//		break;
//	}
//
//	if (HAL_UART_Init(&huart1) != HAL_OK)
//	{
//	_Error_Handler(__FILE__, __LINE__);
//	}
//	osMutexRelease(uart_rx_mutex_id);
//	osMutexRelease(uart_tx_mutex_id);
//	return RET_OK;
//}

///**
// *
// * @return
// */
//retval_t uart_arch_enable_echo(void){
//	enable_local_echo = 1;
//	return RET_OK;
//}
//
///**
// *
// * @return
// */
//retval_t uart_arch_disable_echo(void){
//	enable_local_echo = 0;
//	return RET_OK;
//}
//
//
///**
// *
// * @param argument
// */
//static void uart_rx_timer_cb(void const * argument){
//	update_stored_bytes();
//}
//
//
///**
// *
// */
//static void update_stored_bytes(void){
//	uint8_t* new_uart_rx_ptr_head;
//	uint32_t dma_counter = __HAL_DMA_GET_COUNTER(huart1.hdmarx);	//dma remaining downcounter
//	uint16_t new_bytes = 0;
//
//	if(!dma_counter){							//Prevent error if the dma downcounter is 0 (the uart pointer will overflow)
//		dma_counter = UART_RX_BUFFER_SIZE;
//	}
//	new_uart_rx_ptr_head = uart_rx_buffer + UART_RX_BUFFER_SIZE - dma_counter;
//
//	if (new_uart_rx_ptr_head >= uart_rx_ptr_head){
//		new_bytes = (uint16_t)(new_uart_rx_ptr_head - uart_rx_ptr_head);
//		num_stored_bytes += new_bytes;
//	}
//	else{
//		new_bytes = UART_RX_BUFFER_SIZE - ((uint16_t)(uart_rx_ptr_head - new_uart_rx_ptr_head));
//		num_stored_bytes += new_bytes;
//	}
//
//	uart_rx_ptr_head = new_uart_rx_ptr_head; //Update head pointer
//
//	if(num_stored_bytes >= UART_RX_BUFFER_SIZE){		//The buffer is full. Move the tail pointer
//		num_stored_bytes = UART_RX_BUFFER_SIZE;
//		uart_rx_ptr_tail = uart_rx_ptr_head;			//The first writen byte is the last unwritenn byte when the buffer is full
//	}
//}

/**
 *
 * @param huart
 */
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart){
	if(huart->Instance == USART1){
		osSemaphoreRelease(uart1_tx_semaphore_id);
	}
}
