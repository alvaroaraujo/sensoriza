/*
 * os_huart.h
 *
 *  Created on: 10 dic. 2018
 *      Author: telek
 */

#ifndef PERIPHERALS_OS_UART_H_
#define PERIPHERALS_OS_UART_H_

#include "periph_drivers.h"
#include "generic_list.h"
#include "cmsis_os.h"
#include "types.h"

/**
 * @brief	Struct containing a specific virtual port configuration variables for UART
 */
typedef struct uart_port_
{
	int16_t port_id;			//!> Virtual port id opened
	port_state_t port_state;	//!> Current state of this uart port
}uart_port_t;

/**
 * @brief Struct containing the variables used by an SPI driver
 */
typedef struct uart_vars_
{
	uint8_t uart_periph_number;			//!> Number of HW Uart instance

	uint32_t default_uart_timeout;		//!> SPI read write timeout. If 0 it means an infinite timeout

	gen_list* opened_ports;				//!> List with the ports opened. Each element in the list is a uart_port_t type element.

	driver_state_t driver_state;		//!> Current state of the UART driver
	osMutexId uart_mutex_id;			//!> UART driver mutex. Only one thread can use driver functions at a time
}uart_vars_t;

typedef struct uart_funcs_ uart_funcs_t;

/**
 * @brief	UART Driver object. It must be an object for each physical UART bus.
 */
typedef struct uart_driver_
{
	uart_vars_t* uart_vars;						//!> Uart driver variables

	struct uart_funcs_{
		retval_t (*uart_init) (struct uart_driver_* uart_driver,
							  uint32_t default_uart_timeout);
		retval_t (*uart_deinit) (struct uart_driver_* uart_driver);
//		retval_t (*uart_read) (struct uart_driver_* uart_driver,
//							  uint8_t *prx,
//							  uint16_t size);
		retval_t (*uart_write) (struct uart_driver_* uart_driver,
							   uint8_t *ptx,
							   uint16_t size);
		}uart_funcs;
}uart_driver_t;

retval_t
uart_driver_create(uart_driver_t** uart_driver,
				  uint8_t uart_periph_number);

retval_t
uart_driver_init(uart_driver_t* uart_driver,
				uint32_t uart_timeout);

retval_t
uart_driver_deinit(uart_driver_t* uart_driver);

retval_t
uart_driver_delete(uart_driver_t* uart_driver);

int16_t
uart_port_open(uart_driver_t* uart_driver);
retval_t
uart_port_close(uart_driver_t* uart_driver,
			   int16_t port_number);

port_state_t
uart_port_check(uart_driver_t* uart_driver,
			   int16_t port_number);

retval_t
uart_write (uart_driver_t* uart_driver,
		   int16_t port_number,
		   uint8_t *ptx,
		   uint16_t size);



#endif /* PERIPHERALS_OS_UART_H_ */
