/*
 * os_gpio.h
 *
 *  Created on: 5 mar. 2018
 *      Author: jmartin
 */

#ifndef OS_GPIO_H_
#define OS_GPIO_H_

#include "types.h"

typedef enum GPIOmode_{
	GPIO_PIN_INPUT,
	GPIO_PIN_OUTPUT_PUSH_PULL,
	GPIO_PIN_OUTPUT_OPEN_DRAIN,
	GPIO_PIN_INTERRUPT_FALLING,
	GPIO_PIN_INTERRUPT_RISING,
	GPIO_PIN_INTERRUPT_FALLING_RISING,
	GPIO_PIN_ANALOG,
	GPIO_PIN_ANALOG_ADC_CONTROL
}GPIOmode_t;

typedef enum GPIOpull_{
	GPIO_PIN_NO_PULL,
	GPIO_PIN_PULLUP,
	GPIO_PIN_PULLDOWN
}GPIOpull_t;

typedef enum GPIOstate_{
	GPIO_LOW,
	GPIO_HIGH
}GPIOstate_t;

typedef uint32_t gpioPort_t;
typedef uint32_t gpioPin_t;

retval_t
gpio_init (void);

retval_t
gpio_pin_init(gpioPort_t port,
		 	  gpioPin_t pin, 
		 	  GPIOmode_t mode, 
		 	  GPIOpull_t pull);

retval_t
gpio_pin_set (gpioPort_t port,
			  gpioPin_t pin);

retval_t
gpio_pin_reset (gpioPort_t port,
				gpioPin_t pin);

retval_t
gpio_pin_toggle (gpioPort_t port,
				 gpioPin_t pin);


retval_t
gpio_pin_read (gpioPort_t port,
			   gpioPin_t pin,
			   GPIOstate_t* state);

retval_t
gpio_pin_setIntCallback (gpioPort_t port,
						 gpioPin_t pin,
						 void (*callback)(void*),
						 void* instance);

#endif /* OS_GPIO_H_ */
