#ifndef BSP_PERIPHERAL_I2C_H_
#define BSP_PERIPHERAL_I2C_H_

#include "periph_drivers.h"
#include "generic_list.h"
#include "cmsis_os.h"
#include "types.h"

typedef enum{
	I2C_PORT_SLAVE,				//!> I2C PORT slave mode
	I2C_PORT_MASTER,			//!> I2C PORT master mode
}i2c_port_mode_t;

/**
 * @brief	Struct containing a specific virtual port configuration variables for I2C
 */
typedef struct i2c_port_{
	int16_t port_id;		//!> Virtual port id opened
	uint16_t device_addr;	//!> Physical address of the i2c slave connected to this port
	i2c_port_mode_t i2c_port_mode;	//!> I2C port mode: MASTER or SLAVE
	port_state_t port_state;	//!> Current state of this i2c port
}i2c_port_t;

/**
 * @brief Struct containing the variables used by an SPI driver
 */
typedef struct i2c_vars_{
	uint8_t i2c_periph_number;			//!> Number of HW I2C instance

	uint16_t i2c_own_addr;			//!> Own Physical address of the i2c bus
	uint32_t default_i2c_timeout;	//!> SPI read write timeout. If 0 it means an infinite timeout

	gen_list* opened_ports;			//!> List with the ports opened. Each element in the list is a i2c_port_t type element.

	driver_state_t driver_state;	//!> Current state of the i2c driver
	osMutexId i2c_mutex_id;			//!> I2C driver mutex. Only one thread can use driver functions at a time
}i2c_vars_t;

typedef struct i2c_funcs_ i2c_funcs_t;

/**
 * @brief	I2C Driver object. It must be an object for each physical I2C bus.
 */
typedef struct i2c_driver_
{
	i2c_vars_t* i2c_vars;						//!> I2C driver variables

	struct i2c_funcs_{
		retval_t (*i2c_init) (struct i2c_driver_* i2c_driver, 
					 		  uint8_t own_addr, 
							  uint32_t default_i2c_timeout);
		retval_t (*i2c_deinit) (struct i2c_driver_* i2c_driver);
		retval_t (*i2c_read) (struct i2c_driver_* i2c_driver, 
							  uint16_t device_addr, 
							  uint8_t *prx, 
							  uint16_t size);
		retval_t (*i2c_write) (struct i2c_driver_* i2c_driver, 
							   uint16_t device_addr, 
							   uint8_t *ptx, 
							   uint16_t size);
		retval_t (*i2c_reg_read) (struct i2c_driver_* i2c_driver, 
								  uint16_t device_addr, 
								  uint16_t reg_addr, 
								  uint16_t reg_addr_size, 
								  uint8_t *prx, 
								  uint16_t size);
		retval_t (*i2c_reg_write) (struct i2c_driver_* i2c_driver, 
								   uint16_t device_addr, 
								   uint16_t reg_addr, 
								   uint16_t reg_addr_size, 
								   uint8_t *ptx, 
								   uint16_t size);																			//!> Write to i2c on an internal register the specified bytes number. It blocks till completed or i2c timeout reached
		}i2c_funcs;
}i2c_driver_t;

retval_t
i2c_driver_create(i2c_driver_t** i2c_driver,
				  uint8_t i2c_periph_number);

retval_t
i2c_driver_init(i2c_driver_t* i2c_driver,
				uint16_t own_addr,
				uint32_t i2c_timeout);

retval_t
i2c_driver_deinit(i2c_driver_t* i2c_driver);

retval_t 
i2c_driver_delete(i2c_driver_t* i2c_driver);

int16_t 
i2c_port_open(i2c_driver_t* i2c_driver, 
			  uint16_t device_addr, 
			  i2c_port_mode_t i2c_port_mode);
retval_t 
i2c_port_close(i2c_driver_t* i2c_driver, 
			   int16_t port_number);

port_state_t
i2c_port_check(i2c_driver_t* i2c_driver, 
			   int16_t port_number);

retval_t
i2c_read (i2c_driver_t* i2c_driver,
		  int16_t port_number,
		  uint8_t *prx,
		  uint16_t size);

retval_t
i2c_write (i2c_driver_t* i2c_driver,
		   int16_t port_number,
		   uint8_t *ptx,
		   uint16_t size);

retval_t
i2c_register_read (i2c_driver_t* i2c_driver, 
				   int16_t port_number, 
				   uint16_t reg_addr, 
				   uint16_t reg_addr_size, 
				   uint8_t *prx, 
				   uint16_t size);

retval_t 
i2c_register_write(i2c_driver_t* i2c_driver, 
				   int16_t port_number, 
				   uint16_t reg_addr, 
				   uint16_t reg_addr_size, 
				   uint8_t *prx, 
				   uint16_t size);

#endif /* BSP_PERIPHERAL_I2C_H_ */
