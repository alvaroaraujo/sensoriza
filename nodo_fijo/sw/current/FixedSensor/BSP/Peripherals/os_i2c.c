#include "os_i2c.h"
#include "board_conf.h"


osMutexDef (os_i2c_mutex);

#ifdef OS_I2C_DRIVER_FUNCS
extern i2c_funcs_t OS_I2C_DRIVER_FUNCS;
#else
#error	"I2C FUNCTION PREFIX NOT DEFINED"
#endif

retval_t
i2c_driver_create(i2c_driver_t** i2c_driver,
				  uint8_t i2c_periph_number)
{
	if(i2c_periph_number >16)
	{
		return RET_ERROR;
	}

	/* Allocate memory */
	i2c_driver_t* newI2Cdriver = pvPortMalloc(sizeof(i2c_driver_t));
	newI2Cdriver->i2c_vars = (i2c_vars_t*) pvPortMalloc(sizeof(i2c_vars_t));

	/* Initialize driver */
	newI2Cdriver->i2c_vars->i2c_periph_number = i2c_periph_number;
	newI2Cdriver->i2c_vars->opened_ports = NULL;
	newI2Cdriver->i2c_vars->driver_state = DRIVER_NOT_INIT;
	newI2Cdriver->i2c_vars->i2c_mutex_id = osMutexCreate(osMutex(os_i2c_mutex));
	newI2Cdriver->i2c_funcs = OS_I2C_DRIVER_FUNCS;

	*i2c_driver = newI2Cdriver;
	return RET_OK;
}

retval_t 
i2c_driver_delete(i2c_driver_t* i2c_driver)
{

	if(i2c_driver == NULL)
	{
		return RET_ERROR;
	}

	/* Wait until driver is free */
	osMutexWait(i2c_driver->i2c_vars->i2c_mutex_id, osWaitForever);
	while(i2c_driver->i2c_vars->driver_state == DRIVER_BUSY);

	/* Deinitialize driver */
	i2c_driver->i2c_vars->opened_ports = NULL;
	i2c_driver->i2c_vars->driver_state = DRIVER_NOT_INIT;
	i2c_driver->i2c_vars->i2c_periph_number = 255;
	osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);

	/* Free memory */
	osMutexDelete(i2c_driver->i2c_vars->i2c_mutex_id);
	vPortFree(i2c_driver->i2c_vars);
	i2c_driver->i2c_vars = NULL;
	vPortFree(i2c_driver);

	return RET_OK;
}


retval_t
i2c_driver_init (i2c_driver_t* i2c_driver,
				 uint16_t own_addr, 
				 uint32_t i2c_timeout)
{
	if(i2c_driver == NULL)
	{
		return RET_ERROR;
	}

	osMutexWait(i2c_driver->i2c_vars->i2c_mutex_id, osWaitForever);

	/* Check for already initialized driver */
	if(i2c_driver->i2c_vars->driver_state != DRIVER_NOT_INIT)
	{
		osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);
		return RET_ERROR;
	}

	/* Set timeout accordingly */
	if(i2c_timeout == 0)
	{
		i2c_driver->i2c_vars->default_i2c_timeout = osWaitForever;
	}
	else
	{
		i2c_driver->i2c_vars->default_i2c_timeout = i2c_timeout;
	}

	/* Try to initialize peripheral */
	if(i2c_driver->i2c_funcs.i2c_init(i2c_driver, own_addr , i2c_timeout) != RET_OK)
	{
		osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);
		return RET_ERROR;
	}

	/* Create port list */
	i2c_driver->i2c_vars->opened_ports = gen_list_init();
	/* Set as ready and return */
	i2c_driver->i2c_vars->driver_state = DRIVER_READY;

	osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);
	return RET_OK;
}

retval_t
i2c_driver_deinit (i2c_driver_t* i2c_driver)
{
	if(i2c_driver == NULL)
	{
		return RET_ERROR;
	}

	osMutexWait(i2c_driver->i2c_vars->i2c_mutex_id, osWaitForever);

	/* Avoid deinit of driver if it is being used */
	if(i2c_driver->i2c_vars->driver_state != DRIVER_READY)
	{
		osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);
		return RET_ERROR;
	}

	/* Try to deinitialize peripheral */
	if(i2c_driver->i2c_funcs.i2c_deinit(i2c_driver) != RET_OK)
	{
		osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);
		return RET_ERROR;
	}

	/* Free all driver ports */
	gen_list* current = i2c_driver->i2c_vars->opened_ports;
	i2c_port_t* opened_port;
	/* Get next opened port*/
	while(current->next != NULL)
	{
		opened_port = (i2c_port_t*) current->next->item;

		/* Wait until port is ready */
		while(opened_port->port_state !=  DRIVER_PORT_OPENED_READY);

		vPortFree(opened_port);
		gen_list* next = current->next->next;
		vPortFree(current->next);
		current->next = next;
	}

	gen_list_remove_all(i2c_driver->i2c_vars->opened_ports);
	i2c_driver->i2c_vars->opened_ports = NULL;
	i2c_driver->i2c_vars->driver_state = DRIVER_NOT_INIT;

	osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);
	return RET_OK;
}

/**
 *
 * @param i2c_driver->i2c_vars
 * @param device_addr
 * @return
 */
int16_t 
i2c_port_open(i2c_driver_t* i2c_driver, 
					 uint16_t device_addr, 
					 i2c_port_mode_t i2c_port_mode)
{
	if(i2c_driver == NULL)
	{
		return -1;
	}

	osMutexWait(i2c_driver->i2c_vars->i2c_mutex_id, osWaitForever);

	if(i2c_driver->i2c_vars->driver_state == DRIVER_NOT_INIT)
	{		//Return error if the driver is not initialized
		osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);
		return -1;
	}

	if((i2c_port_mode != I2C_PORT_MASTER) && (i2c_port_mode != I2C_PORT_SLAVE))
	{
		osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);
		return -1;
	}


	i2c_port_t* new_i2c_port = (i2c_port_t*) pvPortMalloc(sizeof(i2c_port_t));

	new_i2c_port->device_addr = device_addr;
	new_i2c_port->i2c_port_mode = i2c_port_mode;

	new_i2c_port->port_id = 0;								//Check the opened port list to select an ordered available port_id

	i2c_port_t* temp_list_i2c_port;
	gen_list* current = i2c_driver->i2c_vars->opened_ports;
	uint8_t repeat_loop = 1;
	while(repeat_loop)
	{
		repeat_loop = 0;
		current = i2c_driver->i2c_vars->opened_ports;
		while(current->next != NULL){
			temp_list_i2c_port = (i2c_port_t*) current->next->item;
			if(temp_list_i2c_port->port_id == new_i2c_port->port_id){
				repeat_loop = 1;
				new_i2c_port->port_id++;
			}
			current = current->next;
		}
	}

	new_i2c_port->port_state = DRIVER_PORT_OPENED_READY;

	gen_list_add(i2c_driver->i2c_vars->opened_ports, (void*) new_i2c_port);
	osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);
	return new_i2c_port->port_id;
}

/**
 *
 * @param i2c_driver->i2c_vars
 * @param port_number
 * @return
 */
retval_t 
i2c_port_close(i2c_driver_t* i2c_driver, 
			   int16_t port_number)
{
	if(i2c_driver == NULL)
	{		//return error if the driver variables does not exist
		return RET_ERROR;
	}

	osMutexWait(i2c_driver->i2c_vars->i2c_mutex_id, osWaitForever);

	if(i2c_driver->i2c_vars->driver_state == DRIVER_NOT_INIT)
	{		//Return error if the driver is not initialized
		osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);
		return RET_ERROR;
	}

	gen_list* current = i2c_driver->i2c_vars->opened_ports;
	i2c_port_t* temp_list_i2c_port;

	while(current->next != NULL)
	{

		temp_list_i2c_port = (i2c_port_t*) current->next->item;
		if(temp_list_i2c_port->port_id == port_number)
		{

			while(temp_list_i2c_port->port_state !=  DRIVER_PORT_OPENED_READY);			//Wait till the port is ready to close it(end all transmissions)

			temp_list_i2c_port->port_id = -1;
			gen_list* next = current->next->next;
			temp_list_i2c_port->port_state = DRIVER_PORT_CLOSED;
			vPortFree(temp_list_i2c_port);
			vPortFree(current->next);
			current->next = next;

			osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);
			return RET_OK;										//Port succesfully deleted
		}
		else
		{
			current = current->next;
		}

	}

	osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);
	return RET_ERROR;										//Port not found. Not deleted
}

/**
 *
 * @param i2c_driver->i2c_vars
 * @param port_number
 * @return
 */
port_state_t 
i2c_port_check(i2c_driver_t* i2c_driver, 
			   int16_t port_number)
{
	/* Check that the driver exists */
	if(i2c_driver == NULL)
	{
		return DRIVER_PORT_CLOSED;
	}

	/* Adquire mutex and check if the driver has been initialized */
	osMutexWait(i2c_driver->i2c_vars->i2c_mutex_id, osWaitForever);
	if(i2c_driver->i2c_vars->driver_state == DRIVER_NOT_INIT)
	{
		osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);
		return DRIVER_PORT_CLOSED;
	}

	/* Check that the supplied port number exists */
	gen_list* current = i2c_driver->i2c_vars->opened_ports;
	i2c_port_t* temp_list_i2c_port;
	while(current->next != NULL)
	{
		temp_list_i2c_port = (i2c_port_t*) current->next->item;
		if(temp_list_i2c_port->port_id == port_number)
		{
			port_state_t port_state = temp_list_i2c_port->port_state;
			osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);

			return port_state;
		}
		else
		{
			current = current->next;
		}
	}

	osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);
	return DRIVER_PORT_CLOSED;
}

retval_t
i2c_read (i2c_driver_t* i2c_driver,
		  int16_t port_number,
		  uint8_t *prx,
		  uint16_t size)
{
	/* Check that the driver exists */
	if(i2c_driver == NULL)
	{
		return RET_ERROR;
	}

	/* Adquire mutex and check if the driver has been initialized */
	osMutexWait(i2c_driver->i2c_vars->i2c_mutex_id, osWaitForever);
	if(i2c_driver->i2c_vars->driver_state == DRIVER_NOT_INIT)
	{
		osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);
		return RET_ERROR;
	}

	/* Wait until the driver is available */
	while(i2c_driver->i2c_vars->driver_state == DRIVER_BUSY);

	/* Check that the supplied port number exists and has the adequate configuration for this operation */
	gen_list* current = i2c_driver->i2c_vars->opened_ports;
	i2c_port_t* temp_list_i2c_port;
	i2c_port_t* currentPort = NULL;
	while(current->next != NULL)
	{
		temp_list_i2c_port = (i2c_port_t*) current->next->item;
		if(temp_list_i2c_port->port_id == port_number)
		{
			/* Wait till the port is ready to read(end all transmissions) */
			while(temp_list_i2c_port->port_state !=  DRIVER_PORT_OPENED_READY);
			/* Read/write only available in master mode */
			if(temp_list_i2c_port->i2c_port_mode != I2C_PORT_MASTER)
			{
				osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);
				return RET_ERROR;
			}
			/* Store port to be operated */
			currentPort = (i2c_port_t*) current->next->item;
			break;
		}
		else
		{
			/* Get the next element in the opened port list */
			current = current->next;
		}
	}
	/* If the port was not found, return error */
	if (currentPort == NULL)
	{
		osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);						//Port id not found
		return RET_ERROR;
	}

	temp_list_i2c_port->port_state = DRIVER_PORT_READING;

	/* Call the i2c function */
	retval_t result = i2c_driver->i2c_funcs.i2c_read(i2c_driver, 
													 temp_list_i2c_port->device_addr, 
													 prx, 
													 size);

	temp_list_i2c_port->port_state = DRIVER_PORT_OPENED_READY;

	/* Release the mutex */
	osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);
	return result;
}

retval_t
i2c_write (i2c_driver_t* i2c_driver,
		   int16_t port_number,
		   uint8_t *ptx,
		   uint16_t size)
{
	/* Check that the driver exists */
	if(i2c_driver == NULL)
	{
		return RET_ERROR;
	}

	/* Adquire mutex and check if the driver has been initialized */
	osMutexWait(i2c_driver->i2c_vars->i2c_mutex_id, osWaitForever);
	if(i2c_driver->i2c_vars->driver_state == DRIVER_NOT_INIT)
	{
		osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);
		return RET_ERROR;
	}

	/* Wait until the driver is available */
	while(i2c_driver->i2c_vars->driver_state == DRIVER_BUSY);

	/* Check that the supplied port number exists and has the adequate configuration for this operation */
	gen_list* current = i2c_driver->i2c_vars->opened_ports;
	i2c_port_t* temp_list_i2c_port;
	i2c_port_t* currentPort = NULL;
	while(current->next != NULL)
	{
		temp_list_i2c_port = (i2c_port_t*) current->next->item;
		if(temp_list_i2c_port->port_id == port_number)
		{
			/* Wait till the port is ready to read(end all transmissions) */
			while(temp_list_i2c_port->port_state !=  DRIVER_PORT_OPENED_READY);
			/* Read/write only available in master mode */
			if(temp_list_i2c_port->i2c_port_mode != I2C_PORT_MASTER)
			{
				osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);
				return RET_ERROR;
			}
			/* Store port to be operated */
			currentPort = (i2c_port_t*) current->next->item;
			break;
		}
		else
		{
			/* Get the next element in the opened port list */
			current = current->next;
		}
	}
	/* If the port was not found, return error */
	if (currentPort == NULL)
	{
		osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);
		return RET_ERROR;
	}

	currentPort->port_state = DRIVER_PORT_WRITING;

	retval_t result = i2c_driver->i2c_funcs.i2c_write(i2c_driver,
													  currentPort->device_addr,
													  ptx,
													  size);
	
	currentPort->port_state = DRIVER_PORT_OPENED_READY;

	/* Release the mutex */
	osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);
	return result;
}


retval_t
i2c_register_read (i2c_driver_t* i2c_driver, 
				   int16_t port_number, 
				   uint16_t reg_addr, 
				   uint16_t reg_addr_size, 
				   uint8_t *prx, 
				   uint16_t size)
{
	/* Check that the driver exists */
	if(i2c_driver == NULL)
	{
		return RET_ERROR;
	}

	/* Adquire mutex and check if the driver has been initialized */
	osMutexWait(i2c_driver->i2c_vars->i2c_mutex_id, osWaitForever);
	if(i2c_driver->i2c_vars->driver_state == DRIVER_NOT_INIT)
	{
		osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);
		return RET_ERROR;
	}
	/* Register can only be byte or word long */
	if((reg_addr_size != 1) && (reg_addr_size != 2))
	{
		osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);
		return RET_ERROR;
	}

	/* Wait until the driver is available */
	while(i2c_driver->i2c_vars->driver_state == DRIVER_BUSY);

	/* Check that the supplied port number exists and has the adequate configuration for this operation */
	gen_list* current = i2c_driver->i2c_vars->opened_ports;
	i2c_port_t* temp_list_i2c_port;
	i2c_port_t* currentPort = NULL;
	while(current->next != NULL)
	{
		temp_list_i2c_port = (i2c_port_t*) current->next->item;
		if(temp_list_i2c_port->port_id == port_number)
		{
			/* Wait till the port is ready to read(end all transmissions) */
			while(temp_list_i2c_port->port_state !=  DRIVER_PORT_OPENED_READY);
			/* Read/write only available in master mode */
			if(temp_list_i2c_port->i2c_port_mode != I2C_PORT_MASTER)
			{
				osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);
				return RET_ERROR;
			}
			/* Store port to be operated */
			currentPort = (i2c_port_t*) current->next->item;
			break;
		}
		else
		{
			/* Get the next element in the opened port list */
			current = current->next;
		}
	}
	/* If the port was not found, return error */
	if (currentPort == NULL)
	{
		osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);						//Port id not found
		return RET_ERROR;
	}

	temp_list_i2c_port->port_state = DRIVER_PORT_READING;

	/* Call the i2c function */
	retval_t result = i2c_driver->i2c_funcs.i2c_reg_read(i2c_driver, 
														 temp_list_i2c_port->device_addr, 
														 reg_addr, 
														 reg_addr_size, 
														 prx, 
														 size);

	temp_list_i2c_port->port_state = DRIVER_PORT_OPENED_READY;

	/* Release the mutex */
	osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);
	return result;
}


retval_t 
i2c_register_write(i2c_driver_t* i2c_driver, 
					   int16_t port_number, 
					   uint16_t reg_addr, 
					   uint16_t reg_addr_size, 
					   uint8_t *ptx, 
					   uint16_t size)
{
	/* Check that the driver exists */
	if(i2c_driver == NULL)
	{
		return RET_ERROR;
	}

	/* Adquire mutex and check if the driver has been initialized */
	osMutexWait(i2c_driver->i2c_vars->i2c_mutex_id, osWaitForever);
	if(i2c_driver->i2c_vars->driver_state == DRIVER_NOT_INIT)
	{
		osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);
		return RET_ERROR;
	}
	/* Register can only be byte or word long */
	if((reg_addr_size != 1) && (reg_addr_size != 2))
	{
		osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);
		return RET_ERROR;
	}

	/* Wait until the driver is available */
	while(i2c_driver->i2c_vars->driver_state == DRIVER_BUSY);

	/* Check that the supplied port number exists and has the adequate configuration for this operation */
	gen_list* current = i2c_driver->i2c_vars->opened_ports;
	i2c_port_t* temp_list_i2c_port;
	i2c_port_t* currentPort = NULL;
	while(current->next != NULL)
	{
		temp_list_i2c_port = (i2c_port_t*) current->next->item;
		if(temp_list_i2c_port->port_id == port_number)
		{
			/* Wait till the port is ready to read(end all transmissions) */
			while(temp_list_i2c_port->port_state !=  DRIVER_PORT_OPENED_READY);
			/* Read/write only available in master mode */
			if(temp_list_i2c_port->i2c_port_mode != I2C_PORT_MASTER)
			{
				osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);
				return RET_ERROR;
			}
			/* Store port to be operated */
			currentPort = (i2c_port_t*) current->next->item;
			break;
		}
		else
		{
			/* Get the next element in the opened port list */
			current = current->next;
		}
	}
	/* If the port was not found, return error */
	if (currentPort == NULL)
	{
		osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);
		return RET_ERROR;
	}

	currentPort->port_state = DRIVER_PORT_WRITING;

	retval_t result = i2c_driver->i2c_funcs.i2c_reg_write(i2c_driver,
														  currentPort->device_addr,
														  reg_addr,
														  reg_addr_size,
														  ptx,
														  size);
	
	currentPort->port_state = DRIVER_PORT_OPENED_READY;

	/* Release the mutex */
	osMutexRelease(i2c_driver->i2c_vars->i2c_mutex_id);
	return result;
}

