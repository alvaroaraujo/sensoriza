/*
 * tsl2561.c
 *
 *  Created on: 16 mar. 2018
 *      Author: albarc
 */

/* Includes ------------------------------------------------------------------*/
#include "tsl2561.h"

/* Variables ------------------------------------------------------------------*/
//uint8_t tx_data[4];
//uint8_t rx_data[4];

uint8_t tsl2561Initialized = FALSE;
uint8_t tsl2561AutoGain = FALSE;
tsl2561IntegrationTime_t tsl2561IntegrationTime = TSL2561_INTEGRATIONTIME_DEFAULT;
tsl2561Gain_t tsl2561Gain = TSL2561_GAIN_DEFAULT;

tsl2561_t* tsl2561;

osMutexDef (tsl2561_mutex);

/***************************************************************************/
/*************General Functions for interfacing with the sensors************/
/*********************************BEGINING**********************************/
retval_t
tsl2561_create (tsl2561_t** device)
{
    /* Allocate memory */
    tsl2561_t* new_device = pvPortMalloc(sizeof(tsl2561_t));
    if (new_device == NULL)
    {
        return RET_ERROR;
    }

    new_device->lock = osMutexCreate(osMutex(tsl2561_mutex));
    new_device->state = DRIVER_NOT_INIT;

    *device = new_device;
    return RET_OK;
}

retval_t
tsl2561_delete (tsl2561_t* device)
{
    if (device == NULL)
    {
        return RET_ERROR;
    }

    if (device->state != DRIVER_NOT_INIT)
    {
        return RET_ERROR;
    }

    /* Wait for mutex */
    osMutexWait(device->lock, osWaitForever);
    device->state = DRIVER_NOT_INIT;
    osMutexRelease(device->lock);

    /* Delete mutex */
    osMutexDelete(device->lock);

    /* Free memory */
    vPortFree(device);

    return RET_OK;
}

retval_t
tsl2561_init (tsl2561_t* device,
              i2c_driver_t* i2cDriver)
{
	/*Standard checkings*/
	if (device == NULL)
    {
        return RET_ERROR;
    }

    if (i2cDriver == NULL)
    {
        return RET_ERROR;
    }

    /* Wait for mutex */
    osMutexWait(device->lock, osWaitForever);

    /* Check for already initialized driver */
    if(device->state != DRIVER_NOT_INIT)
    {
        osMutexRelease(device->lock);
        return RET_ERROR;
    }

    device->i2cDriver = i2cDriver;

    /* Create I2C handle */
    device->i2cHandle = i2c_port_open(device->i2cDriver, TSL2561_ADDRESS, I2C_PORT_MASTER);
    if (device->i2cHandle == -1)
    {
        osMutexRelease(device->lock);
        return RET_ERROR;
    }

    /*Device specific checkings*/
	/* Read the REGISTER_ID to check the connection */
	uint8_t id = tsl2561_readID();

	if (id != TSL2561_ID_VALUE) {
		return RET_ERROR;
	}

	tsl2561Initialized = TRUE;

	/* Set the default sensor values */
	tsl2561AutoGain = FALSE;
	tsl2561IntegrationTime = TSL2561_INTEGRATIONTIME_DEFAULT;
	tsl2561Gain = TSL2561_GAIN_DEFAULT;
	tsl2561_setIntegrationTime(tsl2561IntegrationTime);
	tsl2561_setGain(tsl2561Gain);

	/* Reduce consumption */
	tsl2561_disable();

	/*End of specific checkings*/

	device->state = DRIVER_READY;

    osMutexRelease(device->lock);
    return RET_OK;
}

retval_t
tsl2561_deinit (tsl2561_t* device)
{
    if (device == NULL)
    {
        return RET_ERROR;
    }

    /* Wait for mutex */
    osMutexWait(device->lock, osWaitForever);

    /* Close I2C port */
    i2c_port_close(device->i2cDriver, device->i2cHandle);

    device->state = DRIVER_NOT_INIT;
    osMutexRelease(device->lock);
    return RET_OK;
}
/***************************************************************************/
/*************General Functions for interfacing with the sensors************/
/***********************************END*************************************/


/**************************************************************************/
/*!
    Enables the device
*/
/**************************************************************************/
void tsl2561_enable (void)
{
	/* Enable the device by setting the control bits to 0x03 */
	uint8_t data = TSL2561_CONTROL_POWERON;
	uint8_t command_code = TSL2561_COMMAND_BIT | TSL2561_REGISTER_CONTROL;

	retval_t status = i2c_register_write(tsl2561->i2cDriver, tsl2561->i2cHandle, command_code, 1, &data, 1);

#if defined DEBUG_PERIPH_ERRORS
	if (status == RET_OK){
		gpio_pin_toggle(STM_GPIOC, STM_GPIO_PIN_9); //Toggle the green LED
		gpio_pin_reset(STM_GPIOC, STM_GPIO_PIN_8); //Clear the red LED
//		HAL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin);
	}
	else
	{
		gpio_pin_toggle(STM_GPIOC, STM_GPIO_PIN_8); //Toggle the red LED
		gpio_pin_reset(STM_GPIOC, STM_GPIO_PIN_9); //Clear the green LED
//		HAL_GPIO_TogglePin(LD4_GPIO_Port, LD4_Pin);
	}
#endif
}

/**************************************************************************/
/*!
    Disables the device
*/
/**************************************************************************/
void tsl2561_disable (void)
{
	/* Disable the device by setting the control bits to 0x00 */
	uint8_t data = TSL2561_CONTROL_POWEROFF;
	uint8_t command_code = TSL2561_COMMAND_BIT | TSL2561_REGISTER_CONTROL;

	retval_t status = i2c_register_write(tsl2561->i2cDriver, tsl2561->i2cHandle, command_code, 1, &data, 1);

#if defined DEBUG_PERIPH_ERRORS
	if (status == RET_OK){
		gpio_pin_toggle(STM_GPIOC, STM_GPIO_PIN_9); //Toggle the green LED
		gpio_pin_reset(STM_GPIOC, STM_GPIO_PIN_8); //Clear the red LED
//		HAL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin);
	}
	else
	{
		gpio_pin_toggle(STM_GPIOC, STM_GPIO_PIN_8); //Toggle the red LED
		gpio_pin_reset(STM_GPIOC, STM_GPIO_PIN_9); //Clear the green LED
//		HAL_GPIO_TogglePin(LD4_GPIO_Port, LD4_Pin);
	}
#endif
}

/**************************************************************************/
/*!
    Reads the device ID
    TSL2561T/FN/CL should return 0x50
*/
/**************************************************************************/
uint8_t tsl2561_readID (void)
{
	uint8_t data;
	uint8_t command_code = TSL2561_COMMAND_BIT | TSL2561_REGISTER_ID;

	retval_t statusRx = i2c_register_read(tsl2561->i2cDriver, tsl2561->i2cHandle, command_code, 1, &data, 1);

#if defined DEBUG_PERIPH_ERRORS
	if (statusRx == RET_OK){
		gpio_pin_toggle(STM_GPIOC, STM_GPIO_PIN_9); //Toggle the green LED
		gpio_pin_reset(STM_GPIOC, STM_GPIO_PIN_8); //Clear the red LED
//		HAL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin);
	}
	else
	{
		gpio_pin_toggle(STM_GPIOC, STM_GPIO_PIN_8); //Toggle the red LED
		gpio_pin_reset(STM_GPIOC, STM_GPIO_PIN_9); //Clear the green LED
//		HAL_GPIO_TogglePin(LD4_GPIO_Port, LD4_Pin);
	}
#endif

	if (statusRx == RET_OK)
	{
		return data;
	}
	else
	{
		return (uint8_t)RET_ERROR;
	}
}

/**************************************************************************/
/*!
    Enables or disables the auto-gain settings when reading
    data from the sensor
    Set to true to enable, false to disable
*/
/**************************************************************************/
void tsl2561_enableAutoRange(uint8_t enable)
{
	tsl2561AutoGain = enable;
}

/**************************************************************************/
/*!
    Sets the integration time to one of its three possible values
*/
/**************************************************************************/
void tsl2561_setIntegrationTime (tsl2561IntegrationTime_t time)
{
	/* Wake up */
	tsl2561_enable();

	/* When setting the integration time, the gain must also be written */
	uint8_t data = time | tsl2561Gain;	// Their values are in different nibbles and don't overlap
	uint8_t command_code = TSL2561_COMMAND_BIT | TSL2561_REGISTER_TIMING;

	retval_t status = i2c_register_write(tsl2561->i2cDriver, tsl2561->i2cHandle, command_code, 1, &data, 1);

	/* Update variable */
	tsl2561IntegrationTime = time;

	/* Reduce consumption */
	tsl2561_disable();
}

/**************************************************************************/
/*!
    Sets the gain to one of its two possible values
*/
/**************************************************************************/
void tsl2561_setGain (tsl2561Gain_t gain)
{
	/* Wake up */
	tsl2561_enable();

	/* When setting the gain, the integration time must also be written */
	uint8_t data = tsl2561IntegrationTime | gain;	// Their values are in different nibbles and don't overlap
	uint8_t command_code = TSL2561_COMMAND_BIT | TSL2561_REGISTER_TIMING;

	retval_t status = i2c_register_write(tsl2561->i2cDriver, tsl2561->i2cHandle, command_code, 1, &data, 1);

	/* Update variable */
	tsl2561Gain = gain;

	/* Reduce consumption */
	tsl2561_disable();
}

/**************************************************************************/
/*!
    @brief  Gets the broadband (mixed lighting IR+visible) and IR only
            values from the TSL2561, adjusting gain if auto-gain is enabled
    @param  broadband: Pointer to a uint16_t we will fill with a sensor
                      reading from the IR+visible light diode.
    @param  ir: Pointer to a uint16_t we will fill with a sensor the
               IR-only light diode.
*/
/**************************************************************************/
void tsl2561_getLuminosity (uint16_t *broadband, uint16_t *ir)
{
	uint8_t valid = FALSE;

	/* If AutoGain is disabled get a single reading and continue */
	if(tsl2561AutoGain == FALSE) {
		tsl2561_getData (broadband, ir);
		return;
	}

	/* Read data until we find a valid range */
	uint8_t autogainCheck = FALSE;

	do {
		uint16_t broad, infra;
		uint16_t hi, lo;
		tsl2561IntegrationTime_t it = tsl2561IntegrationTime;

		/* Get the hi/low threshold for the current integration time */
		switch(it) {
			case TSL2561_INTEGRATIONTIME_13MS:
				hi = TSL2561_AGC_THI_13MS;
				lo = TSL2561_AGC_TLO_13MS;
				break;

			case TSL2561_INTEGRATIONTIME_101MS:
				hi = TSL2561_AGC_THI_101MS;
				lo = TSL2561_AGC_TLO_101MS;
				break;

			case TSL2561_INTEGRATIONTIME_402MS:
			default:
				hi = TSL2561_AGC_THI_402MS;
				lo = TSL2561_AGC_TLO_402MS;
				break;
		}

		tsl2561_getData(&broad, &infra);

		/* Run an auto-gain check if we haven't already done so ... */
		if (autogainCheck == FALSE) {

			if ((broad < lo) && (tsl2561Gain == TSL2561_GAIN_1X)) {
				/* Increase the gain to 16x and try again */
				tsl2561_setGain(TSL2561_GAIN_16X);

				/* Drop the previous conversion results */
				tsl2561_getData(&broad, &infra);

				/* Set a flag to indicate we've adjusted the gain */
				autogainCheck = TRUE;
			}

			else if ((broad > hi) && (tsl2561Gain == TSL2561_GAIN_16X)) {
				/* Drop gain to 1x and try again */
				tsl2561_setGain(TSL2561_GAIN_1X);

				/* Drop the previous conversion results */
				tsl2561_getData(&broad, &infra);

				/* Set a flag to indicate we've adjusted the gain */
				autogainCheck = TRUE;
			}

			else {
				/* Nothing to look at here, keep moving ....
				Reading is either valid, or we're already at the chips limits */
				*broadband = broad;
				*ir = infra;
				valid = TRUE;
			}

		}

		else {
			/* If we've already adjusted the gain once, just return the new results.
			This avoids endless loops where a value is at one extreme pre-gain,
			and at the other extreme post-gain */
			*broadband = broad;
			*ir = infra;
			valid = TRUE;
		}

	} while (valid == FALSE);

}

/**************************************************************************/
/*!
    Function to read luminosity on both channels
*/
/**************************************************************************/
void tsl2561_getData (uint16_t *broadband, uint16_t *ir)
{
  /* Wake up */
  tsl2561_enable();

  /* Wait x ms for the ADC to complete its operation */
  switch (tsl2561IntegrationTime)
  {
    case TSL2561_INTEGRATIONTIME_13MS:
      osDelay(TSL2561_DELAY_INTTIME_13MS);  // KTOWN: Was 14ms
      break;

    case TSL2561_INTEGRATIONTIME_101MS:
      osDelay(TSL2561_DELAY_INTTIME_101MS); // KTOWN: Was 102ms
      break;

    case TSL2561_INTEGRATIONTIME_402MS:
    default:
      osDelay(TSL2561_DELAY_INTTIME_402MS); // KTOWN: Was 403ms
      break;
  }

  /* Reads a two byte value from channel 0 (visible + infrared) */
  uint8_t data[2];
  uint8_t command_code = TSL2561_COMMAND_BIT | TSL2561_WORD_BIT | TSL2561_REGISTER_CHAN0_LOW;
  retval_t statusRx = i2c_register_read(tsl2561->i2cDriver, tsl2561->i2cHandle, command_code, 1, data, 2);
  broadband[0] = (data[1] << 8) | data[0];

  /* Reads a two byte value from channel 1 (infrared) */
  command_code = TSL2561_COMMAND_BIT | TSL2561_WORD_BIT | TSL2561_REGISTER_CHAN1_LOW;
  statusRx = i2c_register_read(tsl2561->i2cDriver, tsl2561->i2cHandle, command_code, 1, data, 2);
  ir[0] = (data[1] << 8) | data[0];

#if defined DEBUG_PERIPH_ERRORS
	if (statusRx == RET_OK){
		gpio_pin_toggle(STM_GPIOC, STM_GPIO_PIN_9); //Toggle the green LED
		gpio_pin_reset(STM_GPIOC, STM_GPIO_PIN_8); //Clear the red LED
//		HAL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin);
	}
	else
	{
		gpio_pin_toggle(STM_GPIOC, STM_GPIO_PIN_8); //Toggle the red LED
		gpio_pin_reset(STM_GPIOC, STM_GPIO_PIN_9); //Clear the green LED
//		HAL_GPIO_TogglePin(LD4_GPIO_Port, LD4_Pin);
	}
#endif

  /* Turn the device off to save power */
  tsl2561_disable();
}

/**************************************************************************/
/*!
    @brief  Converts the raw sensor values to the standard SI lux equivalent.
    @param  broadband: The 16-bit sensor reading from the IR+visible light diode.
    @param  ir: The 16-bit sensor reading from the IR-only light diode.
    @returns The integer Lux value we calculate.
             Returns 0 if the sensor is saturated and the values are
             unreliable, or 65536 if the sensor is saturated.
*/
/**************************************************************************/
uint32_t tsl2561_calculateLux(uint16_t broadband, uint16_t ir)
{
	unsigned long chScale;
	unsigned long channel1;
	unsigned long channel0;

	/* Make sure the sensor isn't saturated! */
	uint16_t clipThreshold;
	switch (tsl2561IntegrationTime) {
		case TSL2561_INTEGRATIONTIME_13MS:
			clipThreshold = TSL2561_CLIPPING_13MS;
			break;

		case TSL2561_INTEGRATIONTIME_101MS:
			clipThreshold = TSL2561_CLIPPING_101MS;
			break;

		case TSL2561_INTEGRATIONTIME_402MS:
		default:
			clipThreshold = TSL2561_CLIPPING_402MS;
			break;
	}

	/* Return 65536 lux if the sensor is saturated */
	if ((broadband > clipThreshold) || (ir > clipThreshold)) {
		return 65536;
	}

	/* Get the correct scale depending on the integration time */
	switch (tsl2561IntegrationTime) {
		case TSL2561_INTEGRATIONTIME_13MS:
			chScale = TSL2561_LUX_CHSCALE_TINT0;
			break;

		case TSL2561_INTEGRATIONTIME_101MS:
			chScale = TSL2561_LUX_CHSCALE_TINT1;
			break;

		case TSL2561_INTEGRATIONTIME_402MS:	 /* No scaling ... integration time = 402ms */
		default:
			chScale = (1 << TSL2561_LUX_CHSCALE);
			break;
	}

	/* Scale if gain is not 16x */
	if (tsl2561Gain == TSL2561_GAIN_1X) {
		chScale = chScale << 4;	// Scale 1X to 16X
	}

	/* Scale the channel values */
	channel0 = (broadband * chScale) >> TSL2561_LUX_CHSCALE;
	channel1 = (ir * chScale) >> TSL2561_LUX_CHSCALE;

	/* Find the ratio of the channel values (Channel1/Channel0) */
	unsigned long ratio1 = 0;
	if (channel0 != 0) {	// Protect against divide by 0
		ratio1 = (channel1 << (TSL2561_LUX_RATIOSCALE+1)) / channel0;
	}

	/* Round the ratio value */
	unsigned long ratio = (ratio1 + 1) >> 1;

	unsigned int b, m;

	#ifdef TSL2561_PACKAGE_CS
		if ((ratio >= 0) && (ratio <= TSL2561_LUX_K1C)) {
			b = TSL2561_LUX_B1C;
			m = TSL2561_LUX_M1C;
		}
		else if (ratio <= TSL2561_LUX_K2C) {
			b = TSL2561_LUX_B2C;
			m = TSL2561_LUX_M2C;
		}
		else if (ratio <= TSL2561_LUX_K3C) {
			b = TSL2561_LUX_B3C;
			m = TSL2561_LUX_M3C;
		}
		else if (ratio <= TSL2561_LUX_K4C) {
			b = TSL2561_LUX_B4C;
			m = TSL2561_LUX_M4C;
		}
		else if (ratio <= TSL2561_LUX_K5C) {
			b = TSL2561_LUX_B5C;
			m = TSL2561_LUX_M5C;
		}
		else if (ratio <= TSL2561_LUX_K6C) {
			b = TSL2561_LUX_B6C;
			m = TSL2561_LUX_M6C;
		}
		else if (ratio <= TSL2561_LUX_K7C) {
			b = TSL2561_LUX_B7C;
			m = TSL2561_LUX_M7C;
		}
		else if (ratio > TSL2561_LUX_K8C) {
			b = TSL2561_LUX_B8C;
			m = TSL2561_LUX_M8C;
		}
	#else	// T, FN or CL package
		if ((ratio >= 0) && (ratio <= TSL2561_LUX_K1T)) {
			b = TSL2561_LUX_B1T;
			m = TSL2561_LUX_M1T;
		}
		else if (ratio <= TSL2561_LUX_K2T) {
			b = TSL2561_LUX_B2T;
			m = TSL2561_LUX_M2T;
		}
		else if (ratio <= TSL2561_LUX_K3T) {
			b = TSL2561_LUX_B3T;
			m = TSL2561_LUX_M3T;
		}
		else if (ratio <= TSL2561_LUX_K4T) {
			b = TSL2561_LUX_B4T;
			m = TSL2561_LUX_M4T;
		}
		else if (ratio <= TSL2561_LUX_K5T) {
			b = TSL2561_LUX_B5T;
			m = TSL2561_LUX_M5T;
		}
		else if (ratio <= TSL2561_LUX_K6T) {
			b = TSL2561_LUX_B6T;
			m = TSL2561_LUX_M6T;
		}
		else if (ratio <= TSL2561_LUX_K7T) {
			b = TSL2561_LUX_B7T;
			m = TSL2561_LUX_M7T;
		}
		else if (ratio > TSL2561_LUX_K8T) {
			b = TSL2561_LUX_B8T;
			m = TSL2561_LUX_M8T;
		}
	#endif

	unsigned long temp;
	temp = ((channel0 * b) - (channel1 * m));

	/* Do not allow a negative lux value */
	if (temp < 0) {
		temp = 0;
	}

	/* Round lsb (2^(LUX_SCALE-1)) */
	temp += (1 << (TSL2561_LUX_LUXSCALE-1));

	/* Strip off fractional portion */
	uint32_t lux = temp >> TSL2561_LUX_LUXSCALE;

	/* Signal I2C had no errors */
	return lux;
}

/*Calculate the value for the luminosity*/
uint32_t tsl2561_readLuminosity (void)
{
	uint16_t broadband,ir;
	tsl2561_getData(&broadband, &ir);
	return tsl2561_calculateLux(broadband, ir);
}
