/*
 * nbiot_bc95.c
 *
 *  Created on: 23 oct. 2018
 *      Author: albarc
 */

/* Includes ------------------------------------------------------------------*/
#include "bc95.h"

/* Variables ------------------------------------------------------------------*/
bc95_t* bc95;

osMutexDef (bc95_mutex);

//extern UART_HandleTypeDef huart1;

retval_t
bc95_create (bc95_t** device)
{
    /* Allocate memory */
    bc95_t* new_device = pvPortMalloc(sizeof(bc95_t));
    if (new_device == NULL)
    {
        return RET_ERROR;
    }

    new_device->lock = osMutexCreate(osMutex(bc95_mutex));
    new_device->state = DRIVER_NOT_INIT;

    *device = new_device;
    return RET_OK;
}

retval_t
bc95_delete (bc95_t* device)
{
    if (device == NULL)
    {
        return RET_ERROR;
    }

    if (device->state != DRIVER_NOT_INIT)
    {
        return RET_ERROR;
    }

    /* Wait for mutex */
    osMutexWait(device->lock, osWaitForever);
    device->state = DRIVER_NOT_INIT;
    osMutexRelease(device->lock);

    /* Delete mutex */
    osMutexDelete(device->lock);

    /* Free memory */
    vPortFree(device);

    return RET_OK;
}

retval_t
bc95_init (bc95_t* device,
              uart_driver_t* uartDriver)
//              bc95_config_t* bc95config,
//			  uint8_t* data_config,
//              gpioPort_t nDRDYport,
//              gpioPin_t nDRDYpin)
{
    if (device == NULL)
    {
        return RET_ERROR;
    }

    if (uartDriver == NULL)
    {
        return RET_ERROR;
    }

    /* Wait for mutex */
    osMutexWait(device->lock, osWaitForever);

    /* Check for already initialized driver */
    if(device->state != DRIVER_NOT_INIT)
    {
        osMutexRelease(device->lock);
        return RET_ERROR;
    }

    device->uartDriver = uartDriver;

    /* Create UART handle */
    device->uartHandle = uart_port_open(device->uartDriver);
    if (device->uartHandle == -1)
    {
        osMutexRelease(device->lock);
        return RET_ERROR;
    }

//
//    /* Setup config parameters */
//    if (bc95_register_write (device,
//                                BC95_CONFIG_REG,
//                                data_config) != RET_OK)
//    {
//        bc95_disable(device);
//        uart_port_close (device->uartDriver, device->uartHandle);
//        osMutexRelease(device->lock);
//        return RET_ERROR;
//    }

    device->state = DRIVER_READY;

    osMutexRelease(device->lock);
    return RET_OK;
}

retval_t
bc95_deinit (bc95_t* device)
{
    if (device == NULL)
    {
        return RET_ERROR;
    }

    /* Wait for mutex */
    osMutexWait(device->lock, osWaitForever);

    /* Close UART port */
    uart_port_close(device->uartDriver, device->uartHandle);

    device->state = DRIVER_NOT_INIT;
    osMutexRelease(device->lock);
    return RET_OK;
}

retval_t
bc95_write (bc95_t* device,
			uint8_t* data,
			uint16_t size)
{
	return uart_write(device->uartDriver, device->uartHandle, data, size);
}

/**************************************************************************/
/*!
    Reboots the device

    AT+NRB
*/
/**************************************************************************/
void bc95_reboot (bc95_t* device)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = bc95_create_non_param_AT_command(REBOOT, &at_command);

	/* Send it via UART*/
	bc95_write(device, (uint8_t*) at_command, command_length);
//	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	/*Free meomory*/
#ifdef BC95_DYN_MEM
	vPortFree(at_command);
#endif
}

/**************************************************************************/
/*!
    Registers the device in the network

    AT+CEREG=2
*/
/**************************************************************************/
void bc95_network_register (bc95_t* device)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = bc95_create_one_param_AT_command(NWK_REGISTER, NWK_REGISTER_PARAM, &at_command);

	/* Send it via UART*/
	bc95_write(device, (uint8_t*) at_command, command_length);
//	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	/*Free meomory*/
#ifdef BC95_DYN_MEM
	vPortFree(at_command);
#endif
}

/**************************************************************************/
/*!
    Receive notifications if the device status changes

    AT+CSCON=1
*/
/**************************************************************************/
void bc95_enable_connection_status_messages (bc95_t* device)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = bc95_create_one_param_AT_command(CONNECTION_STATUS, CONNECTION_STATUS_PARAM, &at_command);

	/* Send it via UART*/
	bc95_write(device, (uint8_t*) at_command, command_length);
//	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

#ifdef BC95_DYN_MEM
	/*Free meomory*/
	vPortFree(at_command);
#endif
}

/**************************************************************************/
/*!
    Puts the device in full functionality mode

    AT+CFUN=1
*/
/**************************************************************************/
void bc95_set_full_functionality (bc95_t* device)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = bc95_create_one_param_AT_command(SET_FUNCTIONALITY, SET_FUNCTIONALITY_PARAM, &at_command);

	/* Send it via UART*/
	bc95_write(device, (uint8_t*) at_command, command_length);
//	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

#ifdef BC95_DYN_MEM
	/*Free meomory*/
	vPortFree(at_command);
#endif
}

/**************************************************************************/
/*!
    Sets the Packet Data Protocol (PDP) context

    AT+CGDCONT=0,"IP",""
*/
/**************************************************************************/
void bc95_set_pdp_context (bc95_t* device)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = bc95_create_three_param_AT_command(PDP_CONTEXT, SET_PDP_CONTEXT_PARAM_1,
			SET_PDP_CONTEXT_PARAM_2, SET_PDP_CONTEXT_PARAM_3, &at_command);

	/* Send it via UART*/
	bc95_write(device, (uint8_t*) at_command, command_length);
//	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

#ifdef BC95_DYN_MEM
	/*Free meomory*/
	vPortFree(at_command);
#endif
}

/**************************************************************************/
/*!
    Gets the Packet Data Protocol (PDP) context

    AT+CGDCONT?
*/
/**************************************************************************/
void bc95_get_pdp_context (bc95_t* device)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = bc95_create_get_AT_command(PDP_CONTEXT, &at_command);

	/* Send it via UART*/
	bc95_write(device, (uint8_t*) at_command, command_length);
//	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

#ifdef BC95_DYN_MEM
	/*Free meomory*/
	vPortFree(at_command);
#endif
}

/**************************************************************************/
/*!
    Connects to the specified network

    AT+COPS=1,2,"21401"
*/
/**************************************************************************/
void bc95_connect_to_network (bc95_t* device)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = bc95_create_three_param_AT_command(OPERATOR_SELECTION, OPERATOR_SELECTION_PARAM_1,
			OPERATOR_SELECTION_PARAM_2, VODAFONE_NETWORK_CODE, &at_command);

	/* Send it via UART*/
	bc95_write(device, (uint8_t*) at_command, command_length);
//	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

#ifdef BC95_DYN_MEM
	/*Free meomory*/
	vPortFree(at_command);
#endif
}

/**************************************************************************/
/*!
    Get the assigned IP address

    AT+CGPADDR
*/
/**************************************************************************/
void bc95_get_ip_address (bc95_t* device)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = bc95_create_non_param_AT_command(GET_ASSIGNED_IP_ADDRESS, &at_command);

	/* Send it via UART*/
	bc95_write(device, (uint8_t*) at_command, command_length);
//	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

#ifdef BC95_DYN_MEM
	/*Free meomory*/
	vPortFree(at_command);
#endif
}

/**************************************************************************/
/*!
    Open a UDP socket and return given reference in argument

    AT+NSOCR=DGRAM,17,16666,1
*/
/**************************************************************************/
void bc95_open_udp_socket (bc95_t* device, char** socket_reference)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = bc95_create_four_param_AT_command(CREATE_SOCKET, CREATE_SOCKET_PARAM_TYPE,
			CREATE_SOCKET_PARAM_PROTOCOL, CREATE_SOCKET_PARAM_LIST_PORT, CREATE_SOCKET_PARAM_REF, &at_command);

	/* Send it via UART*/
	bc95_write(device, (uint8_t*) at_command, command_length);
//	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	/*Free meomory*/
#ifdef BC95_DYN_MEM
	vPortFree(at_command);
#endif
	/* Wait for response */
	// HALUARTRECEIVEWITHTIMEOUT!!

	/* Return socket reference number in argument */
	*socket_reference = pvPortMalloc(2);
//	*socket_reference = malloc(2);
	strcpy(*socket_reference, "0");
}

/**************************************************************************/
/*!
    Sends a UDP message (message should be in hex form)

    AT+NSOST=0,35.237.66.97,8888,2,3333
*/
/**************************************************************************/
void bc95_send_udp_message (bc95_t* device, char* socket_reference, char* ip_address, char* port_number, char* message)
{
	/* Convert message to hex */
#ifdef BC95_DYN_MEM
	char* hex_message = pvPortMalloc(1024); // Como max 512 bytes, y cada uno ocupa 2 caracteres hex
#else
	char hex_message[1024];
#endif
	uint8_t hex_length = bc95_string_to_hex(message, (char**)&hex_message);

	/* Calculate message length */
	uint8_t message_length = hex_length/2;	// La mitad de bytes que de caracteres hex
#ifdef BC95_DYN_MEM
	char* message_length_field = pvPortMalloc(3); // Como máximo 512 bytes (3 cifras)
#else
	char message_length_field[3];
#endif
	sprintf(message_length_field, "%d", message_length);

	/* Calculate command length */
	uint16_t command_length = strlen(AT_PREFIX) + strlen(SEND_UDP_DATAGRAM) + strlen(EQUAL_SIGN)
			+ strlen(socket_reference) + strlen(COMMA_SIGN) + strlen(ip_address) + strlen(COMMA_SIGN)
			+ strlen(port_number) + strlen(COMMA_SIGN) + strlen(message_length_field) + strlen(COMMA_SIGN)
			+ hex_length + strlen(END_OF_COMMAND);

	/* Build command */
#ifdef BC95_DYN_MEM
	char *at_command = pvPortMalloc(command_length + 1); // +1 for '\0' string termination
#else
	char at_command[command_length + 1];
#endif

//	char *at_command = malloc(command_length + 1);	// +1 for '\0' string termination
	strcpy(at_command, AT_PREFIX);
	strcat(at_command, SEND_UDP_DATAGRAM);
	strcat(at_command, EQUAL_SIGN);
	strcat(at_command, socket_reference);
	strcat(at_command, COMMA_SIGN);
	strcat(at_command, ip_address);
	strcat(at_command, COMMA_SIGN);
	strcat(at_command, port_number);
	strcat(at_command, COMMA_SIGN);
	strcat(at_command, message_length_field);
	strcat(at_command, COMMA_SIGN);
	strcat(at_command, hex_message);
	strcat(at_command, END_OF_COMMAND);

	/* Send it via UART*/
	bc95_write(device, (uint8_t*) at_command, command_length);
//	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	/*Free meomory*/
#ifdef BC95_DYN_MEM
	vPortFree(hex_message);
	vPortFree(message_length_field);
	vPortFree(at_command);
#endif

}

/**************************************************************************/
/*!
    Close the UDP socket passed in argument

    AT+NSOCL=0
*/
/**************************************************************************/
void bc95_close_udp_socket (bc95_t* device, char* socket_reference)
{
	/* Create AT command */
	char* at_command;
	uint8_t command_length = bc95_create_one_param_AT_command (CLOSE_SOCKET, socket_reference, &at_command);

	/* Send it via UART*/
	bc95_write(device, (uint8_t*) at_command, command_length);
//	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	/* Free memory*/
#ifdef BC95_DYN_MEM
	vPortFree(at_command);
	vPortFree(socket_reference);
#endif
}

/**************************************************************************/
/*!
    Sends all connection commands at once

    AT+CEREG=2;+CSCON=1;+CFUN=1;+CGDCONT=0,"IP","";+COPS=1,2,"21401";
*/
/**************************************************************************/
void bc95_all_connection_commands (bc95_t* device)
{
	/* Create AT command */
	uint8_t command_length = strlen(AT_PREFIX)
					+ strlen(NWK_REGISTER) + strlen(EQUAL_SIGN)	+ strlen(NWK_REGISTER_PARAM) + strlen(SEMICOLON_SIGN)
					+ strlen(PLUS_SIGN) + strlen(CONNECTION_STATUS)	+ strlen(EQUAL_SIGN)
					+ strlen(CONNECTION_STATUS_PARAM) + strlen(SEMICOLON_SIGN)
					+ strlen(PLUS_SIGN) + strlen(SET_FUNCTIONALITY)	+ strlen(EQUAL_SIGN)
					+ strlen(SET_FUNCTIONALITY_PARAM) + strlen(SEMICOLON_SIGN)
					+ strlen(PLUS_SIGN) + strlen(PDP_CONTEXT) + strlen(EQUAL_SIGN)
					+ strlen(SET_PDP_CONTEXT_PARAM_1) + strlen(COMMA_SIGN) + strlen(SET_PDP_CONTEXT_PARAM_2)
					+ strlen(COMMA_SIGN) + strlen(SET_PDP_CONTEXT_PARAM_3) + strlen(SEMICOLON_SIGN)
					+ strlen(PLUS_SIGN) + strlen(OPERATOR_SELECTION) + strlen(EQUAL_SIGN)
					+ strlen(OPERATOR_SELECTION_PARAM_1) + strlen(COMMA_SIGN) + strlen(OPERATOR_SELECTION_PARAM_2)
					+ strlen(COMMA_SIGN) + strlen(VODAFONE_NETWORK_CODE) + strlen(SEMICOLON_SIGN)
					+ strlen(END_OF_COMMAND);

	/* Build command */
#ifdef BC95_DYN_MEM
	char *at_command = pvPortMalloc(command_length + 1); // +1 for '\0' string termination
#else
	char at_command[command_length + 1]; // +1 for '\0' string termination
#endif
//	char *at_command = malloc(command_length + 1);	// +1 for '\0' string termination
	strcpy(at_command, AT_PREFIX);
	strcat(at_command, NWK_REGISTER);
	strcat(at_command, EQUAL_SIGN);
	strcat(at_command, NWK_REGISTER_PARAM);
	strcat(at_command, SEMICOLON_SIGN);

	strcat(at_command, PLUS_SIGN);
	strcat(at_command, CONNECTION_STATUS);
	strcat(at_command, EQUAL_SIGN);
	strcat(at_command, CONNECTION_STATUS_PARAM);
	strcat(at_command, SEMICOLON_SIGN);

	strcat(at_command, PLUS_SIGN);
	strcat(at_command, SET_FUNCTIONALITY);
	strcat(at_command, EQUAL_SIGN);
	strcat(at_command, SET_FUNCTIONALITY_PARAM);
	strcat(at_command, SEMICOLON_SIGN);

	strcat(at_command, PLUS_SIGN);
	strcat(at_command, PDP_CONTEXT);
	strcat(at_command, EQUAL_SIGN);
	strcat(at_command, SET_PDP_CONTEXT_PARAM_1);
	strcat(at_command, COMMA_SIGN);
	strcat(at_command, SET_PDP_CONTEXT_PARAM_2);
	strcat(at_command, COMMA_SIGN);
	strcat(at_command, SET_PDP_CONTEXT_PARAM_3);
	strcat(at_command, SEMICOLON_SIGN);

	strcat(at_command, PLUS_SIGN);
	strcat(at_command, OPERATOR_SELECTION);
	strcat(at_command, EQUAL_SIGN);
	strcat(at_command, OPERATOR_SELECTION_PARAM_1);
	strcat(at_command, COMMA_SIGN);
	strcat(at_command, OPERATOR_SELECTION_PARAM_2);
	strcat(at_command, COMMA_SIGN);
	strcat(at_command, VODAFONE_NETWORK_CODE);
	strcat(at_command, SEMICOLON_SIGN);

	strcat(at_command, END_OF_COMMAND);

	/* Send it via UART*/
	bc95_write(device, (uint8_t*) at_command, command_length);
//	HAL_UART_Transmit(&huart1, (uint8_t*) at_command, command_length, 0xFFFF);

	/* Free Memory*/
#ifdef BC95_DYN_MEM
	vPortFree(at_command);
#endif
}

/**************************************************************************/
/*!
    Set up nb-iot module before sending messages
*/
/**************************************************************************/
void bc95_setup_nbiot_module (bc95_t* device, char** socket_reference)
{
	// Send dummy data to make sure nb-iot module receives next commands correctly
	printf("\r\n");

	// Reboot module
	bc95_reboot(device);
	osDelay(WAIT_AFTER_REBOOT_MS);
//	HAL_Delay(WAIT_AFTER_REBOOT_MS);

	// Send connection parameters
	bc95_all_connection_commands(device);
	osDelay(WAIT_AFTER_CONNECT_MS);
//	HAL_Delay(WAIT_AFTER_CONNECT_MS);

	// Open socket
	bc95_open_udp_socket(device, socket_reference);
	osDelay(WAIT_AFTER_SOCKET_MS);
//	HAL_Delay(WAIT_AFTER_SOCKET_MS);
}

/**************************************************************************/
/*!
    Send nbiot message, hiding details to the main app
*/
/**************************************************************************/
void bc95_send_nbiot_message (bc95_t* device, char* socket_reference, char* message)
{
	bc95_send_udp_message(device, socket_reference, SEND_UDP_DATAGRAM_PARAM_IP, SEND_UDP_DATAGRAM_PARAM_PORT, message);
}


/**************************************************************************/
/*!
    Convert string to hex
*/
/**************************************************************************/
uint8_t bc95_string_to_hex (char* input, char** output)
{
#ifdef BC95_DYN_MEM
	char* aux_in = pvPortMalloc(512);
	char* aux_out = pvPortMalloc(512);
#else
	char aux_in[512];
	char aux_out[512];
#endif

	int i, j;

	strcpy(aux_in, input);
	memset(aux_out, 0, 512);	// Fill aux_out with nulls

	for(i=0, j=0; i < strlen(aux_in); i++,j+=2) {
		sprintf((char*)aux_out+j,"%02X",aux_in[i]);
	}
	aux_out[j] = '\0';
	strcpy(*output, aux_out);

#ifdef BC95_DYN_MEM
	vPortFree(aux_in);
	vPortFree(aux_out);
#endif

	return strlen(*output);
}

/**************************************************************************/
/*!
    Create get AT command: AT+CODE?

    Returns resulting command in pointer 'at_command' passed as argument
*/
/**************************************************************************/
uint8_t bc95_create_get_AT_command (char* command_code, char** at_command)
{
	/* Create AT command */
	uint8_t length = strlen(AT_PREFIX) + strlen(command_code) + strlen(QUESTION_MARK) + strlen(END_OF_COMMAND);
#ifdef BC95_DYN_MEM
	*at_command = pvPortMalloc(length + 1); // +1 for '\0' string termination
#else
	char at_command_aux[length + 1];
	*at_command = at_command_aux;
#endif
//	*at_command = malloc(length + 1);	// +1 for '\0' string termination
	strcpy(*at_command, AT_PREFIX);
	strcat(*at_command, command_code);
	strcat(*at_command, QUESTION_MARK);
	strcat(*at_command, END_OF_COMMAND);

	return length;
}

/**************************************************************************/
/*!
    Create AT command with no parameters: AT+CODE

    Returns resulting command in pointer 'at_command' passed as argument
*/
/**************************************************************************/
uint8_t bc95_create_non_param_AT_command (char* command_code, char** at_command)
{
	/* Create AT command */
	uint8_t length = strlen(AT_PREFIX) + strlen(command_code) + strlen(END_OF_COMMAND);
#ifdef BC95_DYN_MEM
	*at_command = pvPortMalloc(length + 1); // +1 for '\0' string termination
#else
	char at_command_aux[length + 1];
	*at_command = at_command_aux;
#endif
//	*at_command = malloc(length + 1);	// +1 for '\0' string termination
	strcpy(*at_command, AT_PREFIX);
	strcat(*at_command, command_code);
	strcat(*at_command, END_OF_COMMAND);

	return length;
}

/**************************************************************************/
/*!
    Create AT command with one parameter: AT+CODE=PARAM

    Returns resulting command in pointer 'at_command' passed as argument
*/
/**************************************************************************/
uint8_t bc95_create_one_param_AT_command (char* command_code, char* command_param, char** at_command)
{
	/* Create AT command */
	uint8_t length = strlen(AT_PREFIX) + strlen(command_code) + strlen(EQUAL_SIGN)
			+ strlen(command_param) + strlen(END_OF_COMMAND);
#ifdef BC95_DYN_MEM
	*at_command = pvPortMalloc(length + 1); // +1 for '\0' string termination
#else
	char at_command_aux[length + 1];
	*at_command = at_command_aux;
#endif
//	*at_command = malloc(length + 1);	// +1 for '\0' string termination
	strcpy(*at_command, AT_PREFIX);
	strcat(*at_command, command_code);
	strcat(*at_command, EQUAL_SIGN);
	strcat(*at_command, command_param);
	strcat(*at_command, END_OF_COMMAND);

	return length;
}

/**************************************************************************/
/*!
    Create AT command with three parameters: AT+CODE=PARAM,PARAM,PARAM

    Returns resulting command in pointer 'at_command' passed as argument
*/
/**************************************************************************/
uint8_t bc95_create_three_param_AT_command (char* command_code, char* command_param_1,
		char* command_param_2, char* command_param_3, char** at_command)
{
	/* Create AT command */
	uint8_t length = strlen(AT_PREFIX) + strlen(command_code) + strlen(EQUAL_SIGN)
			+ strlen(command_param_1) + strlen(COMMA_SIGN) + strlen(command_param_2) +strlen(COMMA_SIGN)
			+ strlen (command_param_3) + strlen(END_OF_COMMAND);
#ifdef BC95_DYN_MEM
	*at_command = pvPortMalloc(length + 1); // +1 for '\0' string termination
#else
	char at_command_aux[length + 1];
	*at_command = at_command_aux;
#endif
//	*at_command = malloc(length + 1);	// +1 for '\0' string termination
	strcpy(*at_command, AT_PREFIX);
	strcat(*at_command, command_code);
	strcat(*at_command, EQUAL_SIGN);
	strcat(*at_command, command_param_1);
	strcat(*at_command, COMMA_SIGN);
	strcat(*at_command, command_param_2);
	strcat(*at_command, COMMA_SIGN);
	strcat(*at_command, command_param_3);
	strcat(*at_command, END_OF_COMMAND);

	return length;
}


/**************************************************************************/
/*!
    Create AT command with four parameters: AT+CODE=PARAM,PARAM,PARAM,PARAM

    Returns resulting command in pointer 'at_command' passed as argument
*/
/**************************************************************************/
uint8_t bc95_create_four_param_AT_command (char* command_code, char* command_param_1,
		char* command_param_2, char* command_param_3, char* command_param_4, char** at_command)
{
	/* Create AT command */
	uint8_t length = strlen(AT_PREFIX) + strlen(command_code) + strlen(EQUAL_SIGN)
			+ strlen(command_param_1) + strlen(COMMA_SIGN) + strlen(command_param_2) +strlen(COMMA_SIGN)
			+ strlen (command_param_3) +strlen(COMMA_SIGN) + strlen (command_param_4) + strlen(END_OF_COMMAND);
#ifdef BC95_DYN_MEM
	*at_command = pvPortMalloc(length + 1); // +1 for '\0' string termination
#else
	char at_command_aux[length + 1];
	*at_command = at_command_aux;
#endif
//	*at_command = malloc(length + 1);	// +1 for '\0' string termination
	strcpy(*at_command, AT_PREFIX);
	strcat(*at_command, command_code);
	strcat(*at_command, EQUAL_SIGN);
	strcat(*at_command, command_param_1);
	strcat(*at_command, COMMA_SIGN);
	strcat(*at_command, command_param_2);
	strcat(*at_command, COMMA_SIGN);
	strcat(*at_command, command_param_3);
	strcat(*at_command, COMMA_SIGN);
	strcat(*at_command, command_param_4);
	strcat(*at_command, END_OF_COMMAND);

	return length;
}
