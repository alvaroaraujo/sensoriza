/*
 * nbiot_bc95.h
 *
 *  Created on: 23 oct. 2018
 *      Author: albarc
 */

#ifndef INC_NBIOT_BC95_H_
#define INC_NBIOT_BC95_H_

/* Includes ------------------------------------------------------------------*/
#include "os_uart.h"
#include "string.h"
#include "stdlib.h"
#include "types.h"
#include "stm32l1xx_hal.h"

/** Boolean Constants */
#define TRUE	1
#define FALSE	0

/** BC95 Constants */
#define BC95_BAUDRATE							9600
#define VODAFONE_NETWORK_CODE					"\"21401\""			// Vodafone ID (escape quotes)
#define WAIT_AFTER_REBOOT_MS					5000
#define WAIT_AFTER_CONNECT_MS					20000
#define WAIT_AFTER_SOCKET_MS					2000

/** BC95 AT COMMANDS */
/** COMMAND CODES */
#define AT_PREFIX								"AT+"
#define EQUAL_SIGN								"="
#define COMMA_SIGN								","
#define SEMICOLON_SIGN							";"
#define PLUS_SIGN								"+"
#define QUESTION_MARK							"?"
#define END_OF_COMMAND							"\r"
#define	REBOOT									"NRB"
#define NWK_REGISTER							"CEREG"
#define CONNECTION_STATUS						"CSCON"
#define SET_FUNCTIONALITY						"CFUN"
#define PDP_CONTEXT								"CGDCONT"
#define OPERATOR_SELECTION						"COPS"
#define GET_ASSIGNED_IP_ADDRESS					"CGPADDR"
#define CREATE_SOCKET							"NSOCR"
#define SEND_UDP_DATAGRAM						"NSOST"
#define RECEIVE_UDP_DATAGRAM					"NSORF"
#define CLOSE_SOCKET							"NSOCL"

/** COMMAND PARAMETERS */
#define NWK_REGISTER_PARAM						"2"				// Enable registration and location info in response
#define CONNECTION_STATUS_PARAM					"1"				// Result code only mode
#define SET_FUNCTIONALITY_PARAM					"1"				// Full functionality
#define SET_PDP_CONTEXT_PARAM_1					"0"				// Context ID
#define SET_PDP_CONTEXT_PARAM_2					"\"IP\""		// Type of IP connection (escape quotes)
#define SET_PDP_CONTEXT_PARAM_3					"\"\""			// Name of network, leave empty (escape quotes)
#define OPERATOR_SELECTION_PARAM_1				"1"				// Manual
#define OPERATOR_SELECTION_PARAM_2				"2"				// Numeric format
#define CREATE_SOCKET_PARAM_TYPE				"DGRAM"			// Type (datagram)
#define CREATE_SOCKET_PARAM_PROTOCOL			"17"			// UDP code
#define CREATE_SOCKET_PARAM_LIST_PORT			"16666"			// Listening port
#define CREATE_SOCKET_PARAM_REF					"1"				// Reference to the created socket (to use in further sendings)
#define SEND_UDP_DATAGRAM_PARAM_REF				0				// Socket number returned by NSOCR (maybe not a define)
#define SEND_UDP_DATAGRAM_PARAM_IP				"35.237.66.97"	// Remote server IP
#define SEND_UDP_DATAGRAM_PARAM_PORT			"8888"			// Remote server port
#define RECEIVE_UDP_DATAGRAM_PARAM_REF			0				// Socket number returned by NSOCR (maybe not a define)
#define RECEIVE_UDP_DATAGRAM_PARAM_MAX_LENGTH	80				// Received packet max length (in bytes)
#define CLOSE_SOCKET_PARAM_REF					0				// Socket number returned by NSOCR (maybe not a define)


/* Type Definitions  *********************************************/
typedef struct bc95_ {
	uart_driver_t* uartDriver;
	uint16_t uartHandle;
	driver_state_t state;
	osMutexId lock;
}bc95_t;


/* Functions  ****************************************************/
//void reboot (void);
//void network_register (void);
//void enable_connection_status_messages (void);
//void set_full_functionality (void);
//void set_pdp_context (void);
//void get_pdp_context (void);
//void connect_to_network (void);
//void get_ip_address (void);
//void open_udp_socket (char** socket_reference);
//void send_udp_message (char* socket_reference, char* ip_address, char* port_number, char* message);
//void close_udp_socket (char* socket_reference);
//void all_connection_commands (void);
//
//uint8_t string_to_hex (char* input, char** output);
//uint8_t create_get_AT_command (char* command_code, char** at_command);
//uint8_t create_non_param_AT_command (char* command_code, char** at_command);
//uint8_t create_one_param_AT_command (char* command_code, char* command_param, char** at_command);
//uint8_t create_three_param_AT_command (char* command_code, char* command_param_1, char* command_param_2,
//		char* command_param_3, char** at_command);
//uint8_t create_four_param_AT_command (char* command_code, char* command_param_1,
//		char* command_param_2, char* command_param_3, char* command_param_4, char** at_command);
//

retval_t
bc95_create (bc95_t** device);

retval_t
bc95_delete (bc95_t* device);

retval_t
bc95_init (bc95_t* device,
              uart_driver_t* uartDriver);

retval_t
bc95_deinit (bc95_t* device);

retval_t
bc95_write (bc95_t* device,
			uint8_t* data,
			uint16_t size);

/**************************************************************************/
/*!
    Reboots the device

    AT+NRB
*/
/**************************************************************************/
void bc95_reboot (bc95_t* device);

/**************************************************************************/
/*!
    Registers the device in the network

    AT+CEREG=2
*/
/**************************************************************************/
void bc95_network_register (bc95_t* device);

/**************************************************************************/
/*!
    Receive notifications if the device status changes

    AT+CSCON=1
*/
/**************************************************************************/
void bc95_enable_connection_status_messages (bc95_t* device);

/**************************************************************************/
/*!
    Puts the device in full functionality mode

    AT+CFUN=1
*/
/**************************************************************************/
void bc95_set_full_functionality (bc95_t* device);

/**************************************************************************/
/*!
    Sets the Packet Data Protocol (PDP) context

    AT+CGDCONT=0,"IP",""
*/
/**************************************************************************/
void bc95_set_pdp_context (bc95_t* device);

/**************************************************************************/
/*!
    Gets the Packet Data Protocol (PDP) context

    AT+CGDCONT?
*/
/**************************************************************************/
void bc95_get_pdp_context (bc95_t* device);

/**************************************************************************/
/*!
    Connects to the specified network

    AT+COPS=1,2,"21401"
*/
/**************************************************************************/
void bc95_connect_to_network (bc95_t* device);

/**************************************************************************/
/*!
    Get the assigned IP address

    AT+CGPADDR
*/
/**************************************************************************/
void bc95_get_ip_address (bc95_t* device);

/**************************************************************************/
/*!
    Open a UDP socket and return given reference in argument

    AT+NSOCR=DGRAM,17,16666,1
*/
/**************************************************************************/
void bc95_open_udp_socket (bc95_t* device, char** socket_reference);

/**************************************************************************/
/*!
    Sends a UDP message (message should be in hex form)

    AT+NSOST=0,35.237.66.97,8888,2,3333
*/
/**************************************************************************/
void bc95_send_udp_message (bc95_t* device, char* socket_reference, char* ip_address, char* port_number, char* message);

/**************************************************************************/
/*!
    Close the UDP socket passed in argument

    AT+NSOCL=0
*/
/**************************************************************************/
void bc95_close_udp_socket (bc95_t* device, char* socket_reference);

/**************************************************************************/
/*!
    Sends all connection commands at once

    AT+CEREG=2;+CSCON=1;+CFUN=1;+CGDCONT=0,"IP","";+COPS=1,2,"21401";
*/
/**************************************************************************/
void bc95_all_connection_commands (bc95_t* device);

/**************************************************************************/
/*!
    Set up nb-iot module before sending messages
*/
/**************************************************************************/
void bc95_setup_nbiot_module (bc95_t* device, char** socket_reference);

/**************************************************************************/
/*!
    Send nbiot message, hiding details to the main app
*/
/**************************************************************************/
void bc95_send_nbiot_message (bc95_t* device, char* socket_reference, char* message);

/**************************************************************************/
/*!
    Convert string to hex
*/
/**************************************************************************/
uint8_t bc95_string_to_hex (char* input, char** output);

/**************************************************************************/
/*!
    Create get AT command: AT+CODE?

    Returns resulting command in pointer 'at_command' passed as argument
*/
/**************************************************************************/
uint8_t bc95_create_get_AT_command (char* command_code, char** at_command);

/**************************************************************************/
/*!
    Create AT command with no parameters: AT+CODE

    Returns resulting command in pointer 'at_command' passed as argument
*/
/**************************************************************************/
uint8_t bc95_create_non_param_AT_command (char* command_code, char** at_command);

/**************************************************************************/
/*!
    Create AT command with one parameter: AT+CODE=PARAM

    Returns resulting command in pointer 'at_command' passed as argument
*/
/**************************************************************************/
uint8_t bc95_create_one_param_AT_command (char* command_code, char* command_param, char** at_command);

/**************************************************************************/
/*!
    Create AT command with three parameters: AT+CODE=PARAM,PARAM,PARAM

    Returns resulting command in pointer 'at_command' passed as argument
*/
/**************************************************************************/
uint8_t bc95_create_three_param_AT_command (char* command_code, char* command_param_1,
		char* command_param_2, char* command_param_3, char** at_command);

/**************************************************************************/
/*!
    Create AT command with four parameters: AT+CODE=PARAM,PARAM,PARAM,PARAM

    Returns resulting command in pointer 'at_command' passed as argument
*/
/**************************************************************************/
uint8_t bc95_create_four_param_AT_command (char* command_code, char* command_param_1,
		char* command_param_2, char* command_param_3, char* command_param_4, char** at_command);


#endif /* INC_NBIOT_BC95_H_ */
