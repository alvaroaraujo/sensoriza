/*
 * types.h
 *
 *  Created on: 5 jun. 2018
 *      Author: telek
 */

#ifndef TYPES_H_
#define TYPES_H_

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

typedef enum{
	RET_OK,
	RET_ERROR,
	RET_TIMEOUT
}retval_t;


#endif /* TYPES_H_ */
