/*
 * board_conf.h
 *
 *  Created on: 19 de may. de 2017
 *      Author: jmartin
 */

#ifndef BOARD_CONF_H_
#define BOARD_CONF_H_

#include <stm32l1/gpio_stm32l1.h>
#include "Buttons/debouncer.h"

#define OS_I2C_DRIVER_FUNCS		stm32l1_i2c_funcs
#define OS_SPI_DRIVER_FUNCS		stm32l1_spi_funcs
#define OS_UART_DRIVER_FUNCS	stm32l1_uart_funcs

#define I2C1_DRIVER					1
#define I2C1_SPEED 					100
#define I2C_NONBLOCKINGMODE
//#define I2C1_TIMING 				0x00702991
//
//#define SPI1_DRIVER 				1
//#define SPI1_SPEED					10000000
//#define SPI2_DRIVER 				2
//#define SPI2_SPEED					1000000
//#define SPI_NONBLOCKINGMODE
//#define USART1_SPI_DRIVER 			11
//#define USART1_SPI_SPEED			1000000

#define UART1_DRIVER				1
#define UART_NONBLOCKINGMODE

#define BUTTON_GPIO_COUNT 			2
#define BUTTON_STABLE_COUNT 		2

#endif /* BOARD_CONF_H_ */
