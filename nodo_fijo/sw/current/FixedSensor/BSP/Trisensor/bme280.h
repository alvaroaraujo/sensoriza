/*
 * temphumpres_bme280.h
 *
 *  Created on: 8 may 2018
 *      Author: albarc
 */

#ifndef INC_TEMPHUMPRES_BME280_H_
#define INC_TEMPHUMPRES_BME280_H_

/* Includes ------------------------------------------------------------------*/
//#include "i2c.h"
#include "types.h"
#include "os_i2c.h"
#include "os_gpio.h"

/** Boolean Constants */
#define TRUE	1
#define FALSE	0

/** BME280 Constants */
#define BME280_ADDRESS								(0xEE)				// I2C Address (0x77 << 1)
#define BME280_CHIP_ID_VALUE						(0x60)
#define BME280_SOFTRESET_WORD						(0xB6)
#define BME280_STATUS_CAL_BUSY_MASK					(0x01)				// Last bit in status register indicates device is busy calibrating
#define BME280_STATUS_MEAS_BUSY_MASK				(0x08)				// Bit 3 in status register indicates device is busy measuring
#define BME280_TEMP_PRESS_WRONG_READING_VALUE		(0x80000)			// Reading of TEMP or PRESS register sets, when they're disabled
#define BME280_HUM_WRONG_READING_VALUE				(0x8000)			// Reading of the HUM register set, when it is disabled

#define NAN								0.0/0.0
// Registers
enum
{
	BME280_REGISTER_DIG_T1              = 0x88,
	BME280_REGISTER_DIG_T2              = 0x8A,
	BME280_REGISTER_DIG_T3              = 0x8C,

	BME280_REGISTER_DIG_P1              = 0x8E,
	BME280_REGISTER_DIG_P2              = 0x90,
	BME280_REGISTER_DIG_P3              = 0x92,
	BME280_REGISTER_DIG_P4              = 0x94,
	BME280_REGISTER_DIG_P5              = 0x96,
	BME280_REGISTER_DIG_P6              = 0x98,
	BME280_REGISTER_DIG_P7              = 0x9A,
	BME280_REGISTER_DIG_P8              = 0x9C,
	BME280_REGISTER_DIG_P9              = 0x9E,

	BME280_REGISTER_DIG_H1              = 0xA1,
	BME280_REGISTER_DIG_H2              = 0xE1,
	BME280_REGISTER_DIG_H3              = 0xE3,
	BME280_REGISTER_DIG_H4              = 0xE4,
	BME280_REGISTER_DIG_H5              = 0xE5,
	BME280_REGISTER_DIG_H6              = 0xE7,

	BME280_REGISTER_CHIPID             = 0xD0,
	BME280_REGISTER_VERSION            = 0xD1,
	BME280_REGISTER_SOFTRESET          = 0xE0,

	BME280_REGISTER_CAL26              = 0xE1,  // R calibration stored in 0xE1-0xF0

	BME280_REGISTER_CONTROLHUMID       = 0xF2,
	BME280_REGISTER_STATUS             = 0XF3,
	BME280_REGISTER_CONTROL            = 0xF4,
	BME280_REGISTER_CONFIG             = 0xF5,
	BME280_REGISTER_PRESSUREDATA       = 0xF7,
	BME280_REGISTER_TEMPDATA           = 0xFA,
	BME280_REGISTER_HUMIDDATA          = 0xFD
};

typedef struct bme280_ {
	i2c_driver_t* i2cDriver;
	uint16_t i2cHandle;
	driver_state_t state;
	osMutexId lock;
}bme280_t;

// Calibration data
typedef struct
{
	uint16_t dig_T1;
	int16_t  dig_T2;
	int16_t  dig_T3;

	uint16_t dig_P1;
	int16_t  dig_P2;
	int16_t  dig_P3;
	int16_t  dig_P4;
	int16_t  dig_P5;
	int16_t  dig_P6;
	int16_t  dig_P7;
	int16_t  dig_P8;
	int16_t  dig_P9;

	uint8_t  dig_H1;
	int16_t  dig_H2;
	uint8_t  dig_H3;
	int16_t  dig_H4;
	int16_t  dig_H5;
	int8_t   dig_H6;
} bme280_calib_data;

typedef enum {
	SAMPLING_NONE = 0b000,
	SAMPLING_X1   = 0b001,
	SAMPLING_X2   = 0b010,
	SAMPLING_X4   = 0b011,
	SAMPLING_X8   = 0b100,
	SAMPLING_X16  = 0b101
} sensor_sampling_t;

typedef enum {
	MODE_SLEEP  = 0b00,
	MODE_FORCED = 0b01,
	MODE_NORMAL = 0b11
} sensor_mode_t;

typedef enum {
	FILTER_OFF = 0b000,
	FILTER_X2  = 0b001,
	FILTER_X4  = 0b010,
	FILTER_X8  = 0b011,
	FILTER_X16 = 0b100
} sensor_filter_t;

// Standby durations in ms
typedef enum {
	STANDBY_MS_0_5  = 0b000,
	STANDBY_MS_10   = 0b110,
	STANDBY_MS_20   = 0b111,
	STANDBY_MS_62_5 = 0b001,
	STANDBY_MS_125  = 0b010,
	STANDBY_MS_250  = 0b011,
	STANDBY_MS_500  = 0b100,
	STANDBY_MS_1000 = 0b101
} standby_duration_t;

// The config register
typedef struct {
	// inactive duration (standby time) in normal mode
	// 000 = 0.5 ms
	// 001 = 62.5 ms
	// 010 = 125 ms
	// 011 = 250 ms
	// 100 = 500 ms
	// 101 = 1000 ms
	// 110 = 10 ms
	// 111 = 20 ms
	unsigned int t_sb : 3;

	// filter settings
	// 000 = filter off
	// 001 = 2x filter
	// 010 = 4x filter
	// 011 = 8x filter
	// 100 and above = 16x filter
	unsigned int filter : 3;

	// unused - don't set
	unsigned int none : 1;
	unsigned int spi3w_en : 1;
} config;

// The ctrl_meas register
typedef struct {
	// temperature oversampling
	// 000 = skipped
	// 001 = x1
	// 010 = x2
	// 011 = x4
	// 100 = x8
	// 101 and above = x16
	unsigned int osrs_t : 3;

	// pressure oversampling
	// 000 = skipped
	// 001 = x1
	// 010 = x2
	// 011 = x4
	// 100 = x8
	// 101 and above = x16
	unsigned int osrs_p : 3;

	// device mode
	// 00       = sleep
	// 01 or 10 = forced
	// 11       = normal
	unsigned int mode : 2;
} ctrl_meas;

// The ctrl_hum register
typedef struct {
	// unused - don't set
	unsigned int none : 5;

	// hum oversampling
	// 000 = skipped
	// 001 = x1
	// 010 = x2
	// 011 = x4
	// 100 = x8
	// 101 and above = x16
	unsigned int osrs_h : 3;
} ctrl_hum;

/* Functions  ****************************************************/
retval_t bme280_create (bme280_t** device);
//uint8_t bme280_init (void);
retval_t bme280_init (bme280_t* device, i2c_driver_t* i2cDriver);
uint8_t readChipID (void);
void softReset (void);
uint8_t isReadingCalibration (void);
uint8_t isMeasuring (void);
void readCoefficients (void);
void setSampling (sensor_mode_t mode,
		 sensor_sampling_t tempSampling,
		 sensor_sampling_t pressSampling,
		 sensor_sampling_t humSampling,
		 sensor_filter_t filter,
		 standby_duration_t duration);
uint8_t read8 (uint8_t reg);
void write8 (uint8_t reg, uint8_t value);
uint16_t read16_LE (uint8_t reg);
int16_t readS16_LE (uint8_t reg);
uint16_t read16(uint8_t reg);
int32_t read20BitTempPress(uint8_t reg);
uint8_t getConfigReg (config conf);
uint8_t getCtrlMeasReg (ctrl_meas meas);
uint8_t getCtrlHumReg (ctrl_hum hum);
void setMode (sensor_mode_t mode);
void takeForcedMeasurement();
float readTemperature(void);
float readPressure(uint8_t refreshTemperature);
float readHumidity(uint8_t refreshTemperature);
float readAltitude(float seaLevelPressure, uint8_t refreshTemperature);
void readAllValuesAndRoundToInteger(int16_t *temp, uint8_t *hum, uint16_t *press);

#endif /* INC_TEMPHUMPRES_BME280_H_ */
