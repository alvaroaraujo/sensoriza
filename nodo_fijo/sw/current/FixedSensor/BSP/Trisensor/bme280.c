/*
 * temphumpres_bme280.c
 *
 *  Created on: 8 may 2018
 *      Author: albarc
 */

/* Includes ------------------------------------------------------------------*/
#include "bme280.h"
#include "math.h"

/* Variables ------------------------------------------------------------------*/
uint8_t tx_data[4];
uint8_t rx_data[4];

bme280_calib_data bme280_calib;
config configReg;
ctrl_meas measReg;
ctrl_hum humReg;
int32_t t_fine;

bme280_t* bme280;

osMutexDef (bme280_mutex);
/***************************************************************************/
/*************General Functions for interfacing with the sensors************/
/*********************************BEGINING**********************************/
retval_t
bme280_create (bme280_t** device)
{
    /* Allocate memory */
    bme280_t* new_device = pvPortMalloc(sizeof(bme280_t));
    if (new_device == NULL)
    {
        return RET_ERROR;
    }

    new_device->lock = osMutexCreate(osMutex(bme280_mutex));
    new_device->state = DRIVER_NOT_INIT;

    *device = new_device;
    return RET_OK;
}

retval_t
bme280_delete (bme280_t* device)
{
    if (device == NULL)
    {
        return RET_ERROR;
    }

    if (device->state != DRIVER_NOT_INIT)
    {
        return RET_ERROR;
    }

    /* Wait for mutex */
    osMutexWait(device->lock, osWaitForever);
    device->state = DRIVER_NOT_INIT;
    osMutexRelease(device->lock);

    /* Delete mutex */
    osMutexDelete(device->lock);

    /* Free memory */
    vPortFree(device);

    return RET_OK;
}

retval_t
bme280_init (bme280_t* device,
              i2c_driver_t* i2cDriver)
{
	/*Standard checkings*/
	if (device == NULL)
    {
        return RET_ERROR;
    }

    if (i2cDriver == NULL)
    {
        return RET_ERROR;
    }

    /* Wait for mutex */
    osMutexWait(device->lock, osWaitForever);

    /* Check for already initialized driver */
    if(device->state != DRIVER_NOT_INIT)
    {
        osMutexRelease(device->lock);
        return RET_ERROR;
    }

    device->i2cDriver = i2cDriver;

    /* Create I2C handle */
    device->i2cHandle = i2c_port_open(device->i2cDriver, BME280_ADDRESS, I2C_PORT_MASTER);
    if (device->i2cHandle == -1)
    {
        osMutexRelease(device->lock);
        return RET_ERROR;
    }

    /*Device specific checkings*/
    /* Read the CHIP ID to check the connection */
	uint8_t id = readChipID();

	if (id != BME280_CHIP_ID_VALUE) {
		return RET_ERROR;
	}

	/* Soft reset the device, making sure IIR is off, etc */
	softReset();

	/* Wait for the chip to wake up */
	osDelay(300);

	/* If the chip is still reading calibration, delay */
	while (isReadingCalibration()) {
		osDelay(100);
	}

	readCoefficients(); // read trimming parameters, see DS 4.2.2

	/* Recommended settings for weather monitoring (DS 3.5.1) */
	setSampling(MODE_FORCED,	// sensor mode
			 SAMPLING_X1,		// temp sampling
			 SAMPLING_X1,		// press sampling
			 SAMPLING_X1,		// hum sampling
			 FILTER_OFF,		// sensor filter
			 STANDBY_MS_250		// standby duration
			 );

	/*End of specific checkings*/

//    /* Setup config parameters */
//    if (bme280_register_write (device,
//                                BME280_CONFIG_REG,
//                                data_config) != RET_OK)
//    {
//        bme280_disable(device);
//        i2c_port_close (device->i2cDriver, device->i2cHandle);
//        osMutexRelease(device->lock);
//        return RET_ERROR;
//    }

    device->state = DRIVER_READY;

    osMutexRelease(device->lock);
    return RET_OK;
}

retval_t
bme280_deinit (bme280_t* device)
{
    if (device == NULL)
    {
        return RET_ERROR;
    }

    /* Wait for mutex */
    osMutexWait(device->lock, osWaitForever);

    /* Close I2C port */
    i2c_port_close(device->i2cDriver, device->i2cHandle);

    device->state = DRIVER_NOT_INIT;
    osMutexRelease(device->lock);
    return RET_OK;
}
/***************************************************************************/
/*************General Functions for interfacing with the sensors************/
/***********************************END*************************************/

/*****************************COMMENTED BY GUILJA***************************/
///**************************************************************************/
///*!
//    Initializes the device checking if it's properly connected
//    Returns TRUE if the sensor is found and responsive, FALSE otherwise
//*/
///**************************************************************************/
//uint8_t bme280_init (void)
//{
//	/* Read the CHIP ID to check the connection */
//	uint8_t id = readChipID();
//
//	if (id != BME280_CHIP_ID_VALUE) {
//		return FALSE;
//	}
//
//	/* Soft reset the device, making sure IIR is off, etc */
//	softReset();
//
//	/* Wait for the chip to wake up */
//	HAL_Delay(300);
//
//	/* If the chip is still reading calibration, delay */
//	while (isReadingCalibration()) {
//		HAL_Delay(100);
//	}
//
//	readCoefficients(); // read trimming parameters, see DS 4.2.2
//
//	/* Recommended settings for weather monitoring (DS 3.5.1) */
//	setSampling(MODE_FORCED,	// sensor mode
//			 SAMPLING_X1,		// temp sampling
//			 SAMPLING_X1,		// press sampling
//			 SAMPLING_X1,		// hum sampling
//			 FILTER_OFF,		// sensor filter
//			 STANDBY_MS_250		// standby duration
//			 );
//
//	HAL_Delay(100);
//
//	return TRUE;
//}
/************************END OF COMMENTED BY GUILJA************************/


/**************************************************************************/
/*!
    Reads the chip ID
    Should return 0x60
*/
/**************************************************************************/
uint8_t readChipID (void)
{
	uint8_t val = read8(BME280_REGISTER_CHIPID);

	return val;
}

/**************************************************************************/
/*!
    Performs a soft reset to the device by writing to a special register
*/
/**************************************************************************/
void softReset (void)
{
	write8(BME280_REGISTER_SOFTRESET, BME280_SOFTRESET_WORD);
}

/**************************************************************************/
/*!
    Returns true if the chip is busy reading calibration data
*/
/**************************************************************************/
uint8_t isReadingCalibration (void)
{
	uint8_t val = read8(BME280_REGISTER_STATUS);

	if ((val & BME280_STATUS_CAL_BUSY_MASK) != 0) { // Bit is set, device is busy calibrating
		return TRUE;
	} else {
		return FALSE;
	}
}

/**************************************************************************/
/*!
    Returns true if the chip is busy taking a measurement
*/
/**************************************************************************/
uint8_t isMeasuring (void)
{
	uint8_t val = read8(BME280_REGISTER_STATUS);

	if ((val & BME280_STATUS_MEAS_BUSY_MASK) != 0) { // Bit is set, device is busy measuring
		return TRUE;
	} else {
		return FALSE;
	}
}

/**************************************************************************/
/*!
    Reads the factory-set coefficients
*/
/**************************************************************************/
void readCoefficients (void)
{
	bme280_calib.dig_T1 = read16_LE(BME280_REGISTER_DIG_T1);
	bme280_calib.dig_T2 = readS16_LE(BME280_REGISTER_DIG_T2);
	bme280_calib.dig_T3 = readS16_LE(BME280_REGISTER_DIG_T3);

	bme280_calib.dig_P1 = read16_LE(BME280_REGISTER_DIG_P1);
	bme280_calib.dig_P2 = readS16_LE(BME280_REGISTER_DIG_P2);
	bme280_calib.dig_P3 = readS16_LE(BME280_REGISTER_DIG_P3);
	bme280_calib.dig_P4 = readS16_LE(BME280_REGISTER_DIG_P4);
	bme280_calib.dig_P5 = readS16_LE(BME280_REGISTER_DIG_P5);
	bme280_calib.dig_P6 = readS16_LE(BME280_REGISTER_DIG_P6);
	bme280_calib.dig_P7 = readS16_LE(BME280_REGISTER_DIG_P7);
	bme280_calib.dig_P8 = readS16_LE(BME280_REGISTER_DIG_P8);
	bme280_calib.dig_P9 = readS16_LE(BME280_REGISTER_DIG_P9);

	bme280_calib.dig_H1 = read8(BME280_REGISTER_DIG_H1);
	bme280_calib.dig_H2 = readS16_LE(BME280_REGISTER_DIG_H2);
	bme280_calib.dig_H3 = read8(BME280_REGISTER_DIG_H3);
	bme280_calib.dig_H4 = (read8(BME280_REGISTER_DIG_H4) << 4) | (read8(BME280_REGISTER_DIG_H4+1) & 0xF);
	bme280_calib.dig_H5 = (read8(BME280_REGISTER_DIG_H5+1) << 4) | (read8(BME280_REGISTER_DIG_H5) >> 4);
	bme280_calib.dig_H6 = (int8_t)read8(BME280_REGISTER_DIG_H6);
}

/**************************************************************************/
/*!
    Setups the sensor with the given parameters/settings
*/
/**************************************************************************/
void setSampling (sensor_mode_t mode,
		 sensor_sampling_t tempSampling,
		 sensor_sampling_t pressSampling,
		 sensor_sampling_t humSampling,
		 sensor_filter_t filter,
		 standby_duration_t duration)
{
	/* Put the device in sleep mode. Writes to the â€œconfigâ€� register
	 * in normal mode may be ignored. In sleep mode writes are not ignored.
	 * DS 5.4.6
	 */
	setMode(MODE_SLEEP);

	configReg.filter = filter;
	configReg.t_sb   = duration;

	write8(BME280_REGISTER_CONFIG, getConfigReg(configReg));

	/* Make sure to also set REGISTER_CONTROL after setting the
	 * CONTROLHUMID register, otherwise the values won't be applied.
	 * DS 5.4.3
	 */
	humReg.osrs_h    = humSampling;

	measReg.mode     = mode;
	measReg.osrs_t   = tempSampling;
	measReg.osrs_p   = pressSampling;

	write8(BME280_REGISTER_CONTROLHUMID, getCtrlHumReg(humReg));
	write8(BME280_REGISTER_CONTROL, getCtrlMeasReg(measReg));
}

/**************************************************************************/
/*!
    Reads an unsigned 8-bit data from a register
*/
/**************************************************************************/
uint8_t read8 (uint8_t reg)
{
//	retval_t
//	i2c_register_read (i2c_driver_t* i2c_driver,
//				   int16_t port_number,
//				   uint16_t reg_addr,
//				   uint16_t reg_addr_size,
//				   uint8_t *prx,
//				   uint16_t size)
	uint8_t data;

	retval_t statusRx = i2c_register_read(bme280->i2cDriver, bme280->i2cHandle, reg, 1, &data, 1);

//	HAL_StatusTypeDef statusRx = HAL_I2C_Mem_Read(&hi2c1, BME280_ADDRESS, reg, I2C_MEMADD_SIZE_8BIT, rx_data, 1, 100);

#if defined DEBUG_PERIPH_ERRORS
	if (statusRx == RET_OK){
		gpio_pin_toggle(STM_GPIOC, STM_GPIO_PIN_9); //Toggle the green LED
		gpio_pin_reset(STM_GPIOC, STM_GPIO_PIN_8); //Clear the red LED
//		HAL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin);
	}
	else
	{
		gpio_pin_toggle(STM_GPIOC, STM_GPIO_PIN_8); //Toggle the red LED
		gpio_pin_reset(STM_GPIOC, STM_GPIO_PIN_9); //Clear the green LED
//		HAL_GPIO_TogglePin(LD4_GPIO_Port, LD4_Pin);
	}
#endif

//	return rx_data[0];
	return data;
}

/**************************************************************************/
/*!
    Writes an 8-bit value to a register
*/
/**************************************************************************/
void write8 (uint8_t reg, uint8_t value)
{
	tx_data[0] = value;

	retval_t statusRx = i2c_register_write(bme280->i2cDriver, bme280->i2cHandle, reg, 1, tx_data, 1);
//	HAL_StatusTypeDef statusTx = HAL_I2C_Mem_Write(&hi2c1, BME280_ADDRESS, reg, I2C_MEMADD_SIZE_8BIT, tx_data, 1, 100);


#if defined DEBUG_PERIPH_ERRORS
	if (statusRx == RET_OK){
		gpio_pin_toggle(STM_GPIOC, STM_GPIO_PIN_9); //Toggle the green LED
		gpio_pin_reset(STM_GPIOC, STM_GPIO_PIN_8); //Clear the red LED
//		HAL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin);
	}
	else
	{
		gpio_pin_toggle(STM_GPIOC, STM_GPIO_PIN_8); //Toggle the red LED
		gpio_pin_reset(STM_GPIOC, STM_GPIO_PIN_9); //Clear the green LED
//		HAL_GPIO_TogglePin(LD4_GPIO_Port, LD4_Pin);
	}
#endif
}

/**************************************************************************/
/*!
    Reads an unsigned 16-bit data from a register-pair that's in little endian order [7:0][15:8]
*/
/**************************************************************************/
uint16_t read16_LE (uint8_t reg)
{
	uint16_t value;
	uint8_t data[2];

	retval_t statusRx = i2c_register_read(bme280->i2cDriver, bme280->i2cHandle, reg, 1, data, 2);

#if defined DEBUG_PERIPH_ERRORS
	if (statusRx == RET_OK){
		gpio_pin_toggle(STM_GPIOC, STM_GPIO_PIN_9); //Toggle the green LED
		gpio_pin_reset(STM_GPIOC, STM_GPIO_PIN_8); //Clear the red LED
	}
	else
	{
		gpio_pin_toggle(STM_GPIOC, STM_GPIO_PIN_8); //Toggle the red LED
		gpio_pin_reset(STM_GPIOC, STM_GPIO_PIN_9); //Clear the green LED
	}
#endif

	//	HAL_StatusTypeDef statusRx = HAL_I2C_Mem_Read(&hi2c1, BME280_ADDRESS, reg, I2C_MEMADD_SIZE_8BIT, rx_data, 2, 100);
	//
	//	if (statusRx == HAL_OK){
	//		HAL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin);
	//	} else {
	//		HAL_GPIO_TogglePin(LD4_GPIO_Port, LD4_Pin);
	//	}

	/*XXX-GuilJa.REVISAR ¿por qué no ha movido de primeras el [1] a la izquierda y OReado con el [0]?*/
	value = (data[0] << 8) | data[1];

	return (value >> 8) | (value << 8);	// This swaps the high byte with the low byte to present it correctly in LE
}

/**************************************************************************/
/*!
    Reads a signed 16-bit data from a register-pair that's in little endian order [7:0][15:8]
*/
/**************************************************************************/
int16_t readS16_LE (uint8_t reg)
{
	return (int16_t)read16_LE(reg);
}

/**************************************************************************/
/*!
    @brief  Reads a 16 bit value over I2C
*/
/**************************************************************************/
uint16_t read16(uint8_t reg)
{
    uint16_t value;
	uint8_t data[2];

	retval_t statusRx = i2c_register_read(bme280->i2cDriver, bme280->i2cHandle, reg, 1, data, 2);

#if defined DEBUG_PERIPH_ERRORS
	if (statusRx == RET_OK){
		gpio_pin_toggle(STM_GPIOC, STM_GPIO_PIN_9); //Toggle the green LED
		gpio_pin_reset(STM_GPIOC, STM_GPIO_PIN_8); //Clear the red LED
	}
	else
	{
		gpio_pin_toggle(STM_GPIOC, STM_GPIO_PIN_8); //Toggle the red LED
		gpio_pin_reset(STM_GPIOC, STM_GPIO_PIN_9); //Clear the green LED
	}
#endif

//    HAL_StatusTypeDef statusRx = HAL_I2C_Mem_Read(&hi2c1, BME280_ADDRESS, reg, I2C_MEMADD_SIZE_8BIT, rx_data, 2, 100);
//
//	if (statusRx == HAL_OK){
//		HAL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin);
//	} else {
//		HAL_GPIO_TogglePin(LD4_GPIO_Port, LD4_Pin);
//	}

	value = (data[0] << 8) | data[1];

    return value;
}

/**************************************************************************/
/*!
    @brief  Reads the 20 bit value of the temperature and pressure
    register sets, over I2C
*/
/**************************************************************************/
int32_t read20BitTempPress(uint8_t reg)
{
    int32_t value;
	uint8_t data[3];

	if ((reg != BME280_REGISTER_TEMPDATA) && (reg != BME280_REGISTER_PRESSUREDATA))
	{
    	return BME280_TEMP_PRESS_WRONG_READING_VALUE;
    }

	retval_t statusRx = i2c_register_read(bme280->i2cDriver, bme280->i2cHandle, reg, 1, data, 3);

#if defined DEBUG_PERIPH_ERRORS
	if (statusRx == RET_OK){
		gpio_pin_toggle(STM_GPIOC, STM_GPIO_PIN_9); //Toggle the green LED
		gpio_pin_reset(STM_GPIOC, STM_GPIO_PIN_8); //Clear the red LED
	}
	else
	{
		gpio_pin_toggle(STM_GPIOC, STM_GPIO_PIN_8); //Toggle the red LED
		gpio_pin_reset(STM_GPIOC, STM_GPIO_PIN_9); //Clear the green LED
	}
#endif

//    HAL_StatusTypeDef statusRx = HAL_I2C_Mem_Read(&hi2c1, BME280_ADDRESS, reg, I2C_MEMADD_SIZE_8BIT, rx_data, 3, 100);
//
//	if (statusRx == HAL_OK){
//		HAL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin);
//	} else {
//		HAL_GPIO_TogglePin(LD4_GPIO_Port, LD4_Pin);
//	}

	value = ((uint32_t)data[0] << 12) | ((uint32_t)data[1] << 4) | ((data[2] >> 4) & 0x0F);

    return value;
}

/**************************************************************************/
/*!
    Get the value from config register in the correct format
*/
/**************************************************************************/
uint8_t getConfigReg (config conf)
{
	return (conf.t_sb << 5) | (conf.filter << 2) | conf.spi3w_en;
}

/**************************************************************************/
/*!
    Get the value from ctrl_meas register in the correct format
*/
/**************************************************************************/
uint8_t getCtrlMeasReg (ctrl_meas meas)
{
	return (meas.osrs_t << 5) | (meas.osrs_p << 2) | meas.mode;
}

/**************************************************************************/
/*!
    Get the value from ctrl_hum register in the correct format
*/
/**************************************************************************/
uint8_t getCtrlHumReg (ctrl_hum hum)
{
	return (hum.osrs_h);
}

/**************************************************************************/
/*!
    Sets the sensor mode
*/
/**************************************************************************/
void setMode (sensor_mode_t mode)
{
	measReg.mode     = mode;

	write8(BME280_REGISTER_CONTROL, getCtrlMeasReg(measReg));
}

/**************************************************************************/
/*!
    Go to forced state and wait a measurement cycle, only if the sensor
    is in forced mode
*/
/**************************************************************************/
void takeForcedMeasurement()
{
	if(measReg.mode == MODE_FORCED)
	{ // If we are in forced mode
		setMode(MODE_FORCED);	// Go to forced measurement state
	}

	while (isMeasuring() == TRUE)
	{
		osDelay(1);
	}
}

/**************************************************************************/
/*!
    @brief  Returns the temperature from the sensor
*/
/**************************************************************************/
float readTemperature(void)
{
    int32_t var1, var2;

    int32_t adc_T = read20BitTempPress(BME280_REGISTER_TEMPDATA);
    if (adc_T == BME280_TEMP_PRESS_WRONG_READING_VALUE) // value in case temp measurement was disabled
        return NAN;

    var1 = ((((adc_T>>3) - ((int32_t)bme280_calib.dig_T1 <<1))) * ((int32_t)bme280_calib.dig_T2)) >> 11;

    var2 = (((((adc_T>>4) - ((int32_t)bme280_calib.dig_T1)) *
              ((adc_T>>4) - ((int32_t)bme280_calib.dig_T1))) >> 12) *
            ((int32_t)bme280_calib.dig_T3)) >> 14;

    t_fine = var1 + var2;

    float T = (t_fine * 5 + 128) >> 8;
    T /= 100.0F;
    return T;
//    return T/100.0F;
}


/**************************************************************************/
/*!
    @brief  Returns the pressure from the sensor in Pa unit
    @param	refreshTemperature TRUE if you want to update t_fine, FALSE otherwise
*/
/**************************************************************************/
float readPressure(uint8_t refreshTemperature)
{
    int64_t var1, var2, p;

    if (refreshTemperature == TRUE) {
    	readTemperature(); // update t_fine
    }

    int32_t adc_P = read20BitTempPress(BME280_REGISTER_PRESSUREDATA);
    if (adc_P == BME280_TEMP_PRESS_WRONG_READING_VALUE) // value in case pressure measurement was disabled
        return NAN;

    var1 = ((int64_t)t_fine) - 128000;
    var2 = var1 * var1 * (int64_t)bme280_calib.dig_P6;
    var2 = var2 + ((var1*(int64_t)bme280_calib.dig_P5)<<17);
    var2 = var2 + (((int64_t)bme280_calib.dig_P4)<<35);
    var1 = ((var1 * var1 * (int64_t)bme280_calib.dig_P3)>>8) +
           ((var1 * (int64_t)bme280_calib.dig_P2)<<12);
    var1 = (((((int64_t)1)<<47)+var1))*((int64_t)bme280_calib.dig_P1)>>33;

    if (var1 == 0) {
        return 0; // avoid exception caused by division by zero
    }
    p = 1048576 - adc_P;
    p = (((p<<31) - var2)*3125) / var1;
    var1 = (((int64_t)bme280_calib.dig_P9) * (p>>13) * (p>>13)) >> 25;
    var2 = (((int64_t)bme280_calib.dig_P8) * p) >> 19;

    p = ((p + var1 + var2) >> 8) + (((int64_t)bme280_calib.dig_P7)<<4);
    return (float)p/256;
}


/**************************************************************************/
/*!
    @brief  Returns the humidity from the sensor
    @param	refreshTemperature TRUE if you want to update t_fine, FALSE otherwise
*/
/**************************************************************************/
float readHumidity(uint8_t refreshTemperature)
{
	if (refreshTemperature == TRUE) {
		readTemperature(); // update t_fine
	}

    int32_t adc_H = read16(BME280_REGISTER_HUMIDDATA);
    if (adc_H == BME280_HUM_WRONG_READING_VALUE) // value in case humidity measurement was disabled
        return NAN;

    int32_t v_x1_u32r;

    v_x1_u32r = (t_fine - ((int32_t)76800));

    v_x1_u32r = (((((adc_H << 14) - (((int32_t)bme280_calib.dig_H4) << 20) -
                    (((int32_t)bme280_calib.dig_H5) * v_x1_u32r)) + ((int32_t)16384)) >> 15) *
                 (((((((v_x1_u32r * ((int32_t)bme280_calib.dig_H6)) >> 10) *
                      (((v_x1_u32r * ((int32_t)bme280_calib.dig_H3)) >> 11) + ((int32_t)32768))) >> 10) +
                    ((int32_t)2097152)) * ((int32_t)bme280_calib.dig_H2) + 8192) >> 14));

    v_x1_u32r = (v_x1_u32r - (((((v_x1_u32r >> 15) * (v_x1_u32r >> 15)) >> 7) *
                               ((int32_t)bme280_calib.dig_H1)) >> 4));

    v_x1_u32r = (v_x1_u32r < 0) ? 0 : v_x1_u32r;
    v_x1_u32r = (v_x1_u32r > 419430400) ? 419430400 : v_x1_u32r;
    float h = (v_x1_u32r>>12);
    return  h / 1024.0;
}


/**************************************************************************/
/*!
    Calculates the altitude (in meters) from the specified sea-level pressure (in hPa).
    @param  seaLevel      Sea-level pressure in hPa
    @param	refreshTemperature TRUE if you want to update t_fine, FALSE otherwise
*/
/**************************************************************************/
float readAltitude(float seaLevelPressure, uint8_t refreshTemperature)
{
    // Equation taken from BMP180 datasheet (page 16):
    //  http://www.adafruit.com/datasheets/BST-BMP180-DS000-09.pdf

    // Note that using the equation from wikipedia can give bad results
    // at high altitude. See this thread for more information:
    //  http://forums.adafruit.com/viewtopic.php?f=22&t=58064

    float atmospheric = readPressure(refreshTemperature) / 100.0F;
    return 44330.0 * (1.0 - pow(atmospheric / seaLevelPressure, 0.1903));
}


/**************************************************************************/
/*!
    Reads all values in a single function, returning them rounded in int form.
    @param  temp	pointer to the variable to store temperature, in ÂºC
    @param  hum		pointer to the variable to store humidity, in %
    @param  press	pointer to the variable to store pressure, in hPa
*/
/**************************************************************************/
void readAllValuesAndRoundToInteger(int16_t *temp, uint8_t *hum, uint16_t *press)
{
	takeForcedMeasurement();

    float tempFloat = readTemperature();
    float humFloat = readHumidity(FALSE);
    float pressFloat = readPressure(FALSE);

    *temp = (int16_t)(tempFloat + 0.5F);
    *hum = (uint8_t)(humFloat + 0.5F);
    *press = (uint16_t)((pressFloat/100.0F) + 0.5F);
}
