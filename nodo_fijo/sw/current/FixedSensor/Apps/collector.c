/*
 * collector.c
 *
 *  Created on: 4 jun. 2018
 *      Author: telek
 */

#include "collector.h"

extern tmp006_t* tmp006;
extern bc95_t* bc95;

//#define NBIOT_DYNAMIC_MEM

/* Standard functions for UI management */
static retval_t
collectorApp_suspend (app_t* app);

static retval_t
collectorApp_resume (app_t* app);

//XXX-GuilJa
static retval_t
collectorApp_buttonEvent (app_t* app, uiButton_t button, uiButtonEvent_t event);

void send_data(void);

/* Background app task */
void
collectorApp_task (void const * argument);

/* Thread and semaphore definitions */
//osThreadDef(collectorAppThread, collectorApp_task, osPriorityNormal, 0, 512);
osThreadId collectorAppThreadId;

osSemaphoreDef (collectorAppSemaphore);
osSemaphoreId  collectorAppSemaphoreId;

/* Application */
app_t collectorApp;

retval_t
collectorApp_init (ui_t* ui)
{
		/* Initialize app struct */
	collectorApp.menuTitle = "Collector";
	collectorApp.mode = UIAPP_NORMAL;
	collectorApp.app_funcs.ui_resume = &collectorApp_resume;
	collectorApp.app_funcs.ui_suspend = &collectorApp_suspend;
	collectorApp.app_funcs.ui_buttonEvent = &collectorApp_buttonEvent;

	/* Add app to UI menu */
	ui_addApp(ui, &collectorApp);

	/* Initialize OS features */
	osThreadDef(collectorAppThread, collectorApp_task, osPriorityNormal, 0, 512);
	collectorAppSemaphoreId = osSemaphoreCreate(osSemaphore(collectorAppSemaphore), 1);
	osSemaphoreWait(collectorAppSemaphoreId, osWaitForever);
	collectorAppThreadId = osThreadCreate(osThread(collectorAppThread), NULL);

	return RET_OK;
}

static retval_t
collectorApp_suspend (app_t* app)
{
	return RET_OK;
}

static retval_t
collectorApp_resume (app_t* app)
{
	ui_t* ui = (ui_t*) app->uiInstance;

	/* Write app contents */
	ui_setTitle (ui, "BATTERY");

	/* Wake up background task */
	osSemaphoreRelease(collectorAppSemaphoreId);

	return RET_OK;
}

static retval_t
collectorApp_buttonEvent (app_t* app, uiButton_t button, uiButtonEvent_t event)
{
	ui_t* ui = (ui_t*) app->uiInstance;

	if (event == UIBUTTON_PRESSED)
	{
		switch (button)
		{
			case UIBUTTON_B:
			break;
			case UIBUTTON_A:
				ui_showAppMenu(ui);
			break;
			default:
			break;
		}
	}
	return RET_OK;
}

void collectorApp_task(void const * argument)
{
// 	ui_t* ui = (ui_t*) collectorApp.uiInstance;

	/* Wait until UI resumes the app */
	gpio_pin_set (STM_GPIOC, STM_GPIO_PIN_8);
	for (;;)
	{
		osSemaphoreWait(collectorAppSemaphoreId, 500);
//		tmp006_calcTemp(tmp006);
//		int16_t bme_tmp;
//		uint8_t bme_hum;
//		uint16_t bme_pre;
//		readAllValuesAndRoundToInteger(&bme_tmp, &bme_hum, &bme_pre);
//		readTemperature();
//		uint32_t lumin;
//		lumin = tsl2561_readLuminosity ();
		gpio_pin_toggle (STM_GPIOC, STM_GPIO_PIN_9);
		send_data();
	}

}

void send_data(void)
{
		// PRUEBAS NB-IOT + JSON
	/* Rellenar estructura */
	json_all_fields_t example_fields;
	example_fields.node_id = 7;
	example_fields.latitude = 40.12345;
	example_fields.longitude = -3.26651;
	example_fields.air_temp = 17;
	example_fields.ground_temp = 19;
	example_fields.hum = 45;
	example_fields.press = 960;
	example_fields.lum = 512;
	example_fields.rain = 33;
	example_fields.salt = 22;

	/* Crear mensaje JSON */
#ifdef NBIOT_DYNAMIC_MEM
	char* prueba_message = pvPortMalloc(1024);
#else
	char prueba_message[1024];
#endif

	full_json_struct_writer(prueba_message, example_fields);

	// Setup nbiot module
	char *socket_ref;
	bc95_setup_nbiot_module(bc95, &socket_ref);

	uint16_t active_fields = 0x0000;
	active_fields = LATITUDE_MASK | LONGITUDE_MASK | AIR_TEMP_MASK | GROUND_TEMP_MASK | HUM_MASK | PRESS_MASK | LUM_MASK | RAIN_MASK | SALT_MASK;
	requested_fields_json_writer(prueba_message, active_fields, example_fields);

	while(1)
	{
		bc95_send_nbiot_message(bc95, socket_ref, prueba_message);
		gpio_pin_toggle (STM_GPIOC, STM_GPIO_PIN_8);
		osDelay(10000);
	}
	osDelay(5000);

	bc95_close_udp_socket(bc95, socket_ref);

#ifdef NBIOT_DYNAMIC_MEM
		vPortFree(prueba_message);
#endif
	// FIN PRUEBAS NB-IOT + JSON
}
