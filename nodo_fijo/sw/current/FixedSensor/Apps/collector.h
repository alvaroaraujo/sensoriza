/*
 * collector.h
 *
 *  Created on: 4 jun. 2018
 *      Author: telek
 */

#ifndef COLLECTOR_H_
#define COLLECTOR_H_

//#include "cmsis_os.h"
#include "types.h"
#include "buttons.h"
#include "ui.h"
#include "tmp006.h"
#include "bme280.h"
#include "tsl2561.h"
#include "bc95.h"
#include "json_helper.h"

retval_t
collectorApp_init (ui_t* ui);

#endif /* COLLECTOR_H_ */
